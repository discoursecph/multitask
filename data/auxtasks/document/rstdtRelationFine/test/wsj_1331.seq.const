GAF , Part III is scheduled to begin today .	(Topic-Shift(TextualOrganization
After two mistrials , the stakes in the stock manipulation trial of GAF Corp. and its vice chairman , James T. Sherwin , have changed considerably .	(elaboration-additional
The first two GAF trials were watched closely on Wall Street	(Topic-Drift(Contrast(explanation-argumentative
because they were considered to be important tests of the government 's ability	(elaboration-object-attribute
to convince a jury of allegations	(elaboration-general-specific(consequence
stemming from its insider-trading investigations .	consequence)
In an eight-count indictment , the government charged GAF , a Wayne , N.J. , chemical maker , and Mr. Sherwin with illegally attempting to manipulate the common stock of Union Carbide Corp. in advance of GAF 's planned sale of a large block of the stock in 1986 .	(evidence
The government 's credibility in the GAF case depended heavily on its star witness , Boyd L. Jefferies , the former Los Angeles brokerage chief	(elaboration-additional(elaboration-object-attribute
who was implicated by former arbitrager Ivan Boesky ,	(Sequence
and then pointed the finger at Mr. Sherwin , takeover speculator Salim B. Lewis and corporate raider Paul Bilzerian .	Sequence))
The GAF trials were viewed as previews of the government 's strength in its cases against Mr. Lewis and Mr. Bilzerian .	(List
Mr. Jefferies 's performance as a witness was expected to affect his sentencing .	List))))))
But GAF 's bellwether role was short-lived .	(explanation-argumentative
The first GAF trial ended in a mistrial after four weeks	(Sequence(circumstance
when U.S. District Judge Mary Johnson Lowe found	(attribution
that a prosecutor improperly , but unintentionally , withheld a document .	attribution))
After 93 hours of deliberation , the jurors in the second trial said	(consequence(attribution
they were hopelessly deadlocked ,	attribution)
and another mistrial was declared on March 22 .	consequence))))
Meanwhile , a federal jury found Mr. Bilzerian guilty on securities fraud and other charges in June .	(Sequence(antithesis(Sequence
A month later , Mr. Jefferies was spared a jail term by a federal judge	(Sequence(elaboration-object-attribute
who praised him	(reason
for helping the government .	reason))
In August , Mr. Lewis pleaded guilty to three felony counts .	Sequence))
Nevertheless , the stakes are still high for the players	(example(elaboration-object-attribute
directly involved in the GAF case .	elaboration-object-attribute)
The mistrials have left the reputations of GAF , Mr. Sherwin and GAF Chairman Samuel Heyman in limbo .	(List
For Mr. Sherwin , a conviction could carry penalties of five years in prison and a $ 250,000 fine on each count .	(List
GAF faces potential fines of $ 500,000 for each count .	List))))
Individuals familiar with the case said	(hypothetical(attribution
that throughout September , defense attorneys were talking with the government in an effort	(Contrast(elaboration-object-attribute
to prevent a trial ,	elaboration-object-attribute)
but by the end of the month the talks had ended .	Contrast))
There is much speculation among attorneys	(Contrast(elaboration-object-attribute(elaboration-object-attribute
not involved	elaboration-object-attribute)
that the strategy of GAF 's attorney , Arthur Liman , and Mr. Sherwin 's counsel , Stephen Kaufman , will include testimony by Mr. Sherwin or Mr. Heyman .	(Contrast
Neither testified at the previous trials .	Contrast))
For now , defense attorneys are tight-lipped about their plans .	(elaboration-additional
Max Gitter , another GAF defense attorney , said yesterday ,	(attribution
`` As we go in for the third time ,	(attribution(circumstance
Yogi Berra 's famous line is apt :	circumstance)
`It 's deja vu all over again. ' ''	attribution)))))))))
DALKON SHIELD CLAIMANTS hope to stop reorganization-plan appeal .	(Topic-Shift(TextualOrganization
Attorneys for more than 18,000 women	(evaluation(elaboration-additional(elaboration-additional(Same-Unit(elaboration-object-attribute
who claim injuries from the Dalkon Shield contraceptive device	elaboration-object-attribute)
have asked the U.S. Supreme Court to refuse to hear an appeal of the bankruptcy-law reorganization plan for A.H. Robins Co. ,	(elaboration-additional
which manufactured the device .	elaboration-additional))
The dispute pits two groups of claimants against each other .	(elaboration-set-member(elaboration-set-member
Baltimore attorney Michael A. Pretl and 17 other attorneys	(attribution(Same-Unit(elaboration-object-attribute
representing 18,136 claimants in the U.S. and abroad	elaboration-object-attribute)
argue	Same-Unit)
that the appeal would delay	(Same-Unit(interpretation
-- and perhaps even destroy --	interpretation)
a $ 2.38 billion settlement fund	(elaboration-object-attribute
that is the centerpiece of the reorganization plan .	elaboration-object-attribute))))
The bankruptcy-court reorganization is being challenged before the Supreme Court by a dissident group of claimants	(explanation-argumentative
because it places a cap on the total amount of money available	(List(elaboration-object-attribute
to settle claims .	elaboration-object-attribute)
It also bars future suits against Robins company officials , members of the Robins family and Robins 's former insurer , Aetna Life & Casualty Co .	(comment
The latter provision is `` legally unprecedented , ''	(attribution
said Alan B. Morrison , a public interest lawyer in Washington , D.C. ,	(elaboration-additional
who is challenging the plan on behalf of 900 claimants .	elaboration-additional)))))))
More than 100,000 claims against Robins are pending .	(elaboration-additional(background
The company made and marketed the Dalkon Shield in the early 1970s amid mounting evidence	(Sequence(elaboration-object-attribute
that it could cause serious injuries .	elaboration-object-attribute)
Robins has been in proceedings under Chapter 11 of the U.S. Bankruptcy Code since August 1985 ;	(elaboration-additional
such proceedings give it protection from creditor lawsuits	(temporal-same-time
while it works out a plan for paying its debts .	temporal-same-time))))
American Home Products Corp. wants to acquire Robins ,	(condition
but only if all legal challenges to the plan are exhausted .	condition)))
In a brief	(Sequence(Same-Unit(elaboration-object-attribute
filed with the Supreme Court last week ,	elaboration-object-attribute)
Mr. Pretl criticizes the appeal	(elaboration-object-attribute
for raising `` abstract '' and `` theoretical '' legal issues ,	(Temporal-Same-Time
while jeopardizing the proposed reorganization and the settlement payments to claimants .	Temporal-Same-Time)))
The Supreme Court is scheduled to consider the case Nov. 3	(List
and may issue a decision as early as Nov. 6 .	List))))
JURY`S CRIMINAL CONVICTION under Superfund law is a first .	(Topic-Shift(TextualOrganization
Charles A. Donohoo , sole proprieter of a Louisville , Ky. , demolition company , was found guilty of violating the Superfund law as well as the Clean Air Act .	(elaboration-general-specific(explanation-argumentative(Topic-Comment
Criminal convictions under the federal Superfund law are rare ,	(background
and the decision is the first jury verdict in such a case .	background))
Under Superfund , those	(antithesis(elaboration-additional(concession(Same-Unit(elaboration-object-attribute
who owned , generated or transported hazardous waste	elaboration-object-attribute)
are liable for its cleanup ,	Same-Unit)
regardless of whether their actions were legal at the time .	concession)
Environmental lawyers say	(attribution
virtually all of the Superfund cases to date have involved civil penalties	(elaboration-object-attribute
designed to insure cleanup of past polluting activities .	elaboration-object-attribute)))
But Superfund also contains a criminal provision	(background(elaboration-object-attribute
concerning the release of toxic substances into the environment .	elaboration-object-attribute)
In 1986 Congress strengthened the penalty	(means
by making it a felony .	means))))
Mr. Donohoo was convicted in Louisville late last month of violating Superfund	(Sequence(comment(List(consequence
by failing to report the release of asbestos into the environment from a building	(elaboration-object-attribute
he was demolishing .	elaboration-object-attribute))
He was also convicted of failing to properly remove asbestos from the building , a violation of the Clean Air Act .	List)
The government sought a criminal penalty	(explanation-argumentative(reason
because `` no cleanup is possible here .	reason)
Once { asbestos } is released into the environment ,	(attribution(circumstance
it can lodge anywhere , ''	circumstance)
says Richard A. Dennis , the assistant U.S. attorney	(elaboration-object-attribute
who prosecuted the case .	elaboration-object-attribute))))
Mr. Donohoo is scheduled to be sentenced Dec. 11 .	(hypothetical(elaboration-additional
His lawyer could not be reached for comment .	elaboration-additional)
Mr. Donohoo faces as much as three years in prison and a $ 250,000 fine for the Superfund conviction and as much as one year in prison and a $ 100,000 fine for the violation of the Clean Air Act .	hypothetical))))
TED BUNDY 'S LAWYERS switch to victims ' side in death-sentence case .	(Topic-Shift(TextualOrganization
Wilmer , Cutler & Pickering , the Washington , D.C. , law firm	(interpretation(elaboration-additional(elaboration-additional(circumstance(means(Same-Unit(elaboration-object-attribute
that spent over $ 1 million	(manner
fighting the execution of mass-murderer Ted Bundy	(elaboration-additional
-- who eventually was executed --	elaboration-additional)))
has taken on another death penalty case before the Supreme Court , this time on the side of the family of four murder victims in Arkansas .	Same-Unit)
The law firm has filed a friend-of-the-court brief jointly with the Washington Legal Foundation , a conservative legal group .	means)
The key issue in the case ,	(Same-Unit(elaboration-additional
which the law firm is handling without a fee , or pro bono ,	elaboration-additional)
is whether a person	(Same-Unit(elaboration-object-attribute
sentenced to death	elaboration-object-attribute)
can voluntarily waive his rights of appellate review .	Same-Unit)))
The murderer , Ronald Gene Simmons , was convicted of killing 14 people .	(circumstance(background
Another murderer on death row has appealed Mr. Simmons 's death sentence in a `` next friend '' capacity .	background)
Wilmer Cutler 's brief argues	(attribution
that there is no mandatory appellate review of capital sentences	(List
and that the inmate	(Same-Unit(elaboration-object-attribute
who filed the appeal	elaboration-object-attribute)
lacks proper standing .	Same-Unit)))))
P.J. Mode , Wilmer Cutler 's managing partner , says	(attribution
the trial team	(Question-Answer(attribution(Same-Unit(elaboration-object-attribute
that represented Mr. Bundy	elaboration-object-attribute)
was asked by the firm 's pro bono committee	Same-Unit)
whether the new case posed a conflict	attribution)
and that no objections were raised .	Question-Answer)))
The coupling of the law firm and the Washington Legal Foundation is odd also ,	(comment(explanation-argumentative
because Wilmer Cutler was one of the firms	(consequence(elaboration-object-attribute
singled out for criticism two years ago by the conservative legal group	elaboration-object-attribute)
for displaying a liberal bias in its pro bono work .	consequence))
`` We give them a lot of credit	(attribution(circumstance
for taking this case , ''	circumstance)
says WLF 's Alan Slobodin .	attribution))))
THE CASE OF THE FAKE DALIS :	(TextualOrganization
In federal court in Manhattan , three defendants pleaded guilty to charges of fraud in connection with the sale of fake Salvador Dali lithographs .	(elaboration-additional(elaboration-general-specific
James Burke and Larry Evans , formerly owners of the now-defunct Barclay Gallery , and Prudence Clark , a Barclay sales representative , were charged with conducting high-pressure telephone sales	(elaboration-additional(elaboration-object-attribute
in which they misrepresented cheap copies of Dali artwork as signed , limited-edition lithographs .	elaboration-object-attribute)
The posters were sold for $ 1,300 to $ 6,000 ,	(Contrast
although the government says	(attribution
they had a value of only $ 53 to $ 200 apiece .	attribution))))
Henry Pitman , the assistant U.S. attorney	(Statement-Response(attribution(Same-Unit(elaboration-object-attribute
handling the case ,	elaboration-object-attribute)
said	Same-Unit)
about 1,000 customers were defrauded	(List
and that Barclay 's total proceeds from the sales were $ 3.4 million .	List))
Attorneys for Messrs. Burke and Evans and Ms. Clarke said	(explanation-argumentative(attribution
that	(Same-Unit
although their clients admitted to making some misrepresentations in the sales ,	(concession
they had believed	(attribution
that the works were authorized by Mr. Dali ,	(elaboration-additional
who died in January .	elaboration-additional)))))
The posters were printed on paper	(attribution(elaboration-object-attribute
pre-signed by Mr. Dali ,	elaboration-object-attribute)
the attorneys said .	attribution)))))))))
