Britain 's current account deficit dropped to # 1.6 billion ( $ 2.56 billion ) in September from an adjusted # 2 billion ( $ 3.21 billion ) the previous month , but the improvement comes amid increasing concern that a recession could strike the U.K. economy next year .	O	9..267
The Confederation of British Industry 's latest survey shows that business executives expect a pronounced slowdown , largely because of a 16-month series of interest-rate increases that has raised banks ' base lending rates to 15 % .	O	270..498
" The outlook has deteriorated since the summer , with orders and employment falling and output at a standstill , " said David Wigglesworth , chairman of the industry group 's economic committee .	B-expansion	501..690
He also said investment by businesses is falling off .	I-expansion	691..744
Of 1,224 companies surveyed , 31 % expect to cut spending on plant equipment and machinery , while only 28 % plan to spend more .	B-comparison	745..869
But despite mounting recession fears , government data do n't yet show the economy grinding to a halt .	I-comparison	872..972
Unemployment , for example , has continued to decline , and the September trade figures showed increases in both imports and exports .	O	973..1103
As a result , Prime Minister Margaret Thatcher 's government is n't currently expected to ease interest rates before next spring , if then .	O	1106..1241
Chancellor of the Exchequer Nigel Lawson views the high rates as his chief weapon against inflation , which was ignited by tax cuts and loose credit policies in 1986 and 1987 .	O	1242..1416
Officials fear that any loosening this year could rekindle inflation or further weaken the pound against other major currencies .	O	1417..1545
Fending off attacks on his economic policies in a House of Commons debate yesterday , Mr. Lawson said inflation " remains the greatest threat to our economic well-being " and promised to take " whatever steps are needed " to choke it off .	O	1548..1781
The latest government figures said retail prices in September were up 7.6 % from a year earlier .	O	1782..1877
Many economists have started predicting a mild recession next year .	O	1880..1947
David Owen , U.K. economist with Kleinwort Benson Group , reduced his growth forecast for 1990 to 0.7 % from 1.2 % and termed the risk of recession next year " quite high . "	B-comparison	1948..2115
But he said the downturn probably wo n't become a " major contraction " similar to those of 1974 and 1982 .	I-comparison	2116..2219
Still , Britain 's current slump is a cause for concern here as the nation joins in the European Community 's plan to create a unified market by 1992 .	O	2222..2369
Compared with the major economies on the Continent , the U.K. faces both higher inflation and lower growth in the next several months .	O	2370..2503
As a result , Mr. Owen warned , investment will be more likely to flow toward the other European economies and " the U.K. will be less prepared for the single market . "	O	2504..2668
Britain 's latest trade figures contained some positive news for the economy , such as a surge in the volume of exports , which were 8.5 % higher than a year earlier .	B-comparison	2671..2833
But while September exports rose to # 8.43 billion , imports shot up to # 10.37 billion .	I-comparison	2834..2919
The resulting # 1.9 billion merchandise trade deficit was partly offset by an assumed surplus of # 300 million in so-called invisible items , which include income from investments , services and official transfers .	O	2920..3130
Despite the narrowing of the monthly trade gap , economists expect the current account deficit for all of 1989 to swell to about # 20 billion from # 14.6 billion in 1988 .	O	3133..3300
Increasingly , economists say the big deficit reflects the slipping competitive position of British industry .	O	3301..3409
" When the country gets wealthier , we tend to buy high-quality imports , " Mr. Owen said .	O	3410..3496
