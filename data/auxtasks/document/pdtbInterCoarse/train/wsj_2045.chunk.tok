Markets usually get noticed because they soar or plunge .	B-comparison	9..65
Gold , which has n't risen or fallen significantly in quite some time , yesterday achieved what may be a new level of impassiveness : The most actively traded futures contracts closed unchanged despite nervous fluctuations in both the dollar and the stock market .	I-comparison	66..325
The settlement prices of the December , February and April gold contracts were even with Monday 's final prices .	B-expansion	328..438
The December 1989 contract , which has the greatest trading volume , ended at $ 371.20 an ounce .	I-expansion	439..532
The other months posted advances of 10 cents to 20 cents an ounce .	O	533..599
According to one analyst , Bernard Savaiko of PaineWebber , New York , the stock market 's ability on Monday to rally from last Friday 's decline -- which seemed to indicate that the economy was n't going to fall either -- took the starch out of precious metals prices , and out of gold 's , in particular .	O	602..899
Yesterday , gold traded within a narrow range .	B-comparison	902..947
Gold tried to rally on Monday but ran into the same situation that has subdued gold prices for more than a year : selling by gold producers , who want to fix the highest possible price for their gold .	I-comparison	948..1146
" December delivery gold is trading in a range of $ 365 to $ 375 { an ounce } and is having difficulty breaking out above that , " Mr. Savaiko said .	O	1149..1290
" Producers at the moment regard that area a good one in which to sell gold . "	O	1291..1367
Also , Mr. Savaiko noted , stock market investors seeking greater safety are veering toward buying bonds rather than precious metals because " we are tending more toward a disinflationary economy that does n't make gold and precious metals attractive . "	O	1370..1618
Jeffrey Nichols , president of APMS Canada , Toronto precious metals advisers , said there is little to motivate gold traders to buy the metal .	O	1621..1761
" Investors in the U.S. and Europe are comfortable with the actions of the { Federal Reserve } in its willingness to supply liquidity to financial system , which helped the stock market rebound on Monday , " he said .	O	1762..1972
There is n't any rush on the part of investors in the West to buy gold , he said .	O	1975..2054
" They still bear the memory of October 1987 , when they bought gold after the stock market crashed and ended up losing money because gold prices subsequently fell , " Mr. Nichols said .	O	2055..2236
" It 's an experience they do n't want to repeat . "	O	2237..2284
At the moment gold traders are n't concerned about inflation , he said , and as for the dollar , " gold 's association with the currency has been diminishing recently so drops in the currency are n't having much impact on gold . "	O	2287..2508
Dinsa Mehta , chief bullion trader for Chase Manhattan Bank , said : " There is little incentive on the part of traders to sell gold because the stock market may go lower and gold may retain some of its ' flight to safety ' quality .	O	2511..2737
There is little incentive to buy gold because if the stock market goes higher , it may be just a false alarm .	B-contingency	2738..2846
This is keeping the gold traders handcuffed . "	I-contingency	2847..2892
The most remarkable feature about yesterday 's action was that the price of roughly $ 370 an ounce was regarded as attractive enough by gold producers around the world to aggressively sell gold , Mr. Mehta said .	O	2895..3103
" I do n't know what it means over the long run , but for the short term , it appears that gold producers are grateful for the $ 10 or so that gold has risen over the past week or so , " he said .	O	3106..3294
Previously , he noted , gold producers tended to back off from a rising gold market , letting prices rise as much as possible before selling .	O	3297..3435
Mr. Mehta observed that the U.S. merchandise trade deficit , which rose sharply in August , according to yesterday 's report , has been having less and less impact on the gold market .	O	3438..3617
" The dollar has n't reacted much to it , so gold has n't either , " he said .	O	3618..3689
In other commodity markets yesterday :	O	3692..3729
ENERGY :	O	3732..3739
Crude oil prices rose slightly in lackluster activity as traders in the pits tried to assess action in the stock market .	B-expansion	3740..3860
Since stock market indexes plummeted last Friday , participants in all markets have been wary .	B-comparison	3861..3954
When traders become confident that the stock market has stabilized , oil prices are expected to rise as supply and demand fundamentals once again become the major consideration .	B-temporal	3955..4131
Crude oil for November delivery edged up by 16 cents a barrel to $ 20.75 a barrel .	B-expansion	4132..4213
Heating oil prices also rose .	B-comparison	4214..4243
November gasoline slipped slightly .	I-comparison	4244..4279
SUGAR :	O	4282..4288
Futures prices rose on a report that Cuba may seek to postpone some sugar shipments .	B-expansion	4289..4373
The March contract advanced 0.14 cent a pound to 14.11 cents .	I-expansion	4374..4435
According to an analyst , Cuba ca n't meet all its shipment commitments and has asked Japan to accept a delay of shipments scheduled for later this year , into early next year .	O	4436..4609
" Japan is perceived as a wealthy nation that can turn elsewhere in the world market and buy the sugar , " the analyst said .	O	4610..4731
It was the possibility of this demand that helped firm prices , the analyst said .	B-temporal	4732..4812
Another analyst noted that Cuba has been deferring shipments in recent years .	I-temporal	4813..4890
" To the professionals in the trade it did n't cause much surprise .	O	4891..4956
The March futures contract traded as high as 14.24 cents , but could n't sustain the advance , " he said .	O	4957..5058
LIVESTOCK AND MEATS :	O	5061..5081
The prices of cattle , hogs and pork belly futures contracts rebounded as livestock traders shook off fears that the Friday stock market plunge would chill consumer spending , which in turn would hurt retail sales of beef and pork .	B-entrel	5082..5311
The prices of most livestock futures contracts had dropped sharply Monday .	B-expansion	5312..5386
Cattle futures prices were also supported yesterday by signs that supermarket chains are making plans to increase their promotions concerning beef .	I-expansion	5387..5534
GRAINS AND SOYBEANS :	O	5537..5557
The prices of most soybean and soybean-meal futures contracts rose amid rumors that the Soviet Union is interested in buying from the U.S. or South America about 250,000 metric tons of soybeans and as many as 400,000 metric tons of soybean meal .	B-expansion	5558..5803
Traders are especially sensitive to reports of possible U.S. soybean sales because U.S. exports are lagging .	I-expansion	5804..5912
Since Sept. 1 , about 13 million fewer bushels of U.S. soybeans have been sold overseas than for the same period last year .	O	5913..6035
Corn futures prices rose slightly while wheat prices settled mixed .	O	6036..6103
