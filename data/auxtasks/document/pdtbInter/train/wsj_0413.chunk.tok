If there 's somethin' strange in your neighborhood ...	O	9..62
If there 's something ' weird and it do n't look good .	O	65..116
Who ya gon na call ?	O	119..137
For starters , some people call Ed and Lorraine Warren .	O	140..194
When it comes to busting ghosts , the Monroe , Conn. , couple are perfect demons .	B-restatement	197..275
They claim to have busted spirits , poltergeists and other spooks in hundreds of houses around the country .	B-conjunction	276..382
They say they now get three or four " legitimate " calls a week from people harried by haunts .	I-conjunction	383..475
" I firmly believe in angels , devils and ghosts , " says Mr. Warren , whose business card identifies him as a " demonologist . "	O	476..597
If psychics do n't work , but your house still seems haunted , you can call any one of a swelling band of skeptics such as Richard Busch .	O	600..734
A professional magician and musician , he heads the Pittsburgh branch of the Committee for the Scientific Investigation of the Paranormal .	O	735..872
Mr. Busch says there is a scientific explanation for all haunts , and he can even tell you how to encourage the spirits .	O	873..992
" All you have to do is eat a big pizza , and then go to bed , " he says .	O	993..1062
" You 'll have weird dreams , too . "	O	1063..1095
Either way , the ghostbusting business is going like gangbusters .	B-restatement	1098..1162
Tales of haunts and horrors are proliferating beyond the nation 's Elm Streets and Amityvilles .	I-restatement	1163..1257
" I get calls nearly every day from people who have ghosts in their house , " says Raymond Hyman , a skeptical psychology professor at the University of Oregon .	O	1258..1414
In a public opinion poll published in the October issue of Parents Magazine , a third of those queried said they believe that ghosts or spirits make themselves known to people .	O	1417..1592
" The movies , the books , the tabloids -- even Nancy Reagan is boosting this stuff , " says Paul Kurtz , a philosophy professor at the State University of New York at Buffalo , who heads the Committee for the Scientific Investigation of the Paranormal .	B-entrel	1593..1839
The committee , formed in 1967 , now has 60 chapters around the world .	I-entrel	1840..1908
The spirits , of course , could hardly care less whether people do or do n't believe in them .	B-conjunction	1911..2001
They do n't even give a nod to human sensibilities by celebrating Halloween .	I-conjunction	2002..2077
For the spooks it 's just another day of ectoplasmic business as usual , ghostbusters say ; the holiday seems to occasion no unusual number of ghost reports .	O	2078..2232
One of the busiest ghostbusters is Robert Baker , a 68-year-old semi-retired University of Kentucky psychology professor whose bushy gray eyebrows arch at the mere mention of a ghost .	B-entrel	2235..2417
Mr. Baker says he has personally bested more than 50 haunts , from aliens to poltergeists .	I-entrel	2418..2507
Mr. Baker heads the Kentucky Association of Science Educators and Skeptics .	B-entrel	2510..2585
Like Hollywood 's Ghostbusters , Kentucky 's stand ready to roll when haunts get out of hand .	B-concession	2586..2676
But they do n't careen around in an old Cadillac , wear funny suits or blast away at slimy spirits .	B-alternative	2677..2774
Mr. Baker drives a 1987 Chevy and usually wears a tweed jacket on his ghostbusting forays .	I-alternative	2775..2865
" I 've never met a ghost that could n't be explained away by perfectly natural means , " he says .	O	2868..2961
When a Louisville woman complained that a ghost was haunting her attic , Mr. Baker discovered a rat dragging a trap across the rafters .	O	2964..3098
A foul-smelling demon supposedly plagued a house in Mannington , Ky .	B-contrast	3101..3168
Mr. Baker found an opening under the house that led to a fume-filled coal mine .	B-entrel	3169..3248
When the weather cools , Mr. Baker says , hobos often hole up in abandoned houses .	I-entrel	3249..3329
" People see activity in there , and the next thing you know , you 've got a haunting , " he says .	O	3330..3422
On a recent afternoon , Mr. Baker and a reporter go ghost-busting , visiting Kathleen Stinnett , a Lexington woman who has phoned the University of Kentucky to report mysterious happenings in her house .	O	3425..3624
Mrs. Stinnett says she never believed in ghosts before , but lately her vacuum cleaner turned itself on , a telephone flew off its stand , doors slammed inexplicably , and she heard footsteps in her empty kitchen .	O	3625..3834
" I was doing the laundry and nearly broke my neck running upstairs to see who was there , and it was nobody , " she says , eyes wide at the recollection .	O	3837..3986
Mr. Baker hears her out , pokes around a bit , asks a few questions and proposes some explanations .	O	3989..4086
Of the self-starting vacuum cleaner , he says : " Could be Cuddles , { Mrs. Stinnett 's dog } . "	B-conjunction	4087..4175
The flying telephone : " You tangle the base cord around a chair leg , and the receiver does seem to fly off . "	B-conjunction	4176..4283
The ghostly footsteps : " Interstate 64 is a block away , and heavy traffic can sure set a house to vibrating . "	I-conjunction	4284..4392
" I 'm not sure he 's explained everything , " Mrs. Stinnett says grudgingly .	O	4395..4467
" There are some things that have gone on here that nobody can explain , " she says .	B-entrel	4468..4549
Mr. Baker promises to return if the haunting continues .	I-entrel	4550..4605
For especially stubborn haunts , Mr. Baker carries a secret weapon , a vial of cornstarch .	O	4608..4696
" I tell people it 's the groundup bones of saints , " he says .	O	4697..4756
" I sprinkle a little around and tell the demons to leave .	O	4757..4814
It 's reassuring , and it usually works . "	O	4815..4854
Oregon 's Mr. Hyman has investigated claims of flying cats , apparitions and bouncing chandeliers and has come up with a plausible explanation , he says , for every one .	O	4857..5022
" Invariably , " he says , " eyewitnesses are untrustworthy . "	O	5023..5079
Two years ago , a Canadian reader bet Omni Magazine $ 1,000 that it could n't debunk the uncanny goings-on in " the Oregon Vortex , " a former Indian burial ground in southern Oregon .	O	5082..5259
To viewers from a distance , visitors to the spot seemed to shrink disproportionately , relative to the background .	B-entrel	5260..5373
The magazine called in Mr. Hyman as a consultant .	I-entrel	5374..5423
He showed up with a carpenter 's level , carefully measured every surface and showed how the apparent shrinkage was caused by the perspective .	O	5426..5566
" A very striking illusion , " Mr. Hyman says now , his voice dripping with skepticism , " but an illusion nevertheless . "	O	5567..5682
The Canadian wound up writing a check .	O	5683..5721
The Rev. Alphonsus Trabold , a theology professor and exorcism expert at St. Bonaventure University in Olean , N.Y. , frequently is asked to exorcise unruly spirits , and he often obliges .	O	5724..5908
" On certain occasions a spirit could be earthbound and make itself known , " he says .	O	5909..5992
" It happens . "	O	5993..6006
Father Trabold often uses what he calls " a therapeutic exorcism " : a few prayers and an admonition to the spirit to leave .	O	6009..6130
" If the person believes there 's an evil spirit , you ask it to be gone , " he says .	O	6131..6211
" The suggestion itself may do the healing . "	O	6212..6255
But sometimes more energetic attacks are required .	O	6258..6308
To wrestle with a demon in a house owned by a Litchfield , Conn. , woman , the Warrens recently called in an exorcist , the Rev. Robert McKenna , a dissident clergyman who hews to the Catholic Church 's old Latin liturgy .	O	6309..6524
I attend , and so does a television crew from New York City .	O	6525..6584
Mr. Warren pronounces the Litchfield case " your typical demonic infestation . "	O	6587..6664
A Scottish dwarf built the small red house 110 years ago and now his demonic ghost haunts it , Mr. Warren says .	B-conjunction	6665..6775
The owner , who begs anonymity , asserts that the dwarf , appearing as a dark shadow , has manhandled her , tossing her around the living room and yanking out a hank of hair .	B-asynchronous	6776..6945
Two previous exorcisms have failed .	I-asynchronous	6946..6981
" This is a very tenacious ghost , " Mr. Warren says darkly .	O	6984..7041
Father McKenna moves through the house praying in Latin , urging the demon to split .	B-asynchronous	7044..7127
Suddenly the woman begins swaying and then writhing .	I-asynchronous	7128..7180
" She 's being attacked by the demon , " Mrs. Warren stagewhispers as the priest sprinkles holy water over the squirming woman , and the television camera grinds .	O	7181..7338
A half-hour later , the woman is smiling and chatting ; the demon seems to have gone .	O	7339..7422
But Mr. Warren says the woman has " psychic burns " on her back from the confrontation .	O	7423..7508
She declines to show them .	O	7509..7535
" This was an invisible , powerful force that 's almost impossible for a layman to contemplate , " Mr. Warren says solemnly as the ghostbusting entourage packs up to leave .	O	7538..7705
" This time though , " he says , " I think we got it . "	O	7706..7755
Lyrics from " Ghostbusters " by Ray S. Parker Jr. 1984 by Golden Torch Music Corp. ( ASCAP ) and Raydiola Music ( ASCAP ) .	O	7758..7874
All administrative rights for the U.S. jointly controlled by both companies .	O	7875..7951
International copyright secured .	O	7952..7984
Made in USA .	O	7985..7997
All rights Reserved .	O	7998..8018
Reprinted by permission .	O	8019..8043
