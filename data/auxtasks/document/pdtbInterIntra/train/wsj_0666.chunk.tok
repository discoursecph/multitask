Wall Street is just about ready to line the bird cage with paper stocks .	root	9..81
For three years , a healthy economy and the export-boosting effects of a weak dollar propelled sales and earnings of the big paper companies to record levels .	norel	84..241
As the good times rolled they more than doubled their prices for pulp , a raw material used in all sorts of paper , to $ 830 a metric ton this past spring from $ 380 a ton at the start of 1986 .	restatement	242..431
But now the companies are getting into trouble	contrast	434..480
because they undertook a record expansion program while they were raising prices sharply .	cause	481..570
Third-quarter profits fell at several companies .	cause	571..619
" Put your money in a good utility or bank stock , not a paper company , " advises George Adler of Smith Barney .	norel	622..730
Other analysts are nearly as pessimistic .	norel	733..774
Gary Palmero of Oppenheimer & Co. expects a 30 % decline in earnings between now and 1991 for " commodity-oriented " paper companies , which account for the majority of the industry .	instantiation	775..953
Robert Schneider of Duff & Phelps sees paper-company stock prices falling 10 % to 15 % in 1990 , perhaps 25 %	list	954..1059
if there 's a recession .	condition	1060..1083
Paper companies concede that business has been off recently .	norel	1086..1146
But they attribute much of the weakness to customer inventory reductions .	concession	1147..1220
Generally they maintain that , barring a recession and a further strengthening of the dollar against foreign currencies , the industry is n't headed for a prolonged slump .	cause	1221..1389
" It wo n't be an earthshaking drop , " a Weyerhaeuser spokesman says .	restatement	1390..1456
Last week Mr. Adler lowered his rating from hold to " avoid " on Boise Cascade , Champion International , Great Northern Nekoosa , International Paper , Louisiana Pacific and Weyerhaeuser .	norel	1459..1641
Oppenheimer 's Mr. Palmero , meanwhile , is steering clear of Gaylord Container , Stone Container and Federal Paper Board .	synchrony	1642..1760
Mr. Schneider is cool to Georgia Pacific and Abitibi-Price .	list	1761..1820
Lawrence Ross of PaineWebber would avoid Union Camp .	list	1821..1873
The companies in question believe the analysts are too pessimistic .	norel	1876..1943
Great Northern Nekoosa said , " The odds of the dire predictions about us being right are small . "	instantiation	1944..2039
International Paper emphasizes that it is better positioned than most companies for the coming overcapacity because its individual mills can make more than one grade of paper .	list	2040..2215
A Boise-Cascade spokesman referred to a speech by Chairman John Fery , in which he said that markets generally are stable , although some risk of further price deterioration exists .	norel	2218..2397
Stone Container Chairman Roger Stone said that , unlike for some other paper products , demand for Stone 's principal commodity , unbleached containerboard , remains strong .	norel	2400..2568
He expects the price for that product to rise even more next year .	conjunction	2569..2635
Gaylord Container said analysts are skeptical of it because it 's carrying a lot of debt .	norel	2638..2726
Champion International said , " We 've gotten our costs down and we 're better positioned for any cyclical downturn than we 've ever been . "	contrast	2727..2861
Louisiana Pacific and Georgia Pacific said a number of other analysts are recommending them because of their strong wood-products business .	norel	2864..3003
Federal Paper Board said , " We 're not as exposed as the popular perception of us . "	norel	3006..3087
The company explained that its main product , bleached paperboard , which goes into some advertising materials and white boxes , historically does n't have sharp price swings .	restatement	3088..3259
Because the stock prices of some paper companies already reflect an expected profit slump , PaineWebber 's Mr. Ross says he thinks that next year the share prices of some companies may fall at most only 5 % to 10 % .	norel	3262..3473
A company such as Federal Paper Board may be overly discounted and looks " tempting " to him	cause	3474..3564
he says , though he is n't yet recommending the shares .	concession	3565..3619
Wall Street is n't avoiding everything connected with paper .	norel	3622..3681
Mr. Palmero recommends Temple-Inland , explaining that it is " virtually the sole major paper company not undergoing a major capacity expansion , " and thus should be able to lower long-term debt substantially next year .	instantiation	3682..3898
A Temple-Inland spokesman said the company expects record earnings in 1989 , and " we 're still pretty bullish " on 1990 .	norel	3901..4018
The analysts say their gloomy forecasts have a flip side .	norel	4021..4078
Some take a warm view of consumer-oriented paper companies , which buy pulp from the commodity producers and should benefit from the expected declines in pulp prices .	instantiation	4079..4244
Estimates on how much pulp prices will fall next year currently run between $ 150 and $ 250 a metric ton .	entrel	4245..4348
Analysts agree that the price drop should especially benefit the two big tissue makers , Scott Paper and Kimberly-Clark .	norel	4351..4470
A spokesman for Scott says that assuming the price of pulp continues to soften , " We should do well .	pragmatic cause	4471..4570
