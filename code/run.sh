#!/bin/bash
sigma=0.1
h_layers=1
mkdir -p outs
# One task
python disco.py --cnn-seed 3068836234 --iters 20 --sigma $sigma --train ../data/maintask/rstdtConstituent/train/ --test ../data/maintask/rstdtConstituent/dev/ --h_layers $h_layers --pred_layer 1 $h_layers --output outs/baseline.h_layers$h_layers.sigma$sigma


# Two tasks out
#python disco.py --cnn-seed 3068836234 --iters 10 --train ../data/maintask/rstdtConstituent/train/ ../data/auxtasks/document/factbankTense/ --pred_layer 1 1 --test ../data/maintask/rstdtConstituent/dev/ --h_layers $1 

# Two tasks in 
#h_layers=2
#python disco.py --cnn-seed 3068836234 --iters 10 --train ../data/maintask/rstdtConstituent/train/ ../data/auxtasks/document/factbankTense/ --pred_layer 2 1 --test ../data/maintask/rstdtConstituent/dev/ --h_layers $1
