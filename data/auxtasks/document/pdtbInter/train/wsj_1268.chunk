Silicon Graphics Inc., a fast-growing maker of computer workstations, said it landed two federal government contracts worth more than $100 million over the next five years.	O	9..181
One award is part of a Department of Defense contract to Loral Rolm Mil-Spec Computers and could be valued at more than $100 million over five years.	B-conjunction	184..333
The other involves the sale of about 35 of the company's high-end workstations to the National Institutes of Health.	B-entrel	334..450
The models, which cost about $75,000 each, will be used in research.	I-entrel	451..519
The awards are evidence that Silicon Graphics' approach to computer graphics is catching on with users of powerful desktop computers, analysts said.	O	522..670
"The company's on a roll," said Robert Herwick, an analyst at Hambrecht & Quist.	O	673..753
"No other {computer} vendor offers graphics performance that good for their price."	O	754..837
In the battle to supply desktop computers for researchers and design engineers, most of the attention is given to the biggest competitors: Sun Microsystems Inc., Hewlett-Packard Co. and Digital Equipment Corp., which make computers mainly aimed at a wide range of engineering and scientific needs.	O	840..1137
Silicon Graphics, on the other hand, has targeted a specific niche since its inception in 1982, which has been dubbed by some as "motion-picture computing."	O	1140..1296
This is a style of "visual" computing that provides three-dimensional, color models of everything from the inside of a house to the latest in women's fashion.	O	1297..1455
Though Silicon Graphics is much smaller than Digital, Hewlett and Sun, it has emerged in recent years as a feared adversary in this graphics portion of the workstation market.	B-conjunction	1458..1633
In addition, the company has made it tough on competitors by offering a stream of desktop computers at sharply lower prices.	B-instantiation	1636..1760
A year ago, Silicon Graphics introduced a model priced at $15,000 -- almost as cheap as mainstream workstations that don't offer special graphics features.	B-conjunction	1761..1916
Silicon Graphics also plans to unveil even less expensive machines in the near future.	I-conjunction	1919..2005
"It's pretty safe to assume we can bring the cost down of these systems by 30% to 40% a year," said Edward McCracken, the company's chief executive officer.	O	2006..2162
Silicon Graphics' strategy seems to be paying off.	B-cause	2165..2215
Revenue for its first quarter ended Sept. 30 was $86.4 million, a 95% increase over the year-ago period.	B-conjunction	2216..2320
Profit was $5.2 million, compared with $1 million for the year-ago quarter.	I-conjunction	2321..2396
