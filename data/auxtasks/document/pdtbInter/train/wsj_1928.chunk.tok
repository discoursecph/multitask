Sotheby 's Inc. , the world 's biggest auction house , is taking a huge Wall Street-style risk on the outcome of the sale of art from the estate of John T. Dorrance Jr. , the Campbell Soup Co. heir .	O	9..202
The Financial Services division has guaranteed the Dorrance family that it will receive a minimum of $ 100 million for the collection , regardless of what the bids for the art works total , people close to the transaction say .	B-entrel	205..428
The collection , which includes two early Picassos , a van Gogh , a Monet , other paintings , furniture and porcelains , went on sale last night in the first of six auctions .	I-entrel	429..597
What Sotheby 's is doing closely resembles an underwriting by an investment bank .	B-restatement	600..680
A corporation that wants to sell stock or bonds goes to a Wall Street firm , which purchases the securities outright , accepting the financial risk of finding buyers .	B-asynchronous	681..845
If the investment bank can sell the securities at a higher price than it paid the issuer , it makes a profit .	B-instantiation	846..954
At the initial sale last night , for example -- the sale featuring the Impressionists masters -- bids totaled $ 116 million .	I-instantiation	957..1079
That was slightly above Sotheby 's presale estimate of $ 111 million .	O	1080..1147
Normally , Sotheby 's would have earned 20 % of the total in commissions .	O	1148..1218
Instead , people familiar with the transaction said , the auction house opted to forgo that percentage in order to obtain the collection and in exchange for taking a bigger chunk of proceeds exceeding $ 100 million .	O	1219..1431
Art dealers say that while auction houses occasionally guarantee the seller of a highly desirable work of art a minimum price , a financial commitment of this size is unprecedented .	O	1434..1614
Diana D. Brooks , president of Sotheby 's North America division , vehemently denies it offered the Dorrance heirs a money-back guarantee , calling such reports " inaccurate . "	O	1617..1787
Buried in the glossy hardbound catalog for the sale , however , appears the statement , " Sotheby 's has an interest in the property in this catalog . "	O	1788..1933
Explains a Sotheby 's spokeswoman , the statement " means exactly what it says .	O	1936..2012
We have some level of financial interest " in the collection .	B-contrast	2013..2073
" We do n't disclose specifics . "	I-contrast	2074..2104
Frank Mirabello , a lawyer for the Dorrance estate with the Philadelphia law firm of Morgan , Lewis & Bockius , declines to comment on the financial arrangements .	O	2107..2266
Sotheby 's made the $ 100 million guarantee to keep the Dorrance collection away from its archrival , auction house Christie 's International PLC ; Christie 's has handled smaller sales for the Dorrance family over the years .	O	2269..2488
When Christie 's officials asked why the firm was n't picked to sell the Dorrance collection , representatives of the Dorrance family " told us it was a question of financial considerations , " said Michael Findlay , Christie 's head of impressionist and modern paintings .	O	2491..2755
Collectors who have made their money on Wall Street have become an increasingly important part of the art business and their money has helped fuel the art boom , but recently it appears Sotheby 's has been returning the compliment .	O	2758..2987
In November 1987 , Sotheby 's essentially offered a Wall Street-style " bridge loan " of about $ 27 million to Australian businessman Alan Bond to enable him to purchase Vincent van Gogh 's " Irises " for $ 53.9 million .	B-synchrony	2990..3201
It was the highest bid in history for a work of art .	B-concession	3202..3254
But two weeks ago , Sotheby 's said that it has the painting under lock and key because the loan had not been fully repaid .	I-concession	3255..3376
Sotheby 's is offering such deals because it 's an art sellers ' market , at least where the best works are concerned , says Ralph Lerner , an attorney and author of the book " Art Law . "	O	3379..3558
" There seems to be a lot of art for sale , but there 's more competition .	O	3559..3630
The competition gives the seller the ability to cut a better deal , " he says .	O	3631..3707
The Dorrance family will still receive a substantial portion of the auction proceeds above $ 100 million , people familiar with the transaction said .	O	3710..3857
But it 's likely that Sotheby 's will take a higher than usual commission , called an override , on the amount exceeding the guarantee .	O	3858..3989
Sotheby 's has been aggressively promoting the Dorrance sale .	B-instantiation	3992..4052
At a news conference last May announcing plans for the auction , Sotheby 's estimated its value in excess of $ 100 million .	B-contrast	4053..4173
More recently , Sotheby 's has predicted the collection will fetch $ 140 million .	B-entrel	4174..4252
That 's the highest estimate for a single collection in auction history .	I-entrel	4253..4324
The decision to put the entire collection on the block stunned many , since Mr. Dorrance had served as chairman of the Philadelphia Museum of Art , and it had been assumed many of the works would be donated to the institution .	O	4327..4551
At last night 's sale , 13 of 44 works that sold were purchased by Aska International Gallery , the art-acquisition unit of Aichi Financial , a Japanese conglomerate that owns 7.5 % of Christie 's .	O	4554..4745
Meanwhile , Sotheby 's guarantee is raising eyebrows in the art world .	O	4748..4816
" The consumer has to throw out the idea that the auction house is a disinterested middleman , " says New York art dealer David Tunick .	O	4817..4949
While he adds that he has no problem with auction houses who sell works in which they have a financial interest , " It ought not to be hidden in some small print . "	O	4950..5111
In such situations , he says , the house " is going to put the best light on things . "	O	5114..5196
For example , an auction house 's comments on the condition of a work of art that is up for sale should be looked at with " very open eyes , " he says .	O	5197..5343
" There 's more and more of this cash-up-front going on at every level , " says Bruce Miller , president of Art Funding Corp. , an art lender .	O	5346..5482
Dealers and auction houses " know if they do n't lay out a half a million for this , another one will ; it 's that competitive . "	O	5483..5606
In January , two small New York galleries , the Coe Kerr Gallery and Beadleston Fine Arts , snatched a major art collection owned by the Askin family away from rival auction-house bidders with an up-front payment of about $ 25 million .	O	5609..5840
A Christie 's spokeswoman said that while the auction house sometimes waives its seller 's commission to attract art works -- it still gets a commission from the buyer -- Christie 's wo n't offer financial guarantees because " Christie 's believes its primary role is as an auction house , and therefore as an agent { for buyer and seller } , not as a bank .	O	5843..6190
