Beauty Takes Backseat To Safety on Bridges	O	9..53
EVERYONE AGREES that most of the nation 's old bridges need to be repaired or replaced .	B-contrast	56..142
But there 's disagreement over how to do it .	I-contrast	143..186
Highway officials insist the ornamental railings on older bridges are n't strong enough to prevent vehicles from crashing through .	B-contrast	189..318
But other people do n't want to lose the bridges ' beautiful , sometimes historic , features .	I-contrast	319..408
" The primary purpose of a railing is to contain a vehicle and not to provide a scenic view , " says Jack White , a planner with the Indiana Highway Department .	B-cause	411..567
He and others prefer to install railings such as the " type F safety shape , " a four-foot-high concrete slab with no openings .	I-cause	568..692
In Richmond , Ind. , the type F railing is being used to replace arched openings on the G Street Bridge .	B-contrast	695..797
Garret Boone , who teaches art at Earlham College , calls the new structure " just an ugly bridge " and one that blocks the view of a new park below .	I-contrast	798..943
In Hartford , Conn. , the Charter Oak Bridge will soon be replaced , the cast-iron medallions from its railings relegated to a park .	O	946..1075
Compromises are possible .	B-instantiation	1078..1103
Citizens in Peninsula , Ohio , upset over changes to a bridge , negotiated a deal : The bottom half of the railing will be type F , while the top half will have the old bridge 's floral pattern .	B-conjunction	1104..1292
Similarly , highway engineers agreed to keep the old railings on the Key Bridge in Washington , D.C. , as long as they could install a crash barrier between the sidewalk and the road .	I-conjunction	1295..1475
Tray Bon ?	O	1478..1487
Drink Carrier Competes With Cartons	O	1488..1525
PORTING POTABLES just got easier , or so claims Scypher Corp. , the maker of the Cup-Tote .	O	1528..1616
The Chicago company 's beverage carrier , meant to replace cardboard trays at concession stands and fast-food outlets , resembles the plastic loops used on six-packs of beer , only the loops hang from a web of strings .	B-entrel	1619..1833
The new carrier can tote as many as four cups at once .	I-entrel	1834..1888
Inventor Claire Marvin says his design virtually eliminates spilling .	B-restatement	1891..1960
Lids are n't even needed .	I-restatement	1961..1985
He also claims the carrier costs less and takes up less space than most paper carriers .	B-entrel	1986..2073
A few fast-food outlets are giving it a try .	I-entrel	2074..2118
The company acknowledges some problems .	B-instantiation	2121..2160
A driver has to find something to hang the carrier on , so the company supplies a window hook .	B-conjunction	2161..2254
While it breaks down in prolonged sunlight , it is n't recyclable .	I-conjunction	2255..2319
And unlike some trays , there 's no place for food .	O	2320..2369
Spirit of Perestroika Touches Design World	O	2372..2416
AN EXCHANGE of U.S. and Soviet designers promises change on both sides .	O	2419..2490
An exhibition of American design and architecture opened in September in Moscow and will travel to eight other Soviet cities .	B-entrel	2493..2618
The show runs the gamut , from a blender to chairs to a model of the Citicorp building .	I-entrel	2619..2705
The event continues into next year and includes an exchange program to swap design teachers at Carnegie-Mellon and Leningrad 's Mutchin Institute .	O	2708..2853
Dan Droz , leader of the Carnegie-Mellon group , sees benefits all around .	O	2856..2928
The Soviets , who normally have few clients other than the state , will get " exposure to a market system , " he says .	O	2929..3042
Americans will learn more about making products for the Soviets .	O	3043..3107
Mr. Droz says the Soviets could even help U.S. designers renew their sense of purpose .	O	3110..3196
" In Moscow , they kept asking us things like , ' Why do you make 15 different corkscrews , when all you need is one good one ? ' " he says .	O	3197..3330
" They got us thinking maybe we should be helping U.S. companies improve existing products rather than always developing new ones . "	O	3331..3461
Seed for Jail Solution Fails to Take Root	O	3464..3507
IT 'S A TWO BIRDS with one stone deal : Eggers Group architects propose using grain elevators to house prisoners .	O	3510..3621
It would ease jail overcrowding while preserving historic structures , the company says .	O	3622..3709
But New York state , which is seeking solutions to its prison cell shortage , says " no . "	O	3712..3798
Grain elevators built in the 1920s and '30s have six-inch concrete walls and a tubular shape that would easily contain semicircular cells with a control point in the middle , the New York firm says .	O	3801..3998
Many are far enough from residential areas to pass public muster , yet close enough to permit family visits .	O	3999..4106
Besides , Eggers says , grain elevators are worth preserving for aesthetic reasons -- one famed architect compared them to the pyramids of Egypt .	O	4109..4252
A number of cities -- including Minneapolis , Philadelphia and Houston -- have vacant grain elevators , Eggers says .	O	4255..4369
A medium-sized one in Brooklyn , it says , could be altered to house up to 1,000 inmates at a lower cost than building a new prison in upstate New York .	O	4370..4520
A spokesman for the state , however , calls the idea " not effective or cost efficient .	O	4521..4605
