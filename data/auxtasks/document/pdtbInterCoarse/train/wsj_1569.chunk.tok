YOU WENT to college and thought you got an education .	B-comparison	9..62
Now you discover that you never learned the most important lesson : How to send your kids to college .	I-comparison	63..163
True , when you went to college , there was n't that much to learn .	B-expansion	166..230
Stick some money in an interest-bearing account and watch it grow .	B-comparison	231..297
Now , investment salesmen say it 's time to take some risks if you want the kind of returns that will buy your toddler a ticket to Prestige U. in 18 years .	I-comparison	298..451
In short , throw away the passbook and go for the glory .	O	454..509
The reason is cost .	B-entrel	512..531
Nothing in the annals of tuition readied parents for the 1980s .	I-entrel	532..595
Tuitions at private colleges rose 154 % in the 10 years ended in June of this year ; that 's twice the 77 % increase in consumer prices for the same period .	O	596..748
A year at Harvard now goes for $ 19,395 .	B-comparison	751..790
By 2007 , when this year 's newborns hit campus , a four-year Ivy League sheepskin will cost $ 300,000 , give or take a few pizzas-with-everything at exam time .	B-expansion	791..946
Stanford , MIT and other utmosts will cost no less .	B-contingency	947..997
So what 's a parent to do ?	I-contingency	1000..1025
Some investment advisers are suggesting , in effect , a bet on a start-up investment pool -- maybe even on margin .	B-comparison	1026..1138
Others prefer deep-discount zero-coupon bonds .	B-expansion	1139..1185
Still others say , Why not take a chance on a high-octane growth fund ?	I-expansion	1186..1255
" You 're not going to make it in a 5 % bank account , " says James Riepe , director of mutual funds at T. Rowe Price .	O	1258..1370
To get the necessary growth , adds Murray Ruffel , a marketing official at the Financial Programs mutual-fund group , " you need to go to the stock market . "	O	1371..1523
In other words , a little volatility never hurt .	O	1526..1573
It never hurt anyone , that is , unless the growth funds do n't grow when you need them to .	O	1576..1664
Or the zero-coupon bonds turn out not to have been discounted deeply enough to pay your kid 's tuition .	B-expansion	1665..1767
That 's the dilemma for today 's parent .	B-entrel	1768..1806
Although many experts are advising risk , no one has a good answer for you if the risk does n't pay off .	I-entrel	1807..1909
Help may be on the way .	B-contingency	1912..1935
The antitrust division of the Justice Department is investigating the oddly similar tuition charges and increases among the top schools .	B-contingency	1936..2072
Fear of the price police could help cool things off in the 1990s .	B-expansion	2073..2138
Fear of the price police could help cool things off in the 1990s .	B-expansion	2073..2138
And then there 's always State U .	B-comparison	2141..2173
But parents ' craving for a top-rated education for their children is growing like their taste for fancy wheels and vintage wine .	I-comparison	2174..2302
Belatedly aware of public concern , lawmakers and financial middlemen are working overtime to create and sell college savings and investment schemes .	O	2305..2453
Their message , explicit or implicit , is that a good college will cost so much by whenever you want it that the tried and true wo n't do anymore .	O	2456..2599
Forget about Treasury bills or a money-market fund .	O	2600..2651
The latest wave of marketing is instructive .	O	2654..2698
Several outfits -- including the Financial Programs , Franklin , and T. Rowe Price mutual-fund groups and the Edward D. Jones brokerage house -- are advertising " college planner " tables and charts that tell you how much you need to put aside regularly .	B-entrel	2699..2949
The calculations generally rely on an after-tax rate of return of 8 % annually -- a rate historically obtainable by the individual in only one place , the stock market .	I-entrel	2950..3116
Most of the mailers are free , but Denver-based Financial Programs sells , for $ 15 , a version customized to the age of the child and the college of choice .	B-entrel	3119..3272
The figures are shocking .	B-expansion	3273..3298
To build a nest egg that would pay for Stanford when a current first-grader reaches college age , parents would need to set aside $ 773.94 a month -- for 12 years .	B-comparison	3299..3460
They can cut this to $ 691.09 a month if the investing keeps up through college .	B-expansion	3461..3540
And they can further reduce the monthly amount if they start saving earlier -- when mother and child come home from the hospital .	I-expansion	3541..3670
Plugging a cheaper college into the formulas still does n't generate an installment most people can live with .	B-expansion	3673..3782
Using a recent average private-school cost of about $ 12,500 a year , T. Rowe Price 's planner prescribes $ 450 monthly if the plan begins when the child is six .	B-expansion	3783..3940
Since the formula assumes an 8 % before-tax return in a mutual fund , there would also be $ 16,500 in taxes to pay over the 12 years .	I-expansion	3941..4071
Not everyone is so pessimistic .	B-expansion	4074..4105
" People are basically peddling a lot of fear , " says Arthur Hauptman , a consultant to the American Council on Education in Washington .	I-expansion	4106..4239
He takes issue with projections that do n't factor in students ' own contribution , which reduces most parents ' burden substantially .	O	4240..4370
Still , he says , " it 's no bad thing " if all the marketing prods people into putting aside a little more .	O	4373..4476
" The situation you want to avoid is having somebody not save anything and hope they 'll be able to do it out of current income , " he says .	O	4477..4613
" That 's crazy . "	O	4614..4629
His advice : Do n't panic .	O	4632..4656
Parents , he says , should aim at whatever regular investment sum they can afford .	O	4657..4737
Half the amount that the investment tables suggest might be a good goal , he adds .	O	4738..4819
That way , parents will reduce borrowings and outlays from current income when the time comes to pay tuition .	O	4820..4928
Mr. Hauptman reckons that the best investment choice is mutual funds because they are managed and over time have nearly kept up with the broad stock averages .	O	4931..5089
He favors either an all-stock fund or a balanced fund that mixes both stocks and bonds .	O	5090..5177
In their anxiety , however , parents and other student benefactors are flocking to new schemes .	O	5180..5273
They have laid out about $ 1 billion for so-called baccalaureate zero-coupon municipal bonds -- so far offered by Connecticut , Illinois , Virginia and eight other states .	O	5274..5442
And they have bought about $ 500 million in prepaid-tuition plans , offered in Michigan , Florida and Wyoming .	B-entrel	5443..5550
The prepaid plans take payment today -- usually at current tuitions or at a slight discount -- for a promise that tuition will be covered tomorrow .	I-entrel	5551..5698
The baccalaureate bonds -- tax-free , offered in small denominations and usually containing a provision that they wo n't be called before maturity -- seem to be tailor-made for college savers .	O	5701..5891
Like other zeros , they pay all their interest at maturity , meaning that buyers can time things so that their bonds pay off just when Junior graduates from high school .	B-expansion	5892..6059
Their compounding effect is also alluring .	B-expansion	6060..6102
In June , Virginia sold bonds for $ 268.98 that will pay $ 1,000 in 2009 .	I-expansion	6103..6173
But Richard Anderson , head of the Forum for College Financing Alternatives , at Columbia University , a research group partly financed by the federal government , says zeros are particularly ill-suited .	B-contingency	6176..6375
Their price falls further than that of other bonds when inflation and interest rates kick up .	B-contingency	6376..6469
That wo n't matter if they are held to maturity , but if , for any reason , the parents need to sell them before then , there could be a severe loss of principal .	I-contingency	6470..6627
Had zeros been available in 1972 and had parents bought a face amount equal to four years ' tuition at the time , aiming for their children 's 1988 enrollment , they would have been left with only enough to pay for two years , Mr. Anderson figures .	O	6630..6873
Most other bonds , however , would probably not have fared much better .	O	6874..6943
The prepaid plans may be a good bet , provided the guarantee of future tuition is secure .	O	6946..7034
Issuing states generally limit the guarantees to in-state institutions , however , and buyers get refunds without much interest if the children do n't attend the specified schools .	B-temporal	7035..7212
Two private groups are seeking Securities and Exchange Commission approval for plans that could be more broadly transferable .	B-entrel	7213..7338
Mr. Anderson wants the prestige colleges to sponsor such a plan .	I-entrel	7339..7403
The issue here may be the soundness of the guarantee .	B-contingency	7406..7459
Prepayments , much like mutual-fund purchases , are pooled for investment .	B-expansion	7460..7532
Sponsors are naturally counting on their ability to keep ahead of tuition inflation with investment returns .	I-expansion	7533..7641
But buyers are essentially betting on a start-up investment fund with no track record -- and some have been encouraged to borrow to do so .	O	7642..7780
One problem is that the Internal Revenue Service has decided that the investment earnings and gains of the sponsors ' funds are taxable .	B-comparison	7783..7918
The colleges , as educational institutions , had hoped that would n't be the case .	I-comparison	7919..7998
Based on historical rates of return , Mr. Anderson reckons a 100 % stock portfolio , indexed to the market , would have kept up with tuition and taxes in the 20th century .	O	8001..8168
But sponsors might not pick the stocks that will match the market .	B-expansion	8169..8235
And they 're leaning more toward fixed income , whose returns after tax have trailed tuition increases .	I-expansion	8236..8337
" I 'm not sure they 're going to make it work , " says Mr. Anderson .	O	8338..8402
What happens if the sponsors do n't have the cash to pay the tuitions ?	O	8405..8474
Florida and Wyoming have backed up their guarantees with the full faith and credit of the state governments , meaning that taxpayers will pick up any slack .	B-comparison	8475..8630
Not so Michigan .	B-expansion	8631..8647
Its plan is set up as an independent agency .	B-comparison	8648..8692
The state says there 's no worry -- investment returns , combined with fees and the gains from unused plans , will provide all the cash it needs .	I-comparison	8693..8835
Mr. Putka covers education from The Wall Street Journal 's Boston bureau .	O	8838..8910
If you start saving for your child 's eduction on Jan. 1 , 1990 , here 's the monthly sum you will need to invest to pay for four years at Yale , Notre Dame and University of Minnesota .	B-entrel	8913..9093
Figures assume a 7 % annual rise in tuition , fees , room and board and an 8 % annual investment return .	I-entrel	9094..9194
Note : These figures are only for mandatory charges and do n't include books , transportation etc .	O	9197..9292
* For in-state students	O	9295..9317
Source : PaineWebber Inc .	O	9320..9344
