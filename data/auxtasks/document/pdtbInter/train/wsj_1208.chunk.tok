The stock market went on a dizzying ride as UAL , parent of United Airlines , once again led shares into a breathtaking decline and then an afternoon comeback .	O	9..166
At the end of it all , the Dow Jones Industrial Average closed down 3.69 to 2659.22 .	B-contrast	169..252
At one point yesterday morning , the Dow was down 80.53 points .	B-entrel	253..315
New York Stock Exchange volume was 237,960,000 shares .	B-synchrony	316..370
Declining issues swamped advancers , 1,222 to 382 .	I-synchrony	371..420
Yesterday 's sell-off and rebound was a powerful reminder that 11 days after the 190-point plunge on Friday the 13th , the stock market still has a bad case of nerves .	B-conjunction	423..588
Takeover stock speculation and futures-related program trading drove the industrial average through wide ranges .	B-conjunction	589..701
And there is more volatility to come .	I-conjunction	702..739
" October 13th left us with a cut and exposed nerve , " said Jack Solomon , technical analyst for Bear Stearns .	O	742..849
" People are fearful and sensitive .	B-cause	850..884
Everybody 's finger is one inch closer to the button .	B-conjunction	885..937
I have never had as many calls as I had this morning .	B-entrel	938..991
Volatility is here to stay . "	I-entrel	992..1020
The Dow Jones Industrial Average plunged about 80 points in slightly more than one hour after the opening bell .	O	1023..1134
For many , it began to look like a replay of Oct. 13 .	B-cause	1137..1189
As stocks and stock-index futures fell , a trading limit was hit in the S&P 500 stock futures pit .	B-cause	1190..1287
Under a post-1987 crash reform , the Chicago Mercantile Exchange would n't permit the December S&P futures to fall further than 12 points for a half hour .	B-cause	1288..1440
That caused a brief period of panic selling of stocks on the Big Board .	B-contrast	1441..1512
But at a critical moment , stock-index arbitrage traders showed their power and control .	B-synchrony	1513..1600
They scooped up hundreds of S&P futures when the market needed it most .	I-synchrony	1601..1672
At about 10:40 a.m. EDT , several big buy orders hit the S&P pit simultaneously , lifting the futures up out of the trading limit and eventually into ranges that caused computer-driven program buying of stocks .	O	1675..1883
" It is very clear that those buy orders came from people who wanted their franchise protected , " said one Chicago-based futures trader .	O	1886..2020
" These guys wanted to do something to show how powerful they are . "	O	2021..2087
Traders said Goldman Sachs , Shearson Lehman Hutton and Salomon Brothers were the main force behind the futures buying at the pivotal moment .	B-entrel	2090..2230
Shearson Lehman Hutton declined to comment .	B-conjunction	2231..2274
Officials at Goldman Sachs and Salomon Brothers were unavailable for comment .	I-conjunction	2275..2352
As in the Oct. 13 massacre , yesterday morning 's drop was triggered by bad news for speculators in UAL .	B-instantiation	2355..2457
A UAL statement after the market closed Monday indicated that the airline 's board wanted to keep the company independent , effectively crushing hopes of an immediate buy-out .	B-asynchronous	2458..2631
Five minutes before the Big Board opened , a preliminary price was flashed for UAL -- somewhere between 135 and 155 , a loss of as much as $ 43 a share from Monday 's close .	B-asynchronous	2632..2801
UAL finally opened for trading at 10:08 a.m. at 150 , down $ 28 .	I-asynchronous	2804..2866
Floor traders said there was a huge crowd around the Big Board specialist 's post where UAL trades .	O	2867..2965
" There was a seething mass of people , " said one floor trader .	O	2966..3027
" Then there was a big liquidation of stock " across the board , he added .	O	3028..3099
Takeover speculators -- who have already taken a record loss estimated at more than $ 700 million on UAL -- started selling other stocks as well as S&P futures in an attempt to hedge against a further UAL blood bath .	B-conjunction	3102..3317
Shortly after the UAL opening , program traders started selling stocks in the Major Market Index and S&P 500 index .	B-entrel	3318..3432
The 20-stock MMI mimics the Dow Jones Industrial Average .	I-entrel	3433..3490
By 10:30 a.m. the Dow was down 62.70 .	B-conjunction	3493..3530
All 20-stocks in the MMI except Exxon , General Motors and Sears were down $ 1 to $ 2 .	I-conjunction	3531..3614
At 10:33 , when the S&P 500 December futures contract crunched to a 12-point loss under the force of sell programs , S&P futures trading was halted and program trades on the Big Board were routed into a special computer that scans for order imbalances .	B-cause	3617..3867
Under the rules adopted by the Chicago Mercantile Exchange , the futures contract can not drop below the limit , but buyers can purchase futures .	B-synchrony	3868..4010
At this point , the Dow industrials were down 75.41 points and falling .	I-synchrony	4011..4081
The trading halt in the S&P 500 futures exacerbated selling and confusion , many traders maintain .	O	4084..4181
But as the fright began to spread through the S&P pit , the big brokerage firms came in and bought futures aggressively .	O	4182..4301
" It was whooosh ! " said one futures trader .	O	4304..4346
In five minutes , the Dow industrials climbed almost 30 points .	B-asynchronous	4347..4409
The big futures buying triggered stock-index buy programs that eventually trimmed the Dow 's loss to 31 points by 11 a.m .	I-asynchronous	4410..4530
Traders said the futures buying was finely calculated by program traders .	O	4533..4606
These firms sold stock into the big morning decline , but seeing the velocity of the market 's drop , held back on their offsetting purchases of futures until the S&P futures hit the trading limit .	O	4607..4801
Then they completed the other side of the trade by buying futures , which abruptly halted the stock market 's decline as traders began to buy stocks .	O	4802..4949
From then on , the Dow industrials held at a loss of 40 to 50 points .	O	4952..5020
Then , in late-afternoon trading , hundred-thousand-share buy orders for UAL hit the market , including a 200,000-share order through Bear Stearns that seemed to spark UAL 's late price surge .	B-synchrony	5021..5209
Almost simultaneously , PaineWebber began a very visible buy program for dozens of stocks .	B-cause	5210..5299
The combined buying rallied the Dow into a small gain , before closing at a slight loss .	I-cause	5300..5387
Some institutional traders loved the wild ride .	B-instantiation	5390..5437
" This is fun , " asserted Susan Del Signore , head equity trader at Travelers Investment Management Co .	I-instantiation	5438..5538
She said she used the market 's wild swings to buy shares cheaply on the sell-off .	O	5539..5620
On the comeback , Ms. Del Signore unloaded shares she has been aiming to get rid of .	O	5621..5704
But traders who risk money handling big blocks of stock were shaken .	B-instantiation	5707..5775
" This market is eating away my youth , " said Chung Lew , head equity trader at Kleinwort Benson North America Inc .	B-entrel	5776..5888
" Credibility sounds intangible .	I-entrel	5889..5920
But I think we are losing credibility because when the market does this , it does n't present itself as a rational investment .	O	5921..6045
But if you overlook all this , it is a beautiful market for investment still . "	O	6046..6123
Traders attributed rallies in a number of stocks to a Japanese buy program that PaineWebber carried out as part of a shift in portfolio strategy , according to Dow Jones Professional Investor Report .	O	6126..6324
Dow Jones climbed 5 3/4 to 41 on very heavy volume of 786,100 shares .	O	6327..6396
Analysts said a big Japanese buy order was behind the rise .	O	6397..6456
A Dow Jones spokesman said there were no corporate developments that would account for the activity .	B-entrel	6457..6557
Other issues said to be included in the buy program were Procter & Gamble , which rose 2 7/8 to 133 1/2 ; Atlantic Richfield , which gained 2 to 103 3/4 , and Rockwell International , which jumped 2 3/4 to 27 1/8 .	B-entrel	6558..6766
PaineWebber declined to comment .	I-entrel	6767..6799
UAL finished at 170 , off 8 3/8 .	B-conjunction	6802..6833
Other airline stocks fell in response to the UAL board 's decision to remain independent for now , including USAir Group , which separately reported a third-quarter loss of $ 1.86 a share compared with a year-ago profit .	B-restatement	6834..7050
USAir fell 2 1/2 to 40 .	I-restatement	7051..7074
AMR , the parent of American Airlines , fell 1 3/4 to 68 7/8 on 2.3 million shares ; Delta Air Lines lost 1 1/2 to 66 , Southwest Airlines slid 3/4 to 24 1/4 and Midway Airlines dropped 1/4 to 14 7/8 .	B-conjunction	7077..7273
Texas Air , which owns Continental and Eastern airlines , lost 3/8 to 13 1/8 on the American Stock Exchange .	I-conjunction	7274..7380
Metals stocks also were especially weak , as concerns about the earnings outlook for cyclical companies weighed on the group .	B-restatement	7383..7507
Aluminum Co. of America dropped 1 1/2 to 70 1/4 , Phelps Dodge fell 4 to 59 7/8 , Asarco lost 1 3/8 to 31 3/4 , Reynolds Metals slid 1 3/8 to 50 3/8 , Amax dropped 1 1/8 to 21 5/8 and Cyprus Minerals skidded 2 to 26 3/4 .	B-contrast	7508..7724
Alcan Aluminium was an exception , as it gained 1 3/8 to 23 on two million shares .	I-contrast	7725..7806
Goodyear Tire & Rubber tumbled 2 7/8 to 43 7/8 .	B-cause	7809..7856
Its third-quarter earnings were higher than a year ago , but fell short of expectations .	B-conjunction	7857..7944
Other stocks in the Dow industrials that failed to benefit from the market 's rebound included United Technologies , which dropped 1 to 53 5/8 , and Bethle hem Steel , which fell 1 to 16 7/8 .	I-conjunction	7945..8132
BankAmerica dropped 1 1/4 to 29 1/2 on 2.3 million shares amid rumors that the earthquake last week in the San Francisco area had caused structural damage to its headquarters building .	B-contrast	8135..8319
The company denied the rumors and noted that it does n't own the building .	I-contrast	8320..8393
Stocks of California-based thrifts also were hard hit .	B-instantiation	8396..8450
Great Western Financial lost 1 1/8 to 20 1/2 on 1.6 million shares , Golden West Financial dropped 1 1/4 to 28 1/2 and H.F. Ahmanson dipped 5/8 to 21 1/4 .	B-conjunction	8451..8604
HomeFed plunged 3 5/8 to 38 1/2 ; its third-quarter earnings were down from a year ago .	I-conjunction	8605..8691
Golden Valley Microwave Foods skidded 3 5/8 to 31 3/4 after warning that its fourth-quarter results could be hurt by " some fairly large international marketing expenses . "	O	8694..8864
Dividend-related trading swelled volume in two issues : Security Pacific , which fell 7/8 to 44 1/2 and led the Big Board 's most actives list on composite volume of 14.8 million shares , and Nipsco Industries , which lost 3/8 to 17 3/8 on 4.4 million shares .	B-conjunction	8867..9121
Both stocks have dividend yields of about 5 % and will go ex-dividend Wednesday .	I-conjunction	9122..9201
Kellogg surged 4 1/4 to 75 .	B-synchrony	9204..9231
Donaldson , Lufkin & Jenrette placed the stock on its list of recommended issues .	B-entrel	9232..9312
The company noted that its third-quarter results should be released later this week or early next week .	I-entrel	9313..9416
Vista Chemical rose 1 3/8 to 38 5/8 after Bear Stearns added the stock to the firm 's buy list , citing recent price weakness .	O	9419..9543
Georgia Gulf , another producer of commodity chemicals , advanced 2 to 49 1/2 ; Dallas investor Harold Simmons , who holds about 10 % of its shares , said he has n't raised his stake .	O	9546..9722
Norfolk Southern went up 1 1/8 to 37 7/8 .	B-cause	9725..9766
The company 's board approved the repurchase of up to 45 million common shares , or about 26 % of its shares outstanding , through the end of 1992 .	I-cause	9767..9910
Airborne Freight climbed 1 1/8 to 38 1/2 .	B-cause	9913..9954
Its third-quarter earnings more than doubled from a year earlier and exceeded analysts ' expectations .	I-cause	9955..10056
John Harland , which will replace American Medical International on the S&P 500 following Wednesday 's close , gained 5/8 to 24 1/8 .	O	10059..10188
The Amex Market Value Index fell 3.10 to 376.36 .	B-entrel	10191..10239
Volume totaled 14,560,000 shares .	I-entrel	10240..10273
