Newspaper publishers are reporting mixed third-quarter results , aided by favorable newsprint prices and hampered by flat or declining advertising linage , especially in the Northeast .	O	9..191
Adding to unsteadiness in the industry , seasonal retail ad spending patterns in newspapers have been upset by shifts in ownership and general hardships within the retail industry .	O	194..373
In New York , the Bonwit Teller and B. Altman & Co. department stores have filed for protection from creditors under Chapter 11 of the federal Bankruptcy Code , while the R.H. Macy & Co. , Bloomingdale 's and Saks Fifth Avenue department-store chains are for sale .	O	374..634
Many papers throughout the country are also faced with a slowdown in classified-ad spending , a booming category for newspapers in recent years .	O	637..780
Until recently , industry analysts believed decreases in retail ad spending had bottomed out and would in fact increase in this year 's third and fourth quarters .	B-comparison	783..943
All bets are off , analysts say , because of the shifting ownership of the retail chains .	I-comparison	944..1031
" Improved paper prices will help offset weakness in linage , but the retailers ' problems have affected the amount of ad linage they usually run , " said Edward J. Atorino , industry analyst for Salomon Brothers Inc .	O	1034..1245
" Retailers are just in disarray . "	O	1246..1279
For instance , Gannett Co. posted an 11 % gain in net income , as total ad pages dropped at USA Today , but advertising revenue rose because of a higher circulation rate base and increased rates .	O	1282..1473
Gannett 's 83 daily and 35 non-daily newspapers reported a 3 % increase in advertising and circulation revenue .	O	1474..1583
Total advertising linage was " modestly " lower as classified-ad volume increased , while there was " softer demand " for retail and national ad linage , said John Curley , Gannett 's chief executive officer .	O	1584..1784
At USA Today , ad pages totaled 785 for the quarter , down 9.2 % from the 1988 period , which was helped by increased ad spending from the Summer Olympics .	O	1787..1938
While USA Today 's total paid ad pages for the year to date totaled 2,735 , a decrease of 4 % from last year , the paper 's ad revenue increased 8 % in the quarter and 13 % in the nine months .	B-expansion	1939..2124
In the nine months , Gannett 's net rose 9.5 % to $ 270 million , or $ 1.68 a share , from $ 247 million , or $ 1.52 a share .	B-expansion	2125..2240
Revenue gained 6 % to $ 2.55 billion from $ 2.4 billion .	I-expansion	2241..2294
At Dow Jones & Co. , third-quarter net income fell 9.9 % from the year-earlier period .	B-expansion	2297..2381
Net fell to $ 28.8 million , or 29 cents a share , from $ 32 million , or 33 cents a share .	B-comparison	2382..2468
The year-earlier period included a one-time gain of $ 3.5 million , or four cents a share .	B-entrel	2469..2557
Revenue gained 5.3 % to $ 404.1 million from $ 383.8 million .	I-entrel	2558..2616
The drop in profit reflected , in part , continued softness in financial advertising at The Wall Street Journal and Barron 's magazine .	B-expansion	2619..2751
Ad linage at the Journal fell 6.1 % in the third quarter .	I-expansion	2752..2808
Affiliated Publications Inc. reversed a year-earlier third quarter net loss .	B-expansion	2811..2887
The publisher of the Boston Globe reported net of $ 8.5 million , or 12 cents a share , compared with a loss of $ 26.5 million , or 38 cents a share , for the third quarter in 1988 .	I-expansion	2888..3063
William O. Taylor , the parent 's chairman and chief executive officer , said earnings continued to be hurt by softness in ad volume at the Boston newspaper .	O	3066..3220
Third-quarter profit estimates for several companies are being strongly affected by the price of newsprint , which in the last two years has had several price increases .	O	3223..3391
After a supply crunch caused prices to rise 14 % since 1986 to $ 650 a metric ton , analysts are encouraged , because they do n't expect a price increase for the rest of this year .	O	3392..3567
Companies with daily newspapers in the Northeast will need the stable newsprint prices to ease damage from weak ad linage .	O	3570..3692
Mr. Atorino at Salomon Brothers said he estimates that Times Mirror Co. 's earnings were down for the third quarter , because of soft advertising levels at its Long Island Newsday and Hartford Courant newspapers .	O	3693..3904
Trouble on the East Coast was likely offset by improved ad linage at the Los Angeles Times , which this week also unveiled a redesign .	O	3905..4038
New York Times Co. is expected to report lower earnings for the third quarter because of continued weak advertising levels at its flagship New York Times and deep discounting of newsprint at its affiliate , Forest Products Group .	O	4041..4269
" Times Co. 's regional daily newspapers are holding up well , but there is little sign that things will improve in the New York market , " said Alan Kassan , an analyst with Shearson Lehman Hutton .	O	4272..4465
Washington Post Co. is expected to report improved earnings , largely because of increased cable revenue and publishing revenue helped by an improved retail market in the Washington area .	B-expansion	4468..4654
According to analysts , profits were also helped by successful cost-cutting measures at Newsweek .	B-entrel	4655..4751
The news-weekly has faced heightened competition from rival Time magazine and a relatively flat magazine advertising market .	I-entrel	4752..4876
Knight-Ridder Inc. is faced with continued uncertainty over the pending joint operating agreement between its Detroit Free Press and Gannett 's Detroit News , and has told analysts that earnings were down in the third quarter .	O	4879..5103
However , analysts point to positive advertising spending at several of its major daily newspapers , such as the Miami Herald and San Jose Mercury News .	O	5104..5254
" The Miami market is coming back strong after a tough couple of years " when Knight-Ridder " was starting up a Hispanic edition and circulation was falling , " said Bruce Thorp , an analyst for Provident National Bank .	O	5257..5470
