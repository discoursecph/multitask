In the aftermath of the stock market's gut-wrenching 190-point drop on Oct. 13, Kidder, Peabody & Co. 's 1,400 stockbrokers across the country began a telephone and letter-writing campaign aimed at quashing the country's second-largest program trader.	O	9..260
The target of their wrath?	O	263..289
Their own employer, Kidder Peabody.	O	290..325
Since October's minicrash, Wall Street has been shaken by an explosion of resentment against program trading, the computer-driven, lightning-fast trades of huge baskets of stocks and futures that can send stock prices reeling in minutes.	B-contrast	328..565
But the heated fight over program trading is about much more than a volatile stock market.	B-restatement	568..658
The real battle is over who will control that market and reap its huge rewards.	B-cause	659..738
Program trading itself, according to many academics who have studied it, is merely caught in the middle of this battle, unfairly labeled as the evil driving force of the marketplace.	I-cause	739..921
The evidence indicates that program trading didn't, in fact, cause the market's sharp fall on Oct. 13, though it may have exacerbated it.	O	922..1059
On one side of this power struggle stand the forces in ascendency on Wall Street -- the New Guard -- consisting of high-tech computer wizards at the major brokerage firms, their pension fund clients with immense pools of money, and the traders at the fast-growing Chicago futures exchanges.	B-entrel	1062..1352
These are the main proponents of program trading.	I-entrel	1353..1402
Defending their ramparts are Wall Street's Old Guard -- the traditional, stock-picking money managers, tens of thousands of stock brokers, the New York Stock Exchange's listed companies and the clannish floor traders, known as specialists, who make markets in their stocks.	O	1405..1678
So far, Wall Street's Old Guard seems to be winning the program-trading battle, successfully mobilizing public and congressional opinion to bludgeon their tormentors.	O	1681..1847
The Chicago Mercantile Exchange, a major futures marketplace, yesterday announced the addition of another layer of trading halts designed to slow program traders during a rapidly falling stock market, and the Big Board is expected today to approve some additional restrictions on program trading.	O	1850..2146
Stung by charges that their greed is turning the stock market into a gigantic crapshoot, almost all the big investment banking houses have abandoned index arbitrage, a common form of program trading, for their own accounts in the past few days.	B-conjunction	2149..2393
A few, such as giant Merrill Lynch & Co., now refuse even to do index arbitrage trades for clients.	I-conjunction	2394..2493
The Old Guard's assault on program trading and its practitioners has been fierce and broad-based, in part because some Old Guard members feel their very livelihood is at stake.	O	2496..2672
Some, such as traditional money manager Neuberger & Berman, have taken out national newspaper advertisements demanding that market regulators "stop the numbers racket on Wall Street."	O	2673..2856
Big Board stock specialists, in a bold palace revolt, began shortly after Oct. 13 to telephone the corporate executives of the companies whose stock is listed on the Big Board to have them pressure the exchange to ban program trading.	O	2859..3093
Charles Wohlstetter, the chairman of Contel Corp. who is rallying other CEOs to the anti-program trading cause, says he has received "countless" letters offering support.	O	3096..3266
"They said universally, without a single exception: Don't even compromise.	O	3267..3341
Kill it," he says.	O	3342..3360
Wall Street's New Guard isn't likely to take all this lying down for long, however.	B-cause	3363..3446
Its new products and trading techniques have been highly profitable.	B-conjunction	3447..3515
Program trading money managers have gained control over a big chunk of the invested funds in this country, and the pressures on such money managers to produce consistent profits has wedded them to the ability to move rapidly in and out the market that program trading gives them.	I-conjunction	3516..3795
What's more, the last time major Wall Street firms said they were getting out of program trading -- in the aftermath of the 1987 crash -- they waited a few months and then sneaked back into it.	O	3796..3989
Even some members of the Old Guard, despite their current advantage, seem to be conceding that the future belongs with the New Guard.	B-instantiation	3992..4125
Last week, Robert M. Bradley, one of the Big Board's most respected floor traders and head of a major traders' organization, surrendered.	I-instantiation	4126..4263
He sold his exchange seat and wrote a bitter letter to Big Board Chairman John J. Phelan Jr. in which he said the Big Board is too focused on machines, rather than people.	O	4264..4435
He said the exchange is "headed for a real crisis" if program trading isn't curbed.	O	4436..4519
"I do not want my money invested in what I consider as nothing more than a casino," Mr. Bradley wrote.	O	4522..4624
The battle has turned into a civil war at some firms and organizations, causing internal contradictions and pitting employee against employee.	B-instantiation	4627..4769
At Kidder, a unit of General Electric Co., and other big brokerage firms, stockbrokers battle their own firm's program traders a few floors away.	B-conjunction	4770..4915
Corporations like Contel denounce program trading, yet Contel has in the past hired pension fund managers like Bankers Trust Co. that are also big program traders.	I-conjunction	4916..5079
The Big Board -- the nation's premier stock exchange -- is sharply divided between its floor traders and its top executives.	B-restatement	5082..5206
Its entrenched 49 stock specialists firms are fighting tooth and nail against programs.	B-contrast	5207..5294
But the Big Board's leadership -- over the specialists' protests -- two weeks ago began trading a new stock "basket" product designed to facilitate program trading.	B-conjunction	5295..5459
"A lot of people would like to go back to 1970," before program trading, Mr. Phelan said this week.	I-conjunction	5460..5559
I would like to go back to 1970.	B-contrast	5560..5593
But we are not going back to 1970."	I-contrast	5594..5629
Again and again, program-trading's critics raise the "casino" theme.	B-restatement	5632..5700
They say greedy market manipulators have made a shambles of the nation's free-enterprise system, turning the stock market into a big gambling den, with the odds heavily stacked against the small investor.	I-restatement	5701..5905
"The public didn't come to the market to play a game; they can go to Off-Track Betting for that," says A. Brean Murray, chairman of Brean Murray, Foster Securities, a traditional money management firm.	O	5906..6107
The program traders, on the other hand, portray old-fashioned stock pickers as the Neanderthals of the industry.	B-instantiation	6110..6222
Critics like Mr. Murray "are looking for witches, and people who use computers to trade are a convenient boogieman," says J. Thomas Allen, president of Advanced Investment Management Inc., a Pittsburgh firm that runs a $200 million fund that uses index arbitrage.	I-instantiation	6223..6486
"Just a blind fear of the unknown is causing them to beg the regulators for protection."	O	6487..6575
For all the furor, there is nothing particularly complex about the concept of stock-index arbitrage, the most controversial type of computer-assisted program trading.	B-entrel	6578..6744
Like other forms of arbitrage, it merely seeks to take advantage of momentary discrepancies in the price of a single product -- in this case, a basket of stocks -- in different markets -- in this case the New York Stock Exchange and the Chicago futures markets.	I-entrel	6745..7006
That divergence is what stock index traders seek.	O	7009..7058
When it occurs, the traders place orders via computers to buy the basket of stocks (such as the 500 stocks that constitute the Standard & Poor's 500 stock index) in whichever market is cheaper and sell them in the more expensive market; they lock in the difference in price as profit.	O	7059..7343
Such program trades, which can involve the purchase or sale of millions of dollars of stock, occur in a matter of seconds.	B-entrel	7346..7468
A program trade of $5 million of stock typically earns a razor-thin profit of $25,000.	B-cause	7469..7555
To keep program-trading units profitable in the eyes of senior brokerage executives, traders must seize every opportunity their computers find.	I-cause	7556..7699
The speed with which such program trades take place and the volatile price movements they can cause are what program trading critics profess to despise.	O	7702..7854
"If you continue to do this, the investor becomes frightened -- any investor: the odd lotter, mutual funds and pension funds," says Larry Zicklin, managing partner at Neuberger & Berman.	O	7855..8041
But many experts and traders say that program trading isn't the main reason for stock-market gyrations.	B-instantiation	8044..8147
I have not seen one iota of evidence" to support restrictions on program trading, says a Vanderbilt University finance professor, Hans Stoll, an authority on the subject.	B-conjunction	8148..8319
Says the Big Board's Mr. Phelan, "Volatility is greater than program trading."	I-conjunction	8320..8398
The Oct. 13 plunge was triggered not by program traders, but by news of the unraveling of the $6.79 billion buy-out of UAL Corp.	B-cause	8401..8529
Unable to unload UAL and other airline shares, takeover-stock speculators, or risk arbitragers, dumped every blue-chip stock they had.	B-asynchronous	8530..8664
While program trades swiftly kicked in, a "circuit breaker" that halted trading in stock futures in Chicago made some program trading impossible.	B-entrel	8665..8810
Susan Del Signore, head trader at Travelers Investment Management Co., says critics are ignoring "the role the {takeover stock} speculator is taking in the market as a source of volatility."	I-entrel	8811..9001
Many arbs are "overleveraged," she says, and they "have to sell when things look like they fall apart."	O	9002..9105
Like virtually everything on Wall Street, the program-trading battle is over money, and the traditionalists have been losing out on bundles of it to the New Guard in recent years.	B-entrel	9108..9287
Take the traditional money managers, or "stock pickers," as they are derisively known among the computer jockeys.	B-restatement	9288..9401
Traditional stock managers like to charge 50 cents to 75 cents for every $100 they manage for big institutional investors, and higher fees for smaller investors.	B-concession	9402..9563
Yet many such managers consistently fail to even keep up with, much less beat, the returns of standard benchmarks like the S&P	I-concession	9564..9690
Not surprisingly, old-style money managers have been losing clients to giant stock-index funds that use computers to juggle portfolios so they mirror the S&P 500.	B-conjunction	9693..9855
The indexers charge only a few pennies per $100 managed.	B-entrel	9856..9912
Today, about $200 billion, or 20% of all pension-fund stock investments, is held by index funds.	I-entrel	9913..10009
The new Wall Street of computers and automated trading threatens to make dinosaurs of the 49 Big Board stock-specialist firms.	B-entrel	10012..10138
These small but influential floor brokers long have earned fat returns of 30% to 40% a year on their capital, by virtue of their monopoly in making markets in individual stocks.	B-cause	10139..10316
The specialists see any step to electronic trading as a death knell.	B-conjunction	10317..10385
And they believe the Big Board, under Mr. Phelan, has abandoned their interest.	B-conjunction	10388..10467
The son of a specialist and once one himself, Mr. Phelan has nonetheless been striving -- with products like the new stock basket that his former colleagues dislike so much -- to keep index funds and other program traders from taking their business to overseas markets.	I-conjunction	10468..10737
Meanwhile, specialists' trading risks have skyrocketed as a result of stock-market volatility.	O	10740..10834
"When the sell programs hit, you can hear the order printers start to go" on the Big Board trading floor, says one specialist there.	O	10835..10967
"The buyers walk away, and the specialist is left alone" as the buyer of last resort for his stable of stocks, he contends.	O	10968..11091
No one is more unhappy with program trading than the nation's stockbrokers.	B-cause	11094..11169
They are still trying to lure back small investors spooked by the 1987 stock-market crash and the market's swings since then.	I-cause	11170..11295
"Small investors are absolutely dismayed that Wall Street is stacking the deck against them, and these wide swings are scaring them to death," says Raymond A. Mason, chairman of regional broker Legg Mason Inc. in Baltimore.	O	11296..11519
Stockbrokers' business and pay has been falling.	B-instantiation	11520..11568
Last year, the average broker earned $71,309, 24% lower than in 1987.	I-instantiation	11569..11638
Corporate executives resent that their company's stock has been transformed into a nameless piece of a stock-index basket.	O	11641..11763
Index traders who buy all 500 stocks in the S&P 500 often don't even know what the companies they own actually do, complains Andrew Sigler, chairman of Champion International Corp.	O	11764..11944
"Do you make sweatshirts or sparkplugs?	O	11945..11984
Oh, you're in the paper business," is one reaction Mr. Sigler says he's gotten from his big institutional shareholders.	O	11985..12104
By this September, program traders were doing a record 13.8% of the Big Board's average daily trading volume.	B-entrel	12107..12216
Among the top practitioners were Wall Street blue bloods: Morgan Stanley & Co., Kidder Peabody, Merrill Lynch, Salomon Brothers Inc. and PaineWebber Group Inc.	I-entrel	12217..12376
But then came Oct. 13 and the negative publicity orchestrated by the Old Guard, particularly against index arbitrage.	B-cause	12377..12494
The indexers' strategy for the moment is to hunker down and let the furor die.	I-cause	12495..12573
"There's a lynch-mob psychology right now," says the top program-trading official at a Wall Street firm.	O	12574..12678
"Wall Street's cash cow has been gored, but I don't think anyone has proven that index arbitrage is the problem."	O	12679..12792
Too much money is at stake for program traders to give up.	B-instantiation	12795..12853
For example, stock-index futures began trading in Chicago in 1982, and within two years they were the fastest-growing futures contract ever launched.	B-conjunction	12854..13003
Stock futures trading has minted dozens of millionaires in their 20s and 30s.	B-entrel	13004..13081
Now, on a good day, Chicago's stock-index traders trade more dollars worth of stock futures than the Big Board trades in stock.	I-entrel	13082..13209
Now the stage is set for the battle to play out.	B-cause	13212..13260
The anti-programmers are getting some helpful thunder from Congress.	B-instantiation	13261..13329
Program traders' "power to create total panic is so great that they can't be allowed to have their way," says Rep. Edward Markey, a Massachusetts Democrat.	I-instantiation	13330..13485
"We have to have a system that says to those largest investors:	O	13486..13549
`Sit down!	O	13550..13560
You will not panic,	O	13561..13580
you will not put the financial system in jeopardy. '"	O	13581..13634
But the prospects for legislation that targets program trading is unlikely anytime soon.	B-restatement	13637..13725
Many people, including the Big Board, think that it's too late to put the genie back in the bottle.	I-restatement	13726..13825
The Big Board's directors meet today to approve some program-trading restrictions, but a total ban isn't being considered, Big Board officials say.	O	13826..13973
"You're not going to stop the idea of trading a basket of stocks," says Vanderbilt's Prof. Stoll. "	O	13976..14075
Program trading is here to stay, and computers are here to stay, and we just need to understand it."	O	14075..14175
Short of a total ban, some anti-programmers have proposed several middle-ground reforms, which they say would take away certain advantages program traders currently enjoy in the marketplace that other investors don't.	O	14178..14395
One such proposal regarding stock-index futures is an increase in the margin requirement -- or the "good-faith" payment of cash needed to trade them -- to about the same level as the margin requirement for stocks.	O	14398..14611
Currently, margins on stock futures purchases are much lower -- roughly 7% compared with 50% for stocks -- making the futures market much faster and potentially more speculative.	O	14612..14790
Program trading critics also want the Federal Reserve Board, rather than the futures industry, to set such margins.	O	14791..14906
Futures traders respond that low margins help keep their markets active.	O	14909..14981
Higher margins would chase away dozens of smaller traders who help larger traders buy and sell, they say.	O	14982..15087
Another proposed reform is to have program traders answer to an "uptick rule"a reform instituted after the Great Crash of 1929 that protects against stocks being relentlessly beaten downward by those seeking to profit from lower prices, namely short sellers.	O	15090..15348
The Big Board's uptick rule prevents the short sale of a stock when the stock is falling in price.	O	15349..15447
But in 1986, program traders received what amounted to an exemption from the uptick rule in certain situations, to make it easier to link the stock and futures markets.	B-entrel	15450..15618
A reinstatement of the uptick rule for program traders would slow their activity considerably.	I-entrel	15619..15713
Program traders argue that a reinstatement of the rule would destroy the "pricing efficiency" of the futures and stock markets.	O	15714..15841
James A. White contributed to this article.	O	15844..15887
Fundamentalists Jihad	O	15890..15911
Big Board Chairman John Phelan said yesterday that he could support letting federal regulators suspend program trading during wild stock-price swings.	O	15921..16071
Thus the band-wagon psychology of recent days picks up new impetus.	O	16072..16139
Index arbitrage is a common form of program trading.	B-entrel	16142..16194
As usually practiced it takes advantage of a rather basic concept: Two separate markets in different locations, trading basically the same widgets, can't trade them for long at prices that are widely different.	B-restatement	16195..16405
In index arbitrage, the widget is the S&P 500, and its price is constantly compared between the futures market in Chicago and the stock markets largely in New York.	B-entrel	16406..16570
To profit from an index-arbitrage opportunity, someone who owns the S&P 500 widget in New York must sell it and replace it with a cheaper S&P 500 widget in Chicago.	I-entrel	16571..16735
If the money manager performing this service is being paid by his clients to match or beat the return of the S&P 500 index, he is likely to remain fully invested at all times.	B-cause	16738..16913
(Few, if any, index-fund managers will risk leveraging performance by owning more than 100% exposure to stocks, and equally few will want to own less than a 100% position should stocks rise.)	I-cause	16914..17105
By constantly seeking to own the cheapest widget, index-arbitrage traders hope to add between 1% and 3% to the annual return of the S&P 500.	B-restatement	17108..17248
That represents a very thin "excess" return, certainly far less than what most fundamental stock pickers claim to seek as their performance objective.	B-cause	17249..17399
The fact that a vast majority of fundamentalist money managers fail to beat the S&P 500 may contribute to the hysteria surrounding the issue.	I-cause	17400..17541
As more managers pursue the index-arbitrage strategy, these small opportunities between markets will be reduced and, eventually, eliminated.	B-cause	17544..17684
The current opportunities arise because the process for executing a buy or sell order in the actual stocks that make up the S&P 500 is more cumbersome than transacting in the futures market.	B-entrel	17685..17875
The New York Stock Exchange's attempt to introduce a new portfolio basket is evidence of investors' desires to make fast and easy transactions of large numbers of shares.	I-entrel	17876..18046
So if index arbitrage is simply taking advantage of thin inefficiencies between two markets for the same widget, how did "program trading" evolve into the evil creature that is evoking the curses of so many observers?	B-entrel	18049..18266
All arguments against program trading, even those pressed without fact, conclude with three expected results after "reforms" are implemented: 1) reduced volatility, 2) a long-term investment focus, and 3) a level playing field for the small investor.	B-concession	18267..18517
But many of these reforms are unneeded, even harmful.	I-concession	18518..18571
Reducing volatility.	O	18574..18594
An index-arbitrage trade is never executed unless there is sufficient difference between the markets in New York and Chicago to cover all transaction costs.	O	18595..18751
Arbitrage doesn't cause volatility; it responds to it.	O	18752..18806
Think about what causes the difference in prices between the two markets for S&P 500 stocks -- usually it is large investors initiating a buy or sell in Chicago.	O	18807..18968
A large investor will likely cause the futures market to decline when he sells his futures.	B-cause	18971..19062
Arbitrage simply transfers his selling pressure from Chicago to New York, while functioning as a buyer in Chicago.	B-cause	19063..19177
The start of the whole process is the key-someone must fundamentally increase or decrease his ownership in widgets to make widget prices move.	I-cause	19178..19320
Why does this large hypothetical seller trade in Chicago instead of New York?	B-cause	19323..19400
Perhaps he is willing to sacrifice to the arbitrage trader some small profit in order to get quick and certain execution of his large trade.	B-entrel	19401..19541
In a competitive market, this investor has many ways to execute his transactions, and he will have more alternatives (both foreign and domestic) if his volume is profitable for an exchange to handle.	I-entrel	19542..19741
If not Chicago, then in New York; if not the U.S., then overseas.	O	19742..19807
Volatility surrounding his trades occurs not because of index arbitrage, but because his is a large addition or subtraction to a widget market with finite liquidity.	B-cause	19810..19975
Eliminate arbitrage and liquidity will decline instead of rising, creating more volatility instead of less.	B-conjunction	19976..20083
The speed of his transaction isn't to be feared either, because faster and cleaner execution is desirable, not loathsome.	I-conjunction	20084..20205
If slowing things down could reduce volatility, stone tablets should become the trade ticket of the future.	O	20206..20313
Encouraging long-term investing.	O	20316..20348
We must be very cautious about labeling investors as "long-term" or "short-term.	B-cause	20349..20430
Policies designed to encourage one type of investor over another are akin to placing a sign over the Big Board's door saying: "Buyers welcome, sellers please go away!"	I-cause	20431..20598
The ultimate goal of any investor is a profit motive, and regulators should not concern themselves with whether investors are sufficiently focused on the long term.	O	20599..20763
A free market with a profit motive will attract each investor to the liquidity and risks he can tolerate.	O	20764..20869
In point of fact, volatility as measured by the annualized standard deviation of daily stock price movements has frequently been much higher than it is today.	O	20872..21030
Periods before the advent of futures or program trading were often more volatile, usually when fundamental market conditions were undergoing change (1973-75, 1937-40, and 1928-33 for example).	B-cause	21031..21223
It is interesting to see the fundamental stock pickers scream "foul" on program trading when the markets decline, while hailing the great values still abounding as the markets rise.	B-contrast	21224..21405
Could rising volatility possibly be related to uncertainty about the economics of stocks, instead of the evil deeds of program-trading goblins?	I-contrast	21406..21549
Some of the proposed fixes for what is labeled "program-trading volatility" could be far worse than the perceived problem.	B-instantiation	21552..21674
In using program trading as a whipping boy, fundamentalist investors stand to gain the high ground in wooing small investors for their existing stock-selection products.	B-concession	21675..21844
They may, however, risk bringing some damaging interference from outside the markets themselves.	B-instantiation	21845..21941
How does a nice new tax, say 5%, on any financial transaction sound?	I-instantiation	21942..22010
That ought to make sure we're all thinking for the long term.	O	22011..22072
Getting a level playing field.	O	22075..22105
This argument is perhaps the most interesting one for abolishing program trading -- not because of its merits, but because of the firms championing the cause.	B-restatement	22106..22264
The loudest of these reformers are money managers who cater to smaller investors.	B-entrel	22265..22346
They continually advise their clients on which individual stocks to buy or sell, while their clients continue to hope for superior performance.	B-restatement	22347..22490
Even with mutual funds, the little investor continues to tolerate high fees, high commissions and poor performance, while index-fund managers slowly amass a better record with lower fees, lower commissions and less risk.	B-concession	22491..22711
Yet our efforts are somehow less noble than those of an investment expert studiously devouring press clippings on each company he follows.	I-concession	22712..22850
Almost all new regulation is introduced in the interests of protecting the little guy, and he invariably is the one least able to cope with its consequences.	O	22853..23010
If spreads available from index arbitrage are so enormous, surely any sizable mutual-fund company could profit from offering it to small investors.	B-concession	23013..23160
The sad reality is that the retail investor continues to pursue stellar performers first, while leaving institutions to grapple with basis points of performance on large sums of money quarter by quarter.	B-cause	23161..23364
Cost-effective index funds just aren't sexy enough to justify the high fees and commissions that retail customers frequently pay, and that institutional customers refuse to pay.	I-cause	23365..23542
Each new trading roadblock is likely to be beaten by institutions seeking better ways to serve their high-volume clients, here or overseas.	B-cause	23545..23684
Legislating new trading inefficiencies will only make things harder on the least sophisticated investors.	I-cause	23685..23790
So what is next for program trading?	B-restatement	23793..23829
Left to its own devices, index arbitrage will become more and more efficient, making it harder and harder to do profitably.	B-restatement	23830..23953
Left to its own devices, index arbitrage will become more and more efficient, making it harder and harder to do profitably.Spreads will become so tight that it won't matter which market an investor chooses -- arbitrage will prevent him from gaining any temporary profit.	B-contrast	23954..24101
If government or private watchdogs insist, however, on introducing greater friction between the markets (limits on price moves, two-tiered execution, higher margin requirements, taxation, etc.), the end loser will be the markets themselves.	I-contrast	24104..24344
Instead, we ought to be inviting more liquidity with cheaper ways to trade and transfer capital among all participants.	O	24345..24464
Mr. Allen's Pittsburgh firm, Advanced Investment Management Inc., executes program trades for institutions.	O	24467..24574
