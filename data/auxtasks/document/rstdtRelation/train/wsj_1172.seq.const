Britain 's current account deficit dropped to # 1.6 billion	(Summary(Contrast(Same-unit(Summary
( $ 2.56 billion )	Summary)
in September from an adjusted # 2 billion	(Same-unit(Summary
( $ 3.21 billion )	Summary)
the previous month ,	Same-unit))
but the improvement comes amid increasing concern	(Elaboration
that a recession could strike the U.K. economy next year .	Elaboration))
The Confederation of British Industry 's latest survey shows	(Elaboration(Background(Background(Contrast(Explanation(Cause(Attribution
that business executives expect a pronounced slowdown ,	Attribution)
largely because of a 16-month series of interest-rate increases	(Elaboration
that has raised banks ' base lending rates to 15 % .	Elaboration))
`` The outlook has deteriorated since the summer ,	(Joint(Attribution(Elaboration
with orders and employment falling and output at a standstill , ''	Elaboration)
said David Wigglesworth , chairman of the industry group 's economic committee .	Attribution)
He also said	(Elaboration(Attribution
investment by businesses is falling off .	Attribution)
Of 1,224 companies surveyed , 31 % expect to cut spending on plant equipment and machinery ,	(Comparison
while only 28 % plan to spend more .	Comparison))))
But	(Same-unit
despite mounting recession fears ,	(Elaboration(Contrast
government data do n't yet show the economy grinding to a halt .	Contrast)
Unemployment , for example , has continued to decline ,	(Joint
and the September trade figures showed increases in both imports and exports .	Joint))))
As a result , Prime Minister Margaret Thatcher 's government is n't currently expected to ease interest rates before next spring , if then .	(Explanation(Explanation
Chancellor of the Exchequer Nigel Lawson views the high rates as his chief weapon against inflation ,	(Joint(Elaboration
which was ignited by tax cuts and loose credit policies in 1986 and 1987 .	Elaboration)
Officials fear	(Attribution
that any loosening this year could rekindle inflation or further weaken the pound against other major currencies .	Attribution)))
Fending off attacks on his economic policies in a House of Commons debate yesterday ,	(Background(Attribution(Enablement
Mr. Lawson said	Enablement)
inflation `` remains the greatest threat to our economic well-being ''	(Cause
and promised to take `` whatever steps are needed ''	(Cause
to choke it off .	Cause)))
The latest government figures said	(Joint(Attribution
retail prices in September were up 7.6 % from a year earlier .	Attribution)
Many economists have started predicting a mild recession next year .	(Elaboration
David Owen , U.K. economist with Kleinwort Benson Group , reduced his growth forecast for 1990 to 0.7 % from 1.2 %	(Contrast(Joint
and termed the risk of recession next year `` quite high . ''	Joint)
But he said	(Attribution
the downturn probably wo n't become a `` major contraction '' similar to those of 1974 and 1982 .	Attribution)))))))
Still , Britain 's current slump is a cause for concern here	(Background(Background
as the nation joins in the European Community 's plan	(Elaboration
to create a unified market by 1992 .	Elaboration))
Compared with the major economies on the Continent ,	(Cause(Comparison
the U.K. faces both higher inflation and lower growth in the next several months .	Comparison)
As a result ,	(Same-unit(Attribution
Mr. Owen warned ,	Attribution)
investment will be more likely to flow toward the other European economies	(Cause
and `` the U.K. will be less prepared for the single market . ''	Cause)))))
Britain 's latest trade figures contained some positive news for the economy , such as a surge in the volume of exports ,	(Contrast(Explanation(Elaboration
which were 8.5 % higher than a year earlier .	Elaboration)
But while September exports rose to # 8.43 billion ,	(Cause(Contrast
imports shot up to # 10.37 billion .	Contrast)
The resulting # 1.9 billion merchandise trade deficit was partly offset by an assumed surplus of # 300 million in so-called invisible items ,	(Elaboration
which include income from investments , services and official transfers .	Elaboration)))
Despite the narrowing of the monthly trade gap ,	(Cause(Contrast
economists expect the current account deficit for all of 1989 to swell to about # 20 billion from # 14.6 billion in 1988 .	Contrast)
Increasingly , economists say	(Explanation(Attribution
the big deficit reflects the slipping competitive position of British industry .	Attribution)
`` When the country gets wealthier ,	(Attribution(Background
we tend to buy high-quality imports , ''	Background)
Mr. Owen said .	Attribution))))))
