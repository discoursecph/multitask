Congress sent President Bush an $ 18.4 billion fiscal 1990 Treasury and Postal Service bill providing $ 5.5 billion for the Internal Revenue Service and increasing the Customs Service 's air-interdiction program nearly a third .	root	9..233
Final approval came on a simple voice vote in the Senate	norel	236..292
and the swift passage contrasted with months of negotiations over the underlying bill which is laced with special-interest provisions for both members and the executive branch .	conjunction	293..470
An estimated $ 33 million was added for university and science grants , including $ 1.5 million for Smith College .	norel	473..584
And Southwest lawmakers were a driving force behind $ 54.6 million for U.S.-Mexico border facilities , or more than double the administration 's request .	conjunction	585..735
More than $ 1.8 million is allocated for pensions and expenses for former presidents	norel	738..821
and the budget for the official residence of Vice President Quayle is more than doubled , with $ 200,000 designated for improvements to the property .	conjunction	822..970
Even the Office of Management and Budget is remembered with an extra $ 1 million to help offset pay costs that other government departments are being asked to absorb .	conjunction	971..1136
Within the IRS , nearly $ 1.95 billion is provided for processing tax returns , a 12 % increase over fiscal 1989 and double what the government was spending five years ago .	norel	1139..1307
Investigation and taxpayer service accounts would grow to $ 1.6 billion	entrel	1308..1378
and Congress specifically added $ 7.4 million for stepped up criminal investigations of money laundering related to drug traffic .	conjunction	1379..1508
The large increase in Customs Service air-interdiction funds is also intended to counter smuggling	conjunction	1511..1609
and the annual appropriations level has more than quadrupled in five years .	conjunction	1610..1686
The $ 196.7 million provided for fiscal 1990 anticipates the purchase of a Lockheed P-3 surveillance aircraft and five Cessna Citation II jets .	restatement	1687..1829
Despite administration reservations , the plan has had the quiet backing of customs officials as well as influential lawmakers from Cessna 's home state , Kansas .	entrel	1830..1989
Among legislative provisions attached to the bill is a ban on any Treasury Department expenditure for enforcement of a 1986 tax provision intended to counter discrimination in employee-benefit plans .	norel	1992..2191
Small-business interests have lobbied against the so-called Section 89 tax rules .	entrel	2192..2273
Repeal is considered likely now	entrel	2274..2305
but the Treasury Department bill has been used as a vehicle to raise the profile of the issue and block any action in the interim .	contrast	2306..2437
Less noticed is a bit of legislative legerdemain by Houston Republicans on behalf of HEI Corp. of Texas to retroactively " move " a Missouri hospital from one county to the next to justify higher Medicare reimbursements .	norel	2440..2658
The provision seeks to wipe out an estimated $ 1.4 million in claims made by the Health Care Finance Administration against HEI , which owned the hospital in Sullivan , Mo. , during most of the four-year period -- 1983-1987 -- covered in the amendment .	restatement	2659..2907
In a separate development , a private meeting is scheduled this morning between House Appropriations Committee Chairman Jamie Whitten ( D. , Miss . ) and Sen. Dale Bumpers ( D. , Ark . ) in an effort to end a dispute which for two weeks has delayed action on an estimated $ 44 billion agriculture bill .	norel	2910..3202
A House-Senate conference reached agreement Oct. 5 on virtually all major provisions of the bill	entrel	3203..3299
but final settlement has been stalled because of differences between the two men over the fate of a modest Arkansas-based program to provide technical information to farmers seeking to reduce their dependence on chemical fertilizers and pesticides .	concession	3300..3549
The program 's nonprofit sponsors received $ 900,000 in fiscal 1989 through an Extension Service grant	norel	3552..3652
but Mr. Whitten has been adamant in insisting that the program be cut in 1990 .	contrast	3653..3732
The 79-year-old Mississippian takes a more orthodox , entrenched view of agriculture policy than those in the movement to reduce chemical use	cause	3733..3873
but as a master of pork-barrel politics , he is believed to be annoyed as well that the project moved to Arkansas from a Tennessee center near Memphis and the northern Mississippi border .	contrast	3874..4061
