Manville , having rid itself of asbestos , now sells fiberglass , forest products , minerals and industrial goods .	B-entrel	9..119
Heady stuff it 's not .	B-comparison	120..141
But Manville 's ownership is unusual , and it has caught the eye of some canny -- and patient -- investors .	I-comparison	144..249
The Denver-based concern , which emerged from bankruptcy-law proceedings late last year , is controlled by the Manville Personal Injury Settlement Trust .	B-entrel	250..401
The trust , which owns 80 % of Manville 's stock on a fully diluted basis , is a separate legal entity that is settling claims with asbestos victims .	I-entrel	402..547
When and if the trust runs out of cash -- which seems increasingly likely -- it will need to convert its Manville stock to cash .	B-comparison	550..678
But as an 80 % owner , if it tried to sell much of its stock in the market , it would likely depress the price of its shares .	I-comparison	679..801
Some investors say there is a good chance that the trust will , instead , seek to convert the company 's shares to cash , in some sort of friendly restructuring that would n't involve just dumping stock on the market .	O	804..1016
" Their principal asset is Manville common stock , " says Jeffrey Halis , who runs Sabre Associates , an investment fund that owns Manville shares .	O	1019..1161
" If they tried to sell , they 'd be chasing their own tail .	B-expansion	1162..1219
What makes the most sense is to find someone who wants to buy the whole company or cause a recapitalization of all shares . "	I-expansion	1220..1343
The trust is n't commenting on when it might need to liquefy its Manville stock .	B-comparison	1346..1425
However , the trust 's cash flow from investments is far short of its payments to asbestos victims .	B-expansion	1426..1523
Its cash and liquid securities fell by $ 338 million in the first six months of 1989 .	I-expansion	1524..1608
The trust also will receive $ 75 million a year starting in 1991 on a bond it holds from Manville .	B-expansion	1611..1708
And , beginning in 1992 , it will have a claim on as much as 20 % of Manville 's annual net income .	B-comparison	1709..1804
Even so , the trust would seem to be facing a cash crunch .	B-contingency	1805..1862
As of June 30 , it had settled only about 15,000 of the 81,000 received claims from asbestos victims , for an average of $ 40,424 each .	B-comparison	1863..1995
( The average should drop over time , since the most expensive claims are being settled first ) .	I-comparison	1996..2089
And as of midyear , settled but unpaid claims amounted to $ 136 million -- more than half the trust 's total of $ 268 million in cash and marketable securities .	O	2090..2246
" At some point , we 're going to need an infusion of funds , " a person close to the trust says .	O	2247..2339
Even before then , the trust may be eager to unload Manville stock .	B-contingency	2342..2408
It does n't pay a dividend , and this trust needs income .	B-expansion	2409..2464
Moreover , with 88 % of its assets tied up in Manville , the trust is badly in need of diversification .	I-expansion	2465..2565
" Obviously , a diversified portfolio would have less risk , " the person close to the trust says .	O	2566..2660
Manville itself does n't rule out a restructuring .	B-expansion	2663..2712
Though the ink is barely dry on its new , post-bankruptcy law structure , Bill Bullock , Manville 's head of investor relations , says the company is continually pondering " whether there is a better way to be structured .	I-expansion	2713..2928
We understand that the trust is ultimately going to need to sell some of our shares , " he says .	O	2929..3023
Of course , one option would be for Manville to buy out the trust 's shares -- which would do little to benefit public stockholders .	O	3026..3156
But the trust , in most cases , is forbidden from seeking a buyer for only its shares before November 1993 .	B-expansion	3157..3262
And both the trust and Manville are seeking to avoid the bad publicity that , in the asbestos era , tarred the Manville name .	I-expansion	3263..3386
Thus , analysts say , the trust is unlikely to do anything that would hurt Manville 's other shareholders .	O	3387..3490
" This is a rare case of a company with a big majority holder which will probably act in the interests of the minority holders , " one investor says .	O	3491..3637
Even if there is no restructuring , Manville seems to be attractive long-term .	B-comparison	3640..3717
Its stock , at 9 5/8 , trades at about 8 1/2 times estimated 1989 earnings -- an appropriately low multiple for a company with recession-sensitive customers .	I-comparison	3718..3873
Mr. Bullock says 45 % of revenues are tied to construction .	B-expansion	3874..3932
Analysts predict little or no near-term growth .	B-comparison	3933..3980
They are , nonetheless , high on Manville 's management .	B-expansion	3983..4036
" It 's one of the best in the business , " says Salomon Brothers analyst Stephen Dobi .	I-expansion	4037..4120
And he says Manville has the financial flexibility to buy other companies at the best possible moment when other construction-related firms are hurting and selling cheap .	O	4121..4290
With a conservative debt-to-equity ratio of 1-to-1 -- and $ 329 million in cash -- Manville is indeed actively on the prowl .	O	4291..4414
So far , as a price-conscious shopper , Manville has n't bought much .	O	4417..4483
Paul Kleinaitis , an analyst at Duff & Phelps , says , " Even though they have borrowing power , they have been disciplined about acquisitions . "	O	4484..4623
And Mr. Kleinaitis says that with 80 % of its stock in friendly hands , Manville is the rare publicly traded company that can ignore short-term stock fluctuations and plan for the long haul .	O	4624..4812
Manville ( NYSE ; Symbol : MVL )	O	4815..4845
Business : Forest products and roofing	O	4848..4885
Year ended Dec. 31 , 1988 : Sales : $ 2.06 billions Net loss : $ 1.30 billion *	O	4888..4964
Third quarter , Sept. 30 , 1989 : Per-share earnings : 30 cents vs. 44 cents **	O	4967..5043
Average daily trading volume : 74,351 shares	O	5046..5091
Common shares outstanding : 120 million	O	5094..5132
* Includes $ 1.29 billion extraordinary charge .	O	5135..5180
** Year ago figure is restated .	O	5183..5213
