ARTICLE I, SECTION 7, CLAUSE 2:	O	9..40
Every Bill which shall have passed the House of Representatives and the Senate, shall, before it becomes a Law, be presented to the President of the United States; If he approve he shall sign it, but if not he shall return it, with his Objections to that House in which it shall have originated, who shall enter the Objections at large on their Journal, and proceed to reconsider it.	O	41..424
If after such Reconsideration two thirds of that House shall agree to pass the Bill, it shall be sent, together with the Objections, to the other House, by which it shall likewise be reconsidered, and if approved by two thirds of that House, it shall become a Law... .	O	425..693
ARTICLE I, SECTION 7, CLAUSE 3:	O	696..727
Every Order, Resolution, or Vote to which the Concurrence of the Senate and House of Representatives may be necessary (except on a question of Adjournment) shall be presented to the President of the United States; and before the Same shall take Effect, shall be approved by him, or being disapproved by him, shall be repassed by two thirds of the Senate and House of Representatives, according to the Rules and Limitations prescribed in the Case of a Bill.	O	728..1184
President Bush told reporters a few months ago that he was looking for the right test case to see whether he already has the line-item veto	B-conjunction	1187..1327
Vice President Quayle and Budget Director Darman said recently they've joined the search.	I-conjunction	1328..1417
On Tuesday, the subject came up again when Marlin Fitzwater explained the constitutional argument based on the provisions above to the White House press corps.	O	1418..1577
President Bush doesn't have any provision in mind, but line-item-veto bait will be like earthworms at midnight in the coming Continuing Resolution.	O	1580..1727
The harder question is whether anyone yet understands that Mr. Bush's fight for his constitutional prerogatives is about politics as much as it is about law.	O	1728..1885
We have been persuaded by the constitutional argument for the inherent line-item veto since 1987, when lawyer Stephen Glazier first made the case on this page.	O	1888..2047
The 1974 budget "reform," passed over President Nixon's veto, took away the presidential impoundment power, thereby introducing monstrous CRs and eviscerating the presidential veto.	O	2048..2229
Mr. Glazier discovered that the Founders had worried that Congress might take the President out of the loop.	O	2232..2340
Article I, Section 7, Clause 3 says that whether it's called an "order, resolution or vote" or anything else, Presidents must have the chance to veto	B-cause	2341..2491
Labeling an omnibus budget a "bill" can't deprive the President of his power to veto items.	I-cause	2492..2583
Finding a test case shouldn't be hard, but there is something to be said for picking the best one possible.	O	2586..2693
The White House had the perfect case, but Congress blinked before it could go to court.	O	2694..2781
After the HUD and S&L stories broke, some Congressmen began to worry that their influence peddling at executive-branch and independent agencies might some day get them in trouble	B-restatement	2782..2961
They worried about an Interior Department directive to log all communications with Members or their staffs	B-cause	2962..3069
Congress inserted the following into the Interior appropriation: "None of the funds available under this title may be used to prepare reports on contacts between employees of the Dept. of the Interior and Members and committees of Congress and their staff."	I-cause	3070..3327
The White House warned that this would be an unconstitutional usurpation of its power	B-conjunction	3330..3416
When it threatened to use this provision as the test for a line-item veto, Congress caved	B-restatement	3417..3507
The fear Congress has of any line-item-veto test led Members to add the single most contorted and ridiculous provision this year, "This section shall be effective only on Oct. 1, 1989."	I-restatement	3508..3693
This means Interior contacts cannot be logged only on one day -- a Sunday that had already passed.	O	3694..3792
If the White House is looking for another unconstitutional bill, Rep. John Dingell is trying again to raise the Fairness Doctrine from the dead	B-entrel	3795..3939
President Reagan vetoed this as a First Amendment violation	B-entrel	3940..4000
The "Fairness" Doctrine's enthusiasts are incumbents in the House who know the rules squelch lively discussions on broadcasts, deterring feisty challengers.	I-entrel	4001..4157
There are also other provisions requiring Congressmen to join treaty-negotiating teams and new restrictions on OMB.	O	4158..4273
Unconstitutional bills make good legal targets, but the line-item veto is better understood as a political opportunity than as mere fodder for lawyers.	O	4276..4427
Commenting on the budget mess this week, President Bush said: "The perception out there is that it's the fault of Congress.	O	4428..4551
And you can look to the leadership and ask them why that is the perception of the American people	B-entrel	4552..4651
Exactly right	B-cause	4652..4666
Now's the time to make the political case that Presidents need the line-item weapon to restore discipline to the budget.	I-cause	4667..4787
Congress is in no position to naysay Mr. Bush now that we're into Gramm-Rudman's sequestration.	O	4790..4885
Just this week, the House-Senate conference met -- 231 conferees, divided into 26 different subconferences	B-synchrony	4886..4993
Senator Daniel Inouye agreed to close some bases in Hawaii in exchange for such goodies as $11 million for a parking lot at Walter Reed Hospital	B-conjunction	4994..5139
Conference negotiator Rep. Bill Hefner pulled down $40 million in military bases for North Carolina and graciously allowed Senator James Sasser $70 million for bases in Tennessee.	I-conjunction	5140..5319
President Bush should take the Constitution in one hand and a budget ax in the other and get to work.	O	5320..5421
He should chop out both unconstitutional provisions and budget pork.	O	5422..5490
Congress may have lost any sense of discipline, but that doesn't mean the country must learn to live forever with this mess.	O	5491..5615
President Bush has the power to change how Washington works, if only he will use it.	O	5616..5700
