Of all the one-time expenses incurred by a corporation or professional firm , few are larger or longer term than the purchase of real estate or the signing of a commercial lease .	root	9..186
To take full advantage of the financial opportunities in this commitment , however , the corporation or professional firm must do more than negotiate the best purchase price or lease terms .	contrast	189..376
It must also evaluate the real-estate market in the chosen location from a new perspective .	conjunction	377..468
Specifically , it must understand how real-estate markets overreact to shifts in regional economies	restatement	469..567
and then take advantage of these opportunities .	asynchronous	568..615
When a regional economy catches cold , the local real-estate market gets pneumonia .	norel	618..700
In other words , real-estate market indicators , such as building permits and leasing activity , plummet much further than a local economy in recession .	restatement	701..850
This was seen in the late 1960s in Los Angeles and the mid-1970s in New York .	instantiation	851..928
But the reverse is also true	norel	929..957
When a region 's economy rebounds from a slowdown , these real-estate indicators will rebound far faster than the improving economy .	restatement	958..1089
Why do local real-estate markets overreact to regional economic cycles ?	norel	1092..1163
Because real-estate purchases and leases are such major long-term commitments that most companies and individuals make these decisions only when confident of future economic stability and growth .	cause	1164..1359
Metropolitan Detroit was written off economically during the early 1980s	norel	1362..1434
as the domestic auto industry suffered a serious sales depression and adjustment .	cause	1435..1517
Area employment dropped by 13 % from its 1979 peak	cause	1518..1567
and retail sales were down 14 % .	conjunction	1568..1599
However , the real-estate market was hurt even more .	norel	1600..1651
For example , residential building permits in the trough year of 1982 were off 76 % from the 1979 peak level .	instantiation	1652..1759
Once metropolitan Detroit 's economy rallied in the mid-1980s , real estate rebounded .	norel	1762..1846
Building permits , for example , soared a staggering 400 % between 1982 and the peak year of 1986 .	instantiation	1847..1942
Where , savvy corporations and professional firms are now asking , are today 's opportunities ?	norel	1945..2036
Look no further than metropolitan Houston and Denver , two of the most depressed , overbuilt and potentially undervalued real-estate markets in the nation .	norel	2039..2192
Of course , some observers have touted Houston and Denver for the past five years as a counter-cyclical play .	contrast	2193..2301
But now appears to be the time to act .	contrast	2302..2340
Metropolitan Houston 's economy did drop	norel	2343..2382
and then flatten in the years after its 1982 peak .	conjunction	2383..2433
In the mid-1980s , employment was down as much as 5 % from the 1982 peak	instantiation	2434..2504
and retail sales were off 13 % .	conjunction	2505..2535
The real-estate market suffered even more severe setbacks .	norel	2538..2596
Office construction dropped 97 % .	restatement	2597..2629
The vacancy rate soared more than 20 % in nearly every product category	conjunction	2630..2700
and more than 30 % of office space was vacant .	conjunction	2701..2747
To some observers , the empty office buildings of Houston 's " see-through skyline " were indicative of a very troubled economy .	entrel	2748..2872
As usual , the real-estate market had overreacted .	norel	2875..2924
Actually , the region 's economy retained a firm foundation .	cause	2925..2983
Metropolitan Houston 's population has held steady over the past six years .	restatement	2984..3058
And personal income , after slumping in the mid-1980s , has returned to its 1982 level in real dollar terms .	conjunction	3059..3165
Today , metropolitan Houston 's real-estate market is poised for a significant turnaround .	norel	3168..3256
More than 42,000 jobs were added in metro Houston last year , primarily in biotechnology , petrochemical processing , and the computer industry .	cause	3257..3398
This growth puts Houston in the top five metro areas in the nation last year .	conjunction	3399..3476
And forecasts project a 2.5 % to 3 % growth rate in jobs over the next few years -- nearly twice the national average .	norel	3477..3593
Denver is another metropolitan area where the commercial real-estate market has overreacted to the region 's economic trends	norel	3596..3719
although Denver has not experienced as severe an economic downturn as Houston .	contrast	3720..3799
By some measures , metropolitan Denver 's economy has actually improved in the past four years .	restatement	3800..3893
Its population has continued to increase since 1983 , the peak year of the economic cycle .	instantiation	3894..3983
Employment is now 4 % higher than in 1983 .	list	3984..4025
Buying income in real dollars actually increased 15 % between 1983 and 1987 ( the most recent year available ) .	list	4026..4134
The rates of increase , however , are less than the rapid growth of the boom years	norel	4137..4217
and this has resulted in a loss of confidence in the economy .	conjunction	4218..4280
In a self-fulfilling prophecy , therefore , the region 's real-estate market all but collapsed in recent years .	cause	4281..4389
Housing building permits are down more than 75 % from their 1983 peaks .	instantiation	4390..4460
Although no one can predict when metropolitan Denver 's real-estate market will rebound , major public works projects costing several billion dollars are under way or planned -- such as a new convention center , a major beltway encircling the metropolitan area , and a new regional airport .	norel	4463..4749
When Denver 's regional economy begins to grow faster -- such a recovery could occur as early as next year -- business and consumer confidence will return , and the resulting explosion of real-estate activity will dwarf the general economic rebound .	asynchronous	4750..4997
What real-estate strategy should one follow in a metropolitan area whose economic health is not as easy to determine as Houston 's or Denver 's ?	norel	5000..5142
Generally , overcapacity in commercial real estate is dropping from its mid-1980s peak , even in such economically healthy metropolitan areas as Washington , New York and Los Angeles .	norel	5143..5323
Vacancy rates in the 15 % to 19 % range today may easily rise to the low to mid-20 % range in a couple of years .	cause	5324..5433
Under these conditions , even a flattening out of economic growth -- " catching cold " -- in the healthy metropolitan areas will create significant opportunities for corporations and professional service firms looking for bargains as the realestate industry catches pneumonia .	cause	5434..5707
Those looking for real-estate bargains in distressed metropolitan areas should lock in leases or buy now	norel	5710..5814
those looking in healthy metropolitan areas should take a short-term ( three-year ) lease and wait for the bargains ahead .	contrast	5815..5936
Mr. Leinberger is managing partner of a real-estate advisory firm based in Beverly Hills , Calif .	norel	5939..6035
