For Cathay Pacific Airways , the smooth ride may be ending .	root	9..67
The first signs of trouble came last month when the Hong Kong carrier , a subsidiary of Swire Pacific Ltd. , posted a 5 % drop in operating profit for the first six months and warned that margins will remain under pressure for the rest of the year .	norel	70..315
Securities analysts , many of whom scrapped their buy recommendations after seeing Cathay 's interim figures , believe more jolts lie ahead .	norel	318..455
Fuel and personnel costs are rising	cause	456..491
and tourism in and through Hong Kong remains clouded by China 's turmoil since the June 4 killings in Beijing .	conjunction	492..602
In addition , delivery delays for the first two of as many as 28 Boeing 747-400s that the carrier has ordered have raised costs	conjunction	605..731
because personnel had been hired to man the planes .	cause	732..783
And tough competition in the air-freight market is cutting into an important sideline .	conjunction	784..870
There also is concern that once Hong Kong reverts to China 's sovereignty in 1997 , Cathay will be forced to play second fiddle to China 's often-disparaged flag carrier , Civil Aviation Administration of China , or CAAC .	conjunction	873..1089
" The sense is we would never be in a position again where everything works for us the way it did before , " says Rod Eddington , Cathay 's commercial director .	norel	1092..1247
Sarah Hall , an analyst at James Capel ( Far East ) Ltd. , says there is n't much Cathay can do about rising costs for jet fuel , Hong Kong 's tight labor market , or the strengthening of the local currency , which is pegged to the U.S. dollar .	norel	1250..1485
These factors are further complicated by the airline 's push to transform itself from a regional carrier to an international one , Ms. Hall says .	norel	1486..1629
Ms. Hall expects Cathay 's profit to grow around 13 % annually this year and next .	norel	1632..1712
In 1988 , it earned $ 2.82 billion Hong Kong ( US$ 361.5 million ) on revenue of HK$ 11.79 billion .	entrel	1713..1806
Cathay is taking several steps to bolster business .	norel	1809..1860
One step is to beef up its fleet .	instantiation	1861..1894
In addition to aircraft from Boeing Co. , Cathay announced earlier this year an order for as many as 20 Airbus A330-300s .	restatement	1895..2015
The expansion , which could cost as much as US$ 5.7 billion over the next eight years , will expand the fleet to about 43 planes by 1991 , up from 30 at the end of last year , according to Sun Hung Kai Securities Ltd .	cause	2016..2228
The fuel-efficient Airbus planes will be used largely to replace Cathay 's aging fleet of Lockheed Tristars for regional flights	norel	2231..2358
while the Boeing aircraft will be used on long-haul routes to Europe and North America .	synchrony	2359..2447
Cathay also is moving some of its labor-intensive data-processing operations outside Hong Kong .	norel	2450..2545
Fierce bidding for young employees in Hong Kong is pushing up Cathay 's labor costs by 20 % a year for low-level staff	cause	2546..2662
while experienced , skilled employees are leaving the colony as part of the brain drain .	comparison	2663..2751
Some jobs already have been moved to Australia	cause	2752..2798
and there are plans to place others in Canada .	conjunction	2799..2846
David Bell , a spokesman for the airline , says the move is partly aimed at retaining existing staff who are leaving to secure foreign passports ahead of 1997 .	cause	2847..3004
Cathay is working to promote Hong Kong as a destination worth visiting on its own merits , rather than just a stopover .	norel	3007..3125
Although the June 4 killings in Beijing have hurt its China flights , Cathay 's other routes have retained high load factors .	entrel	3126..3249
Mr. Eddington regards promoting Hong Kong as an important part of attracting visitors from Japan , South Korea and Taiwan , where the number of people looking to travel abroad has surged .	cause	3250..3435
There also has been speculation that Cathay will be among the major private-sector participants in the Hong Kong government 's plans to build a new airport , with the carrier possibly investing in its own terminal .	norel	3438..3650
Cathay officials decline to comment on the speculation .	contrast	3651..3706
Mr. Eddington sees alliances with other carriers -- particularly Cathay 's recent link with AMR Corp. 's American Airlines -- as an important part of Cathay 's strategy .	norel	3709..3876
But he emphasizes that Cathay has n't any interest in swapping equity stakes with the U.S. carrier or with Lufthansa , the West German airline with which it has cooperated for about a decade .	comparison	3877..4066
Analysts believe Cathay is approached for such swaps by other carriers on a regular basis , particularly as the popularity of share exchanges has grown among European carriers .	entrel	4067..4242
" We think alliances are very important , " Mr. Eddington says .	norel	4245..4305
" But we 'd rather put funds into our own business rather than someone else 's .	concession	4306..4382
I 'm not sure cross-ownership would necessarily make things smoother . "	conjunction	4383..4452
In a pattern it aims to copy in several key U.S. destinations , Cathay recently announced plans to serve San Francisco by flying into American Airlines ' Los Angeles hub and routing continuing passengers onto a flight on the U.S. carrier .	norel	4455..4691
" We 'll never have a big operation in the U.S. , and they 'll never have one as big as us in the Pacific , " Mr. Eddington says .	norel	4694..4817
" But this way , American will coordinate good extensions to Boston , New York , Chicago and Dallas .	pragmatic contrast	4818..4914
We 'll coordinate on this end to places like Bangkok , Singapore and Manila . "	contrast	4915..4990
Asian traffic , which currently accounts for 65 % of Cathay 's business , is expected to continue as the carrier 's mainstay .	norel	4993..5113
Cathay has long stated its desire to double its weekly flights into China to 14	restatement	5114..5193
and it is applying to restart long-canceled flights into Vietnam .	conjunction	5194..5260
Further expansion into southern Europe is also possible , says Mr. Bell , the spokesman .	conjunction	5261..5347
While a large number of Hong Kong companies have reincorporated offshore ahead of 1997 , such a move is n't an option for Cathay because it would jeopardize its landing rights in Hong Kong .	norel	5350..5537
And Mr. Eddington emphatically rules out a move to London : " Our lifeblood is Hong Kong traffic rights . "	conjunction	5538..5641
He says the airline is putting its faith in the Sino-British agreement on Hong Kong 's return to China .	norel	5644..5746
A special section dealing with aviation rights states that landing rights for Hong Kong 's airlines , which include the smaller Hong Kong Dragon Airlines , will continue to be negotiated by Hong Kong 's government .	restatement	5747..5957
But critics fret that post-1997 officials ultimately will be responsible to Beijing .	contrast	5958..6042
" My feeling is { Cathay does n't } have a hope in the long run , " says an analyst , who declines to be identified .	norel	6045..6154
" Cathay would love to keep going , but the general sense is they 're going to have to do something . "	cause	6155..6253
Mr. Eddington acknowledges that the carrier will have to evolve and adapt to local changes , but he feels that the Sino-British agreement is firm ground to build on for the foreseeable future .	norel	6256..6447
" We 're confident that it protects our route structure , " he says , " and our ability to grow and prosper .	restatement	6448..6550
