If you think you have stress - related problems on the job , there 's good news and bad news .	ROOT	0
You 're probably right , and you are n't alone .	NONE	1
A new Gallup Poll study commissioned by the New York Business Group on Health , found that a full 25 % of the work force at companies may suffer from anxiety disorders or a stress - related illness , with about 13 % suffering from depression .	NONE	2
The study surveyed a national group of medical directors , personnel managers and employee assistance program directors about their perceptions of these problems in their companies .	COREF_PREV	3
It is one of a series of studies on health commissioned by the New York Business Group , a non-profit organization with about 300 members .	COREF_PREV	4
The stress study was undertaken because problems related to stress `` are much more prevalent than they seem , '' said Leon J. Warshaw , executive director of the business group .	COREF_PREV	5
In presenting the study late last week , Dr. Warshaw estimated the cost of these types of disorders to business is substantial .	COREF_PREV	6
Occupational disability related to anxiety , depression and stress costs about $ 8,000 a case in terms of worker 's compensation .	COREF_OTHER	7
In terms of days lost on the job , the study estimated that each affected employee loses about 16 work days a year because of stress , anxiety or depression .	COREF_PREV	8
He added that the cost for stress - related compensation claims is about twice the average for all injury claims .	COREF_OTHER	9
`` We hope to sensitize employers '' to recognize the problems so they can do something about them , Dr. Warshaw said .	COREF_PREV	10
Early intervention into these types of problems can apparently save businesses long - term expense associated with hospitalization , which sometimes results when these problems go untreated for too long .	COREF_PREV	11
Even the courts are beginning to recognize the link between jobs and stress - related disorders in compensation cases , according to a survey by the National Council on Compensation Insurance .	NONE	12
But although 56 % of the respondents in the study indicated that mental - health problems were fairly pervasive in the workplace , there is still a social stigma associated with people seeking help .	COREF_OTHER	13
The disorders , which 20 years ago struck middle - age and older people , `` now strike people at the height of productivity , '' says Robert M.A. Hirschfeld , of the National Institute of Mental Health , who spoke at the presentation of the study 's findings .	COREF_PREV	14
The poll showed that company size had a bearing on a manager 's view of the problem , with 65 % of those in companies of more than 15,000 employees saying stress - related problems were `` fairly pervasive '' and 55 % of those in companies with fewer than 4,000 employees agreeing .	COREF_PREV	15
The poll also noted fear of a takeover as a stress - producing event in larger companies .	COREF_PREV	16
More than eight in 10 respondents reported such a stress - provoking situation in their company .	NONE	17
Mid-sized companies were most affected by talk of layoffs or plant closings .	COREF_PREV	18
The study , which received funding from Upjohn Co. , which makes several drugs to treat stress - related illnesses , also found 47 % of the managers said stress , anxiety and depression contribute to decreased production .	COREF_OTHER	19
Alcohol and substance abuse as a result of stress - related problems was cited by 30 % of those polled .	NONE	20
Although Dr. Warshaw points out that stress and anxiety have their positive uses , `` stress perceived to be threatening implies a component of fear and anxiety that may contribute to burnout . ''	COREF_OTHER	21
He also noted that various work environments , such as night work , have their own `` stressors . ''	COREF_PREV	22
`` We all like stress , but there 's a limit , '' says Paul D'Arcy , of Rohrer , Hibler -AMP- Replogle , a corporate psychology and management consulting firm .	NONE	23
The problem , says Mr. D'Arcy , a psychologist , is that `` it 's very hard to get any hard measures on how stress affects job performance .	COREF_PREV	24
