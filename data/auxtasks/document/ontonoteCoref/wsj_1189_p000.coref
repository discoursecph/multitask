Savings and loans reject blacks for mortgage loans twice as often as they reject whites , the Office of Thrift Supervision said .	ROOT	0
But that does n't necessarily mean thrifts are discriminating against blacks , the agency said .	COREF_PREV	1
The office , an arm of the Treasury , said it does n't have data on the financial position of applicants and thus ca n't determine why blacks are rejected more often .	COREF_PREV	2
Nevertheless , on Capitol Hill , where the information was released yesterday at a Senate banking subcommittee hearing , lawmakers said they are worried that financial institutions are routinely discriminating against minorities .	NONE	3
They asked regulators to suggest new ways to force banks and thrifts to comply with anti-discrimination laws .	COREF_PREV	4
Sen. Alan Dixon ( D , Ill. ) , chairman of ,the subcommittee on consumer and regulatory affairs said , `` I 'm not a statistician .	NONE	5
But when blacks are getting their loan applications rejected twice as often as whites -- and in some cities , it is three and four times as often -- I conclude that discrimination is part of the problem . ''	COREF_PREV	6
James Grohl , a spokesman for ,the U.S. League of Savings Institutions said , `` The data is a red flag , but lacking the financial data you ca n't make a case that discrimination is widespread . ''	COREF_OTHER	7
The trade group official added : `` Certainly the federal government should take a hard look at it . ''	COREF_PREV	8
Sen. Dixon held the hearing to follow up on a provision in the savings and loan bailout bill that required regulators to report on evidence of discimination in mortgage lending .	COREF_OTHER	9
The legislation also requires broad new disclosures of the race , sex and income level of borrowers , but that information wo n't be gathered in new studies for several months at least .	COREF_PREV	10
The Federal Reserve said its studies in recent years , which adjust for income differences and other variables , showed that blacks received fewer home mortgages from banks and thrifts than whites .	NONE	11
But John LaWare , a ,Fed governor told the subcommittee the evidence is mixed and that the Fed 's believes the vast majority of banks are n't discriminating .	COREF_PREV	12
For instance , he noted , the Fed studies have shown that blacks receive more home improvement loans than whites .	COREF_PREV	13
Several lawmakers were angered that the bank and thrift regulators generally said they have been too busy handling the record number of bank and thrift failures in the past few years to put much energy into investigating possible discrimination .	NONE	14
`` We would be the first to admit that we have not devoted the necessary amount of emphasis over the past several years '' to developing examinations for discrimination , said Jonathan Fiechter , a top official of the Office of Thrift Supervision .	COREF_OTHER	15
`` If we 've got folks out there who are being turned away in the mortgage market improperly and unfairly , '' said Sen. Donald Riegle ( D. , Mich. ) , chairman of the banking committee , `` then that is a matter that needs remedy now , not six months from now , or six years from now , or 26 years from now . ''	NONE	16
Officials of the Federal Deposit Insurance Corp. and the Office of the Comptroller of the Currency said they have punished only a few banks for violations of anti-discrimination laws .	NONE	17
The FDIC said it has issued five citations to banks over the past three years for discriminatory practices .	COREF_PREV	18
The comptroller 's office said it found no indications of illegal discrimination in 3,437 examinations of banks since April 1987 .	COREF_OTHER	19
The comptroller 's office also said that of 37,000 complaints it received since January 1987 , only 16 alleged racial discrimination in real estate lending .	COREF_PREV	20
The agency investigated the complaints but no violations were cited .	COREF_PREV	21
Thrift regulators did n't give any figures on their enforcement actions .	NONE	22
Mr. Fiechter said that among the possibilities being considered by regulators to fight discrimination is the use of `` testers '' -- government investigators who would pose as home buyers .	COREF_OTHER	23
The Department of Housing and Urban Development has used testers to investigate discrimination in rental housing .	NONE	24
Using testers could be controversial with financial institutions , but Mr. Grohl said the U.S. League of Savings Institutions had n't yet taken any position on the matter .	COREF_OTHER	25
