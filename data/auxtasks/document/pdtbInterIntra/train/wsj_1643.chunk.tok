Row 21 of Section 9 of the Upper Reserved at Candlestick Park is a lofty perch , only a few steps from the very top of the stands .	root	9..138
From my orange seat , I looked over the first-base line and the new-mown ball field in the warm sun of the last few minutes before what was to have been the third game of the World Series .	entrel	139..326
It was five in the afternoon	norel	329..357
but that was Pacific time .	contrast	358..385
Back in New York the work day was already over	contrast	386..432
so I did n't have to feel guilty .	cause	433..466
Even still , I did feel self-indulgent	concession	467..504
and I could n't help remembering my father 's contempt for a rich medical colleague who would go to watch the Tigers on summer afternoons .	conjunction	505..642
This ballpark , the Stick , was not a classic baseball stadium -- too symmetrical , too much bald concrete .	norel	645..749
And it did n't have the crowded wild intimacy of Yankee Stadium .	conjunction	750..813
But I liked the easy friendliness of the people around me , liked it that they 'd brought their children , found it charming that , true citizens of the state of the future , they had brought so many TVs and radios to stay in touch with electroreality at a live event .	contrast	814..1077
Maybe it was their peculiar sense of history .	norel	1080..1125
The broadcasters were , after all , documenting the game , ratifying its occurrence for millions outside the Stick .	cause	1126..1238
Why not watch or hear your experience historicized	cause	1239..1289
while you were living it ?	synchrony	1290..1315
The day was saturated with the weight of its own impending history .	norel	1318..1385
Long lines of people waited to buy special souvenir World Series postcards with official postmarks .	cause	1386..1485
Thousands of us had paid $ 5 for the official souvenir book with its historical essays on Series trivia , its historical photographs of great moments in Series past , and its instructions , in English and Spanish , for filling in the scorecard .	conjunction	1486..1725
Pitcher = lanzador .	norel	1726..1743
Homerun = jonron .	norel	1744..1759
Players ran out on the field way below	norel	1762..1800
and the stands began to reverberate .	conjunction	1801..1838
It must be a local custom , I thought , stamping feet to welcome the team .	cause	1839..1911
But then the noise turned into a roar .	contrast	1912..1950
And no one was shouting .	conjunction	1951..1975
No one around me was saying anything .	conjunction	1976..2013
Because we all were busy riding a wave .	cause	2014..2053
Sixty thousand surfers atop a concrete wall , waiting for the wipeout .	restatement	2054..2123
Only at the moment of maximum roll did I grasp what was going on .	norel	2126..2191
Then I remembered the quake of '71 , which I experienced in Santa Barbara in a second-story motel room .	asynchronous	2192..2294
When the swaying of the building woke me up , I reasoned that a ) I was in Southern California ; b ) the bed was moving ; c ) it must be a Magic Fingers bed that had short-circuited .	restatement	2295..2471
Then I noticed the overhead light was swaying on its cord and realized what had happened .	asynchronous	2472..2561
What should I do ?	cause	2562..2579
Get out of the possibly collapsing building to the parking lot .	norel	2580..2643
But the lot might split into crevasses	contrast	2644..2682
so I had better stand on my car , which probably was wider than the average crevasse .	cause	2683..2768
Fortunately , the quake was over	contrast	2769..2800
before I managed to run out and stand naked on the hood .	asynchronous	2801..2857
At the Stick , while the world shook , I thought of that morning	norel	2860..2922
and then it struck me that this time was different .	conjunction	2923..2974
If I survived , I would have achieved every journalist 's highest wish .	restatement	2975..3044
I was an eyewitness of the most newsworthy event on the planet at that moment .	restatement	3045..3123
What was my angle ?	cause	3124..3142
How would I file ?	norel	3143..3160
All these thoughts raced through my head in the 15 seconds of the earthquake 's actual duration .	norel	3163..3258
The rest is , of course , history .	contrast	3259..3291
The Stick did n't fall .	cause	3292..3314
The real tragedies occurred elsewhere , as we soon found out .	contrast	3315..3375
But for a few minutes there , relief abounded .	contrast	3376..3421
A young mother found her boy , who had been out buying a hotdog .	instantiation	3422..3485
The wall behind me was slightly deformed	conjunction	3486..3526
but the center had held .	contrast	3527..3552
And most of us waited for a while for the game to start .	conjunction	3553..3609
Then we began to file out , to wait safely on terra firma for the opening pitch .	asynchronous	3610..3689
It was during the quiet exodus down the pristine concrete ramps of the Stick that I really understood the point of all those Walkmen and Watchmen .	norel	3692..3838
The crowd moved in clumps , clumps magnetized around an electronic nucleus .	cause	3839..3913
In this way , while the Stick itself was blacked out , we kept up to date on events .	cause	3914..3996
Within 15 minutes of the quake itself , I was able to see pictures of the collapsed section of the Bay Bridge .	instantiation	3997..4106
Increasingly accurate estimates of the severity of the quake became available	conjunction	4107..4184
before I got to my car .	asynchronous	4185..4208
And by then , expensive automobile sound systems were keeping the gridlocked parking lot by the bay informed about the fire causing the big black plume of smoke we saw on the northern horizon .	conjunction	4209..4400
Darkness fell .	norel	4403..4417
But the broadcasts continued through the blacked-out night , with pictures of the sandwiched highway ganglion in Oakland and firefighting in the Marina district .	contrast	4418..4578
By then , our little sand village of cars had been linked with a global village of listeners and viewers .	entrel	4579..4683
Everyone at the Stick that day had started out as a spectator and ended up as a participant .	norel	4686..4778
In fact , the entire population of the Bay Area had ended up with this dual role of actor and audience .	conjunction	4779..4881
The reporters were victims	cause	4882..4908
and some of the victims turned into unofficial reporters .	conjunction	4909..4966
The outstanding example of this was the motorist on the Bay Bridge who had the presence of mind to take out a video camera at the absolutely crucial moment and record the car in front as it fell into the gap in the roadway .	norel	4969..5192
The tape was on tv	entrel	5193..5211
before the night was out .	asynchronous	5212..5237
Marshall McLuhan , you should have been there at that hour .	conjunction	5238..5296
