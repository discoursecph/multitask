At a private dinner Thursday , Drexel Burnham Lambert Inc. chief executive Frederick Joseph delivered a sobering message about the junk bond market to officials of Prudential Insurance Co. of America .	(Elaboration(Elaboration(Elaboration(Elaboration(Elaboration(Elaboration
Mr. Joseph conceded	(Elaboration(Attribution(Attribution
the junk market was in disarray ,	Attribution)
according to people familiar with the discussion .	Attribution)
He said	(Attribution
Drexel	(Explanation(Background(Same-unit(Elaboration
-- the leading underwriter of high-risk junk bonds --	Elaboration)
could no longer afford to sell any junk offerings	Same-unit)
if they might later become troubled	Background)
because Drexel risked losing its highly lucrative junk franchise .	Explanation))))
The dinner was a stark confirmation	(Cause(Elaboration(Elaboration
that 1989 is the worst year ever for the $ 200 billion junk market .	Elaboration)
And investors and traders alike say	(Attribution
the current turmoil could take years to resolve .	Attribution))
Amid the market disorder , even Drexel ,	(Explanation(Same-unit(Elaboration
which has the widest and most loyal following of junk bond investors ,	Elaboration)
is pulling in its horns .	Same-unit)
Although the big investment bank still dominates the junk market ,	(Contrast
Drexel has been unable to stem the fallout from growing junk bond defaults , withdrawn new offerings , redemptions by shareholders in junk bond mutual funds and an exodus of once-devoted investors .	Contrast))))
For many money managers , the past four months have been humiliating .	(Elaboration(Elaboration
`` This is the worst shakeout ever in the junk market ,	(Explanation(Attribution(Joint
and it could take years before it 's over , ''	Joint)
says Mark Bachmann , a senior vice president at Standard & Poor 's Corp. , a credit rating company .	Attribution)
In the third quarter , for example , junk bonds	(Joint(Same-unit(Elaboration
-- those with less than an investment-grade rating --	Elaboration)
showed negative returns , the only major sector of the bond market	(Elaboration
to do so .	Elaboration))
Since the end of last year , junk bonds have been outperformed by all categories of investment-grade bonds ,	(Elaboration
including ultra-safe Treasury securities .	Elaboration))))
The junk market ,	(Elaboration(Same-unit(Elaboration
which mushroomed to $ 200 billion from less than $ 2 billion at the start of the decade ,	Elaboration)
has been declining for months	(Explanation
as issuers have stumbled under the weight of hefty interest payments .	Explanation))
The fragile market received its biggest jolt last month from Campeau Corp. ,	(Cause(Elaboration(Elaboration
which created its U.S. retailing empire with more than $ 3 billion in junk financing .	Elaboration)
Campeau developed a cash squeeze	(Elaboration
that caused it to be tardy on some interest payments and to put its prestigious Bloomingdale 's department store chain up for sale .	Elaboration))
At that point , the junk market went into a tailspin	(Explanation
as buyers disappeared	(Joint
and investors tried to sell .	Joint))))))
In an interview , Mr. Joseph says	(Elaboration(Elaboration(Attribution
his dinner discussion with the Prudential executives acknowledged problems for junk .	Attribution)
`` What I thought	(Attribution(Same-unit(Elaboration
I was saying	Elaboration)
is that the market is troubled but still viable and , appropriately enough , quite quality-conscious ,	(Elaboration
which is not at all bad , ''	Elaboration))
he says .	Attribution))
`` Nobody 's been perfect in their credit judgments the past couple years ,	(Background
and we 're going to make sure	(Attribution
our default rates are going to be in the acceptable parameters of the market . ''	Attribution))))
What has jolted many junk buyers is the sudden realization	(Elaboration(Elaboration(Elaboration(Elaboration(Explanation(Elaboration
that junk bonds can not necessarily be bought and sold with the ease of common stocks and many investment-grade bonds .	Elaboration)
Unlike the New York Stock Exchange ,	(Same-unit(Elaboration
where buyers and sellers are quickly matched ,	Elaboration)
the junk market ,	(Same-unit(Elaboration
where risky corporate loans are traded ,	Elaboration)
is sometimes closed for repairs .	Same-unit)))
At closely held Deltec Securities Corp. , junk bond money managers Amy K. Minella and Hannah H. Strasser say	(Cause(Elaboration(Elaboration(Elaboration(Elaboration(Attribution
the problems of the junk market go deeper than a temporary malaise .	Attribution)
In recent months ,	(Evaluation(Contrast(Same-unit(Attribution
they say ,	Attribution)
there has been heavy selling of junk bonds by some of the market 's traditional investors ,	Same-unit)
while new buyers have n't materialized	(Enablement
to replace them .	Enablement))
Wall Street securities firms , `` the primary source of liquidity for the high yield market , '' have been net sellers of junk bonds	(Attribution(Explanation
because of trading losses ,	Explanation)
Deltec said in a recent , grim report to customers .	Attribution)))
Mutual funds have also been net sellers of junk bonds	(Attribution(Explanation
as junk 's relatively poor performance and negative press coverage have produced `` above-normal '' redemptions by shareholders ,	Explanation)
Deltec said .	Attribution))
Investors ,	(Elaboration(Same-unit(Elaboration
trying to raise cash ,	Elaboration)
have sold `` large liquid issues '' such as RJR Holdings Corp. and Kroger Co. ;	Same-unit)
declines in these benchmark issues have contributed to the market 's distress .	Elaboration))
And ,	(Explanation(Same-unit(Attribution
Deltec said ,	Attribution)
buying has been severely reduced	Same-unit)
because savings and loans have been restricted in their junk purchases by recently passed congressional legislation .	(Elaboration
`` In fact , savings and loans were sellers of high yield holdings throughout the quarter , ''	(Attribution
Deltec said .	Attribution))))
Ms. Minella and Ms. Strasser say	(Attribution
they are managing their junk portfolios defensively ,	(Elaboration
building cash	(Joint
and selectively upgrading the overall quality .	Joint)))))
Meanwhile , Prudential , the nation 's largest insurer and the biggest investor in junk bonds , has seen the value of its junk bond portfolio drop to $ 6.5 billion from $ 7 billion since August	(Elaboration(Cause
because of falling junk prices .	Cause)
`` We certainly do have a lack of liquidity here ,	(Elaboration(Attribution(Elaboration
and it 's something	(Elaboration
to be concerned about , ''	Elaboration))
says James A. Gregoire , a managing director .	Attribution)
`` I have no reason	(Evaluation(Contrast(Elaboration
to think	(Attribution
things will get worse ,	Attribution))
but this market has a knack	(Elaboration
for surprising us .	Elaboration))
This market teaches us to be humble . ''	(Elaboration
The junk market 's `` yield hogs are learning a real painful lesson , ''	(Attribution
he says .	Attribution))))))
Although the majority of junk bonds outstanding show no signs of default ,	(Explanation(Attribution(Contrast
the market has downgraded many junk issues	(Manner-Means
as if they were in trouble ,	Manner-Means))
says Stuart Reese , manager of Aetna Life & Casualty Insurance Co. 's $ 17 billion investment-grade public bond portfolio .	Attribution)
`` But we think	(Elaboration(Attribution
the risks are there	(Elaboration
for things getting a lot worse .	Elaboration))
And the risks are n't appropriate for us , ''	(Explanation(Attribution
he says .	Attribution)
The big insurer , unlike Prudential , owns only about $ 150 million of publicly sold junk bonds .	Explanation))))
The string of big junk bond defaults ,	(Elaboration(Attribution(Same-unit(Elaboration
which have been a major cause of the market 's problems this year ,	Elaboration)
probably will persist ,	Same-unit)
some analysts say .	Attribution)
`` If anything , we 're going to see defaults increase	(Contrast(Elaboration(Attribution(Explanation
because credit ratings have declined , ''	Explanation)
says Paul Asquith , associate professor at the Massachusetts Institute of Technology 's Sloan School of Management .	Attribution)
Mr. Asquith ,	(Elaboration(Same-unit(Elaboration
whose study on junk bond defaults caused a furor on Wall Street	(Background
when it was disclosed last April ,	Background))
says	(Attribution
this year 's junk bond defaults already show a high correlation with his own findings .	Attribution))
His study showed	(Elaboration(Attribution
that junk bonds over time had a cumulative default rate of 34 % .	Attribution)
One indication of a growing number of junk defaults ,	(Elaboration(Same-unit(Attribution
Mr. Asquith says ,	Attribution)
is that about half of the $ 3 billion of corporate bonds outstanding	(Same-unit(Elaboration
that have been lowered to a default rating by S & P this year	Elaboration)
are junk bonds	(Elaboration
sold during the market 's big issue years of 1984 through 1986 .	Elaboration)))
These bonds ,	(Same-unit(Elaboration
now rated single-D ,	Elaboration)
include junk offerings by AP Industries , Columbia Savings	(Same-unit(Elaboration
( Colorado ) ,	Elaboration)
First Texas Savings Association , Gilbraltar Financial Corp. , Integrated Resources Inc. , Metropolitan Broadcasting Corp. , Resorts International Inc. , Southmark Corp. and Vyquest Inc .	Same-unit))))))
`` Obviously , we got a lot more smoke than fire from the people	(Elaboration(Elaboration(Attribution(Elaboration
who told us	(Attribution
the market was n't so risky , ''	Attribution))
says Bradford Cornell , professor of finance at University of California 's Anderson Graduate School of Management in Los Angeles .	Attribution)
Mr. Cornell has just completed a study	(Elaboration
that finds	(Attribution
that the risks and returns of junk bonds are less than on common stocks but more than on investment-grade bonds .	Attribution)))
Mr. Cornell says :	(Attribution
`` The junk market is no bonanza	(Contrast(Attribution
as Drexel claimed ,	Attribution)
but it also is n't a disaster	(Attribution
as the doomsayers say . ''	Attribution))))))))
Despite the junk market 's problems ,	(Evaluation(Contrast(Explanation(Contrast
Drexel continues to enjoy a loyalty among junk bond investors	(Elaboration
that its Wall Street rivals have n't found .	Elaboration))
During the past three weeks , for example , Drexel has sold $ 1.3 billion of new junk bonds for Turner Broadcasting Co. , Uniroyal Chemical , Continental Air and Duff & Phelps .	Explanation)
Still , the list of troubled Drexel bond offerings dwarfs that of any firm on Wall Street ,	(Elaboration(Comparison
as does its successful offerings .	Comparison)
Troubled Drexel-underwritten issues include Resorts International , Braniff , Integrated Resources , SCI TV , Gillette Holdings , Western Electric and Southmark .	Elaboration))
`` Quality junk bonds will continue to trade well , ''	(Contrast(Attribution
says Michael Holland , chairman of Salomon Brothers Asset Management Inc .	Attribution)
`` But the deals	(Same-unit(Elaboration
that never should have been brought	Elaboration)
have now become nuclear waste . ''	Same-unit))))
