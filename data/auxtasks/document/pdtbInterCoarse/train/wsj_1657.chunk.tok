When President Bush arrives here next week for a hemispheric summit organized to commemorate a century of Costa Rican democracy , will he be able to deliver a credible message in the wake of the Panamanian fiasco ?	O	9..221
Undoubtedly Mr. Bush will be praised by some Latin leaders prone to pay lip service to nonintervention , while they privately encourage more assertive U.S. action to remove Gen. Manuel Noriega and safeguard their countries from a Sandinista onslaught .	O	222..472
The Panamanian affair is only the tip of a more alarming iceberg .	B-expansion	475..540
It originates in a Bush administration decision not to antagonize the U.S. Congress and avoid , at all costs , being accused of meddling in the region .	I-expansion	541..690
The result has been a dangerous vacuum of U.S. leadership , which leaves Central America open to Soviet adventurism .	O	691..806
" The { influence of the } U.S. is not being felt in Central America ; Washington 's decisions do not respond to a policy , and are divorced from reality , " says Fernando Volio , a Costa Rican congressman and former foreign minister .	O	807..1032
The disarray of the Bush administration 's Latin diplomacy was evident in the failure of the Organization of American States to condemn categorically Gen. Noriega .	B-temporal	1035..1197
Faced with this embarrassment , U.S. diplomats expressed confidence that the influential Rio Group of South American nations , which gathered last week in Peru , would take a stronger posture toward the Panamanian dictator .	I-temporal	1198..1418
But other than a few slaps on the wrist , Gen. Noriega went unpunished by that body , too ; he was not even singled out in the closing statement .	O	1419..1561
Now Mr. Bush will come to Costa Rica and encounter Nicaraguan strongman Daniel Ortega , eager for photo opportunities with the U.S. president .	B-entrel	1564..1705
The host , Costa Rican President Oscar Arias , did not invite Chile , Cuba , Panama or Haiti to the summit , which was to be restricted to democracies .	B-comparison	1706..1852
However , Mr. Ortega was included .	I-comparison	1853..1886
Formally upgrading the Sandinistas to a democratic status was an initiative harshly criticized in the Costa Rican press .	O	1887..2007
Even Carlos Manuel Castillo -- the presidential candidate for Mr. Arias 's National Liberation Party -- made public his opposition to the presence of Nicaragua " in a democratic festivity . "	O	2008..2195
Nevertheless , the Bush administration agreed to the dubious arrangement in July , a few weeks before the Central American presidents met in Tela , Honduras , to discuss a timetable for disbanding the anti-Sandinista rebels .	O	2198..2418
According to officials in Washington , the State Department hoped that by pleasing President Arias , it would gain his support to postpone any decision on the Contras until after Mr. Ortega 's promises of democratic elections were tested next February .	B-comparison	2419..2668
However , relying on an ardent critic of the Reagan administration and the Contra movement for help in delaying the disarming of the Contras was risky business .	B-expansion	2669..2828
And even some last-minute phone calls that Mr. Bush made ( at the behest of some conservative U.S. senators ) to enlist backing for the U.S. position failed to stop the march of Mr. Arias 's agenda .	I-expansion	2829..3024
Prior to this episode , Sen. Christopher Dodd ( D. , Conn. ) , sensing an open field , undertook a personal diplomatic mission through Central America to promote an early disbanding of the rebels .	B-expansion	3027..3217
Visiting Nicaragua , he praised the Sandinistas for their electoral system and chided the Bush administration for not rewarding the Sandinistas .	I-expansion	3218..3361
In Honduras , where the Contras are a hot political issue , he promised to help unblock some $ 70 million in assistance withheld due to the failure of local agencies to comply with conditions agreed upon with Washington .	O	3362..3579
Aid was also the gist of the talks Sen. Dodd had with Salvadoran President Alfredo Cristiani ; Mr. Cristiani 's government is very much at the mercy of U.S. largess and is forced to listen very carefully to Sen. Dodd 's likes and dislikes .	O	3582..3818
It was therefore not surprising that close allies of the U.S. , virtually neglected by the Bush administration , ordered the Nicaraguan insurgents dismantled by December , long before the elections .	B-contingency	3819..4014
Fittingly , the Tela Accords were nicknamed by Hondurans " the Dodd plan . "	I-contingency	4015..4087
The individual foreign policy carried out by U.S. legislators adds to a confusing U.S. performance that has emboldened Soviet initiatives in Central America .	B-expansion	4090..4247
On Oct. 3 , following conversations with Secretary of State James Baker , Soviet Foreign Minister Eduard Shevardnadze arrived in Managua to acclaim " Nicaragua 's great peace efforts . "	B-entrel	4248..4428
There , Mr. Shevardnadze felt legitimized to unveil his own peace plan : The U.S.S.R. would prolong a suspension of arms shipments to Nicaragua after the February election if the U.S. did likewise with its allies in Central America .	B-expansion	4429..4659
He also called on Nicaragua 's neighbors to accept a " military equilibrium " guaranteed by both superpowers .	B-comparison	4660..4766
The Pentagon claims that in spite of Moscow 's words , East bloc weapons continue to flow into Nicaragua through Cuba at near-record levels .	I-comparison	4767..4905
Since Mr. Shevardnadze 's proposals followed discussions with Mr. Baker , speculations arose that the Bush administration was seeking an accommodation with the Soviets in Central America .	B-contingency	4908..5093
This scheme would fit the Arias Plan , which declared a false symmetry between Soviet military aid to the Sandinista dictatorship and that provided by Washington to freely elected governments .	I-contingency	5094..5285
Furthermore , it is also likely to encourage those on Capitol Hill asking for cuts in the assistance to El Salvador if President Cristiani does not bend to demands of the Marxist guerrillas .	O	5286..5475
The sad condition of U.S. policy in Central America is best depicted by the recent end to U.S. sponsorship of Radio Costa Rica .	B-expansion	5478..5605
In 1984 , the Costa Rican government requested help to establish a radio station in the northern part of the country , flooded by airwaves of Sandinista propaganda .	B-temporal	5606..5768
Recovering radiophonic sovereignty was the purpose of Radio Costa Rica , funded by the U.S. and affiliated with the Voice of America ( VOA ) .	I-temporal	5769..5907
A few months ago , the Bush administration decided to stop this cooperation , leaving Radio Costa Rica operating on a shoestring .	B-contingency	5910..6037
According to news reports , the abrupt termination was due to fears that VOA transmissions could interfere with the peace process .	I-contingency	6038..6167
In the meantime , Russia gave Nicaragua another powerful radio transmitter , which has been installed in the city of Esteli .	B-entrel	6168..6290
It is capable of reaching the entire Caribbean area and deep into North America .	B-contingency	6291..6371
Perhaps its loud signal may generate some awareness of the Soviet condominium being created in the isthmus thanks to U.S. default .	I-contingency	6372..6502
The Soviet entrenchment in Nicaragua is alarming for Costa Rica , a peaceful democracy without an army .	B-entrel	6505..6607
Questioned in Washington about what would happen if his much-heralded peace plan would fail , President Arias voiced expectations of direct U.S. action .	I-entrel	6608..6759
A poll conducted in July by a Gallup affiliate showed that 64 % of Costa Ricans believe that if their country is militarily attacked by either Nicaragua or Panama , the U.S. will come to its defense .	B-comparison	6760..6957
But in the light of events in Panama , where the U.S. has such clear strategic interests , waiting for the Delta Force may prove to be a dangerous gambit .	I-comparison	6958..7110
Mr. Daremblum is a lawyer and a columnist for La Nacion newspaper .	O	7113..7179
