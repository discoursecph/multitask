As Helen Boehm ,	(Summary-Evaluation-Topic(Summary-Evaluation-Topic(Summary-Evaluation-Topic(Temporal(Summary-Evaluation-Topic(Elaboration(Temporal(Temporal(Joint(Elaboration
who owns an art porcelain company ,	Elaboration)
sipped her luncheon cocktail ,	Joint)
she reeled off the names of a few pals	(Elaboration
-- Prince Charles , Princess Diana , Sarah Ferguson , John Kluge , Milton Petrie .	Elaboration))
Then ,	(Textual-organization
flashing a diamond ring as big as the Ritz	(Temporal(Summary-Evaluation-Topic
( `` my day diamond , darling '' ) ,	Summary-Evaluation-Topic)
she told her two companions	(Attribution
that she is on the `` board '' of the Vatican Museum in Rome .	Attribution))))
As it turns out ,	(Comparison(Cause
the board has a lot of important members ,	(Elaboration
including Winton Blount	(Joint(Elaboration
( former postmaster general of the U.S. ) ,	Elaboration)
Mrs. Henry Gaisman	(Joint(Elaboration
( widow of the inventor of auto-strop razor )	Elaboration)
and Vincent Murphy	(Elaboration
( an investment banker at Merrill Lynch & Co . )	Elaboration)))))
But Mrs. Boehm did n't mention any of them .	Comparison))
`` Helen Boehm has a way with names , ''	(Elaboration(Attribution
says James Revson , a gossip columnist for Newsday	(Elaboration
( and son of Joseph Revson , a founder of Revlon ) .	Elaboration))
Like which are droppable and which are not .	Elaboration))
With the fall social season well under way , name-droppers are out in force ,	(Comparison(Temporal
trying to impress their betters and sometimes put down their lessers .	Temporal)
But the truth is that almost everyone , from real-estate agents to city fathers , name-drops ;	(Elaboration
and a surprising number of people have an ancient uncle	(Elaboration
who claims he lived next door to the cartoonist	(Elaboration(Elaboration
who did the Katzenjammer Kids .	Elaboration)
( In case you have forgotten ,	(Cause
his name was Rudolph Dirks . )	Cause))))))
`` Name-dropping is pervasive	(Joint(Elaboration(Cause(Attribution(Temporal(Comparison
and getting more so	Comparison)
as society becomes more complex and alienating , ''	Temporal)
says Herbert Freudenberger , a New York psychoanalyst , with a high-powered clientele .	Attribution)
`` It can be an avenue of entrance to a certain sector of society. . . .	(Elaboration(Elaboration
It provides some people a needed sense of affiliation	(Joint
and can help open up a conversation with someone	(Elaboration
you do n't know . ''	Elaboration)))
Like the Long Island matron in the theater district the other day	(Elaboration
who swore to a stranger	(Elaboration(Attribution
that she once met Liza Minnelli .	Attribution)
`` I was having a drink in Sardi 's ,	(Elaboration(Temporal
when all of a sudden I saw a woman 's backside	(Elaboration(Elaboration
coming up the steps on the second floor	Elaboration)
and she was wearing sequined slacks .	Elaboration))
I knew	(Elaboration(Cause(Attribution
it was someone important ,	Attribution)
so I followed her into the ladies room	Cause)
and sure enough , it was Liza .	(Cause
So I said ,	(Summary-Evaluation-Topic(Summary-Evaluation-Topic(Attribution
`Hello. '	Attribution)
And she said ,	(Attribution
`Hello. '	Attribution))
Can you imagine ?	(Elaboration
Liza said hello to me . ''	Elaboration)))))))))
Some people must drop names	(Elaboration(Elaboration
-- call it an irresistible impulse .	Elaboration)
`` They ca n't help talking about the big important people	(Elaboration(Attribution(Temporal(Elaboration
they know ,	Elaboration)
even if they do n't really know them , ''	Temporal)
says Dr. Freudenberger .	Attribution)
Beauregard Houston-Montgomery , a New York writer	(Cause(Textual-organization(Elaboration
who changed his name from William Stretch in 1980 ,	Elaboration)
is an inveterate name-dropper .	Textual-organization)
`` I do it innately and pathologically ,	(Elaboration(Attribution(Joint
and	(Textual-organization
while it may occasionally get me into trouble ,	(Comparison
it 's also gotten me access to parties and society , ''	Comparison)))
he says .	Attribution)
Name-dropping recently helped Mr. Houston-Montgomery crash a party	(Elaboration
Fame magazine threw for 100 of the 2,809 people	(Cause(Elaboration
mentioned in the diaries of the late Andy Warhol .	Elaboration)
`` I guess	(Elaboration(Elaboration(Attribution(Comparison(Attribution
I might have asked Beauregard to leave ,	Attribution)
but he drops so many good names ,	(Temporal
we decided to let him stay , ''	Temporal))
says Steven Greenberg , publisher of Fame .	Attribution)
`` After all , Warhol was the ultimate namedropper ,	(Elaboration(Elaboration
dropping five a day in his diaries .	Elaboration)
And Beauregard was mentioned twice	(Elaboration
-- although very briefly and in passing . ''	Elaboration)))
Mr. Houston-Montgomery says	(Attribution
that at the party he waved to Malcolm Forbes , publisher of Forbes magazine	(Textual-organization(Summary-Evaluation-Topic
( `` We 've been in the columns together '' ) ,	Summary-Evaluation-Topic)
Mary Boone , a New York art dealer	(Textual-organization(Summary-Evaluation-Topic
( `` I think	(Comparison(Attribution
she knows me ,	Attribution)
but I 'm not sure `` )	Comparison))
and Bridget Fonda , the actress	(Summary-Evaluation-Topic
( `` She knows me ,	(Comparison
but we 're not really the best of friends '' ) .	Comparison)))))))))))))
Mr. Revson , the gossip columnist , says	(Elaboration(Attribution
there are people	(Elaboration
who actually plan whose names they are going to drop	(Temporal
before attending a party .	Temporal)))
These droppers do n't flaunt only their friendships with the Trumps , Brooke Astor or Georgette Mosbacher .	(Elaboration
`` They even drop semi-obscure names like Wolfgang Flottl ,	(Elaboration(Attribution(Elaboration
whom everybody these days apparently has heard of	(Comparison
but no one really knows , ''	Comparison))
says Mr. Revson .	Attribution)
`` It 's the one-upsmanship of name-dropping that counts . ''	(Comparison
But name-dropping has other benefits , often civic .	(Cause
In the name of civic pride and from the desire	(Elaboration(Textual-organization(Elaboration
to nullify a negative image ,	Elaboration)
some city promoters seek to link their municipality with the most recognizable names	(Elaboration
the city has to offer .	Elaboration))
Take Cleveland .	(Joint(Cause
It has gotten a bad rep	(Elaboration(Summary-Evaluation-Topic(Cause
because its once heavily polluted Cuyahoga River caught fire ,	(Joint
because former Mayor Ralph Perk set his hair on fire with an acetylene torch	(Joint
and because its proposed Rock 'n ' Roll Hall of Fame was recently refused an urban-development grant .	Joint)))
Some people call it `` The Mistake on the Lake ''	(Elaboration
-- Lake Erie , that is .	Elaboration))
`` It helps to point out how many important people came through Cleveland on their way to the top , ''	(Elaboration(Attribution
says George Miller , executive director of the New Cleveland Campaign , a nonprofit organization	(Elaboration
devoted to citing the city 's strengths .	Elaboration))
Mr. Miller notes	(Elaboration(Attribution
that actor Paul Newman 's family owned a sporting-goods store in Cleveland ,	(Joint
that the late actress Margaret Hamilton ,	(Joint(Textual-organization(Elaboration
who played the bad witch in `` The Wizard Of Oz , ''	Elaboration)
once ran a nursery school in Cleveland	Textual-organization)
and that comedian Bob Hope 's father , a stonemason , once worked on a church next to Severence Hall , Cleveland 's main concert hall .	Joint)))
`` Power names like that do n't hurt the city 's reputation , ''	(Attribution
Mr. Miller says .	Attribution)))))
In Hollywood , an average family can gain cachet	(Cause(Joint
from moving into a home	(Elaboration
vacated by the famous or near famous .	Elaboration))
`` Why we even just sold a three-bedroom house in Van Nuys	(Joint(Elaboration(Attribution(Joint
and were able to keep the price firm in a weak real-estate market	(Joint
by noting	(Attribution
that the original Lone Ranger lived there , ''	Attribution)))
says David Rambo , a sales associate with Jon Douglas Co. , a Los Angeles real-estate agency .	Attribution)
`` Most people ca n't even remember his name . ''	(Elaboration
( It is John Hart . )	Elaboration))
Mr. Rambo says	(Elaboration(Attribution
that a 3.2-acre property	(Cause(Textual-organization(Elaboration
overlooking the San Fernando Valley	Elaboration)
is priced at $ 4 million	Textual-organization)
because the late actor Erroll Flynn once lived there .	Cause))
`` If Flynn had n't lived there ,	(Attribution(Cause
the property might have been priced $ 1 million lower , ''	Cause)
says Mr. Rambo ,	(Elaboration
noting	(Attribution
that Flynn 's house has been bulldozed ,	(Cause
and only the swimming pool remains .	Cause))))))))))))))))
Press agents and public-relations practitioners are notorious name-droppers .	(Elaboration(Elaboration
And some even do it with malice aforethought .	Elaboration)
Len Kessler , a financial publicist in New York , sometimes uses it	(Cause(Cause
to get the attention of journalists	(Elaboration
who try to avoid him .	Elaboration))
He says	(Elaboration(Attribution
that	(Textual-organization
when Dan Dorfman , a financial columnist with USA Today , has n't returned his phone calls ,	(Temporal
he leaves messages with Mr. Dorfman 's office	(Elaboration
saying	(Attribution
that he has an important story on Donald Trump , Meshulam Riklis or Marvin Davis .	Attribution)))))
He admits	(Comparison(Attribution
he has no story on any of them on these occasions .	Attribution)
`` But it does get him to return my calls ,	(Attribution(Joint
and it makes me feel better for the times	(Elaboration
he 's given me the brushoff , ''	Elaboration))
Mr. Kessler says .	Attribution))))))
There are , of course , obvious dangers to blatant , unsubstantiated name-dropping .	(Elaboration(Cause
Jeffry Thal , a publicity agent for the Lantz Office in Los Angeles , warns	(Elaboration(Attribution
that dropping the wrong name labels the dropper as a fake and a fraud .	Attribution)
`` Get caught	(Elaboration(Attribution(Cause
and you 're dead in the water , ''	Cause)
says Mr. Thal .	Attribution)
Mr. Thal says	(Joint(Cause(Attribution
that Elizabeth Taylor , a client , `` hates being called `Liz. '. . .	Attribution)
If directors or producers phone me	(Cause(Cause(Joint
and say	(Attribution
they know `Liz , '	Attribution))
I know	(Attribution
they 've never met her .	Attribution))
She prefers `Elizabeth .	Cause))
' '' In New York society , Pat Buckley , the very social wife of author William Buckley , has the nicknames `` Mrs. Buckles '' and `` Patsy . ''	(Comparison(Elaboration
And her husband sometimes calls her `` Ducky . ''	Elaboration)
`` But call her `Patty , '	(Attribution(Cause(Cause
and it 's a sure giveaway	(Elaboration
you 're not in her circle ,	Elaboration))
because she does n't use that name , ''	Cause)
says Joan Kron , editor-in-chief of Avenue magazine , a monthly publication	(Elaboration
sent to all the right names .	Elaboration)))))))
John Spencer Churchill , a nephew of the late Sir Winston Churchill , former prime minister of Great Britain , is n't that impressed with most name-droppers	(Cause(Cause(Elaboration
he meets .	Elaboration)
That 's because they only drop `` mere names , ''	(Attribution
says Mr. Churchill .	Attribution))
Currently writing his memoirs ,	(Elaboration(Temporal
Mr. Churchill , an artist , tells how tycoons such as the late Jean Paul Getty , the oil billionnaire , were , in fact , known only by one initial , their last .	Temporal)
`` When you 're at the club ,	(Summary-Evaluation-Topic(Temporal
you ask	(Elaboration(Attribution
whether they 've spoken to `G . '	Attribution)
Now they know	(Comparison(Joint(Attribution
who you mean	Attribution)
and you know	(Attribution
who you mean .	Attribution))
But no one else does .	Comparison)))
Now that 's name-dropping ,	(Cause
if you know	(Attribution
what I mean . ''	Attribution)))))))
