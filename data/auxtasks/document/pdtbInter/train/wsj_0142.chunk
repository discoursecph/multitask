Despite a deluge of economic news, the Treasury market remained quiet but the corporate market was abuzz over International Business Machines Corp. 's huge debt offering.	O	9..179
"There were so many economic reports but the market didn't care about any of them," said Kathleen Camilli, a money market economist at Drexel Burnham Lambert Inc.	O	182..344
"So the focus turned to other fixed-income markets, corporate and mortgages in particular," she said.	O	345..446
IBM, the giant computer maker, offered $750 million of non-callable 30-year debentures priced to yield 8.47%, or about 1/2 percentage point higher than the yield on 30-year Treasury bonds.	O	449..637
The size of IBM's issue was increased from an originally planned $500 million as money managers and investors scrambled to buy the bonds.	O	640..777
In the investment-grade corporate market, "it's rare that you get an opportunity to buy a name that has such broad appeal and has such attractive call features," said James Ednie, a Drexel industrial bond trader.	O	780..992
Money managers ranked IBM's offering as the most significant investment-grade sale of the year because large issues of long-term debt by companies with triple-A credit are infrequent.	O	995..1178
Syndicate officials at lead underwriter Salomon Brothers Inc. said the debentures were snapped by up pension funds, banks, insurance companies and other institutional investors.	O	1179..1356
In the Treasury market, investors paid scant attention to the day's economic reports, which for the most part provided a mixed view of the economy.	O	1357..1504
"Whether you thought the economy was growing weak or holding steady, yesterday's economic indicators didn't change your opinion," said Charles Lieberman, a managing director at Manufacturers Hanover Securities Corp.	O	1507..1722
The government reported that orders for manufactured goods were essentially unchanged in September while construction spending was slightly lower	B-entrel	1725..1871
Both indicators were viewed as signs that the nation's industrial sector is growing very slowly, if at all.	I-entrel	1872..1979
A survey by the Federal Reserve's 12 district banks and the latest report by the National Association of Purchasing Management blurred that picture of the economy.	O	1982..2145
In a monthly report prepared for use at the Fed's next Federal Open Market Committee meeting on Nov. 14., the nation's central bank found that price increases have moderated and economic activity has grown at a sluggish pace in recent weeks.	O	2146..2387
Among other things, the survey found that manufacturing activity varied considerably across districts and among industries	B-instantiation	2390..2513
The Philadelphia and Cleveland districts, for example, reported declines in manufacturing activity while the Boston, Dallas and San Francisco banks noted that business expanded.	I-instantiation	2514..2691
The purchasing managers index of economic activity rose in October, although it remains below 50%.	O	2694..2792
A reading below 50% indicates that the manufacturing sector is slowing while a reading above 50% suggests that the industry is expanding.	O	2793..2930
Mr. Lieberman said the diverse showing in yesterday's reports "only enhances the importance of the employment data	B-entrel	2933..3049
The employment report, which at times has caused wide swings in bond prices, is due out tomorrow	B-entrel	3050..3147
The average estimate of 22 economists polled by Dow Jones Capital Markets Report was that non-farm payrolls expanded by 152,000 in October	B-conjunction	3148..3287
The economists forecast a 0.1% rise in the unemployment rate to 5.4%.	I-conjunction	3288..3357
Treasury Securities	O	3360..3379
In a surprise announcement, the Treasury said it will reopen the outstanding benchmark 30-year bond rather than create a new one for next week's quarterly refunding of the federal debt.	O	3382..3567
The Treasury will raise $10 billion in fresh cash by selling $30 billion of securities, including $10 billion of new three-year notes and $10 billion of new 10-year notes.	O	3570..3741
But rather than sell new 30-year bonds, the Treasury will issue $10 billion of 29year, nine-month bonds -- essentially increasing the size of the current benchmark 30-year bond that was sold at the previous refunding in August.	O	3744..3971
Credit market analysts said the decision to reopen the current benchmark, the 8 1/8% bond due August 2019, is unusual because the issue trades at a premium to its face amount	B-entrel	3974..4149
Some dealers said the Treasury's intent is to help government bond dealers gauge investor demand for the securities, given uncertainties about when the auction will occur.	I-entrel	4150..4321
The Treasury said the refunding is contingent upon congressional and presidential passage of an increase in the federal debt ceiling	B-contrast	4324..4457
Until such action takes places, the Treasury has no ability to issue new debt of any kind.	I-contrast	4458..4548
Meanwhile, Treasury bonds ended modestly higher in quiet trading	B-instantiation	4551..4616
The benchmark 30-year bond about 1/4 point, or $2.50 for each $1,000 face amount	B-conjunction	4617..4698
The benchmark was priced at 102 22/32 to yield 7.88% compared with 102 12/32 to yield 7.90% Tuesday	B-contrast	4699..4799
The latest 10-year notes were quoted at 100 22/32 to yield 7.88% compared with 100 16/32 to yield 7.90%.	I-contrast	4800..4904
The discount rate on three-month Treasury bills was essentially unchanged at 7.79%, while the rate on six-month bills was slightly lower at 7.52% compared with 7.60% Tuesday.	O	4907..5081
Corporate Issues	O	5084..5100
IBM's $750 million debenture offering dominated activity in the corporate debt market	B-synchrony	5103..5189
Meanwhile, most investment-grade bonds ended unchanged to as much as 1/8 point higher.	I-synchrony	5192..5278
In its latest compilation of performance statistics, Moody's Investors Service found that investment-grade bonds posted a total return of 2.7% in October while junk bonds showed a negative return of 1.5%.	O	5281..5485
Moody's said those returns compare with a 3.8% total return for longer-term Treasury notes and bonds	B-entrel	5486..5587
Total return measures price changes and interest income.	I-entrel	5588..5644
For the year to date, Moody's said total returns were topped by the 16.5% of longer-term Treasury issues, closely followed by 15% for investment-grade bonds.	O	5647..5804
Junk bonds trailed the group again.	O	5805..5840
"Even the 7.2% return from the risk-free three-month Treasury bill has easily outdistanced the 4.1% return from junk bonds," wrote Moody's economist John Lonski in yesterday's market report.	O	5841..6031
"Little wonder that buyers for junk have been found wanting," he said.	O	6032..6102
Moody's said the average net asset value of 24 junk-bond mutual funds fell by 4.2% in October.	O	6105..6199
Mortgage-Backed Issues	O	6202..6224
Mortgage securities ended slightly higher but trailed gains in the Treasury market.	O	6227..6310
Ginnie Mae's 9% issue for November delivery finished at 98 5/8, up 2/32, and its 9 1/2% issue at 100 22/32, also up 2/32.	O	6313..6434
The Ginnie Mae 9% securities were yielding 9.32% to a 12-year average life.	O	6437..6512
Activity was light in derivative markets, with no new issues priced.	O	6515..6583
Municipal Issues	O	6586..6602
Municipal bonds were mostly unchanged to up 1/8 point in light, cautious trading prior to tomorrow's unemployment report.	O	6605..6726
A $114 million issue of health facility revenue bonds from the California Health Facilities Financing Authority was temporarily withdrawn after being tentatively priced by a First Boston Corp. group.	O	6727..6926
An official for the lead underwriter declined to comment on the reason for the delay, but market participants speculated that a number of factors, including a lack of investor interest, were responsible	B-contrast	6929..7132
The issue could be relaunched, possibly in a restructured form, as early as next week, according to the lead underwriter.	I-contrast	7133..7254
A $107.03 million offering of Santa Ana Community Redevelopment Agency, Calif., tax allocation bonds got off to a slow start and may be repriced at lower levels today, according to an official with lead underwriter Donaldson Lufkin & Jenrette Securities Corp	B-entrel	7257..7516
The Santa Ana bonds were tentatively priced to yield from 6.40% in 1991 to 7.458% in	I-entrel	7517..7601
Bucking the market trend, an issue of $130 million general obligation distributable state aid bonds from Detroit, Mich., apparently drew solid investor interest	B-entrel	7604..7765
They were tentatively priced to yield from 6.20% in 1991 to 7.272% in	I-entrel	7766..7835
Foreign Bond	O	7838..7850
West German dealers said there was little interest in Treasury bonds ahead of Thursday's new government bond issue.	O	7853..7968
So far, they said, investors appear unenthusiastic about the new issue which might force the government to raise the coupon to more than 7%.	O	7971..8111
It is generally expected to be the usual 10-year, four billion mark issue	B-contrast	8112..8186
Rumors to the contrary have been that it would be a six billion mark issue, or that the last Bund, a 7% issue due October 1999, would be increased by two billion marks.	I-contrast	8187..8355
Elsewhere:	O	8358..8368
-- In Japan, the benchmark No. 111 4.6% issue due 1998 ended on brokers screens unchanged at 95.09 to yield 5.435%.	O	8371..8486
In Britain, the benchmark 11 3/4% bond due 2003/2007 fell 14/32 to 111 2/32 to yield 10.19%	B-entrel	8489..8584
The 12% notes due 1995 fell 9/32 to 103 3/8 to yield 11.10%.	I-entrel	8585..8645
