Nicaraguan President Daniel Ortega may have accomplished over the weekend what his U.S. antagonists have failed to do : revive a constituency for the Contra rebels .	root	9..172
Lawmakers have n't publicly raised the possibility of renewing military aid to the Contras , and President Bush parried the question at a news conference here Saturday , saying only that " if there 's an all-out military offensive , that 's going to change the equation 180 degrees . "	norel	175..451
But Mr. Ortega 's threat over the weekend to end a 19-month cease-fire with the rebels seeking to topple him , effectively elevated the Contras as a policy priority	contrast	454..616
just as they were slipping from the agendas of their most ardent supporters .	synchrony	617..693
Senate Majority Leader George Mitchell ( D. , Maine ) said yesterday on NBC-TV 's " Meet the Press " that Mr. Ortega 's threat was " a very unwise move , particularly the timing of it . "	norel	696..872
The threat came during a two-day celebration in Costa Rica to highlight Central America 's progress toward democracy in the region , attended by President Bush , Canadian Prime Minister Brian Mulroney and 14 other Western Hemisphere leaders .	norel	875..1113
Mr. Bush returned to Washington Saturday night .	asynchronous	1114..1161
Mr. Ortega announced on Friday that he would end the cease-fire this week in response to the periodic Contra assaults against his army .	norel	1164..1299
Saturday , he amended his remarks to say that he would continue to abide by the cease-fire if the U.S. ends its financial support for the Contras .	asynchronous	1300..1445
He asked that the remaining U.S. humanitarian aid be diverted to disarming and demobilizing the rebels .	conjunction	1446..1549
Not only did Mr. Ortega 's comments come in the midst of what was intended as a showcase for the region , it came as Nicaragua is under special international scrutiny in anticipation of its planned February elections .	norel	1552..1767
Outside observers are gathering in Nicaragua to monitor the registration and treatment of opposition candidates .	restatement	1768..1880
And important U.S. lawmakers must decide at the end of November if the Contras are to receive the rest of the $ 49 million in so-called humanitarian assistance under a bipartisan agreement reached with the Bush administration in March .	conjunction	1881..2115
The humanitarian assistance , which pays for supplies such as food and clothing for the rebels amassed along the Nicaraguan border with Honduras , replaced the military aid cut off by Congress in February 1988 .	norel	2118..2326
While few lawmakers anticipated that the humanitarian aid would be cut off next month , Mr. Ortega 's threat practically guarantees that the humanitarian aid will be continued .	norel	2327..2501
Senate Minority Leader Robert Dole ( R. , Kan . ) said yesterday on " Meet the Press " : " I would hope after his { Mr. Ortega 's } act yesterday or the day before , we 'd have unanimous support for quick action on remaining humanitarian aid . "	norel	2504..2734
Sen. Dole also said he hoped for unanimous support for a resolution he plans to offer tomorrow denouncing the Nicaraguan leader .	conjunction	2735..2863
While renewing military aid had been considered out of the question , rejected by Congress and de-emphasized by the Bush administration , Mr. Ortega 's statement provides Contra supporters with the opportunity to press the administration on the issue .	norel	2866..3114
" The administration should now state that if the { February } election is voided by the Sandinistas ... they should call for military aid , " said former Assistant Secretary of State Elliott Abrams .	norel	3117..3311
" In these circumstances , I think they 'd win . "	cause	3312..3357
Sen. Mitchell said that congressional Democrats intend to honor the March agreement to give non-lethal support to the Contras through the February elections , although he added that the agreement requires that the Contras not initiate any military action .	norel	3360..3614
Mr. Ortega 's threat to breach the cease-fire comes as U.S. officials were acknowledging that the Contras have at times violated it themselves .	norel	3617..3759
Secretary of State James Baker , who accompanied President Bush to Costa Rica , told reporters Friday : " I have no reason to deny reports that some Contras ambushed some Sandinista soldiers . "	instantiation	3760..3948
Mr. Baker 's assistant for inter-American affairs , Bernard Aronson , while maintaining that the Sandinistas had also broken the cease-fire , acknowledged : " It 's never very clear who starts what . "	norel	3951..4143
He added that the U.S. has cut off aid to some rebel units when it was determined that those units broke the cease-fire .	conjunction	4144..4264
In addition to undermining arguments in favor of ending Contra aid , Mr. Ortega 's remarks also played to the suspicions of some U.S. officials and conservatives outside the government that he is searching for ways to manipulate or void the February elections .	norel	4267..4525
Administration officials traveling with President Bush in Costa Rica interpreted Mr. Ortega 's wavering as a sign that he is n't responding to the military attacks so much as he is searching for ways to strengthen his hand prior to the elections .	norel	4528..4772
Mr. Abrams said that Mr. Ortega is seeking to demobilize the Contras prior to the elections to remove any pressure to hold fair elections .	instantiation	4773..4911
" My sense is what they have in mind is an excuse for clamping down on campaigning " by creating an atmosphere of a military emergency , he said .	norel	4914..5056
