Magazine publishers are facing spiraling costs and a glut of new titles .	B-concession	9..81
But even a raft of recent failures is n't stopping them from launching new publications .	I-concession	82..169
At the American Magazine Conference here , publishers are plenty worried about the industry 's woes .	B-contrast	172..270
At the American Magazine Conference here , publishers are plenty worried about the industry 's woes .	B-conjunction	172..270
But they are also talking about new magazines .	I-conjunction	271..317
For example , Toronto-based Telemedia Inc. will publish Eating Well , a new food and health magazine due out next summer .	B-conjunction	318..437
New York-based Hearst Corp. this fall plans to publish its first issue of 9 Months , a magazine for expectant mothers , and has already launched American Home .	B-conjunction	438..595
And Time Warner Inc. is developing a spinoff of Time magazine aimed at kids , on the heels of its successful Sports Illustrated for Kids .	I-conjunction	596..732
Over the past four years , the number of consumer magazines has increased by an average of 80 magazines annually , according to Donald Kummerfeld , president of the Magazine Publishers of America .	B-entrel	735..928
" This is an impressive show of faith in the future of the magazine industry , " said Mr. Kummerfeld .	I-entrel	929..1027
" Entrepreneurs do n't rush to get into a stagnant or declining industry . "	O	1028..1100
And despite the recent tough advertising climate , industry figures released at the meeting here indicate things may be turning around .	B-restatement	1103..1237
For the first nine months , advertising pages in consumer magazines tracked by the Publishers Information Bureau increased 4 % from the same period last year , to 125,849 pages .	B-conjunction	1238..1412
Total magazine ad revenue for the same period increased 12 % to $ 4.6 billion .	B-cause	1413..1489
Though for some magazines categories a tough advertising climate persists , the industry in general is doing well compared with the newspaper industry .	I-cause	1490..1640
Though some magazines are thriving , the magazine publishing industry remains a risky business .	B-instantiation	1643..1737
Within the same nine months , News Corp. closed down In Fashion , a once-promising young woman 's fashion magazine , Drake Publications Inc. has folded the long-troubled Venture magazine , and Lang Communications has announced Ms. magazine , after 17 years , will no longer carry advertising as of January .	I-instantiation	1738..2037
Lang is cutting costs and will attempt to operate the magazine with only subscription revenue .	B-synchrony	2038..2132
Meanwhile , American Health Partners , publisher of American Health magazine , is deep in debt , and Owen Lipstein , founder and managing partner , is being forced to sell the magazine to Reader 's Digest Association Inc .	B-conjunction	2135..2349
Mr. Lipstein 's absence from the meeting here raised speculation that the sale is in trouble .	B-contrast	2350..2442
Mr. Lipstein said in a telephone interview from New York that the sale was proceeding as planned .	I-contrast	2443..2540
" The magazine is strong .	O	2541..2565
It 's simply the right time to do what we are doing , " Mr. Lipstein said .	O	2566..2637
" Magazines can no longer be considered institutions , " said James Autry , president of Meredith Corp. 's magazine group .	O	2640..2758
" Publishers will find that some magazines have served their purpose and should die , " he added .	O	2759..2853
" Magazines could , like other brands , find that they have only a limited life . "	O	2854..2932
There are also indications that the number of magazine entrepreneurs , traditionally depended upon to break new ground with potentially risky start-ups , are dwindling .	B-instantiation	2935..3101
More than ever , independent magazines and small publishing groups are being gobbled up by larger publishing groups , such as American Express Publishing Corp. , a unit of American Express Co. , and Conde Nast Publications Inc. , a unit of Advance Publications Inc. , which are consolidating in order to gain leverage with advertisers .	I-instantiation	3102..3431
Some entrepreneurs are still active , though .	B-instantiation	3434..3478
Gerry Ritterman , president of New York-based Network Publishing Corp. , earlier this year sold his Soap Opera Digest magazine to Rupert Murdoch 's News Corp .	I-instantiation	3479..3634
Mr. Ritterman said that in the next six months he will take $ 50 million from the Soap Opera Digest sale to acquire new magazines .	B-concession	3635..3764
He would not reveal which magazines he is considering .	I-concession	3765..3819
" The magazines I am looking for are underdeveloped , " said Mr. Ritterman .	O	3822..3894
" They could be old or new , but they are magazines whose editorial quality needs to be improved .	B-asynchronous	3895..3990
They will be the next hot magazines .	I-asynchronous	3991..4027
