Measuring cups may soon be replaced by tablespoons in the laundry room .	(Elaboration
Procter & Gamble Co. plans to begin testing next month a superconcentrated detergent	(Elaboration(Background(Elaboration(Background(Elaboration
that will require only a few spoonfuls per washload .	Elaboration)
The move stems from lessons	(Background(Joint(Elaboration
learned in Japan	(Elaboration
where local competitors have had phenomenal success with concentrated soapsuds .	Elaboration))
It also marks P & G 's growing concern	(Elaboration
that its Japanese rivals , such as Kao Corp. , may bring their superconcentrates to the U.S .	Elaboration))
The Cincinnati consumer-products giant got clobbered two years ago in Japan	(Explanation(Background
when Kao introduced a powerful detergent ,	(Same-unit(Elaboration
called Attack ,	Elaboration)
which quickly won a 30 % stake in the Japanese markets .	Same-unit))
`` They do n't want to get caught again , ''	(Attribution
says one industry watcher .	Attribution))))
Retailers in Phoenix , Ariz. , say	(Elaboration(Attribution
P & G 's new powdered detergent	(Same-unit(Elaboration
-- to be called Cheer with Color Guard --	Elaboration)
will be on shelves in that market by early November .	Same-unit))
A P & G spokeswoman confirmed	(Elaboration(Attribution
that shipments to Phoenix started late last month .	Attribution)
She said	(Attribution
the company will study results from this market	(Background
before expanding to others .	Background)))))
Superconcentrates are n't entirely new for P & G .	(Elaboration(Elaboration(Explanation
The company introduced a superconcentrated Lemon Cheer in Japan	(Explanation
after watching the success of Attack .	Explanation))
When Attack hit the shelves in 1987 ,	(Elaboration(Cause
P & G 's share of the Japanese market fell to about 8 % from more than 20 % .	Cause)
With the help of Lemon Cheer , P & G 's share is now estimated to be 12 % .	Elaboration))
While the Japanese have embraced the compact packaging and convenience of concentrated products ,	(Elaboration(Elaboration(Contrast
the true test for P & G will be in the $ 4 billion U.S. detergent market ,	(Background
where growth is slow	(Joint
and liquids have gained prominence over powders .	Joint)))
The company may have chosen to market the product under the Cheer name	(Contrast(Explanation
since it 's already expanded its best-selling Tide into 16 different varieties ,	(Elaboration
including this year 's big hit , Tide with Bleach .	Elaboration))
With superconcentrates , however , it is n't always easy to persuade consumers that less is more ;	(Explanation
many people tend to dump too much detergent into the washing machine ,	(Explanation
believing	(Attribution
that it takes a cup of powder to really clean the laundry .	Attribution)))))
In the early 1980s , P & G tried to launch here a concentrated detergent under the Ariel brand name	(Elaboration(Contrast(Elaboration
that it markets in Europe .	Elaboration)
But the product ,	(Joint(Same-unit(Elaboration
which was n't as concentrated as the new Cheer ,	Elaboration)
bombed in a market test in Denver	Same-unit)
and was dropped .	Joint))
P & G and others also have tried repeatedly to hook consumers on detergent and fabric softener combinations in pouches ,	(Explanation(Contrast
but they have n't sold well ,	(Contrast
despite the convenience .	Contrast))
But P & G contends	(Joint(Attribution
the new Cheer is a unique formula	(Elaboration
that also offers an ingredient	(Elaboration
that prevents colors from fading .	Elaboration)))
And retailers are expected to embrace the product ,	(Explanation(Explanation
in part because it will take up less shelf space .	Explanation)
`` When shelf space was cheap ,	(Elaboration(Contrast(Attribution(Background
bigger was better , ''	Background)
says Hugh Zurkuhlen , an analyst at Salomon Bros .	Attribution)
But with so many brands	(Same-unit(Elaboration
vying for space ,	Elaboration)
that 's no longer the case .	Same-unit))
If the new Cheer sells well ,	(Attribution(Background(Condition
the trend toward smaller packaging is likely to accelerate	(Background
as competitors follow with their own superconcentrates .	Background))
Then retailers `` will probably push the { less-established } brands out altogether , ''	Background)
he says .	Attribution)))))))))
Competition is bound to get tougher	(Explanation(Explanation(Elaboration(Condition
if Kao introduces a product like Attack in the U.S .	Condition)
To be sure ,	(Evaluation
Kao would n't have an easy time	(Background
taking U.S. market share away from the mighty P & G ,	(Elaboration
which has about 23 % of the market .	Elaboration))))
Kao officials previously have said	(Attribution
they are interested in selling detergents in the U.S. ,	(Elaboration(Contrast
but so far the company has focused on acquisitions , such as last year 's purchase of Andrew Jergens Co. , a Cincinnati hand-lotion maker .	Contrast)
It also has a product-testing facility in California .	Elaboration)))
Some believe	(Elaboration(Attribution
P & G 's interest in a superconcentrated detergent goes beyond the concern for the Japanese .	Attribution)
`` This is something	(Attribution(Elaboration
P & G would do with or without Kao , ''	Elaboration)
says Mr. Zurkuhlen .	Attribution)))))
