Wanted :	(NS-Summary(NS-Elaboration
An investment	(NS-Elaboration
that 's as simple and secure as a certificate of deposit	(NN-Joint
but offers a return	(NS-Elaboration
worth getting excited about .	NS-Elaboration))))
With $ 150 billion of CDs	(NS-Elaboration(NS-Elaboration(NS-Elaboration(SN-Contrast(NS-Elaboration(NS-Explanation(SN-Background(SN-Background(NS-Elaboration
maturing this month ,	NS-Elaboration)
a lot of people have been scouring the financial landscape for just such an investment .	SN-Background)
In April ,	(SN-Background(SN-Contrast(SN-Background(NN-Same-unit(NS-Temporal
when many of them bought their CDs ,	NS-Temporal)
six-month certificates were yielding more than 9 % ;	NN-Same-unit)
investors	(NN-Same-unit(NS-Elaboration
willing to look	NS-Elaboration)
could find double-digit yields at some banks and thrifts .	NN-Same-unit))
Now , the nationwide average yield on a six-month CD is just under 8 % ,	(NS-Elaboration
and 8.5 % is about the best around .	NS-Elaboration))
But investors	(NN-Same-unit(NS-Elaboration
looking for alternatives	NS-Elaboration)
are n't finding it easy .	NN-Same-unit)))
Yields on most fixed-income securities are lower than several months ago .	(NN-Joint
And the stock market 's recent gyrations are a painful reminder of the dangers there .	NN-Joint))
`` If you 're looking for a significantly higher yield with the same level of risk as a CD ,	(NS-Attribution(SN-Condition
you 're not going to find it , ''	SN-Condition)
says Washington financial planner Dennis M. Gurtz .	NS-Attribution))
There are , however , some alternatives	(NS-Attribution(NS-Elaboration
that income-oriented investors should consider ,	NS-Elaboration)
investment advisers say .	NS-Attribution))
Short-term municipal bonds , bond funds and tax-deferred annuities are some of the choices	(NS-Elaboration(NS-Elaboration
they mention	NS-Elaboration)
-- and not just as a way	(NS-Elaboration
to get a higher return .	NS-Elaboration)))
In particular ,	(SN-Topic-Comment(NN-Same-unit(NS-Attribution
advisers say ,	NS-Attribution)
investors may want to look at securities	(NS-Elaboration
that reduce the risk	(NS-Elaboration(NS-Elaboration
that CD holders are confronting right now ,	NS-Elaboration)
of having to reinvest the proceeds of maturing short-term certificates at lower rates .	NS-Elaboration)))
A mix of CDs and other holdings may make the most sense .	(NS-Elaboration
`` People should remember	(NS-Attribution(SN-Attribution
their money is n't all or nothing	(NS-Summary
-- they do n't need to be shopping for the one interest-rate-type investment and putting all their money in it , ''	NS-Summary))
says Bethesda , Md. , adviser Karen Schaeffer .	NS-Attribution))))
Here 's a look at some of the alternatives :	(NS-Elaboration
SHORT-TERM MUNICIPALS :	(NN-Joint(NS-Elaboration
Investors with a heavy tax load should take out their calculators .	(SN-Background(NS-Elaboration
Yields on municipal bonds can be higher than after-tax yields on CDs for maturities of perhaps one to five years .	(NS-Elaboration(NS-Explanation
That 's because municipal-bond interest is exempt from federal income tax	(NS-Elaboration
-- and from state and local taxes too , for in-state investors .	NS-Elaboration))
For an investor	(NN-Same-unit(NS-Elaboration
paying tax at a 33 % rate ,	NS-Elaboration)
a seemingly puny 6 % yield on a one-year muni is equivalent to a taxable 9 % .	(NS-Comparison
Rates approach 6.5 % on five-year municipals .	NS-Comparison))))
Some of the more cautious CD holders might like `` pre-refunded '' municipals .	(NS-Elaboration(NS-Elaboration(NS-Elaboration
These securities get top credit ratings	(NS-Explanation
because the issuers have put aside U.S. bonds	(NS-Elaboration
that will be sold	(NS-Enablement
to pay off holders	(NS-Background
when the municipals are retired .	NS-Background)))))
`` It 's a no-brainer :	(NS-Attribution(NS-Elaboration
You do n't have to worry about diversification ;	(NN-Joint
you do n't have to worry about quality , ''	NN-Joint))
says Steven J. Hueglin , executive vice president of the New York bond firm of Gabriele , Hueglin & Cashman Inc .	NS-Attribution))
Consider a `` laddered '' bond portfolio , with issues	(NS-Elaboration(NS-Attribution(NS-Elaboration
maturing in , say , 1992 , 1993 and 1994 ,	NS-Elaboration)
advises Malcolm A. Makin , a Westerly , R.I. , financial planner .	NS-Attribution)
The idea is to have money rolling over each year at prevailing interest rates .	NS-Elaboration))))
BOND FUNDS :	(NN-Joint(NS-Elaboration
Bond mutual funds offer diversification	(NS-Elaboration(NS-Evaluation(NN-Joint
and are easy to buy and sell .	NN-Joint)
That makes them a reasonable option for investors	(NS-Elaboration(NS-Elaboration
who will accept some risk of price fluctuation	(NS-Enablement
in order to make a bet	(NS-Elaboration
that interest rates will decline over the next year or so .	NS-Elaboration)))
Buyers can look forward to double-digit annual returns	(NN-Contrast(NS-Condition
if they are right .	NS-Condition)
But they will have disappointing returns or even losses	(NS-Condition
if interest rates rise instead .	NS-Condition))))
Bond resale prices , and thus fund share prices , move in the opposite direction from rates .	(NS-Elaboration(NS-Elaboration(NS-Elaboration
The price movements get bigger	(NS-Background
as the maturity of the securities lengthens .	NS-Background))
Consider , for instance , two bond funds from Vanguard Group of Investment Cos .	(NS-Elaboration(NS-Elaboration
that were both yielding 8.6 % on a recent day .	NS-Elaboration)
The Short Term Bond Fund , with an average maturity of 2 1/2 years , would deliver a total return for one year of about 10.6 %	(NN-Contrast(NN-Same-unit(NS-Condition
if rates drop one percentage point	NS-Condition)
and a one-year return of about 6.6 %	(NS-Condition
if rates rise by the same amount .	NS-Condition))
But , in the same circumstances , the returns would be a more extreme 14.6 % and 2.6 % for the Vanguard Bond Market Fund , with its 12 1/2-year average maturity .	NN-Contrast)))
`` You get equity-like returns '' from bonds	(NS-Elaboration(NS-Attribution(NS-Condition
if you guess right on rates ,	NS-Condition)
says James E. Wilson , a Columbia , S.C. , planner .	NS-Attribution)
If interest rates do n't change ,	(SN-Condition
bond fund investors ' returns will be about equal to the funds ' current yields .	SN-Condition)))))
DEFERRED ANNUITIES :	(NN-Joint(NS-Elaboration
These insurance company contracts feature some of the same tax benefits and restrictions as non-deductible individual retirement accounts :	(NS-Elaboration(NS-Elaboration(NS-Elaboration
Investment gains are compounded	(NN-Contrast(NS-Temporal(NS-Background
without tax consequences	NS-Background)
until money is withdrawn ,	NS-Temporal)
but a 10 % penalty tax is imposed on withdrawals	(NS-Elaboration
made before age 59 1/2 .	NS-Elaboration)))
Aimed specifically at CD holders are so-called CD-type annuities , or certificates of annuity .	(NS-Explanation(NS-Elaboration
An interest rate is guaranteed for between one and seven years ,	(NS-Temporal
after which holders get 30 days	(NS-Enablement
to choose another guarantee period	(NS-Background(NN-Joint
or to switch to another insurer 's contract	NN-Joint)
without the surrender charges	(NS-Elaboration
that are common to annuities .	NS-Elaboration)))))
Some current rates exceed those on CDs .	(NS-Elaboration
For instance , a CD-type annuity from North American Co. for Life & Health Insurance , Chicago , offers 8.8 % interest for one year or a 9 % rate for two years .	NS-Elaboration)))
Annuities are rarely a good idea at age 35	(SN-Contrast(NS-Explanation
because of the withdrawal restrictions .	NS-Explanation)
But at age 55 , `` they may be a great deal , ''	(NS-Attribution
says Mr. Wilson , the Columbia , S.C. , planner .	NS-Attribution))))
MONEY MARKET FUNDS :	(NS-Elaboration
That 's right ,	(NS-Elaboration(SN-Evaluation
money market mutual funds .	SN-Evaluation)
The conventional wisdom is to go into money funds	(NS-Elaboration(NS-Elaboration(NS-Contrast(NS-Elaboration(NN-Contrast(NS-Background
when rates are rising	NS-Background)
and shift out at times such as the present ,	(NS-Background
when rates seem headed down .	NS-Background))
With average maturities of a month or so , money funds offer fixed share prices and floating returns	(NN-Same-unit(NS-Elaboration
that track market interest rates ,	NS-Elaboration)
with a slight lag .	NN-Same-unit))
Still , today 's highest-yielding money funds may beat CDs over the next year	(NS-Explanation(NS-Attribution(NS-Contrast
even if rates fall ,	NS-Contrast)
says Guy Witman , an editor of the Bond Market Advisor newsletter in Atlanta .	NS-Attribution)
That 's because top-yielding funds currently offer yields almost 1 1/2 percentage points above the average CD yield .	NS-Explanation))
Mr. Witman likes the Dreyfus Worldwide Dollar Money Market Fund , with a seven-day compound yield just under 9.5 % .	(NS-Elaboration
A new fund , its operating expenses are being temporarily subsidized by the sponsor .	NS-Elaboration))
Try combining a money fund and an intermediate-term bond fund as a low-risk bet on falling rates ,	(NS-Explanation(NS-Attribution
suggests Back Bay Advisors Inc. , a mutual fund unit of New England Insurance Co .	NS-Attribution)
If rates unexpectedly rise ,	(SN-Condition
the increasing return on the money fund will partly offset the lower-than-expected return from the bond fund .	SN-Condition)))))))))))
