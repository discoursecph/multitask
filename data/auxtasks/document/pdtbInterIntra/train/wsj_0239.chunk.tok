I had to reach back to French 101	root	9..42
when the monsieur avec clipboard leaned over my shoulder during the coffee phase of dinner and asked whether I wanted to ride in a montgolfiere .	synchrony	43..187
I was a last-minute ( read interloping ) attendee at a French journalism convention	norel	190..271
and so far the festivities had been taken up entirely by eating , drinking , smoking , sleeping and drinking .	conjunction	272..378
The man with the clipboard represented a halfhearted attempt to introduce a bit of les sportif into our itinerary .	entrel	379..493
But as the French embody a Zen-like state of blase when it comes to athletics ( try finding a Nautilus machine in Paris ) , my fellow conventioners were having none of it .	contrast	494..662
The diners at my table simply lit more Gauloises and scoffed at the suggestion of interrupting a perfectly good Saturday morning to go golfing or even montgolfing ( ballooning to you ; the brothers Montgolfier , French of course , were the world 's first hot-air balloonists ) .	alternative	663..934
Back in the U.S.A. this kind of chi-chi airborne activity wins heartwarmingly covetous responses .	norel	937..1034
As in : " You went ballooning ? ? ! !	restatement	1035..1066
In France ? ? ! ! "	norel	1067..1081
Americans it seems have followed Malcolm Forbes 's hot-air lead and taken to ballooning in a heady way .	cause	1082..1184
During the past 25 years , the number of balloonists ( those who have passed a Federal Aviation Authority lighter-than-air test ) have swelled from a couple hundred to several thousand , with some estimates running as high as 10,000 .	cause	1185..1414
Some 30 balloon shows are held annually in the U.S. , including the world 's largest convocation of ersatz Phineas Foggs -- the nine-day Albuquerque International Balloon Fiesta that attracts some 800,000 enthusiasts and more than 500 balloons , some of which are fetchingly shaped to resemble Carmen Miranda , Garfield or a 12-story-high condom .	conjunction	1415..1757
( The condom balloon was denied official entry status this year . )	concession	1758..1822
But in Epinal , a gray 16th-century river town adjacent to France 's Vosges mountain region , none of these Yankee-come-lately enthusiasms for things aloft was evident .	norel	1825..1990
Ballooning at the de rigueur hour of 6 a.m. held all the attraction for most people of sunrise root-canal work .	restatement	1991..2102
Feeling the naggings of a culture imperative , I promptly signed up .	contrast	2103..2170
The first thing anybody will tell you about ballooning is that it requires zip in the way of athletic prowess , or even a measure of derring-do .	norel	2173..2316
( So long as you do n't look down . )	condition	2317..2350
They will also tell you that even if you hate heights , you can still balloon .	norel	2351..2428
( I still say do n't look down .	norel	2429..2458
At least not when you are ascending . )	synchrony	2459..2496
What they wo n't tell you is not to go aloft in anything you do n't want to get wet .	norel	2497..2579
I 'm not referring to the traditional champagne drenching during the back-on-terra-firma toast .	norel	2580..2674
I 'm talking about landing in a canal .	restatement	2675..2712
In a porous wicker basket .	list	2713..2739
With a pilot who speaks no English .	list	2740..2775
To wit , my maiden voyage ( and novitiates are referred to as virgins ) began at dawn on a dew-sodden fairway and ended at noon in a soggy field .	norel	2778..2920
( Balloon flights almost always occur at dawn or dusk , when the winds are lightest . )	entrel	2921..3004
In between came lots of coffee drinking while watching the balloons inflate and lots of standing around deciding who would fly in what balloon and in what order ( the baskets hold no more than four passengers ) .	entrel	3005..3214
When it was n't my turn in the balloon I followed its progress from the " chase car , " listening to the driver holler into a walkie-talkie .	conjunction	3215..3351
After long stretches of this attendant ground activity came 20 or so lovely minutes of drifting above the Vosges watching the silver mists rise off the river and the French cows amble about the fields .	asynchronous	3352..3553
It 's hard not to feel that God 's in his heaven with this kind of bird's-eye view of the world	conjunction	3554..3647
even if your pilote in silly plaid beret kept pointing out how " belle " it all was .	concession	3648..3731
Eventually little French farmers and their little French farmwives came out of their stone houses and put their hands above their tiny eyes and squinted at us .	norel	3732..3891
No wonder .	norel	3894..3904
We were coming down straight into their canal .	cause	3905..3951
See , the other rule of thumb about ballooning is that you ca n't steer .	cause	3952..4022
And neither can your pilot .	conjunction	4023..4050
You can go only up or down ( by heating the balloon 's air with a propane burner , which does make the top of your head feel hot ) and ride the air currents .	restatement	4051..4204
Which makes the chase car necessary .	cause	4205..4241
Most balloonists seldom go higher than 2,000 feet	entrel	4242..4291
and most average a leisurely 5-10 miles an hour .	conjunction	4292..4340
When the balloon is cruising along at a steady altitude there is little sense of motion .	cause	4341..4429
Only when one is ascending -- or in our case descending a tad trop rapidement -- does one feel , well , airborne in a picnic basket .	contrast	4430..4560
" What 's he doing ? " hissed my companion , who was the only other English-speaking member of the convention and whose knuckles were white .	norel	4563..4698
" Attention , " yelled our pilot as our basket plunged into the canal .	asynchronous	4699..4766
" You bet attention , " I yelled back , leaping atop the propane tanks , " I 'm wearing alligator loafers ! "	asynchronous	4767..4867
Our pilot simply laughed , fired up the burner and with another blast of flame lifted us , oh , a good 12-inches above the water level .	asynchronous	4868..5000
We scuttled along for a few feet	asynchronous	5001..5033
before he plunged us into the drink again .	asynchronous	5034..5076
Eventually we came to rest in a soggy patch of field where we had the exquisite pleasure of scrambling out of the basket into the mud while the French half of our ballooning tag team scrambled in .	norel	5079..5275
I looked at my watch .	asynchronous	5276..5297
Barely half-an-hour aloft .	entrel	5298..5324
Back in the chase car , we drove around some more , got stuck in a ditch , enlisted the aid of a local farmer to get out the trailer hitch and pull us out of the ditch .	entrel	5325..5490
We finally rendezvoused with our balloon , which had come to rest on a dirt road amid a clutch of Epinalers who watched us disassemble our craft -- another half-an-hour of non-flight activity -- that included the precision routine of yanking the balloon to the ground , punching all the air out of it , rolling it up and cramming it and the basket into the trailer .	asynchronous	5491..5853
It was the most exercise we 'd had all morning	conjunction	5854..5899
and it was followed by our driving immediately to the nearest watering hole .	conjunction	5900..5976
This meant returning to the golf course , where we watched a few French duffers maul the first tee while we sat under Cinzano umbrellas , me nursing an espresso and my ego .	norel	5979..6149
A whole morning of ballooning and I had been off the ground barely 30 minutes .	cause	6150..6228
Still , I figured the event 's envy-quotient back in the U.S.A. was near peerless .	concession	6229..6309
As for the ride back to camp , our pilot and all the other French-speaking passengers clambered into the chase car .	norel	6312..6426
My American companion and I were left to ride alfresco in the wicker basket .	contrast	6427..6503
As we streaked by a blase gendarme , I could n't resist rearing up on my soggy loafers and saluting .	cause	6504..6602
Ms. de Vries is a free-lance writer .	norel	6605..6641
