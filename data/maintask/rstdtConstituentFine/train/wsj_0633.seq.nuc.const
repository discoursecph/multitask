These are the last words	(NN-Topic-Drift(NS-elaboration-additional(NS-restatement(NS-consequence(SN-background(NS-elaboration-additional(SN-antithesis(NS-elaboration-general-specific(NS-elaboration-additional(NS-temporal-before(NS-elaboration-object-attribute
Abbie Hoffman ever uttered , more or less ,	NS-elaboration-object-attribute)
before he killed himself .	NS-temporal-before)
And You Are There , sort of :	NS-elaboration-additional)
ABBIE :	(NN-Sequence(NS-elaboration-additional(NS-elaboration-additional(NS-elaboration-additional(SN-attribution
`` I 'm OK , Jack .	(NS-restatement
I 'm OK . ''	NS-restatement))
( listening )	(SN-circumstance
`` Yeah .	SN-circumstance))
I 'm out of bed .	(NS-elaboration-additional(NS-restatement(NS-evidence
I got my feet on the floor .	NS-evidence)
Yeah .	NS-restatement)
Two feet .	NS-elaboration-additional))
I 'll see you Wednesday ?	(NN-Question-Answer
. . . Thursday . ''	NN-Question-Answer))
He listens impassively .	(NN-Sequence
ABBIE	(NN-Sequence(SN-attribution(NS-elaboration-additional
( cont'd. ) :	NS-elaboration-additional)
`` I 'll always be with you , Jack .	(NS-elaboration-additional
Do n't worry . ''	NS-elaboration-additional))
Abbie lies back	(NN-List
and leaves the frame empty .	NN-List)))))
Of course that was n't the actual conversation	(NS-elaboration-object-attribute
the late anti-war activist , protest leader and founder of the Yippies ever had with his brother .	NS-elaboration-object-attribute))
It 's a script	(NS-elaboration-object-attribute
pieced together from interviews by CBS News for a re-enactment , a dramatic rendering by an actor of Mr. Hoffman 's ultimately unsuccessful struggle with depression .	NS-elaboration-object-attribute))
The segment is soon to be broadcast on the CBS News series `` Saturday Night With Connie Chung , ''	SN-background)
thus further blurring the distinction between fiction and reality in TV news .	NS-consequence)
It is the New Journalism come to television .	NS-restatement)
Ms. Chung 's program is just one of several network shows	(NS-elaboration-additional(NS-elaboration-additional(NS-elaboration-object-attribute(NS-elaboration-additional
( and many more in syndication )	NS-elaboration-additional)
that rely on the controversial technique	(NS-elaboration-object-attribute
of reconstructing events ,	(NS-means
using actors	(NS-elaboration-object-attribute
who are supposed to resemble real people , living and dead .	NS-elaboration-object-attribute))))
Ms. Chung 's , however , is said to be the only network news program in history	(NS-elaboration-object-attribute
to employ casting directors .	NS-elaboration-object-attribute))
Abbie Hoffman in this case is to be played by Hollywood actor Paul Lieber ,	(NS-elaboration-additional(NS-elaboration-additional
who is n't new to the character .	NS-elaboration-additional)
He was Mr. Hoffman in a 1979 Los Angeles production of a play	(NS-elaboration-object-attribute
called `` The Chicago Conspiracy Trial . ''	NS-elaboration-object-attribute))))
Television news , of course , has always been part show-biz .	(NN-Contrast(NS-elaboration-additional(NS-interpretation(NS-comparison(NS-example(SN-background(NS-explanation-argumentative
Broadcasters have a healthy appreciation of the role	(NS-elaboration-object-attribute
entertainment values play in captivating an audience .	NS-elaboration-object-attribute))
But ,	(NN-Same-Unit(NS-attribution
as CBS Broadcast Group president Howard Stringer puts it ,	NS-attribution)
the network now needs to `` broaden the horizons of nonfiction television ,	(NS-elaboration-additional
and that includes some experimentation . ''	NS-elaboration-additional)))
Since its premiere Sept. 16 , the show	(NN-List(NN-Same-Unit(NS-elaboration-object-attribute
on which Ms. Chung appears	NS-elaboration-object-attribute)
has used an actor to portray the Rev. Vernon Johns , a civil-rights leader , and one to play a teenage drug dealer .	NN-Same-Unit)
It has depicted the bombing of Pan Am flight 103 over the Scottish town of Lockerbie .	(NN-List
On Oct. 21 , it did a rendition of the kidnapping and imprisonment of Associated Press correspondent Terry Anderson ,	(NS-elaboration-additional(NS-elaboration-object-attribute
who was abducted in March 1985	(NN-List
and is believed to be held in Lebanon .	NN-List))
The production had actors	(NS-elaboration-object-attribute
playing Mr. Anderson and former hostages David Jacobsen , the Rev. Benjamin Weir and Father Lawrence Jenco .	NS-elaboration-object-attribute)))))
ABC News has similarly branched out into entertainment gimmickry .	(NN-Comparison(NS-elaboration-additional(NS-example
`` Prime Time Live , '' a new show this season	(NN-Same-Unit(NS-elaboration-object-attribute
featuring Sam Donaldson and Diane Sawyer ,	NS-elaboration-object-attribute)
has a studio audience	(NS-elaboration-object-attribute
that applauds	(NN-List
and that one night	(NN-Same-Unit
( to the embarrassment of the network )	(SN-consequence
waved at the camera like the crowd on `` Let 's Make a Deal . ''	SN-consequence))))))
( ABC stops short of using an `` applause '' sign and a comic	(NS-elaboration-additional(NS-purpose
to warm up the audience .	NS-purpose)
The stars do that themselves . )	NS-elaboration-additional))
NBC News has produced three episodes of an occasional series	(NS-elaboration-object-attribute(NS-elaboration-additional(NS-elaboration-object-attribute(NS-elaboration-object-attribute
produced by Sid Feders	NS-elaboration-object-attribute)
called `` Yesterday , Today and Tomorrow , ''	NS-elaboration-object-attribute)
starring Maria Shriver , Chuck Scarborough and Mary Alice Williams ,	NS-elaboration-additional)
that also gives work to actors .	NS-elaboration-object-attribute)))
Call it a fad .	(NN-Contrast
Or call it the wave of the future .	NN-Contrast))
NBC 's re-creations are produced by Cosgrove-Meurer Productions ,	(NS-elaboration-additional(NS-circumstance(SN-example(NS-elaboration-object-attribute
which also makes the successful prime-time NBC Entertainment series `` Unsolved Mysteries . ''	NS-elaboration-object-attribute)
The marriage of news and theater , if not exactly inevitable , has been consummated nonetheless .	SN-example)
News programs ,	(NN-Comparison(NS-explanation-argumentative(NN-Same-Unit
particularly if they score well in the ratings ,	(SN-condition
appeal to the networks ' cost-conscious corporate parents	SN-condition))
because they are so much less expensive to produce	(NS-elaboration-additional(NS-comparison
than an entertainment show is	NS-comparison)
-- somewhere between $ 400,000 and $ 500,000 for a one-hour program .	NS-elaboration-additional))
Entertainment shows tend to cost twice that .	NN-Comparison))
Re-enactments have been used successfully for several seasons on such syndicated `` tabloid TV '' shows as `` A Current Affair , ''	(NS-elaboration-additional(SN-evaluation(NS-elaboration-additional(NS-elaboration-additional
which is produced by the Fox Broadcasting Co. unit of Rupert Murdoch 's News Corp .	NS-elaboration-additional)
That show ,	(NN-Same-Unit(NS-elaboration-additional
whose host is Ms. Chung 's husband , Maury Povich ,	NS-elaboration-additional)
has a particular penchant for grisly murders and stories	(NS-elaboration-additional(NS-elaboration-object-attribute
having to do with sex	NS-elaboration-object-attribute)
-- the Robert Chambers murder case , the Rob Lowe tapes ,	(NS-comment
what have you .	NS-comment))))
Gerald Stone , the executive producer of `` A Current Affair , '' says ,	(SN-attribution
`` We have opened eyes to being a little less conservative and more imaginative in how to present the news . ''	SN-attribution))
Nowhere have eyes been opened wider than at CBS News .	(NS-elaboration-additional(NS-elaboration-additional(NS-example(NS-evidence
At 555 W. 57th St. in Manhattan , one floor below the offices of `` 60 Minutes , '' the most successful prime-time news program ever , actors wait in the reception area	(NN-List(NS-purpose
to audition for `` Saturday Night With Connie Chung . ''	NS-purpose)
CBS News sends scripts to agents ,	(NN-List(NS-elaboration-object-attribute
who pass them along to clients .	NS-elaboration-object-attribute)
The network deals a lot with unknowns ,	(NN-Contrast(NS-example
including Scott Wentworth ,	(NN-Same-Unit(NS-elaboration-object-attribute
who portrayed Mr. Anderson ,	NS-elaboration-object-attribute)
and Bill Alton as Father Jenco ,	NN-Same-Unit))
but the network has some big names to contend with , too .	(NS-example
James Earl Jones is cast to play the Rev. Mr. Johns .	(NN-List
Ned Beatty may portray former California Gov. Pat Brown in a forthcoming epsiode on Caryl Chessman , the last man	(NS-elaboration-object-attribute
to be executed in California , in 1960 .	NS-elaboration-object-attribute)))))))
`` Saturday Night '' has cast actors to appear in future stories	(NS-elaboration-general-specific
ranging from the abortion rights of teen-agers to a Nov. 4 segment on a man	(NS-elaboration-object-attribute
named Willie Bosket ,	(NS-elaboration-object-attribute
who calls himself a `` monster ''	(NN-List
and is reputed to be the toughest prisoner in New York .	NN-List)))))
CBS News ,	(NS-evaluation(NN-Same-Unit(NS-elaboration-additional
which as recently as two years ago fired hundreds of its employees in budget cutbacks ,	NS-elaboration-additional)
now hires featured actors	(NS-elaboration-object-attribute
beginning at $ 2,700 a week .	NS-elaboration-object-attribute))
That is n't much	(NN-Contrast(NS-comparison
compared with what Bill Cosby makes ,	(NN-List
or even Connie Chung for that matter	(NS-elaboration-additional
( who is paid $ 1.6 million a year	(NN-List
and who recently did a guest shot of her own on the sitcom `` Murphy Brown '' ) .	NN-List))))
But the money is n't peanuts either , particularly for a news program .	NN-Contrast)))
CBS News is also re-enacting the 1979 Three Mile Island nuclear accident in Middletown , Pa. , with something less than a cast of thousands .	(NS-elaboration-additional(NS-elaboration-additional
It is combing the town of 10,000 for about 200 extras .	(NS-elaboration-additional
On Oct. 20 , the town 's mayor , Robert Reid , made an announcement on behalf of CBS during half-time at the Middletown High School football game	(NN-Statement-Response(NS-elaboration-object-attribute
asking for volunteers .	NS-elaboration-object-attribute)
`` There was a roll of laughter through the stands , ''	(NS-attribution
says Joe Sukle , the editor of the weekly Press and Journal in Middletown .	NS-attribution))))
`` They 're filming right now at the bank down the street ,	(NS-elaboration-additional(NN-List
and they want shots of people	(NN-Same-Unit(NS-elaboration-object-attribute
getting out of cars	NS-elaboration-object-attribute)
and kids on skateboards .	NN-Same-Unit))
They are approaching everyone on the street	(NN-Question-Answer(NN-List
and asking	(SN-attribution
if they want to be in a docudrama . ''	SN-attribution))
Mr. Sukle says	(SN-attribution
he would n't dream of participating himself :	(NS-reason(NS-restatement
`` No way .	NS-restatement)
I think	(SN-attribution
re-enactments stink . ''	SN-attribution))))))))))
Though a re-enactment may have the flavor ,	(NS-elaboration-additional(NS-elaboration-additional(NS-evaluation(NS-consequence(NS-elaboration-additional(SN-evaluation
Hollywood on the Hudson it is n't .	SN-evaluation)
Some producers seem tentative about the technique , squeamish even .	NS-elaboration-additional)
So the results , while not news , are n't exactly theater either , at least not good theater .	NS-consequence)
And some people do think	(NS-example(SN-attribution
that acting out scripts is n't worthy of CBS News ,	(NS-elaboration-object-attribute
which once lent prestige to the network	(NN-List
and set standards for the industry .	NN-List)))
In his review of `` Saturday Night With Connie Chung , '' Tom Shales , the TV critic of the Washington Post and generally an admirer of CBS , wrote	(SN-attribution
that	(NN-Same-Unit
while the show is `` impressive , . . .	(SN-antithesis
one has to wonder	(SN-attribution
if this is the proper direction for a network news division	(NS-elaboration-object-attribute
to take . ''	NS-elaboration-object-attribute)))))))
Re-creating events has , in general , upset news traditionalists ,	(NS-example(NS-example
including former CBS News President Richard S. Salant and former NBC News President Reuven Frank , former CBS News anchorman Walter Cronkite and the new dean of the Columbia University Graduate School of Journalism , Joan Konner .	NS-example)
Says she :	(SN-attribution
`` Once you add dramatizations ,	(NS-elaboration-additional(SN-evaluation(NS-interpretation(SN-condition
it 's no longer news ,	SN-condition)
it 's drama ,	NS-interpretation)
and that has no place on a network news broadcast. . . .	SN-evaluation)
They should never be on .	(NS-restatement
Never . ''	NS-restatement)))))
Criticism of the Abbie Hoffman segment is particularly scathing among people	(NN-Contrast(NS-elaboration-additional(NS-example(NS-elaboration-object-attribute
who knew and loved the man .	NS-elaboration-object-attribute)
That includes his companion of 15 years , Johanna Lawrenson , as well as his former wife , Anita .	(NS-elaboration-additional(NS-elaboration-additional(NS-elaboration-additional
Both women say	(SN-attribution
they also find it distasteful	(SN-attribution
that CBS News is apparently concentrating on Mr. Hoffman 's problems as a manic-depressive .	SN-attribution)))
`` This is dangerous	(NS-elaboration-additional(NS-attribution(NN-List
and misrepresents Abbie 's life , ''	NN-List)
says Ms. Lawrenson ,	(NS-elaboration-object-attribute
who has had an advance look at the 36-page script .	NS-elaboration-object-attribute))
`` It 's a sensational piece about someone	(NS-elaboration-object-attribute
who is not here	(NS-purpose
to defend himself . ''	NS-purpose))))
Mrs. Hoffman says	(NS-elaboration-additional(SN-attribution
that dramatization `` makes the truth flexible .	SN-attribution)
It takes one person 's account	(NN-List
and gives it authenticity . ''	NN-List))))
CBS News interviewed Jack Hoffman and his sister , Phyllis , as well as Mr. Hoffman 's landlord in Solebury Township , Pa .	(NS-elaboration-additional
Also Jonathan Silvers ,	(NS-elaboration-additional(NS-elaboration-object-attribute
who collaborated with Mr. Hoffman on two books .	NS-elaboration-object-attribute)
Mr. Silvers says ,	(SN-attribution
`` I wanted to be interviewed	(NS-elaboration-additional(NS-purpose
to get Abbie 's story out ,	NS-purpose)
and maybe talking about the illness will do some good . ''	NS-elaboration-additional)))))
The executive producer of `` Saturday Night With Connie Chung , '' Andrew Lack , declines to discuss re-creactions as a practice or his show , in particular .	(NN-List(NS-elaboration-additional
`` I do n't talk about my work , ''	(NS-attribution
he says .	NS-attribution))
The president of CBS News , David W. Burke , did n't return numerous telephone calls .	(NN-List
One person close to the process says	(NN-List(NS-elaboration-additional(SN-attribution
it would not be in the best interest of CBS News to comment on a `` work in progress , '' such as the Hoffman re-creation ,	SN-attribution)
but says	(NS-antithesis(SN-attribution
CBS News is `` aware '' of the concerns of Ms. Lawrenson and Mr. Hoffman 's former wife .	SN-attribution)
Neither woman was invited by CBS News to participate in a round-table discussion about Mr. Hoffman	(NS-elaboration-object-attribute
that is to follow the re-enactment .	NS-elaboration-object-attribute)))
Mr. Lieber , the actor	(NS-explanation-argumentative(SN-attribution(NS-attribution(NS-elaboration-object-attribute
who plays Mr. Hoffman ,	NS-elaboration-object-attribute)
says	NS-attribution)
he was concerned at first	(SN-antithesis(SN-attribution
that the script would `` misrepresent an astute political mind , one	(NS-elaboration-object-attribute
that I admired , ''	NS-elaboration-object-attribute))
but that his concerns were allayed .	SN-antithesis))
The producers ,	(NN-Same-Unit(NS-attribution
he says ,	NS-attribution)
did a good job	(NS-elaboration-object-attribute
of depicting someone	(NS-elaboration-object-attribute
`` who had done so much ,	(NN-Contrast
but who was also a manic-depressive . ''	NN-Contrast))))))))))))
