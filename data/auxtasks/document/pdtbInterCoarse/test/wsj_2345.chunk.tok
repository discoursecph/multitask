Many of the nation 's highest-ranking executives saluted Friday 's market plunge as an overdue comeuppance for speculators and takeover players .	O	9..151
Assuming that the market does n't head into a bottomless free fall , some executives think Friday 's action could prove a harbinger of good news -- as a sign that the leveraged buy-out and takeover frenzy of recent years may be abating .	B-expansion	154..387
" This is a reaction to artificial LBO valuations , rather than to any fundamentals , " said John Young , chairman of Hewlett-Packard Co. , whose shares dropped $ 3.125 to $ 48.125 .	B-entrel	388..561
" If we get rid of a lot of that nonsense , it will be a big plus . "	I-entrel	562..627
A few of the executives here for the fall meeting of the Business Council , a group that meets to discuss national issues , were only too happy to personalize their criticism .	O	630..803
" People wish the government would do something about leveraged buy-outs , do something about takeovers , do something about Donald Trump , " said Rand Araskog , chairman of ITT Corp. , whose stock dropped $ 3.375 .	O	804..1010
" Where 's the leadership ?	O	1011..1035
Where 's the guy who can say : ' Enough is enough ' " ?	O	1036..1085
The executives were remarkably unperturbed by the plunge even though it lopped billions of dollars off the value of their companies -- and millions off their personal fortunes .	O	1088..1264
" I 'm not going to worry about one day 's decline , " said Kenneth Olsen , Digital Equipment Corp. president , who was leisurely strolling through the bright orange and yellow leaves of the mountains here after his company 's shares plunged $ 5.75 to close at $ 86.50 .	O	1267..1526
" I did n't bother calling anybody ; I did n't even turn on TV . "	O	1527..1587
" There has n't been any fundamental change in the economy , " added John Smale , whose Procter & Gamble Co. took an $ 8.75 slide to close at $ 120.75 .	O	1590..1734
" The fact that this happened two years ago and there was a recovery gives people some comfort that this wo n't be a problem . "	O	1735..1859
Of course , established corporate managements often tend to applaud the setbacks of stock speculators and takeover artists .	B-expansion	1862..1984
Indeed , one chief executive who was downright delighted by Friday 's events was Robert Crandall , chairman of AMR Corp. , the parent of American Airlines and the target of a takeover offer by Mr. Trump .	B-expansion	1985..2184
Asked whether Friday 's action could help him avoid being Trumped by the New York real estate magnate , Mr. Crandall smiled broadly and said : " No comment . "	I-expansion	2185..2338
On Friday morning , before the market 's sell-off , the business leaders issued a report predicting the economy would grow at roughly an inflation-adjusted 2 % annual rate , through next year , then accelerate anew in 1991 .	O	2341..2558
Of the 19 economists who worked on the Business Council forecast , only two projected periods of decline in the nation 's output over the next two years , and in " both instances the declines are too modest to warrant the phrase recession , " said Lewis Preston , chairman of J.P. Morgan & Co. and vice chairman of the Business Council .	O	2559..2888
