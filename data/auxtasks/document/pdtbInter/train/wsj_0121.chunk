Program trading is "a racket," complains Edward Egnuss, a White Plains, N.Y., investor and electronics sales executive, "and it's not to the benefit of the small investor, that's for sure."	O	9..198
But although he thinks that it is hurting him, he doubts it could be stopped.	O	199..276
Mr. Egnuss's dislike of program trading is echoed by many small investors interviewed by Wall Street Journal reporters across the country.	B-concession	279..417
But like Mr. Egnuss, few expect it to be halted entirely, and a surprising number doubt it should be.	I-concession	418..519
"I think program trading is basically unfair to the individual investor," says Leo Fields, a Dallas investor.	O	522..631
He notes that program traders have a commission cost advantage because of the quantity of their trades, that they have a smaller margin requirement than individual investors do and that they often can figure out earlier where the market is heading.	B-contrast	632..880
But he blames program trading for only some of the market's volatility.	B-conjunction	883..954
He also considers the market overvalued and cites the troubles in junk bonds.	I-conjunction	955..1032
He adds: "The market may be giving us another message, that a recession is looming."	O	1033..1117
Or, as Dorothy Arighi, an interior decorator in Arnold, Calif., puts it: "All kinds of funny things spook the market these days."	O	1120..1249
But she believes that "program trading creates deviant swings.	O	1250..1312
It's not a sound thing; there's no inherent virtue in it."	O	1313..1371
She adds that legislation curbing it would be "a darned good idea."	O	1372..1439
At the Charles Schwab & Co. office in Atlanta's Buckhead district, a group of investors voices skepticism that federal officials would curb program trading.	B-instantiation	1442..1598
Citing the October 1987 crash, Glenn Miller says, "It's like the last crash -- they threatened, but no one did anything.	B-conjunction	1599..1720
A. Donald Anderson, a 59-year-old Los Angeles investor who says the stock market's "fluctuations and gyrations give me the heebie-jeebies," doesn't see much point in outlawing program trading.	B-cause	1721..1913
At the Charles Schwab & Co. office in Atlanta's Buckhead district, a group of investors voices skepticism that federal officials would curb program trading.Citing the October 1987 crash, Glenn Miller says, "It's like the last crash -- they threatened, but no one did anything." A. Donald Anderson, a 59-year-old Los Angeles investor who says the stock market's "fluctuations and gyrations give me the heebie-jeebies," doesn't see much point in outlawing program trading.Those who still want to do it "will just find some way to get around" any attempt to curb it.	B-conjunction	1914..2007
Similarly, Rick Wamre, a 31-year-old asset manager for a Dallas real-estate firm, would like to see program trading disappear because "I can't see that it does anything for the market or the country."	I-conjunction	2010..2210
Yet he isn't in favor of new legislation.	O	2211..2252
"I think we've got enough securities laws," he says.	O	2253..2305
"I'd much rather see them dealing with interest rates and the deficit."	O	2306..2377
Peter Anthony, who runs an employment agency in New York, decries program trading as "limiting the game to a few," but he also isn't sure it should be more strictly regulated.	O	2380..2555
"I don't want to denounce it because denouncing it would be like denouncing capitalism," he explains.	O	2556..2657
And surprising numbers of small investors seem to be adapting to greater stock market volatility and say they can live with program trading.	O	2660..2800
Glenn Britta, a 25-year-old New York financial analyst who plays options for his personal account, says he is "factoring" the market's volatility "into investment decisions.	B-entrel	2803..2977
He adds that program trading "increases liquidity in the market.	I-entrel	2978..3042
You can't hold back technology."	O	3043..3075
And the practice shouldn't be stopped, he says, because "even big players aren't immune to the rigors of program trading."	O	3076..3198
Also in New York, Israel Silverman, an insurance-company lawyer, comments that program trading "increases volatility, but I don't think it should be banned.	O	3201..3357
There's no culprit here.	B-restatement	3358..3382
The market is just becoming more efficient."	I-restatement	3383..3427
Arbitraging on differences between spot and futures prices is an important part of many financial markets, he says.	O	3428..3543
He adds that his shares in a company savings plan are invested in a mutual fund, and volatility, on a given day, may hurt the fund.	O	3544..3675
But "I'm a long-term investor," he says.	O	3676..3716
"If you were a short-term investor, you might be more leery about program trading."	O	3717..3800
Jim Enzor of Atlanta defends program trading because he believes that it can bring the market back up after a plunge.	O	3803..3920
"If we have a real bad day, the program would say, `Buy, '" he explains.	O	3921..3993
"If you could get the rhythm of the program trading, you could take advantage of it."	O	3994..4079
What else can a small investor do?	O	4082..4116
Scott Taccetta, a Chicago accountant, is going into money-market funds.	O	4119..4190
Mr. Taccetta says he had just recouped the $5,000 he lost in the 1987 crash when he lost more money last Oct. 13.	B-cause	4191..4304
Now, he plans to sell all his stocks by the first quarter of 1990.	I-cause	4305..4371
In October, before the market dropped, Mrs. Arighi of Arnold, Calif., moved to sell the "speculative stocks" in her family trust "so we will be able to withstand all this flim-flammery" caused by program trading.	O	4374..4586
She believes that the only answer for individuals is to "buy stocks that'll weather any storm."	O	4587..4682
Lucille Gorman, an 84-year-old Chicago housewife, has become amazingly immune to stock-market jolts.	B-instantiation	4685..4785
Mrs. Gorman took advantage of low prices after the 1987 crash to buy stocks and has hunted for other bargains since the Oct. 13 plunge.	I-instantiation	4786..4921
"My stocks are all blue chips," she says.	O	4924..4965
If the market goes down, I figure it's paper profits I'm losing.	B-contrast	4966..5031
If the market goes down, I figure it's paper profits I'm losing.On the other hand, if it goes way sky high, I always sell.	B-restatement	5032..5090
You don't want to get yourself too upset about these things.	I-restatement	5091..5151
