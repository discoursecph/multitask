Japanese Firms Push Posh Car Showrooms	O	9..49
JAPANESE luxury-car makers are trying to set strict design standards for their dealerships .	B-comparison	52..143
But some dealers are negotiating looser terms , while others decline to deal at all .	I-comparison	144..227
Nissan Motor Co. 's Infiniti division likes to insist that every dealer construct and furnish a building in a Japanese style .	O	230..355
Specifications include a polished bronze sculpture at the center of each showroom and a tile bridge spanning a stream that flows into the building from outside .	O	356..516
" Infiniti has it down to the ashtrays , " says Jay Ferron , a partner at J.D. Power & Associates , an auto research firm .	O	519..636
Toyota Motor Corp. 's Lexus division also provides specifications .	B-comparison	639..705
But only two-thirds of Lexus dealers are constructing new buildings according to the Lexus specs .	B-expansion	706..803
Some are even coming up with their own novel designs .	B-expansion	804..857
In Louisville , Ky. , for example , David Peterson has built a Lexus dealership with the showroom on the second floor .	I-expansion	858..973
Yet some dealers have turned down Infiniti or Lexus franchises because they were unwilling or unable to meet the design requirements .	O	976..1109
Lee Seidman of Cleveland says Infiniti " was a bear on interiors " but at least let him retrofit an existing building -- without the stream .	O	1110..1248
Mr. Seidman says he turned down a Lexus franchise in part because " the building was gorgeous but very expensive . "	O	1249..1362
To head off arguments , Infiniti offers dealers cash bonuses and low-interest construction loans .	O	1365..1461
Dictation Device 's Saga Plays Back a Lesson	O	1464..1509
PRODUCTS DON'T have to be first to be winners .	B-expansion	1512..1558
That 's the lesson offered through one case study featured in a design exhibit .	I-expansion	1559..1637
Dictaphone Corp. was caught off guard in 1974 when its main competitor , Lanier Office Products of Japan , introduced a microcassette dictation recorder half the size of standard cassette devices .	B-contingency	1640..1834
Blocked by patent protection from following suit , Dictaphone decided to go a step further and cut the cassette in half again -- down to the length of a paperclip .	I-contingency	1835..1997
By 1979 , designers and engineers at Dictaphone , a Pitney Bowes subsidiary , had produced a working model of a " picocassette " recorder .	O	1998..2131
By 1982 , however , the patent status of the Lanier microcassette had changed , permitting Dictaphone to develop its own competitive micro system , which it did .	O	2134..2291
Marketing and sales departments then urged abandonment of the pico project .	B-comparison	2292..2367
But others said pico should proceed .	B-expansion	2368..2404
Both were right .	I-expansion	2405..2421
Dictaphone went ahead and introduced the pico in 1985 , but it has n't sold well .	O	2424..2503
To date , says Emil Jachmann , a Dictaphone vice president , it has " broken even or shown a small loss . "	O	2504..2605
Nevertheless , the device has been successful in other ways .	B-expansion	2608..2667
It helped Dictaphone attract better engineers , and it provided new technology for other company products .	B-expansion	2668..2773
The picocassette recorder also helped transform the company 's reputation from follower to leading-edge innovator .	I-expansion	2774..2887
" It gave me great pride to see the inventor of the microcassette in Japan look at the pico and shake his head and say ' unbelievable , ' " says Mr. Jachmann .	O	2890..3044
Dictaphone 's picocassette recorder is one of 13 case studies in the TRIAD Design Project , sponsored by the Design Management Institute of Boston and Harvard Business School .	B-entrel	3047..3220
The studies are on exhibit at Harvard this month and will travel to Chicago 's Institute of Design and the University of California at Berkeley .	I-entrel	3221..3364
A Rake 's Progress Means Branching Out	O	3367..3406
ONE DAY Carl Barrett of Mobile , Ala. , was raking some sycamore leaves , but the rake kept riding up over the piles .	B-entrel	3409..3523
The harder he tried to push them into large piles , the closer he came to breaking the rake and straining his back .	B-contingency	3524..3638
So Mr. Barrett , then vice president of the Alabama Steamship Association , took a steel-toothed garden rake and taped it to the underside of a leaf rake about nine inches up .	I-contingency	3641..3814
His crude device worked : The lower teeth gathered the leaves into a pile , while the higher , harder teeth moved the top of the pile .	B-expansion	3815..3946
Now incorporated into a polypropylene rake , the four-inch prongs , or " wonderbars , " also are supposed to aid in picking up leaves .	I-expansion	3947..4076
One customer , Donald Blaggs of Mobile , says the Barrett Rake allowed him to do his lawn in 2 1/2 hours , two hours less than usual .	B-comparison	4079..4209
But other rake makers have their doubts .	B-expansion	4212..4252
Richard Mason , president of Ames Co. in Parkersburg , W. Va. , says the Barrett rake " makes sense , " but it would be " tough " to explain to consumers .	I-expansion	4253..4399
John Stoner , marketing director for True Temper Corp. , a subsidiary of Black & Decker , says people do n't want to move a leaf pile .	O	4402..4532
" They either pick it up , " he says , " or they start pulling from a fresh direction . "	O	4533..4615
Odds and Ends	O	4618..4631
NO MORE STUBBED toes or bruised shins , promises Geste Corp. of Goshen , Ind. , the designer of a bed support to replace traditional frames .	B-contingency	4632..4769
Four tubular steel " Bedfellows , " each roughly in the shape of a " W , " are attached to the bottom of the box spring in a recessed position ... .	I-contingency	4770..4911
Nearly half of U.S. consumers say they 'll pay up to 5 % more for packaging that can be recycled or is biodegradable , according to a survey commissioned by the Michael Peters Group , a design consultant .	O	4912..5112
