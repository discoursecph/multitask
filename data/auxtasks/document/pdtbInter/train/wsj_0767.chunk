The Federal Communications Commission allowed American Telephone & Telegraph Co. to continue offering discount phone services for large-business customers and said it would soon re-examine its regulation of the long-distance market.	O	9..241
The FCC moves were good news for AT&T, which has been striving since the breakup of the phone system for greater latitude in pricing and reduced regulation.	B-conjunction	244..400
Alfred Sikes, the new FCC chairman, championed deregulation of AT&T at his last job as head of a Commerce Department telecommunications agency.	I-conjunction	401..544
But it has been an open question whether Mr. Sikes, an extraordinarily cautious man, would continue pushing deregulation at the FCC in the face of what is likely to be great political pressure.	O	545..738
It means that Sikes is serious about the deregulation of long distance," said Jack Grubman, a telecommunications analyst at PaineWebber Inc., who attended the FCC meeting.	B-entrel	741..913
"All the commissioners were in amazing agreement {to re-examine regulation} for only having been together for a few months."	I-entrel	914..1038
The FCC took three specific actions regarding AT&T.	O	1041..1092
By a 4-0 vote, it allowed AT&T to continue offering special discount packages to big customers, called Tariff 12, rejecting appeals by AT&T competitors that the discounts were illegal.	O	1093..1277
Then by a separate 4-0 vote, it chose the narrowest possible grounds to strike down a different discount plan, called Tariff 15, that AT&T offered to Holiday Corp.	O	1278..1441
AT&T gave a 5% to 10% discount to the Memphis, Tenn., company that oversees Holiday Inns, in response to a similar discount offered to Holiday Corp. by MCI Communications Corp.	O	1444..1620
The agency said that because MCI's offer had expired AT&T couldn't continue to offer its discount plan.	B-restatement	1621..1724
But the agency specifically didn't rule whether AT&T had the right to match offers by competitors if that means giving discounts not generally available to other phone users.	I-restatement	1725..1899
Indeed, Joe Nacchio, AT&T's vice president for business-communications services, said AT&T offered a similar Tariff 15 discount to Resort Condominium International, of Indianapolis, to meet another MCI bid.	O	1902..2108
The FCC "didn't say I couldn't do it again," he said.	O	2109..2162
Apart from those two actions, Mr. Sikes and the three other commissioners said they expect to re-examine how AT&T is regulated since competition has increased.	O	2165..2324
Richard Firestone, chief of the FCC's common-carrier bureau, said he expected the agency to propose new rules next year.	O	2325..2445
AT&T applauded the FCC's actions.	B-restatement	2448..2481
"The time is long overdue to take a look at the fierce competition in the long-distance business and the rules governing it," the New York telecommunications firm said in a statement.	I-restatement	2482..2665
But MCI, of Washington, was displeased with the FCC decision concerning Tariff 12, arguing that "AT&T cannot be allowed to flaunt FCC rules."	O	2666..2807
United Telecommunications Inc. 's US Sprint unit said it was "obviously disappointed" with the FCC decision on Tariff 12.	O	2810..2931
US Sprint said was it will petition the FCC decision in federal court.	O	2932..3002
"We believe that the court will find it unlawful," said a US Sprint spokesman.	O	3003..3081
Separately, AT&T filed a countersuit against MCI accusing it of misleading consumers through allegedly "false and deceptive" advertising.	B-conjunction	3084..3221
The AT&T action was the most recent blow in a nasty fight.	B-cause	3222..3280
Earlier this month, MCI sued AT&T in federal district court, claiming that AT&T's ads are false.	I-cause	3281..3377
AT&T assembled three of its top executives in Washington, all visibly angry, to try to refute MCI's charges.	B-entrel	3380..3488
"MCI has made hawks out of the upper echelon of AT&T," said PaineWebber's Mr. Grubman, who said he expected AT&T to become increasingly aggressive in dealing with its longtime nemesis.	I-entrel	3489..3673
Julie Amparano Lopez in Philadelphia also contributed to this article.	O	3676..3746
