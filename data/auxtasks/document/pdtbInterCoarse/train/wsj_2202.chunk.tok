You know what the law of averages is , do n't you ?	O	9..57
It 's what 1 ) explains why we are like , well , ourselves rather than Bo Jackson ; 2 ) cautions that it 's possible to drown in a lake that averages two feet deep ; and 3 ) predicts that 10,000 monkeys placed before 10,000 pianos would produce 1,118 publishable rock 'n' roll tunes .	O	58..332
Baseball , that game of the long haul , is the quintessential sport of the mean , and the mean ol' law caught up with the San Francisco Giants in the World Series last weekend .	O	335..508
The team that dumped runs by the bushel on the Chicago Cubs in the National League playoffs was held to just one in two games by the home-team Oakland A 's , the gang that had been done unto similarly by the Los Angeles Dodgers and Orel Hershiser in last year 's tournament .	O	509..780
Morever , much of the damage was accomplished by A 's who had some catching up to do .	O	781..864
In game two , on a cool Sunday evening in this land of perpetual autumn , a lot of the catching up was done by the A 's catcher , Terry Steinbach .	B-expansion	867..1009
He hit a 2-0 pitch from Rick Reuschel into the left-field stands in inning four to stretch his team 's lead from 2-1 to a decisive 5-1 , where it stayed .	B-comparison	1010..1161
So what if Steinbach had struck just seven home runs in 130 regular-season games , and batted in the seventh position of the A 's lineup .	I-comparison	1162..1297
" If you get your pitch , and take a good swing , anything can happen , " he later remarked .	O	1298..1385
On Saturday night , quite a few of the boys in green and gold salted away successes to salve the pain of past and , no doubt , future droughts .	B-expansion	1388..1528
Mark McGwire , the big , red-haired Oakland first baseman , had three hits in four at bats , two more than he 'd had in the five-game Dodger series in which he 'd gone 1-for-17 .	B-expansion	1529..1700
The A-men batting Nos. 6 through 9 , a.k.a. the " bottom of the order , " got seven of their team 's 11 hits and scored four of its runs in a 5-0 decision .	I-expansion	1701..1851
Right-hander Dave Stewart held the Giants to five hits to account for the zero on the other side of the Saturday ledger .	B-entrel	1854..1974
That he was the A 's winningest pitcher during its American League campaign with a 21-9 mark , plus two wins over Toronto in the playoffs , indicates he may have some evening up coming , but with the way his split-fingered fastball is behaving , that might not be this week .	B-expansion	1975..2244
The same goes for Mike Moore , another veteran who overcame early struggles to permit the Giants but a run and four hits in seven innings in Sunday 's contest .	I-expansion	2245..2402
" Every guy they put out there had a better split-finger than the guy before , " marveled Giant manager Roger Craig .	B-entrel	2405..2518
He 's an ex-hurler who 's one of the leading gurus of the fashionable delivery , which looks like a fastball until it dives beneath the lunging bat .	I-entrel	2519..2664
The upshot of the downshoot is that the A 's go into San Francisco 's Candlestick Park tonight up two games to none in the best-of-seven fest .	B-entrel	2667..2807
The stat to reckon with here says that about three of four clubs ( 29 of 39 ) that took 2-0 Series leads went on to win it all .	B-expansion	2808..2933
That 's not an average to soothe Giant rooters .	I-expansion	2934..2980
One might think that the home fans in this Series of the Subway Called BART ( that 's a better name for a public conveyance than " Desire , " do n't you think ? ) would have been ecstatic over the proceedings , but they observe them in relative calm .	O	2983..3224
Partisans of the two combatants sat side by side in the 49,000-plus seats of Oakland Coliseum , and while they cheered their favorites and booed the opposition , hostilities advanced no further , at least as far as I could see .	O	3225..3449
A few folks even showed up wearing caps bearing the colors and emblems of both teams .	B-expansion	3452..3537
" I 'm for the Giants today , but only because they lost yesterday .	B-expansion	3538..3602
I love 'em both .	I-expansion	3603..3619
The only thing I 'm rooting for is for the Series to go seven games , " said David Williams , a Sacramento septuagenarian , at the Coliseum before Sunday 's go .	O	3620..3774
The above represents a triumph of either apathy or civility .	B-entrel	3777..3837
I choose to believe it 's the latter , although it probably springs from the fact that just about everyone out here , including the A 's and Giants , is originally from somewhere else .	I-entrel	3838..4017
Suffice it to say that if this were a New York Yankees-Mets series , or one between the Chicago Cubs and White Sox ( hey , it 's possible ) , you 'd need uniformed police in every other seat to separate opposing fans , and only the suicidal would bifurcate their bonnets .	O	4018..4281
Anyway , the A 's gave you a lot of heroes to root for .	B-expansion	4284..4337
In the opening game , besides Steinbach and Stewart , there was Walt Weiss , a twiggy-looking , second-year shortstop who had lost a couple months of the season to knee surgery .	I-expansion	4338..4511
He was flawless afield ( ditto in game two ) , moved a runner along in the A 's three-run second inning , and homered for his team 's final tally .	B-entrel	4512..4652
Such is his reputation among the East Bay Bashers that when he hit his first career home run last season , the fan who caught it agreed to turn the ball over to him in return for an autograph .	I-entrel	4653..4844
Not his autograph ; power-hitter McGwire 's .	O	4845..4887
An A 's co-hero of the second game was Rickey Henderson , who exemplifies the hot side of the hot-cold equation .	B-expansion	4890..5000
He smoked Toronto in the playoffs with six hits , seven walks and eight stolen bases in 22 at bats , and continued that by going 3-for-3 at the plate Sunday , along with walking , stealing a base and scoring a run .	I-expansion	5001..5211
" When you 're in the groove , you see every ball tremendously , " he lectured .	O	5212..5286
The cold guys in the set were Will Clark , Kevin Mitchell and Matt Williams , the Giants ' 3-4-5 hitters .	B-entrel	5289..5391
They combined for 25 hits , six home runs and 24 runs batted in in the five games against the Cubs .	B-comparison	5392..5490
They went a collective 5-for-24 here , with zero homers and ribbies .	I-comparison	5491..5558
It 's that last set of numbers , as much as anything else , that gives the Giants hope in the Series games to come .	O	5561..5673
" I believe in the law of averages , " declared San Francisco batting coach Dusty Baker after game two .	O	5674..5774
" I 'd rather see a so-so hitter who 's hot come up for the other side than a good hitter who 's cold . "	B-comparison	5775..5874
But the old Dodger slugger wisely offered no prediction about when good times would return to his side .	B-expansion	5877..5980
" When it goes , you never know when you 'll get it back , " he said .	I-expansion	5981..6045
" That 's baseball .	O	6046..6063
