When Nucor Corp. begins shipping steel from the world's first thin-slab plant this month, it will begin testing the competitive mettle of its giant competitors.	O	9..169
The new technology, which creates a very thin piece of steel, radically reduces the costs of making flat-rolled sheets.	O	172..291
An ebullient Kenneth Iverson, Nucor's chairman, says the company's plant eventually will make a ton of steel in 1.5 man hours, compared with four to six man hours at a conventional mill.	O	292..478
"We've had the Russians and Chinese, and people from India visiting us," Mr. Iverson beams.	O	481..572
"Everyone in the world is watching us very closely."	O	573..625
Especially his neighbors, the major U.S. steelmakers.	B-restatement	628..681
Already, USX Corp. and Armco Inc. are studying Nucor's technology to see if they can adopt it.	I-restatement	682..776
Says the chief executive officer of a major Midwest steel company: "It's damn worrisome."	O	777..866
The once-staid steel industry is about to be turned topsy-turvy by a 1990s technology revolution.	B-cause	869..966
New, efficient and sophisticated processes make it easier for smaller, less cash-rich companies to make steel at a fraction of what Big Steel paid decades ago.	B-conjunction	967..1126
New, efficient and sophisticated processes make it easier for smaller, less cash-rich companies to make steel at a fraction of what Big Steel paid decades ago.It also enables minimills finally to get a toehold in the flat-rolled steel market -- the major steelmakers' largest, most prized, and until now, untouchable, market.	B-concession	1127..1293
But such thin-slab technology is only the beginning.	B-cause	1296..1348
Eager engineers espouse direct-steelmaking and direct casting, which by the end of the 1990s will enable production without coke ovens and blast furnaces.	I-cause	1349..1503
Those massive structures, while posing cost and environmental headaches, effectively locked out all but deep-pocketed giants from steelmaking.	O	1504..1646
"There's a revolution ahead of us that will ultimately change the way we market and distribute steel," says William Dennis, vice president, manufacturing and technology, for the American Iron Ore and Steel Institute.	O	1649..1865
It isn't that major steelmakers have blithely ignored high technology.	O	1868..1938
In fact, they've spent billions of dollars to boost the percentage of continously cast steel to 60.9% in 1988, from 39.6% five years before.	B-conjunction	1939..2079
In fact, they've spent billions of dollars to boost the percentage of continously cast steel to 60.9% in 1988, from 39.6% five years before.Moreover, their balance sheets are rich with diversity, their old plants shuttered, and work forces lean.	B-concession	2080..2185
But that won't suffice.	B-cause	2188..2211
"It's no longer enough to beat the guy down the street.	I-cause	2212..2267
You have to beat everyone around the world," says Mr. Dennis.	O	2268..2329
He wants to see steelmakers more involved in computers and artificial intelligence.	O	2330..2413
The problem: They're saddled with huge plants that require costly maintenance.	O	2416..2494
And try plying new dollars free in a market that is softening, hurt by a strong dollar and concerned about overcapacity -- the industry's Darth Vadar.	O	2495..2645
"The technology revolution is going to be very threatening to established producers," says Peter Marcus, an analyst with PaineWebber Inc.	O	2648..2785
"They've got too much invested in the old stuff and they can't get their workers to be flexible."	O	2786..2883
No one expects minimills to eclipse major integrated steelmakers, who remain the undisputed kings of highest-quality steel used for autos and refrigerators.	O	2886..3042
Nucor's plant in Crawfordsville, Ind., ultimately will produce only one million tons annually, a drop in the 40-million-ton-a-year flat-rolled steel bucket, and it will be years before such plants can compete in the high-profit market.	B-contrast	3043..3278
Still, flat-rolled is the steel industry's bread and butter, representing about half of the 80 million tons of steel expected to be shipped this year.	I-contrast	3279..3429
Moreover, the process isn't without its headaches.	B-instantiation	3432..3482
Because all operations are connected, one equipment failure forces a complete plant shutdown.	I-instantiation	3483..3576
On some days, the Nucor plant doesn't produce anything.	O	3577..3632
"At this point, the minimill capacity won't make a great dent in the integrated market, but it does challenge them to develop new markets," says James McCall, vice president, materials, at Battelle, a technology and management-research giant based in Columbus, Ohio.	O	3635..3901
Indeed, with demand for steel not growing fast enough to absorb capacity, steelmakers will have to change the way they do business.	O	3904..4035
In the past, says Armco's chief economist John Corey, steelmakers made a product and set it out on the loading dock.	O	4036..4152
"We said: `We've got a product: if you want it, you can buy it, '" he says, adding: "Now we're figuring out what people need, and are going back to make it."	O	4153..4310
Armco's sales representatives visit the General Motors Corp. 's Fairfax assembly plant in Kansas City, Mo., two or three days a week.	B-entrel	4313..4446
When they determined that GM needed parts more quickly, Armco convinced a steel service center to build a processing plant nearby so shipments could be delivered within 15 minutes.	I-entrel	4447..4627
Cementing such relationships with major clients -- car and appliance makers -- is a means of survival, especially when those key clients are relying on a smaller pool of producers and flirting with plastic and aluminum makers.	O	4630..4856
For example, when Detroit began talking about plastic-bodied cars, the American Iron and Steel Institute began a major lobbying effort to show auto makers how they could use steel more efficiently by simply redesigning how a car door is assembled.	O	4859..5106
But steelmakers must also find new markets.	B-instantiation	5109..5152
After letting aluminum-makers take the recycling lead, a group of the nation's largest steelmakers started a recycling institute to promote steel cans to an environmentally conscious nation.	I-instantiation	5153..5343
Battelle's Mr. McCall thinks steelmakers should concentrate more on construction.	O	5346..5427
Weirton Steel Corp., Weirton, W. Va., for example, is touting to homeowners fashionable steel doors, with leaded glass inserts, as a secure and energy-efficient alternative to wooden or aluminum ones.	B-synchrony	5428..5628
Other steelmakers envision steel roofs covering suburbia.	I-synchrony	5629..5686
Still others are looking at overseas markets.	B-instantiation	5689..5734
USX is funneling drilling pipe to steel-hungry Soviet Union.	B-restatement	5735..5795
This year, the nation's largest steelmaker reactivated its overseas sales operation.	I-restatement	5796..5880
Producers also are trying to differentiate by concentrating on higher-profit output, such as coated and electrogalvanized products, which remain beyond the reach of minimills.	O	5883..6058
Almost all capital-improvement programs announced by major steelmakers within the past year involve building electrogalvanizing lines, used to produce steel for such products as household appliances and car doors.	O	6059..6272
But unfortunately, that segment is much smaller than the bread-and-butter flat-rolled steel.	O	6275..6367
"It's like everyone climbing out of the QE II and getting into a lifeboat," says John Jacobson, an analyst with AUS Consultants.	O	6368..6496
After a while, someone has to go over the side.	B-cause	6497..6546
Although he doesn't expect any bankruptcies, he does see more plants being sold or closed.	I-cause	6547..6637
Robert Crandall, with the Brookings Institute, agrees.	B-restatement	6640..6694
"Unless there is an enormous rate of economic growth or a further drop in the dollar, it's unlikely that consumption of U.S. produced steel will grow sufficiently to offset the growth of minimills."	I-restatement	6695..6893
Not to mention the incursion of imports.	O	6896..6936
Japanese and European steelmakers, which have led the recent technology developments, are anxiously awaiting the lifting of trade restraints in 1992.	B-conjunction	6937..7086
Moreover, the U.S. can expect more competition from low-cost producing Pacific Rim and Latin American countries.	B-instantiation	7087..7199
A Taiwanese steelmaker recently announced plans to build a Nucor-like plant.	I-instantiation	7200..7276
"People think of the steel business as an old and mundane smokestack business," says Mr. Iverson.	O	7279..7376
"They're dead wrong."	O	7377..7398
*USX, LTV, Bethlehem, Inland, Armco, National Steel	O	7401..7452
**Projected	O	7455..7466
