Margaret Thatcher 's instinctive response to the latest upheaval in her government is to promise business as usual .	O	9..123
That may be the last thing she needs .	O	124..161
As the air clears from last week 's storm of resignations and reshufflings , the government faces a daunting job of rebuilding confidence in its policies .	O	164..316
The prime minister and her new chancellor of the exchequer , the untested John Major , need to haul the country through something like a recession to bring down inflation and set the economy moving again .	B-conjunction	317..519
Mrs. Thatcher has to come to terms with European economic integration , beginning with the European Monetary System , which Britain is committed to joining fully someday .	B-conjunction	520..688
Finally , the government has to convince a rattled financial community -- and voters -- it is proceeding coherently toward its goals .	I-conjunction	689..821
It sounds like the work of a decade , but the deadline is late 1991 , when Mrs. Thatcher is expected to call another national election .	O	824..957
What 's worrying her supporters is that the economic cycle may be out of kilter with the political timetable .	O	958..1066
She could end up seeking a fourth term in an economy sick with inflation , high interest rates and a heavy trade deficit .	O	1067..1187
Though Mrs. Thatcher has pulled through other crises , supporters wonder if her steely , autocratic ways are the right formula today .	O	1190..1321
" There 's a rising fear that perhaps Mrs. Thatcher 's style of management has become a political liability , " says Bill Martin , senior economist at London brokers UBS-Phillips & Drew .	O	1322..1502
The prime minister 's insistence on keeping a private coterie of advisers -- including an economic guru who openly criticized former Chancellor of the Exchequer Nigel Lawson -- confused the financial community .	B-conjunction	1505..1714
Last week , the strategy of playing the two experts off each other blew up : Mr. Lawson quit in exasperation and Sir Alan Walters , the adviser , announced his resignation within an hour .	I-conjunction	1715..1898
The confusion could be costly .	B-cause	1901..1931
Currency traders , suspecting Mr. Major wo n't defend the pound strenuously , sent the British currency sharply lower Friday against the dollar and West German mark .	B-conjunction	1932..2094
Analysts expect further jitters this week .	I-conjunction	2095..2137
A continuing slide in the pound could force the government to push through another rise in the base rate , currently 15 % .	B-cause	2140..2260
That could shove a weak economy into recession .	B-conjunction	2261..2308
Economists have been anticipating a slump for months , but they now say it will be deeper and longer than they had thought .	I-conjunction	2309..2431
Britain 's economy " is developing rapidly toward no-growth , " says J. Paul Horne , international economist with Smith Barney , Harris Upham Co. in Paris .	O	2432..2581
A mild slowdown probably would have run its course by early 1991 , economists say , while the grueling downturn now expected could stretch into 1992 .	O	2584..2731
Recovery could be hampered if Britain 's major trading partners in Europe , which are enjoying robust economic activity , cool off as expected in late 1990 and 1991 .	O	2732..2894
That would leave Mrs. Thatcher little room for maneuver .	B-cause	2897..2953
For the Tories to win the next election , voters will need to sense economic improvement for about a year beforehand .	B-conjunction	2954..3070
Though Mrs. Thatcher does n't need to call an election until June 1992 , she would prefer doing so in late 1991 .	I-conjunction	3071..3181
" If the economy shows no sign of turning around in about year 's time , she will be very vulnerable , " says John Barnes , a lecturer at the London School of Economics .	O	3182..3345
There 's an equally pressing deadline for the government to define its monetary and economic ties to the rest of the European Community .	O	3348..3483
It has sent mixed signals about its willingness to take part in the exchange-rate mechanism of the European Monetary System , which links the major EC currencies .	O	3484..3645
At a June EC summit , Mrs. Thatcher appeared to ease her opposition to full EMS membership , saying Britain would join once its inflation rate fell and the EC liberalized capital movements .	O	3646..3833
Since then , the government has left observers wondering if it ever meant to join .	B-cause	3836..3917
Sir Alan assailed the monetary arrangement as " half-baked " in an article being published in an American economics journal .	B-conjunction	3918..4040
That produced little reaction from his boss , reinforcing speculation the government would use its two conditions as a pretext for avoiding full EMS membership .	I-conjunction	4041..4200
Despite the departure of Mr. Lawson and Sir Alan , the tug-of-war over the EMS could continue .	B-cause	4203..4296
Sir Geoffrey Howe , deputy prime minister and a Lawson ally on the EMS , has signaled he will continue pressing for early membership .	I-cause	4297..4428
Of immediate concern is whether the Thatcher government will continue Mr. Lawson 's policy of tracking the monetary policies of the West German Bundesbank and responding in kind when the Frankfurt authorities move interest rates .	O	4431..4659
Mrs. Thatcher " does n't like taking orders from foreigners , " says Tim Congdon , economist with Gerrard & National Holding PLC .	O	4660..4784
As Conservatives rally around Mrs. Thatcher during the crisis , many harbor hopes last week 's debacle will prompt change .	B-instantiation	4787..4907
" We wo n't have any more of this wayward behavior , " says Sir Peter Hordern , a backbench Tory member of Parliament .	I-instantiation	4908..5021
" The party is fed up with sackings of good people . "	O	5022..5073
It 's an unrealistic expectation .	B-cause	5076..5108
As long as a decade ago , Mrs. Thatcher declared she did n't want debate in her cabinet ; she wanted strong government .	B-conjunction	5109..5225
Over the weekend , she said she did n't intend to change her style and denied she is authoritarian .	I-conjunction	5226..5323
" Nonsense , " she told an interviewer yesterday on London Weekend Television .	O	5324..5399
" I am staying my own sweet , reasonable self .	O	5400..5444
