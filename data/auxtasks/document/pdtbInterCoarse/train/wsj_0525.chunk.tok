While Wall Street is retreating from computer-driven program trading , big institutional investors are likely to continue these strategies at full blast , further roiling the stock market , trading executives say .	O	9..219
Bowing to a mounting public outcry , three more major securities firms -- Bear , Stearns & Co. Inc. , Morgan Stanley & Co. and Oppenheimer & Co. -- announced Friday they would suspend stock-index arbitrage trading for their own accounts .	O	222..456
PaineWebber Group Inc. announced a pullback on Thursday from stock-index arbitrage , a controversial program-trading strategy blamed by many investors for encouraging big stock-market swings .	O	459..649
Though the trading halts are offered as a sign of concern about recent stock market volatility , most Wall Street firms remain open to handle program trading for customers .	B-expansion	652..823
Trading executives privately say that huge stock-index funds , which dwarf Wall Street firms in terms of the size of their program trades , will continue to launch big programs through the stock market .	I-expansion	824..1024
Wells Fargo Investment Advisers , Bankers Trust Co. and Mellon Capital Management are among the top stock-index arbitrage clients of Wall Street , trading executives say .	B-entrel	1027..1195
These huge stock-index funds build portfolios that match the S&P 500 stock index or other stock indexes , and frequently swap between stocks and futures to capture profits .	I-entrel	1196..1367
" They will do it every chance they get , " said one program-trading executive .	O	1368..1444
Consequently , abrupt swings in the stock market are not likely to disappear anytime soon , they say .	O	1447..1546
In fact , without Wall Street firms trading for their own accounts , the stock-index arbitrage trading opportunities for the big funds may be all the more abundant .	O	1547..1709
" More customers may come to us now , " said James Cayne , president of Bear Stearns Cos .	O	1710..1795
Executives who manage these funds see the current debate over program trading as a repeat of the concern expressed after the 1987 crash .	O	1798..1934
They noted that studies completed after the 1987 crash exonerated program trading as a source of volatility .	O	1935..2043
" The issues that are ( now ) being raised , in classic anti-intellectual fashion , fly in the face of a number of post-crash studies , " said Fred Grauer , chairman of Wells Fargo Investment Advisers .	O	2044..2237
A Bankers Trust spokesman said that the company 's investment arm uses stock-index arbitrage to enhance investors ' returns .	B-entrel	2240..2362
Officials at Mellon Capital were unavailable for comment .	I-entrel	2363..2420
Stock-index funds have grown in popularity over the past decade as pension funds and other institutional investors have sought a low-cost way to match the performance of the stock market as a whole .	B-contingency	2423..2621
Many money managers who trade stock actively have trouble consistently matching the S&P-500 's returns .	I-contingency	2622..2724
Some stock-index funds are huge .	B-expansion	2727..2759
Wells Fargo Investment Advisers , for example , managed $ 25 billion in stock investments tracking the S&P 500 at the end of June , according to Standard & Poor 's Corp .	B-entrel	2760..2924
Mr. Grauer said $ 2 billion of that is used in active index arbitrage .	I-entrel	2925..2994
Stock-index funds frequently use the futures markets as a hedging tool , but that is a far less aggressive strategy than stock-index arbitrage , in which traders buy and sell big blocks of stocks with offsetting trades in stock-index futures to profit from price differences .	O	2997..3270
The 190-point plunge in the stock market Oct. 13 has heightened concerns about volatility .	B-expansion	3273..3363
And while signs of an economic slowdown , softer corporate earnings and troubles with takeover financing all have contributed to the stock market 's recent weakness , many investors rushed to blame program trading for aggravating market swings .	I-expansion	3364..3605
The Wall Street firms ' pullback followed their recent blacklisting by several institutional investors .	B-expansion	3608..3710
Last Tuesday , Kemper Corp. 's Kemper Financial Services Inc. unit said it would no longer trade with firms committed to stock-index arbitrage , including the three that later suspended stock-index arbitrage trading on Friday .	I-expansion	3711..3935
Phoenix Mutual Life Insurance Co. and Founders Asset Management Inc. also cut off brokerage firms that engage in program trading .	O	3936..4065
Though it is still doing stock-index arbitrage trades for customers , Morgan Stanley 's trading halt for its own account is likely to shake up firms such as Kidder , Peabody & Co. that still do such trades for their own account .	O	4068..4293
Morgan Stanley has consistently been one of the top stock-index arbitrage traders in recent months .	O	4294..4393
Indeed , Morgan Stanley 's president , Richard B. Fisher , said the firm is putting up money to form a group of regulators , investors and investment banks to find out if stock-index arbitrage artificially induces stock-market volatility .	O	4396..4629
" We have to clear up these issues and find out what is present that is creating artificial volatility , " Mr. Fisher said .	O	4632..4752
" There is no question that investor confidence ( in the stock market ) is critical . "	O	4753..4835
Joining the call for some kind of study or regulatory action , Merrill Lynch & Co. recommended program-trading reforms late Friday , including higher margins on stock-index futures and greater regulatory coordination .	O	4838..5053
Separately , Mr. Cayne of Bear Stearns said his firm is working with regulators to balance margin requirements to " enhance stabilization . "	B-entrel	5054..5191
Margin rules determine the minimum amount of cash an investor must put up when buying a security .	B-expansion	5192..5289
Current rules permit investors to put up less cash for futures than for stocks .	I-expansion	5290..5369
Some observers say that different rules governing stock and futures markets are partly responsible for volatility .	O	5372..5486
These rules , they say , permit faster and cheaper trading in futures than in stocks , which frequently knocks the two markets out of line .	O	5487..5623
Stock-index arbitrage , because it sells the more " expensive " market and buys the " cheaper " one , attempts to reestablish the link between the stock and futures markets , and the adjustments are often abrupt .	B-comparison	5626..5831
But unequal trading rules allow the futures market to trade differently from stocks , which invites frequent bouts of stock-index arbitrage in the first place .	I-comparison	5832..5990
" There has to be better coordination on a regulatory basis , " said Christopher Pedersen , director of trading at Twenty-First Securities Corp .	O	5993..6133
" One agency should have the authority over all equity products .	O	6134..6197
