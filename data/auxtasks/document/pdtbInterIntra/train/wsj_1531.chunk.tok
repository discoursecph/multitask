A labor-management group is preparing a revised buy-out bid for United Airlines parent UAL Corp. that would transfer majority ownership to employees while leaving some stock in public hands , according to people familiar with the group .	root	9..244
The group has been discussing a proposal valued in a range of $ 225 to $ 240 a share , or $ 5.09 billion to $ 5.42 billion .	norel	247..365
But to avoid the risk of rejection , the group does n't plan to submit the plan formally at a UAL board meeting today .	conjunction	366..482
Instead , the group is raising the proposal informally to try to test the board 's reaction .	alternative	483..573
People familiar with the company say the board is n't likely to give quick approval to any offer substantially below the $ 300-a-share , $ 6.79 billion buy-out bid that collapsed last week after banks would n't raise needed loans and after a key partner , British Airways PLC , dropped out .	norel	576..859
In composite trading Friday on the New York Stock Exchange , UAL closed at $ 168.50 a share , down $ 21.625 .	norel	862..966
But the pilots union , which has been pushing for a takeover since 1987 , appears to be pressing ahead with the revised bid to avoid further loss of momentum	concession	969..1124
even though it has n't found a partner to replace British Air .	concession	1125..1186
Although the bidding group has n't had time to develop its latest idea fully or to discuss it with banks , it believes bank financing could be obtained .	norel	1189..1339
After the collapse of the last effort , the group does n't plan to make any formal proposal without binding commitments from banks covering the entire amount to be borrowed .	concession	1340..1511
Under the type of transaction being discussed , the pilot-management group would borrow several billion dollars from banks	norel	1514..1635
that could then be used to finance a cash payment to current holders .	asynchronous	1636..1705
Those current holders would also receive minority interests in the new company .	conjunction	1706..1785
For example , the group could offer $ 200 a share in cash plus stock valued at $ 30 a share .	instantiation	1786..1875
UAL currently has 22.6 million shares , fully diluted .	entrel	1876..1929
The new structure would be similar to a recapitalization in which holders get a special dividend yet retain a controlling ownership interest .	norel	1932..2073
The difference is that current holders would n't retain majority ownership or control .	contrast	2074..2159
The failed takeover would have given UAL employees 75 % voting control of the nation 's second-largest airline , with management getting 10 % control and British Air 15 % .	norel	2162..2328
It was n't clear how the ownership would stack up under the new plan	entrel	2329..2396
but employees would keep more than 50 % .	contrast	2397..2437
Management 's total could be reduced	conjunction	2438..2473
and the public could get more than the 15 % control that had been earmarked for British Air .	conjunction	2474..2566
One option the board is likely to consider today is some sort of cooling-off period .	norel	2569..2653
Although the pilots are expected to continue to pursue the bid , UAL Chairman Stephen Wolf may be asked to withdraw from the buy-out effort , at least temporarily , and to return to running the company full time .	cause	2654..2863
The board could eventually come under some pressure to sell the company because its members can be ousted by a majority shareholder vote , particularly since one-third of UAL stock is held by takeover stock speculators who favor a sale .	norel	2866..3101
The labor-management buy-out group plans to keep its offer on the table in an apparent attempt to maintain its bargaining position with the board .	synchrony	3102..3248
However , the only outsider who has emerged to lead such a shareholder vote , Los Angeles investor Marvin Davis , who triggered the buy-out with a $ 5.4 billion bid in early August , is hanging back -- apparently to avoid being blamed for contributing to the deal 's collapse .	contrast	3251..3521
Three top advisers to Mr. Davis visited New York late last week , at least in part to confer with executives at Citicorp .	norel	3524..3644
Mr. Davis had paid $ 6 million for Citicorp 's backing of his last bid .	entrel	3645..3714
But Citicorp has lost some credibility	contrast	3715..3753
because it also led the unsuccessful effort to gain bank loans for the labor-management group .	cause	3754..3848
On Friday , British Air issued a statement saying it " does not intend to participate in any new deal for the acquisition of UAL in the foreseeable future . "	norel	3851..4005
However , several people said that British Air might yet rejoin the bidding group and that the carrier made the statement to answer questions from British regulators about how it plans to use proceeds of a securities offering previously earmarked for the UAL buy-out .	contrast	4006..4272
Also late last week , UAL flight attendants agreed to participate with the pilots in crafting a revised offer .	conjunction	4275..4384
But the machinists union , whose opposition helped scuttle the first buy-out bid , is likely to favor a recapitalization with a friendly third-party investor .	contrast	4385..4541
One advantage the buy-out group intends to press with the board is that pilots have agreed to make $ 200 million in annual cost concessions to help finance a bid .	norel	4544..4705
Speculation has also arisen that the UAL executive most closely identified with the failure to gain bank financing , chief financial officer John Pope , may come under pressure to resign .	norel	4708..4893
However , people familiar with the buy-out group said Mr. Pope 's departure would weaken the airline 's management at a critical time .	contrast	4894..5025
Despite the buy-out group 's failure to obtain financing , UAL remains obligated to pay $ 26.7 million in investment banking and legal fees to the group 's advisers , Lazard Freres & Co. , Salomon Brothers Inc. , and Paul Weiss Rifkind Wharton & Garrison .	norel	5028..5276
