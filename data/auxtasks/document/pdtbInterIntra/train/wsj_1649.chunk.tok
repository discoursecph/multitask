Raymond Chandler , in a 1950 letter defending a weak Hemingway book , likened a champion writer to a baseball pitcher .	root	9..125
When the champ has lost his stuff , the great mystery novelist wrote , " when he can no longer throw the high hard one , he throws his heart instead .	restatement	126..271
He throws something .	restatement	272..292
He does n't just walk off the mound and weep . "	contrast	293..338
Chandler might have been predicting the course of his own career .	norel	341..406
His last published novel featuring private detective Philip Marlowe , the inferior " Playback " ( 1958 ) , at times read almost like a parody of his previous work .	cause	407..564
When he died in 1959 , Chandler left behind four chapters of yet another Marlowe book , " The Poodle Springs Story , " which seemed to go beyond parody into something like burlesque .	asynchronous	565..742
" Champ " Chandler 's last pitch , apparently , was a screwball .	expansion	743..802
Now Robert Parker , author of several best sellers featuring Spenser , a contemporary private eye in the Marlowe mold , has with the blessings of the Chandler estate been hired to complete " The Poodle Springs Story . "	norel	805..1018
The result , " Poodle Springs " ( Putnam 's , 288 pages , $ 18.95 ) is an entertaining , easy to read and fairly graceful extension of the Marlowe chronicle , full of hard-boiled wisecracks and California color .	entrel	1019..1219
If it does not quite have Chandler 's special magic -- well , at the end , neither did Chandler .	entrel	1220..1313
As the book begins , a newly wed Marlowe roars into the desert resort of Poodle ( a.k.a . Palm ) Springs at the wheel of a Cadillac Fleetwood .	norel	1316..1454
His bride is the rich and beautiful Linda Loring , a character who also appeared in Chandler 's " The Long Goodbye " and " Playback . "	entrel	1455..1583
Philip and Linda move into her mansion and ca n't keep their hands off each other , even in front of the Hawaiian/Japanese houseman .	entrel	1584..1714
But the lovebirds have a conflict .	contrast	1715..1749
He wants to continue being a low-paid private eye	cause	1750..1799
and she wants him to live off the million dollars she 's settled on him .	contrast	1800..1872
That 's Chandler 's setup .	norel	1875..1899
Mr. Parker spins it into a pretty satisfying tale involving Poodle Springs high life , Hollywood low life and various folk who hang their hats in both worlds .	entrel	1900..2057
The supporting lineup is solid , the patter is amusing	restatement	2058..2111
and there 's even a cameo by Bernie Ohls , the " good cop " of previous Chandler books who still does n't hesitate to have Marlowe jailed when it suits his purposes .	conjunction	2112..2272
The style throughout bears a strong resemblance to Chandler 's prose at its most pared down .	conjunction	2273..2364
All told , Mr. Parker does a better job of making a novel out of this abandoned fragment than anyone might have had a right to expect .	conjunction	2365..2498
But there are grounds for complaint .	contrast	2501..2537
At one point , the reader is two steps ahead of Marlowe in catching on to a double identity scam	instantiation	2538..2633
-- and Marlowe is supposed to be the pro .	conjunction	2634..2675
More bothersome , there are several apparent anachronisms .	contrast	2676..2733
Contact lenses , tank tops , prostitutes openly working the streets of Hollywood and the Tequila Sunrise cocktail all seem out of place in the 1950s .	instantiation	2734..2881
A little more care in re-creating Marlowe 's universe would have made the book that much more enjoyable .	cause	2882..2985
Mr. Nolan is a contributing editor at Los Angeles Magazine .	norel	2988..3047
