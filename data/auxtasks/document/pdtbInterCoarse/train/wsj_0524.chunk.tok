In the aftermath of the Beijing massacre on June 4 , economists advanced wildly divergent views on how Hong Kong would be affected .	O	9..139
Among the most upbeat was BT Brokerage ( Asia ) Ltd .	O	142..192
In a June 5 reaction , the Bankers Trust Co. unit proclaimed the economy " shockproof . "	O	193..278
Others were more cautious .	O	281..307
In a July analysis titled " From Euphoria to Despair , " W.I. Carr ( Far East ) Ltd. , another securities firm , said that eroding confidence might undermine future economic development .	O	308..487
Today , with business activity in Hong Kong staggering along at an uneven pace , the economy itself seems locked in a struggle between hope and fear .	O	490..637
Manufacturers have survived the turmoil in China largely unscathed .	B-expansion	640..707
Signs of revival seem evident in Hong Kong 's hard-hit hotel sector .	B-comparison	708..775
But in the stock and real-estate markets , activity remains spotty even though prices have regained much of their lost ground .	B-expansion	776..901
Waning demand reported by importers , retailers and even fancy restaurants all reinforce a profile of a community that is sharply tightening its belt .	I-expansion	902..1051
As many economists and businessmen see it , those incongruities underscore a paradox that seems likely to bedevil the economy throughout the 1990s .	O	1054..1200
That paradox is Hong Kong 's economically rewarding yet politically perilous relationship with China .	O	1201..1301
As a model of capitalist efficiency on southern China 's doorstep , Hong Kong 's prospects look good .	B-contingency	1304..1402
China 's land and labor offer inexpensive alternatives to local industry .	B-expansion	1403..1475
China-bound freight streams through the territory 's port .	B-expansion	1476..1533
In the decade since the communist nation emerged from isolation , its burgeoning trade with the West has lifted Hong Kong 's status as a regional business center .	B-expansion	1534..1694
These benefits seem secure despite China 's current economic and political troubles .	B-comparison	1695..1778
But to Hong Kong , China is n't purely business .	B-expansion	1779..1825
It is also the sovereign power that , come 1997 , will take over this British colony .	B-comparison	1826..1909
China 's leaders have promised generous liberties for post-1997 Hong Kong .	B-comparison	1910..1983
That promise sounds shaky now that those same leaders have fallen back on Marxist dogma and brute force to crush their nation 's democracy movement .	I-comparison	1984..2131
Outflows of people and capital from Hong Kong have been growing since the sovereignty issue first arose in the early 1980s .	O	2134..2257
A widely held assumption all along has been that , given its robust economy , Hong Kong will be able to attract sufficient foreign money and talent to comfortably offset the outflows .	B-comparison	2258..2439
With interest in emigration and investment abroad soaring since June 4 , that assumption no longer seems so safe .	I-comparison	2440..2552
Investment and emigration plans take time to come to fruition .	B-expansion	2555..2617
Only four months have passed since the Beijing massacre , and few are prepared to predict its ultimate impact .	I-expansion	2618..2727
The only consensus is that more money and people may leave Hong Kong than had been thought likely .	B-contingency	2728..2826
This expected blow has cast a pall over the economy 's prospects .	I-contingency	2827..2891
The question , as many people see it , is how long such uncertainty will last .	O	2892..2968
Maureen Fraser , an economist with W.I. Carr , a subsidiary of France 's Banque Indosuez , believes that the territory may not be able to regain its momentum until some time after 1997 .	O	2971..3152
It may experience an upswing or two in between .	O	3153..3200
But with local investors shaken by China 's political and economic turmoil , she says , a genuine recovery may not arrive until Hong Kong can prove itself secure under Chinese sovereignty .	O	3201..3386
" Investors have to accept the possibility of a significant slowdown in economic activity in the runup to 1997 , " she says .	O	3389..3510
" Over the next few years , I would advise caution . "	O	3511..3561
In a soon-to-be published book on the territory , a political economist , Miron Mushkat , has derived three future scenarios from interviews with 41 Hong Kong government officials and businessmen .	B-entrel	3564..3757
Nearly half of them argue that Hong Kong 's uneasy relationship with China will constrain -- though not inhibit -- long-term economic growth .	B-comparison	3758..3898
The rest are split roughly between optimists who expect Hong Kong to hum along as before and pessimists who foresee irreparable chaos .	I-comparison	3899..4033
The interviews took place two years ago .	B-entrel	4036..4076
Since the China crisis erupted , Mr. Mushkat says , the scenario as depicted by the middle-of-the-road group bears a remarkable resemblance to the difficulties Hong Kong currently faces .	I-entrel	4077..4261
The consensus of this group , which he dubs " realists , " is that the local economy will grow through the 1990s at annual rates averaging between 3 % and 5 % .	O	4262..4415
Such a pace of growth , though respectable for mature industrialized economies , would be unusually slow for Hong Kong .	B-contingency	4418..4535
Only twice since the 1960s has annual gross domestic product growth here fallen below 5 % for two or more consecutive years .	B-expansion	4536..4659
The first instance occurred in 1967-68 , when China 's Cultural Revolution triggered bloody street rioting in the colony .	B-expansion	4660..4779
The other came in 1974-75 from the combined shock of world recession and a severe local stock market crash .	B-comparison	4780..4887
During the past 10 years , Hong Kong 's economic growth has averaged 8.3 % annually .	B-contingency	4888..4969
Given Hong Kong 's record , Mr. Mushkat 's " realists " might have sounded unduly conservative when the interviews took place two years ago .	I-contingency	4970..5105
Under the current circumstances , he says , their scenario no longer seems unrealistic .	O	5106..5191
" The city could lose some of its entrepreneurial flavor .	O	5194..5250
It could lose some of its dynamism , " says Mr. Mushkat , a director of Baring Securities ( Hong Kong ) Ltd. , a unit of Britain 's Barings PLC . "	O	5251..5390
It does n't have to be a disaster .	B-expansion	5390..5423
It just means that Hong Kong would become a less exciting place . "	I-expansion	5424..5489
Going by official forecasts of GDP , which measures the colony 's output of goods and services , minus foreign income , Mr. Mushkat 's " realists " seem relatively close to the mark .	B-expansion	5492..5667
After taking into account the fallout from the China crisis , the government has projected 1989 GDP growth of 5 % .	B-comparison	5668..5780
The updated forecast , published Aug. 25 , compares with an earlier forecast of 6 % published March 1 and a 7.4 % rate achieved in	I-comparison	5781..5907
Sir Piers Jacobs , Hong Kong 's financial secretary , says a further downward revision may be justified unless the economy stages a more convincing rally .	O	5910..6061
" We are n't looking at anything like a doomsday scenario , " he says .	O	6062..6128
" But clearly we 're entering a difficult period . "	O	6129..6177
Many factors besides a dread of 1997 will have a bearing on Hong Kong 's economy .	B-expansion	6180..6260
One concerns Japanese investors .	B-expansion	6261..6293
Barely visible on Hong Kong 's property scene in 1985 , by last year Japan had become the top foreign investor , spending $ 602 million .	B-comparison	6294..6426
The market has grown relatively quiet since the China crisis .	B-comparison	6427..6488
But if the Japanese return in force , their financial might could compensate to some extent for local investors ' waning commitment .	I-comparison	6489..6619
Another -- and critical -- factor is the U.S. , Hong Kong 's biggest export market .	B-contingency	6622..6703
Even before the China crisis , weak U.S. demand was slowing local economic growth .	B-comparison	6704..6785
Conversely , strong consumer spending in the U.S. two years ago helped propel the local economy at more than twice its current rate .	I-comparison	6786..6917
Indeed , a few economists maintain that global forces will continue to govern Hong Kong 's economic rhythm .	O	6920..7025
Once external conditions , such as U.S. demand , swing in the territory 's favor , they argue , local businessmen will probably overcome their 1997 worries and continue doing business as usual .	O	7026..7214
But economic arguments , however solid , wo n't necessarily impress Hong Kong 's 5.7 million people .	B-contingency	7217..7313
Many are refugees , having fled China 's unending cycles of political repression and poverty since the Communist Party took power in 1949 .	B-contingency	7314..7450
As a result , many of those now planning to leave Hong Kong ca n't easily be swayed by momentary improvements in the colony 's political and economic climate .	I-contingency	7451..7606
Emigration applications soared in 1985 , when Britain and China ratified their accord on Hong Kong 's future .	B-temporal	7609..7716
In 1987 , Hong Kong 's most prosperous year for a decade , 30,000 left , up 58 % from the previous year .	B-comparison	7717..7816
Last year , 45,000 went .	B-comparison	7817..7840
The government predicts that annual outflows will level off over the next few years at as much as 60,000 -- a projection that is widely regarded as unrealistically low .	B-expansion	7841..8009
A large number of those leaving are managers and professionals .	B-expansion	8010..8073
While no one professes to know the exact cost of such a " brain drain " to the economy , hardly anyone doubts that it poses a threat .	I-expansion	8074..8204
" When the economy loses a big portion of its work force that also happens to include its most productive members , economic growth is bound to be affected , " says Anthong Wong , an economist with Hang Seng Bank .	O	8207..8415
