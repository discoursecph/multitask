This is the year the negative ad , for years a secondary presence in most political campaigns , became the main event .	root	9..125
The irony is that the attack commercial , after getting a boost in last year 's presidential campaign , has come of age in an off-off election year with only a few contests scattered across the country .	norel	128..327
But in the three leading political contests of 1989 , the negative ads have reached new levels of hostility , raising fears that this kind of mudslinging , empty of significant issues , is ushering in a new era of campaigns without content .	contrast	330..566
" Now , " says Joseph Napolitan , a pioneer in political television , " the idea is to attack first , last and always . "	norel	569..681
A trend that started with the first stirrings of politics , accelerated with the dawn of the television age and became a sometimes-tawdry art form in 1988 , has reached an entirely new stage .	norel	684..873
" To get people 's attention these days , " says Douglas Bailey , a political consultant , " your TV ad needs to be bold and entertaining , and , more often than not , that means confrontational .	norel	876..1061
And , unlike a few years ago , you do n't even have to worry whether the ad is truthful . "	conjunction	1062..1148
In 1989 , as often as not , the principal fights in the major campaigns are prompted by the ads themselves .	norel	1151..1256
Take a look , then , at the main attack commercials that set the tone for Tuesday 's elections in New York City , New Jersey and Virginia :	cause	1259..1393
New York City :	norel	1396..1410
The screen fills with a small , tight facial shot of David Dinkins , Democratic candidate for mayor of New York City .	norel	1411..1526
" David Dinkins failed to file his income taxes for four straight years , " says a disembodied male voice .	asynchronous	1527..1630
And then this television commercial , paid for by Republican Rudolph Giuliani 's campaign and produced by Roger Ailes , the master of negative TV ads , really gets down to business .	conjunction	1633..1810
Mr. Dinkins , the ad charges , also failed to report his campaign contributions accurately , hid his links to a failing insurance company and paid a convicted kidnapper " through a phony organization with no members , no receipts and no office . "	norel	1811..2051
" David Dinkins , " says the kicker , " Why does he always wait until he 's caught ? "	norel	2054..2132
" Nasty innuendoes , " says John Siegal , Mr. Dinkins 's issues director , " designed to prosecute a case of political corruption that simply does n't exist . "	norel	2135..2285
Stung by the Giuliani ads , Mr. Dinkins 's TV consultants , Robert Shrum and David Doak , finally unleashed a negative ad of their own .	norel	2288..2419
The screen shows two distorted , unrecognizable photos , presumably of two politicians .	restatement	2420..2505
" Compare two candidates for mayor , " says the announcer .	asynchronous	2506..2561
" One says he 's for banning cop-killer bullets .	entrel	2562..2608
The other has opposed a ban on cop-killer bullets .	contrast	2609..2659
One claims he 's pro-choice .	entrel	2660..2687
The other has opposed a woman 's right to choose . "	contrast	2688..2737
" Funny thing , " says the kicker , " both these candidates are named Rudolph Giuliani . "	norel	2740..2823
Who 's telling the truth ?	norel	2826..2850
Everybody -- and nobody .	expansion	2851..2875
It 's a classic situation of ads that are true but not always fully accurate .	restatement	2876..2952
Mr. Dinkins did fail to file his income taxes for four years	norel	2955..3015
but he insists he voluntarily admitted the " oversight " when he was being considered for a city job .	contrast	3016..3116
He was on the board of an insurance company with financial problems	conjunction	3117..3184
but he insists he made no secret of it .	contrast	3185..3225
The city 's Campaign Finance Board has refused to pay Mr. Dinkins $ 95,142 in matching funds	entrel	3226..3316
because his campaign records are incomplete .	cause	3317..3361
The campaign has blamed these reporting problems on computer errors .	contrast	3362..3430
And , says Mr. Dinkins , he did n't know the man his campaign paid for a get-out-the-vote effort had been convicted of kidnapping .	conjunction	3431..3558
But , say Mr. Dinkins 's managers , he did have an office	contrast	3559..3613
and his organization did have members .	conjunction	3614..3652
Mr. Giuliani 's campaign chairman , Peter Powers , says the Dinkins ad is " deceptive . "	norel	3655..3738
The other side , he argues , " knows Giuliani has always been pro-choice , even though he has personal reservations .	cause	3739..3851
They know he is generally opposed to cop-killer bullets , but that he had some reservations about the language in the legislation . "	conjunction	3852..3982
Virginia :	norel	3985..3994
Democratic Lt. Gov. Douglas Wilder opened his gubernatorial battle with Republican Marshall Coleman with an abortion commercial produced by Frank Greer that analysts of every political persuasion agree was a tour de force .	norel	3995..4217
Against a shot of Monticello superimposed on an American flag , an announcer talks about the " strong tradition of freedom and individual liberty " that Virginians have nurtured for generations .	list	4218..4409
Then , just as an image of the statue of Thomas Jefferson dissolves from the screen , the announcer continues : " On the issue of abortion , Marshall Coleman wants to take away your right to choose and give it to the politicians . "	asynchronous	4410..4635
That commercial -- which said Mr. Coleman wanted to take away the right of abortion " even in cases of rape and incest , " a charge Mr. Coleman denies -- changed the dynamics of the campaign , transforming it , at least in part , into a referendum on abortion .	norel	4638..4892
The ad prompted Mr. Coleman , the former Virginia attorney general , to launch a series of advertisements created by Bob Goodman and designed to shake Mr. Wilder 's support among the very women who were attracted by the abortion ad .	cause	4893..5122
The Coleman counterattack featured a close-up of a young woman in shadows	norel	5125..5198
and the ad suggested that she was recalling an unpleasant courtroom ordeal .	conjunction	5199..5274
A voice says , " C'mon , now , do n't you have boyfriends ? "	asynchronous	5275..5329
Then an announcer interjects : " It was Douglas Wilder who introduced a bill to force rape victims age 13 and younger to be interrogated about their private lives by lawyers for accused rapists .	asynchronous	5330..5522
So the next time Mr. Wilder talks about the rights of women , ask him about this law he tried to pass . "	cause	5523..5625
Mr. Wilder did introduce such legislation 17 years ago	norel	5628..5682
but he did so at the request of a constituent , a common legislative technique used by lawmakers .	contrast	5683..5780
The legislation itself noted that it was introduced " by request	restatement	5781..5844
" and in 1983 Mr. Wilder introduced a bill to protect rape victims from unfounded interrogation .	conjunction	5845..5941
" People have grown tired of these ads and Coleman has gotten the stigma of being a negative campaigner , " says Mark Rozell , a political scientist at Mary Washington College .	norel	5944..6116
" Wilder has managed to get across the idea that Coleman will say anything to get elected governor and -- more important -- has been able to put the onus for all the negative campaigning on Coleman . "	cause	6117..6315
Mr. Coleman said this week that he would devote the remainder of the political season to positive campaigning	norel	6318..6427
but the truce lasted only hours .	concession	6428..6461
By Tuesday night , television stations were carrying new ads featuring Mr. Coleman himself raising questions about Mr. Wilder 's sensitivity to rape victims .	restatement	6462..6617
New Jersey :	norel	6620..6631
The attacks began	norel	6632..6649
when Democratic Rep. James Florio aired an ad featuring a drawing of Pinocchio and a photograph of Mr. Florio 's rival , Republican Rep. Jim Courter .	synchrony	6650..6797
" Remember Pinocchio ? " says a female voice .	asynchronous	6798..6840
" Consider Jim Courter . "	expansion	6841..6864
And then this commercial , produced by Bob Squier , gets down to its own mean and dirty business .	conjunction	6867..6962
Pictures of rusted oil drums swim into focus , and the female voice purrs , " That hazardous waste on his { Mr. Courter 's } property -- the neighbors are suing for consumer fraud . "	restatement	6963..7138
And the nose on Mr. Courter 's face grows .	conjunction	7139..7180
The only fraud involved , cry Mr. Courter 's partisans , is the Florio commercial itself	norel	7183..7268
and so the Courter campaign has responded with its own Pinocchio commercial , produced by Mr. Ailes .	cause	7269..7369
In this one , the screen fills with photographs of both candidates .	entrel	7370..7436
" Who 's really lying ? " asks a female voice .	asynchronous	7437..7479
" Florio 's lying , " the voice goes on , because " the barrel on Courter 's land ... contained heating oil , was cleaned up and caused no pollution . "	entrel	7480..7622
Mr. Courter 's long nose shrinks while Mr. Florio 's grows .	norel	7625..7682
Who 's telling the truth ?	norel	7685..7709
Stephen Salmore , a political scientist at New Jersey 's Eagleton Institute , says it 's another example of an ad that 's true but not fully accurate .	norel	7710..7855
Barrels were dumped on the Courter property , a complaint was made , but there is no evidence the barrels were a serious threat to the environment .	cause	7856..8001
Even so , according to Mr. Salmore , the ad was " devastating " because it raised questions about Mr. Courter 's credibility .	norel	8004..8124
But it 's building on a long tradition .	contrast	8125..8163
In 1966 , on route to a re-election rout of Democrat Frank O'Connor , GOP Gov. Nelson Rockefeller of New York appeared in person saying , " If you want to keep the crime rates high , O'Connor is your man . "	restatement	8164..8364
