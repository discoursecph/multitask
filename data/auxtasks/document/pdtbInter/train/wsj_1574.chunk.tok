FOX HUNTING HAS been defined as the unspeakable in pursuit of the inedible , but at least it 's exercise .	O	9..112
At least it has a little dash .	B-contrast	113..143
Most of us have to spend our time on pursuits that afford neither , drab duties rather than pleasures .	B-instantiation	144..245
Like trying to buy life insurance , for instance , an endeavor notably lacking in dash .	B-restatement	246..331
Call it the uninformed trudging after the incomprehensible .	I-restatement	332..391
But sooner or later , most of us have to think about life insurance , just as we often have to think about having root-canal work .	B-conjunction	394..522
And my time has come .	I-conjunction	523..544
I 'm 33 , married , no children , and employed in writing stories like this one .	B-entrel	547..623
In times past , life-insurance salesmen targeted heads of household , meaning men , but ours is a two-income family and accustomed to it .	I-entrel	624..758
So if anything happened to me , I 'd want to leave behind enough so that my 33-year-old husband would be able to pay off the mortgage and some other debts ( though not , I admit , enough to put any potential second wife in the lap of luxury ) .	O	759..996
Figuring that maybe $ 100,000 to $ 150,000 would do but having no idea of what kind of policy I wanted , I looked at the myriad products of a dozen companies -- and plunged into a jungle of gibberish .	O	999..1196
Over the past decade or two , while I was thinking about fox hunting , the insurance industry has spawned an incredible number of products , variations on products , and variations on the variations .	O	1199..1394
Besides term life and whole life ( the old standbys ) , we now have universal life , universal variable life , flexible adjustable universal life , policies with persistency bonuses , policies festooned with exotic riders , living benefit policies , and on and on .	B-cause	1395..1650
What to do ?	I-cause	1651..1662
First , generalize .	B-restatement	1665..1683
Shorn of all their riders , special provisions , and other bells and whistles , insurance policies can still be grouped under two broad categories : so-called pure insurance , which amasses no cash value in the policy and pays off only upon death , and permanent insurance , which provides not only a death benefit but also a cash value in the policy that can be used in various ways while the insured is still alive .	I-restatement	1684..2094
If all you want is death-benefit coverage , pure insurance -- a term policy -- gives you maximum bang for your buck , within limits .	B-conjunction	2097..2227
It 's much cheaper than permanent insurance bought at the same age .	I-conjunction	2228..2294
But " term " means just that ; the policy is written for a specific time period only and must be renewed when it expires .	O	2297..2415
It may also stipulate that the insured must pass another medical exam before renewal ; if you flunk -- which means you need insurance more than ever -- you may not be able to buy it .	O	2416..2597
Even if you 're healthy and can renew , your premium will go up sharply because you 're that much older .	B-cause	2598..2699
So term insurance may not be as cheap as it looks .	I-cause	2700..2750
There are all sorts of variations on term insurance : policies structured to pay off your mortgage debt , term riders tacked on to permanent insurance , and many others .	B-entrel	2753..2919
One variation that appealed to me at first was the " Money Smart Term Life " policy offered by Amex Life Insurance Co. , the American Express unit , to the parent company 's credit-card holders .	B-contrast	2920..3109
Upon examination , however , I wondered whether the plan made a lot of sense .	I-contrast	3110..3185
Amex said it would charge me $ 576 a year for $ 100,000 of coverage -- and would pay me back all the premiums I put in if I canceled the policy after 10 years .	B-cause	3188..3345
Sounds great -- or does it ?	I-cause	3346..3373
First , if I canceled , I 'd have no more insurance , a not insignificant consideration .	O	3376..3460
Second , the $ 5,760 I 'd get back would be much diminished in purchasing power by 10 years of inflation ; Amex , not I , would get the benefit of the investment income on my money , income that would have exceeded the inflation rate and thus given the company a real profit .	O	3461..3729
Third and most important , Amex would charge me a far higher premium than other reputable companies would on a straight term policy for the same amount ; I 'd be paying so heavily just to have the option of getting my premiums back that I 'd almost have to cancel to make the whole thing worthwhile .	O	3732..4027
That would be all right with Amex , which could then lock in its investment profit , but it does n't add up to a " smart money " move for me .	O	4028..4164
Which goes to show that the First Law applies in insurance as in anything else : There is no free lunch , there is only marketing .	O	4167..4295
And the Second Law , unique to insurance ?	O	4298..4338
If I die early , I win -- a hollow victory , since I ca n't enjoy it -- and if I live long , the insurer wins .	B-entrel	4339..4445
Always .	I-entrel	4446..4453
This is worth remembering when insurers and their salesmen try to sell you permanent insurance , the kind that amasses cash value .	B-cause	4456..4585
The word " death " can not be escaped entirely by the industry , but salesmen dodge it wherever possible or cloak it in euphemisms , preferring to talk about " savings " and " investment " instead .	I-cause	4586..4774
The implication is that your permanent-insurance policy is really some kind of CD or mutual-fund account with an added feature .	O	4775..4902
That is gilding the lily .	B-cause	4905..4930
The fact is that as a savings or investment vehicle , insurance generally runs a poor second to any direct investment you might make in the same things the insurance company is putting your money into .	B-cause	4931..5131
That 's because you have to pay for the insurance portion of the policy and the effort required to sell and service the whole package .	B-cause	5132..5265
Again , no free lunch .	I-cause	5266..5287
This is reflected in a built-in mortality cost -- in effect , your share of the company 's estimated liability in paying off beneficiaries of people who had the effrontery to die while under its protection .	O	5290..5494
And in most cases , a huge hunk of your premium in the initial year or two of the the policy is , in effect , paying the salesman 's commission as well ; investment returns on most policies are actually negative for several years , largely because of this .	O	5495..5745
So view permanent insurance for what it is -- a compromise between pure insurance and direct investment .	O	5748..5852
The simplest , most traditional form of permanent insurance is the straight whole life policy .	B-restatement	5855..5948
You pay a set premium for a set amount of coverage , the company invests that premium in a portfolio of its choosing , and your cash value and dividends grow over the years .	I-restatement	5949..6120
One newer wrinkle , so called single-premium life ( you pay for the whole policy at once ) , has been immensely popular in recent years for tax reasons ; the insured could extract cash value in the form of policy " loans , " and none of the proceeds were taxable even though they included gains on investment .	O	6123..6424
Congress closed this loophole last year , or thought it did .	B-contrast	6427..6486
However , Monarch Capital Corp. of Springfield , Mass. , has developed a " combination plan " of annuity and insurance coverage that it says does not violate the new regulations and that allows policy loans without tax consequences .	B-contrast	6487..6714
But the percentage of your cash reserve that you can borrow tax-free is very small .	I-contrast	6715..6798
I 'm not prepared in any case to put that much money into a policy immediately , so I look into the broad category called universal life .	O	6801..6936
Hugely popular , it is far more flexible than straight whole life .	O	6937..7002
I can adjust the amount of insurance I want against the amount going into investment ; I can pay more or less than the so-called target premium in a given year ; and I can even skip payments if my cash reserves are enough to cover the insurance portion of the policy .	O	7003..7268
In looking at these and other policies , I learn to ask pointed questions about some of the assumptions built into " policy illustrations " -- the rows of numbers that show me the buildup of my cash values over the years .	B-entrel	7271..7489
They commonly give two scenarios : One is based on interest rates that the company guarantees ( usually 4 % to 4.5 % ) and the other on the rate it is currently getting on investment , often 8.5 % or more .	I-entrel	7490..7688
Projecting the latter over several decades , I find my cash buildup is impressive -- but can any high interest rate prevail for that long ?	O	7691..7828
Not likely , I think .	B-conjunction	7829..7849
Also , some policy illustrations assume that mortality costs will decline or that I will get some sort of dividend bonus after the 10th year .	B-contrast	7850..7990
These are not certain , either .	I-contrast	7991..8021
Companies " are n't comfortable playing these games , but they realize they 're under pressure to make their policies look good , " says Timothy Pfiefer , an actuarial consultant at Tillinghast , a unit of Towers Perrin Co. , the big New York consulting firm .	B-entrel	8024..8274
Another factor to consider : Some of the companies currently earning very high yields are doing so through substantial investment in junk bonds , and you know how nervous the market has been about those lately .	I-entrel	8275..8483
There are seemingly endless twists to universal life , and it pays to ask questions about all of them .	O	8486..8587
At a back-yard barbecue , for example , a friend boasts that she 'll only have to pay premiums on her John Hancock policy for seven years and that her death benefits will then be " guaranteed . "	B-cause	8588..8777
I call her agent , David Dominici .	I-cause	8778..8811
Yes , he says , premiums on such variable-rate coverage can be structured to " vanish " after a certain period -- but usually only if interest rates stay high enough to generate sufficient cash to cover the annual cost of insurance protection .	B-contrast	8814..9053
If interest rates plunge , the insurer may be knocking on my door , asking for steeper premium payments to maintain the same amount of protection .	B-entrel	9054..9198
I do n't like the sound of that .	I-entrel	9199..9230
Some insurers have also started offering " persistency bonuses , " such as extra dividends or a marginally higher interest yield , if the policy is maintained for 10 years .	O	9233..9401
But Glenn Daily , a New York-based financial consultant , warns that many of these bonuses are " just fantasies , " because most are n't guaranteed by the companies .	O	9402..9561
And the feature is so new , he adds , that no insurer has yet established a track record for actually making such payments .	B-conjunction	9562..9683
So-called living-benefits provisions also merit a close inspection .	I-conjunction	9686..9753
Offered by insurers that include Security-Connecticut Life Insurance Co. , Jackson National Life Insurance Co. , and National Travelers Life Insurance Co. , these policy riders let me tap a portion of my death benefits while I 'm still alive .	O	9754..9992
Some provisions would let me collect a percentage of the policy 's face value to pay for long-term care such as nursing-home stays ; others would allow payments for catastrophic illnesses and conditions such as cancer , heart attarcks , renal failure and kidney transplants .	B-contrast	9993..10263
But the catastrophic events for which the policyholder can collect are narrowly defined , vary from policy to policy , and generally permit use of only a small fraction of the face amount of insurance .	B-conjunction	10266..10465
Also , financial planners advising on insurance say that to their knowledge there has not yet been a tax ruling exempting these advance payments from taxes .	I-conjunction	10466..10621
And considering the extra cost of such provisions , some figure that people interested in , say , paying for extended nursing-home care would be better off just buying a separate policy that provides it .	O	10622..10822
I 'm more favorably impressed by " no-load life , " even though it turns out to be low-load life .	O	10825..10918
Insureres selling these policies market them directly to the public or otherwise do n't use commissioned salesmen ; there is still a load -- annual administrative fees and initial " setup " charges -- but I figure that the lack of commission and of " surrender fees " for dropping the policy early still saves me a lot .	O	10919..11232
I compared one universal policy for $ 130,000 face amount from such an insurer , American Life Insurance Corp. of Lincoln , Neb. , with a similar offering from Equitable Life Assurance Society of the U.S. , which operates through 11,000 commissioned salesmen .	B-restatement	11235..11489
After one year I could walk away from the Ameritas policy with $ 792 , but Id get only $ 14 from the Equitable .	B-conjunction	11490..11598
The difference is magnified by time , too .	B-restatement	11599..11640
At age 65 , when I 'd stop paying premiums , the Ameritas offering would have a projected cash value $ 14,000 higher than the other , even though the Equitable 's policy illustration assumed a fractionally higher interest rate .	I-restatement	11641..11862
Did I buy it ?	O	11865..11878
Well , not yet .	B-alternative	11879..11893
I 'm thinking about using the $ 871 annual premium to finance a trip to Paris first .	B-cause	11894..11976
A person can do some heavy thinking about insurance there -- and shop for something more exciting while she 's doing it .	I-cause	11977..12096
