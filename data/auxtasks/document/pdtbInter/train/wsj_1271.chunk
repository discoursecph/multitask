About 400,000 commuters trying to find their way through the Bay area's quake-torn transportation system wedged cheek-to-jowl into subways, sat in traffic jams on major freeways or waited forlornly for buses yesterday.	B-restatement	9..227
In other words, it was a better-than-average Manhattan commute.	I-restatement	230..293
City officials feared widespread gridlock on the first day that normal business operations were resumed following last Tuesday's earthquake.	B-cause	296..436
The massive temblor, which killed at least 61 people, severed the Bay Bridge, a major artery to the east, and closed most ramps leading to and from Highway 101, the biggest artery to the south.	B-entrel	437..630
It will take several weeks to repair the bridge, and several months to repair some of the 101 connections.	I-entrel	631..737
But in spite of a wind-driven rainstorm, gridlock never materialized, mainly because the Bay Area Rapid Transit subway system carried 50% more passengers than normal.	O	740..906
For the first time in memory, it was standing-room only in BART's sleek, modern railcars.	O	907..996
Moreover, the two main bridges still connecting San Francisco with the East Bay didn't charge tolls, allowing traffic to zip through without stopping.	O	999..1149
Officials also suspect that traffic benefited from steps by major employers to get workers to come in at odd hours, or that many workers are still staying at home.	O	1150..1313
Many commuters who normally drove across the Bay Bridge, which is shut down for several weeks because of damage to one span, actually may have reached work a bit faster on BART yesterday, provided they could find a parking space at the system's jammed stations.	O	1316..1577
In the best of times, the Bay Bridge is the worst commute in the region, often experiencing back-ups of 20 to 30 minutes or more.	O	1578..1707
Not that getting into town was easy.	B-instantiation	1710..1746
Storm flooding caused back-ups on the freeway, and many commuters had to find rides to BART's stations, because parking lots were full before dawn.	I-instantiation	1747..1894
Bus schedules were sometimes in disarray, stranding commuters such as Marilyn Sullivan.	B-entrel	1895..1982
Her commute from Petaluma, Calif., normally takes an hour and 15 minutes, via the Golden Gate Bridge, which connects San Francisco with the North Bay area.	I-entrel	1983..2138
Yesterday, she was still waiting at a bus stop after three hours, trying to transfer to a bus going to the financial district.	O	2139..2265
"It's worse than I thought," she said.	O	2268..2306
"I don't know where all the buses are."	O	2307..2346
But while traffic was heavy early in the commute over the Golden Gate, by 8 a.m. it already had thinned out.	O	2349..2457
"It's one of the smoothest commutes I've ever had," said Charles Catania, an insurance broker on the bus from Mill Valley in Marin County.	O	2458..2596
"It looks like a holiday.	O	2597..2622
I think a lot of people got scared and stayed home."	O	2623..2675
However, a spokeswoman for BankAmerica Corp. said yesterday's absenteeism at the bank holding company was no greater than on an average day.	O	2676..2816
At the San Mateo Bridge, which connects the San Francisco peninsula with the East Bay, police were surprised at the speed with which traffic moved.	O	2819..2966
"Everybody pretty much pitched in and cooperated," said Stan Perez, a sergeant with the California Highway Patrol.	O	2967..3081
There were many indications that the new work hours implemented by major corporations played a big role.	B-cause	3084..3188
The Golden Gate handled as many cars as normally yesterday, but over four hours rather than the usual two-hour crush.	I-cause	3189..3306
Bechtel Group Inc., the giant closely held engineering concern, says it has instituted a 6 a.m. to 8 p.m. flextime arrangement, whereby employees may select any eight-hour period during those hours to go to work.	B-entrel	3309..3521
Of Bechtel's 17,500 employees, about 4,000 work in San Francisco -- one-third of them commuting from stricken East Bay.	I-entrel	3522..3641
Pacific Gas & Electric Co. is offering its 6,000 San Francisco employees a two-tier flextime schedule -- either 6 a.m. to 2 p.m. or 10 a.m. to 6 p.m.	O	3644..3793
The flextime may cut by almost a third the number of PG&E employees working conventional 9-5 hours, a spokesman says.	O	3794..3911
Some of the utility's employees may opt for a four-day workweek, 10 hours a day, to cut the commute by 20%.	O	3912..4019
At Pacific Telesis Group, flextime is left up to individual working groups, because some of the telephone company's employees must be on-site during normal business hours, a spokeswoman says.	O	4022..4213
Some individuals went to some lengths on their own to avoid the anticipated gridlock.	O	4216..4301
Some individuals went to some lengths on their own to avoid the anticipated gridlock.One senior vice president at Bechtel said he got up at 3 a.m. to drive into San Francisco from the East Bay.	B-concession	4302..4410
But transportation officials worry that such extraordinary measures and cooperation may not last.	I-concession	4413..4510
Although one transportation official said drivers who didn't use car pools were committing "an anti-social act," about two-thirds of the motorists crossing the Golden Gate were alone, compared with the normal 70% rate.	O	4511..4729
And some commuters, relieved by the absence of gridlock, were planning to return to their old ways.	O	4730..4829
