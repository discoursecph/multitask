In a nondescript office building south of Los Angeles , human behavior is being monitored , dissected and , ultimately , manipulated .	O	9..138
A squiggly line snakes across a video screen , gyrating erratically as subjects with hand-held computers register their second-by-second reactions to a speaker 's remarks .	B-conjunction	141..310
Agreement , disapproval , boredom and distraction all can be inferred from the subjects ' twist of a dial .	B-conjunction	311..414
In another experiment , an elaborate chart with color codes reveals how people 's opinions were shaped -- and how they can be reshaped .	I-conjunction	415..548
Donald Vinson , who oversees the experiments , is n't some white-coated researcher .	O	551..631
He heads Litigation Sciences Inc. , the nation 's largest legal consulting firm , which is helping corporate America prepare for high-stakes litigation by predicting and shaping jurors ' reactions .	B-synchrony	632..825
In the process , Litigation Sciences is quietly but inexorably reshaping the world of law .	I-synchrony	826..915
Little known outside the legal world but a powerhouse within , Litigation Sciences , a unit of Saatchi & Saatchi PLC , employs more than 100 psychologists , sociologists , marketers , graphic artists and technicians .	B-restatement	918..1128
Twenty-one of its workers are Ph. D.s .	B-entrel	1129..1167
Among other services , the firm provides pre-trial opinion polls , creates profiles of " ideal " jurors , sets up mock trials and " shadow " juries , coaches lawyers and witnesses , and designs courtroom graphics .	I-entrel	1168..1372
Much like their cohorts in political consulting and product marketing , the litigation advisers encourage their clients to play down complex or ambiguous matters , simplify their messages and provide their target audiences with a psychological craving to make the desired choice .	B-entrel	1375..1652
With jury verdicts getting bigger all the time , companies are increasingly willing to pay huge sums for such advice .	I-entrel	1653..1769
Recently , Litigation Sciences helped Pennzoil Co. win a $ 10.5 billion jury verdict against Texaco Inc .	B-conjunction	1772..1874
It advised the National Football League in its largely successful defense of antitrust charges by the United States Football League .	B-conjunction	1875..2007
And it helped win defense verdicts in product-liability suits involving scores of products , ranging from Firestone 500 tires to the anti-nausea drug Bendectin .	B-cause	2008..2167
Largely as a result , Litigation Sciences has more than doubled in size in the past two years .	B-conjunction	2168..2261
Its 1988 revenue was $ 25 million .	I-conjunction	2262..2295
Meanwhile , competitors are being spawned almost daily ; some 300 new businesses -- many just one-person shops -- have sprung up .	O	2298..2425
Mr. Vinson estimates the industry 's total revenues approach $ 200 million .	O	2426..2499
In any high-stakes case , you can be sure that one side or the other -- or even both -- is using litigation consultants .	O	2500..2619
Despite their ubiquity , the consultants are n't entirely welcome .	O	2622..2686
Some lawyers and scholars see the social scientists ' vision of the American jury system as a far cry from the ideal presented in civics texts and memorialized on the movie screen .	O	2687..2866
In the film classic " Twelve Angry Men , " the crucible of deliberations unmasks each juror 's bias and purges it from playing a role in the verdict .	B-cause	2867..3012
After hours of conflict and debate , that jury focuses on the facts with near-perfect objectivity .	B-cause	3013..3110
In real life , jurors may not always work that way , but some court observers question why they should n't be encouraged to do so rather than be programmed not to .	I-cause	3111..3271
Litigation consulting is , as New York trial attorney Donald Zoeller puts it , " highly manipulative . "	O	3274..3373
He adds , " The notion they try to sell is that juries do n't make decisions rationally .	O	3374..3459
But the effort is also being made to try and cause jurors not to decide things rationally .	B-cause	3460..3550
I find it troubling . "	I-cause	3551..3572
But Mr. Zoeller also acknowledges that consultants can be very effective .	O	3573..3646
" It 's gotten to the point where if the case is large enough , it 's almost malpractice not to use them , " he says .	O	3647..3758
Others complain that the consultants ' growing influence exacerbates the advantage of litigants wealthy enough to afford such pricey services .	O	3761..3902
" The affluent people and the corporations can buy it , the poor radicals { in political cases } get it free , and everybody in between is at a disadvantage , and that 's not the kind of system we want , " says Amitai Etzioni , a prominent sociologist who teaches at George Washington University .	O	3903..4189
Sophisticated trial consulting grew , ironically , from the radical political movements of the 1960s and 1970s before finding its more lucrative calling in big commercial cases .	B-instantiation	4192..4367
The Harrisburg 7 trial in 1972 , in which Daniel Berrigan and others were charged with plotting anti-war-related violence , was a landmark .	I-instantiation	4368..4505
In that case , a group of left-leaning sociologists interviewed 252 registered voters around Harrisburg .	O	4508..4611
The researchers discovered that Episcopalians , Presbyterians , Methodists and fundamantalist Protestants were nearly always against the defendants ; the lawyers resolved to try to keep them off the jury .	O	4612..4813
The defense also learned that college-educated people were uncharacteristically conservative about the Vietnam War .	B-cause	4816..4931
A more blue-collar panel became a second aim .	B-asynchronous	4932..4977
Ultimately , that carefully picked jury deadlocked with a 10-2 vote to acquit , and the prosecution decided not to retry the case .	B-restatement	4978..5106
Litigation consulting had arrived .	I-restatement	5107..5141
The fledgling science went corporate in 1977 when International Business Machines Corp. hired a marketing professor to help defend a complex antitrust case .	B-synchrony	5144..5300
The problem for IBM trial lawyers Thomas Barr and David Boies was how to make such a highly technical case understandable .	B-cause	5301..5423
As the trial progressed , they were eager to know if the jury was keeping up with them .	I-cause	5424..5510
The solution devised by the professor was to hire six people who would mirror the actual jury demographically , sit in on the trial and report their reactions to him .	O	5513..5678
He then briefed Messrs. Boies and Barr , who had the chance to tilt their next day 's presentation accordingly .	O	5679..5788
Thus , the " shadow " jury was born .	B-conjunction	5789..5822
Mr. Vinson , the professor , got the law bug and formed Litigation Sciences .	B-conjunction	5823..5897
( IBM won the case . )	I-conjunction	5898..5917
" The hardest thing in any complex case is to retain objectivity and , in some sense , your ignorance , " says Mr. Boies of Cravath , Swaine & Moore .	O	5920..6063
" What you look for in a shadow jury is very much what you do when you give an opening argument to your wife or a friend and get some response to it .	O	6064..6212
A shadow jury is a way to do that in a more systematic and organized way . "	O	6213..6287
The approach worked well in the recent antitrust case in which Energy Transportation Systems Inc. sued Santa Fe Pacific Corp. over the transport of semi-liquefied coal -- the kind of case likely to make almost anyone 's eyes glaze over .	O	6290..6525
Energy Transportation retained Litigation Sciences , at a cost of several hundred thousand dollars , to poll , pre-try , profile and shadow .	B-asynchronous	6528..6664
Just before the actual closing arguments , the firm put the case to a vote of the five shadow jurors , each of whom was being paid $ 150 a day .	B-restatement	6665..6805
The jurors , who did n't know which side had retained them , decided for Energy Transportation , and awarded $ 500 million in damages .	B-contrast	6806..6935
The real jury returned days later with a $ 345 million victory for Energy Transportation .	I-contrast	6936..7024
" It 's just like weather forecasting , " says Energy Transportation trial attorney Harry Reasoner of Vinson & Elkins .	O	7027..7141
" It 's often wrong , but it 's better than consulting an Indian rain dancer . "	O	7142..7216
Forecasting is only one part of Litigation Sciences ' work .	B-cause	7219..7277
Changing the outcome of the trial is what really matters .	B-conjunction	7278..7335
And to the uninitiated , some of the firm 's approaches may seem chillingly manipulative .	I-conjunction	7336..7423
Theoretically , jurors are supposed to weigh the evidence in a case logically and objectively .	O	7426..7519
Instead , Mr. Vinson says , interviews with thousands of jurors reveal that they start with firmly entrenched attitudes and try to shoe-horn the facts of the case to fit their views .	O	7520..7700
Pre-trial polling helps the consultants develop a profile of the right type of juror .	O	7703..7788
If it is a case in which the client seeks punitive damages , for example , depressed , underemployed people are far more likely to grant them .	O	7789..7928
Someone with a master 's degree in classical arts who works in a deli would be ideal , Litigation Sciences advises .	O	7929..8042
So would someone recently divorced or widowed .	B-contrast	8043..8089
( Since Litigation Sciences generally represents the defense , its job is usually to help the lawyers identify and remove such people from the jury . )	I-contrast	8090..8237
For personal-injury cases , Litigation Sciences seeks defense jurors who believe that most people , including victims , get what they deserve .	B-conjunction	8240..8379
Such people also typically hold negative attitudes toward the physically handicapped , the poor , blacks and women .	B-entrel	8380..8493
The consultants help the defense lawyers find such jurors by asking questions about potential jurors ' attitudes toward volunteer work , or toward particular movies or books .	I-entrel	8494..8666
Litigation Sciences does n't make moral distinctions .	B-cause	8669..8721
If a client needs prejudiced jurors , the firm will help find them .	I-cause	8722..8788
As Mr. Vinson explains it , " We do n't control the facts .	O	8789..8844
They are what they are .	O	8845..8868
But any lawyer will select the facts and the strategy to employ .	B-cause	8869..8933
In our system of advocacy , the trial lawyer is duty bound to present the best case he possibly can . "	I-cause	8934..9034
Once a jury is selected , the consultants often continue to determine what the jurors ' attitudes are likely to be and help shape the lawyers ' presentation accordingly .	B-conjunction	9037..9203
Logic plays a minimal role here .	B-alternative	9204..9236
More important are what LSI calls " psychological anchors " -- a few focal points calculated to appeal to the jury on a gut level .	I-alternative	9237..9365
In one personal-injury case , a woman claimed she had been injured when she slipped in a pool , but the fall did n't explain why one of her arms was discolored bluish .	B-cause	9368..9532
By repeatedly drawing the jury 's attention to the arm , the defense lawyers planted doubt about the origin of the woman 's injuries .	B-conjunction	9533..9663
The ploy worked .	B-cause	9664..9680
The defense won .	I-cause	9681..9697
In a classic defense of a personal-injury case , the consultants concentrate on encouraging the jury to shift the blame .	O	9700..9819
" The ideal defense in a case involving an accident is to persuade the jurors to hold the accident victim responsible for his or her plight , " Mr. Vinson has written .	O	9820..9984
Slick graphics , pre-tested for effectiveness , also play a major role in Litigation Sciences ' operation .	O	9987..10090
Studies show , the consultants say , that people absorb information better and remember it longer if they receive it visually .	O	10091..10215
Computer-generated videos help .	B-cause	10216..10247
" The average American watches seven hours of TV a day .	I-cause	10248..10302
They are very visually sophisticated , " explains LSI graphics specialist Robert Seltzer .	O	10303..10390
Lawyers remain divided about whether anything is wrong with all this .	B-instantiation	10393..10462
Supporters acknowledge that the process aims to manipulate , but they insist that the best trial lawyers have always employed similar tactics .	I-instantiation	10463..10604
" They may not have been able to articulate it all , but they did it , " says Stephen Gillers , a legal ethics expert at New York University law school .	O	10605..10752
" What you have here is intuition made manifest . "	O	10753..10801
Many lawyers maintain that all 's fair in the adversary system as long as no one tampers with the evidence .	B-conjunction	10804..10910
Others point out that lawyers in small communities have always had a feel for public sentiment -- and used that to advantage .	I-conjunction	10911..11036
Litigation consulting is n't a guarantee of a favorable outcome .	O	11039..11102
Litigation Sciences concedes that in one in 20 cases it was flatout wrong in its predictions .	O	11103..11196
A few attorneys offer horror stories of jobs botched by consultants or of overpriced services -- as when one lawyer paid a consultant ( not at Litigation Sciences ) $ 70,000 to interview a jury after a big trial and later read more informative interviews with the same jurors in The American Lawyer magazine .	O	11197..11502
Some litigators scoff at the notion that a sociologist knows more than they do about what makes a jury tick .	B-instantiation	11505..11613
" The essence of being a trial lawyer is understanding how people of diverse backgrounds react to you and your presentation , " says Barry Ostrager of Simpson Thacher & Bartlett , who recently won a huge case on behalf of insurers against Shell Oil Co .	B-entrel	11614..11862
He says he used consultants in the case but " found them to be virtually useless . "	I-entrel	11863..11944
But most lawyers accept that the marketplace has spoken .	B-conjunction	11947..12003
And the question remains whether the jury system can maintain its integrity while undergoing such a skillful massage .	B-entrel	12004..12121
For more than a decade , Mr. Etzioni , the sociologist , has been a leading critic of the masseurs .	I-entrel	12122..12218
" There 's no reason to believe that juries rule inappropriately , " he says .	O	12219..12292
" But the last thing you want to do is manipulate the subconscious to make them think better .	B-cause	12293..12385
What you then do is you make them think inappropriately . "	I-cause	12386..12443
To hamper the work of litigation scientists , he suggests that courts sharply limit the number of jurors that lawyers can remove from the jury panel through so-called peremptory challenges -- exclusions that do n't require explanations .	B-entrel	12446..12680
In most civil cases , judges allow each side three such challenges .	B-contrast	12681..12747
For complex cases , judges sometimes allow many more .	I-contrast	12748..12800
Mr. Etzioni also suggests forbidding anyone from gathering background information about the jurors .	B-cause	12803..12902
( Some courts release names and addresses , and researchers can drive by houses , look up credit ratings , and even question neighbors . )	I-cause	12903..13035
Furthermore , he says , psychologists should not be allowed to analyze jurors ' personalities .	O	13036..13127
Even some lawyers who have used consultants to their advantage see a need to limit their impact .	B-instantiation	13130..13226
Mr. Boies , the first lawyer to use Mr. Vinson 's services , cautions against courts ' allowing extensive jury questioning ( known as voir dire ) or giving out personal information about the jurors .	I-instantiation	13227..13419
" The more extensive the voir dire , the easier you make it for that kind of research to be effective , and I do n't think courts should lend themselves to that , " Mr. Boies says .	O	13420..13594
