Why ca n't we teach our children to read , write and reckon ?	root	9..67
It 's not that we do n't know how to	cause	68..102
because we do .	cause	103..118
It 's that we do n't want to .	restatement	119..146
And the reason we do n't want to is that effective education would require us to relinquish some cherished metaphysical beliefs about human nature in general and the human nature of young people in particular , as well as to violate some cherished vested interests .	conjunction	147..410
These beliefs so dominate our educational establishment , our media , our politicians , and even our parents that it seems almost blasphemous to challenge them .	conjunction	411..568
Here is an example .	norel	571..590
If I were to ask a sample of American parents , " Do you wish the elementary schools to encourage creativity in your children ? " the near-unanimous answer would be , " Yes , of course . "	entrel	591..770
But what do we mean , specifically , by " creativity " ?	contrast	771..822
No one can say .	conjunction	823..838
In practice , it ends up being equated with a " self-expression " that encourages the youngsters ' " self-esteem . "	contrast	839..948
The result is a generation of young people whose ignorance and intellectual incompetence is matched only by their good opinion of themselves .	cause	949..1090
The whole notion of " creativity " in education was ( and is ) part and parcel of a romantic rebellion against disciplined instruction , which was ( and is ) regarded as " authoritarian , " a repression and frustration of the latent talents and the wonderful , if as yet undefined , potentialities inherent in the souls of all our children .	norel	1093..1421
It is not surprising that parents find this romantic extravagance so attractive .	cause	1422..1502
Fortunately , these same parents do want their children to get a decent education as traditionally understood	norel	1505..1613
and they have enough common sense to know what that demands .	conjunction	1614..1675
Their commitment to " creativity " can not survive adolescent illiteracy .	cause	1676..1746
American education 's future will be determined by the degree to which we -- all of us -- allow this common sense to prevail over the illusions that we also share .	entrel	1747..1909
The education establishment will fight against common sense every inch of the way .	norel	1912..1994
The reasons are complex	entrel	1995..2018
but one simple reason ought not to be underestimated .	contrast	2019..2073
" Progressive education " ( as it was once called ) is far more interesting and agreeable to teachers than is disciplined instruction .	restatement	2074..2204
It is nice for teachers to think they are engaged in " personality development " and even nicer to minimize those irksome tests with often disappointing results .	norel	2207..2366
It also provides teachers with a superior self-definition as a " profession	conjunction	2367..2441
" since they will have passed courses in educational psychology and educational philosophy .	cause	2442..2533
I myself took such courses in college , thinking I might end up a schoolteacher .	conjunction	2534..2613
They could all fairly be described as " pap " courses .	contrast	2614..2666
But it is unfair to dump on teachers , as distinct from the educational establishment .	concession	2669..2754
I know many schoolteachers	cause	2755..2781
and , on the whole , they are seriously committed to conscientious teaching .	conjunction	2782..2856
They may not be among the " best and brightest " of their generation -- there are very few such people , by definition .	contrast	2857..2973
But they need not be to do their jobs well .	contrast	2974..3017
Yes , we all can remember one or two truly inspiring teachers from our school days .	contrast	3018..3100
But our education proceeded at the hands of those others , who were merely competent and conscientious .	contrast	3101..3203
In this sense , a teacher can be compared to one 's family doctor .	cause	3204..3268
If he were brilliant , he probably would not be a family doctor in the first place .	cause	3269..3351
If he is competent and conscientious , he serves us well .	contrast	3352..3408
Our teachers are not an important factor in our educational crisis .	norel	3411..3478
Whether they are or are not underpaid is a problem of equity ; it is not an educational problem .	cause	3479..3574
It is silly libel on our teachers to think they would educate our children better if only they got a few thousand dollars a year more .	restatement	3575..3709
It is the kind of libel the teachers ' unions do n't mind spreading , for their own narrow purposes .	contrast	3710..3807
It is also the kind of libel politicians find useful , since it helps them strike a friendly posture on behalf of an important constituency .	conjunction	3808..3947
But there is not one shred of evidence that , other things being equal , salary differentials result in educational differentials .	contrast	3948..4076
If there were such evidence , you can be sure you would have heard of it .	conjunction	4077..4149
If we wish to be serious about American education , we know exactly what to do -- and , just as important , what not to do .	norel	4152..4272
There are many successful schools scattered throughout this nation , some of them in the poorest of ghettos	cause	4273..4379
and they are all sending us the same message .	conjunction	4380..4426
Conversely , there are the majority of unsuccessful schools	contrast	4427..4485
and we know which efforts at educational reform are doomed beforehand .	conjunction	4486..4557
We really do know all we need to know	cause	4558..4595
if only we could assimilate this knowledge into our thinking .	condition	4596..4658
In this respect , it would be helpful if our political leaders were mute , rather than eloquently " concerned . "	norel	4661..4769
They are inevitably inclined to echo the conventional pap	cause	4770..4827
since this is the least controversial option that is open to them .	cause	4828..4895
Thus at the recent governors ' conference on education , Gov. Bill Clinton of Arkansas announced that " this country needs a comprehensive child-development policy for children under five . "	cause	4896..5082
A comprehensive development policy for governors over 30 would seem to be a more pressing need .	contrast	5083..5178
What Gov. Clinton is advocating , in effect , is extending the educational system down to the pre-kindergarten years .	cause	5179..5294
Whether desirable or not , this is a child-care program , not an educational program .	contrast	5295..5378
We know that very early exposure to schooling improves performance in the first grade , but afterward the difference is quickly washed away .	conjunction	5379..5518
Let us sum up what we do know about education and about those education reforms that do work and do n't work : -- " Parental involvement " is a bad idea .	norel	5521..5670
Parents are too likely to blame schools for the educational limitations of their children .	cause	5671..5761
Parents should be involved with their children 's education at home , not in school .	contrast	5762..5844
They should see to it that their kids do n't play truant ; they should make certain that the children spend enough time doing homework ; they should scrutinize the report card .	restatement	5845..6018
If parents are dissatisfied with a school , they should have the option of switching to another .	conjunction	6019..6114
-- " Community involvement " is an even worse idea .	norel	6117..6166
Here , the experience of New York City is decisive .	entrel	6167..6217
Locally elected school boards , especially in our larger cities , become the prey of ambitious , generally corrupt , and invariably demagogic local politicians or would-be politicians .	restatement	6218..6398
New York is in the process of trying to disengage itself from a 20-year-old commitment to this system of school governance	cause	6399..6521
even as Chicago and other cities are moving to institute it .	contrast	6522..6583
-- In most states , increasing expenditures on education , in our current circumstances , will probably make things worse , not better .	norel	6586..6717
The reason is simple : Education takes place in the classroom , where the influence of money is minimal .	cause	6718..6820
Decades of educational research tell us unequivocally that even smaller classes have zero effect on the academic performance of the pupils -- though they may sometimes be desirable for other reasons .	norel	6823..7022
The new money flows into the already top-heavy administrative structure , which busies itself piling more and more paper work on the teachers .	alternative	7023..7164
There is neither mystery nor paradox in the fact that as educational expenditures ( in real terms ) have increased sharply in the past quarter-of-a-century -- we now spend more per pupil than any other country in the world -- educational performance has declined .	cause	7165..7426
That is the way the system works .	cause	7427..7460
-- Students should move up the educational ladder as their academic potential allows .	norel	7463..7548
No student should be permitted to be graduated from elementary school without having mastered the 3 R 's at the level that prevailed 20 years ago .	contrast	7549..7694
This means " tracking , " whose main purpose is less to permit the gifted youngsters to flourish ( though that is clearly desirable ) than to ensure that the less gifted get the necessary grounding for further study or for entering the modern world of work .	cause	7695..7947
The notion that tracking is somehow " undemocratic " is absurd .	entrel	7948..8009
The purpose of education is to encourage young men and women to realize their full academic potential .	cause	8010..8112
No one in his right mind actually believes that we all have an equal academic potential .	concession	8113..8201
-- It is generally desirable to use older textbooks -- many of them , alas , out of print -- rather than newer ones .	norel	8204..8318
The latter are modish , trendy , often downright silly , and at best insubstantial .	cause	8319..8399
They are based on dubious psychological and sociological theories rather than on educational experience .	conjunction	8400..8504
One of the reasons American students do so poorly in math tests , as compared with British , French , German or Japanese students , is the influence of the " New Math " on American textbooks and teaching methods .	instantiation	8505..8711
Anyone who wants to appreciate just how bizarre this situation is -- with students who ca n't add or subtract " learning " the conceptual basis of mathematical theory -- should read the article by Caleb Nelson ( himself a recent math major at Harvard ) in the November American Spectator .	norel	8714..8997
-- Most important of all , schools should have principals with a large measure of authority over the faculty , the curriculum , and all matters of student discipline .	norel	9000..9163
Study after study -- the most recent from the Brookings Institution -- tells us that the best schools are those that are free of outside interference and are governed by a powerful head .	cause	9164..9350
With that authority , of course , goes an unambiguous accountability .	conjunction	9351..9418
Schools that are structured in this way produce students with higher morale and superior academic performance .	restatement	9419..9529
This is a fact	entrel	9530..9544
-- though , in view of all the feathers that are ruffled by this fact , it is not surprising that one hears so little about it .	contrast	9545..9670
Mr. Kristol , an American Enterprise Institute fellow , co-edits The Public Interest and publishes The National Interest .	norel	9673..9792
