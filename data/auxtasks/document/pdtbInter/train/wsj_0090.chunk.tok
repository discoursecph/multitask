Investors unsettled by the stock market 's gyrations can take some comfort in the predictable arrival of quarterly dividend checks .	B-restatement	9..139
That has been particularly true this year with many companies raising their payouts more than 10 % .	I-restatement	140..238
But do n't breathe too easy : Those dividend increases may signal trouble ahead for stock prices , some analysts warn .	O	241..356
In the past , they say , the strongest dividend growth has often come at times when the stock-market party was almost over .	O	359..480
That can be a trap for unwary investors , says Richard Bernstein , senior quantitative analyst at Merrill Lynch & Co .	O	483..598
Strong dividend growth , he says , is " the black widow of valuation " -- a reference to the female spiders that attract males and then kill them after mating .	O	599..754
Stephen Boesel , president of T. Rowe Price Growth and Income Fund , explains that companies raise their payouts most robustly only after the economy and corporate profits have been growing for some time .	O	757..959
" Invariably , those strong periods in the economy give way to recessionary environments , " he says .	O	960..1057
" And recessionary environments are n't hospitable to the stock market . "	O	1058..1128
Indeed , analysts say that payouts have sometimes risen most sharply when prices were already on their way down from cyclical peaks .	O	1131..1262
In 1976 , for example , dividends on the stocks in Standard & Poor 's 500-stock index soared 10 % , following much slower growth the year before .	O	1263..1403
The S&P index started sliding in price in September 1976 , and fell 12 % in 1977 -- despite a 15 % expansion in dividends that year .	O	1404..1533
That pattern has n't always held , but recent strong growth in dividends makes some market watchers anxious .	O	1536..1642
Payouts on the S&P 500 stocks rose 10 % in 1988 , according to Standard & Poor 's Corp. , and Wall Street estimates for 1989 growth are generally between 9 % and 14 % .	B-entrel	1643..1804
Many people believe the growth in dividends will slow next year , although a minority see double-digit gains continuing .	I-entrel	1805..1924
Meanwhile , many market watchers say recent dividend trends raise another warning flag : While dividends have risen smartly , their expansion has n't kept pace with even stronger advances in stock prices .	O	1927..2127
As a result , the market 's dividend yield -- dividends as a percentage of price -- has slid to a level that is fairly low and unenticing by historical standards .	O	2128..2288
Put another way , the decline in the yield suggests stocks have gotten pretty rich in price relative to the dividends they pay , some market analysts say .	O	2291..2443
They are keeping a close watch on the yield on the S&P 500 .	B-entrel	2444..2503
The figure is currently about 3.3 % , up from 3.2 % before the recent market slide .	B-entrel	2504..2584
Some analysts say investors should run for the exits if a sustained market rebound pushes the yield below 3 % .	I-entrel	2585..2694
A drop below that 3 % benchmark " has always been a strong warning sign that stocks are fully valued , " says Mr. Boesel of T. Rowe Price .	O	2697..2831
In fact , " the market has always tanked .	B-restatement	2834..2873
Always .	I-restatement	2874..2881
There 's never been an exception , " says Gerald W. Perritt , a Chicago investment adviser and money manager , based on a review of six decades of stock-market data .	O	2882..3042
The last time the S&P 500 yield dropped below 3 % was in the summer of 1987 .	B-asynchronous	3045..3120
Stockholders who took the hint and sold shares escaped the October debacle .	I-asynchronous	3121..3196
There have been only seven other times -- in 1929 , 1933 , 1961 , 1965 , 1968 , 1971 and 1972 -- when the yield on the S&P 500 dropped below 3 % for at least two consecutive months , Mr. Perritt found .	O	3199..3393
And in each case , he says , a sharp drop in stock prices began within a year .	O	3394..3470
Still , some market analysts say the current 3.3 % reading is n't as troublesome as it might have been in years past .	O	3473..3587
" It 's not a very meaningful indicator currently because corporations are not behaving in a traditional manner , " says James H. Coxon , head of stock investments for Cigna Corp. , the Philadelphia-based insurer .	O	3588..3795
In particular , Mr. Coxon says , businesses are paying out a smaller percentage of their profits and cash flow in the form of dividends than they have historically .	O	3798..3960
So , while stock prices may look fairly high relative to dividends , they are not excessive relative to the underlying corporate strength .	O	3961..4097
Rather than increasing dividends , some companies have used cash to buy back some of their shares , notes Steven G. Einhorn , co-chairman of the investment policy committee at Goldman , Sachs & Co .	O	4100..4293
He factors that into the market yield to get an adjusted yield of about 3.6 % .	O	4294..4371
That is just a tad below the average of the past 40 years or so , he says .	O	4372..4445
What will happen to dividend growth next year ?	O	4448..4494
Common wisdom suggests a single-digit rate of growth , reflecting a weakening in the economy and corporate profits .	O	4495..4609
PaineWebber Inc. , for instance , is forecasting growth in S&P 500 dividends of just under 5 % in 1990 , down from an estimated 11 % this year .	O	4612..4750
In other years in which there have been moderate economic slowdowns -- the environment the firm expects in 1990 -- the change in dividends ranged from a gain of 4 % to a decline of 1 % , according to PaineWebber analyst Thomas Doerflinger .	O	4751..4988
The minority argument , meanwhile , is that businesses have the financial wherewithal this time around to declare sharply higher dividends even if their earnings weaken .	O	4991..5158
Dividend growth on the order of 12 % is expected by both Mr. Coxon of Cigna and Mr. Einhorn of Goldman Sachs .	O	5161..5269
Those dividend bulls argue that corporations are in the unusual position of having plenty of cash left over after paying dividends and making capital expenditures .	O	5270..5433
One indicator investors might want to watch is the monthly tally from Standard & Poor 's of the number of public companies adjusting their dividends .	O	5436..5584
A total of 139 companies raised dividends in October , basically unchanged from 138 a year ago , S&P said Wednesday .	O	5585..5699
That followed four straight months in which the number of increases trailed the year-earlier pace .	O	5700..5798
While the S&P tally does n't measure the magnitude of dividend changes , a further slippage in the number of dividend increases could be a harbinger of slower dividend growth next year .	O	5801..5984
In any case , opinion is mixed on how much of a boost the overall stock market would get even if dividend growth continues at double-digit levels .	O	5987..6132
Mr. Einhorn of Goldman Sachs estimates the stock market will deliver a 12 % to 15 % total return from appreciation and dividends over the next 12 months -- vs. a " cash rate of return " of perhaps 7 % or 8 % if dividend growth is weak .	B-contrast	6133..6362
But Mr. Boesel of T. Rowe Price , who also expects 12 % growth in dividends next year , does n't think it will help the overall market all that much .	I-contrast	6365..6510
" Having the dividend increases is a supportive element in the market outlook , but I do n't think it 's a main consideration , " he says .	O	6511..6643
With slower economic growth and flat corporate earnings likely next year , " I would n't look for the market to have much upside from current levels .	O	6644..6790
