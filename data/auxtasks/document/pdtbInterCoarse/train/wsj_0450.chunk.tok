An ancient red-figured Greek kylix , or drinking cup , was recovered backstage at Sotheby 's this spring and has been returned to the Manhattan couple who lost it in a burglary three years ago .	B-expansion	9..199
Robert Guy , an associate curator at the Princeton Art Museum , was previewing a June antiquities sale at the auction house when he recognized the kylix , which he , as a specialist in Attic pottery and a careful reader of the Stolen Art Alert in " IFAR Reports , " knew was stolen .	I-expansion	200..475
The timing of his visit was fortuitous ; the man who had brought it in for an estimate had returned to collect it and was waiting in the hall .	O	476..617
To confirm Mr. Guy 's identification , Sotheby 's and IFAR exchanged photos by fax , and the waiting man , apparently innocent of knowledge that the kylix was stolen , agreed to release it .	O	618..801
The cup had been insured , and in short order it was given over to a Chubb & Son representative .	B-contingency	802..897
The original owners happily repaid the claim and took their kylix home .	I-contingency	898..969
A former curator of the Museum of Cartoon Art in Rye Brook , N.Y. , pleaded guilty in July to stealing and selling original signed and dated comic strips , among them 29 Dick Tracy strips by Chester Gould , 77 Prince Valiant Sunday cartoons by Hal Foster , and a dozen Walt Disney animation celluloids , according to Barbara Hammond , the museum 's director .	O	972..1322
He sold them well below market value to raise cash " to pay off mounting credit-card debts , " incurred to buy presents for his girlfriend , his attorney , Philip Russell , told IFAR .	O	1323..1500
The curator , 27-year-old Sherman Krisher of Greenwich , Conn. , had worked his way up from janitor in seven years at the museum .	B-entrel	1501..1627
The theft was discovered early this year , soon after Ms. Hammond took her post .	I-entrel	1628..1707
Sentencing was postponed on Aug. 18 , when Mr. Krisher was hospitalized for depression .	B-entrel	1710..1796
His efforts to get back the stolen strips had resulted in recovery of just three .	I-entrel	1797..1878
But on Oct. 6 , he had reason to celebrate .	B-contingency	1879..1921
Two days earlier , his attorney met in a Park Avenue law office with a cartoon dealer who expected to sell 44 of the most important stolen strips to Mr. Russell for $ 62,800 .	I-contingency	1922..2094
Instead , New York City police seized the stolen goods , and Mr. Krisher avoided jail .	O	2095..2179
He was sentenced to 500 hours of community service and restitution to the museum of $ 45,000 .	O	2180..2272
Authorities at London 's Heathrow Airport are investigating the disappearance of a Paul Gauguin watercolor , " Young Tahitian Woman in a Red Pareo , " that has two sketches on its verso ( opposite ) side .	B-entrel	2275..2472
Valued at $ 1.3 million , it was part of a four-crate shipment .	B-entrel	2473..2534
The air-waybill number was changed en route , and paper work showing that the crates had cleared customs was misplaced , so it was a week before three of the four crates could be located in a bonded warehouse and the Gauguin discovered missing .	B-contingency	2535..2777
Although Heathrow authorities have been watching a group of allegedly crooked baggage handlers for some time , the Gauguin may be " lost . "	I-contingency	2778..2914
Chief Inspector Peter Seacomb of the Criminal Investigation Department at the airport said , " It is not uncommon for property to be temporarily mislaid or misrouted . "	O	2915..3080
Officials at the University of Virginia Art Museum certainly would agree .	B-temporal	3083..3156
Their museum had purchased an Attic black-figured column krater and shipped it from London .	B-comparison	3157..3248
It was reported stolen in transit en route to Washington , D.C. , in February .	B-comparison	3249..3325
Months later , the Greek vase arrived in good condition at the museum in Charlottesville , having inexplicably traveled by a circuitous route through Nairobi .	I-comparison	3326..3482
Two Mexican college dropouts , not professional art thieves , have been arrested for a 1985 Christmas Eve burglary from the National Museum of Anthropology in Mexico City .	B-temporal	3485..3654
About 140 Mayan , Aztec , Mixtec and Zapotec objects , including some of Mexico 's best-known archaeological treasures , were taken .	B-temporal	3655..3782
The government offered a reward for the return of the antiquities , but routine police work led to the recovery .	I-temporal	3783..3894
As it turned out , Carlos Perches Trevino and Ramon Sardina Garcia had hidden the haul in a closet in the Perches family 's home for a year .	O	3895..4033
Then they took the art to Acapulco and began to trade some of it for cocaine .	B-temporal	4034..4111
Information from an arrested drug trafficker led to the two men and the recovery of almost all the stolen art .	I-temporal	4112..4222
Among other happy news bulletins from the German Democratic Republic , the Leipzig Museum of Fine Arts announced that it has recovered " Cemetery in the Snow , " a painting by the German Romantic painter Caspar David Friedrich .	O	4225..4448
The artist 's melancholy subjects bring high prices on the world market , and the U.S. State Department notified IFAR of the theft in February 1988 .	B-entrel	4449..4595
According to a source at the East Europe desk , two previously convicted felons were charged , tried , convicted and sentenced to prison terms of four and 12 years .	I-entrel	4596..4757
The precious canvas , cut from its frame at the time of the theft , was found in nearby Jena , hidden in the upholstery of an easy chair in the home of the girlfriend of one of the thieves .	B-comparison	4760..4946
No charges were brought against her .	I-comparison	4947..4983
Trompe l'oeil painting is meant to fool the eye , but Robert Lawrence Trotter , 35 , of Kennett Square , Pa. , took his fooling seriously .	B-expansion	4986..5119
He painted one himself in the style of John Haberle and sold it as a 19th-century original to antique dealers in Woodbridge , Conn .	I-expansion	5120..5250
Mr. Trotter 's painting showed a wall of wood boards with painted ribbons tacked down in a rectangle ; tucked behind the ribbons were envelopes , folded , faded and crumpled papers and currency .	B-entrel	5251..5441
Mr. Trotter 's fake Haberle was offered at a bargain price of $ 25,000 with a phony story that it belonged to his wife 's late aunt in New Canaan , Conn .	I-entrel	5442..5591
The dealers immediately showed their new acquisition to an expert and came to see it as a fake .	B-temporal	5594..5689
They persuaded Mr. Trotter to take it back and , with the help of the FBI , taped their conversation with him .	B-expansion	5690..5798
After his arrest , the forger admitted to faking and selling other paintings up and down the Eastern seaboard .	I-expansion	5799..5908
Ms. Lowenthal is executive director of the International Foundation for Art Research ( IFAR ) .	O	5911..6003
