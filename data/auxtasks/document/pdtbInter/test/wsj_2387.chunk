The crowded field for notebook-sized computers is about to become a lot more crowded.	O	9..94
Compaq Computer Corp. 's long-awaited entry today into the notebook field is expected to put immediate heat on others in the market, especially Zenith Electronics Corp., the current market leader, and on a swarm of promising start-ups.	O	97..332
Compaq's series of notebooks extends a trend toward downsizing in the personal computer market.	B-instantiation	335..430
One manufacturer already has produced a clipboard-sized computer called a notepad, and two others have introduced even smaller "palmtops.	B-contrast	431..569
But those machines are still considered novelties, with keyboards only a munchkin could love and screens to match.	B-contrast	572..686
Compaq's notebooks, by contrast, may be the first in their weight class not to skimp on features found in much bigger machines.	I-contrast	687..814
Analysts say they're faster and carry more memory than anything else of their size on the market -- and they're priced aggressively at $2,400 to $5,000.	B-conjunction	815..967
All of this comes in a machine that weighs only six pounds and fits comfortably into most briefcases.	I-conjunction	968..1069
In recent months, Compaq's competition, including Zenith, Toshiba Corp., Tandy Corp. and NEC Corp. all have introduced portables that weigh approximately the same and that are called notebooks -- perhaps misleadingly.	O	1072..1289
One analyst, noting that most such machines are about two inches thick, takes exception to the name.	O	1290..1390
"This isn't quite a notebook -- I call it a phonebook," he says.	O	1391..1455
That can't be said of the $2,400 notepad computer introduced a few weeks ago by GRiD Systems Corp., a unit of Tandy.	B-cause	1458..1574
Instead of a keyboard, it features a writing surface, an electronic pen and the ability to "read" block printing.	B-cause	1575..1688
At 4 1/2 pounds, it may be too ambitiously named, but it nevertheless opens up the kind of marketing possibilities that make analysts froth.	I-cause	1689..1829
Palmtops aren't far behind.	O	1832..1859
Atari Corp. 's Portfolio, introduced in Europe two months ago and in the U.S. in early September, weighs less than a pound, costs a mere $400 and runs on three AA batteries, yet has the power to run some spreadsheets and word processing programs.	O	1860..2106
Some critics, however, say its ability to run commonplace programs is restricted by a limited memory.	O	2107..2208
Poquet Computer Corp., meanwhile, has introduced a much more sophisticated palmtop that can run Lotus 1-2-3 and other sophisticated software programs, but costs five times as much.	O	2211..2391
At stake is what Mike Swavely, Compaq's president of North America operations, calls "the Holy Grail of the computer industry" -- the search for "a real computer in a package so small you can take it everywhere.	B-contrast	2394..2606
The market is so new, nobody knows yet how big it can be.	I-contrast	2607..2664
"I've had a lot of people trying to sell me services to find out how big it is," says Tom Humphries, director of marketing for GRiD.	O	2665..2797
Whether it's $5 billion or $3.5 billion, it doesn't matter.	B-cause	2798..2858
It's huge."	I-cause	2859..2870
Consider the growth of portables, which now comprise 12% of all personal computer sales.	O	2873..2961
Laptops -- generally anything under 15 pounds -- have become the fastest-growing personal computer segment, with sales doubling this year.	O	2962..3100
Responding to that demand, however, has led to a variety of compromises.	B-restatement	3103..3175
Making computers smaller often means sacrificing memory.	B-conjunction	3176..3232
It also has precluded use of the faster, more powerful microprocessors found in increasing numbers of desktop machines.	B-conjunction	3233..3352
Size and weight considerations also have limited screen displays.	I-conjunction	3353..3418
The competitive sniping can get pretty petty at times.	B-instantiation	3421..3475
A Poquet spokesman, for example, criticizes the Atari Portfolio because it requires three batteries while the Poquet needs only two.	I-instantiation	3476..3608
Both palmtops are dismissed by notebook makers, who argue that they're too small -- a problem Poquet also encountered in focus groups, admits Gerry Purdy, director of marketing.	O	3609..3786
Poquet, trying to avoid the "gadget" label, responded with the tag line, "The Poquet PC -- a Very Big Computer."	O	3787..3899
Despite the sniping, few question the inevitability of the move to small machines that don't make compromises.	O	3902..4012
Toward that end, experts say the real battle will take place between center-stage players like Toshiba, Zenith and now Compaq.	O	4013..4139
Compaq's new machines are considered a direct threat to start-up firms like Dynabook Inc., which introduced in June a computer that, like Compaq's, uses an Intel 286 microprocessor and has a hard disk drive.	O	4142..4349
But the Dynabook product is twice as heavy and costs more than Compaq's.	O	4350..4422
Compaq's announcement also spells trouble for Zenith, which last year had 28% of the U.S. laptop market but recently agreed to sell its computer business to Cie. des Machines Bull, the French government-owned computer maker.	B-entrel	4425..4649
Zenith holders will vote in December on the proposed $635 million sale, a price that could slip because it is pegged to Zenith's share and sales.	I-entrel	4650..4795
Compaq is already taking aim at Zenith's market share.	B-instantiation	4798..4852
Rod Canion, Compaq's president and chief executive officer, notes pointedly that Zenith's $2,000 MinisPort uses an "unconventional" two-inch floppy disk, whereas Compaq's new machines use the more common 3 1/2-inch disk.	I-instantiation	4853..5073
John P. Frank, president of Zenith Data Systems, simply shrugs off such criticism, noting that 3 1/2-inch floppies were also "unconventional" when they first replaced five-inch disks.	O	5074..5257
"We don't look at it as not being a standard, we look at it as a new standard," he argues.	O	5258..5348
Analysts don't see it that way.	B-instantiation	5351..5382
Analysts don't see it that way. "I can't imagine that you'll talk to anyone who won't tell you this is dynamite for Compaq and a stopper for everyone else," says Gene Talsky, president of Professional Marketing Management Inc.	B-conjunction	5383..5577
Adds Bill Lempesis, senior industry analyst for DataQuest, a high-technology market research firm: "We basically think that these are very hot products.	I-conjunction	5578..5730
The problem Compaq is going to have is that they won't be able to make enough of them."	O	5731..5818
Compaq's machines include the 3 1/2-inch floppy disk drive, a backlit screen that is only 1/4-inch thick and an internal expansion slot for a modem -- in other words, almost all the capabilities of a typical office machine.	O	5821..6044
Others undoubtedly will follow, but most analysts believe Compaq has at least a six-month lead on the competition.	O	6047..6161
Toshiba's line of portables, for example, features the T-1000, which is in the same weight class but is much slower and has less memory, and the T-1600, which also uses a 286 microprocessor, but which weighs almost twice as much and is three times the size.	O	6162..6419
A third model, marketed in Japan, may hit the U.S. by the end of the first quarter of 1990, but by then, analysts say, Compaq will have established itself as one of three major players.	O	6420..6605
What about Big Blue?	O	6608..6628
International Business Machines Corp., analysts say, has been burned twice in trying to enter the laptop market and shows no signs of trying to get into notebooks anytime soon.	O	6629..6805
