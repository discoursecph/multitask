While corn and soybean prices have slumped well below their drought-induced peaks of 1988, wheat prices remain stubbornly high.	O	9..136
And they're likely to stay that way for months to come, analysts say.	O	137..206
For one thing, even with many farmers planting more winter wheat this year than last, tight wheat supplies are likely to support prices well into 1990, the analysts say.	O	209..378
And if rain doesn't fall soon across many of the Great Plains' wheat-growing areas, yields in the crop now being planted could be reduced, further squeezing supplies.	O	379..545
Also supporting prices are expectations that the Soviet Union will place substantial buying orders over the next few months.	O	546..670
By next May 31, stocks of U.S. wheat to be carried over into the next season -- before the winter wheat now being planted is harvested -- are projected to drop to 443 million bushels.	B-entrel	673..856
That would be the lowest level since the early 1970s.	B-entrel	857..910
Stocks were 698 million bushels on May 31 of this year.	I-entrel	911..966
In response to dwindling domestic supplies, Agriculture Secretary Clayton Yeutter last month said the U.S. government would slightly increase the number of acres farmers can plant in wheat for next year and still qualify for federal support payments.	O	969..1219
The government estimates that the new plan will boost production next year by about 66 million bushels.	B-cause	1220..1323
It now estimates production for next year at just under 2.6 billion bushels, compared with this year's estimated 2.04 billion and a drought-stunted 1.81 billion in 1988.	I-cause	1324..1493
But the full effect on prices of the winter wheat now being planted won't be felt until the second half of next year.	O	1496..1613
Until then, limited stocks are likely to keep prices near the $4-a-bushel level, analysts say.	O	1614..1708
On the Chicago Board of Trade Friday, wheat for December delivery settled at $4.0675 a bushel, unchanged.	O	1709..1814
In theory at least, tight supplies next spring could leave the wheat futures market susceptible to a supply-demand squeeze, said Daniel Basse, a futures analyst with AgResource Co. in Chicago.	B-entrel	1817..2009
Such a situation can wreak havoc, as was shown by the emergency that developed in soybean futures trading this summer on the Chicago Board of Trade.	I-entrel	2010..2158
In July, the CBOT ordered Ferruzzi Finanziaria S.p. A. to liquidate futures positions equal to about 23 million bushels of soybeans.	O	2161..2293
The exchange said it feared that some members wouldn't be able to find enough soybeans to deliver and would have to default on their contractual obligation to the Italian conglomerate, which had refused requests to reduce its holdings.	O	2294..2529
Ferruzzi has denied it was trying to manipulate the soybean futures market.	O	2530..2605
Unseasonably hot, dry weather across large portions of the Great Plains and in wheat-growing areas in Washington and Oregon is threatening to reduce the yield from this season's winter wheat crop, said Conrad Leslie, a futures analyst and head of Leslie Analytical in Chicago.	O	2608..2884
For example, in the Oklahoma panhandle, 40% or more of the topsoil is short of moisture.	O	2885..2973
That figure climbs to about 47% in wheat-growing portions of Kansas, he said.	O	2974..3051
The Soviet Union hasn't given any clear indication of its wheat purchase plans, but many analysts expect Moscow to place sizable orders for U.S. wheat in the next few months, further supporting prices.	O	3054..3255
"Wheat prices will increasingly pivot off of Soviet demand" in coming weeks, predicted Richard Feltes, vice president, research, for Refco Inc. in Chicago.	O	3256..3411
Looking ahead to other commodity markets this week:	O	3414..3465
Orange Juice   Traders will be watching to see how long and how far the price decline that began Friday will go.	O	3468..3580
Late Thursday, after the close of trading, the market received what would normally have been a bullish U.S. Department of Agriculture estimate of the 1989-90 Florida orange crop.	B-restatement	3583..3761
It was near the low range of estimates, at 130 million 90-pound boxes, compared with 146.6 million boxes last season.	I-restatement	3762..3879
However, as expected, Brazil waited for the crop estimate to come out and then cut the export price of its juice concentrate to about $1.34 a pound from around $1.55.	B-cause	3882..4048
Friday's consequent selling of futures contracts erased whatever supportive effect the U.S. report might have had and sent the November orange juice contract down as much as 6.55 cents a pound at one time.	I-cause	4049..4254
It settled with a loss of 4.95 cents at $1.3210 a pound.	O	4255..4311
Brazilian juice, after a delay caused by drought at the start of its crop season, is beginning to arrive in the U.S. in large quantities.	O	4314..4451
Brazil wants to stimulate demand for its product, which is going to be in plentiful supply.	O	4452..4543
The price cut, one analyst said, appeared to be aimed even more at Europe, where consumption of Brazilian juice has fallen.	O	4546..4669
It's a dollar-priced product, and the strong dollar has made it more expensive in Europe, the analyst said.	O	4670..4777
New York futures prices have dropped significantly from more than $2 a pound at midyear.	O	4780..4868
Barring a cold snap or other crop problems in the growing areas, downward pressure on prices is likely to continue into January, when harvesting and processing of oranges in Florida reach their peak, the analyst said.	O	4869..5086
Energy	O	5089..5095
Although some analysts look for profit-taking in the wake of Friday's leap in crude oil prices, last week's rally is generally expected to continue this week.	O	5098..5256
"I would continue to look for a stable crude market, at least in futures" trading, said William Hinton, an energy futures broker with Stotler & Co.	O	5259..5406
Friday capped a week of steadily rising crude oil prices in both futures and spot markets.	B-instantiation	5409..5499
On the New York Mercantile Exchange, West Texas Intermediate crude for November delivery finished at $20.89 a barrel, up 42 cents on the day.	B-synchrony	5500..5641
On European markets, meanwhile, spot prices of North Sea crudes were up 35 to 75 cents a barrel.	I-synchrony	5642..5738
This market still wants to go higher," said Nauman Barakat, a first vice president at Shearson Lehman Hutton Inc.	B-restatement	5741..5855
He predicted that the November contract will reach $21.50 a barrel or more on the New York Mercantile Exchange.	I-restatement	5856..5967
There has been little news to account for such buoyancy in the oil markets.	B-contrast	5970..6045
Analysts generally cite a lack of bearish developments as well as rumors of a possible tightening of supplies of some fuels and crudes.	B-conjunction	6046..6181
There also are recurring reports that the Soviet Union is having difficulties with its oil exports and that Nigeria has about reached its production limit and can't produce as much as it could sell.	B-cause	6182..6380
Many traders foresee a tightening of near-term supplies, particularly of high-quality crudes such as those produced in the North Sea and in Nigeria.	I-cause	6381..6529
