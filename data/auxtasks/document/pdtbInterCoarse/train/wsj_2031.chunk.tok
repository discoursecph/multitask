William Seidman , chairman of the Federal Deposit Insurance Corp. , said Lincoln Savings & Loan Association should have been seized by the government in 1986 to contain losses that he estimated will cost taxpayers as much as $ 2 billion .	O	9..243
Mr. Seidman , who has been the nation 's top bank regulator , inherited the problems of Lincoln , based in Irvine , Calif. , after his regulatory role was expanded by the new savings-and-loan bailout law .	B-entrel	246..444
He made his comments before House Banking Committee hearings to investigate what appears to be the biggest thrift disaster in a scandal-ridden industry .	I-entrel	445..597
The inquiry also will cover the actions of Charles Keating Jr. , who is chairman of American Continental Corp. , Lincoln 's parent , and who contributed heavily to several U.S. senators .	O	598..780
Mr. Seidman told the committee that the Resolution Trust Corp. , the agency created to sell sick thrifts , has studied Lincoln 's examination reports by former regulators dating back to 1986 .	B-entrel	783..971
" My staff indicated that had we made such findings in one of our own institutions , we would have sought an immediate cease-and-desist order to stop the hazardous operations , " Mr. Seidman said .	I-entrel	972..1164
When Lincoln was seized by the government , for example , 15 % of its loans , or $ 250 million , were to borrowers who were buying real estate from one of American Continental 's 50 other subsidiaries , according to Mr. Seidman .	O	1167..1387
But the government did n't step in until six months ago , when thrift officials put Lincoln into conservatorship -- the day after American Continental filed for Chapter 11 bankruptcy protection from creditors .	B-entrel	1390..1597
The bankruptcy filing , the government has charged in a $ 1.1 billion civil lawsuit , was part of a pattern to shift insured deposits to the parent company , which used the deposits as a cache for real-estate deals .	B-entrel	1598..1809
The deposits that have been transferred to other subsidiaries are now under the jurisdiction of the bankruptcy court .	B-entrel	1810..1927
" I think it 's fairly clear { Mr. Keating } knew , " that regulators were set to seize Lincoln , Mr. Seidman said .	I-entrel	1928..2036
Further investigation , he said , may result in further actions against Lincoln 's executives , said Mr. Seidman , " including fraud actions . "	O	2039..2175
Mr. Keating , for his part , has filed suit alleging that regulators unlawfully seized the thrift .	O	2176..2272
Leonard Bickwit , an attorney in Washington for Mr. Keating , declined to comment on the hearings , except to say , " We will be responding comprehensively in several forums to each of these allegations at the appropriate time . "	O	2275..2498
Lincoln 's treatment by former thrift regulators , in an agency disbanded by the new law , has proved embarrassing for five senators who received thousands of dollars in campaign contributions from Mr. Keating .	O	2501..2708
Mr. Seidman said yesterday , for example , that Sen. Dennis DeConcini ( D. , Ariz. ) , who received $ 48,100 in contributions from Mr. Keating , phoned Mr. Seidman to request that he push for a sale of Lincoln before it would be seized .	O	2709..2937
After the government lawsuit was filed against Lincoln , Sen. DeConcini returned the campaign contributions .	B-entrel	2940..3047
The senator 's spokesman said yesterday that he pushed for the sale of Lincoln because " hundreds of Arizona jobs { at Lincoln } were on the line . "	I-entrel	3048..3191
Senate Banking Committee Chairman Donald Riegle ( D. , Mich . ) has also returned contributions he received from Mr. Keating a year ago .	O	3194..3326
Sens. John Glenn ( D. , Ohio ) , John McCain , ( R. , Ariz. ) and Alan Cranston ( D. , Calif. ) also received substantial contributions from Mr. Keating and sought to intervene on behalf of Lincoln .	O	3327..3514
House Banking Committee Chairman Henry Gonzalez ( D. , Texas ) said Sen. Cranston volunteered to appear before the House committee , if necessary .	O	3517..3659
But a committee staff member said the panel is unlikely to pursue closely the role of the senators .	O	3660..3759
At the hearing , Mr. Seidman said the RTC has already pumped $ 729 million into Lincoln for liquidity .	B-expansion	3762..3862
He also held out little hope of restitution for purchasers of $ 225 million in American Continental subordinated debt .	B-entrel	3863..3980
Some of those debtholders have filed a suit , saying they believed they were buying government-insured certificates of deposit .	I-entrel	3981..4107
" We have no plans at this time to pay off those notes , " he said .	O	4108..4172
