General Motors Corp. 's general counsel hopes to cut the number of outside law firms	(NN-Topic-Shift(NS-interpretation(NS-comment(NS-enablement(NS-explanation-argumentative(NS-means(NS-reason(NN-Same-Unit(NS-elaboration-object-attribute
the auto maker uses	NS-elaboration-object-attribute)
from about 700 to 200 within two years .	NN-Same-Unit)
Harry J. Pearce ,	(SN-attribution(NN-Same-Unit(NS-elaboration-additional
named general counsel in May 1987 ,	NS-elaboration-additional)
says	NN-Same-Unit)
the reduction is a cost-cutting measure and an effort	(NS-elaboration-object-attribute
to let the No. 1 auto maker 's 134-lawyer in-house legal department take on matters	(NS-elaboration-object-attribute
it is better equipped and trained to handle .	NS-elaboration-object-attribute))))
GM trimmed about 40 firms from its approved local counsel list ,	(NS-attribution
Mr. Pearce says .	NS-attribution))
The move is consistent with a trend	(NS-elaboration-object-attribute
for corporate legal staffs to do more work in-house ,	(NN-Contrast
instead of farming it out to law firms .	NN-Contrast)))
Mr. Pearce set up GM 's first in-house litigation group in May with four lawyers , all former assistant U.S. attorneys with extensive trial experience .	(NN-List(NS-elaboration-additional(NS-elaboration-additional
He intends to add to the litigation staff .	NS-elaboration-additional)
Among the types of cases	(NN-Same-Unit(NS-elaboration-object-attribute
the in-house litigators handle	NS-elaboration-object-attribute)
are disputes	(NS-elaboration-object-attribute
involving companies	(NS-example(NS-elaboration-object-attribute
doing business with GM and product-related actions ,	NS-elaboration-object-attribute)
including one	(NS-elaboration-object-attribute
in which a driver is suing GM for damages	(NS-result
resulting from an accident .	NS-result))))))
Mr. Pearce has also encouraged his staff to work more closely with GM 's technical staffs	(NS-consequence(NS-purpose
to help prevent future litigation .	NS-purpose)
GM lawyers have been working with technicians	(NN-List(NS-explanation-argumentative(NS-purpose
to develop more uniform welding procedures	NS-purpose)
-- the way	(NN-Same-Unit(NS-elaboration-object-attribute
a vehicle is welded	NS-elaboration-object-attribute)
has a lot to do with its durability .	NN-Same-Unit))
The lawyers also monitor suits	(NS-purpose
to identify specific automobile parts	(NS-elaboration-object-attribute
that cause the biggest legal problems .	NS-elaboration-object-attribute))))))
Mr. Pearce says	(NS-interpretation(NS-restatement(SN-attribution
law firms with the best chance	(NN-Same-Unit(NS-elaboration-object-attribute
of retaining or winning business with GM	NS-elaboration-object-attribute)
will be those	(NS-elaboration-object-attribute
providing the highest-quality service at the best cost	NS-elaboration-object-attribute)))
-- echoing similar directives from GM 's auto operations to suppliers .	NS-restatement)
This does n't necessarily mean larger firms have an advantage ;	(NS-explanation-argumentative
Mr. Pearce said	(SN-attribution
GM works with a number of smaller firms	(NS-elaboration-object-attribute
it regards highly .	NS-elaboration-object-attribute)))))
Mr. Pearce has shaken up GM 's legal staff	(NS-means
by eliminating all titles	(NN-List
and establishing several new functions ,	(NS-elaboration-general-specific
including a special-projects group	(NS-elaboration-object-attribute
that has made films on safety and drunk driving .	NS-elaboration-object-attribute)))))
FEDERAL PROSECUTORS are concluding fewer criminal cases with trials .	(NN-Topic-Shift(NS-purpose(NN-List(NS-explanation-argumentative(NS-comment(NS-antithesis(NS-background(NS-conclusion(NS-attribution
That 's a finding of a new study of the Justice Department by researchers at Syracuse University .	NS-attribution)
David Burnham , one of the authors , says	(NS-evidence(SN-attribution
fewer trials probably means a growing number of plea bargains .	SN-attribution)
In 1980 , 18 % of federal prosecutions concluded at trial ;	(NN-Comparison
in 1987 , only 9 % did .	NN-Comparison)))
The study covered 11 major U.S. attorneys ' offices	(NN-Same-Unit(NS-elaboration-set-member
-- including those in Manhattan and Brooklyn , N.Y. , and New Jersey --	NS-elaboration-set-member)
from 1980 to 1987 .	NN-Same-Unit))
The Justice Department rejected the implication	(NS-explanation-argumentative(SN-attribution
that its prosecutors are currently more willing to plea bargain .	SN-attribution)
`` Our felony caseloads have been consistent for 20 years , ''	(NS-attribution(NS-manner
with about 15 % of all prosecutions going to trial ,	NS-manner)
a department spokeswoman said .	NS-attribution)))
The discrepancy is somewhat perplexing in that the Syracuse researchers said	(SN-attribution
they based their conclusions on government statistics .	SN-attribution))
`` One possible explanation for this decline ''	(NS-evidence(NN-Same-Unit(NS-attribution(NS-elaboration-object-attribute
in taking cases to trial ,	NS-elaboration-object-attribute)
says Mr. Burnham ,	NS-attribution)
`` is that the number of defendants	(NN-Same-Unit(NS-elaboration-object-attribute
being charged with crimes by all U.S. attorneys	NS-elaboration-object-attribute)
has substantially increased . ''	NN-Same-Unit))
In 1980 ,	(NN-Comparison(NN-Same-Unit(NS-attribution
the study says ,	NS-attribution)
prosecutors surveyed filed charges against 25 defendants for each 100,000 people	(NS-elaboration-object-attribute
aged 18 years and older .	NS-elaboration-object-attribute))
In 1987 , prosecutors filed against 35 defendants for every 100,000 adults .	NN-Comparison)))
Another finding from the study :	(NS-elaboration-general-specific
Prosecutors set significantly different priorities .	(NS-evidence
The Manhattan U.S. attorney 's office stressed criminal cases from 1980 to 1987 ,	(NN-List(NN-Contrast(NS-evaluation
averaging 43 for every 100,000 adults .	NS-evaluation)
But the New Jersey U.S. attorney averaged 16 .	NN-Contrast)
On the civil side , the Manhattan prosecutor filed an average of only 11 cases for every 100,000 adults during the same period ;	(NN-Contrast
the San Francisco U.S. attorney averaged 79 .	NN-Contrast)))))
The study is to provide reporters , academic experts and others raw data	(NS-elaboration-object-attribute
on which to base further inquiries .	NS-elaboration-object-attribute))
IMELDA MARCOS asks for dismissal ,	(NN-Topic-Shift(NS-elaboration-additional(NS-elaboration-additional(NS-circumstance(NS-summary(NN-List
says	(SN-attribution
she was kidnapped .	SN-attribution))
The former first lady of the Philippines , asked a federal court in Manhattan to dismiss an indictment against her ,	(NS-explanation-argumentative
claiming among other things ,	(SN-attribution
that she was abducted from her homeland .	SN-attribution)))
Mrs. Marcos and her late husband , former Philippines President Ferdinand Marcos , were charged with embezzling more than $ 100 million from that country and then fraudulently concealing much of the money through purchases of prime real estate in Manhattan .	NS-circumstance)
Mrs. Marcos 's attorneys asked federal Judge John F. Keenan to give them access to all U.S. documents about her alleged abduction .	(NS-interpretation
The U.S. attorney 's office , in documents	(SN-attribution(NN-Same-Unit(NS-elaboration-object-attribute
it filed in response ,	NS-elaboration-object-attribute)
said	NN-Same-Unit)
Mrs. Marcos was making the `` fanciful	(NN-Same-Unit(NS-elaboration-additional
-- and factually unsupported --	NS-elaboration-additional)
claim	(NS-elaboration-object-attribute
that she was kidnapped into this country ''	(NS-purpose
in order to obtain classified material in the case .	NS-purpose))))))
The office also said	(NN-Contrast(NS-evidence(SN-attribution
Mrs. Marcos and her husband were n't brought to the U.S. against their will	(NS-temporal-after
after Mr. Marcos was ousted as president .	NS-temporal-after))
The prosecutor quoted statements from the Marcoses	(NS-elaboration-object-attribute
in which they said	(SN-attribution
they were in this country at the invitation of President Reagan	(NN-List
and that they were enjoying the hospitality of the U.S .	NN-List))))
Lawyers for Mrs. Marcos say	(SN-attribution
that	(NN-Same-Unit(NS-circumstance
because she was taken to the U.S. against her wishes ,	NS-circumstance)
the federal court lacks jurisdiction in the case .	NN-Same-Unit))))
THE FEDERAL COURT of appeals in Manhattan ruled	(NN-Topic-Shift(NS-elaboration-additional(NN-Temporal-Same-Time(NS-explanation-argumentative(NS-elaboration-general-specific(SN-attribution
that the dismissal of a 1980 indictment against former Bank of Crete owner George Koskotas should be reconsidered .	SN-attribution)
The indictment ,	(NN-Same-Unit(NS-elaboration-additional
which was sealed	(NN-List
and apparently forgotten by investigators until 1987 ,	NN-List))
charges Mr. Koskotas and three others with tax fraud and other violations .	NN-Same-Unit))
He made numerous trips to the U.S. in the early 1980s ,	(NN-Contrast(SN-background(SN-antithesis
but was n't arrested until 1987	(NS-circumstance
when he showed up as a guest of then-Vice President George Bush at a government function .	NS-circumstance))
A federal judge in Manhattan threw out the indictment ,	(NS-reason
finding	(SN-attribution
that the seven-year delay violated the defendant 's constitutional right to a speedy trial .	SN-attribution)))
The appeals court , however , said	(SN-attribution
the judge did n't adequately consider	(SN-attribution
whether the delay would actually hurt the chances of a fair trial .	SN-attribution))))
Mr. Koskotas is fighting extradition proceedings	(NS-elaboration-general-specific(NS-elaboration-object-attribute
that would return him to Greece ,	NS-elaboration-object-attribute)
where he is charged with embezzling more than $ 250 million from the Bank of Crete .	NS-elaboration-general-specific))
His attorney could n't be reached for comment .	NS-elaboration-additional)
PRO BONO VOLUNTARISM :	(NN-Topic-Shift(NN-TextualOrganization
In an effort	(NS-evaluation(NS-elaboration-additional(NN-Same-Unit(NS-elaboration-object-attribute
to stave off a plan	(NS-elaboration-object-attribute
that would require all lawyers in New York state to provide twenty hours of free legal aid a year ,	NS-elaboration-object-attribute))
the state bar recommended an alternative program	(NS-elaboration-object-attribute
to increase voluntary participation in pro bono programs .	NS-elaboration-object-attribute))
The state bar association 's policy making body , the House of Delegate , voted Saturday to ask Chief Judge Sol Wachtler to give the bar 's voluntary program three years to prove its effectiveness	(NS-temporal-before
before considering mandatory pro bono .	NS-temporal-before))
`` We believe	(NS-attribution(SN-attribution
our suggested plan is more likely to improve the availability of quality legal service to the poor	(NN-List(NN-Comparison
than is the proposed mandatory pro bono plan	NN-Comparison)
and will achieve that objective	(NS-manner
without the divisiveness , distraction , administrative burdens and possible failure	(NS-elaboration-object-attribute
that	(NN-Same-Unit(NS-attribution
we fear	NS-attribution)
would accompany an attempt	(NS-elaboration-object-attribute
to impose a mandatory plan , ''	NS-elaboration-object-attribute))))))
said Justin L. Vigdor of Rochester ,	(NS-elaboration-additional
who headed the bar 's pro bono study committee .	NS-elaboration-additional))))
DALLAS AND HOUSTON law firms merge :	(NN-Topic-Shift(NN-TextualOrganization
Jackson & Walker , a 130-lawyer firm in Dallas and Datson & Scofield , a 70-lawyer firm in Houston said	(NS-elaboration-additional(NS-elaboration-additional(NS-consequence(SN-attribution
they have agreed in principle to merge .	SN-attribution)
The consolidated firm ,	(NN-Same-Unit(NS-elaboration-additional
which would rank among the 10 largest in Texas ,	NS-elaboration-additional)
would operate under the name Jackson & Walker .	NN-Same-Unit))
The merger must be formally approved by the partners of both firms	(SN-antithesis
but is expected to be completed by year end .	SN-antithesis))
Jackson & Walker has an office in Fort Worth , Texas ,	(NN-List
and Dotson & Scofield has an office in New Orleans .	NN-List)))
PILING ON ?	(NN-TextualOrganization
Piggybacking on government assertions	(NN-Contrast(NS-elaboration-additional(NS-elaboration-additional(NS-background(NS-elaboration-general-specific(NS-elaboration-additional(SN-circumstance(NS-elaboration-object-attribute
that General Electric Co. may have covered up fraudulent billings to the Pentagon ,	NS-elaboration-object-attribute)
two shareholders have filed a civil racketeering suit against the company .	SN-circumstance)
The suit was filed by plaintiffs ' securities lawyer Richard D. Greenfield in U.S. District Court in Philadelphia .	NS-elaboration-additional)
He seeks damages from the company 's 15 directors on grounds	(NS-elaboration-object-attribute
that they either `` participated in or condoned the illegal acts . . .	(NN-Disjunction
or utterly failed to carry out their duties as directors . ''	NN-Disjunction)))
GE is defending itself against government criminal charges of fraud and false claims in connection with a logistics-computer contract for the Army .	NS-background)
The trial begins today in federal court in Philadelphia .	NS-elaboration-additional)
The government 's assertions of the cover-up were made in last minute pretrial motions .	NS-elaboration-additional)
GE ,	(NS-elaboration-general-specific(NN-Same-Unit(NS-elaboration-additional
which vehemently denies the government 's allegations ,	NS-elaboration-additional)
denounced Mr. Greenfield 's suit .	NN-Same-Unit)
`` It is a cheap-shot suit	(NS-elaboration-additional(NS-attribution(NS-elaboration-object-attribute(NS-elaboration-additional
-- procedurally defective and thoroughly fallacious --	NS-elaboration-additional)
which was hurriedly filed by a contingency-fee lawyer	(NS-result
as a result of newspaper reports , ''	NS-result))
said a GE spokeswoman .	NS-attribution)
She added	(SN-attribution
that the company was considering bringing sanctions against Mr. Greenfield	(NS-reason
for making `` grossly inaccurate and unsupported allegations . ''	NS-reason))))))))))))
