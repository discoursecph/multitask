R.H. Macy & Co. , the closely held department store chain , said in a financial filing Friday that its sales for the fiscal fourth quarter ended July 29 were up 10 % to $ 1.59 billion against $ 1.44 billion a year earlier .	O	9..226
Comparable store sales for the quarter were up 7.3 % .	O	229..281
The net loss for the quarter was $ 43.1 million against a year-earlier loss of $ 106 million .	B-expansion	284..375
The loss in the fourth quarter of 1988 reflected in part expenses for an unsuccessful bid for Federated Department Stores Inc. , as well as the restructuring of some of its department store operations .	I-expansion	376..576
For the year , sales were up 5.6 % to $ 6.97 billion compared with $ 6.61 billion in fiscal 1988 .	B-expansion	579..672
Sales for both years reflect 12-month performances for each year of I. Magnin , Bullock 's , and Bullocks Wilshire .	B-entrel	673..785
Macy acquired those three businesses in May 1988 .	B-entrel	786..835
On a comparable store basis , including the new acquisitions for both years , sales for fiscal 1989 were up 1.9 % .	I-entrel	836..947
Macy reported a net loss for fiscal 1989 of $ 53.7 million compared with a net loss of $ 188.2 million for fiscal 1988 .	O	950..1067
The company 's earnings before interest , taxes and depreciation , which bondholders use a measurement of the chain 's ability to pay its existing debt , increased 11 % in fiscal 1989 to $ 926.1 million from $ 833.6 million .	B-entrel	1070..1286
The $ 833.6 million figures includes the new acquisitions .	B-comparison	1287..1344
Excluding those businesses , earnings before interest , taxes and depreciation for 1988 would have been $ 728.5 million .	I-comparison	1345..1462
As of Feb. 1 , 1990 , the Bullocks Wilshire stores will operate as I. Magnin stores .	B-entrel	1465..1547
Altogether , Macy and its subsidiaries own or lease 149 department stores and 61 specialty stores nationwide .	I-entrel	1548..1656
Although management led a leveraged buy-out of R.H. Macy in July 1986 , the company still makes financial filings because of its publicly traded debt .	B-entrel	1659..1808
The company estimates its total debt at about $ 5.2 billion .	B-expansion	1809..1868
This includes $ 4.6 billion of long-term debt , $ 457.5 million in short-term debt , and $ 95.7 million of the current portion of long-term debt .	I-expansion	1869..2009
In a letter to investors , Chairman Edward S. Finkelstein wrote that he expects the company to " benefit from some of the disruption faced by our competitors .	O	2012..2168
While our competitors are concerned with their financial viability and possible ownership changes , we will be concentrating on buying and selling merchandise our customers need and want . "	O	2169..2356
Mr. Finkelstein is apparently referring to B. Altman and Bonwit Teller , two New York retailers that have recently filed for Chapter 11 bankruptcy protection , as well as the retail chains owned by financially troubled Campeau Corp .	B-entrel	2359..2589
Those chains include Bloomingdale 's , which Campeau recently said it will sell .	B-expansion	2590..2668
Other retail properties for sale include Saks Fifth Avenue and Marshall Field , retailers now owned by B.A.T PLC , the British tobacco conglomerate .	I-expansion	2669..2815
In his letter , Mr. Finkelstein also referred to the recent San Francisco earthquake .	B-entrel	2818..2902
Mr. Finkelstein flew to San Francisco the day after the earthquake , and found that 10 to 12 of his company 's stores had sustained some damage , including the breakage of most windows at the I. Magnin store on Union Square .	I-entrel	2903..3124
" The volume and profit impact on our fiscal first quarter will not be positive , but looking at the whole fiscal year , we do n't see the effect as material , " wrote Mr. Finkelstein .	O	3127..3305
