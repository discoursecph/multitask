The chemical industry is expected to report that profits eroded in the third quarter because of skidding prices in the commodity end of the business .	O	9..158
Producers of commodity chemicals , the basic chemicals produced in huge volumes for other manufacturers , have seen sharp inventory cutting by buyers .	B-expansion	161..309
Once the chief beneficiaries of the industry 's now fading boom , these producers also will be reporting against exceptionally strong performances in the 1988 third quarter .	I-expansion	310..481
" For some of these companies , this will be the first quarter with year-to-year negative comparisons , " says Leonard Bogner , a chemical industry analyst at Prudential Bache Research .	O	484..664
" This could be the first of five or six down quarters . "	O	665..720
Perhaps most prominent , Dow Chemical Co. , which as of midyear had racked up eight consecutive record quarters , is expected to report that profit decreased in the latest quarter from a year earlier , if only by a shade .	O	723..940
Though Dow has aggressively diversified into specialty chemicals and pharmaceuticals , the company still has a big stake in polyethylene , which is used in packaging and housewares .	O	941..1120
Analysts ' third-quarter estimates for the Midland , Mich. , company are between $ 3.20 a share and $ 3.30 a share , compared with $ 3.36 a year ago , when profit was $ 632 million on sales of $ 4.15 billion .	B-entrel	1123..1321
A Dow spokeswoman declined to comment on the estimates .	I-entrel	1322..1377
At the investment firm of Smith Barney , Harris Upham & Co. , the commodity-chemical segment is seen pulling down overall profit for 20 companies representative of the whole industry by 8 % to 10 % .	B-expansion	1380..1574
" You will find the commodities off more than the others and the diversified companies about even or slightly better , " says James Wilbur , a Smith Barney analyst .	I-expansion	1575..1735
First Boston Corp. projects that 10 of the 15 companies it follows will report lower profit .	B-entrel	1738..1830
Most of the 10 have big commodity-chemical operations .	I-entrel	1831..1885
Still , some industry giants are expected to report continuing gains , largely because so much of their business is outside commodity chemicals .	B-expansion	1888..2030
Du Pont Co. is thought to have had steady profit growth in white pigments , fibers and polymers .	B-expansion	2031..2126
Moreover , the Wilmington , Del. , company is helped when prices weaken on the commodity chemicals it buys for its own production needs , such as ethylene .	I-expansion	2127..2278
Analysts are divided over whether Du Pont will report much of a gain in the latest quarter from its Conoco Inc. oil company .	B-entrel	2281..2405
The estimates for Du Pont range from $ 2.25 to $ 2.45 a share .	B-comparison	2406..2466
In the 1988 third quarter , the company earned $ 461 million , or $ 1.91 a share , on sales of $ 7.99 billion .	B-entrel	2467..2571
Du Pont declined to comment .	I-entrel	2572..2600
Monsanto Co. , too , is expected to continue reporting higher profit , even though its sales of crop chemicals were hurt in the latest quarter by drought in northern Europe and the western U.S.	B-expansion	2603..2793
The St. Louis-based company is expected to report again that losses in its G.D. Searle & Co. pharmaceutical business are narrowing .	B-expansion	2794..2925
Searle continued to operate in the red through the first half of the year , but Monsanto has said it expects Searle to post a profit for all of 1989 .	I-expansion	2926..3074
Most estimates for Monsanto run between $ 1.70 and $ 2 a share .	B-comparison	3077..3138
A year ago , the company posted third-quarter profit of $ 116 million , or $ 1.67 a share , on sales of $ 2.02 billion .	B-entrel	3139..3252
Monsanto declined to comment .	I-entrel	3253..3282
But the commodity-chemical producers are caught on the downside of a pricing cycle .	B-comparison	3285..3368
By some accounts on Wall Street and in the industry , the inventory reductions are near an end , which may presage firmer demand .	I-comparison	3369..3496
But doubters say growing production capacity could keep pressure on prices into the early 1990s .	O	3497..3593
In the latest quarter , at least , profit is expected to fall sharply .	B-expansion	3596..3664
For Himont Inc. , " how far down it is , we do n't know , " says Leslie Ravitz at Salomon Brothers .	I-expansion	3665..3758
The projections are in the neighborhood of 50 cents a share to 75 cents , compared with a restated $ 1.65 a share a year earlier , when profit was $ 107.8 million on sales of $ 435.5 million .	O	3759..3945
Himont faces lower prices for its mainstay product , polypropylene , while it goes forward with a heavy capital investment program to bolster its raw material supply and develop new uses for polypropylene , whose markets include the packaging and automobile industries .	B-entrel	3948..4214
The company , based in Wilmington , Del. , is 81%-owned by Montedison S.p . A. , Milan , which has an offer outstanding for the Himont shares it does n't already own .	I-entrel	4215..4374
At Quantum Chemical Corp. , New York , the trouble is lower prices for polyethylene , higher debt costs and the idling of an important plant due to an explosion .	B-entrel	4377..4535
Some analysts hedge their estimates for Quantum , because it is n't known when the company will book certain one-time charges .	I-entrel	4536..4660
But the estimates range from break-even to 35 cents a share .	B-comparison	4661..4721
In the 1988 third quarter , Quantum earned $ 99.8 million , or $ 3.92 a share , on sales of $ 724.4 million .	I-comparison	4722..4824
Another big polyethylene producer , Union Carbide Corp. , is expected to post profit of between $ 1 a share and $ 1.25 , compared with $ 1.56 a share a year earlier , when the company earned $ 213 million on sales of $ 2.11 billion .	O	4827..5050
Himont , Quantum and Union Carbide all declined to comment .	O	5053..5111
