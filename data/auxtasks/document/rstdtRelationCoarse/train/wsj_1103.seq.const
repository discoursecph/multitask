Boston Co. , the upper-crust financial services concern	(Elaboration(Elaboration(Elaboration(Elaboration(Elaboration(Elaboration(Cause(Textual-organization(Elaboration
that was rocked by a management scandal late last year ,	Elaboration)
has had a sharp drop in profitability	Textual-organization)
-- mainly because a high-risk bet on interest rates backfired .	Cause)
Boston Co. 's fall from grace is bad news for its parent , Shearson Lehman Hutton Holdings Inc. ,	(Elaboration(Elaboration
which has relied heavily on the banking and money management unit 's contributions in recent years .	(Elaboration
In 1988 , for example , Boston Co. had an estimated pretax profit of at least $ 110 million ,	(Comparison
while Shearson managed net income of just $ 96 million .	Comparison)))
Shearson does n't break out the earnings of its subsidiaries .	(Comparison(Comparison
But people familiar with Boston Co. 's performance say	(Attribution
the unit had profit of around $ 17 million for the third quarter ,	(Temporal
after barely breaking even for the first six months .	Temporal)))
Shearson , meanwhile , posted net income of $ 106 million for the first nine months of the year , down slightly from $ 110 million for the year-ago period .	Comparison)))
Moody 's Investors Service Inc. last week downgraded the long-term deposit rating of Boston Co. 's Boston Safe Deposit & Trust Co. subsidiary , to single-A-1 from double-A-3 ,	(Elaboration
citing problems in the company 's `` aggressively managed securities portfolio . ''	Elaboration))
John Kriz , a Moody 's vice president , said	(Elaboration(Attribution
Boston Safe Deposit 's performance has been hurt this year by a mismatch in the maturities of its assets and liabilities .	Attribution)
The mismatch exposed the company to a high degree of interest-rate risk ,	(Attribution(Cause
and	(Textual-organization
when rates moved unfavorably	(Cause(Elaboration
-- beginning late last year	(Temporal
and continuing into this year --	Temporal))
`` it cost them , ''	Cause)))
Mr. Kriz said .	Attribution)))
Mr. Kriz noted	(Comparison(Elaboration(Attribution
that Boston Safe Deposit `` has taken some actions	(Joint(Cause
to better control asset-liability management	(Joint
and improve controls in general ,	Joint))
and we think	(Attribution
these will serve to improve credit quality . ''	Attribution)))
As some securities mature	(Attribution(Cause(Temporal
and the proceeds are reinvested ,	Temporal)
the problems ought to ease ,	Cause)
he said .	Attribution))
But he also cited concerns over the company 's mortgage exposure in the troubled New England real estate market .	Comparison))
Boston Co. officials declined to comment on Moody 's action or on the unit 's financial performance this year	(Elaboration(Comparison
-- except to deny a published report	(Elaboration
that outside accountants had discovered evidence of significant accounting errors in the first three quarters ' results .	Elaboration))
An accounting controversy at the end of last year forced Boston Co. to admit	(Elaboration(Attribution
it had overstated pretax profits by some $ 44 million .	Attribution)
The resulting scandal led to the firing of James N. von Germeten as Boston Co. 's president and to the resignations of the company 's chief financial officer and treasurer .	(Elaboration
The executives were accused of improperly deferring expenses and booking revenue early , in an effort	(Comparison(Elaboration(Elaboration
to dress up results	Elaboration)
-- and perhaps bolster performance-related bonuses .	Elaboration)
Mr. von Germeten , in turn , attributed the controversy to judgmental errors by accountants	(Joint
and accused Shearson of conducting a `` witch hunt . ''	Joint))))))
Mr. Kriz of Moody 's said	(Cause(Attribution
the problems in the securities portfolio stem largely from positions	(Elaboration
taken last year .	Elaboration))
The company 's current management found itself `` locked into this , ''	(Attribution
he said .	Attribution)))
