Defense intellectuals have complained for years that the Pentagon cannot determine priorities because it has no strategy.	B-entrel	9..130
Last April, the new defense secretary, Richard Cheney, acknowledged that, "given an ideal world, we'd have a nice, neat, orderly process.	I-entrel	131..268
We'd do the strategy and then we'd come around and do the budget.	B-contrast	269..334
This city doesn't work that way.	B-contrast	335..368
With a five-year defense plan costing more than $1.6 trillion, it's about time we put together a defense strategy that works in Washington.	I-contrast	369..508
This won't happen until strategists come down from their ivory tower and learn to work in the real world of limited budgets and uncertain futures.	B-contrast	511..657
As it is, we identify national goals and the threats to these goals, we shape a strategy to counter these threats, we determine the forces needed to execute the strategy, before finally forging the budgets needed to build and maintain the forces.	B-restatement	658..904
These procedures consume millions of man-hours of labor and produce tons of paper, and each year, their end product -- the Five Year Defense Plan -- promptly melts away.	I-restatement	905..1074
The graph on the left shows how this happens (see accompanying illustration -- WSJ Oct. 30, 1989).	O	1077..1175
Compare the past eight five-year plans with actual appropriations.	B-restatement	1176..1242
The Pentagon's strategists produce budgets that simply cannot be executed because they assume a defense strategy depends only on goals and threats.	I-restatement	1243..1390
Strategy, however, is about possibilities, not hopes and dreams.	B-cause	1391..1455
By ignoring costs, U.S. strategists abdicate their responsibility for hard decisions.	B-cause	1456..1541
That puts the real strategic decisions in the hands of others: bean counters, budgeteers, and pork-barrelers.	B-contrast	1542..1651
These people have different agendas.	I-contrast	1652..1688
And as a result -- as the recent vote by the House to undo Mr. Cheney's program terminations suggests -- the preservation of jobs is becoming the real goal of defense "strategy."	O	1689..1867
How can we turn this situation around?	O	1870..1908
Reform starts in the Pentagon.	B-restatement	1911..1941
Strategists should consider the impact of budget uncertainties at the beginning of the planning process.	B-restatement	1942..2046
They ought to examine how a range of optimistic to pessimistic budget scenarios would change the defense program.	B-asynchronous	2047..2160
They would then develop priorities by identifying the least painful program cuts as they moved from higher to lower budgets.	B-conjunction	2161..2285
They would also identify the best way to add programs, should the budget come in at higher levels.	B-entrel	2286..2384
This kind of contingency analysis is common in war planning and business planning.	B-conjunction	2385..2467
There is no reason that it can not be done for defense planning.	I-conjunction	2468..2532
Two steps are necessary to translate this idea into action.	B-restatement	2535..2594
Step 1 cleans up our books.	B-cause	2595..2622
Our five-year plan contains three accounting devices -- negative money, an above guidance management reserve and optimistic inflation estimates -- which understate the spending the Pentagon has committed itself to by almost $100 billion.	I-cause	2623..2860
Negative money was invented in 1988 to make the 1990-94 Five Year Defense Plan conform to the numbers in President Reagan's final budget submission to Congress.	B-entrel	2863..3023
That plan exceeded the numbers contained in his budget message by $45 billion.	B-cause	3024..3102
To make the books balance, as is required by law, somebody invented a new budget line item that simply subtracted $45 billion.	B-entrel	3103..3229
It is known in the Pentagon as the "negative wedge."	I-entrel	3230..3282
The Pentagon argues that the negative wedge is the net effect of $22 billion in the as-yet unidentified procurement reductions that it intends to find in future years and $23 billion in an "above guidance" management reserve that accounts for undefined programs that will materialize in the future.	O	3285..3583
The 1990 plan also assumes inflation will decline to 1.7% by 1994.	B-concession	3586..3652
Most forecasters, including those in the Congressional Budget Office, assume inflation will be in excess of 4% in each of those five years.	B-entrel	3653..3792
At that rate, the defense plan is underfunded by $48 billion.	I-entrel	3793..3854
By adding the negative wedge and recalculating the remaining program using a more probable inflation estimate, we arrive at a baseline program costing $1.7 trillion between 1990 and 1994.	O	3857..4044
Step 2 examines how four progressively lower budget scenarios would change the baseline and how these changes would affect our national security.	O	4047..4192
The graph on the right (which assumes a 4% rate of inflation), places these scenarios in the context of recent appropriations (see accompanying illustration -- WSJ Oct. 30, 1989).	O	4193..4372
Note how the baseline program assumes a sharp increase in future appropriations.	B-entrel	4373..4453
Step 2 will answer the question: What happens if these increases do not materialize?	I-entrel	4454..4538
Scenario 1, known as the "Constant Dollar Freeze," reimburses the Pentagon for inflation only -- it slopes upward at 4% per year.	O	4541..4670
This scenario has been the rough position of the U.S. Senate since 1985, and it reduces the baseline by $106 billion between 1990 and 1994.	O	4671..4810
Scenario 3, the "Current Dollar Freeze," has been the approximate position of the House of Representatives for about four years.	B-restatement	4813..4941
It freezes the budget at its current level, and forces the Pentagon to eat the effects of inflation until 1994.	B-cause	4942..5053
This reduces the baseline by $229 billion.	I-cause	5054..5096
Scenario 2 extends the recent compromises between the House and the Senate; it splits the difference between Scenarios 1 and 3, by increasing the budget at 2% per year.	B-cause	5099..5267
Scenario 1, known as the "Constant Dollar Freeze," reimburses the Pentagon for inflation only -- it slopes upward at 4% per year.This scenario has been the rough position of the U.S. Senate since 1985, and it reduces the baseline by $106 billion between 1990 and 1994. Scenario 3, the "Current Dollar Freeze," has been the approximate position of the House of Representatives for about four years.It freezes the budget at its current level, and forces the Pentagon to eat the effects of inflation until 1994.This reduces the baseline by $229 billion. Scenario 2 extends the recent compromises between the House and the Senate; it splits the difference between Scenarios 1 and 3, by increasing the budget at 2% per year.It reduces the baseline by $169 billion.	B-list	5268..5308
Finally, Scenario 4 reduces the budget by 2% per year for the next five years -- a total reduction of $287 billion.	B-contrast	5311..5426
This can be thought of as a pessimistic prediction, perhaps driven by the sequestering effects of the Gramm-Rudman deficit reduction law or possibly a relaxation of tensions with the Soviet Union.	I-contrast	5427..5623
The strategic planners in the Joint Chiefs of Staff would construct the most effective defense program for each scenario, maximizing strengths and minimizing weaknesses.	B-asynchronous	5626..5795
They would conclude their efforts by producing a comprehensive net assessment for each plan -- including the assumptions made, an analysis of its deficiencies and limitations, the impact on national security, and the best strategy for working around these limitations.	I-asynchronous	5796..6064
This exercise would reveal the true cost of a particular program by forcing the strategists to make hard decisions.	B-instantiation	6067..6182
If, for example, they choose to keep the B-2 Stealth bomber, they would have to sacrifice more and more other programs -- such as carrier battlegroups or army divisions -- as they moved toward lower budget levels.	B-cause	6183..6396
These trade-offs would evolve priorities by revealing when the cost of the B-2 became prohibitive.	I-cause	6397..6495
Some may be tempted to argue that the idea of a strategic review merely resurrects the infamous Zero-Based Budgeting (ZBB) concept of the Carter administration.	O	6498..6658
But ZBB did not involve the strategic planners in the Joint Chiefs of Staff, and therefore degenerated into a bean-counting drill driven by budget politics.	O	6659..6815
Anyway, ZBB's procedures were so cumbersome that everyone involved was crushed under a burden of marginalia.	B-contrast	6816..6924
A strategic review is fundamentally different.	B-cause	6925..6971
It would be run by the joint chiefs under simple directions: Produce the best possible force for each budget scenario and provide the Secretary of Defense with a comprehensive net assessment of how that force could be used to achieve U.S. goals.	I-cause	6972..7217
It might be feared that even thinking about lower budgets will hurt national security because the door will be opened to opportunistic budget cutting by an irresponsible Congress.	B-conjunction	7220..7399
This argument plays well in the atmosphere of gaming and mistrust permeating the Pentagon and Congress, and unfortunately, there is some truth to it.	B-concession	7400..7549
But in the end, it must be rejected for logical as well as moral reasons.	I-concession	7550..7623
To say that the Pentagon should act irresponsibly because acting responsibly will provoke Congress into acting irresponsibly leads to the conclusion that the Pentagon should deliberately exaggerate its needs in the national interest; in other words, that it is justified in committing a crime -- lying to Congress -- because it is morally superior.	O	7624..7972
Strategy is not a game between the Pentagon and Congress; it is the art of the possible in a world where constraints force us to choose between unpleasant or imperfect alternatives.	O	7975..8156
If we want meaningful priorities, we must understand the trade-offs they imply before we make commitments.	O	8157..8263
Strategy is not a separate event in an idealized sequence of discrete events; it is a way of thinking that neutralizes threats to our interests in a manner consistent with our financial, cultural and physical limitations.	O	8264..8485
Mr. Spinney is a permanent Pentagon official.	O	8488..8533
This is a condensed version of an essay that will appear in the January issue of the Naval Institute Proceedings.	B-entrel	8534..8647
The views expressed do not reflect the official policy of the Department of Defense.	I-entrel	8648..8732
