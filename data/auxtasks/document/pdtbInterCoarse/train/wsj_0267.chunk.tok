The crowning moment in the career of Joseph F. O'Kicki came as 300 local and state dignitaries packed into his elegant , marble-columned courtroom here last year for his swearing in as President Judge of Cambria County .	O	9..227
Baskets of roses and potted palms adorned his bench .	B-expansion	230..282
The local American Legion color guard led the way .	B-expansion	283..333
As the judge marched down the center aisle in his flowing black robe , he was heralded by a trumpet fanfare .	I-expansion	334..441
To many , it was a ceremony more befitting a king than a rural judge seated in the isolated foothills of the southern Allegheny Mountains .	B-comparison	444..581
But then Judge O'Kicki often behaved like a man who would be king -- and , some say , an arrogant and abusive one .	B-contingency	582..694
While his case may be extreme , it reflects the vulnerability of many small communities to domineering judges .	I-contingency	695..804
Last March , nine months after the judge 's swearing-in , the state attorney general 's office indicted him on a sweeping array of charges alleging more than 10 years of " official oppression " in Cambria County , a depressed steel and mining community in western Pennsylvania .	B-expansion	807..1077
The allegations , ranging from theft and bribery to coercion and lewdness , paint a disquieting picture .	I-expansion	1078..1180
According to testimony in a public , 80-page grand-jury report handed up to the state attorney general , Judge O'Kicki extorted cash from lawyers , muscled favorable loans from banks and bullied local businesses for more than a decade .	O	1183..1415
Prosecutors , in an indictment based on the grand jury 's report , maintain that at various times since 1975 , he owned a secret and illegal interest in a beer distributorship ; plotted hidden ownership interests in real estate that presented an alleged conflict of interest ; set up a dummy corporation to buy a car and obtain insurance for his former girlfriend ( now his second wife ) ; and maintained 54 accounts in six banks in Cambria County .	O	1418..1857
In testimony recorded in the grand jury report , court employees said the judge , now 59 years old , harassed his secretaries , made imperial demands on his staff and hounded anyone who crossed him .	O	1860..2054
Bailiffs claimed they were required to chauffeur him to and from work , mow his lawn , chop his wood , fix his car and even drop by his house to feed his two grown mutts , Dixie and Husky .	B-expansion	2055..2239
One former bailiff charged that the judge double-crossed him by reneging on a promise of a better paying job after pocketing a $ 500 bribe .	I-expansion	2240..2378
Some of the allegations are simply bizarre .	B-expansion	2381..2424
Two former secretaries told the grand jury they were summoned to the judge 's chambers on separate occasions to take dictation , only to find the judge in his bikini underwear .	B-expansion	2425..2599
One secretary testified that the judge once called her to his office while wearing nothing at all .	I-expansion	2600..2698
The judge , suspended from his bench pending his trial , which began this week , vehemently denies all the allegations against him , calling them " ludicrous " and " imaginative , political demagoguery . "	B-expansion	2701..2896
He blames the indictment on local political feuding , unhappiness with his aggressive efforts to clear the courthouse 's docket and a vendetta by state investigators and prosecutors angered by some of his rulings against them .	I-expansion	2897..3121
" I do n't know whose toes I 've stepped on , " says the judge .	O	3124..3182
" I 'll find out , eventually , who pushed the state police buttons into action . "	O	3183..3260
Even if only some of the allegations stand up , however , they provide ample testimony to the awesome power of judges in rural communities .	B-entrel	3263..3400
That power can sometimes be abused , particularly since jurists in smaller jurisdictions operate without many of the restraints that serve as corrective measures in urban areas .	I-entrel	3401..3577
Lawyers and their clients who frequently bring business to a country courthouse can expect to appear before the same judge year after year .	O	3580..3719
Fear of alienating that judge is pervasive , says Maurice Geiger , founder and director of the Rural Justice Center in Montpelier , Vt. , a public interest group that researches rural justice issues .	O	3720..3915
As a result , says Mr. Geiger , lawyers think twice before appealing a judge 's ruling , are reluctant to mount , or even support , challenges against him for re-election and are usually loath to file complaints that might impugn a judge 's integrity .	O	3918..4162
Judge O'Kicki , a stern and forbidding-looking man , has been a fixture in the local legal community for more than two decades .	B-entrel	4165..4290
The son of an immigrant stonemason of Slovenian descent , he was raised in a small borough outside Ebensburg , the Cambria County seat , and put himself through the University of Pittsburgh Law School .	I-entrel	4291..4489
He graduated near the top of his class , serving on the school law review with Richard Thornburgh , who went on to become governor of Pennsylvania and , now , U.S. Attorney General .	O	4490..4667
It was also in law school that Mr. O'Kicki and his first wife had the first of seven daughters .	B-comparison	4668..4763
He divorced his first wife three years ago and married the daughter of his court clerk .	I-comparison	4764..4851
Last year , Pennsylvania Supreme Court Justice John P. Flaherty called Mr. O'Kicki one of the finest judges " not only in Pennsylvania but in the United States . "	B-contingency	4854..5013
Clearly , the judge has had his share of accomplishments .	I-contingency	5014..5070
After practicing law locally , he was elected to his first 10-year term as judge in 1971 ; in 1981 , he was effectively re-elected .	B-temporal	5071..5199
Six years ago , Judge O'Kicki was voted president of the Pennsylvania Conference of State Trial Judges by the state 's 400 judges .	B-expansion	5200..5328
He has been considered several times for appointments to federal district and appellate court vacancies in Pennsylvania .	B-expansion	5329..5449
And when he ran unsuccessfully for a state appellate court seat in 1983 , the Pennsylvania Bar Association rated him " one of the best available , " after interviewing local lawyers .	I-expansion	5450..5628
" He probably was the smartest guy who ever sat on our bench , " says a former president of Cambria County 's 150-member bar association , who , like most lawyers in Cambria County , refuses to talk about the judge publicly .	O	5631..5848
" He 's sharp as a tack .	B-expansion	5849..5871
He could grasp an issue with the blink of an eye . "	I-expansion	5872..5922
For more than a decade , virtually no one complained about Judge O'Kicki .	B-contingency	5925..5997
" What about those institutions that are supposed to be the bedrock of society , the banks and the bar association ... ? " wrote a columnist for the Tribune-Democrat , a newspaper in nearby Johnstown , shortly after the scandal became public .	I-contingency	5998..6234
" If only a banker or a lawyer had spoken out years ago , the judicial process would n't be under the taint it is today . "	O	6235..6353
Officials with the Pennsylvania Judicial Inquiry and Review Board , the arm of the state that investigates judicial misconduct , counter that they had no inkling of anything amiss in Ebensburg .	O	6356..6547
" Nobody told us ; nobody called us , " says an official close to the case who asked not to be named .	O	6548..6645
" Nobody had the guts to complain . "	O	6646..6680
Certainly not the lawyers .	B-comparison	6683..6709
Johnstown attorney Richard J. Green Jr. shelled out $ 500 in loans to the judge over five years , he said in testimony to the grand jury .	I-comparison	6710..6845
" The judge never made a pretense of repaying the money , " said Mr. Green .	O	6846..6918
Eventually , Mr. Green testified , he began ducking out of his office rather than face the judge when he visited .	O	6919..7030
When Mr. Green won a $ 240,000 verdict in a land condemnation case against the state in June 1983 , he says Judge O'Kicki unexpectedly awarded him an additional $ 100,000 .	O	7033..7201
Mr. Green thought little of it , he told the grand jury , until the judge walked up to him after the courtroom had cleared and suggested a kickback .	O	7202..7348
" Do n't you think I ought to get a commission ... or part of your fee in this case ? " Mr. Green said the judge asked him .	O	7351..7470
Appalled , Mr. Green never paid the money , he testified .	O	7473..7528
But he did n't complain to the state 's Judicial Inquiry and Review Board , either , saying later that he feared retribution .	B-entrel	7529..7650
Mr. O'Kicki said he will respond to Mr. Green 's allegation at his trial .	I-entrel	7651..7723
Like most of Cambria County 's lawyers and residents who had dealings with the judge , Mr. Green declined to be interviewed for this article .	O	7726..7865
And no one with a complaint about the judge would allow his name to be printed .	O	7866..7945
" I do n't have anything much to say , and I think that 's what you 're going to find from everyone else you talk to up here , " says local attorney Edward F. Peduzzi .	O	7948..8108
Says another lawyer : " The practice of law is a matter of biting one 's lip when you live in a small community .	O	8111..8220
One had best not dance on top of a coffin until the lid is sealed tightly shut . "	O	8221..8301
The judge was considered imperious , abrasive and ambitious , those who practiced before him say .	O	8304..8399
He sipped tea sweetened with honey from his high-backed leather chair at his bench , while scribbling notes ordering spectators to stop whispering or to take off their hats in his courtroom .	B-expansion	8400..8589
Four years ago , he jailed all nine members of the Cambria County School Board for several hours after they defied his order to extend the school year by several weeks to make up for time lost during a teachers ' strike .	I-expansion	8590..8808
Visitors in his chambers say he could cite precisely the years , months , weeks and days remaining until mandatory retirement would force aside the presiding president judge , giving Judge O'Kicki the seniority required to take over as the county 's top court administrator .	O	8811..9081
The judge , they say , was fiercely proud of his abilities and accomplishments .	O	9082..9159
" My name is judge , " Judge O'Kicki told a car salesman in Ebensburg when he bought a new red Pontiac Sunbird in October 1984 , according to the grand-jury report .	O	9162..9322
The dealership dutifully recorded the sale under the name " Judge O'Kicki . "	O	9323..9397
Yet , despite the judge 's imperial bearing , no one ever had reason to suspect possible wrongdoing , says John Bognato , president of Cambria County 's 150-member bar association .	O	9400..9574
" The arrogance of a judge , his demeanor , the way he handles people are not a basis for filing a complaint , " says Mr. Bognato .	O	9577..9702
" Until this came up and hit the press , there was never any indication that he was doing anything wrong . "	O	9703..9807
State investigators dispute that view now , particularly in light of the judge 's various business dealings in Cambria County .	B-entrel	9810..9934
The judge came under scrutiny in late 1987 , after the state attorney general 's office launched an unrelated investigation into corruption in Cambria County .	I-entrel	9935..10091
The inquiry soon focused on the judge .	B-contingency	10094..10132
Even his routine business transactions caused trouble , according to the grand jury report .	B-expansion	10133..10223
When the judge bought his new Sunbird from James E. Black Pontiac-Cadillac in Ebensburg five years ago , the dealership had " certain apprehensions " about the judge 's reputation , according to the grand-jury report .	B-contingency	10224..10436
The dealership took the extra step of having all the paper work for the transaction pre-approved by Ebensburg 's local lender , Laurel Bank .	I-contingency	10437..10575
Then , as an additional precaution , the car dealership took the judge 's photograph as he stood next to his new car with sales papers in hand -- proof that he had received the loan documents .	O	10578..10767
But when the judge received his payment book , he disavowed the deal .	O	10770..10838
" There was no loan , there is no loan , there never shall be a loan , " the judge wrote the bank on his judicial stationery , according to the report .	O	10839..10984
Later , the judge went a step farther .	B-expansion	10987..11024
After Laurel Bank tried to repossess the car , a vice president asked him to intervene in an unrelated legal dispute involving a trust account .	I-expansion	11025..11167
The judge wrote again .	O	11168..11190
" I find myself in an adversary relationship with Laurel Bank , and I am not inclined to extend myself as far as any favors are concerned , " the judge wrote back in a letter attached to the grand jury 's report .	O	11193..11400
" Perhaps if my personal matters can be resolved with Laurel bank in the near future , I may be inclined to reconsider your request ... . "	O	11401..11536
The judge now says it was " unfortunate " that he chose to write the letter but says " there was certainly no intent to extort there . "	O	11537..11668
The bank acquiesced .	B-contingency	11671..11691
It refinanced the judge 's loan , lowered its interest rate and accepted a trade-in that had n't originally been part of the deal -- a beat up 1981 Chevy Citation the dealer had to repair before it could be resold .	I-contingency	11692..11903
The incident was n't the only time the judge got special treatment from his local bank .	O	11906..11992
Two years later , he wrote to complain that the interest he was paying on an unsecured $ 10,000 loan was " absolutely onerous . "	O	11993..12117
Paul L. Kane , Laurel 's president at the time , quickly responded .	O	12118..12182
The bank , he wrote back , was " immediately " lowering the rate by 3.5 % , " as a concession to you . "	O	12183..12278
The judge says he ca n't discuss in detail how he will defend himself at his trial , although he contends that if he were as corrupt as state prosecutors believe , he would be far wealthier than he is .	O	12281..12479
His seven-bedroom cedar and brick house outside of Johnstown is up for sale to pay for his lawyers .	O	12480..12579
The judge says he is confident he will return to his old bench .	B-expansion	12582..12645
Already , he notes , the 76 charges originally filed against him have been trimmed to 27 .	I-expansion	12646..12733
Most of the allegations no longer pending were ethics charges withdrawn by state prosecutors as part of a pre-trial agreement .	B-comparison	12734..12860
The heart of the case -- " official oppression " -- remains intact .	I-comparison	12861..12926
" If I lose , I lose my position , my career , my pension , my home and my investments , " says the judge .	O	12929..13028
" My God and I know I am correct and innocent .	O	13029..13074
