Dennis Farney 's Oct. 13 page-one article `` River of Despair , '' about the poverty along the Mississippi , fanned childhood memories	root
of when my parents were sharecroppers in southeastern Arkansas , only a few miles from the river .	-1_NS-Elaboration
the same economic factors affected us	-1_SN-Contrast
as affects the black people	-1_NS-Comparison
Mr. Farney writes about .	-1_NS-Elaboration
Fortunately , an aunt with a college degree bought a small farm	-6_NS-Elaboration
Fortunately , an aunt with a college degree bought a small farm	-3_SN-Background
and moved us 50 miles north to good schools and an environment	-1_NN-Temporal
that opened the world of opportunity for me as an eight-year-old .	-1_NS-Elaboration
I 've never forgotten or lost contact with those memories of the 1930s .	-3_NN-Temporal
I 've never forgotten or lost contact with those memories of the 1930s .	-1_SN-Contrast
Most of the land in that and other parts of the Delta are now owned by second , third or fourth generations of the same families .	-1_NN-Temporal
These are the families	-1_NS-Elaboration
who used	-1_NS-Elaboration
-- and sometime abused	-1_NN-Joint
-- their sharecroppers , people	-2_NN-Same-unit
who had no encouragement and little access to an education or training for a better life .	-1_NS-Elaboration
when one family with mechanized equipment could farm crops	-1_SN-Background
formerly requiring 20 families ,	-1_NS-Comparison
the surplus people were dumped into the mainstream of society with no Social Security , no skills in the workplace , no hope for their future except welfare .	-9_NS-Elaboration
the surplus people were dumped into the mainstream of society with no Social Security , no skills in the workplace , no hope for their future except welfare .	-2_SN-Background
And today , many of their children , grandchildren and great-grandchildren remain on welfare .	-1_NN-Cause
In the meantime , the landowners continue receiving generous subsidies	-2_NN-Contrast
that began during New Deal days .	-1_NS-Elaboration
Or those	-2_NN-Joint
who choose not to farm	-1_NS-Elaboration
can lease their lands and crop allotments for handsome sums .	-2_NN-Same-unit
Farmers in the Midwest and other areas have suffered ,	-27_SN-Explanation
but those along the Mississippi continue to prosper with holdings	-1_NN-Contrast
that were built with the sweat of men and women	-1_NS-Elaboration
living in economic slavery .	-1_NS-Elaboration
they were turned loose	-4_NS-Elaboration
they were turned loose	-1_SN-Background
unprepared to build lives of their own .	-1_NN-Joint
Denton Harris	-7_NN-Textual-organization
Chairman	-1_NN-Joint
Metro Bank	-1_NN-Joint
Atlanta	-1_NN-Joint
breaking this cycle will be next to impossible .	-1_SN-Cause
Sadly , the cycle appears not as waves but as a downward spiral .	-1_NS-Elaboration
Yet the evidence	-14_NN-Textual-organization
Yet the evidence	-2_SN-Evaluation
that we have not hit bottom	-1_NS-Elaboration
is found in the fact	-2_NN-Same-unit
that we are not yet helping ourselves .	-1_NS-Elaboration
The people of the Delta are waiting for that big factory to open , river traffic to increase , government spending to fund job-training programs or public schools to educate apathetic students .	-4_NS-Explanation
the questions continue as fodder for the commissions and committees , for the media and politicians .	-2_NN-Joint
the questions continue as fodder for the commissions and committees , for the media and politicians .	-1_SN-Explanation
Coffee-shop chatter does not lend itself to solving the problems of racism , teen pregnancy or lack of parental support or guidance .	-1_NN-Joint
Does the Delta deserve government help	-8_NS-Topic-Comment
in attracting industry	-1_NS-Elaboration
racism alienates potential employers ?	-3_NS-Background
racism alienates potential employers ?	-1_SN-Attribution
Should we focus on the region 's infant-mortality rate	-4_NN-Joint
when the vocal right-wingers and the school boards , mayors and legislators prohibit schools from teaching the two ways	-1_NS-Background
( abstinence or contraceptives )	-1_NS-Elaboration
of decreasing teen pregnancy ?	-2_NN-Same-unit
that we are ready to solve them yet .	-18_NS-Evaluation
that we are ready to solve them yet .	-2_SN-Contrast
that we are ready to solve them yet .	-1_SN-Attribution
Leslie Falls Humphries	-19_NN-Textual-organization
Little Rock , Ark .	-1_NN-Joint
I would like to issue a challenge to corporate America .	-21_NN-Textual-organization
The next time expansion plans are mentioned at the old company	-1_NS-Elaboration
`` Aw heck , guys , nobody can do it like Japan or South Korea , ''	-2_NN-Temporal
`` Aw heck , guys , nobody can do it like Japan or South Korea , ''	-1_SN-Attribution
would butt in	-4_NN-Topic-Comment
would butt in	-1_SN-Attribution
`` Hold it , fellas ,	-2_NN-Temporal
`` Hold it , fellas ,	-1_SN-Attribution
why do n't we compare prices	-1_NN-Temporal
and use our own little Third World country .	-1_NN-Temporal
We would even save on freight . ''	-1_NS-Cause
There is no mystery	-11_NS-Evaluation
why the Delta originated `` Singin ' the Blues . ''	-1_NS-Elaboration
Eugene S. Clarke IV	-13_NN-Textual-organization
Hollandale , Miss .	-1_NN-Joint
Your story is an insult to the citizens of the Mississippi Delta .	-15_NN-Textual-organization
Many of the problems	-1_NS-Explanation
you presented	-1_NS-Elaboration
exist in every part of this country .	-2_NN-Same-unit
Poverty is only two blocks from President Bush 's residence .	-3_NS-Explanation
For years , we tried to ignore the problem of poverty ,	-4_NS-Elaboration
and	-1_NN-Joint
it 's a new crusade for the media and our Democratic Congress .	-2_NN-Same-unit
it 's a new crusade for the media and our Democratic Congress .	-1_SN-Cause
Nobody should have to live in such poor conditions as in `` Sugar Ditch , ''	-8_NS-Elaboration
but	-1_NN-Joint
the same problems exist .	-2_NN-Same-unit
the same problems exist .	-1_SN-Background
The only difference is , in those cities the poor are housed in high-rise-project apartments	-3_NS-Explanation
each consisting of one room , with rusty pipes	-1_NS-Elaboration
called plumbing ,	-1_NS-Elaboration
rodents and cockroaches everywhere and nonworking elevators	-2_NN-Joint
-- and with the building patrolled by gangs and drug dealers .	-1_NN-Joint
Then maybe I could stay home	-19_NS-Evaluation
Then maybe I could stay home	-1_SN-Background
and have seven children	-1_NN-Joint
and watch Oprah Winfrey , like Beulah in the article ,	-1_NN-Joint
instead of having one child	-3_NN-Contrast
and working constantly	-1_NN-Joint
just to stay above water , like so many families in this country .	-1_NS-Enablement
Dee Ann Wilson	-25_NN-Textual-organization
Greenville , Miss .	-1_NN-Joint
