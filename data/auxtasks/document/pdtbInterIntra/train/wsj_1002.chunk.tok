At a time when Jon Levy should be planning the biggest spring season in his dress company 's 17 years , his work day is studded with intense moments of concern about one of his biggest customers , Campeau Corp .	root	9..216
" The dress business has always been a gamble , but it 's never been like this , " says Mr. Levy , president of St. Gillian Group Ltd. , which has become a hot name thanks to a campaign of sexy TV commercials .	norel	219..421
Every day , Mr. Levy checks orders from Campeau department store chains , trying to guess if he will be paid .	norel	424..531
" I 'm now monitoring every major account . "	restatement	532..573
Campeau , owner of such retailers as Bloomingdale 's , Bon Marche , and Jordan Marsh , sidestepped financial collapse last month after an emergency $ 250 million loan from Olympia & York Developments Ltd. , a Canadian developer and a major shareholder in Campeau .	norel	576..832
The need for the loan surprised many analysts and bond holders who had been told at the company 's annual meeting in July that there were n't any major problems ahead .	entrel	833..998
The risk of doing business with Campeau 's Federated and Allied department store chains is about to increase greatly , not only for Mr. Levy but for hundreds of other small apparel makers , button suppliers , trucking firms and fabric houses .	norel	1001..1239
Next week , the country 's top designers and manufacturers will begin showing fashions for spring 1990 , the second most important selling season of the year .	norel	1242..1397
And as the applause dies down in showrooms along Seventh Avenue and Broadway , stylishly clad Campeau buyers will begin writing orders .	conjunction	1398..1532
Orders from Campeau retailers used to be cause for celebration .	norel	1535..1598
This is no longer true because of Campeau 's massive debt load .	contrast	1599..1661
" It 's all anybody wants to talk about , " says Richard Posner , executive vice president for Credit Exchange Inc. , a leading credit service .	norel	1664..1801
" People wonder what 's going to happen next . "	cause	1802..1846
Many manufacturers are worried about being paid for merchandise already shipped to Campeau stores .	norel	1849..1947
But those dollars at risk pale in comparison to the investment required to make and ship spring goods to Campeau stores .	contrast	1948..2068
" The few million dollars I could lose today is nothing against what I could lose on the spring line , " says Mr. Levy , who estimates that Campeau stores will sell $ 25 million worth of his clothes this year .	norel	2071..2275
" I 'm buying fabric right now for clothes which I may not be paid for until April or May .	cause	2276..2364
What happens to me if Campeau collapses between now and then ? "	cause	2365..2427
Some credit concerns , such as Bernard Sands Credit Consultants Inc. , have told clients not to ship anything to Federated or Allied stores on credit .	norel	2430..2578
" This is especially true for spring merchandise , " says Jim Rindos , credit manager at Bernard Sands .	restatement	2579..2678
" Campeau has too much debt . "	cause	2679..2707
Other credit houses , such as Credit Exchange and Solo Credit Service Corp. , are suggesting that their clients study each order before shipping .	norel	2710..2853
" Payments are good right now , but we are n't recommending any long-term lines of credit , " says Richard Hastings , a retail credit analyst , referring to credit lines which make inventory purchases automatic .	instantiation	2854..3058
" The Campeau situation is a little uncertain and very difficult to analyze . "	cause	3059..3135
Because of those concerns , some manufacturers say they will ask for letters of credit before shipping spring merchandise .	norel	3138..3259
" We 're being paid today , but we 're worried about tomorrow and will want " letters of credit , says the sales director at one major dress maker who asked not to be identified .	instantiation	3260..3432
Howard Bloom , president of the dress firm Chetta B Inc. , says : " It 's big time chaos today .	norel	3435..3525
I 'm going to ship and hope I get paid .	concession	3526..3564
If I need to ask for money up front later , I will . "	asynchronous	3565..3616
Carol Sanger , vice president , corporate communications at Campeau , says that all of the Federated and Allied chains are paying their bills in a timely manner .	norel	3619..3777
" They continue to pay their bills and will do so , " says Ms. Sanger .	restatement	3778..3845
" We 're confident we 'll be paying our bills for spring merchandise as well . "	conjunction	3846..3921
Typically , manufacturers are paid 10 days after the month in which they ship .	norel	3924..4001
If goods are shipped to Bloomingdale 's between Oct. 1 and Oct. 20 , manufacturers expect to be paid by Nov. 10 .	restatement	4002..4112
But manufacturers now buying fabric for spring season goods wo n't be paid until March , April or even May .	norel	4115..4220
Some in the market question whether Campeau will be in a position to pay bills at that time .	conjunction	4221..4313
" Everybody is worried about the possibility of cancellations , " says Kurt Barnard , publisher of Barnard 's Retail Marketing Report .	norel	4316..4445
" The buyers who work for the various Campeau chains may lose their jobs .	instantiation	4446..4518
The stores they work for may be sold .	cause	4519..4556
What that will mean for manufacturers is anybody 's guess . "	conjunction	4557..4615
Campeau 's financial situation is complicated by an estimated $ 1.23 billion in debt due next spring .	norel	4618..4717
This includes a working capital facility for Allied Stores of $ 350 million that matures March 15 , 1990 , and an $ 800 million bridge loan due April 30 , 1990 .	entrel	4718..4873
The company has stated in recently filed financial documents that it anticipates refinancing its March 1990 payments .	entrel	4874..4991
In recent months , numerous retailers have filed for Chapter 11 bankruptcy protection , including Bonwit Teller , B. Altman & Co. , and Miller & Rhoads Inc .	norel	4992..5144
Those filings , plus the expected sale of a number of financially healthy chains , such as Saks Fifth Avenue , Marshall Field 's and Bloomingdale 's , have added to the anxiety .	entrel	5145..5316
" Right now , Federated owes us a considerable amount of money , " says Morris Marmalstein , president of David Warren Enterprises , a major dress manufacturer .	norel	5319..5473
" We expect they will be current with their debts by the end of the week , but we are considering asking them for letters of credit before we take more orders . "	contrast	5474..5632
Mr. Marmalstein adds that his company is now holding some goods in anticipation of being paid in full .	norel	5635..5737
" It 's become a day-by-day business , " he says .	restatement	5738..5783
" Business has never been this tough before .	entrel	5784..5827
Not only does your product have to be excellent , but you also have to be able to collect . "	cause	5828..5918
Other manufacturers are equally cautious .	norel	5921..5962
Bud Konheim , president of Nicole Miller Inc. , says his company is now shipping only to the flagship stores of the Federated and Allied chains .	instantiation	5963..6105
This limits his financial exposure , he says .	cause	6106..6150
" The branches are just warmed over , empty halls , " says Mr. Konheim .	norel	6153..6220
" Why should I be part of that problem ?	cause	6221..6259
I 've got limited production , and I ca n't give it to underperformers . "	cause	6260..6329
Campeau 's Ms. Sanger disputes Mr. Konheim 's comments .	norel	6332..6385
" Many of the branches are very lucrative , " she says .	restatement	6386..6438
" That 's just nonsense . "	cause	6439..6462
As for Mr. Levy at St. Gillian , he says he will maintain his credit lines with the various Campeau stores unless they miss a payment .	norel	6465..6598
" If they slip for 10 cents for 10 minutes , I 'll stop , " he says .	contrast	6599..6662
