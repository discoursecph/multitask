UAL Corp. 's board quashed any prospects for an immediate revival of a labor-management buy-out ,	(Elaboration(Elaboration(Elaboration(Elaboration(Cause(Background(Cause(Cause(Elaboration
saying	(Attribution
United Airlines ' parent should remain independent for now .	Attribution))
As a result , UAL 's chairman , Stephen M. Wolf , pulled out of the buy-out effort	(Enablement
to focus on running the company .	Enablement))
The two developments put the acquisition attempt back to square one	(Joint
and leaves the airline with an array of unresolved matters ,	(Elaboration
including an unsettled labor situation and a management	(Elaboration
scrambling to restore its damaged credibility .	Elaboration))))
The effort	(Temporal(Same-unit(Elaboration
to create the nation 's largest employee-owned company	Elaboration)
began unraveling Oct. 13	(Background
when the labor-management group was unable to obtain financing for its $ 300-a-share , $ 6.79 billion offer .	Background))
Just last week it suffered another major setback	(Explanation
when British Airways PLC , the largest equity investor in the labor-management bid , withdrew its support .	Explanation)))
Takeover stock traders ,	(Explanation(Same-unit(Elaboration
focusing on the company 's intention	(Elaboration
to stay independent ,	Elaboration))
took the announcement as bad news .	Same-unit)
UAL ,	(Elaboration(Elaboration(Joint(Same-unit(Elaboration
which had risen $ 9.875 to $ 178.375 in composite trading on the New York Stock Exchange on reports of a new bid	(Elaboration
being prepared by the group ,	Elaboration))
reversed course	Same-unit)
and plummeted in off-exchange trading after the 5:09 p.m. EDT announcement .	Joint)
Among the first trades	(Same-unit(Elaboration
reported by the securities firm of Jefferies & Co. ,	(Elaboration
which makes a market in UAL	(Temporal
after the exchange is closed ,	Temporal)))
were 10,000 shares at $ 170 , 6,000 shares at $ 162 , 2,500 at $ 162 , and 10,000 at $ 158 .	Same-unit))
The rebound in UAL stock during regular trading hours Monday was its first daily gain	(Temporal
after six consecutive losses left the price 41 % below its level before Oct. 13 , the day	(Elaboration
the group announced	(Attribution
the bank financing could n't be obtained for the original deal .	Attribution))))))
Twelve of UAL 's outside directors met at a five-hour meeting yesterday in Chicago	(Evaluation(Contrast(Explanation(Elaboration(Background(Enablement
to consider an informal proposal from the buy-out group for a revised bid .	Enablement)
But the board said	(Attribution
it was n't interested for now .	Attribution))
That proposal ,	(Joint(Same-unit(Elaboration
valued at between $ 225 and $ 240 a share ,	Elaboration)
would have transferred majority ownership to employees	Same-unit)
while leaving some stock in public hands .	Joint))
The buy-out group had no firm financing for the plan .	(Background
And , with no other offers on the table , the board apparently felt no pressure to act on it .	Background))
The directors signaled , however ,	(Elaboration(Attribution
that they would be willing to consider future offers or take some other action	(Elaboration
to maximize shareholder value ,	Elaboration))
saying	(Attribution
they would continue to explore `` all strategic and financial alternatives . ''	Attribution)))
But it was clear that for the time being , the board wants the company to return to normalcy .	(Explanation
The board said	(Attribution
it concluded	(Attribution
that `` the welfare of the company , its shareholders , its employees and the broader public . . . can best be enhanced by continued development of UAL as a strong , viable , independent company . ''	Attribution)))))
Mr. Wolf urged all employees to `` now turn their full attention '' to operating the airline .	(Elaboration(Elaboration(Contrast(Elaboration
He also vowed to `` make every effort	(Elaboration
to nurture . . . a constructive new relationship	(Elaboration
that has been forged with participating employee groups . ''	Elaboration)))
But Mr. Wolf faces a monumental task	(Elaboration
in pulling the company back together again .	Elaboration))
Labor problems top the list .	(Elaboration
For a brief time , the buy-out effort seemed to solve his problems with United 's pilot union .	(Joint(Elaboration(Contrast(Explanation
In return for an ownership stake in the company , the pilots were willing to agree to a seven-year contract	(Elaboration(Elaboration(Elaboration(Elaboration
that included a no-strike clause and significant wage concessions and productivity gains	Elaboration)
the union previously resisted .	Elaboration)
That contract was tied to the success of the buy-out .	Elaboration)
As a `` good-will measure , '' the pilots had been working four extra hours a month	(Joint
and had agreed to fly UAL 's two new Boeing 747-400 aircraft .	Joint)))
It 's uncertain if the pilots will continue to do so	(Background
without a contract settlement .	Background))
The union said late last night	(Attribution
that it is still committed to majority employee ownership	(Joint
and that the labor disputes	(Same-unit(Elaboration
that faced the company prior to the buy-out effort	Elaboration)
`` still need to be addressed . ''	Same-unit))))
The buy-out effort also worsened already-strained relations between United 's pilot and machinist unions .	(Elaboration(Elaboration
The machinists ' criticisms of the labor-management bid and their threats of a strike	(Same-unit(Condition
unless they received substantial wage increases this year	Condition)
helped cool banks ' interest	(Elaboration
in financing the transaction .	Elaboration)))
The machinists previously had shown themselves to be an ally to Mr. Wolf ,	(Elaboration(Contrast
but he lost much of his credibility with that group	(Explanation
when he teamed up with the pilot union .	Explanation))
The machinists criticized the terms	(Elaboration(Elaboration
Mr. Wolf and management received in the buy-out .	Elaboration)
They paid $ 15 million for a 1 % stake	(Joint
and received an additional 9 % of the company at no additional cost .	Joint)))))))
His credibility is also on the line in the investment community .	(Elaboration
Until the collapse of this bid , Mr. Wolf was regarded as one of the nation 's savviest airline executives	(Contrast(Explanation
after engineering turnarounds of Tiger International Inc. and Republic Airlines .	Explanation)
But he and his chief financial officer , John Pope , sowed some of the seeds for the deal 's failure	(Explanation
by insisting	(Contrast(Attribution
banks accept low financing fees and interest rates ,	Attribution)
while they invested in the transaction only a small fraction of the $ 114.3 million	(Elaboration
they stood to gain from sale of their UAL stock and options .	Elaboration)))))))
The board 's actions leave takeover stock traders nursing some $ 700 million in losses and eager to respond to anyone	(Elaboration(Joint(Elaboration
who might make a new offer .	Elaboration)
It also inevitably leaves a residue of shareholder lawsuits .	Joint)
Arbitragers said	(Joint(Background(Attribution
they were disappointed	(Attribution
the company did n't announce some recapitalization or other plan	(Elaboration
to maximize value .	Elaboration)))
One takeover expert noted	(Attribution
that arbitragers could force a recapitalization through the written consent process	(Elaboration
under which holders may oust the board by a majority vote .	Elaboration)))
The machinists union has suggested	(Attribution
it may propose a recapitalization	(Elaboration
that includes a special dividend for holders and a minority ownership stake for employees .	Elaboration)))))
Los Angeles investor Marvin Davis ,	(Contrast(Same-unit(Elaboration
whose $ 240-a-share offer for UAL in August triggered a bidding war ,	Elaboration)
says	(Attribution
he remains interested in the airline .	Attribution))
However , he is restricted from making certain hostile moves by an agreement	(Elaboration(Elaboration(Elaboration
he signed	Elaboration)
to obtain confidential UAL data .	Elaboration)
Essentially , he ca n't make any hostile moves	(Condition
unless he makes a tender offer at least $ 300 a share .	Condition))))
