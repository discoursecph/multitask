Part of a Series }	root	9..26
SMYRNA , Ga. --	norel	29..43
The auto-dealer strip in this booming suburb runs nearly five miles along Cobb Parkway , stretching from the Perimeter highway that circles Atlanta to the " Big Chicken , " a pullet-roofed fast-food restaurant and local landmark .	norel	44..269
Twenty years ago , in the infancy of suburban sprawl , just a handful of dealerships were here .	norel	272..365
Now there are 23 .	contrast	366..383
Alongside such long-familiar names as Chevrolet , Ford and Dodge are nameplates that did n't exist until three years ago : Acura , Sterling , Hyundai .	entrel	384..529
Under construction is the strip 's 24th showroom , the future home of Lexus , a luxury marque launched by Toyota Motor Corp. just two months ago .	conjunction	530..672
The 1980s have spawned an explosion of consumer choice in America , in everything from phone companies to colas .	norel	675..786
And especially , as the Cobb Parkway strip attests , in cars .	norel	787..846
Americans now can choose among 572 different models of cars , vans and trucks , up from just 408 when the decade began , according to Automotive News , a trade publication .	restatement	847..1015
For car marketers , it has become a much tougher battle to keep loyal customers from defecting to one of the new makes on the block .	norel	1018..1149
For American car buyers , the proliferation of choice is both liberating and confusing .	conjunction	1150..1236
Malcolm MacDougall , vice chairman of the Jordan , McGrath , Case & Taylor advertising agency in New York , calls the proliferation " nameplate mania . "	norel	1239..1385
He says the number of automobile choices is causing stress among consumers today , and that people will simply ignore new models that lack a well-defined image .	restatement	1386..1545
" The winners , " he predicts , " will be brands from car makers that have traditionally been associated with quality and value . "	alternative	1546..1670
He says it 's important for a new make to be as distinctive as possible while still retaining links to the parent company 's quality image .	norel	1673..1810
He applauds Toyota and Nissan Motor Co. for creating separate divisions for their new luxury models , rather than simply adding more nameplates to their standard car lines .	cause	1811..1982
Some auto executives believe the benefits of more choice outweigh the drawbacks .	norel	1985..2065
" There 's more noise out there , and the consumer may have to work harder to cut through it , " says Vincent P. Barabba , executive director of market research and planning at General Motors Corp .	contrast	2066..2257
" But the reward is that there 's less need to make tradeoffs " in choosing one 's wheels .	contrast	2258..2344
Jeanene Page , of North Salt Lake City , Utah , likes the broader selection .	norel	2347..2420
She wants something big , and already has looked at the Chrysler New Yorker and Lincoln Town Car .	entrel	2421..2517
Now , the 55-year-old car shopper is zeroing in on a full-sized van , figuring that it 's just the thing to haul nine grandchildren and pull a boat at the same time .	entrel	2518..2680
" That seems to be what all my friends are using to take the grandkids to the lake , " she says .	conjunction	2681..2774
Market segmentation in cars is n't new	norel	2777..2814
but it 's far more extensive than when Alfred P. Sloan Jr. conceived the idea 50 years ago .	contrast	2815..2906
The legendary GM chairman declared that his company would make " a car for every purse and purpose . "	synchrony	2907..3006
Now there are many cars for every purse and purpose .	norel	3009..3061
Just four years ago , GM planners divided the combined car and truck market into seven segments .	contrast	3062..3157
Today , they identify 19 distinct segments for cars , and another 11 for trucks and vans .	contrast	3158..3245
The number of makes has mushroomed because the U.S. is the world 's biggest and richest market for automobiles	norel	3248..3357
virtually every auto maker wants to sell here .	cause	3358..3405
For every brand like Renault or Fiat that has been squeezed out , others such as Isuzu , Daihatsu and Mitsubishi have come in .	cause	3406..3530
Detroit tries to counter the foreign invasion with new brands of its own .	norel	3533..3606
GM launched the Geo marque this year to sell cars made in partnership with foreign auto makers	instantiation	3607..3701
and next year GM 's long-awaited Saturn cars will make their debut .	conjunction	3702..3769
Ford Motor Co. created the Merkur nameplate in 1985 to sell its German-made touring sedans in the U.S.	conjunction	3770..3872
But slow sales forced Ford to kill the brand just last week .	contrast	3873..3933
When consumers have so many choices , brand loyalty is much harder to maintain .	norel	3936..4014
The Wall Street Journal 's " American Way of Buying " survey found that 53 % of today 's car buyers tend to switch brands .	restatement	4015..4132
For the survey , Peter D. Hart Research Associates and the Roper Organization each asked about 2,000 U.S. consumers about their buying habits .	entrel	4133..4274
Which cars do Americans favor most these days ?	norel	4277..4323
It 's hard to generalize	conjunction	4324..4347
but age seems to be the best predictor .	contrast	4348..4388
Adults under age 30 like sports cars , luxury cars , convertibles and imports far more than their elders do .	instantiation	4389..4495
Three of every 10 buyers under 30 would prefer to buy a sports car , compared with just 16 % of adults 30 and over , according to the Journal survey .	restatement	4496..4642
Young consumers prefer luxury cars by a 37 % to 28 % margin	norel	4645..4702
-- even though older buyers , because of their incomes , are more likely to actually purchase a luxury car .	concession	4703..4808
Perhaps most striking , 35 % of households headed by people aged 18 to 44 have at least one foreign car .	norel	4811..4913
That 's true of only 14 % of households headed by someone 60 or older .	contrast	4914..4982
Generally , imports appeal most to Americans who live in the West and are well-educated , affluent and , especially , young .	entrel	4983..5103
" For many baby boomers , buying a domestic car is a totally foreign experience , " says Christopher Cedergren , auto-market analyst with J.D. Power & Co. of Agoura Hills , Calif .	conjunction	5104..5277
Such preferences persist	norel	5280..5304
even though many Americans believe differences between imported and domestic cars are diminishing .	concession	5305..5403
Only 58 % of Americans now believe that foreign cars get better gas mileage than domestic models , the Journal survey found , down from 68 % in 1987 .	instantiation	5404..5549
Some 46 % give foreign cars higher quality ratings , down from 50 % two years ago .	conjunction	5550..5629
On the other hand , only 42 % say foreign cars are less comfortable than U.S. models , down from 55 % in 1987 .	contrast	5632..5738
People in the automotive business disagree over how susceptible younger Americans are to brand switching .	norel	5741..5846
" Once buying habits are formed , they 're very hard to break , " declares Thomas Mignanelli , executive vice president for Nissan 's U.S. sales operations .	instantiation	5847..5996
But out on Cobb Parkway , Ted Negas sees it differently .	contrast	5999..6054
" The competition is so intense that an owner 's loyalty to a dealership or a car is virtually nonexistent , " says Mr. Negas , vice president of Ed Voyles Oldsmobile , one of the first dealerships to locate on the strip .	restatement	6055..6270
Thus the very fickleness of baby boomers may make it possible to win them back , just as it was possible to lose them .	cause	6271..6388
The battle for customer loyalty is evident along the Cobb Parkway strip .	norel	6391..6463
Ed Voyles Olds recently established a special section in the service department for owners whose cars are less than a year old	instantiation	6464..6590
so they get quicker service .	cause	6591..6620
Just down the street , Chris Volvo invites serious shoppers to test-drive a new Volvo to any other dealership along the strip , and compare the cars side-by-side .	conjunction	6621..6781
Manufacturers , too , are stretching further to lure buyers .	norel	6784..6842
GM 's Cadillac division , ignoring Detroit 's long-held maxim that safety does n't sell , is airing television commercials touting its cars ' safety features .	instantiation	6843..6995
Cadillac may be on to something .	norel	6998..7030
Some 60 % of the survey respondents said they would buy anti-lock brakes even if they carry a medium or high price tag .	cause	7031..7149
More than 50 % felt the same way about air bags .	conjunction	7150..7197
Both features appealed most to buyers under 45 .	conjunction	7198..7245
In contrast , dashboard computers , power seats and turbo-charged engines had little appeal .	norel	7246..7336
But even a little appeal has a lot of attraction these days .	contrast	7339..7399
GM 's Pontiac division is offering a turbo-charged V-6 engine on its Grand Prix model	instantiation	7400..7484
even though it expects to sell only about 4,000 cars equipped with that option .	contrast	7485..7565
The reason : Items with narrow appeal can be important in a market as fragmented as today 's .	cause	7566..7657
Americans spent more than $ 190 billion on new cars and trucks last year	norel	7660..7731
and just 1 % of that market exceeded Polaroid Co. 's sales of $ 1.86 billion .	conjunction	7732..7808
" Even if it 's only 1 % , " says GM 's Mr. Barabba , " would you throw away sales the size of Polaroid ? "	cause	7809..7906
