Westmoreland Coal Co. , realizing benefits of a sustained effort to cut costs and boost productivity , reported sharply improved third-quarter results .	O	9..158
The producer and marketer of low-sulfur coal said net income for the quarter was $ 5.9 million , or 71 cents a share , on revenue of $ 145.4 million .	O	161..306
For the year-earlier period , the company reported a loss of $ 520,000 or six cents a share .	O	307..397
In the latest nine months , the company earned $ 8.5 million , or $ 1.03 a share .	B-expansion	400..477
Last year 's net loss of $ 3,524,000 included a benefit of $ 1,640,000 from an accounting change .	B-expansion	478..572
Revenue for the nine months rose to $ 449 million from $ 441.1 million .	I-expansion	573..642
In an interview , Pemberton Hutchinson , president and chief executive , cited several reasons for the improvement : higher employee productivity and " good natural conditions " in the mines , as well as lower costs for materials , administrative overhead and debt interest .	O	645..911
In the latest nine months , Mr. Hutchinson said , total coal sales rose to about 14.6 million tons from about 14.3 million tons a year earlier .	O	914..1055
In addition , long-term debt has been trimmed to about $ 72 million from $ 96 million since Jan. 1 .	O	1056..1152
He predicted the debt ratio will improve further in coming quarters .	O	1153..1221
Westmoreland 's strategy is to retain and expand its core business of mining and selling low-sulphur coal in the Appalachia region .	B-entrel	1224..1354
The operating territory includes coal terminals on the Ohio River and in Newport News , Va .	B-entrel	1355..1445
Westmoreland exports about a fourth of its coal tonnage , including a significant amount of metallurgical coal produced by others that is used by steelmakers overseas .	I-entrel	1446..1612
For the past couple of years , Westmoreland has undertaken an aggressive streamlining of all aspects of its business .	B-expansion	1615..1731
Marginal operations and assets have been sold .	B-expansion	1732..1778
The size of the company 's board has been reduced to eight directors from 13 .	B-expansion	1779..1855
About 140 salaried management jobs and hundreds of hourly wage positions have been eliminated .	B-expansion	1856..1950
Even perks have been reduced .	I-expansion	1951..1980
For example , the chief executive himself now pays 20 % of the cost of his health benefits ; the company used to pay 100 % .	O	1981..2100
" I think the ship is now righted , the bilges are pumped and we are on course , " Mr. Hutchinson said of the restructuring program .	O	2103..2231
" Much of what we set out to do is completed . "	O	2232..2277
But he cautioned that Westmoreland 's third quarter is typically better than the fourth , so investors " should n't just multiply the third quarter by four " and assume the same rate of improvement can be sustained .	O	2280..2490
One difference , he said , is that the fourth quarter has significantly fewer workdays because of holidays and the hunting season .	O	2491..2619
" I do n't want to give the impression that everybody can relax now , " he said .	O	2622..2698
" We have to keep working at improving our core business to stay efficient .	B-expansion	2699..2773
It 's a process that never really ends . "	I-expansion	2774..2813
Nevertheless , Mr. Hutchinson predicted that 1989 would be " solidly profitable " for Westmoreland and that 1990 would bring " more of the same . "	O	2816..2957
For all of 1988 , the company reported an after-tax operating loss of $ 134,000 on revenue of $ 593.5 million .	B-comparison	2958..3065
An accounting adjustment made net income $ 1.5 million , or 18 cents a share .	I-comparison	3066..3141
In a move that complements the company 's basic strategy , its Westmoreland Energy Inc. unit is developing four coal-fired cogeneration plants with a partner in Virginia .	B-entrel	3144..3312
Some of the coal the plants buy will come from Westmoreland mines .	B-entrel	3313..3379
Mr. Hutchinson predicted that the unit 's contribution to company results in the 1990s " will be exciting . "	B-expansion	3380..3485
He said Westmoreland is looking at investment stakes in other cogeneration plants east of the Mississippi River .	I-expansion	3486..3598
Westmoreland expects energy demand to grow annually in the 2.5 % range in the early 1990s .	O	3601..3690
" We see coal 's piece of the action growing , " Mr. Hutchinson said .	O	3691..3756
" Coal prices , while not skyrocketing , will grow modestly in real terms , we think .	O	3757..3838
