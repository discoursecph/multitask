General Motors Corp. 's general counsel hopes to cut the number of outside law firms the auto maker uses from about 700 to 200 within two years.	O	9..153
Harry J. Pearce, named general counsel in May 1987, says the reduction is a cost-cutting measure and an effort to let the No. 1 auto maker's 134-lawyer in-house legal department take on matters it is better equipped and trained to handle.	B-entrel	156..394
GM trimmed about 40 firms from its approved local counsel list, Mr. Pearce says.	I-entrel	395..475
The move is consistent with a trend for corporate legal staffs to do more work in-house, instead of farming it out to law firms.	B-restatement	478..606
Mr. Pearce set up GM's first in-house litigation group in May with four lawyers, all former assistant U.S. attorneys with extensive trial experience.	B-conjunction	607..756
He intends to add to the litigation staff.	B-entrel	757..799
Among the types of cases the in-house litigators handle are disputes involving companies doing business with GM and product-related actions, including one in which a driver is suing GM for damages resulting from an accident.	I-entrel	800..1024
Mr. Pearce has also encouraged his staff to work more closely with GM's technical staffs to help prevent future litigation.	O	1027..1150
GM lawyers have been working with technicians to develop more uniform welding procedures -- the way a vehicle is welded has a lot to do with its durability.	B-conjunction	1151..1307
The lawyers also monitor suits to identify specific automobile parts that cause the biggest legal problems.	I-conjunction	1308..1415
Mr. Pearce says law firms with the best chance of retaining or winning business with GM will be those providing the highest-quality service at the best cost -- echoing similar directives from GM's auto operations to suppliers.	O	1418..1644
This doesn't necessarily mean larger firms have an advantage; Mr. Pearce said GM works with a number of smaller firms it regards highly.	O	1647..1783
Mr. Pearce has shaken up GM's legal staff by eliminating all titles and establishing several new functions, including a special-projects group that has made films on safety and drunk driving.	O	1784..1975
FEDERAL PROSECUTORS are concluding fewer criminal cases with trials.	O	1978..2046
That's a finding of a new study of the Justice Department by researchers at Syracuse University.	O	2049..2145
David Burnham, one of the authors, says fewer trials probably means a growing number of plea bargains.	B-entrel	2146..2248
In 1980, 18% of federal prosecutions concluded at trial; in 1987, only 9% did.	I-entrel	2249..2327
The study covered 11 major U.S. attorneys' offices -- including those in Manhattan and Brooklyn, N.Y., and New Jersey -- from 1980 to 1987.	O	2330..2469
The Justice Department rejected the implication that its prosecutors are currently more willing to plea bargain.	B-restatement	2472..2584
"Our felony caseloads have been consistent for 20 years," with about 15% of all prosecutions going to trial, a department spokeswoman said.	I-restatement	2585..2724
The discrepancy is somewhat perplexing in that the Syracuse researchers said they based their conclusions on government statistics.	O	2725..2856
"One possible explanation for this decline" in taking cases to trial, says Mr. Burnham, "is that the number of defendants being charged with crimes by all U.S. attorneys has substantially increased."	O	2859..3058
In 1980, the study says, prosecutors surveyed filed charges against 25 defendants for each 100,000 people aged 18 years and older.	O	3061..3191
In 1987, prosecutors filed against 35 defendants for every 100,000 adults.	O	3192..3266
Another finding from the study: Prosecutors set significantly different priorities.	O	3269..3352
The Manhattan U.S. attorney's office stressed criminal cases from 1980 to 1987, averaging 43 for every 100,000 adults.	B-contrast	3353..3471
The Manhattan U.S. attorney's office stressed criminal cases from 1980 to 1987, averaging 43 for every 100,000 adults.But the New Jersey U.S. attorney averaged 16.	B-contrast	3472..3517
On the civil side, the Manhattan prosecutor filed an average of only 11 cases for every 100,000 adults during the same period; the San Francisco U.S. attorney averaged 79.	I-contrast	3518..3689
The study is to provide reporters, academic experts and others raw data on which to base further inquiries.	O	3692..3799
IMELDA MARCOS asks for dismissal, says she was kidnapped.	O	3802..3859
The former first lady of the Philippines, asked a federal court in Manhattan to dismiss an indictment against her, claiming among other things, that she was abducted from her homeland.	B-entrel	3862..4046
Mrs. Marcos and her late husband, former Philippines President Ferdinand Marcos, were charged with embezzling more than $100 million from that country and then fraudulently concealing much of the money through purchases of prime real estate in Manhattan.	I-entrel	4047..4301
Mrs. Marcos's attorneys asked federal Judge John F. Keenan to give them access to all U.S. documents about her alleged abduction.	O	4304..4433
The U.S. attorney's office, in documents it filed in response, said Mrs. Marcos was making the "fanciful -- and factually unsupported -- claim that she was kidnapped into this country" in order to obtain classified material in the case.	B-conjunction	4434..4670
The office also said Mrs. Marcos and her husband weren't brought to the U.S. against their will after Mr. Marcos was ousted as president.	I-conjunction	4671..4808
The prosecutor quoted statements from the Marcoses in which they said they were in this country at the invitation of President Reagan and that they were enjoying the hospitality of the U.S.	O	4809..4998
Lawyers for Mrs. Marcos say that because she was taken to the U.S. against her wishes, the federal court lacks jurisdiction in the case.	O	5001..5137
THE FEDERAL COURT of appeals in Manhattan ruled that the dismissal of a 1980 indictment against former Bank of Crete owner George Koskotas should be reconsidered.	B-entrel	5140..5302
The indictment, which was sealed and apparently forgotten by investigators until 1987, charges Mr. Koskotas and three others with tax fraud and other violations.	B-entrel	5303..5464
He made numerous trips to the U.S. in the early 1980s, but wasn't arrested until 1987 when he showed up as a guest of then-Vice President George Bush at a government function.	B-asynchronous	5465..5640
A federal judge in Manhattan threw out the indictment, finding that the seven-year delay violated the defendant's constitutional right to a speedy trial.	B-contrast	5641..5794
The appeals court, however, said the judge didn't adequately consider whether the delay would actually hurt the chances of a fair trial.	I-contrast	5795..5931
Mr. Koskotas is fighting extradition proceedings that would return him to Greece, where he is charged with embezzling more than $250 million from the Bank of Crete.	B-entrel	5932..6096
His attorney couldn't be reached for comment.	I-entrel	6097..6142
PRO BONO VOLUNTARISM: In an effort to stave off a plan that would require all lawyers in New York state to provide twenty hours of free legal aid a year, the state bar recommended an alternative program to increase voluntary participation in pro bono programs.	O	6145..6405
The state bar association's policy making body, the House of Delegate, voted Saturday to ask Chief Judge Sol Wachtler to give the bar's voluntary program three years to prove its effectiveness before considering mandatory pro bono.	O	6406..6637
"We believe our suggested plan is more likely to improve the availability of quality legal service to the poor than is the proposed mandatory pro bono plan and will achieve that objective without the divisiveness, distraction, administrative burdens and possible failure that we fear would accompany an attempt to impose a mandatory plan," said Justin L. Vigdor of Rochester, who headed the bar's pro bono study committee.	O	6638..7060
DALLAS AND HOUSTON law firms merge: Jackson & Walker, a 130-lawyer firm in Dallas and Datson & Scofield, a 70-lawyer firm in Houston said they have agreed in principle to merge.	O	7063..7240
The consolidated firm, which would rank among the 10 largest in Texas, would operate under the name Jackson & Walker.	B-entrel	7241..7358
The merger must be formally approved by the partners of both firms but is expected to be completed by year end.	B-entrel	7359..7470
Jackson & Walker has an office in Fort Worth, Texas, and Dotson & Scofield has an office in New Orleans.	I-entrel	7471..7575
PILING ON?	O	7578..7588
Piggybacking on government assertions that General Electric Co. may have covered up fraudulent billings to the Pentagon, two shareholders have filed a civil racketeering suit against the company.	B-restatement	7589..7784
The suit was filed by plaintiffs' securities lawyer Richard D. Greenfield in U.S. District Court in Philadelphia.	B-restatement	7785..7898
He seeks damages from the company's 15 directors on grounds that they either "participated in or condoned the illegal acts ... or utterly failed to carry out their duties as directors."	I-restatement	7899..8084
GE is defending itself against government criminal charges of fraud and false claims in connection with a logistics-computer contract for the Army	B-entrel	8085..8232
The trial begins today in federal court in Philadelphia	B-asynchronous	8233..8289
The government's assertions of the cover-up were made in last minute pretrial motions.	I-asynchronous	8290..8376
GE, which vehemently denies the government's allegations, denounced Mr. Greenfield's suit.	O	8377..8467
"It is a cheap-shot suit -- procedurally defective and thoroughly fallacious -- which was hurriedly filed by a contingency-fee lawyer as a result of newspaper reports," said a GE spokeswoman.	O	8468..8659
She added that the company was considering bringing sanctions against Mr. Greenfield for making "grossly inaccurate and unsupported allegations.	O	8660..8804
