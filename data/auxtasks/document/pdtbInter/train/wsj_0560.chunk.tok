When Westinghouse Electric Corp. shuttered its massive steam turbine plant in Lester , Pa. , three years ago , it seemed like the company had pulled the plug on its century-old power generation business .	O	9..209
But now Westinghouse is enjoying a resurgence in demand for both steam and combustion turbines and may even join the growing legion of independent electric producers .	B-conjunction	212..378
And with its new venture with Japan 's Mitsubishi Heavy Industries Ltd. , announced last week , it is poised to penetrate growing markets overseas .	I-conjunction	379..523
For the first time since the mid-1970s , Westinghouse this year has seen a significant increase in orders for power plants .	O	526..648
Most are from independent producers instead of regulated utilities , and Westinghouse believes it will ride a wave of demand stretching over the next six years .	O	649..808
Analysts agree , predicting that the revived market could significantly boost Westinghouse 's bottom line in coming years .	O	811..931
" Westinghouse 's earnings could be materially enhanced in the mid-1990s or sooner , " says Russell L. Leavitt , of Salomon Brothers Inc .	O	932..1064
The company expects a need for 140,000 megawatts of new generation in the U.S. over the next decade .	B-conjunction	1065..1165
Already this year , it has received orders for four 150-megawatt advanced combustion turbines from Florida Power & Light Co. and for two 300-megawatt plants from Intercontinental Energy Corp. , among others .	I-conjunction	1166..1371
Westinghouse 's own role as a supplier also is changing .	B-cause	1374..1429
In the past , the company usually took token equity positions in power plants it supplied as a " kicker " to close deals .	B-contrast	1430..1548
But last June 's annnouncement that Westinghouse would put up all of the $ 70 million to build a new 55-megawatt plant could herald a new age .	B-entrel	1551..1691
Westinghouse 's plant will provide electrical power to the Southern California Edison Co. and backup power and steam to the U.S. Borax & Chemical Co .	I-entrel	1692..1840
" We have n't decided on a strategy yet , but we could become an independent producer depending on whether we 're the developer or just the supplier , " says Theodore Stern , executive vice president of the company 's energy and utility systems group .	O	1843..2086
At the same time , Westinghouse hopes its venture with Mitsubishi will help fend off growing competition , particularly in the U.S. , from such European competitors as Asea Brown Boveri AG , Siemens AG , and British General Electric Co .	O	2089..2320
Under the agreement , Westinghouse will be able to purchase smaller combustion turbines from its Japanese partner , and package and sell them with its own generators and other equipment .	B-conjunction	2321..2505
Westinghouse also jointly will bid on projects with Misubishi , giving it an edge in developing Asian markets .	B-conjunction	2508..2617
In addition , the two companies will develop new steam turbine technology , such as the plants ordered by Florida Power , and even utilize each other 's plants at times to take advantage of currency fluctuations .	I-conjunction	2618..2826
" Even though we 'll still compete against Mitsubishi , we can also work jointly on some projects , and we 'll gain a lot of sourcing flexibility , " Mr. Stern contends .	O	2829..2991
The Westinghouse-Mitsubishi venture was designed as a non-equity transaction , circumventing any possible antitrust concerns .	O	2994..3118
Westinghouse carefully crafted the agreement because the Justice Department earlier this year successfully challenged a proposed steam turbine joint venture with Asea Brown Boveri .	O	3119..3299
It is expected that the current surge in demand for new power will be filled primarily by independent producers which , unlike utilities , are n't regulated and therefore do n't need government approval to construct new plants .	O	3302..3525
Westinghouse expects about half of its new orders for turbines to come from independent producers for at least the next six years .	O	3526..3656
Despite shutdowns of the company 's Lester and East Pittsburgh plants , the company believes it has sufficient capacity to meet near-term demand with its much smaller and more efficient manufacturing facilities in North Carolina .	O	3659..3886
Still , Westinghouse acknowledges that demand from independent producers could evaporate if prices for fuel such as natural gas or oil rise sharply or if utilities , which have been pressured by regulators to keep down rates , are suddenly freed to add significant generating capacity .	O	3889..4171
Even if that scenario occurs , Westinghouse figures it is prepared .	B-cause	4174..4240
The company already is gearing up for a renaissance of nuclear power even though it has n't received an order for a domestic nuclear plant in a decade .	I-cause	4241..4391
John C. Marous , chairman and chief executive officer , says he expects a commercial order by 1995 for the company 's AP600 nuclear power plant , which is under development .	O	4392..4561
" Once we see an order , we expect it 'll be on line by 2000 .	O	4562..4620
