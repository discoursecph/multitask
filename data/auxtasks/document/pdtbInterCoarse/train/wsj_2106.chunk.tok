Right away you notice the following things about a Philip Glass concert .	O	9..81
It attracts people with funny hair ( or with no hair -- in front of me a girl with spiked locks sat beside a boy who had shaved his ) .	O	82..214
Whoever constitute the local Left Bank come out in force , dressed in black , along with a smattering of yuppies who want to be on the cutting edge .	B-expansion	215..361
People in Glass houses tend to look stoned .	I-expansion	362..405
And , if still conscious at the evening 's end , you notice something else : The audience , at first entranced and hypnotized by the music , releases its pent-up feelings in collective gratitude .	O	406..595
Currently in the middle of a four-week , 20-city tour as a solo pianist , Mr. Glass has left behind his synthesizers , equipment and collaborators in favor of going it alone .	B-expansion	598..769
He sits down at the piano and plays .	I-expansion	770..806
And plays .	B-entrel	807..817
Either one likes it or one does n't .	I-entrel	818..853
The typical Glass audience , which is more likely to be composed of music students than their teachers , certainly does .	B-comparison	854..972
The work , though , sounds like Muzak for spaceships .	B-expansion	975..1026
Philip Glass is the emperor , and his music the new clothes , of the avant-garde .	B-entrel	1027..1106
His success is easy to understand .	B-entrel	1107..1141
Softly introducing and explaining his pieces , Mr. Glass looks and sounds more like a shaggy poet describing his work than a classical pianist playing a recital .	I-entrel	1142..1302
The piano compositions , which have been labeled variously as minimalist , Oriental , repetitive , cyclical , monophonic and hypnotic , are relentlessly tonal ( therefore unthreatening ) , unvaryingly rhythmic ( therefore soporific ) , and unflaggingly harmonious but unmelodic ( therefore both pretty and unconventional ) .	O	1303..1612
It is music for people who want to hear something different but do n't want to work especially hard at the task .	B-expansion	1613..1724
It is E-Z listening for the now generation .	I-expansion	1725..1768
Mr. Glass has inverted the famous modernist dictum " less is more . "	B-contingency	1771..1837
His more is always less .	B-expansion	1838..1862
Far from being minimalist , the music unabatingly torments us with apparent novelties not so cleverly disguised in the simplicities of 4/4 time , octave intervals , and ragtime or gospel chord progressions .	I-expansion	1863..2066
But the music has its charm , and Mr. Glass has constructed his solo program around a move from the simple to the relatively complex .	O	2067..2199
" Opening " ( 1981 ) , from Glassworks , introduces the audience to the Glass technique : Never straying too far from the piano 's center , Mr. Glass works in the two octaves on either side of middle C , and his fingers seldom leave the keys .	O	2200..2432
There is a recognizable musical style here , but not a particular performance style .	O	2433..2516
The music is not especially pianistic ; indeed , it 's hard to imagine a bad performance of it .	O	2517..2609
Nothing bravura , no arpeggios , no ticklish fingering problems challenge the performer .	B-contingency	2610..2696
We hear , we may think , inner voices , but they all seem to be saying the same thing .	I-contingency	2697..2780
With " Planet News , " music meant to accompany readings of Allen Ginsberg 's " Wichita Vortex Sutra , " Mr. Glass gets going .	O	2783..2902
His hands sit farther apart on the keyboard .	B-expansion	2903..2947
Seventh chords make you feel as though he may break into a ( very slow ) improvisatory riff .	I-expansion	2948..3038
The chords modulate , but there is little filigree even though his fingers begin to wander over more of the keys .	O	3039..3151
Contrasts predictably accumulate : First the music is loud , then it becomes soft , then ( you realize ) it becomes louder again .	B-expansion	3152..3276
" The Fourth Knee Play , " an interlude from " Einstein on the Beach , " is like a toccata but it does n't seem to move much beyond its left-hand ground in " Three Blind Mice . "	I-expansion	3277..3445
When Mr. Glass decides to get really fancy , he crosses his hands and hits a resonant bass note with his right hand .	B-expansion	3448..3563
He does this in at least three of his solo pieces .	B-contingency	3564..3614
You might call it a leitmotif or a virtuoso accomplishment .	B-entrel	3615..3674
In " Mad Rush , " which came from a commission to write a piece of indeterminate length ( Mr. Glass charmingly , and tellingly , confessed that " this was no problem for me " ) , an A section alternates with a B section several times before the piece ends unresolved .	I-entrel	3675..3932
Not only is the typical Glasswork open-ended , it is also often multiple in its context ( s ) .	B-expansion	3935..4025
" Mad Rush " began its life as the accompaniment to the Dalai Lama 's first public address in the U.S. , when Mr. Glass played it on the organ at New York 's Cathedral of St. John the Divine .	I-expansion	4026..4212
Later it was performed on Radio Bremen in Germany , and then Lucinda Childs took it for one of her dance pieces .	B-expansion	4213..4324
The point is that any piece can be used as background music for virtually anything .	I-expansion	4325..4408
The evening ended with Mr. Glass 's " Metamorphosis , " another multiple work .	B-expansion	4411..4485
Parts 1 , 2 , and 5 come from the soundtrack of Errol Morris 's acclaimed film , " The Thin Blue Line , " and the two other parts from incidental music to two separate dramatizations of the Kafka story of the same name .	I-expansion	4486..4698
When used as background in this way , the music has an appropriate eeriness , as when a two-note phrase , a descending minor third , accompanies the seemingly endless litany of reports , interviews and confessions of witnesses in the Morris film .	O	4699..4940
Served up as a solo , however , the music lacks the resonance provided by a context within another medium .	O	4941..5045
Admirers of Mr. Glass may agree with the critic Richard Kostelanetz 's sense that the 1974 " Music in Twelve Parts " is as encyclopedic and weighty as " The Well-Tempered Clavier . "	O	5048..5224
But while making the obvious point that both composers develop variations from themes , this comparison ignores the intensely claustrophobic nature of Mr. Glass 's music .	O	5225..5393
Its supposedly austere minimalism overlays a bombast that makes one yearn for the astringency of neoclassical Stravinsky , the genuinely radical minimalism of Berg and Webern , and what in retrospect even seems like concision in Mahler .	O	5394..5628
Mr. Spiegelman is professor of English at Southern Methodist University and editor of the Southwest Review .	O	5631..5738
