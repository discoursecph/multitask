The recently revived enthusiasm among small investors for stock mutual funds has been damped by a jittery stock market and the tumult over program trading .	root	9..164
After hitting two-year highs this summer , net sales of stock funds slowed in September , according to the Investment Company Institute , a trade group .	norel	167..316
The sales recovery screeched to a halt this month , some analysts say .	conjunction	317..386
" Confidence was starting to come back because we did n't have wildly volatile days , " says Tyler Jenks , research director for Kanon Bloch Carre & Co. , a Boston research firm .	norel	389..561
" Now everything " -- such as program trading and wide stock market swings -- " that everyone had pushed back in their consciousness is just sitting right there . "	contrast	562..721
Net sales of stock funds in September totaled $ 839.4 million , down from $ 1.1 billion in August , the institute said .	norel	724..839
But if reinvested dividends are excluded , investors put in only $ 340 million more than they pulled out for the month .	concession	840..957
October 's numbers , which wo n't be released for a month , are down further , mutual fund executives say .	conjunction	958..1059
Investors in stock funds did n't panic the weekend after mid-October 's 190-point market plunge .	norel	1062..1156
Most of the those who left stock funds simply switched into money market funds .	alternative	1157..1236
And some fund groups said investors actually became net buyers .	conjunction	1237..1300
But the stock market swings have continued .	concession	1303..1346
The recent outcry over program trading will cast a pall over the stock-fund environment in the coming months , some analysts say .	conjunction	1347..1475
" The public is very close to having had it , " Mr. Jenks says .	norel	1478..1538
Investors pulled back from bond funds in September , too .	norel	1541..1597
Net sales of bond funds for the month totaled $ 1.1 billion , down two-thirds from $ 3.1 billion in August .	restatement	1598..1702
The major reason : heavy outflows from high-risk , high-yield junk bond funds .	cause	1703..1779
Big withdrawals from the junk funds have continued this month .	conjunction	1780..1842
Overall , net sales of all mutual funds , excluding money market funds , fell to $ 1.9 billion in September from $ 4.2 billion in August , the trade group said .	norel	1845..1999
" Small net inflows into stock and bond funds were offset by slight declines in the value of mutual fund stock and bond portfolios " stemming from falling prices , said Jacob Dreyer , the institute 's chief economist .	entrel	2000..2212
Many small investors went for the safety of money market funds .	norel	2215..2278
Assets of these and other short-term funds surged more than $ 5 billion in September , the institute said .	restatement	2279..2383
Analysts say additional investors transferred their assets into money funds this month .	conjunction	2384..2471
At Fidelity Investments , the nation 's largest fund group , money funds continue to draw the most business , says Michael Hines , vice president , marketing .	norel	2474..2626
In October , net sales of stock funds at Fidelity dropped sharply , Mr. Hines said .	contrast	2627..2708
But he emphasized that new accounts , new sales , inquiries and subsequent sales of stock funds are all up this month from September 's level .	contrast	2709..2848
Investor interest in stock funds " has n't stalled at all , " Mr. Hines maintains .	norel	2851..2929
He notes that most of the net sales drop stemmed from a three-day period following the Friday the 13th plunge .	restatement	2930..3040
" If that follows through next month , then it will be a different story , " he says .	norel	3043..3124
But , Mr. Hines adds , sales " based on a few days ' events do n't tell you much about October 's trends . "	contrast	3125..3225
One trend that continues is growth in the money invested in funds .	norel	3228..3294
Buoyed by the continued inflows into money funds , assets of all mutual funds swelled to a record $ 953.8 billion in September , up fractionally from $ 949.3 billion in August .	instantiation	3295..3467
Stock-fund managers , meantime , went into October with less cash on hand than they held earlier this year .	norel	3470..3575
These managers held 9.8 % of assets in cash at the end of September , down from 10.2 % in August and 10.6 % in September 1988 .	restatement	3576..3698
Large cash positions help buffer funds from market declines but can cut down on gains in rising markets .	entrel	3699..3803
Managers of junk funds were bolstering their cash hoards after the September cash crunch at Campeau Corp .	norel	3806..3911
Junk-portfolio managers raised their cash position to 9.4 % of assets in September from 8.3 % in August .	restatement	3912..4014
In September 1988 , that level was 9.5 % .	contrast	4015..4054
Investors in all funds will seek safety in the coming months , some analysts say .	norel	4057..4137
Among stock funds , the conservative growth-and-income portfolios probably will remain popular , fund specialists say .	cause	4138..4254
" There will be a continuation and possibly greater focus on conservative equity funds , at the expense of growth and aggressive growth funds , " says Avi Nachmany , an analyst at Strategic Insight , a New York fund-research concern .	norel	4257..4484
