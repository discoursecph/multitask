The commercial was absolutely silent.	B-restatement	9..46
Breaking into the raucous Chicago Bears-Cleveland Browns match during last week's Monday night football game, it was nothing but simple block letters superimposed on the TV screen.	I-restatement	47..227
Due to the earthquake in San Francisco, Nissan is donating its commercial air time to broadcast American Red Cross Emergency Relief messages.	B-entrel	230..372
Please contribute what you can," the ad said.	I-entrel	373..418
The Nissan logo flashed on the screen for a moment, and then came a taped plea for donations from former President Reagan -- followed by the silent print telling viewers where to call.	B-cause	419..603
Within two hours, viewers pledged over $400,000, according to a Red Cross executive.	I-cause	604..688
Call it disaster marketing.	B-entrel	691..718
Nissan Motor is just one of a slew of advertisers that have hitched their ads to the devastating San Francisco quake and Hurricane Hugo.	I-entrel	719..855
Sometimes, the ads attempt to raise money; always, they try to boost good will.	O	856..935
By advertising disaster relief, these companies are hoping to don a white hat and come out a hero.	O	936..1034
But the strategy can backfire; if the ads appear too self-serving, the companies may end up looking like rank opportunists instead of good Samaritans.	O	1035..1185
That hasn't deterred plenty of companies.	B-instantiation	1188..1229
Along with Nissan, Grand Metropolitan PLC's Burger King and New York Life Insurance have tied ads to Red Cross donations.	I-instantiation	1230..1351
Other ads don't bother with the fundraising; a touching, if self-congratulatory, American Telephone & Telegraph ad that aired Sunday intermixed footage of the devastation in San Francisco and Charleston, S.C., with interviews of people recounting how AT&T helped.	O	1352..1615
At Nissan, "we felt we wanted to do something to help them gather money, and we had this airtime on Monday Night Football," explains Brooke Mitzel, a Nissan advertising creative manager.	O	1618..1804
"What did we get out of it?	O	1805..1832
We got some exposure ... and pretty much good will."	O	1833..1885
The ads are just the latest evidence of how television advertising is getting faster on the draw	B-restatement	1888..1985
While TV commercials typically take weeks to produce, advertisers in the past couple of years have learned to turn on a dime, to crash out ads in days or even hours	B-instantiation	1986..2151
The big brokerage houses learned the art of the instant commercial after the 1987 crash, when they turned out reassuring ads inviting investors right back into the stock market.	I-instantiation	2152..2329
They trotted out another crop of instant commercials after the sudden market dip a few weeks ago	B-conjunction	2330..2427
Nissan created its quake ad in a weekend.	I-conjunction	2428..2469
But as advertisers latch onto disasters with increasing frequency, they risk hurting themselves as much as helping the cause	B-cause	2472..2597
They chance alienating the customers they hope to woo by looking like opportunistic sharks.	I-cause	2598..2689
"People see extra messages in advertising, and if a manufacturer is clearly trying to get something out of it ... if it's too transparent ... then consumers will see through that," warns John Philip Jones, chairman of the advertising department at the Newhouse School of Public Communications at Syracuse University.	O	2690..3006
"It can backfire because companies can step across the line and go too far, be too pushy," agrees Gary Stibel, a principal with New England Consulting Group, Westport, Conn.	O	3009..3182
"The ultimate form of charity is when you don't tell anyone."	O	3183..3244
Still, he says that only a few of the quake-related campaigns have been "tasteless" and that "the majority have been truly beneficial to the people who need the help.	O	3245..3411
We don't consider that ambulance chasing."	O	3412..3454
The companies running the disaster ads certainly don't see themselves as ambulance chasers, either.	O	3457..3556
Burger King's chief executive officer, Barry Gibbons, stars in ads saying that the fast-food chain will donate 25 cents to the Red Cross for every purchase of a BK Doubles hamburger.	O	3557..3739
The campaign, which started last week and runs through Nov. 23, with funds earmarked for both the quake and Hugo, "was Barry's idea," a spokeswoman says.	O	3740..3893
"Barry felt very committed.	O	3894..3921
He felt we should be giving something back."	O	3922..3966
While the campaign was Mr. Gibbons's idea, however, he won't be paying for it: The donations will come out of the chain's national advertising fund, which is financed by the franchisees.	O	3969..4155
And by basing donations on BK Doubles, a new double-hamburger line the fast-food chain is trying to push, Burger King works a sales pitch into its public-service message.	O	4156..4326
Toyota's upscale Lexus division, a sponsor of the World Series, also put in a plug for Red Cross donations in a World Series game it sponsored.	O	4329..4472
"The World Series is brought to you by Lexus, who urges you to help relieve the suffering caused by the recent earthquake ... ," the game announcer said.	O	4473..4626
And New York Life made a plea for Red Cross donations in newspaper ads in the San Francisco area, latching onto the coattails of the Red Cross's impeccable reputation: "The Red Cross has been helping people for 125 years.	O	4627..4848
New York Life has been doing the same for over 140 years."	O	4849..4907
Nancy Craig, advertising manager for the Red Cross, readily admits "they're piggybacking on our reputation."	O	4910..5018
But she has no problem with that, she says: "In the meanwhile, they're helping us."	O	5019..5102
The Red Cross doesn't track contributions raised by the disaster ads, but it has amassed $46.6 million since it first launched its hurricane relief effort Sept. 23.	O	5103..5267
Ad Notes... .	O	5270..5283
NEW ACCOUNT:	O	5286..5298
Northrup King Co., Golden Valley, Minn., awarded its $4 million field-crop-seeds account to Creswell, Munsell, Fultz & Zirbel, a Cedar Rapids, Iowa, division of Young & Rubicam	B-asynchronous	5299..5476
The account had previously been handled by Saatchi & Saatchi Wegener, New York.	I-asynchronous	5477..5556
TV GUIDE:	O	5559..5568
Wieden & Kennedy, Portland, Ore., was named to handle the News Corp. publication's $1 million to $2 million trade-ad account.	O	5569..5694
N W Ayer, the New York agency that had handled the account since 1963, resigned the account about two weeks ago.	O	5695..5807
NO ALCOHOL:	O	5810..5821
Miller Brewing Co. will introduce its first non-alcoholic beer Jan. 1.	O	5822..5892
The brew, called Miller Sharp's, will be supported by ads developed by Frankenberry, Laughlin & Constable, Milwaukee.	O	5893..6010
