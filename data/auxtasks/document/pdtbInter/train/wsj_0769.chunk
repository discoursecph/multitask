The surprise resignation yesterday of British Chancellor of the Exchequer Nigel Lawson sent sterling into a tailspin against the dollar by creating uncertainties about the direction of the British economy.	B-conjunction	9..214
The U.S. unit also firmed against other currencies on the back of sterling's tumble, as market participants switched out of pounds.	I-conjunction	217..348
The pound also dropped precipitously against the mark, falling below the key 2.90-mark level to 2.8956 marks from 2.9622 marks late Wednesday.	O	351..493
Mr. Lawson's resignation shocked many analysts, despite the recent recurring speculation of a rift between the chancellor and Prime Minister Margaret Thatcher.	B-conjunction	496..655
Indeed, only hours earlier, Mrs. Thatcher had called Mr. Lawson's economic policies "sound" and said she has "always supported" him.	I-conjunction	658..790
"There was a general feeling that we'd seen the worst," said Patrick Foley, deputy chief economic adviser for Lloyds Bank in London.	O	793..925
"The resignation came as a great surprise."	O	926..969
Graham Beale, manager of foreign-exchange operations at Hong Kong & Shanghai Banking Corp. in New York, added that Mrs. Thatcher's comments reinforced the market's growing confidence about sterling and compounded the unit's later decline.	O	972..1210
"The market was caught totally the wrong way... .	O	1213..1262
Everyone was extremely long on sterling," Mr. Beale said.	O	1263..1320
In late New York trading yesterday, the dollar was quoted at 1.8400 marks, up from 1.8353 marks late Wednesday, and at 142.10 yen, up from 141.52 yen late Wednesday	B-contrast	1323..1488
Sterling was quoted at $1.5765, sharply down from $1.6145 late Wednesday.	I-contrast	1489..1562
In Tokyo Friday, the U.S. currency opened for trading at 142.02 yen, up from Thursday's Tokyo close of 141.90 yen.	O	1565..1679
Few analysts had much good to say about the pound's near-term prospects, despite the fact that most don't anticipate a shift in Mrs. Thatcher's economic policies.	O	1682..1844
Mr. Foley of Lloyds noted that Mr. Lawson's replacement, John Major, the British foreign minister, will take time to establish his credibility and, in the meantime, sterling could trend downward in volatile trade	B-concession	1847..2060
But Mr. Foley predicted few economic policy changes ahead, commenting that Mr. Major shares "a very similar view of the world" with Mr. Lawson	B-conjunction	2063..2206
Bob Chandross, chief economist at Lloyds Bank in New York, also noted that the pound's sharp decline is pegged more to uncertainty in the market than a vision of altered United Kingdom economic policies.	I-conjunction	2209..2412
Unless Mr. Lawson's resignation leads to a change in British interest-rate policy -- Mrs. Thatcher's administration firmly supports high interest rates to keep inflation in check -- or posturing toward full inclusion in the European Monetary System's exchange-rate mechanism, Mr. Lawson's withdrawal will have little long-term impact on exchange rates, Mr. Chandross concluded.	O	2415..2792
Also announcing his resignation Thursday was Alan Walters, Mrs. Thatcher's economic adviser and Mr. Lawson's nemesis.	O	2795..2912
The pound, which had been trading at about $1.6143 in New York prior to Mr. Lawson's announcement, sank more than two cents to $1.5930, prompting the Federal Reserve Bank to buy pounds for dollars.	O	2915..3112
The Fed's move, however, only proved a stopgap to the pound's slide and the Fed intervened for a second time at around $1.5825, according to New York traders.	O	3113..3271
Meanwhile, dollar trading was relatively uninspired throughout the session, according to dealers, who noted that Thursday's release of the preliminary report on the U.S. third-quarter gross national product was something of a nonevent.	O	3274..3509
U.S. GNP rose at an annual rate of 2.5% in the third quarter	B-conjunction	3512..3573
The implicit price deflator, a measure of inflation, was down to a 2.9% annual rate of increase in the quarter from a 4.6% rate of gain in the second quarter.	I-conjunction	3574..3732
In Europe, the dollar ended lower in dull trading	B-entrel	3735..3785
The market closed prior to Mr. Lawson's announcement.	I-entrel	3786..3839
On the Commodity Exchange in New York, gold for current delivery rose $3.40 to $372.50 an ounce in heavy trading	B-entrel	3842..3955
The close was the highest since August 3	B-entrel	3956..3997
Estimated volume was five million ounces.	I-entrel	3998..4039
In early trading in Hong Kong Friday, gold was quoted at $370.85 an ounce.	O	4042..4116
