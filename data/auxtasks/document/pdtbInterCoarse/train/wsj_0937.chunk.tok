This has been a week of stunning events behind what was once called the Iron Curtain and interesting shifts in official American policy toward Moscow .	B-expansion	9..159
It has also been a week when inside-the-beltway Washington has had a high old time gnawing over ex-President Reagan 's multimillion-dollar junket in Japan .	B-entrel	160..314
The latter may seem oddly irrelevant , if not downright trivial , given the big picture and the way we have handled it in the nation 's capital has done nothing to dispel that impression .	I-entrel	315..499
In fact , however , Mr. Reagan 's casual debasement of the office he so recently held raises issues about which Americans can actually do something .	B-comparison	500..645
Our ability to influence the outcome of events in Eastern Europe and the Soviet Union is far more marginal .	I-comparison	646..753
Those events continue to move at a rate , and in a direction , which leave informed commentary -- let alone policy -- far in their wake .	O	756..890
Earlier this week , Soviet Foreign Minister Eduard A. Shevardnadze confessed that the U.S.S.R. ignored universal human values by invading Afghanistan and , to put it bluntly , " engaged in a violation of the ABM Treaty " by building its radar station at Krasnoyarsk .	O	891..1152
Hungary is no longer a " Socialist Peoples " republic , the Communist Party no longer has automatic delegates in the U.S.S.R. 's Congress of Peoples Deputies and Egon Krenz was not backed unanimously by his fellow party functionaries when he took over as East Germany 's new maximum leader .	O	1153..1438
All of that is just for starters , or so the hundreds of thousands of Eastern Europeans in the streets seem to hope and are certainly demanding .	O	1439..1582
Of like , though lesser , note , Secretary of State James Baker put the administration four-square behind perestroika and glasnost , and therefore behind Mikhail Gorbachev , in a pair of carefully reasoned speeches over the past week or so .	B-expansion	1585..1820
And , last but not least , President George Bush now views the changes in Eastern Europe as " absolutely extraordinary " and believes that Mr. Krenz " ca n't turn the clock back " in East Germany because the change is too inexorable , " as he told the New York Times 's R.W. Apple Jr .	I-expansion	1821..2095
( In other words , after some highly visible dithering and public airing of differences , the administration has come down on the side of those who believe that what we are witnessing from Berlin to Siberia is a good thing to be welcomed , rather than a new thing to be feared or viewed with suspicion . )	O	2096..2395
All of this is what history will note , assuming that events do n't make it seem a bad joke , when the record of this time is put down .	O	2398..2530
For journalists , however , who write what they fondly view as history 's first draft , this has also been a week to give a lot of space and time to Ron and Nancy 's sales appearance in Japan on behalf of a communications giant and its controversial founder .	B-entrel	2531..2784
It has been a well-paid transaction , this bartering away of the prestige of the republic 's highest office .	B-expansion	2785..2891
The Japanese industrialist has coughed up at least $ 2 million , the Japanese government has put up just about as much , or so it is reported and at least one estimate puts the total tab at $ 7 million .	I-expansion	2892..3090
All of which has enabled those of us in Washington who enjoy wallowing in such things to go into high public dudgeon , as Mr. Apple and I did the other night on ABC 's " Nightline . "	O	3093..3271
Punching away , we raised what I still think were all the right issues and landed more than one hard blow , but at the end of the affair , there was just the tiniest nagging worry that we had been aiming at the wrong target .	O	3272..3493
As one of his defenders so aptly put it , President Reagan was simply doing what he had always done before his election ( and some would say thereafter as well ) .	O	3494..3653
He was performing for pay , and why should anyone expect anything more ?	O	3654..3724
Primarily because there 's more to the matter than Ronald Reagan 's personal values , or lack of them .	B-contingency	3727..3826
Selling the presidency for a mess of pottage is not so much a devaluation from the norm of public life today as it is a reflection of the disintegration of public standards .	I-contingency	3827..4000
The theme song for the 1980s has been , " Anything Goes , " and it has been whistled with gusto from Wall Street to some of the highest peaks of televangelism .	O	4001..4156
There are those who say that this is nothing new , that America has always suffered from a bad case of schizophrenia when it comes to the dichotomy between what is professed and what is practiced .	O	4159..4354
There is evidence to support that view .	B-expansion	4355..4394
Eighty-three years ago , William James wrote to H.G. Wells : " The moral flabbiness born of the exclusive worship of the bitch goddess success ... that , with the squalid cash interpretation put on the word success , is our national disease . "	I-expansion	4395..4632
But if it was the national disease in 1906 , it is today the national commonplace .	O	4635..4716
If there is no law against it , do it .	O	4717..4754
If the law leaves loopholes , use them .	O	4755..4793
If there is no moral prohibition that expressly forbids it , full speed ahead .	B-expansion	4794..4871
And if you are caught or if people complain , simply argue that " everyone does it " or " no one said I should n't " and brazen it out .	I-expansion	4872..5002
As a last recourse , when all else has failed and you are pinned , apologize for having disappointed those who trusted you but deny having actually done anything wrong .	B-expansion	5003..5169
( See , for instance , Jim Bakker 's remarks upon being sentenced to prison this Tuesday for defrauding the faithful . )	B-expansion	5170..5284
Consider the troubling dissonance between Mr. Shevardnadze 's speech of confession this week and the hang-tough defense of everyone concerned with the Iran-Contra affair .	B-expansion	5285..5454
The Soviet foreign minister publicly concedes that his government " violated norms of behavior " in Afghanistan and just plain lied about the radar station .	I-expansion	5455..5609
We have people in high place still lying through their teeth about Iran-Contra , and that apparently is n't going to change .	O	5610..5732
For that matter , those long ago identified as liars are still given respectful hearings in the press .	O	5733..5834
That is the key to the current " national disease . "	O	5837..5887
No one seems willing to hold anyone in public life to a standard higher than the narrowest construction of the law .	O	5888..6003
The occasional media witch hunt about some politician 's private peccadilloes notwithstanding , the general inclination is to offer a version of the old refrain , " Who am I to judge ? "	O	6004..6184
Thus , no standards , no judgment and no values .	O	6185..6231
" You are mad because he 's making so much money , " say President Reagan 's defenders .	O	6234..6316
No , we ought to be mad because he has demeaned the office we gave him , enlisting it in the service of private gain , just as we ought to be mad that public officials lie through their teeth , play disingenuous games about their activities or , to steal a phrase , make public service a private trough .	O	6317..6614
" I 'm not going to be stampeded into overreacting to any of this , President Bush told Mr. Apple in this week 's interview .	O	6617..6737
He was referring to the " absolutely extraordinary " events in Eastern Europe , and it is a defensible position .	O	6738..6847
But there is no defense at all for the ethos of the 1980s .	O	6848..6906
We did n't stampede into it , we slithered and slipped down the long slope , and now we have as its quintessential symbol a former president huckstering for a foreign poohbah .	O	6907..7079
Or perhaps that is a fitting symbol for the United States of 1989 : Everything for sale ; nothing of real value .	O	7080..7190
Mr. Carter is a political commentator who heads a television production firm .	O	7193..7270
