Wall Street is just about ready to line the bird cage with paper stocks .	root
For three years , a healthy economy and the export-boosting effects of a weak dollar propelled sales and earnings of the big paper companies to record levels .	-1_NS-Explanation
they more than doubled their prices for pulp , a raw material	-1_SN-Background
used in all sorts of paper ,	-1_NS-Elaboration
to $ 830 a metric ton this past spring from $ 380 a ton at the start of 1986 .	-2_NN-Same-unit
But now the companies are getting into trouble	-5_NS-Elaboration
But now the companies are getting into trouble	-3_SN-Contrast
because they undertook a record expansion program	-1_NN-Cause
while they were raising prices sharply .	-1_NN-Temporal
Third-quarter profits fell at several companies .	-3_NN-Cause
`` Put your money in a good utility or bank stock , not a paper company , ''	-4_NS-Evaluation
advises George Adler of Smith Barney .	-1_NS-Attribution
Other analysts are nearly as pessimistic .	-6_NS-Evaluation
Gary Palmero of Oppenheimer & Co. expects a 30 % decline in earnings between now and 1991 for `` commodity-oriented '' paper companies ,	-1_NS-Elaboration
which account for the majority of the industry .	-1_NS-Elaboration
Robert Schneider of Duff & Phelps sees paper-company stock prices falling 10 % to 15 % in 1990 , perhaps 25 %	-2_NN-Joint
if there 's a recession .	-1_NS-Condition
Paper companies concede that business has been off recently .	-5_NN-Topic-Comment
But they attribute much of the weakness to customer inventory reductions .	-1_NS-Explanation
that ,	-2_NN-Joint
that ,	-1_SN-Attribution
the industry is n't headed for a prolonged slump .	-2_NN-Same-unit
the industry is n't headed for a prolonged slump .	-1_SN-Background
`` It wo n't be an earthshaking drop , ''	-3_NS-Elaboration
a Weyerhaeuser spokesman says .	-1_NS-Attribution
Last week Mr. Adler lowered his rating from hold to `` avoid '' on Boise Cascade , Champion International , Great Northern Nekoosa , International Paper , Louisiana Pacific and Weyerhaeuser .	-13_NN-Joint
Oppenheimer 's Mr. Palmero , meanwhile , is steering clear of Gaylord Container , Stone Container and Federal Paper Board .	-1_NN-Joint
Mr. Schneider is cool to Georgia Pacific and Abitibi-Price .	-1_NN-Joint
Lawrence Ross of PaineWebber would avoid Union Camp .	-1_NN-Joint
the analysts are too pessimistic .	-5_NS-Evaluation
the analysts are too pessimistic .	-1_SN-Attribution
`` The odds of the dire predictions	-2_NS-Elaboration
`` The odds of the dire predictions	-1_SN-Attribution
about us being right	-1_NS-Elaboration
are small . ''	-2_NN-Same-unit
that it is better positioned than most companies for the coming overcapacity	-4_NN-Joint
that it is better positioned than most companies for the coming overcapacity	-1_SN-Attribution
because its individual mills can make more than one grade of paper .	-1_NS-Cause
A Boise-Cascade spokesman referred to a speech by Chairman John Fery ,	-2_NN-Joint
that markets generally are stable ,	-2_NS-Elaboration
that markets generally are stable ,	-1_SN-Attribution
although some risk of further price deterioration exists .	-1_NS-Contrast
that , unlike for some other paper products , demand for Stone 's principal commodity , unbleached containerboard , remains strong .	-5_NN-Joint
that , unlike for some other paper products , demand for Stone 's principal commodity , unbleached containerboard , remains strong .	-1_SN-Attribution
He expects the price for that product to rise even more next year .	-1_NS-Elaboration
analysts are skeptical of it	-3_NN-Joint
analysts are skeptical of it	-1_SN-Attribution
because it 's carrying a lot of debt .	-1_NS-Explanation
and we 're better positioned for any cyclical downturn	-4_NN-Joint
and we 're better positioned for any cyclical downturn	-2_SN-Attribution
and we 're better positioned for any cyclical downturn	-1_SN-Background
than we 've ever been . ''	-1_NN-Comparison
a number of other analysts are recommending them	-3_NN-Joint
a number of other analysts are recommending them	-1_SN-Attribution
because of their strong wood-products business .	-1_NS-Explanation
`` We 're not as exposed as the popular perception of us . ''	-3_NN-Joint
`` We 're not as exposed as the popular perception of us . ''	-1_SN-Attribution
that its main product , bleached paperboard ,	-2_NS-Explanation
that its main product , bleached paperboard ,	-1_SN-Attribution
which goes into some advertising materials and white boxes ,	-1_NS-Elaboration
historically does n't have sharp price swings .	-2_NN-Same-unit
Because the stock prices of some paper companies already reflect an expected profit slump ,	-49_NS-Elaboration
that next year the share prices of some companies may fall at most only 5 % to 10 % .	-3_NN-Cause
that next year the share prices of some companies may fall at most only 5 % to 10 % .	-2_SN-Attribution
that next year the share prices of some companies may fall at most only 5 % to 10 % .	-1_SN-Attribution
A company such as Federal Paper Board may be overly discounted	-4_NS-Elaboration
and looks `` tempting '' to him ,	-1_NN-Joint
he says ,	-2_NS-Attribution
though he is n't yet recommending the shares .	-3_NS-Contrast
Wall Street is n't avoiding everything	-69_NN-Contrast
connected with paper .	-1_NS-Elaboration
Mr. Palmero recommends Temple-Inland ,	-2_NS-Explanation
that it is `` virtually the sole major paper company	-2_NS-Explanation
that it is `` virtually the sole major paper company	-1_SN-Attribution
not undergoing a major capacity expansion , ''	-1_NS-Elaboration
and thus should be able to lower long-term debt substantially next year .	-2_NN-Cause
the company expects record earnings in 1989 ,	-6_NS-Elaboration
the company expects record earnings in 1989 ,	-1_SN-Attribution
and `` we 're still pretty bullish '' on 1990 .	-1_NN-Joint
their gloomy forecasts have a flip side .	-9_NN-Joint
their gloomy forecasts have a flip side .	-1_SN-Attribution
Some take a warm view of consumer-oriented paper companies ,	-1_NS-Elaboration
which buy pulp from the commodity producers	-1_NS-Elaboration
and should benefit from the expected declines in pulp prices .	-1_NN-Joint
Estimates on how much pulp prices will fall next year currently run between $ 150 and $ 250 a metric ton .	-3_NS-Elaboration
that the price drop should especially benefit the two big tissue makers , Scott Paper and Kimberly-Clark .	-5_NS-Evaluation
that the price drop should especially benefit the two big tissue makers , Scott Paper and Kimberly-Clark .	-1_SN-Attribution
that	-2_NS-Elaboration
that	-1_SN-Attribution
`` We should do well . ''	-2_NN-Same-unit
`` We should do well . ''	-1_SN-Background
