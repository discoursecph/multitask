A surprising surge in the U.S. trade deficit raised fears	(NN-Textual-organization(NS-Elaboration(NS-Elaboration(NS-Elaboration(NS-Elaboration
that the nation 's export drive has stalled ,	(NN-Joint
and caused new turmoil in financial markets .	NN-Joint))
The merchandise trade deficit widened in August to $ 10.77 billion ,	(NS-Elaboration(NS-Cause(NS-Elaboration(NN-Textual-organization(NS-Attribution
the Commerce Department reported ,	NS-Attribution)
a sharp deterioration from July 's $ 8.24 billion and the largest deficit of any month this year .	NN-Textual-organization)
Exports fell for the second month in a row ,	(NN-Comparison
while imports rose to a record .	NN-Comparison))
`` This is one of the worst trade releases	(NS-Elaboration(NS-Elaboration(NS-Attribution(NS-Temporal(NS-Elaboration
we 've had	NS-Elaboration)
since the dollar troughed out in 1987 , ''	NS-Temporal)
said Geoffrey Dennis , chief international economist at James Capel Inc .	NS-Attribution)
Like most analysts , Mr. Dennis was hesitant to read too much into one month 's numbers ;	(NS-Elaboration(NN-Comparison
but he said ,	(SN-Attribution
`` It indicates perhaps	(SN-Attribution
that the balance in the U.S. economy is not as good	(NN-Comparison
as we 've been led to believe . ''	NN-Comparison))))
The number had a troubling effect on Wall Street ,	(NS-Elaboration(NS-Elaboration
suggesting	(SN-Attribution
that more fundamental economic problems may underlie last Friday 's stock market slide .	SN-Attribution))
The Dow Jones Industrial Average tumbled more than 60 points after the report 's release ,	(NS-Elaboration(NN-Temporal
before recovering	(NS-Cause
to close 18.65 points lower at 2638.73 .	NS-Cause))
`` This bad trade number raises some deeper issues about the market decline , ''	(NS-Elaboration(NS-Attribution
said Norman Robertson , chief economist for Mellon Bank .	NS-Attribution)
`` It raises questions about more deep-seated problems , the budget deficit and the trade deficit and the seeming lack of ability	(NS-Elaboration
to come to grips with them . ''	NS-Elaboration))))))
The trade report drew yet another unsettling parallel to October 1987 .	(NS-Elaboration(NS-Cause
On Oct. 14 of that year , the announcement of an unusually large August trade deficit helped trigger a steep market decline .	(SN-Comparison(NS-Elaboration
The slide continued until the record 508-point market drop on Oct. 19 .	NS-Elaboration)
In 1987 , however , the news was the latest in a string of disappointments on trade ,	(NN-Comparison
while the current report comes after a period of improvement .	NN-Comparison)))
The bleak trade report was played down by the Bush administration .	(NS-Cause
Commerce Secretary Robert Mosbacher called the worsening trade figures `` disappointing after two very good months . ''	(NN-Joint
And White House spokesman Marlin Fitzwater said	(SN-Comparison(NS-Elaboration(SN-Attribution
the deficit was `` an unwelcome increase , ''	SN-Attribution)
adding	(SN-Attribution
that `` we 're hopeful	(SN-Attribution
that it simply is a one-month situation	(NN-Joint
and will turn around . ''	NN-Joint))))
But the figures reinforced the view of many private analysts	(NS-Cause(NS-Elaboration
that the improvement in the U.S. trade deficit has run out of steam .	NS-Elaboration)
`` The figures today add further evidence	(NS-Elaboration(NS-Attribution(NS-Elaboration
to support the view	(NS-Elaboration
that the improvement in the U.S. trade deficit has essentially stalled out at a level of about a $ 110 billion annual rate , ''	NS-Elaboration))
said Jeffrey Scott , a research fellow at the Institute for International Economics here .	NS-Attribution)
`` That 's still an improvement over last year ,	(NN-Comparison
but it leads one to conclude	(SN-Attribution
that basically we 've gotten all the mileage	(NN-Textual-organization(NS-Elaboration
we can	NS-Elaboration)
out of past dollar depreciation and past marginal cuts in the federal budget deficit . ''	NN-Textual-organization)))))))))))
Exports declined for the second consecutive month in August ,	(NS-Elaboration(NN-Comparison(NS-Attribution(NS-Elaboration
slipping 0.2 % to $ 30.41 billion ,	NS-Elaboration)
the Commerce Department reported .	NS-Attribution)
Imports , on the other hand , leaped 6.4 % to a record $ 41.18 billion .	NN-Comparison)
Not only was August 's deficit far worse than July 's ,	(SN-Comparison
but the government revised the July figure substantially from the $ 7.58 billion deficit	(NS-Elaboration
it had initially reported last month .	NS-Elaboration)))))
Many economists contend	(NS-Cause(SN-Attribution
that deep cuts in the U.S. budget deficit are needed	(SN-Temporal
before further trade improvement can occur .	SN-Temporal))
That 's because the budget deficit feeds an enormous appetite in this country for both foreign goods and foreign capital ,	(NS-Cause(NS-Cause
overwhelming the nation 's capacity to export .	NS-Cause)
`` People are sick and tired of hearing about these deficits ,	(NS-Elaboration(NS-Attribution(NN-Comparison
but the imbalances are still there	(NN-Joint
and they are still a problem , ''	NN-Joint))
said Mr. Robertson .	NS-Attribution)
In addition , the rise in the value of the dollar against foreign currencies over the past several months has increased the price of U.S. products in overseas markets	(NS-Elaboration(NN-Temporal(NS-Cause(NN-Joint
and hurt the country 's competitiveness .	NN-Joint)
Since March , exports have been virtually flat .	NS-Cause)
At the same time , William T. Archey , international vice president at the U.S. Chamber of Commerce , notes :	(SN-Attribution
`` Clearly the stronger dollar has made imports more attractive ''	(NS-Joint
by causing their prices to decline .	NS-Joint)))
Most economists expect the slowing U.S. economy to curb demand for imports .	(NS-Cause(NN-Comparison
But they foresee little substantial progress in exports unless the dollar and the federal budget deficit come down .	NN-Comparison)
`` The best result	(NS-Elaboration(NS-Attribution(NN-Textual-organization(NS-Elaboration
we could get from these numbers	NS-Elaboration)
would be to see the administration and Congress get serious about putting the U.S. on an internationally competitive economic footing , ''	NN-Textual-organization)
said Howard Lewis , vice president of international economic affairs at the National Association of Manufacturers .	NS-Attribution)
`` That must start with cutting the federal budget deficit . ''	NS-Elaboration)))))))
August 's decline in exports reflected decreases in sales of industrial supplies , capital goods and food abroad and increases in sales of motor vehicles , parts and engines .	(NS-Elaboration(NN-Joint
The jump in imports stemmed from across-the-board increases in purchases of foreign goods .	NN-Joint)
The numbers were adjusted for usual seasonal fluctuations .	NS-Elaboration))
Alan Murray contributed to this article .	(NN-Textual-organization
( In billions of U.S. dollars , not seasonally adjusted )	(NN-Textual-organization
*Newly industrialized countries : Singapore , Hong Kong , Taiwan , South Korea	(NN-Textual-organization
Source :	(NS-Attribution
Commerce Department	NS-Attribution)))))
