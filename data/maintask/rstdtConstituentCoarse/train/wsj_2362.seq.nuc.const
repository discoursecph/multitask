British Aerospace PLC and France 's Thomson-CSF S.A. said	(NS-Elaboration(NS-Elaboration(NS-Elaboration(NS-Elaboration(NS-Elaboration(NS-Elaboration(SN-Attribution
they are nearing an agreement	(NS-Cause(NS-Elaboration
to merge their guided-missile divisions ,	NS-Elaboration)
greatly expanding collaboration between the two defense contractors .	NS-Cause))
The 50-50 joint venture ,	(NN-Joint(NN-Textual-organization(NS-Elaboration
which may be dubbed Eurodynamics ,	NS-Elaboration)
would have combined annual sales of at least # 1.4 billion	(NS-Summary-Evaluation-Topic
( $ 2.17 billion )	NS-Summary-Evaluation-Topic))
and would be among the world 's largest missile makers .	NN-Joint))
After two years of talks , plans for the venture are sufficiently advanced	(NS-Elaboration(SN-Cause
for the companies to seek French and British government clearance .	SN-Cause)
The companies hope for a final agreement by year-end .	NS-Elaboration))
The venture would strengthen the rapidly growing ties between the two companies ,	(NS-Elaboration(NS-Elaboration(NS-Temporal(NN-Joint
and help make them a leading force in European defense contracting .	NN-Joint)
In recent months , a string of cross-border mergers and joint ventures have reshaped the once-balkanized world of European arms manufacture .	NS-Temporal)
Already , British Aerospace and French government-controlled Thomson-CSF collaborate on a British missile contract and on an air-traffic control radar system .	(NS-Cause(NN-Joint
Just last week they announced	(SN-Attribution
they may make a joint bid	(NS-Elaboration
to buy Ferranti International Signal PLC , a smaller British defense contractor	(NS-Elaboration
rocked by alleged accounting fraud at a U.S. unit .	NS-Elaboration))))
The sudden romance of British Aerospace and Thomson-CSF	(NN-Textual-organization(NS-Elaboration
-- traditionally bitter competitors for Middle East and Third World weapons contracts --	NS-Elaboration)
is stirring controversy in Western Europe 's defense industry .	NN-Textual-organization)))
Most threatened by closer British Aerospace-Thomson ties would be their respective national rivals ,	(NS-Elaboration(SN-Comparison(NS-Elaboration
including Matra S.A. in France and Britain 's General Electric Co. PLC .	NS-Elaboration)
But neither Matra nor GEC	(NN-Textual-organization(NS-Elaboration
-- unrelated to Stamford , Conn.-based General Electric Co. --	NS-Elaboration)
are sitting quietly by	(NS-Temporal
as their competitors join forces .	NS-Temporal)))
Yesterday , a source close to GEC confirmed	(NS-Comparison(NS-Elaboration(NS-Elaboration(NS-Elaboration(NS-Elaboration(SN-Attribution
that his company may join the Ferranti fight , as part of a possible consortium	(NS-Elaboration
that would bid against British Aerospace and Thomson-CSF .	NS-Elaboration))
Companies	(NN-Textual-organization(NS-Elaboration
with which GEC has had talks about a possible joint Ferranti bid	NS-Elaboration)
include Matra , Britain 's Dowty Group PLC , West Germany 's Daimler-Benz AG , and France 's Dassault group .	NN-Textual-organization))
But it may be weeks	(NS-Cause(NS-Attribution(SN-Temporal
before GEC and its potential partners decide whether to bid ,	SN-Temporal)
the source indicated .	NS-Attribution)
GEC plans first to study Ferranti 's financial accounts ,	(NS-Elaboration
which	(NN-Textual-organization(NS-Attribution
auditors recently said	NS-Attribution)
included # 215 million in fictitious contracts at a U.S. unit , International Signal & Control Group ,	(NS-Elaboration
with which Ferranti merged last year .	NS-Elaboration)))))
Also , any GEC bid might be blocked by British antitrust regulators ;	(NN-Summary-Evaluation-Topic(NS-Cause
Ferranti is GEC 's main competitor on several key defense-electronics contracts ,	(SN-Temporal
and its purchase by GEC may heighten British Defense Ministry worries about concentration in the country 's defense industry .	SN-Temporal))
A consortium bid , however , would diminish GEC 's direct role in Ferranti	(NS-Cause
and might consequently appease ministry officials .	NS-Cause)))
A British Aerospace spokeswoman appeared unperturbed by the prospect of a fight with GEC for Ferranti :	(NS-Elaboration
`` Competition is the name of the game , ''	(NS-Attribution
she said .	NS-Attribution)))
At least one potential GEC partner , Matra , insists	(NS-Elaboration(SN-Attribution
it is n't interested in Ferranti .	SN-Attribution)
`` We have nothing to say about this affair ,	(NS-Attribution(NS-Elaboration
which does n't concern us , ''	NS-Elaboration)
a Matra official said Sunday .	NS-Attribution))))))
The missile venture ,	(NS-Elaboration(NS-Elaboration(NN-Textual-organization(NS-Attribution
the British Aerospace spokeswoman said ,	NS-Attribution)
is a needed response to the `` new environment '' in defense contracting .	NN-Textual-organization)
For both Thomson and British Aerospace , earnings in their home markets have come under pressure from increasingly tight-fisted defense ministries ;	(NN-Joint
and Middle East sales , a traditional mainstay for both companies ' exports , have been hurt by five years of weak oil prices .	NN-Joint))
The venture 's importance for Thomson is great .	(NS-Elaboration(NS-Elaboration(NS-Elaboration
Thomson feels	(SN-Attribution
the future of its defense business depends on building cooperation with other Europeans .	SN-Attribution))
The European defense industry is consolidating ;	(SN-Cause(NS-Elaboration
for instance , West Germany 's Siemens AG recently joined GEC in a takeover of Britain 's Plessey Co. ,	(NN-Joint
and Daimler-Benz agreed to buy Messerschmitt-Boelkow Blohm G.m.b. H .	NN-Joint))
In missiles , Thomson is already overshadowed by British Aerospace and by its home rival , France 's Aerospatiale S.A. ;	(SN-Temporal
to better compete ,	(NS-Cause(NS-Attribution
Thomson officials say ,	NS-Attribution)
they need a partnership .	NS-Cause))))
To justify 50-50 ownership of the planned venture ,	(SN-Cause
Thomson would make a cash payment to British Aerospace .	SN-Cause))))
Annual revenue of British Aerospace 's missile business is about # 950 million ,	(NS-Attribution
a Thomson spokesman said .	NS-Attribution))
British Aerospace 's chief missile products include its 17-year-old family of Rapier surface-to-air missiles .	(NN-Joint
Thomson missile products , with about half British Aerospace 's annual revenue , include the Crotale surface-to-air missile family .	NN-Joint))
