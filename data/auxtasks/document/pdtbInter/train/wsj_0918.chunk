Du Pont Co. reported that third-quarter profit grew a robust 19% from a year ago on the strength of the company's operations in various chemicals and fibers, and in petroleum.	B-conjunction	9..184
Du Pont also raised its quarterly dividend to $1.20 a share from $1.05, a change that will increase the annualized payout to shareholders by some $140 million.	I-conjunction	187..346
Du Pont, unlike companies hurt badly by sharp price declines for basic chemicals and plastics, is benefiting from its broad range of businesses.	B-conjunction	349..493
The profit gain was made despite a weakening in the housing market, for which the company is a supplier, and a strengthening in the dollar, which lowers the value of overseas earnings when they are translated into dollars.	I-conjunction	494..716
The Wilmington, Del., company reported net of $547 million, or $2.36 a share, which was in line with Wall Street estimates.	O	719..842
In the year-earlier period, the company earned $461 million, or $1.91 a share.	B-entrel	843..921
Sales in the latest quarter were $8.59 billion, up 9.4% from $7.85 billion.	I-entrel	922..997
The dividend increase was Du Pont's second this year, an affirmation of statements by top executives that they intend to increase rewards to shareholders.	O	1000..1154
"We haven't benefited the shareholder as much as we need to," said Edgar Woolard Jr., Du Pont's chairman and chief executive officer, in an interview several months before he entered his current position in April.	O	1155..1368
The largest beneficiary will be Seagram Co., which owns about 23% of Du Pont.	O	1371..1448
A spokesman for Seagram, the Montreal wine and spirits concern controlled by the Bronfman family, said the company will post additional pretax profit of about $33 million a year because of the additional Du Pont dividends.	O	1449..1671
Du Pont also announced plans for a 3-for-1 stock split, although the initial higher dividend will be paid on pre-split shares.	O	1674..1800
Du Pont's stock rose $2.50 a share to close at $117.375 in New York Stock Exchange composite trading yesterday.	B-synchrony	1801..1912
Seagram closed at $84.75, up 12.5 cents a share in Big Board trading.	I-synchrony	1913..1982
Leading the gains for Du Pont in the latest quarter was its industrial products segment, where profit soared to $155 million from $99 million a year earlier.	O	1985..2142
The company benefited from continued strong demand and higher selling prices for titanium dioxide, a white pigment used in paints, paper and plastics.	O	2143..2293
James Fallon, a New Providence, N.J., marketing consultant to the chemicals industry, says Du Pont still holds an edge in making the pigment because the company was "first in with the technology" to lower costs.	O	2296..2507
He said Du Pont holds about 23% of the world-wide market, the largest single share, at a time when growing uses for the pigment have kept it in tight supply, although others are now adding low-cost production capacity.	O	2508..2726
Profit climbed to $98 million from $71 million in the petroleum segment, as Du Pont's Conoco Inc. oil company was helped by crude oil prices higher than a year ago and by higher natural gas prices and volume.	O	2729..2937
In the diversifed businesses segment, which includes herbicides, profit grew to $64 million from $27 million.	O	2940..3049
A spokesman said herbicide use in some areas of the U.S. was delayed earlier in the year by heavy rains, thus increasing sales in the third quarter.	O	3050..3198
In the fibers segment, profit rose to $180 million from $155 million, a gain Du Pont attributed to higher demand in the U.S. for most textile products.	O	3201..3352
Two segments posted lower earnings for the quarter.	B-restatement	3355..3406
Profit from coal fell to $41 million from $58 million, partly because of a miners' strike.	B-conjunction	3407..3497
And profit from polymers dropped to $107 million from $122 million amid what Du Pont called lower demand and selling prices in certain packaging and industrial markets.	I-conjunction	3498..3666
For the nine months, Du Pont earned $2 billion, or $8.46 a share, up 18% from $1.69 billion, or $7.03 a share, a year earlier.	B-entrel	3669..3795
Sales increased 10% to $26.54 billion from $24.05 billion.	I-entrel	3796..3854
The increased dividend will be paid Dec. 14 to holders of record Nov. 15.	B-conjunction	3857..3930
The stock split, which is subject to holder approval, would be paid on a still unspecified date in January to holders of record Dec. 21.	I-conjunction	3931..4067
