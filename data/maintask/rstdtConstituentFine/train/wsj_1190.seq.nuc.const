Time Warner Inc. is considering a legal challenge to Tele-Communications Inc. 's plan	(NS-elaboration-additional(NS-elaboration-additional(NN-Same-Unit(NS-elaboration-object-attribute
to buy half of Showtime Networks Inc. ,	NS-elaboration-object-attribute)
a move	(NS-elaboration-object-attribute
that could lead to all-out war between the cable industry 's two most powerful players .	NS-elaboration-object-attribute))
Time is also fighting the transaction on other fronts ,	(NS-antithesis(NS-attribution(NS-elaboration-general-specific
by attempting to discourage other cable operators from joining Tele-Communications as investors in Showtime ,	NS-elaboration-general-specific)
cable-TV industry executives say .	NS-attribution)
Time officials declined to comment .	NS-antithesis))
Last week , Tele-Communications agreed to pay Viacom Inc. $ 225 million for a 50 % stake in its Showtime subsidiary ,	(NS-elaboration-additional(NS-circumstance(NS-elaboration-additional(NS-antithesis(SN-background(NS-elaboration-additional(NS-elaboration-additional(NS-elaboration-additional
which is a distant second to Time 's Home Box Office in the delivery of pay-TV networks to cable subscribers .	NS-elaboration-additional)
Tele-Communications , the U.S. 's largest cable company , said	(SN-attribution
it may seek other cable partners to join in its investment .	SN-attribution))
Tele-Communications is HBO 's largest customer ,	(SN-concession(SN-circumstance(NS-elaboration-additional
and the two have a number of other business relationships .	NS-elaboration-additional)
Earlier this year , Time even discussed bringing Tele-Communications in as an investor in HBO ,	(NS-attribution
executives at both companies said .	NS-attribution))
The purchase of the Showtime stake is `` a direct slap in our face , ''	(NS-attribution
said one senior Time executive .	NS-attribution)))
Time is expected to mount a legal challenge in U.S. District Court in New York ,	(NS-elaboration-additional(NS-elaboration-object-attribute
where Viacom in May filed a $ 2.5 billion antitrust suit	(NS-elaboration-additional
charging Time and HBO with monopolizing the pay-TV business and trying to crush competition from Showtime .	NS-elaboration-additional))
Executives	(NS-elaboration-general-specific(SN-attribution(NN-Same-Unit(NS-elaboration-object-attribute
involved in plotting Time 's defense	NS-elaboration-object-attribute)
say	NN-Same-Unit)
it is now preparing a countersuit	(NS-elaboration-object-attribute
naming both Viacom and Tele-Communications as defendants .	NS-elaboration-object-attribute))
The executives say	(SN-attribution
Time may seek to break up the transaction	(NN-Disjunction(NS-temporal-after
after it is consummated ,	NS-temporal-after)
or may seek constraints	(NS-elaboration-object-attribute
that would prevent Tele-Communications from dropping HBO in any of its cable systems in favor of Showtime .	NS-elaboration-object-attribute))))))
Viacom officials declined to comment .	(NS-elaboration-additional(SN-antithesis
Jerome Kern , Tele-Communications ' chief outside counsel , said	(SN-attribution
he was n't aware of Time 's legal plans .	SN-attribution))
But he said	(NS-explanation-argumentative(SN-attribution
that any effort by Time	(NN-Same-Unit(NS-elaboration-object-attribute
to characterize the Tele-Communications investment in Showtime as anti-competitive	NS-elaboration-object-attribute)
would be `` the pot	(NS-elaboration-object-attribute
calling the kettle black . ''	NS-elaboration-object-attribute)))
`` It 's hard to see how an investment by the largest { cable operator } in the weaker of the two networks is anti-competitive ,	(NS-elaboration-additional(NS-attribution(NS-explanation-argumentative
when the stronger of the two networks is owned by the second largest '' cable operator ,	NS-explanation-argumentative)
Mr. Kern said .	NS-attribution)
In addition to owning HBO , with 22 million subscribers ,	(NN-Comparison(NS-elaboration-additional
Time Warner separately operates cable-TV system	(NS-elaboration-object-attribute
serving about 5.6 million cable-TV subscribers .	NS-elaboration-object-attribute))
Tele-Communications controls close to 12 million cable subscribers ,	(NN-Comparison
and Viacom has about one million .	NN-Comparison))))))
In its suit against Time , Viacom says	(NN-Contrast(SN-attribution
the ownership of both cable systems and cable-programming networks gives the company too much market power .	SN-attribution)
Time argues	(SN-attribution
that	(NN-Same-Unit
in joining up with Tele-Communications ,	(SN-circumstance
Viacom has potentially more power ,	(NS-reason
particularly since Viacom also owns cable networks MTV , VH-1 and Nick at Nite .	NS-reason))))))
Ironically , Tele-Communications and Time have often worked closely in the cable business .	(NS-elaboration-additional(SN-antithesis(NS-elaboration-general-specific
Together , they control nearly 40 % of Turner Broadcasting Systems Inc. ;	(NS-elaboration-set-member
Tele-Communications has a 21.8 % stake ,	(NN-Comparison
while Time Warner has a 17.8 % stake .	NN-Comparison)))
But since Time 's merger with Warner Communications Inc. , relations between the two have become strained .	SN-antithesis)
Each company worries	(SN-attribution
that the other is becoming too powerful	(NN-List
and too vertically integrated .	NN-List))))
Meanwhile , some legal observers say	(NS-explanation-argumentative(SN-attribution
the Tele-Communications investment and other developments are weakening Viacom 's antitrust suit against Time .	SN-attribution)
Viacom accuses Time in its suit of refusing to carry Showtime or a sister service , The Movie Channel , on Time 's Manhattan Cable TV system , one of the nation 's largest urban systems .	(NN-List(SN-concession
But yesterday , Manhattan Cable announced	(SN-attribution
it will launch Showtime on Nov. 1 to over 230,000 subscribers .	SN-attribution))
Showtime has also accused HBO of locking up the lion 's share of Hollywood 's movies	(SN-concession(NS-means
by signing exclusive contracts with all the major studios .	NS-means)
But Showtime has continued to sign new contracts with Hollywood studios ,	(NS-evidence
and yesterday announced	(SN-attribution
it will buy movies from Columbia Pictures Entertainment Inc. ,	(NS-elaboration-object-attribute
which currently has a non-exclusive arrangement with HBO .	NS-elaboration-object-attribute))))))))
