Small investors matched their big institutional brethren in anxiety over the weekend ,	(NN(NN(NS(SN
but most seemed to be taking a philosophical approach	(NN
and said	(SN
they were resigned to riding out the latest storm in the stock market .	SN)))
`` I 'm not losing faith in the market , ''	(NN(NS(NS(NS(NS
said Boston lawyer Christopher Sullivan	NS)
as he watched the market plunge on a big screen in front of a brokerage firm .	NS)
But he 's not so sure about everyone else .	(NS
`` I think	(NN(NS(SN
on Monday the small ( investors ) are going to panic and sell , ''	SN)
predicted Mr. Sullivan ,	(NS
whose investments include AMR Corp. 's American Airlines unit and several mutual funds .	NS))
`` And I think	(SN
institutions are going to come in and buy . . .	SN))))
I 'm going to hold on .	(NS
If I sell now ,	(SN
I 'll take a big loss . ''	SN)))
Some evinced an optimism	(NN(NS(NS
that had been rewarded	(NS
when they did n't flee the market in 1987 .	NS))
`` Oh , I bet	(NS(NS(SN
it 'll be up 50 points on Monday , ''	SN)
said Lucy Crump , a 78-year-old retired housewife in Lexington , Ky .	NS)
Mrs. Crump said	(SN
her Ashwood Investment Club 's portfolio lost about one-third of its value	(NN(NS
following the Black Monday crash ,	NS)
`` but no one got discouraged ,	(NN
and we gained that back	(NS
-- and more . ''	NS))))))
At the annual congress of the National Association of Investors Corp. at the Hyatt Regency hotel in Minneapolis , the scene was calm .	(NS
Some 500 investors	(NS(SN(NN(NN(NS
representing investor clubs from around the U.S .	NS)
were attending	NN)
when the market started to slide Friday .	NN)
But Robert Showalter , an official of the association , said	(SN
no special bulletins or emergency meetings of the investors ' clubs are planned .	SN))
In fact , some of the association 's members	(NS(NN(NS
-- long-term , buy-and-hold investors --	NS)
welcomed the drop in prices .	NN)
`` We hope to take advantage of it , ''	(NS(NS
said John Snyder , a member of a Los Angeles investors ' club .	NS)
He has four stocks in mind	(NS
to buy	(NS
if the prices drop to the level	(NS
he wants .	NS))))))))))
Not everyone is reacting so calmly , however ,	(NS(NN
and many wonder about the long-term implications of what is widely viewed as the cause of Friday 's slide , reluctance by banks	(NS
to provide financing for a buy-out of UAL Corp. , parent of United Airlines .	NS))
Marc Perkins , a Tampa , Fla. , investment banker , said	(NN(NN(NS(SN
the market drop is one of `` a tremendous number of signs	(NS
that the leveraged take-out era is ending .	NS))
There 's no question	(NS
that there 's a general distaste for leverage among lenders . ''	NS))
Mr. Perkins believes , however ,	(SN
that the market could be stabilized	(NS
if California investor Marvin Davis steps back in to the United bidding with an offer of $ 275 a share .	NS)))
Sara Albert , a 34-year-old Dallas law student , says	(NS(NS(NS(SN
she 's generally skittish about the stock market and the takeover activity	(NS
that seems to fuel it .	NS))
`` I have this feeling	(NS(NS(NS
that it 's built on sand , ''	NS)
she says ,	NS)
that the market rises	(NN
`` but there 's no foundation to it . ''	NN)))
She and her husband pulled most of their investments out of the market after the 1987 crash ,	(NS
although she still owns some Texaco stock .	NS))
Partly because of concern about the economy	(SN(NN
and partly because she recently quit her job as a legal assistant	(NS
to go to school ,	NS))
`` I think at this point	(SN
we want to be a lot more liquid . ''	SN))))))
Others wonder	(NN(NS(NS(SN
how many more of these shocks the small investor can stand .	SN)
`` We all assumed	(NS(NS(NS(NS(SN
October '87 was a one-time shot , ''	SN)
said San Francisco attorney David Greenberg .	NS)
`` We told the little guy	(NN(SN
it could only happen once in a lifetime ,	(NS
come on back .	NS))
Now it 's happening again . ''	NN))
Mr. Greenberg got out just before the 1987 crash	(NN(NN
and , to his regret , never went back	(NS
even as the market soared .	NS))
This time he 's ready to buy in	(NN(NS
`` when the panic wears off . ''	NS)
Still , he adds :	(NS(SN
`` We ca n't have this kind of thing happen very often .	SN)
When the little guy gets frightened ,	(NN
the big guys hurt badly .	NN)))))
Merrill Lynch ca n't survive	(NS
without the little guy . ''	NS)))
Small investors have tiptoed back into the market	(NN(NS(NS(NS
following Black Monday ,	NS)
but mostly through mutual funds .	NS)
Discount brokerage customers `` have been in the market somewhat but not whole hog	(NS(NN(NS(NS
like they were two years ago , ''	NS)
says Leslie Quick Jr. , chairman of the Quick & Reilly discount brokerage firm .	NS)
Hugo Quackenbush , senior vice president at Charles Scwhab Corp. , says	(SN
Schwab customers `` have been neutral to cautious recently about stocks . ''	SN))
Individual investors are still angry about program trading ,	(NS
Mr. Quackenbush says .	NS)))
Avner Arbel , a Cornell University finance professor , says	(SN
government regulators will have to more closely control program trading	(NS
to `` win back the confidence of the small investor . ''	NS))))
But it 's not only the stock market that has some small investors worried .	(NN(NS
Alan Helfman , general sales manager of a Chrysler dealership in Houston , said	(NS(SN
he and his mother have some joint stock investments ,	(SN
but the overall economy is his chief worry .	SN))
`` These high rollers took a big bath today , ''	(NS(NS
he said in his showroom ,	(NS
which is within a few miles of the multi-million dollar homes of some of Houston 's richest citizens .	NS))
`` And I can tell you	(SN
that a high roller is n't going to come in tomorrow	(NN
and buy a Chrysler TC by Maserati . ''	NN)))))
And , finally , there were the gloaters .	(NS
`` I got out in 1987 .	(NS(NS(NS(NS
Everything , ''	NS)
said Pascal Antori , an Akron , Ohio , plumbing contractor	(NS
who was visiting Chicago	(NN
and stopped by Fidelity Investments ' LaSalle Street office .	NN)))
`` I just stopped by	(NS
to see	(SN
how much I would have lost . ''	SN)))
Would Mr. Antori ever get back in ?	(NN
`` Are you kidding !	(NN
When it comes to money :	(SN
Once bitten , 2,000 times shy . ''	SN))))))))
