Rep. John Dingell, an important sponsor of President Bush's clean-air bill, plans to unveil a surprise proposal that would break with the White House on a centerpiece issue: acid rain.	O	9..193
The Michigan Democrat's proposal, which is expected today, is described by government sources and lobbyists as significantly weaker than the Bush administration's plan to cut utility emissions that lead to acid rain.	B-entrel	196..412
The administration's plan could cost utilities, mainly those that use coal, up to $4 billion a year.	I-entrel	413..513
The proposal comes as a surprise even to administration officials and temporarily throws into chaos the House's work on clean-air legislation.	O	516..658
As chairman of the House Energy and Commerce Committee, Mr. Dingell has almost single-handed control over clean-air legislation.	O	659..787
People close to the utility industry said Mr. Dingell's proposal appears to guarantee only an estimated seven-million-ton cut in annual sulfur-dioxide emissions that lead to acid rain, though additional cuts could be ordered later.	O	790..1021
Mr. Bush's legislative package promises to cut emissions by 10 million tons -- basically in half -- by the year 2000.	O	1022..1139
Although final details weren't available, sources said the Dingell plan would abandon the president's proposal for a cap on utilities' sulfur-dioxide emissions.	B-entrel	1142..1302
That proposal had been hailed by environmentalists but despised by utilities because they feared it would limit their growth.	I-entrel	1303..1428
It also would junk an innovative market-based system for trading emissions credits among polluters.	O	1429..1528
In addition, it is believed to offer a cost-sharing mechanism that would help subsidize the clean-up costs for the dirtiest coal-fired utilities in the country, sparing their customers from exorbitant jumps in their electric bills.	O	1531..1762
The administration, sticking to its vow of avoiding tax increases, has staunchly opposed cost-sharing.	O	1763..1865
Mr. Dingell's staff was expected to present its acid-rain alternative to other committee members, apparently in an attempt to appease Midwestern lawmakers from high-polluting states who insist on cost-sharing.	O	1868..2077
It isn't clear, however, whether support for the proposal will be broad enough to pose a serious challenge to the White House's acid-rain plan.	O	2078..2221
While the new proposal might appeal to the dirtiest utilities, it might not win the support of utilities, many in the West, that already have added expensive cleanup equipment or burn cleaner-burning fuels.	O	2224..2430
Lawmakers representing some of the cleaner utilities have been quietly working with the White House to devise ways to tinker with the administration bill to address their acid-rain concerns.	O	2431..2621
