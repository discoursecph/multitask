"""
A neural network based tagger discourse tagger
:author: Barbara Plank
"""
import argparse
import random
import time
import sys
import numpy as np
import os
import pickle
import pycnn

from lib.nnl import FFSequencePredictor, Layer, RNNSequencePredictor, BiRNNSequencePredictor
from lib.mio import read_discourse_chunk_file, load_embeddings_file

def main():
    parser = argparse.ArgumentParser(description="""Run a NN tagger""")
    parser.add_argument("--train1", help="train folder task1", required=False)
    parser.add_argument("--train2", help="train folder task2", required=False)
    parser.add_argument("--model", help="load model from file", required=False)
    parser.add_argument("--iters", help="training iterations [default: 10]", required=False,type=int,default=10)
    parser.add_argument("--in_dim", help="input dimension [default: 128]", required=False,type=int,default=128)
    parser.add_argument("--c_in_dim", help="input dimension for character embeddings [default: 100]", required=False,type=int,default=100)
    parser.add_argument("--h_dim", help="hidden dimension [default: 100]", required=False,type=int,default=100)
    parser.add_argument("--h_layers", help="number of stacked LSTMs [default: 1 = no stacking]", required=False,type=int,default=1)
    parser.add_argument("--test1", help="test folder task 1", required=False)
    parser.add_argument("--test2", help="test folder task 2", required=False)
    parser.add_argument("--dev", help="dev folder", required=False)

    parser.add_argument("--output", help="output predictions to stdout", required=False,default=False,action="store_true")
    parser.add_argument("--lower", help="lowercase words", required=False,default=False,action="store_true")
    parser.add_argument("--save", help="save model to file (appends .model as well as .pickle)", required=False,default=None)
    parser.add_argument("--embeds", help="word embeddings file", required=False, default=None)
    parser.add_argument("--sigma", help="noise sigma", required=False, default=0.2, type=float)
    parser.add_argument("--runs", help="runs [default: 1]", required=False,type=int,default=1)
    parser.add_argument("--ac", help="activation function [rectify, tanh, ...]", default="tanh", type=MyNNTaggerArgumentOptions.acfunct)
    parser.add_argument("--type", help="NN type [ff, rnn, lstm, bilstm]; default: lstm", required=False,default="bilstm",type=MyNNTaggerArgumentOptions.nntype)
    parser.add_argument("--trainer", help="trainer [sgd, adam] default: sgd", required=False, default="sgd")
    parser.add_argument("--cnn-seed", help="random seed for cnn (needs to be first argument!)", required=False, type=int)
    parser.add_argument("--cnn-mem", help="memory for cnn (needs to be first argument!)", required=False, type=int)
    parser.add_argument("--save-embeds", help="save word embeddings file", required=False, default=None)

    args = parser.parse_args()

    if args.cnn_seed:
        print(">>> using seed: ", args.cnn_seed, file=sys.stderr)
        np.random.seed(args.cnn_seed)
        random.seed(args.cnn_seed)

    if args.c_in_dim == 0:
        print("no character embeddings", file=sys.stderr)

    # average over several runs
    accuracy_runs=[]
    for r in range(args.runs):
        start = time.time()

        if args.model:
            print("loading model from file {}".format(args.model), file=sys.stderr)
            tagger = load(args)
        else:
            tagger = NNTagger(args.in_dim,
                              args.h_dim,
                              args.c_in_dim,
                              args.h_layers,
                              embeds_file=args.embeds,
                              nn=args.type,
                              activation=args.ac,
                              lower=args.lower,
                              noise_sigma=args.sigma)


        if args.train1 and args.train2:
            tagger.fit(args.train1, args.train2,args.iters, args.trainer, dev=args.dev)
            if args.save:
                save(tagger, args)

        # so, test1 for task1
        if args.test1:
            sys.stderr.write('\nTesting Task 1\n')
            sys.stderr.write('*******\n')
            test_X, test_Y, org_X, org_Y, task_labels = tagger.get_data_as_indices(args.test1, "task1")
            corr, total = tagger.evaluate(test_X, test_Y, org_X, org_Y, task_labels, output_predictions=args.output)
            accuracy_runs.append((corr, total, corr/total))
        print(("Task1 Done. Took {0:.2f} seconds.".format(time.time()-start)),file=sys.stderr)

        if args.test2:
            sys.stderr.write('\nTesting Task 2\n')
            sys.stderr.write('*******\n')
            test_X, test_Y, org_X, org_Y, task_labels = tagger.get_data_as_indices(args.test2, "task2")
            corr, total = tagger.evaluate(test_X, test_Y, org_X, org_Y, task_labels, output_predictions=args.output)
            accuracy_runs.append((corr, total, corr/total))
        print(("Task2 Done. Took {0:.2f} seconds.".format(time.time()-start)),file=sys.stderr)

    if accuracy_runs:
        macro=np.mean([a for c,t,a in accuracy_runs])*100
        std=np.std([a for c,t,a in accuracy_runs])*100
        print("accuracy: macro average (std) over {0} runs: {1:.2f} ({2:.2f})".format(args.runs,macro,std), file=sys.stderr)

    print("Info: type {5}\n\tin_dim: {0}\n\tc_in_dim: {8}\n\th_dim: {1}"
          "\n\th_layers: {2}\n\tactivation: {4}\n\tsigma: {6}\n\tlower: {7}"
          "\tembeds: {3}".format(args.in_dim,args.h_dim,args.h_layers,args.embeds,args.ac.__name__,args.type, args.sigma, args.lower, args.c_in_dim), file=sys.stderr)

    if args.save_embeds:
        tagger.save_embeds(args.save_embeds)

def load(args):
    """
    load a model from file; specify the .model file, it assumes the *pickle file in the same location
    """
    myparams = pickle.load(open(args.model+".pickle", "rb"))#python3 needs it as bytes
    tagger = NNTagger(myparams["in_dim"], myparams["h_dim"], myparams["c_in_dim"], myparams["h_layers"], nn=myparams["type"], activation=myparams["activation"])
    tagger.predictors, tagger.word_rnn, tagger.wembeds = tagger.build_computation_graph(myparams["num_words"], myparams["num_chars"], myparams["num_tags"])
    tagger.set_indices(myparams["w2i"],myparams["c2i"],myparams["tag2idx"])
    tagger.model.load(str.encode(args.model))
    print("model loaded: {}".format(args.model), file=sys.stderr)
    return tagger

def save(nntagger, args):
    """
    save a model; pycnn only saves the parameters, need to store the rest separately
    """
    outdir = args.save
    modelname = outdir + ".model"
    nntagger.model.save(str.encode(modelname))  #python3 needs it as bytes
    import pickle
    myparams = {"num_words": len(nntagger.w2i),
                "num_chars": len(nntagger.c2i),
                "num_tags": len(nntagger.tag2idx),
                "w2i": nntagger.w2i,
                "c2i": nntagger.c2i,
                "tag2idx": nntagger.tag2idx,
                "type": nntagger.nn,
                "activation": nntagger.activation,
                "in_dim": nntagger.in_dim,
                "h_dim": nntagger.h_dim,
                "c_in_dim": nntagger.c_in_dim,
                "h_layers": nntagger.h_layers,
                "embeds_file": nntagger.embeds_file,
                }
    pickle.dump(myparams, open( modelname+".pickle", "wb" ) )
    print("model stored: {}".format(modelname), file=sys.stderr)


class NNTagger(object):

    def __init__(self,in_dim,h_dim,c_in_dim,h_layers,embeds_file=None,nn="ff", activation=pycnn.tanh, lower=False, noise_sigma=0.1):
        self.w2i = {}  # word to index mapping
        self.c2i = {}  # char to index mapping
        self.tags = set()
        #self.tag2idx = {}  # pick needs numbers, map labels to integers
        self.tag2idx_task1 = {} # need one dictionary per task
        self.tag2idx_task2 = {}
        self.model = pycnn.Model() #init model
        self.in_dim = in_dim
        self.h_dim = h_dim
        self.c_in_dim = c_in_dim
        self.activation = activation
        self.lower = lower
        self.noise_sigma = noise_sigma
        self.h_layers = h_layers
        self.predictors = []  # list of layers
        self.wembeds = None # lookup: embeddings for words
        self.cembeds = None # lookup: embeddings for characters
        self.embeds_file = embeds_file
        self.nn = nn
        #self.char_rnn = None # RNN for character input
        self.word_rnn = None # RNN for word input


    def pick_neg_log(self, pred, gold):
        return -pycnn.log(pycnn.pick(pred, gold))

    def set_indices(self, w2i, c2i, t2i_task1, t2i_task2):
        self.tag2idx_task1 = t2i_task1
        self.tag2idx_task2 = t2i_task2
        self.w2i = w2i
        self.c2i = c2i
        ## instantiate other helper functions used?
        #self.tags = t2i.keys()

    def fit(self, folder_name_task1, folder_name_task2, num_iterations, train_algo, dev=None):
        """
        train the tagger
        """
        print("read training data",file=sys.stderr)
        train_X, train_Y, task_labels, w2i, c2i, t2i_task1, t2i_task2 =self.get_train_data(folder_name_task1, folder_name_task2)
        # store mappings of words and tags to indices
        self.set_indices(w2i,c2i, t2i_task1, t2i_task2)

        #if dev:
        #    print("read dev data",file=sys.stderr)
        #    dev_X, dev_Y, _, _ = self.get_data_as_indices(dev)

        # init lookup parameters and define graph
        print("build graph",file=sys.stderr)
        num_words = len(self.w2i)
        num_tags_task1 = len(self.tag2idx_task1)
        num_tags_task2 = len(self.tag2idx_task2)
        num_chars = len(self.c2i)
        self.predictors, self.word_rnn, self.wembeds = self.build_computation_graph(num_words, num_chars)

        if train_algo == "sgd":
            trainer = pycnn.SimpleSGDTrainer(self.model)
        elif train_algo == "adam":
            trainer = pycnn.AdamTrainer(self.model)

        train_data = list(zip(train_X,train_Y, task_labels))


        for iter in range(num_iterations):
            total_loss=0.0
            total_tagged=0.0
            random.shuffle(train_data)
            for ((sent_indices,char_indices),y, task_of_instance) in train_data:

                pycnn.renew_cg() # new computation graph for this example

                sentence_emb = []
                rev_sentence_emb = []
                for sentence_ids in sent_indices:
                    last_state = self.word_rnn.predict_sequence([self.wembeds[w] for w in sentence_ids])[-1]
                    rev_last_state = self.word_rnn.predict_sequence([self.wembeds[w] for w in reversed(sentence_ids)])[-1]
                    sentence_emb.append(last_state)
                    rev_sentence_emb.append(rev_last_state)

                # use last state of sentence as feature
                #features = [pycnn.noise(fe,self.noise_sigma) for fe in sentence_emb]
                features = [pycnn.concatenate([s,rev_s]) for s,rev_s in zip(sentence_emb,reversed(sentence_emb))]
                features = [pycnn.noise(fe,self.noise_sigma) for fe in features]

                # go through layers
                prev = features
                num_layers = len(self.predictors)
                for i in range(0,num_layers-1):
                    predictor = self.predictors[i]
                    out = predictor.predict_sequence(prev)
                    prev = out

                if task_of_instance == 'task1':
                    output_predictor = self.predictors[-1]['task1']
                    output = output_predictor.predict_sequence(prev)
                else:
                    output_predictor = self.predictors[-1]['task2']
                    output = output_predictor.predict_sequence(prev)

                loss1 = pycnn.esum([self.pick_neg_log(pred,gold) for pred, gold in zip(output, y)])
                lv = loss1.value()
                total_loss += lv
                total_tagged += len(sent_indices)

                loss1.backward()
                trainer.update()

            print("iter {2} {0:>12}: {1:.2f}".format("total loss",total_loss/total_tagged,iter), file=sys.stderr)

    def build_computation_graph(self, num_words, num_chars):
        """
        build graph and link to parameters
        """

         # initialize the word embeddings and the parameters
        if self.embeds_file:
            print("loading embeddings", file=sys.stderr)
            embeddings, emb_dim = load_embeddings_file(self.embeds_file, lower=self.lower)
            assert(emb_dim==self.in_dim)
            num_words=len(set(embeddings.keys()).union(set(self.w2i.keys()))) # initialize all with embeddings
            # init model parameters and initialize them
            wembeds = self.model.add_lookup_parameters("lookup_wembeds", (num_words, self.in_dim))
            #cembeds = self.model.add_lookup_parameters("lookup_cembeds", (num_chars, self.c_in_dim))
            init=0
            l = len(embeddings.keys())
            for word in embeddings.keys():
                # for those words we have already in w2i, update vector, otherwise add to w2i (since we keep data as integers)
                if word in self.w2i:
                    wembeds.init_row(self.w2i[word], embeddings[word])
                else:
                    self.w2i[word]=len(self.w2i.keys()) # add new word
                    wembeds.init_row(self.w2i[word], embeddings[word])
                init+=1
            print("initialized: {}".format(init), file=sys.stderr)

        else:
            wembeds = self.model.add_lookup_parameters("lookup_wembeds", (num_words, self.in_dim))

        #make it more flexible to add number of layers as specified by parameter

        builders = [
                # layers, input_dim, hidden_dim, model
                pycnn.LSTMBuilder(self.h_layers, self.in_dim+self.in_dim, self.h_dim, self.model),
                pycnn.LSTMBuilder(self.h_layers, self.in_dim+self.in_dim, self.h_dim, self.model),
                ]
        layers = []

        for layer_num in range(0,self.h_layers):
            if layer_num == 0:
                #input: in RNNs we first need the recurrent state sequence
                layers.append(BiRNNSequencePredictor(builders)) #returns concat hence 2
                layers.append(FFSequencePredictor(Layer(self.model, self.h_dim*2, self.h_dim, self.activation)))
            elif layer_num == 1:
                output_pred_task1 = FFSequencePredictor(Layer(self.model, self.h_dim, self.num_labels_task1, pycnn.softmax))
                layers.append(FFSequencePredictor(Layer(self.model, self.h_dim, self.h_dim, self.activation)))
            else:
                layers.append(FFSequencePredictor(Layer(self.model, self.h_dim, self.h_dim, self.activation)))
        #output layer
        #layers.append(FFSequencePredictor(Layer(self.model, self.h_dim, num_tags, pycnn.softmax)))
        #print(self.num_labels_task1, self.num_labels_task2)

        output_pred_task2 = FFSequencePredictor(Layer(self.model, self.h_dim, self.num_labels_task2, pycnn.softmax))
        layers.append({'task1': output_pred_task1, 'task2': output_pred_task2})

        word_rnn = RNNSequencePredictor(pycnn.LSTMBuilder(1, self.in_dim, self.in_dim, self.model))
        return layers, word_rnn, wembeds

    def get_features(self, sentences):
        """
        from a list of words, return the word and word char indices
        """
        instance_word_indices=[]
        word_char_indices = [] #not used
        for i, sentence in enumerate(sentences):

            sentence2ids = []
            for word in sentence:
                sentence2ids.append(self.w2i.get(word, self.w2i["_UNK"]))
            instance_word_indices.append(sentence2ids)

        return instance_word_indices, word_char_indices

    def get_data_as_indices(self, folder_name, task):
        """
        X = list of (word_indices, word_char_indices)
        Y = list of tag indices
        """
        X, Y = [],[]
        org_X, org_Y = [], []
        task_labels = []
        for (sentences, tags) in read_discourse_chunk_file(folder_name):
            word_indices, word_char_indices = self.get_features(sentences)
            if task == "task1":
                tag_indices = [self.tag2idx_task1.get(tag) for tag in tags]
            else:
                tag_indices = [self.tag2idx_task2.get(tag) for tag in tags]
            X.append((word_indices,word_char_indices))
            Y.append(tag_indices)
            org_X.append(sentences)
            org_Y.append(tags)
            task_labels.append( task )
        return X, Y, org_X, org_Y, task_labels


    def predict(self, sent_indices, word_char_indices, task_of_instance):
        """
        predict tags for a sequence of words
        """

        pycnn.renew_cg()


        sentence_emb = []
        rev_sentence_emb = []
        for sentence_ids in sent_indices:
            last_state = self.word_rnn.predict_sequence([self.wembeds[w] for w in sentence_ids])[-1]
            rev_last_state = self.word_rnn.predict_sequence([self.wembeds[w] for w in reversed(sentence_ids)])[-1]
            sentence_emb.append(last_state)
            rev_sentence_emb.append(rev_last_state)

# use last state of sentence as feature

        features = [pycnn.concatenate([s,rev_s]) for s,rev_s in zip(sentence_emb,reversed(sentence_emb))]
        features = [pycnn.noise(fe,self.noise_sigma) for fe in features]

        # go through layers
        prev = features
#         for pred in self.predictors:
#              out = pred.predict_sequence(prev)
#              prev = out


        num_layers = len(self.predictors)
        for i in range(0,num_layers-1):
            predictor = self.predictors[i]
            out = predictor.predict_sequence(prev)
            prev = out

        if task_of_instance == 'task1':
            output_predictor = self.predictors[-1]['task1']
            output = output_predictor.predict_sequence(prev)
        else:
            output_predictor = self.predictors[-1]['task2']
            output = output_predictor.predict_sequence(prev)



        return [np.argmax(o.value()) for o in output]


    def evaluate(self, test_X, test_Y, org_X, org_Y, task_labels, output_predictions=False, verbose=True):
        """
        compute accuracy on a test file
        """
        correct = 0
        total = 0.0

        if output_predictions:
            i2w = {self.w2i[w] : w for w in self.w2i.keys()}
            #i2t = {self.tag2idx[t] : t for t in self.tag2idx.keys()}
            # TODO cleaner
            if task_labels[0] == "task1":
                i2t = {self.tag2idx_task1[t] : t for t in self.tag2idx_task1.keys()}
            else:
                i2t = {self.tag2idx_task2[t] : t for t in self.tag2idx_task2.keys()}


        for i, ((word_indices, word_char_indices), gold_tag_indices, task_of_instance) in enumerate(zip(test_X, test_Y, task_labels)):
            if verbose:
                if i%100==0:
                    sys.stderr.write('%s'%i)
                elif i%10==0:
                    sys.stderr.write('.')

            predicted_tag_indices = self.predict(word_indices, word_char_indices, task_of_instance)
            if output_predictions:
                prediction = [i2t[idx] for idx in predicted_tag_indices]

                words = org_X[i]
                gold = org_Y[i]

                for w,g,p in zip(words,gold,prediction):
                    print(("{}\t{}\t{}".format(" ".join(w),g,p)))
                print("")
            correct += sum([1 for (predicted, gold) in zip(predicted_tag_indices, gold_tag_indices) if predicted == gold])
            total += len(gold_tag_indices)
        if verbose:
            print("\nTest accuracy on %s items: %.4f" % (i+1, correct/total), file=sys.stderr)
        return correct, total

    def get_train_data(self, folder_name_task1, folder_name_task2):
        """

        :param file_name: input file name
        :param lower: whether to lowercase tokens

        transform training data to features (word indices)
        map tags to integers
        """
        X = []
        Y = []
        task_labels = [] #keeps track of where instances come from "task1" or "task2"

        num_sentences=0
        num_tokens=0

        # word 2 indices and tag 2 indices
        w2i = {} # word to index
        c2i = {} # char to index
        tag2idx_task1 = {}
        tag2idx_task2 = {}

        w2i["_UNK"] = 0  # unk word / OOV


        for instance_idx, (sentences, tags) in enumerate(read_discourse_chunk_file(folder_name_task1)):

            num_sentences += 1
            instance_word_indices = [] #sequence of word indices
            instance_char_indices = [] #sequence of char indices # for now not used
            instance_tags_indices = [] #sequence of tag indices

            for i, (sentence, tag) in enumerate(zip(sentences, tags)):
                num_tokens += 1

                sentence2ids = []
                for word in sentence:
                    # map words and tags to indices
                    if word not in w2i:
                        w2i[word] = len(w2i)
                    sentence2ids.append(w2i[word])
                instance_word_indices.append(sentence2ids)

                if tag not in tag2idx_task1:
                    #tag2idx[tag]=len(tag2idx)
                    tag2idx_task1[tag]=len(tag2idx_task1)

                instance_tags_indices.append(tag2idx_task1.get(tag))

            X.append((instance_word_indices, instance_char_indices)) # list of word indices, for every word list of char indices
            Y.append(instance_tags_indices)
            task_labels.append('task1')

        self.num_labels_task1 = len(tag2idx_task1)

        #labels_task2 = set()

        for instance_idx, (sentences, tags) in enumerate(read_discourse_chunk_file(folder_name_task2)):

            num_sentences += 1
            instance_word_indices = [] #sequence of word indices
            instance_char_indices = [] #sequence of char indices # for now not used
            instance_tags_indices = [] #sequence of tag indices

            for i, (sentence, tag) in enumerate(zip(sentences, tags)):
                num_tokens += 1
                #labels_task2.add(tag)

                sentence2ids = []
                for word in sentence:
                    # map words and tags to indices
                    if word not in w2i:
                        w2i[word] = len(w2i)
                    sentence2ids.append(w2i[word])
                instance_word_indices.append(sentence2ids)

                if tag not in tag2idx_task2:
                    tag2idx_task2[tag]=len(tag2idx_task2)

                instance_tags_indices.append(tag2idx_task2.get(tag))

            X.append((instance_word_indices, instance_char_indices)) # list of word indices, for every word list of char indices
            Y.append(instance_tags_indices)
            task_labels.append('task2')

        self.num_labels_task2 = len(tag2idx_task2)

        print("%s sentences %s tokens" % (num_sentences, num_tokens), file=sys.stderr)
        print("%s w features, %s c features " % (len(w2i),len(c2i)), file=sys.stderr)

        assert(len(X)==len(Y))
        return X, Y, task_labels, w2i, c2i, tag2idx_task1, tag2idx_task2  #sequence of features, sequence of labels, necessary mappings

    def save_embeds(self, out_filename):
        # construct reverse mapping
        i2w = {self.w2i[w]: w for w in self.w2i.keys()}

        OUT = open(out_filename+".w.emb","w")
        for word_id in i2w.keys():
            wembeds_expression = self.wembeds[word_id]
            word = i2w[word_id]
            OUT.write("{} {}\n".format(word," ".join([str(x) for x in wembeds_expression.npvalue()])))
        OUT.close()


class MyNNTaggerArgumentOptions(object):
    def __init__(self):
        pass
    ### functions for checking arguments
    def acfunct(arg):
        """ check for allowed argument for --ac option """
        try:
            functions = [pycnn.rectify, pycnn.tanh]
            functions = { function.__name__ : function for function in functions}
            return functions[str(arg)]
        except:
            raise argparse.ArgumentTypeError("String {} does not match required format".format(arg,))

    def nntype(arg):
        """ check for allowed argument for --nn option """
        nntypes = ["rnn", "lstm", "ff", "bilstm"]
        if arg in nntypes:
            return arg
        else:
            raise argparse.ArgumentTypeError("String {} does not match required format".format(arg,))


if __name__=="__main__":
    main()
