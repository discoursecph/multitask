Soichiro Honda 's picture now hangs with Henry Ford 's in the U.S. Automotive Hall of Fame , and the game-show " Jeopardy " is soon to be Sony-owned .	B-comparison	9..153
But no matter how much Japan gets under our skin , we 'll still have mom and apple pie .	I-comparison	154..239
On second thought , make that just mom .	O	242..280
A Japanese apple called the Fuji is cropping up in orchards the way Hondas did on U.S. roads .	B-expansion	283..376
By 1995 it will be planted more often than any other apple tree , according to a recent survey of six apple-industry sages by Washington State University horticulturist Robert Norton .	I-expansion	377..559
Some fruit visionaries say the Fuji could someday tumble the Red Delicious from the top of America 's apple heap .	O	560..672
It certainly wo n't get there on looks .	B-contingency	675..713
Compared to the Red Delicious , the exemplar of apple pulchritude , the Fuji is decidedly more dowdy -- generally smaller , less-perfectly shaped , greenish , with tinges of red .	B-expansion	714..887
To hear most U.S. growers tell it , we 'd still be in Paradise if the serpent had proffered one to Eve .	I-expansion	888..989
But how sweet it is .	O	992..1012
It has more sugar " than any apple we 've ever tested , " says Duane Greene , a University of Massachusetts pomologist , or apple scholar .	O	1013..1145
It has a long shelf life and " does n't fool the public , " says Grady Auvil , an Orondo , Wash. , grower who is planting Fujis and spreading the good word about them .	O	1146..1306
" It does n't look nice on the outside while getting mealy inside . "	O	1307..1372
Mr. Auvil , razor sharp at 83 , has picked and packed a zillion pecks of apples over the past 65 years .	B-expansion	1375..1476
He is known as the father of the U.S.-grown Granny Smith , a radically different apple that the conventional wisdom once said would never catch on .	I-expansion	1477..1623
It did , shaking the apple establishment to its roots .	B-comparison	1624..1677
Now , even more radical changes seem afoot as the grand old maverick of American apples plays the role of Hiroshi Appleseed .	I-comparison	1678..1801
" The Fuji is going to be No. 1 to replace the Red Delicious , " he says .	O	1802..1872
The Delicious hegemony wo n't end anytime soon .	B-contingency	1875..1921
New apple trees grow slowly , and the Red Delicious is almost as entrenched as mom .	I-contingency	1922..2004
Its roots are patriotic -- with the first trees appearing in 1872 in an orchard near Peru , Iowa , to be exact .	B-expansion	2005..2114
For more than 50 years , it has been the apple of our eye .	B-expansion	2115..2172
A good Delicious can indeed be delicious .	B-expansion	2173..2214
More than twice as many Red Delicious apples are grown as the Golden variety , America 's No. 2 apple .	I-expansion	2215..2315
But the apple industry is ripe for change .	O	2318..2360
" Red Delicious has been overplanted , and its prices have dropped below the cost of production , " says Washington State 's Mr. Norton .	O	2361..2492
The scare over Alar , a growth regulator that makes apples redder and crunchier but may be carcinogenic , made consumers shy away from the Delicious , though they were less affected than the McIntosh .	O	2493..2690
The glut and consequent lower prices , combined with cancer fears , was a very serious blow to growers .	O	2691..2792
" A lot of growers wo n't be around in a few years , " says Mr. Norton , although they have stopped using Alar .	O	2793..2899
One may be William Broderick , a Sterling , Mass. , grower .	O	2902..2958
" This is beautiful stuff , " he says , looking ruefully at big boxes of just-picked Red Delicious next to his barn .	O	2959..3071
" But I 'm going to lose $ 50,000 to $ 60,000 on it .	B-contingency	3072..3120
I 'm going to have to get another job this year just to eat . "	I-contingency	3121..3181
Besides rotten prices , he has been hit recently by hail , a bark-nibbling horde of mice , fungi and bugs .	B-entrel	3184..3287
Some 500 insects and 150 diseases wiggle , chew and romp through growers ' nightmares , including maggots , mites , mildew , thrips , black rot and the flat-headed borer .	I-entrel	3288..3451
Even if a grower beats them back , his $ 2,000 rented bees might buzz off to the neighbors ' orchards instead of pollinating his , Mr. Broderick says .	O	3452..3598
Though growers ca n't always keep the worm from the apple , they can protect themselves against the price vagaries of any one variety by diversifying -- into the recently imported Gala , a sweet New Zealand native ; the Esopus Spitzenburg , reportedly Thomas Jefferson 's favorite apple ; disease-resistant kinds like the Liberty .	O	3601..3924
" I 've ripped out a lot of Delicious " and grafted the trees with many different shoots , says Steve Wood , a West Lebanon , N.H. , grower , tramping through his 100-acre Poverty Lane Orchard on a crisp autumn day recently .	O	3925..4141
" I 've got 70 kinds of apples .	O	4142..4171
Here 's a Waltana , " he exclaims , picking one off a tree .	B-temporal	4172..4227
He bites it , scowls and throws it down . "	I-temporal	4228..4269
It 's a real dog . "	O	4269..4286
Supermarkets are getting into the variety act , too .	O	4289..4340
They still buy apples mainly for big , red good looks -- that 's why so many taste like woodchucks ' punching bags .	O	4341..4453
But freshness counts more than it once did , and stores are expanding shelf space for unconventional , but tastier , and often pricier , apples .	O	4454..4594
" Rather than sell 39-cents-a-pound Delicious , maybe we can sell 79-cents-a-pound Fujis , " says Chuck Tryon , perishables director for Super Valu Inc. , a Minneapolis supermarket chain and food distributor .	O	4595..4797
The Fuji is a product of meticulous Japanese pomological engineering , which fostered it 50 years ago at a government research orchard .	O	4800..4934
Japanese researchers have bred dozens of strains of Fujis to hone its color , taste and shelf life .	B-contingency	4935..5033
Now the best of them age as gracefully as Grannies , the industry 's gold standard for storability .	B-entrel	5034..5131
In the cornucopia of go-go apples , the Fuji 's track record stands out : During the past 15 years , it has gone from almost zilch to some 50 % of Japan 's market .	I-entrel	5132..5289
" The Japanese apple market is very keyed to high quality , " says David Lane , a scientist at a Canadian horticulture research center in Summerland , British Columbia , and so apples are more of a delicacy there than a big food commodity .	O	5292..5525
The U.S. Department of Agriculture estimates that this year Americans will eat about 40 % more fresh apples per capita than the Japanese .	O	5526..5662
The Fuji is still small potatoes in the U.S. , sold mainly in fruit boutiques .	O	5665..5742
But in California , says Craig Ito , a Fuji-apple grower , " There 's a Fuji apple cult .	O	5743..5826
Once somebody eats one , they get hooked . "	O	5827..5868
Mr. Auvil , the Washington grower , says that he could sell Fujis to Taiwan buyers at $ 40 a box if he had them .	O	5871..5980
( Taiwan already is a big importer of Fujis from other places , he adds . )	O	5981..6052
But his first crop wo n't be picked till next year .	O	6053..6103
" I expect to see the demand exceed supply for Fujis for the next 10 to 15 years , " he adds .	O	6104..6194
Washington Red Delicious , by the way , are wholesaling for less than $ 10 a box these days .	O	6195..6284
Mr. Auvil sees Fujis , in part , as striking a blow against the perversion of U.S. apples by supermarkets .	O	6287..6391
" When the chain stores took over , there was no longer a connection between grower and consumer .	B-comparison	6392..6487
A guy is sitting up in an office deciding what you 're going to eat . "	I-comparison	6488..6556
After all , until the 1950s even the Red Delicious was a firm , delectable morsel .	B-temporal	6559..6639
Then , as growers bred them more for looks , and to satisfy supermarket chains ' demands of long-term storage , the Red went into decline .	B-contingency	6640..6774
Now , those red applelike things stores sell in summer are fruitbowl lovely , but usually not good eating .	I-contingency	6775..6879
They do deserve respect , however -- they are almost a year old , probably equal to about 106 in human years .	O	6880..6987
The Fuji , to be sure , has blemishes too .	O	6990..7030
It ripens later than most apples , and growing it in U.S. areas with chilly autumns may be tricky .	B-expansion	7031..7128
Moreover , the frumpy Fuji must compete with an increasingly dolledup Delicious .	I-expansion	7129..7208
Mr. Broderick , the Massachusetts grower , says the " big boss " at a supermarket chain even rejected his Red Delicious recently because they were n't waxed and brushed for extra shine .	O	7209..7389
And he had n't used hormones , which many growers employ to elongate their Delicious apples for greater eye appeal .	O	7390..7503
Still , Mr. Auvil points out , Grannies became popular without big , red looks , so why not Fujis ?	B-expansion	7504..7598
He sees a shift in American values -- at least regarding apples -- toward more emphasis on substance and less on glitz .	I-expansion	7599..7718
" Taste has finally come to the fore , " he says .	O	7719..7765
Or , for that matter , the core .	O	7766..7796
