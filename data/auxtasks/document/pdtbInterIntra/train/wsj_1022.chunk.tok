After coming close to a partial settlement a year ago , shareholders who filed civil suits against Ivan F. Boesky and the partnerships he once controlled again are approaching an accord , people familiar with the case said .	root	9..230
Meanwhile , within the next few weeks , the limited partners in Ivan F. Boesky & Co . L.P. are expected to reach a partial settlement with Drexel Burnham Lambert Inc. regarding the distribution of the $ 330 million in partnership assets , said one of the individuals .	conjunction	233..495
Under the terms of the settlement , the limited partners would drop their civil suits against Drexel , now pending in federal court in New York , another individual said .	restatement	496..663
Attorneys involved in the talks said that the parties were closer to accord than they were a year ago , when reports of an imminent agreement circulated .	norel	666..818
One individual said the shareholders ' accord was " well worked out . "	conjunction	819..886
However , less optimistic attorneys warned that because of the tangle of numerous defendants and plaintiffs with overlapping claims , there is always the possibility that the talks will again fall apart .	norel	889..1090
A shareholders ' accord would provide the first restitution to thousands of individuals and institutions claiming losses as a result of insider trading by Boesky & Co. , once the largest arbitrage fund in the U.S.	norel	1093..1304
The plaintiffs are investors who bought and sold securities in which Mr. Boesky and his partnerships were dealing .	entrel	1305..1419
Some claim they suffered losses because they sold while he was buying	restatement	1420..1489
and others because they bought while he was selling .	conjunction	1490..1542
Stocks involved in the shareholder suits include Union Carbide , RJR Nabisco , American Natural Resources , Boise Cascade Corp. , General Foods Corp. , Houston Natural Gas and FMC Corp .	norel	1545..1725
There are at least 27 class-action shareholder suits that have been consolidated in federal court in New York under U.S. District Judge Milton Pollack .	norel	1728..1879
Among the defendants are Mr. Boesky ; the now-defunct Ivan F. Boesky & Co. ; Mr. Boesky 's main underwriter , Drexel Burnham ; and Cambrian & General Securities PLC , a British investment fund once controlled by Mr. Boesky .	entrel	1880..2097
Individuals familiar with the negotiations said the partial settlement being negotiated would remove the Boesky partnership , the British fund and Mr. Boesky as defendants , while Drexel and other defendants would remain .	norel	2100..2319
Charles Davidow , of the Washington , D.C.-based law firm Wilmer , Cutler & Pickering , which represents Mr. Boesky in this matter , said only that " discussions are under way .	norel	2322..2492
There are no agreements yet . "	contrast	2493..2522
It has been three years	norel	2525..2548
since Mr. Boesky , now in prison , agreed to pay a $ 100 million fine to settle the government 's charges that he had traded illegally using insider information .	asynchronous	2549..2706
Out of this , the government set up a $ 50 million fund for plaintiffs who can prove their financial losses .	entrel	2707..2813
According to William Orbe , an attorney at Grais & Richards , the escrow agents for the fund , as of Sept. 30 the fund amounted to $ 60.5 million .	entrel	2814..2956
Separately , attorneys for the 42 or so limited partners have had serious discussions that could lead to the distribution of the partnership 's assets .	norel	2959..3108
The limited partners include insurance companies , financial institutions and individual investors .	entrel	3109..3207
An agreement with Drexel regarding the limited partners ' investments is an essential step toward getting their money back .	norel	3210..3332
This is	entrel	3333..3340
because a Delaware court earlier this year said that Drexel is entitled to get its money back before or at the same time as the limited partners .	cause	3341..3486
Drexel is owed $ 20 million by the partnership .	entrel	3487..3533
An individual familiar with the negotiations said that whatever investments the limited partners do not recoup from the $ 330 million in partnership assets , they will receive from the $ 350 million restitution fund available as a result of Drexel 's settlement with the government in December 1988 .	norel	3536..3831
Drexel agreed to plead guilty to six felony counts and pay $ 650 million , of which $ 350 million was set aside for shareholders and other plaintiffs , including the limited partners , who claim they were injured by Drexel .	norel	3834..4052
JAILED AFRICAN-AMERICAN activist wins a battle against the	norel	4055..4113
U.S. District Judge Robert P. Patterson Jr. ordered the FBI to immediately begin processing Herman Benjamin Ferguson 's request for documents stemming from the agency 's investigation of him during the 1960s .	norel	4116..4322
The FBI had said it would not be able to begin processing the request until June	contrast	4323..4403
Mr. Ferguson , who is 68 years old , fled the U.S. in 1970	norel	4406..4462
after exhausting his appeals of a 1968 conviction on conspiracy to murder .	asynchronous	4463..4537
He turned himself in to authorities in New York earlier this year .	contrast	4538..4604
He maintains that the information from the FBI will help him get his 1968 conviction vacated and his bail-jumping indictment dismissed .	conjunction	4605..4740
His attorneys claim he was framed by the FBI and New York police as part of a campaign to destroy the black liberation movement of the 1960s .	restatement	4741..4882
Because the federal Freedom of Information Act was n't law at that time , the FBI was n't required to turn over information on its investigations when Mr. Ferguson appealed his conviction in the 1960s .	norel	4885..5083
But in federal court in Manhattan , Judge Patterson said the FBI records could show that Mr. Ferguson 's arrest was the result of questionable legal practices .	contrast	5084..5241
The judge said that if the FBI 's proposed schedule was followed in releasing the documents , " a delay of over one year will have occurred and plaintiff will have served approximately two-thirds of his 3 1/2-year minimum sentence by the time he receives the files . "	norel	5244..5507
Gabriel W. Gorenstein , the assistant U.S. attorney handling the case for the FBI , said no decision has been made about appealing the judge 's ruling .	norel	5510..5658
FEDERAL COURTS URGED to cut costs and reduce delays of civil suits .	norel	5661..5728
The study , conducted by a task force of the Brookings Institution , suggests that Congress should require the courts to develop the plans .	norel	5731..5868
The study was initiated by Senate Judiciary Committee chairman Joseph Biden ( D. , Del . ) .	entrel	5869..5956
The Washington , D.C. , think tank recommends that the courts adopt different " tracks " for different types of civil cases in order to separate the handling of highly complex suits from simpler ones .	norel	5959..6155
Complex cases , such as antitrust suits and many business disputes , would receive intense supervision by federal judges to keep pretrial proceedings moving .	cause	6156..6311
Standard cases would require less judicial attention	contrast	6312..6364
and fast-track cases could be resolved quickly .	conjunction	6365..6413
The study also said each federal court should set strict time limits for the pretrial exchange of documents and evidence , ranging from as much as 100 days for cases in the fast track to as much as 18 months for complex disputes .	norel	6416..6644
And the study said federal courts should set firm trial dates early in the process .	conjunction	6645..6728
To take advantage of local expertise and custom , the study said , Congress should require each of the 94 federal district courts to adopt its own plan to speed the handling of civil suits and to reduce the high costs in civil cases .	norel	6731..6962
Although some of the study 's recommendations resemble those of similar projects , the makeup of the task force was unusually diverse , adding significance to the effort .	norel	6965..7132
It included lawyers from civil rights and consumer groups , plaintiffs ' lawyers and defense attorneys , corporate counsel and law professors .	restatement	7133..7272
