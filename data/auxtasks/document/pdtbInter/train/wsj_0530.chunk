Since its founding in 1818, Brooks Brothers, the standard-bearer of the Ivy League look, has eschewed flashy sales promotions and fashion trends -- the rules that most retailers live by.	B-concession	9..195
But with sales growth sluggish and other men's stores putting on the heat, the venerable retailer can no longer afford such a smug attitude.	B-cause	196..336
So two weeks ago, thousands of Brooks Brothers charge customers -- customers conditioned to wait for twice-yearly clearance sales -- got a surprise: an invitation to come in and buy any one item for 25% off.	B-cause	339..546
During the four-day promotion, shoppers at the Short Hills, N.J., store lined up to pay for big-ticket items like coats and suits.	I-cause	547..677
That's not all.	B-instantiation	680..695
Departing from its newspaper ads featuring prim sketches of a suit or a coat, Brooks Brothers is marketing an updated image in a new campaign that carries the slogan, "The Surprise of Brooks Brothers.	B-restatement	696..897
One color photo displays a rainbow of dress shirts tied in a knot; another picture shows neckties with bold designs.	B-cause	898..1014
The message is loud and clear: This is not your father's Brooks Brothers.	I-cause	1015..1088
As part of its national ad pitch, Brooks Brothers will show less preppy women's clothes, moving away from its floppy-tie business stereotype.	B-instantiation	1091..1232
One ad shows a bright red jacket paired with a black leather skirt.	B-conjunction	1233..1300
And the ad copy is cheeky: "How can you be a Wall Street hot shot without at least one Brooks Brothers suit in your portfolio?"	I-conjunction	1301..1428
Brooks Brothers hopes that shaking its time-honored traditions will attract more young men and more women and change consumer perceptions about its range of merchandise.	O	1431..1600
"We have men who only buy their shirts and underwear here or younger customers who only buy their {job} interview suit here," says William Roberti, chairman and chief executive officer of Brooks Brothers. "	O	1601..1807
We want them to buy more of their wardrobe here.	B-entrel	1807..1856
Industry watchers agree that Brooks Brothers is long overdue in updating its buttoned-down image, which has stunted its growth.	B-entrel	1857..1984
When acquired in May 1988 by British retailer Marks & Spencer PLC, Brooks Brothers' annual operating profit was about $41.8 million on sales of $290.1 million.	I-entrel	1985..2144
Mr. Roberti concedes that since the $750 million takeover, "sales growth hasn't been dramatic."	O	2145..2240
For the 11 months ended March 31, operating profit at the 52-store chain totaled $40.5 million on sales of $286.8 million.	O	2241..2363
As Brooks Brothers jumps into the fashion fray, it will be playing catch up.	B-cause	2366..2442
Many clothiers, especially Ralph Lauren, have cashed in on the recent popularity of updated Ivy League and English styles.	B-conjunction	2443..2565
Many clothiers, especially Ralph Lauren, have cashed in on the recent popularity of updated Ivy League and English styles.In keeping with men's broader fashion scope today, businessmen are dabbling in English and Italian suits that are conservative but not stodgy.	B-contrast	2566..2708
Many clothiers, especially Ralph Lauren, have cashed in on the recent popularity of updated Ivy League and English styles.In keeping with men's broader fashion scope today, businessmen are dabbling in English and Italian suits that are conservative but not stodgy.The rigid Ivy League customer, Brooks Brothers' bread and butter, meanwhile is becoming extinct.	B-cause	2709..2805
Thus, Brooks Brothers has lost customers to stores that offer more variety such as Paul Stuart, Barneys New York and Louis, Boston.	I-cause	2806..2937
"Brooks Brothers no longer has a lock on the {Ivy League} customer who is status-conscious about his clothes," says Charlie Davidson, president of the Andover Shop, a traditional men's store in Cambridge, Mass.	O	2940..3150
By making a break from tradition, Brooks Brothers is seeking a delicate balance.	O	3153..3233
By making a break from tradition, Brooks Brothers is seeking a delicate balance.If it promotes fashion too much, the shop risks alienating its old-line customers; by emphasizing "value," it risks watering down its high-minded mystique.	B-conjunction	3234..3389
Fashion industry consultants also question whether the company can make significant strides in its women's business, given that its customer base is less established and that conservative business dress for women is on the decline.	B-contrast	3392..3623
Brooks Brothers' aim is for 20% of total sales to come from the women's department, up from the current 12%.	I-contrast	3624..3732
"Everybody forgets that there are fashion cycles in classic merchandise," observes Carol Farmer, a retail consultant.	O	3735..3852
"For women, dressing for success in a real structured way is over."	O	3853..3920
Despite these challenges, Marks & Spencer sees big potential in Brooks Brothers, noting the widely recognized name and global presence.	B-cause	3923..4058
Marks & Spencer plans to open roughly 18 more U.S. stores in the next five years.	B-conjunction	4059..4140
Brooks Brothers says business is robust at its 30 outlets in Japan and two shops in Hong Kong.	I-conjunction	4141..4235
Marks & Spencer is also considering opening stores across Europe sometime in the future.	O	4236..4324
Alan Smith, president of Marks & Spencer North America and Far East, says that Brooks Brothers' focus is to boost sales by broadening its merchandise assortment while keeping its "traditional emphasis."	O	4327..4529
The British parent is also streamlining: Brooks Brothers, which continues to make almost all of its merchandise, recently shut one of its two shirt plants in Paterson, N.J., and has closed boys' departments in all but 20 stores.	O	4530..4758
Brooks Brothers is also remodeling its stores.	B-instantiation	4761..4807
Wednesday, it will unveil a $7 million refurbishing at its flagship store on Madison Avenue.	B-instantiation	4808..4900
With newly installed escalators, the store retains its signature wood-and-brass look but is less intimidating.	I-instantiation	4901..5011
More shirts and sweaters will be laid out on tables, instead of sitting behind glass cases, so that customers can "walk up and touch them," Mr. Roberti says.	O	5012..5169
Because the biggest growth in menswear is in casual sportswear, Brooks Brothers is chasing more of that business.	B-cause	5172..5285
The entire second floor of its Madison Avenue store is now casual sportswear featuring items such as ski sweaters, leather backpacks and a $42 wool baseball hat with the store's crest.	I-cause	5286..5470
The centerpiece of the overhaul, according to Mr. Roberti, is the men's tailored clothing department, where Brooks Brothers has added new suit styles and fabrics.	O	5473..5635
"The perception out there is that we are very conservative and we only sell one type of suit," Mr. Roberti says, referring to Brooks Brothers' signature three-button "sack suit," with a center-vented jacket and boxy fit.	O	5636..5856
But it now offers more two-button versions and suits with a tapered fit.	B-conjunction	5859..5931
It also plans to add suits cut for athletic men with broader upper bodies.	I-conjunction	5932..6006
Next spring, nearly 30% of its suits will have pleated pants, compared with virtually none a couple of years ago.	O	6007..6120
Says Mr. Roberti: "We want to turn the customer on.	O	6123..6174
