Japanese investors, reassured by Monday's strong rally on Wall Street, erased most of that day's losses on the Tokyo Stock Exchange.	O	9..141
But analysts said the rebound didn't remove the cautious mood from the market.	O	142..220
In London, stocks closed lower in volatile trading as an opening rally was obliterated by worse-than-expected U.S. trade figures.	O	223..352
Paris shares had a similar reaction, but most other European bourses posted gains, as did all major Asian and Pacific stock markets.	O	353..485
Tokyo's Nikkei Index of 225 stocks jumped 527.39 points to close at 34996.08.	B-conjunction	488..565
The rise came a day after the year's biggest drop on Monday, when the Nikkei fell 647.33, or 1.8%, in response to Friday's 6.9% plunge on Wall Street.	I-conjunction	566..716
In early trading Wednesday in Tokyo, the Nikkei index rose 19.30 points to 35015.38.	O	719..803
On Tuesday, the broader-based Tokyo Stock Price Index of issues listed in the first section, which fell 45.66 Monday, rose 41.76, or 1.61%, to 2642.64.	B-entrel	806..957
Trading was relatively thin at an estimated 650 million shares, though brisker than Monday's 526 million.	B-entrel	958..1063
Advancing issues outnumbered decliners 821-201, with 103 unchanged.	I-entrel	1064..1131
"We're back to square one," said Simon Smithson, an analyst in Japan for Kleinwort Benson International Inc.	O	1134..1242
Japanese domestic institutions, including trust banks and investment management firms, that had been on the sidelines during Monday's fall were back in the market, analysts said.	O	1245..1423
Foreign investors reportedly started off selling but later joined in the buying.	O	1424..1504
The Tokyo rally seemed to confirm the view, frequently expressed in Japan in the past few days, that the drop in New York was a local problem related to merger and acquisition activity in the U.S.	O	1507..1703
"This time we don't really have to worry about Tokyo," said an official at Daiwa Securities Co.	O	1706..1801
"Nothing has changed fundamentally in the Tokyo market."	O	1802..1858
But even though Tokyo appears unharmed by recent market volatility, analysts and traders say there are still a few concerns on the horizon.	O	1861..2000
In particular, Japanese investors will be keeping a wary eye on Wall Street to see whether Monday's 88.12-point rally holds up as fresh U.S. economic data are released.	O	2001..2169
"People are placing small bets.	O	2172..2203
There's no huge buying," said Stephen Hill, head of equity sales at Jardine Fleming Securities Ltd. in Tokyo.	O	2204..2313
"Really brave views right now would be foolhardy."	O	2314..2364
Yesterday's buyers favored real estate, construction and other large-capitalization issues, reflecting the fact that many Tokyo investors now feel safer with domestically oriented stocks, analysts said.	O	2367..2569
They also are concerned about the persistent strength of the dollar against the yen, as a weaker yen leads to higher import prices in Japan and adds to domestic inflationary pressures.	B-conjunction	2572..2756
Currency concerns also weigh heavily on interest rate-sensitive stocks such as banking and other financial issues because of fears that Japanese interest rates might have to rise to keep the dollar in check.	I-conjunction	2759..2966
Among steel shares, NKK rose 19 to 705 yen ($4.97) a share, and Nippon Steel gained 17 to 735.	B-conjunction	2969..3063
Construction shares that gained included Shimizu, which rose 130 to 2,080.	B-conjunction	3064..3138
In the real estate sector, Mitsui Real Estate Development was up 100 at 2,760, and Mitsubishi Estate gained 80 to 2,360.	I-conjunction	3139..3259
London's Financial Times-Stock Exchange 100-share index fell 27.9 points to 2135.5.	O	3262..3345
It was down more than 40 points a half-hour before the close, marking a 61.5-point turnaround from its high, reached in the first 15 minutes of trading.	B-conjunction	3346..3498
The narrower Financial Times 30-share index fell 29.6 to 1730.7.	B-entrel	3499..3563
Volume was an active 643.3 million shares, about double the recent levels but down from 959.3 million the previous day, which U.K. traders have dubbed "Manic Monday."	I-entrel	3564..3730
Prices opened strongly on the basis of Monday's Wall Street rally and yesterday's gains in Tokyo.	B-contrast	3733..3830
But the advance faltered as index-options traders and investors jittery about the U.K. economic outlook took over.	B-asynchronous	3831..3945
The unexpectedly wide U.S. August trade deficit of $10.77 billion hit an already jittery U.K. market in midafternoon.	I-asynchronous	3946..4063
Michael Hicks, who manages sales and trading for brokerage concern Societe Generale Strauss Turnbull, said: "It's a nervous market.	O	4066..4197
It was all over the place.	B-cause	4198..4224
If you bought, you wish you hadn't, and if you sold, you wish you hadn't."	I-cause	4225..4299
He said the current market "is all about sentiment, and the sentiment in London is 90% anxiety and worry."	O	4300..4406
Britain's economic fundamentals, he said, "don't look very bright."	O	4407..4474
Dealers said London showed signs of calming in midafternoon after Wall Street avoided sharp losses despite the trade report, but a wave of futures-related selling later in the session sent buyers back to the sidelines.	O	4477..4695
Still, some sectors found buying interest after being actively sold in recent weeks.	O	4696..4780
Merchant banks were stronger across the board.	O	4783..4829
Morgan Grenfell, which has been mentioned in takeover rumors, rose 20 to 392 pence ($6.18) a share.	O	4830..4929
S.G. Warburg, a rumored target of some European banking concerns, finished 22 higher at 400.	O	4930..5022
Hambros rose 5 to 204, and Schroders rose 25 to #12.75.	O	5023..5078
On the corporate front, Ford Motor announced that it raised its stake in U.K. luxury car maker Jaguar to 10.4% from 5%.	B-asynchronous	5081..5200
Jaguar shares jumped 23 before easing to close at 654, up 6.	I-asynchronous	5201..5261
Amstrad, a British computer hardware and communications equipment maker, eased 4 to 47.	B-asynchronous	5264..5351
It announced a 52% plunge in pretax profit for the latest year.	I-asynchronous	5352..5415
Brewery stocks were firm to higher on talk of early bargain-hunting, but most ended below their peaks.	B-instantiation	5418..5520
Bass ended up 3 higher at 966, Guinness closed at 589, down 7, and Scottish & Newcastle dropped 11 to 359, but Whitbread Class A shares rose 17 to 363.	I-instantiation	5521..5672
Dealers said there was late talk of a Whitbread sale of brewing operations to Scottish & Newcastle.	O	5673..5772
The most active shares were major blue-chips, particularly oils and utilities such as British Gas and British Telecommunications.	O	5775..5904
Traders attributed the action in them largely to defensive positioning in a volatile market.	B-entrel	5905..5997
British Gas finished at 197, down 2, on 13 million shares, British Petroleum fell 8 to 291 on 9.4 million shares, and British Telecom was 4 lower at 261 on turnover of 10 million shares.	B-conjunction	5998..6184
Cable & Wireless fell 20 to 478.	I-conjunction	6185..6217
Also in active trading, British Steel fell 1 to 124 as 20 million shares changed hands.	B-conjunction	6220..6307
Racal Electric, which traded 11 million shares, declined 12 to 218.	I-conjunction	6308..6375
In other European markets, share prices closed sharply higher in Frankfurt and Zurich and posted moderate rises in Stockholm, Amsterdam and Milan.	O	6378..6524
Paris closed lower, and most Brussels shares were unable to trade for a second consecutive day because of technical problems.	B-entrel	6525..6650
South African gold stocks closed higher.	I-entrel	6651..6691
Elsewhere, share prices rebounded in Hong Kong, Sydney, Singapore, Wellington, Taipei, Manila and Seoul.	O	6694..6798
In Hong Kong, Sydney and Singapore -- the largest of those exchanges -- stocks recovered one-third to one-half of the ground they lost in Monday's plunge, with major market indexes posting gains of 3.6% to 4.4%.	O	6799..7010
Here are price trends on the world's major stock markets, as calculated by Morgan Stanley Capital International Perspective, Geneva.	B-entrel	7013..7145
To make them directly comparable, each index is based on the close of 1969 equaling 100.	B-entrel	7146..7234
The percentage change is since year-end.	I-entrel	7235..7275
