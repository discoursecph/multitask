The Persian Gulf showdown between Iraq and the United States took a more personal turn Thursday when Iraq 's Saddam Hussein called President Bush a liar and said the outbreak of holy war could bring thousands of Americans home in coffins .	NONE	2
Bush , commenting on the two-week-old gulf crisis from his vacation home in Maine , said he saw little reason to be optimistic about a settlement of the dispute , which stems from Iraq 's invasion of oil-wealthy Kuwait and its subsequent military buildup on the border of Saudi Arabia .	NONE	3
After a two-hour meeting at his Kennebunkport home with King Hussein of Jordan , Bush said , `` I did not come away with any feeling of hope '' that Iraq would withdraw its army from Kuwait .	NONE	4
Bush also said Thursday that King Hussein assured him Jordan would close the last remaining free port to most Iraqi trade as the economic embargo on materials to Iraq continued unabated .	PAST	5
Foodstuffs are among the goods being blocked from entry ;	NONE	6
Pentagon sources in Washington meanwhile said the Bush administration plans to deploy 45,000 Marines to the region to back up the thousands of Army , Navy and Air Force troops already in place in the gulf and the Saudi desert .	INFINITIVE	8
At a news conference , Secretary of State James A. Baker III said Jordan `` is seeking some guidance '' about a provision in the U.N.-backed trade embargo that allows food for humanitarian purposes .	NONE	9
Worries however grew about the safety of Americans and other Westerners trapped in Kuwait .	PAST	10
Iraqi military authorities ordered all Americans and Britons in Kuwait to assemble at a hotel , officials said .	PAST	11
`` Very few '' of the 2,500 Americans in occupied Kuwait complied with the order , a senior U.S. official told The Associated Press .	PAST	12
Iraq said the roundup was to protect them from unspecified threats ;	NONE	13
British Foreign Office minister William Waldegrave called the order `` grave and sinister . ''	PAST	14
`` What we fear is that they will be interned somewhere , most likely in Iraq , '' Waldegrave said .	PAST	15
A total of about 3,000 Americans , 3,000 Britons and more than 450 Japanese are in Iraq and Kuwait .	PRESENT	16
Overall , more than 2 million foreigners are in both countries .	PRESENT	17
In addition to the estimated 45,000 Marines to ultimately be part of Operation Desert Shield , Stealth fighter planes and the aircraft carrier John F. Kennedy are also headed to Saudi Arabia to protect it from Iraqi expansionism .	INFINITIVE	19
In Washington , Pentagon spokesman Pete Williams said Iraq has continued to increase its armed forces in Kuwait and they now number about 160,000 .	PRESENT	20
Saddam has been under international quarantine since his Aug. 2 power-grab , or what he calls an `` eternal merger '' with Kuwait .	NONE	21
In a long verbal attack read on Iraqi television Thursday , Saddam repeatedly called Bush `` a liar '' and said a shooting war could produce body bags courtesy of Baghdad .	NONE	22
`` We continue to pray and pray hard to God so that there will be no confrontation whereby you will receive thousands of Americans wrapped in sad coffins after you had pushed them into a dark tunnel , '' Saddam said .	PAST	23
He called U.S. soldiers massing in Saudi Arabia the real occupiers in the Persian Gulf .	PAST	24
Replied State Department deputy spokesman Richard Boucher , `` We have n't really analyzed the statement in detail but it appears to be just another example of his outlandish rhetoric and his attempts to distort the truth .	NONE	25
`` We believe that his words can not distract the world from the facts of Iraqi aggression . ''	NONE	26
An international land , sea and air force has mobilized since Iraq 's invasion , which was sparked by disputes over oil , land and repayment of war loans .	NONE	27
In the largest U.S. military operation since Vietnam , an estimated 20,000 American GIs have already massed to defend Saudi Arabia .	NONE	28
`` We do n't just arrive , '' said four-star Gen. John Dailey , assistant commandant of the U.S. Marine Corps .	PAST	29
`` We 're there to stay for a fairly lengthy period . ''	INFINITIVE	30
Egypt , Syria , Morocco and Bangladesh also committed ground troops , to a much lesser degree .	PAST	31
The U.S. Navy has 27 ships in the maritime barricade of Iraq .	PRESENT	32
They are aided by Britain , West Germany , Australia , Canada , the Netherlands and Belgium .	PRESENT	33
Bush was expected to authorize naval commanders to use `` the minimum force necessary '' to interdict shipments to and from Iraq , a U.S. official said .	INFINITIVE	34
That could include firing across the bow to halt a ship .	INFINITIVE	35
In the air , U.S. Air Force fliers say they have engaged in `` a little cat and mouse '' with Iraqi warplanes , which have retreated when weapons radar locks onto them .	PRESENT	36
`` They do n't want to play with us , '' one U.S. crew chief said .	PAST	37
In Kuwait , the Iraqis have rimmed the capital city with an air-defense system , according to a U.S. official who spoke on the condition of anonymity .	PRESPART	38
He declined to say if the weapons included missiles , but the Iraqis have them in their arsenal .	PAST	39
The combined operations are designed to isolate and strangle Iraq until it retreats from Kuwait .	INFINITIVE	41
The quarantine hopes to staunch the flow of Iraqi oil , which is Iraq 's economic lifeblood , and clamp down on food and supplies going in .	NONE	42
Iraq now controls 20 percent of the world 's oil reserves with its conquest of Kuwait .	NONE	43
Only Saudi Arabia has more oil reserves .	PRESENT	44
The economic chokehold appears to be working .	NONE	45
The Lloyd 's List International newspaper , which monitors worldwide shipping , said Iraq 's fleet of 80 tankers and cargo ships has stopped regular trading .	PAST	46
John Prescott , a shipping correspondent , said there was no shipping in Kuwaiti or Iraqi ports and that activity was trailing off in the Jordanian port of Aqaba .	PAST	47
Bush 's chief objective in his meeting with Hussein was to press the king to shut down Iraq 's food and oil supply route from Aqaba on the Red Sea .	INFINITIVE	48
Aqaba is Iraq 's only outlet now that an international noose has tightened .	NONE	49
Bush has indicated the U.S. Navy will barricade the port from Iraqi ships .	FUTURE	50
The president also has offered to help offset Jordan 's costs because 40 percent of its exports go to Iraq and 90 percent of its oil comes from there .	PRESENT	51
`` It 's our only outlet to the sea and the rest of the world , '' Hussein said .	PAST	52
He also said of trade with Iraq : `` There are no shipments at the moment . ''	PAST	53
A day earlier , scores of trucks , many with Iraqi license plates , streamed north out of Aqaba to Amman and onto the desert highway bound for Iraq .	PAST	54
The Jordanian monarch met this week with Saddam , but he told reporters he had no message from Baghdad .	PAST	55
`` I am not talking on behalf of anyone in the area ... but myself , '' Hussein said .	PAST	56
In the United Nations , Libya called for the replacement of U.S. forces in the Persian Gulf with Arab League forces and U.N. soldiers .	PAST	57
Libyan leader Moammar Gadhafi , in a letter to the U.N. Secretary-General , also called for an emergency Security Council meeting in Geneva to remove U.S. forces .	PAST	58
There was no decision on a meeting .	PAST	59
Thirty-two of the 159 U.N. members had filed compliance reports by Wednesday , and all were honoring the sanctions Iraq .	PAST	60
Also Thursday , Saudi Arabia called for an emergency conference of the Organization of Petroleum Exporting Countries to discuss how much oil to pump .	INFINITIVE	61
The minister denied the kingdom had notified notified any of its customers of any cutbacks in oil supply .	PAST	62
Reports attributed to the Japanese foreign ministry said Saudi Arabia told U.S. , European and Japanese oil companies of a 15-20 percent cutback in its oil supply in September .	PAST	63
Meanwhile , Egypt 's official Middle East News Agency said Thursday that Saddam was the target of an assassination attempt , which led to `` large-scale '' arrests , including some close associates of the Iraqi strongman .	PAST	64
The agency quoted witnesses as saying tanks and armored cars are patrolling the streets of Baghdad .	PAST	65
There was no independent confirmation of the report by the government-run news agency , which did not say when the reported attempt occurred .	PAST	66
