Microsoft Corp. 's earnings growth continued to outstrip that of most of its competitors and customers in the personal-computer industry, as it reported a 36% jump in fiscal first-quarter earnings on a 33% revenue gain.	O	9..228
The Redmond, Wash. company, a bellwether provider of operating systems and software for personal-computer makers and users, reported net income for the quarter ended Sept. 30 of $49.6 million, or 87 cents a share, up from $36.6 million, or 65 cents a share, in the year-ago period.	B-conjunction	231..512
Revenue rose to $235.2 million, from $176.4 million.	I-conjunction	513..565
Microsoft previously indicated it would have a strong quarter by forecasting its revenue gain on Oct. 4, causing a $6.50 a share jump in its stock.	B-contrast	568..715
But its stock jumped again yesterday as it disclosed surprisingly strong margins on those sales.	I-contrast	716..812
Microsoft's stock rose $2.875 a share in national over-the-counter trading to $78.625.	B-contrast	815..901
The stock had hit a high of $81 a share early last week but collapsed to $73.50 in the Friday stock plunge.	I-contrast	902..1009
The company had been experiencing softening margins because of increased sales of software applications, which have lower margins than do operating systems.	O	1012..1168
But the company said that trend was offset in the first quarter by better economies of scale and efficiencies in manufacturing.	O	1169..1296
As a result, Microsoft's cost of goods, as a percentage of sales, fell 17% from the year-ago quarter and 13% from the previous period.	B-cause	1299..1433
The trend drove up the aftertax margin -- net income as a percentage of revenues -- to 21.1% in the quarter, compared with 20.7% a year earlier.	I-cause	1434..1578
Microsoft officials said the strong results also reflected continuing high demand for its software applications and operating systems.	O	1581..1715
While it has predicted that overall growth in unit sales of personal computers is slowing to about a 10% yearly rate, its own products are selling at a much faster rate because many are geared to the high-performance end of the market.	B-conjunction	1716..1951
That segment continues to post strong quarter-to-quarter gains, while the low-end, or commodity segment, of the industry is experiencing sluggish growth or even sales declines.	I-conjunction	1952..2128
Compared with its previous quarter, the final period of its 1989 fiscal year, net rose 9%, and sales rose 7%.	O	2131..2240
