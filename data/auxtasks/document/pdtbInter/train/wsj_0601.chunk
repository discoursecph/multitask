In an age of specialization, the federal judiciary is one of the last bastions of the generalist.	O	9..106
A judge must jump from murder to antitrust cases, from arson to securities fraud, without missing a beat.	O	107..212
But even on the federal bench, specialization is creeping in, and it has become a subject of sharp controversy on the newest federal appeals court.	O	215..362
The Court of Appeals for the Federal Circuit was created in 1982 to serve, among other things, as the court of last resort for most patent disputes.	B-asynchronous	365..513
Previously, patent cases moved through the court system to one of the 12 circuit appeals courts.	I-asynchronous	514..610
There, judges who saw few such cases and had no experience in the field grappled with some of the most technical and complex disputes imaginable.	O	611..756
A new specialty court was sought by patent experts, who believed that the generalists had botched too many important, multimillion-dollar cases.	B-conjunction	759..903
Some patent lawyers had hoped that such a specialty court would be filled with experts in the field.	B-contrast	904..1004
But the Reagan administration thought otherwise, and so may the Bush administration.	I-contrast	1005..1089
Since 1984, the president has filled four vacancies in the Federal Circuit court with non-patent lawyers.	O	1092..1197
Now only three of the 12 judges -- Pauline Newman, Chief Judge Howard T. Markey, 68, and Giles Rich, 85 -- have patent-law backgrounds.	O	1198..1333
The latter two and Judge Daniel M. Friedman, 73, are approaching senior status or retirement.	O	1334..1427
Three seats currently are vacant and three others are likely to be filled within a few years, so patent lawyers and research-based industries are making a new push for specialists to be added to the court.	O	1430..1635
Several organizations, including the Industrial Biotechnical Association and the Pharmaceutical Manufacturers Association, have asked the White House and Justice Department to name candidates with both patent and scientific backgrounds.	B-restatement	1636..1872
The associations would like the court to include between three and six judges with specialized training.	I-restatement	1873..1977
Some of the associations have recommended Dr. Alan D. Lourie, 54, a former patent agent with a doctorate in organic chemistry who now is associate general counsel with SmithKline Beckman Corp. in Philadelphia.	B-entrel	1980..2189
Dr. Lourie says the Justice Department interviewed him last July.	I-entrel	2190..2255
Their effort has received a lukewarm response from the Justice Department.	O	2258..2332
"We do not feel that seats are reserved (for patent lawyers)," says Justice spokesman David Runkel, who declines to say how soon a candidate will be named.	O	2333..2488
"But we will take it into consideration."	O	2489..2530
The Justice Department's view is shared by other lawyers and at least one member of the court, Judge H. Robert Mayer, a former civil litigator who served at the claims court trial level before he was appointed to the Federal Circuit two years ago.	O	2533..2780
"I believe that any good lawyer should be able to figure out and understand patent law," Judge Mayer says, adding that "it's the responsibility of highly paid lawyers (who argue before the court) to make us understand (complex patent litigation)."	O	2781..3028
Yet some lawyers point to Eli Lilly & Co. vs. Medtronic, Inc., the patent infringement case the Supreme Court this month agreed to review, as an example of poor legal reasoning by judges who lack patent litigation experience.	O	3031..3256
(Judge Mayer was not on the three-member panel.)	O	3257..3305
In the Lilly case, the appeals court broadly construed a federal statute to grant Medtronic, a medical device manufacturer, an exemption to infringe a patent under certain circumstances.	B-entrel	3308..3494
If the Supreme Court holds in Medtronic's favor, the decision will have billion-dollar consequences for the manufacturers of medical devices, color and food additives and all other non-drug products that required Food & Drug Administration approval.	I-entrel	3495..3744
Lisa Raines, a lawyer and director of government relations for the Industrial Biotechnical Association, contends that a judge well-versed in patent law and the concerns of research-based industries would have ruled otherwise.	B-list	3747..3972
And Judge Newman, a former patent lawyer, wrote in her dissent when the court denied a motion for a rehearing of the case by the full court, "The panel's judicial legislation has affected an important high-technological industry, without regard to the consequences for research and innovation or the public interest."	I-list	3973..4290
Says Ms. Raines, "(The judgment) confirms our concern that the absence of patent lawyers on the court could prove troublesome.	O	4293..4419
