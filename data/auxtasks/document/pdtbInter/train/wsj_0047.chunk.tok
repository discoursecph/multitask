The Department of Health and Human Services plans to extend its moratorium on federal funding of research involving fetal-tissue transplants .	O	9..150
Medical researchers believe the transplantation of small amounts of fetal tissue into humans could help treat juvenile diabetes and such degenerative diseases as Alzheimer 's , Parkinson 's and Huntington 's .	B-contrast	153..357
But anti-abortionists oppose such research because they worry that the development of therapies using fetal-tissue transplants could lead to an increase in abortions .	I-contrast	358..524
James Mason , assistant secretary for health , said the ban on federal funding of fetal-tissue transplant research " should be continued indefinitely . "	O	527..675
He said the ban wo n't stop privately funded tissue-transplant research or federally funded fetal-tissue research that does n't involve transplants .	O	676..822
Department officials say that HHS Secretary Louis Sullivan will support Dr. Mason 's ruling , which will be issued soon in the form of a letter to the acting director of the National Institutes of Health .	B-entrel	825..1027
Both Dr. Mason and Dr. Sullivan oppose federal funding for abortion , as does President Bush , except in cases where a woman 's life is threatened .	I-entrel	1028..1172
The controversy began in 1987 when the National Institutes of Health , aware of the policy implications of its research , asked for an HHS review of its plan to implant fetal tissue into the brain of a patient suffering from Parkinson 's disease .	B-asynchronous	1175..1418
The department placed a moratorium on the research , pending a review of scientific , legal and ethical issues .	I-asynchronous	1419..1528
A majority of an NIH-appointed panel recommended late last year that the research continue under carefully controlled conditions , but the issue became embroiled in politics as anti-abortion groups continued to oppose federal funding .	O	1531..1764
The dispute has hampered the administration 's efforts to recruit prominent doctors to fill prestigious posts at the helm of the NIH and the Centers for Disease Control .	O	1765..1933
Several candidates have withdrawn their names from consideration after administration officials asked them for their views on abortion and fetal-tissue transplants .	O	1936..2100
Antonio Novello , whom Mr. Bush nominated to serve as surgeon general , reportedly has assured the administration that she opposes abortion .	B-entrel	2101..2239
Dr. Novello is deputy director of the National Institute of Child Health and Human Development .	I-entrel	2240..2335
Some researchers have charged that the administration is imposing new ideological tests for top scientific posts .	O	2338..2451
Earlier this week , Dr. Sullivan tried to defuse these charges by stressing that candidates to head the NIH and the CDC will be judged by " standards of scientific and administrative excellence , " not politics .	B-contrast	2452..2659
But the administration 's handling of the fetal-tissue transplant issue disturbs many scientists .	I-contrast	2662..2758
" When scientific progress moves into uncharted ground , there has to be a role for society to make judgments about its applications , " says Myron Genel , associate dean of the Yale Medical School .	O	2759..2952
" The disturbing thing about this abortion issue is that the debate has become polarized , so that no mechanism exists " for finding a middle ground .	O	2953..3099
Yale is one of the few medical institutions conducting privately funded research on fetal-tissue transplants .	B-contrast	3102..3211
But Dr. Genel warns that Dr. Mason 's ruling may discourage private funding .	I-contrast	3212..3287
" The unavailability of federal funds , and the climate in which the decision was made , certainly do n't provide any incentive for one of the more visible foundations to provide support , " he said .	O	3288..3481
Despite the flap over transplants , federal funding of research involving fetal tissues will continue on a number of fronts .	B-conjunction	3484..3607
Despite the flap over transplants , federal funding of research involving fetal tissues will continue on a number of fronts .	B-cause	3484..3607
" Such research may ultimately result in the ability to regenerate damaged tissues or to turn off genes that cause cancer " or to regulate genes that cause Down 's syndrome , the leading cause of mental retardation , according to an NIH summary .	B-entrel	3608..3848
The NIH currently spends about $ 8 million annually on fetal-tissue research out of a total research budget of $ 8 billion .	I-entrel	3849..3970
