Walter Sisulu and the African National Congress came home yesterday .	O	9..77
After 26 years in prison , Mr. Sisulu , the 77-year-old former secretary-general of the liberation movement , was dropped off at his house by a prison services ' van just as the sun was coming up .	O	80..272
At the same time , six ANC colleagues , five of whom were arrested with him in 1963 and sentenced to life imprisonment , were reunited with their families at various places around the country .	O	273..462
And as the graying men returned to their homes , the ANC , outlawed in South Africa since 1960 and still considered to be the chief public enemy by the white government , defiantly returned to the streets of the country 's black townships .	O	465..700
A huge ANC flag , with black , green and gold stripes , was hoisted over the rickety gate at Mr. Sisulu 's modest house , while on the street out front , boys displayed the ANC colors on their shirts , caps and scarves .	B-expansion	701..913
At the small four-room home of Elias Motsoaledi , a leading ANC unionist and a former commander in the group 's armed wing , Umkhonto we Sizwe , well-wishers stuck little ANC flags in their hair and a man tooted on an antelope horn wrapped in ANC ribbons .	I-expansion	914..1165
" I am happy to see the spirit of the people , " said Mr. Sisulu , looking dapper in a new gray suit .	O	1168..1265
As the crowd outside his home shouted " ANC , ANC , " the old man shot his fists into the air .	B-contingency	1266..1356
" I 'm inspired by the mood of the people . "	I-contingency	1357..1398
Under the laws of the land , the ANC remains an illegal organization , and its headquarters are still in Lusaka , Zambia .	O	1401..1519
But the unconditional release of the seven leaders , who once formed the intellectual and organizational core of the ANC , is a de facto unbanning of the movement and the rebirth of its internal wing .	O	1520..1718
" The government can never put the ANC back into the bottle again , " said Cassim Saloojee , a veteran anti-apartheid activist on hand to welcome Mr. Sisulu .	O	1721..1874
" Things have gone too far for the government to stop them now .	B-expansion	1875..1937
There 's no turning back . "	I-expansion	1938..1963
There was certainly no stopping the tide of ANC emotion last night , when hundreds of people jammed into the Holy Cross Anglican Church in Soweto for what became the first ANC rally in the country in 30 years .	B-entrel	1966..2174
Deafening chants of " ANC " and " Umkhonto we Sizwe " shook the church as the seven aging men vowed that the ANC would continue its fight against the government and the policies of racial segregation on all fronts , including the armed struggle .	I-entrel	2175..2415
And they called on the government to release Nelson Mandela , the ANC 's leading figure , who was jailed with them and remains in prison .	O	2416..2550
Without him , said Mr. Sisulu , the freeing of the others " is only a half-measure . "	O	2551..2632
President F.W. de Klerk released the ANC men -- along with one of the founding members of the Pan Africanist Congress , a rival liberation group -- as part of his efforts to create a climate of trust and peace in which his government can begin negotiations with black leaders over a new constitution aimed at giving blacks a voice in national government .	O	2635..2988
But Pretoria may instead be creating a climate for more turmoil and uncertainty in this racially divided country .	B-contingency	2991..3104
As other repressive governments , particularly Poland and the Soviet Union , have recently discovered , initial steps to open up society can create a momentum for radical change that becomes difficult , if not impossible , to control .	I-contingency	3105..3334
As the days go by , the South African government will be ever more hard pressed to justify the continued imprisonment of Mr. Mandela as well as the continued banning of the ANC and enforcement of the state of emergency .	B-entrel	3337..3555
If it does n't yield on these matters , and eventually begin talking directly to the ANC , the expectations and promise raised by yesterday 's releases will turn to disillusionment and unrest .	I-entrel	3556..3744
If it does , the large number of right-wing whites , who oppose any concessions to the black majority , will step up their agitation and threats to take matters into their own hands .	O	3745..3924
The newly released ANC leaders also will be under enormous pressure .	O	3927..3995
The government is watching closely to see if their presence in the townships leads to increased anti-government protests and violence ; if it does , Pretoria will use this as a reason to keep Mr. Mandela behind bars .	O	3996..4210
Pretoria has n't forgotten why they were all sentenced to life imprisonment in the first place : for sabotage and conspiracy to overthrow the government .	O	4211..4362
In addition , the government is figuring that the releases could create a split between the internal and external wings of the ANC and between the newly freed leaders and those activists who have emerged as leaders inside the country during their imprisonment .	B-comparison	4365..4624
In order to head off any divisions , Mr. Mandela , in a meeting with his colleagues before they were released , instructed them to report to the ANC headquarters in Lusaka as soon as possible .	I-comparison	4625..4814
The men also will be faced with bridging the generation gap between themselves and the country 's many militant black youths , the so-called young lions who are anxious to see the old lions in action .	O	4817..5015
Says Peter Mokaba , president of the South African Youth Congress : " We will be expecting them to act like leaders of the ANC . "	O	5016..5141
They never considered themselves to be anything else .	B-contingency	5144..5197
At last night 's rally , they called on their followers to be firm , yet disciplined , in their opposition to apartheid .	B-entrel	5198..5314
" We emphasize discipline because we know that the government is very , very sensitive , " said Andrew Mlangeni , another early Umkhonto leader who is now 63 .	I-entrel	5315..5468
" We want to see Nelson Mandela and all our comrades out of prison , and if we are n't disciplined we may not see them here with us .	O	5469..5598
