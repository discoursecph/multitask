A consortium of private investors operating as LJH Funding Co. said it has made a $ 409 million cash bid for most of L.J. Hooker Corp. 's real-estate and shopping-center holdings .	O	9..187
The $ 409 million bid includes the assumption of an estimated $ 300 million in secured liabilities on those properties , according to those making the bid .	O	190..342
The group is led by Jay Shidler , chief executive officer of Shidler Investment Corp. in Honolulu , and A. Boyd Simpson , chief executive of the Atlanta-based Simpson Organization Inc .	O	345..526
Mr. Shidler 's company specializes in commercial real-estate investment and claims to have $ 1 billion in assets ; Mr. Simpson is a developer and a former senior executive of L.J. Hooker .	O	527..711
" The assets are good , but they require more money and management " than can be provided in L.J. Hooker 's current situation , said Mr. Simpson in an interview . "	B-entrel	714..872
Hooker 's philosophy was to build and sell .	B-contrast	872..914
We want to build and hold . "	I-contrast	915..942
L.J. Hooker , based in Atlanta , is operating with protection from its creditors under Chapter 11 of the U.S. Bankruptcy Code .	B-synchrony	945..1069
Its parent company , Hooker Corp. of Sydney , Australia , is currently being managed by a court-appointed provisional liquidator .	B-entrel	1070..1196
Sanford Sigoloff , chief executive of L.J. Hooker , said yesterday in a statement that he has not yet seen the bid but that he would review it and bring it to the attention of the creditors committee .	I-entrel	1197..1395
The $ 409 million bid is estimated by Mr. Simpson as representing 75 % of the value of all Hooker real-estate holdings in the U.S.	B-concession	1398..1526
Not included in the bid are Bonwit Teller or B. Altman & Co. , L.J. Hooker 's department-store chains .	I-concession	1527..1627
The offer covers the massive 1.8 million-square-foot Forest Fair Mall in Cincinnati , the 800,000 square-foot Richland Fashion Mall in Columbia , S.C. , and the 700,000 square-foot Thornton Town Center mall in Thornton , Colo .	O	1630..1852
The Thornton mall opened Sept. 19 with a Bigg 's hypermarket as its anchor ; the Columbia mall is expected to open Nov. 15 .	O	1853..1974
Other Hooker properties included are a 20-story office tower in midtown Atlanta , expected to be completed next February ; vacant land sites in Florida and Ohio ; L.J. Hooker International , the commercial real-estate brokerage company that once did business as Merrill Lynch Commercial Real Estate , plus other shopping centers .	O	1977..2301
The consortium was put together by Hoare Govett , the London-based investment banking company that is a subsidiary of Security Pacific Corp .	B-entrel	2304..2443
" We do n't anticipate any problems in raising the funding for the bid , " said Allan Campbell , the head of mergers and acquisitions at Hoare Govett , in an interview .	B-entrel	2444..2606
Hoare Govett is acting as the consortium 's investment bankers .	I-entrel	2607..2669
According to people familiar with the consortium , the bid was code-named Project Klute , a reference to the film " Klute " in which a prostitute played by actress Jane Fonda is saved from a psychotic businessman by a police officer named John Klute .	O	2672..2918
L.J. Hooker was a small home-building company based in Atlanta in 1979 when Mr. Simpson was hired to push it into commercial development .	B-asynchronous	2921..3058
The company grew modestly until 1986 , when a majority position in Hooker Corp. was acquired by Australian developer George Herscu , currently Hooker 's chairman .	I-asynchronous	3059..3218
Mr. Herscu proceeded to launch an ambitious , but ill-fated , $ 1 billion acquisition binge that included Bonwit Teller and B. Altman & Co. , as well as majority positions in Merksamer Jewelers , a Sacramento chain ; Sakowitz Inc. , the Houston-based retailer , and Parisian Inc. , the Southeast department-store chain .	O	3221..3531
Eventually Mr. Simpson and Mr. Herscu had a falling out over the direction of the company , and Mr. Simpson said he resigned in 1988 .	O	3534..3666
Since then , Hooker Corp. has sold its interest in the Parisian chain back to Parisian 's management and is currently attempting to sell the B. Altman & Co. chain .	B-conjunction	3667..3828
In addition , Robert Sakowitz , chief executive of the Sakowitz chain , is seeking funds to buy out the Hooker interest in his company .	B-conjunction	3829..3961
The Merksamer chain is currently being offered for sale by First Boston Corp .	I-conjunction	3962..4039
Reached in Honolulu , Mr. Shidler said that he believes the various Hooker malls can become profitable with new management .	O	4042..4164
" These are n't mature assets , but they have the potential to be so , " said Mr. Shidler .	O	4165..4250
" Managed properly , and with a long-term outlook , these can become investment-grade quality properties .	O	4251..4353
