This year is the 75th anniversary of the Federal Reserve System	root	9..72
and some members of Congress think it 's time to take a fresh look at the nation 's central bank .	conjunction	73..169
After 75 years there may be a few things that are worth reexamining .	cause	170..238
The regional Federal Reserve Bank setup , for instance , may be out of date .	instantiation	241..315
In earlier years it may have seemed reasonable to give Richmond , Va. , a bank and allow Los Angeles only a branch of the San Francisco bank	instantiation	316..454
but times have changed .	contrast	455..479
Maybe the whole regional system is an anachronism ; the Fed , after all , is a national central bank .	restatement	480..578
Some of the would-be reformers , however , want to restore an arrangement we once had -- or , at least , part of it .	norel	581..693
In the beginning , the treasury secretary and the comptroller of the currency were both ex officio members of the Federal Reserve Board .	restatement	694..829
But in 1935 , when Congress was trying to find someone or something to blame for the Great Depression , it decided to drop both the secretary and the comptroller from the board .	asynchronous	830..1005
Carter Glass , a former treasury secretary who was then back in Congress , probably influenced the decision .	norel	1008..1114
He said that when he was on the board he felt that he had a great deal of power and , somehow , he did n't think that was a good thing .	restatement	1115..1247
Times have changed .	norel	1250..1269
Rep. Byron Dorgan ( D. , N.D . ) has introduced a bill in Congress , co-sponsored by Rep. Lee Hamilton ( D. , Ind. ) , that would put the treasury secretary back on the board .	restatement	1270..1436
There is doubt that the change would accomplish much but at least Congress , as in 1935 , would be doing something .	comparison	1437..1550
So far no one has suggested putting the comptroller back on the board .	entrel	1551..1621
Nicholas Brady , the incumbent treasury secretary , is of course aware of the proposal	norel	1624..1708
and he does n't like it much .	conjunction	1709..1738
Mr. Dorgen has changed tactics , dropping the seat-for-the-secretary idea .	cause	1739..1812
That may have pleased the secretary	entrel	1813..1848
but H. Erich Heinemann , chief economist of the investment firm of Ladenburg , Thalmann & Co. , suggests that Mr. Brady may figure he already has all the power he needs .	contrast	1849..2016
Like most treasury secretaries , Mr. Brady takes a keen interest in monetary matters , of course .	norel	2019..2114
He was , in fact , taking an especially keen interest in board matters	restatement	2115..2183
even before he went to the treasury .	asynchronous	2184..2220
After the October 1987 market crash , Mr. Brady as a private citizen headed a presidential commission that tried to decide what went wrong and what should be done about it .	restatement	2221..2392
One of the commission 's recommendations was that a single agency , probably the Federal Reserve , should coordinate regulation of all financial markets .	entrel	2393..2543
This recommendation might have encouraged a turf-hungry bureaucrat to try to expand his power	norel	2546..2639
but so far Federal Reserve Chairman Alan Greenspan has n't made a pitch for the job .	concession	2640..2724
The Fed has plenty of responsibilities in times of market turmoil	cause	2725..2790
and in 1987 and again in 1989 it appears to have handled them well .	conjunction	2791..2858
Mr. Brady has said he thought government agencies in the latest market drop were better prepared to coordinate their actions , but he has left no doubt that he still likes the ideas the commission advanced nearly two years ago .	contrast	2859..3085
In recent weeks , moreover , Mr. Brady has joined other administration officials in trying to urge the Fed toward lower interest rates .	conjunction	3088..3221
The urging admittedly has been gentle .	contrast	3222..3260
In an interview with the Washington Post in early October , the secretary said the Fed may be slightly more interested in curbing inflation than the administration is , while the administration may put slightly more emphasis on spurring economic growth .	restatement	3261..3512
At least some economists , of course , would argue that inflation deserves a lot of emphasis .	norel	3515..3606
Earlier this month the St. Louis Fed held a conference to assess the system 's first 75 years .	restatement	3607..3700
Allan Meltzer , a Carnegie-Mellon University economist , noted that the Fed 's record included the longest , most sustained , peacetime inflation in our history , dating from either 1966 or 1967 to 1989 .	entrel	3701..3898
The inflation-growth argument is an old one	entrel	3899..3942
but Mr. Brady , on the board or off , is surely trying to influence Fed policy .	concession	3943..4021
Equally importantly , the treasury secretary has spearheaded the administration effort to bring the U.S. dollar down by shopping avidly for West German marks and Japanese yen .	norel	4024..4198
The treasury can do something on its own	entrel	4199..4239
but to have any hope of success it needs help from the Fed .	contrast	4240..4300
The central bank has been helping , but apparently not especially eagerly .	conjunction	4301..4374
The Fed has been intervening in foreign currency markets , all right	norel	4377..4444
but through August , at least , it appeared to be " sterilizing " the intervention .	contrast	4445..4525
In other words , it was offsetting purchases of marks and yen by buying dollars in the domestic money market .	restatement	4526..4634
Now , sterilized intervention may have some effect .	norel	4637..4687
When traders see the Fed is in the exchange market it may make them tread a little carefully , for fear of what the central bank may do .	cause	4688..4823
But it 's generally accepted that sterilized intervention has little or no lasting impact on currency values .	norel	4824..4932
After August the Fed may have stopped sterilizing	restatement	4933..4982
but it 's hard to see much impact on the dollar .	concession	4983..5031
The dollar is still highly volatile .	restatement	5032..5068
The Fed has let interest rates slip slightly	entrel	5069..5113
but whether the main reason was dollar intervention , the gloomy reports on manufacturing employment , or the Friday 13 market drop , only Mr. Greenspan and his associates know .	contrast	5114..5289
Earlier this year , Martin Feldstein , president of the National Bureau of Economic Research , argued forcefully that a government that wants steady , stable exchange rates might try some steady stable economic policies .	norel	5292..5508
Trying to manage exchange rates to some desired level , he said , " would mean diverting monetary and fiscal policies from their customary roles and thereby risking excessive inflation and unemployment and inadequate capital formation . "	comparison	5509..5742
The more we think about it , the more we suspect Mr. Brady does indeed have enough power where he already is .	norel	5745..5853
