Japanese investors nearly single-handedly bought up two new mortgage securities-based mutual funds totaling $ 701 million , the U.S. Federal National Mortgage Association said .	root	9..183
The purchases show the strong interest of Japanese investors in U.S. mortgage-based instruments , Fannie Mae 's chairman , David O. Maxwell , said at a news conference .	norel	186..350
He said more than 90 % of the funds were placed with Japanese institutional investors .	restatement	351..436
The rest went to investors from France and Hong Kong .	conjunction	437..490
Earlier this year , Japanese investors snapped up a similar , $ 570 million mortgage-backed securities mutual fund .	norel	493..605
That fund was put together by Blackstone Group , a New York investment bank .	entrel	606..681
The latest two funds were assembled jointly by Goldman , Sachs & Co. of the U.S. and Japan 's Daiwa Securities Co .	entrel	682..794
The new , seven-year funds -- one offering a fixed-rate return and the other with a floating-rate return linked to the London interbank offered rate -- offer two key advantages to Japanese investors .	norel	797..995
First , they are designed to eliminate the risk of prepayment -- mortgage-backed securities can be retired early if interest rates decline , and such prepayment forces investors to redeploy their money at lower rates .	norel	996..1211
Second , they channel monthly mortgage payments into semiannual payments , reducing the administrative burden on investors .	norel	1212..1333
By addressing those problems , Mr. Maxwell said , the new funds have become " extremely attractive to Japanese and other investors outside the U.S. "	norel	1336..1481
Such devices have boosted Japanese investment in mortgage-backed securities to more than 1 % of the $ 900 billion in such instruments outstanding	norel	1484..1627
and their purchases are growing at a rapid rate .	conjunction	1628..1677
They also have become large purchasers of Fannie Mae 's corporate debt , buying $ 2.4 billion in Fannie Mae bonds during the first nine months of the year , or almost a tenth of the total amount issued .	conjunction	1678..1876
