At a private dinner Thursday , Drexel Burnham Lambert Inc. chief executive Frederick Joseph delivered a sobering message about the junk bond market to officials of Prudential Insurance Co. of America .	ROOT	0
Mr. Joseph conceded the junk market was in disarray , according to people familiar with the discussion .	COREF_PREV	1
He said Drexel -- the leading underwriter of high - risk junk bonds -- could no longer afford to sell any junk offerings if they might later become troubled because Drexel risked losing its highly lucrative junk franchise .	COREF_PREV	2
The dinner was a stark confirmation that 1989 is the worst year ever for the $ 200 billion junk market .	COREF_OTHER	3
And investors and traders alike say the current turmoil could take years to resolve .	COREF_OTHER	4
Amid the market disorder , even Drexel , which has the widest and most loyal following of junk bond investors , is pulling in its horns .	COREF_PREV	5
Although the big investment bank still dominates the junk market , Drexel has been unable to stem the fallout from growing junk bond defaults , withdrawn new offerings , redemptions by shareholders in junk bond mutual funds and an exodus of once - devoted investors .	COREF_PREV	6
For many money managers , the past four months have been humiliating .	NONE	7
`` This is the worst shakeout ever in the junk market , and it could take years before it 's over , '' says Mark Bachmann , a senior vice president at Standard -AMP- Poor 's Corp. , a credit rating company .	COREF_OTHER	8
In the third quarter , for example , junk bonds -- those with less than an investment - grade rating -- showed negative returns , the only major sector of the bond market to do so .	NONE	9
Since the end of last year , junk bonds have been outperformed by all categories of investment - grade bonds , including ultra-safe Treasury securities .	NONE	10
The junk market , which mushroomed to $ 200 billion from less than $ 2 billion at the start of the decade , has been declining for months as issuers have stumbled under the weight of hefty interest payments .	COREF_OTHER	11
The fragile market received its biggest jolt last month from Campeau Corp. , which created its U.S. retailing empire with more than $ 3 billion in junk financing .	COREF_PREV	12
Campeau developed a cash squeeze that caused it to be tardy on some interest payments and to put its prestigious Bloomingdale 's department store chain up for sale .	COREF_PREV	13
At that point , the junk market went into a tailspin as buyers disappeared and investors tried to sell .	COREF_PREV	14
In an interview , Mr. Joseph says his dinner discussion with the Prudential executives acknowledged problems for junk .	COREF_OTHER	15
`` What I thought I was saying is that the market is troubled but still viable and , appropriately enough , quite quality - conscious , which is not at all bad , '' he says .	COREF_PREV	16
`` Nobody 's been perfect in their credit judgments the past couple years , and we 're going to make sure our default rates are going to be in the acceptable parameters of the market . ''	COREF_PREV	17
What has jolted many junk buyers is the sudden realization that junk bonds can not necessarily be bought and sold with the ease of common stocks and many investment - grade bonds .	COREF_OTHER	18
Unlike the New York Stock Exchange , where buyers and sellers are quickly matched , the junk market , where risky corporate loans are traded , is sometimes closed for repairs .	COREF_PREV	19
At closely held Deltec Securities Corp. , junk bond money managers Amy K. Minella and Hannah H. Strasser say the problems of the junk market go deeper than a temporary malaise .	COREF_PREV	20
In recent months , they say , there has been heavy selling of junk bonds by some of the market 's traditional investors , while new buyers have n't materialized to replace them .	COREF_PREV	21
Wall Street securities firms , `` the primary source of liquidity for the high yield market , '' have been net sellers of junk bonds because of trading losses , Deltec said in a recent , grim report to customers .	COREF_PREV	22
Mutual funds have also been net sellers of junk bonds as junk 's relatively poor performance and negative press coverage have produced `` above - normal '' redemptions by shareholders , Deltec said .	COREF_PREV	23
Investors , trying to raise cash , have sold `` large liquid issues '' such as RJR Holdings Corp. and Kroger Co. ; declines in these benchmark issues have contributed to the market 's distress .	COREF_OTHER	24
And , Deltec said , buying has been severely reduced because savings and loans have been restricted in their junk purchases by recently passed congressional legislation .	COREF_PREV	25
`` In fact , savings and loans were sellers of high yield holdings throughout the quarter , '' Deltec said .	COREF_OTHER	26
Ms. Minella and Ms. Strasser say they are managing their junk portfolios defensively , building cash and selectively upgrading the overall quality .	COREF_OTHER	27
Meanwhile , Prudential , the nation 's largest insurer and the biggest investor in junk bonds , has seen the value of its junk bond portfolio drop to $ 6.5 billion from $ 7 billion since August because of falling junk prices .	COREF_OTHER	28
`` We certainly do have a lack of liquidity here , and it 's something to be concerned about , '' says James A. Gregoire , a managing director .	COREF_PREV	29
`` I have no reason to think things will get worse , but this market has a knack for surprising us .	COREF_PREV	30
This market teaches us to be humble . ''	COREF_PREV	31
The junk market 's `` yield hogs are learning a real painful lesson , '' he says .	COREF_PREV	32
Although the majority of junk bonds outstanding show no signs of default , the market has downgraded many junk issues as if they were in trouble , says Stuart Reese , manager of Aetna Life -AMP- Casualty Insurance Co. 's $ 17 billion investment - grade public bond portfolio .	COREF_PREV	33
`` But we think the risks are there for things getting a lot worse .	COREF_PREV	34
And the risks are n't appropriate for us , '' he says .	COREF_PREV	35
The big insurer , unlike Prudential , owns only about $ 150 million of publicly sold junk bonds .	COREF_OTHER	36
The string of big junk bond defaults , which have been a major cause of the market 's problems this year , probably will persist , some analysts say .	COREF_OTHER	37
`` If anything , we 're going to see defaults increase because credit ratings have declined , '' says Paul Asquith , associate professor at the Massachusetts Institute of Technology 's Sloan School of Management .	NONE	38
Mr. Asquith , whose study on junk bond defaults caused a furor on Wall Street when it was disclosed last April , says this year 's junk bond defaults already show a high correlation with his own findings .	COREF_PREV	39
His study showed that junk bonds over time had a cumulative default rate of 34 % .	COREF_PREV	40
One indication of a growing number of junk defaults , Mr. Asquith says , is that about half of the $ 3 billion of corporate bonds outstanding that have been lowered to a default rating by S-AMP-P this year are junk bonds sold during the market 's big issue years of 1984 through 1986 .	COREF_PREV	41
These bonds , now rated single - D , include junk offerings by AP Industries , Columbia Savings ( Colorado ) , First Texas Savings Association , Gilbraltar Financial Corp. , Integrated Resources Inc. , Metropolitan Broadcasting Corp. , Resorts International Inc. , Southmark Corp. and Vyquest Inc .	COREF_PREV	42
`` Obviously , we got a lot more smoke than fire from the people who told us the market was n't so risky , '' says Bradford Cornell , professor of finance at University of California 's Anderson Graduate School of Management in Los Angeles .	COREF_OTHER	43
Mr. Cornell has just completed a study that finds that the risks and returns of junk bonds are less than on common stocks but more than on investment - grade bonds .	COREF_PREV	44
Mr. Cornell says : `` The junk market is no bonanza as Drexel claimed , but it also is n't a disaster as the doomsayers say . ''	COREF_PREV	45
Despite the junk market 's problems , Drexel continues to enjoy a loyalty among junk bond investors that its Wall Street rivals have n't found .	COREF_PREV	46
During the past three weeks , for example , Drexel has sold $ 1.3 billion of new junk bonds for Turner Broadcasting Co. , Uniroyal Chemical , Continental Air and Duff -AMP- Phelps .	COREF_PREV	47
Still , the list of troubled Drexel bond offerings dwarfs that of any firm on Wall Street , as does its successful offerings .	COREF_PREV	48
Troubled Drexel - underwritten issues include Resorts International , Braniff , Integrated Resources , SCI TV , Gillette Holdings , Western Electric and Southmark .	COREF_PREV	49
`` Quality junk bonds will continue to trade well , '' says Michael Holland , chairman of Salomon Brothers Asset Management Inc .	COREF_OTHER	50
`` But the deals that never should have been brought have now become nuclear waste .	COREF_PREV	51
