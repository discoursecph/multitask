President Bush insists	root
it would be a great tool	-1_SN-Attribution
for curbing the budget deficit	-1_NS-Elaboration
and slicing the lard out of government programs .	-1_NN-Joint
He wants it now .	-3_NS-Elaboration
Not so fast ,	-4_NS-Elaboration
says Rep. Mickey Edwards of Oklahoma , a fellow Republican .	-1_NS-Attribution
`` I consider it one of the stupidest ideas of the 20th century , ''	-2_NS-Explanation
he says .	-1_NS-Attribution
It 's the line-item veto , a procedure	-8_NS-Elaboration
that would allow the president to kill individual items in a big spending bill	-1_NS-Elaboration
passed by Congress	-1_NS-Elaboration
without vetoing the entire bill .	-2_NS-Background
it 's far more than the budgetary gimmick	-5_NS-Elaboration
it 's far more than the budgetary gimmick	-1_SN-Evaluation
it may seem at first glance .	-1_NS-Elaboration
Rather , it 's a device	-2_NS-Elaboration
that could send shock waves through the president 's entire relationship with Democrats and Republicans alike in Congress ,	-1_NS-Elaboration
fundamentally enhance the power of the presidency	-1_NN-Joint
and transform the way	-1_NN-Joint
the government does its business .	-1_NS-Elaboration
and has long called for a law	-1_NN-Joint
giving it to the president .	-1_NS-Elaboration
that he might not rely on Congress	-4_SN-Background
that he might not rely on Congress	-1_SN-Attribution
-- which has n't shown any willingness	-1_NS-Elaboration
to surrender such authority --	-1_NS-Elaboration
to pass the line-item veto law	-3_NN-Same-unit
he seeks .	-1_NS-Elaboration
that the Constitution gives him the power ,	-31_SN-Background
that the Constitution gives him the power ,	-7_SN-Background
that the Constitution gives him the power ,	-2_SN-Attribution
that the Constitution gives him the power ,	-1_SN-Attribution
exercising a line-item veto and inviting a court challenge	-1_NN-Same-unit
whether he has the right .	-2_NS-Elaboration
whether he has the right .	-1_SN-Attribution
it would set off a political earthquake .	-5_NS-Cause
it would set off a political earthquake .	-1_SN-Contrast
`` The ramifications are enormous , ''	-1_NS-Elaboration
says Rep. Don Edwards , a California Democrat	-1_NS-Attribution
who is a senior member of the House Judiciary Committee .	-1_NS-Elaboration
`` It 's a real face-to-face arm wrestling challenge to Congress . ''	-3_NS-Elaboration
that ca n't be taken lightly	-1_NS-Elaboration
-- and for that reason , the president may back down from launching a test case this year .	-3_SN-Attribution
-- and for that reason , the president may back down from launching a test case this year .	-2_SN-Explanation
that	-1_SN-Attribution
Mr. Bush already has enough pending confrontations with Congress .	-2_NN-Same-unit
Mr. Bush already has enough pending confrontations with Congress .	-1_SN-Background
They prefer to put off the line-item veto until at least next year .	-5_NS-Elaboration
They prefer to put off the line-item veto until at least next year .	-3_SN-Explanation
Still , Mr. Bush and some other aides are strongly drawn to the idea	-19_NS-Elaboration
Still , Mr. Bush and some other aides are strongly drawn to the idea	-6_SN-Contrast
of trying out a line-item veto .	-1_NS-Elaboration
that Mr. Bush was looking for a test case .	-4_NS-Explanation
that Mr. Bush was looking for a test case .	-2_SN-Temporal
that Mr. Bush was looking for a test case .	-1_SN-Attribution
that Mr. Bush was interested in the idea ,	-2_NN-Joint
that Mr. Bush was interested in the idea ,	-1_SN-Attribution
that there was n't a firm decision	-2_NN-Contrast
that there was n't a firm decision	-1_SN-Attribution
to try it .	-1_NS-Elaboration
that a line-item veto would go a long way	-30_NS-Elaboration
that a line-item veto would go a long way	-1_SN-Attribution
in restoring discipline to the budget process .	-1_NS-Elaboration
that a president needs the ability	-3_NS-Elaboration
that a president needs the ability	-1_SN-Attribution
to surgically remove pork-barrel spending projects	-1_NS-Elaboration
that are attached to big omnibus spending bills .	-1_NS-Elaboration
Those bills ca n't easily be vetoed in their entirety	-1_NS-Elaboration
because they often are needed	-1_NS-Explanation
to keep the government operating .	-1_NS-Enablement
that 43 governors have the line-item veto	-10_NS-Elaboration
that 43 governors have the line-item veto	-1_SN-Attribution
to use on state budgets .	-1_NS-Elaboration
that Mr. Bush does n't need to wait for a law	-13_NS-Elaboration
that Mr. Bush does n't need to wait for a law	-1_SN-Attribution
giving him the veto	-1_NS-Elaboration
because the power already is implicit in the Constitution .	-2_NS-Explanation
buried in Article I , Section 7 , of the Constitution	-1_NS-Elaboration
that states :	-2_NS-Elaboration
`` Every order , resolution , or vote	-6_NS-Elaboration
`` Every order , resolution , or vote	-3_SN-Attribution
to which the concurrence of the Senate and House of Representatives may be necessary	-1_NS-Elaboration
( except on a question of adjournment )	-1_NS-Elaboration
shall be presented to the President of the United States ;	-3_NN-Same-unit
and	-4_NN-Joint
shall be approved by him	-2_NN-Same-unit
shall be approved by him	-1_SN-Temporal
or . . . disapproved by him. . . . ''	-1_NN-Joint
This clause ,	-8_NS-Elaboration
they argue ,	-1_NS-Attribution
is designed to go beyond an earlier clause	-2_NN-Same-unit
that the president can veto a `` bill , ''	-2_NS-Elaboration
that the president can veto a `` bill , ''	-1_SN-Attribution
to allow him to strike out items and riders within bills .	-6_NN-Joint
to allow him to strike out items and riders within bills .	-1_SN-Cause
Senate Minority Leader Robert Dole	-7_NS-Elaboration
( R. , Kan. ) ,	-1_NS-Elaboration
for one , accepts this argument	-2_NN-Same-unit
whether or not it is constitutional . ''	-4_NN-Joint
whether or not it is constitutional . ''	-1_SN-Attribution
There 's little doubt	-69_NS-Elaboration
that such a move would be immediately challenged in court	-1_NS-Elaboration
-- and that it would quickly make its way to the Supreme Court	-1_NS-Elaboration
to be ultimately resolved .	-1_NS-Enablement
and they would n't want to leave it at a lower level , ''	-5_NS-Explanation
and they would n't want to leave it at a lower level , ''	-1_SN-Explanation
says Stephen Glazier , a New York attorney	-1_NS-Attribution
whose writings have been instrumental in pushing the idea	-1_NS-Elaboration
that a president already has a line-item veto .	-1_NS-Elaboration
Rep. Edwards , the California Democrat , is one	-9_NS-Explanation
that he would immediately challenge Mr. Bush in the courts ,	-2_NS-Elaboration
that he would immediately challenge Mr. Bush in the courts ,	-1_SN-Attribution
a line-item veto would expand a president 's powers far beyond anything	-2_NS-Elaboration
a line-item veto would expand a president 's powers far beyond anything	-1_SN-Attribution
the framers of the Constitution had in mind .	-1_NS-Elaboration
`` It puts this president in the legislative business , ''	-6_NS-Elaboration
he declares .	-1_NS-Attribution
`` That 's not what our fathers had in mind . ''	-2_NS-Evaluation
In addition to giving a president powers	-3_NS-Elaboration
to rewrite spending bills	-1_NS-Elaboration
meant to be written in Congress ,	-1_NS-Elaboration
Rep. Edwards argues ,	-3_NS-Attribution
a line-item veto would allow the chief executive to blackmail lawmakers .	-4_NN-Joint
that , as a lawmaker from the San Francisco area , he fights each year	-1_SN-Attribution
to preserve federal funds for the Bay Area Rapid Transit system .	-1_NS-Enablement
and wanted to force him to support a controversial foreign-policy initiative ,	-1_NN-Joint
Rep. Edwards says ,	-2_NS-Attribution
the president could call	-7_NS-Elaboration
the president could call	-5_SN-Background
the president could call	-3_SN-Background
that he would single-handedly kill the BART funds	-2_NN-Temporal
that he would single-handedly kill the BART funds	-1_SN-Attribution
unless the congressman `` shapes up '' on the foreign-policy issue .	-1_NS-Condition
that a president would choose to use a line-item veto more judiciously than that .	-25_NS-Contrast
that a president would choose to use a line-item veto more judiciously than that .	-1_SN-Attribution
But there may be another problem with the device :	-35_NS-Elaboration
it would cause ,	-1_NS-Elaboration
it might n't be effective in cutting the deficit .	-3_NS-Elaboration
it might n't be effective in cutting the deficit .	-2_SN-Contrast
Big chunks of the government budget , like the entitlement programs of Social Security and Medicare , would n't be affected .	-1_NS-Elaboration
that they have to use the device sparingly	-6_NS-Elaboration
that they have to use the device sparingly	-1_SN-Attribution
to maintain political comity .	-1_NS-Enablement
And it is n't even clear that some pork-barrel projects can be hit with a line-item veto	-8_NS-Elaboration
because they tend to be listed in informal conference reports	-1_NS-Explanation
accompanying spending bills rather than in the official bills themselves .	-1_NS-Elaboration
that the veto would have what Mr. Glazier calls an important `` chilling effect '' on all manner of appropriations bills .	-12_NN-Contrast
that the veto would have what Mr. Glazier calls an important `` chilling effect '' on all manner of appropriations bills .	-1_SN-Attribution
Lawmakers ,	-1_NS-Elaboration
they say ,	-1_NS-Attribution
would avoid putting many spending projects into legislation in the first place for fear of the embarrassment	-2_NN-Same-unit
of having them singled out for a line-item veto later .	-1_NS-Elaboration
Whatever the outcome of a test case , President Bush would have to move cautiously	-52_NS-Elaboration
becase the very attempt would `` antagonize not just Democrats but Republicans , ''	-1_NS-Explanation
says Louis Fisher , a scholar at the Congressional Research Service	-2_NS-Attribution
who specializes in executive-legislative relations .	-1_NS-Elaboration
Republicans have as much interest as Democrats in `` the way	-4_NS-Elaboration
the system works , ''	-1_NS-Elaboration
he notes .	-2_NS-Attribution
some ,	-8_NS-Explanation
some ,	-1_SN-Contrast
ranging from liberal Oregon Sen. Mark Hatfield to conservative Rep. Edwards	-1_NS-Elaboration
are opposed .	-2_NN-Same-unit
Rep. Edwards voices the traditional conservative view	-3_NS-Elaboration
that it 's a mistake to put too much power in the hands of a single person .	-1_NS-Elaboration
Conservatives	-2_NS-Elaboration
pushing for a line-item veto now ,	-1_NS-Elaboration
he notes ,	-2_NS-Attribution
may regret it later :	-3_NN-Same-unit
`` Sometime , you 're going to have a Democratic president again ''	-4_NS-Explanation
who 'll use his expanded powers against those very same conservatives .	-1_NS-Elaboration
`` Every order , resolution , or vote	-140_NN-Textual-organization
to which the concurrence of the Senate and House of Representatives may be necessary	-1_NS-Elaboration
( except on a question of adjournment )	-1_NS-Elaboration
shall be presented to the President of the United States ;	-3_NN-Same-unit
and	-4_NN-Joint
shall be approved by him ,	-2_NN-Same-unit
shall be approved by him ,	-1_SN-Temporal
or	-3_NN-Joint
shall be repassed by two-thirds of the Senate and House of Representatives ,	-2_NN-Same-unit
shall be repassed by two-thirds of the Senate and House of Representatives ,	-1_SN-Background
according to the rules and limitations	-10_NS-Attribution
prescribed in the case of a bill . ''	-1_NS-Elaboration
-- The Constitution , Article I , Section 7 , Clause 3	-12_NN-Textual-organization
