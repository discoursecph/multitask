Troubled Saatchi & Saatchi Co. has attracted offers for some of its advertising units , with potential suitors including Interpublic Group , but has rejected them , people familiar with the company said .	O	9..209
Industry executives said Interpublic approached Saatchi in August about buying its Campbell-Mithun-Esty unit , but was turned down by Chairman Maurice Saatchi .	O	212..370
More recently , Interpublic inquired about one of Saatchi 's smaller communications companies -- identified as the Rowland public relations firm by several industry executives -- but again was rebuffed , they said .	B-entrel	371..582
Interpublic 's chairman and chief executive officer , Philip Geier Jr. , made the pitches in visits to Mr. Saatchi in London , the executives said .	I-entrel	583..726
A Saatchi spokesman declined to comment about Interpublic .	O	729..787
But the spokesman confirmed that Saatchi has received several inquiries from companies interested in acquiring its Campbell-Mithun and Rowland units .	O	788..937
He added , " We have no intention of selling either business . "	O	938..998
Interpublic declined comment .	O	1001..1030
The offers come as Saatchi is struggling through the most troubled period in its 19-year history .	B-expansion	1033..1130
Takeover speculation has been rife , its consulting business is on the block , and its largest shareholder , Southeastern Asset Management , has said it 's been approached by third parties regarding a possible restructuring .	B-expansion	1131..1350
Analysts have continually lowered their earnings estimates for the company , and their outlook , at least for the short term , is bleak .	I-expansion	1351..1484
In the midst of the current turmoil , Saatchi is attempting to shore up its ad businesses .	B-expansion	1487..1576
It named a new chief executive officer , former IMS International head Robert Louis-Dreyfus .	B-temporal	1577..1668
It rebuffed an offer by Carl Spielvogel , head of Saatchi 's Backer Spielvogel Bates unit , to lead a management buy-out of all or part of Saatchi .	B-expansion	1669..1813
And last week , people close to Saatchi said Maurice Saatchi and his brother , Charles , would lead a buy-out if a hostile bid emerged .	I-expansion	1814..1946
But Saatchi 's troubles have only whipped up interest among outsiders interested in picking off pieces of its ad businesses .	B-expansion	1949..2072
While Saatchi 's major agency networks -- Backer Spielvogel and Saatchi & Saatchi Advertising -- would be difficult for any ad firm to buy because of potential client conflicts , its smaller businesses are quite attractive .	B-expansion	2073..2294
Campbell-Mithun-Esty , for example , has had big problems at its New York office , but offers strong offices in other areas of the country , including Minneapolis and Chicago .	B-contingency	2297..2468
That would would make it appealing to a network such as Interpublic that already has a healthy New York presence .	B-entrel	2469..2582
( While there would be some client conflicts , they would n't be nearly as onerous as with Saatchi 's other agencies . )	I-entrel	2583..2697
Campbell-Mithun also would be a sizable addition to an agency network : It has billings of about $ 850 million and blue-chip clients including General Mills , Jeep/Eagle and Dow Brands .	O	2698..2880
Rowland , meanwhile , has expanded aggressively , and now ranks as the fifth-largest U.S. public relations firm , according to O'Dwyer's Directory of Public Relations Firms .	B-contingency	2883..3052
It would be attractive to an agency such as Interpublic , one of the few big agency groups without an affiliated public relations firm of its own .	B-entrel	3053..3198
Other Saatchi units include ad agency McCaffrey & McCall , which has the Mercedes account and which has been attempting to buy itself back ; and Howard Marlboro , a sports and event marketing firm .	I-entrel	3199..3393
Despite Saatchi 's firm stand against selling its ad units , U.S. analysts believe the company may ultimately sell some of the smaller units .	O	3396..3535
Mr. Louis-Dreyfus , in a recent interview , said he might sell " a marginal agency or office . "	O	3536..3627
Analysts believe he may ultimately dispose of some of the non-advertising businesses .	O	3628..3713
Prudential 's Final Four	O	3716..3739
Prudential Insurance Co. of America said it selected four agencies to pitch its $ 60 million to $ 70 million account .	O	3742..3857
In addition to Backer Spielvogel Bates , a Saatchi unit that has handled the account since 1970 , the other agencies include Lowe Marschalk , a unit of the Lowe Group ; Grey Advertising ; and WPP Group 's Scali , McCabe , Sloves agency .	B-entrel	3860..4088
All agencies are New York-based .	I-entrel	4089..4121
A spokesman for the insurance and financial services firm , based in Newark , N.J. , said it hopes to make a decision within three to four months .	O	4124..4267
Jamaica Fires Back	O	4270..4288
The Jamaica Tourist Board , in the wake of Young & Rubicam 's indictment on charges that it bribed Jamaican officials to win the account in 1981 , released a scathing memo blaming the agency for the embarrassing incident .	O	4291..4509
The memo attempts to remove the tourist board as far as possible from the agency , which pleaded innocent to the charges .	O	4512..4632
Among other things , the memo contends that Young & Rubicam gave false assurances that the investigation would n't uncover any information that would " embarrass the government of Jamaica or the Jamaica Tourist Board . "	B-expansion	4633..4848
It also contends that Young & Rubicam never told the tourist board about its relationship with Ad Ventures , a Jamaican firm hired by the agency .	B-temporal	4849..4993
The U.S. indictment charges Ad Ventures was a front used to funnel kickbacks to the then-minister of tourism .	I-temporal	4994..5103
The memo also chastises the agency for the timing of its announcement Thursday that it would no longer handle the $ 5 million to $ 6 million account .	B-entrel	5106..5253
The agency declined comment , but said it will continue work until a new agency is chosen .	I-entrel	5254..5343
Ad Notes ... .	O	5346..5359
NEW ACCOUNT	O	5362..5373
: American Suzuki Motor Corp. , Brea , Calif. , awarded its estimated $ 10 million to $ 30 million account to Asher/Gould , Los Angeles .	B-entrel	5373..5503
: American Suzuki Motor Corp. , Brea , Calif. , awarded its estimated $ 10 million to $ 30 million account to Asher/Gould , Los Angeles .	B-expansion	5373..5503
Also participating in the finals was Los Angeles agency Hakuhodo Advertising America .	B-comparison	5504..5589
American Suzuki 's previous agency , Keye/Donna/Pearlstein , did n't participate .	I-comparison	5590..5667
AYER TALKS :	O	5670..5681
N W Ayer 's president and chief executive officer , Jerry J. Siano , said the agency is holding " conversations " about acquiring Zwiren Collins Karo & Trusk , a midsized Chicago agency , but a deal is n't yet close to being completed .	O	5682..5909
WHO 'S NEWS :	O	5912..5923
John Wells , 47 , former president and chief executive of N W Ayer 's Chicago office , was named management director and director of account services at WPP Group 's J. Walter Thompson agency in Chicago ... .	O	5924..6126
Shelly Lazarus , 42 , was named president and chief operating officer of Ogilvy & Mather Direct , the direct mail division of WPP Group 's Ogilvy & Mather agency .	O	6127..6285
