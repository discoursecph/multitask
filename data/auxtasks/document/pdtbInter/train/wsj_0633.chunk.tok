These are the last words Abbie Hoffman ever uttered , more or less , before he killed himself .	B-conjunction	9..101
And You Are There , sort of :	I-conjunction	102..129
ABBIE : " I 'm OK , Jack .	O	132..153
I 'm OK . "	O	154..162
( listening ) " Yeah .	O	163..181
I 'm out of bed .	B-entrel	182..197
I got my feet on the floor .	B-entrel	198..225
Yeah .	B-entrel	226..231
Two feet .	B-entrel	232..241
I 'll see you Wednesday ? ... Thursday . "	I-entrel	242..280
He listens impassively .	O	283..306
ABBIE ( cont'd. ) : " I 'll always be with you , Jack .	O	309..357
Do n't worry . "	O	358..371
Abbie lies back and leaves the frame empty .	O	374..417
Of course that was n't the actual conversation the late anti-war activist , protest leader and founder of the Yippies ever had with his brother .	O	420..562
It 's a script pieced together from interviews by CBS News for a re-enactment , a dramatic rendering by an actor of Mr. Hoffman 's ultimately unsuccessful struggle with depression .	O	563..740
The segment is soon to be broadcast on the CBS News series " Saturday Night With Connie Chung , " thus further blurring the distinction between fiction and reality in TV news .	O	743..915
It is the New Journalism come to television .	O	916..960
Ms. Chung 's program is just one of several network shows ( and many more in syndication ) that rely on the controversial technique of reconstructing events , using actors who are supposed to resemble real people , living and dead .	O	963..1189
Ms. Chung 's , however , is said to be the only network news program in history to employ casting directors .	O	1190..1295
Abbie Hoffman in this case is to be played by Hollywood actor Paul Lieber , who is n't new to the character .	O	1298..1404
He was Mr. Hoffman in a 1979 Los Angeles production of a play called " The Chicago Conspiracy Trial . "	O	1405..1505
Television news , of course , has always been part show-biz .	B-cause	1508..1566
Broadcasters have a healthy appreciation of the role entertainment values play in captivating an audience .	I-cause	1567..1673
But , as CBS Broadcast Group president Howard Stringer puts it , the network now needs to " broaden the horizons of nonfiction television , and that includes some experimentation . "	O	1674..1850
Since its premiere Sept. 16 , the show on which Ms. Chung appears has used an actor to portray the Rev. Vernon Johns , a civil-rights leader , and one to play a teenage drug dealer .	O	1853..2031
It has depicted the bombing of Pan Am flight 103 over the Scottish town of Lockerbie .	O	2032..2117
On Oct. 21 , it did a rendition of the kidnapping and imprisonment of Associated Press correspondent Terry Anderson , who was abducted in March 1985 and is believed to be held in Lebanon .	B-restatement	2118..2303
The production had actors playing Mr. Anderson and former hostages David Jacobsen , the Rev. Benjamin Weir and Father Lawrence Jenco .	B-conjunction	2304..2436
ABC News has similarly branched out into entertainment gimmickry .	B-instantiation	2439..2504
" Prime Time Live , " a new show this season featuring Sam Donaldson and Diane Sawyer , has a studio audience that applauds and that one night ( to the embarrassment of the network ) waved at the camera like the crowd on " Let 's Make a Deal . "	I-instantiation	2505..2740
( ABC stops short of using an " applause " sign and a comic to warm up the audience .	B-alternative	2741..2822
The stars do that themselves . )	I-alternative	2823..2853
NBC News has produced three episodes of an occasional series produced by Sid Feders called " Yesterday , Today and Tomorrow , " starring Maria Shriver , Chuck Scarborough and Mary Alice Williams , that also gives work to actors .	O	2854..3076
Call it a fad .	O	3079..3093
Or call it the wave of the future .	O	3094..3128
NBC 's re-creations are produced by Cosgrove-Meurer Productions , which also makes the successful prime-time NBC Entertainment series " Unsolved Mysteries . "	O	3131..3284
The marriage of news and theater , if not exactly inevitable , has been consummated nonetheless .	B-entrel	3287..3381
News programs , particularly if they score well in the ratings , appeal to the networks ' cost-conscious corporate parents because they are so much less expensive to produce than an entertainment show is -- somewhere between $ 400,000 and $ 500,000 for a one-hour program .	B-contrast	3382..3649
Entertainment shows tend to cost twice that .	I-contrast	3650..3694
Re-enactments have been used successfully for several seasons on such syndicated " tabloid TV " shows as " A Current Affair , " which is produced by the Fox Broadcasting Co. unit of Rupert Murdoch 's News Corp .	O	3695..3899
That show , whose host is Ms. Chung 's husband , Maury Povich , has a particular penchant for grisly murders and stories having to do with sex -- the Robert Chambers murder case , the Rob Lowe tapes , what have you .	O	3900..4109
Gerald Stone , the executive producer of " A Current Affair , " says , " We have opened eyes to being a little less conservative and more imaginative in how to present the news . "	O	4112..4284
Nowhere have eyes been opened wider than at CBS News .	O	4287..4340
At 555 W. 57th St. in Manhattan , one floor below the offices of " 60 Minutes , " the most successful prime-time news program ever , actors wait in the reception area to audition for " Saturday Night With Connie Chung . "	O	4341..4554
CBS News sends scripts to agents , who pass them along to clients .	B-entrel	4557..4622
The network deals a lot with unknowns , including Scott Wentworth , who portrayed Mr. Anderson , and Bill Alton as Father Jenco , but the network has some big names to contend with , too .	B-instantiation	4623..4805
James Earl Jones is cast to play the Rev. Mr. Johns .	B-list	4806..4858
Ned Beatty may portray former California Gov. Pat Brown in a forthcoming epsiode on Caryl Chessman , the last man to be executed in California , in 1960 .	I-list	4859..5010
" Saturday Night " has cast actors to appear in future stories ranging from the abortion rights of teen-agers to a Nov. 4 segment on a man named Willie Bosket , who calls himself a " monster " and is reputed to be the toughest prisoner in New York .	O	5013..5256
CBS News , which as recently as two years ago fired hundreds of its employees in budget cutbacks , now hires featured actors beginning at $ 2,700 a week .	O	5259..5409
That is n't much compared with what Bill Cosby makes , or even Connie Chung for that matter ( who is paid $ 1.6 million a year and who recently did a guest shot of her own on the sitcom " Murphy Brown " ) .	O	5410..5608
But the money is n't peanuts either , particularly for a news program .	O	5609..5677
CBS News is also re-enacting the 1979 Three Mile Island nuclear accident in Middletown , Pa. , with something less than a cast of thousands .	O	5680..5818
It is combing the town of 10,000 for about 200 extras .	O	5819..5873
On Oct. 20 , the town 's mayor , Robert Reid , made an announcement on behalf of CBS during half-time at the Middletown High School football game asking for volunteers .	O	5874..6038
" There was a roll of laughter through the stands , " says Joe Sukle , the editor of the weekly Press and Journal in Middletown .	O	6041..6165
" They 're filming right now at the bank down the street , and they want shots of people getting out of cars and kids on skateboards .	O	6166..6296
They are approaching everyone on the street and asking if they want to be in a docudrama . "	O	6297..6387
Mr. Sukle says he would n't dream of participating himself :	O	6390..6448
" No way .	O	6449..6457
I think re-enactments stink . "	O	6458..6487
Though a re-enactment may have the flavor , Hollywood on the Hudson it is n't .	B-conjunction	6490..6566
Some producers seem tentative about the technique , squeamish even .	I-conjunction	6567..6633
So the results , while not news , are n't exactly theater either , at least not good theater .	B-conjunction	6634..6723
And some people do think that acting out scripts is n't worthy of CBS News , which once lent prestige to the network and set standards for the industry .	I-conjunction	6724..6874
In his review of " Saturday Night With Connie Chung , " Tom Shales , the TV critic of the Washington Post and generally an admirer of CBS , wrote that while the show is " impressive , ... one has to wonder if this is the proper direction for a network news division to take . "	O	6877..7145
Re-creating events has , in general , upset news traditionalists , including former CBS News President Richard S. Salant and former NBC News President Reuven Frank , former CBS News anchorman Walter Cronkite and the new dean of the Columbia University Graduate School of Journalism , Joan Konner .	B-instantiation	7148..7439
Says she : " Once you add dramatizations , it 's no longer news , it 's drama , and that has no place on a network news broadcast ... .	I-instantiation	7440..7567
They should never be on .	O	7568..7592
Never . "	O	7593..7600
Criticism of the Abbie Hoffman segment is particularly scathing among people who knew and loved the man .	B-instantiation	7603..7707
That includes his companion of 15 years , Johanna Lawrenson , as well as his former wife , Anita .	I-instantiation	7708..7802
Both women say they also find it distasteful that CBS News is apparently concentrating on Mr. Hoffman 's problems as a manic-depressive .	O	7803..7938
" This is dangerous and misrepresents Abbie 's life , " says Ms. Lawrenson , who has had an advance look at the 36-page script .	O	7941..8063
" It 's a sensational piece about someone who is not here to defend himself . "	O	8064..8139
Mrs. Hoffman says that dramatization " makes the truth flexible .	O	8142..8205
It takes one person 's account and gives it authenticity . "	O	8206..8263
CBS News interviewed Jack Hoffman and his sister , Phyllis , as well as Mr. Hoffman 's landlord in Solebury Township , Pa .	O	8266..8384
Also Jonathan Silvers , who collaborated with Mr. Hoffman on two books .	O	8385..8455
Mr. Silvers says , " I wanted to be interviewed to get Abbie 's story out , and maybe talking about the illness will do some good . "	O	8458..8585
The executive producer of " Saturday Night With Connie Chung , " Andrew Lack , declines to discuss re-creactions as a practice or his show , in particular .	O	8588..8738
" I do n't talk about my work , " he says .	O	8739..8777
The president of CBS News , David W. Burke , did n't return numerous telephone calls .	O	8778..8860
One person close to the process says it would not be in the best interest of CBS News to comment on a " work in progress , " such as the Hoffman re-creation , but says CBS News is " aware " of the concerns of Ms. Lawrenson and Mr. Hoffman 's former wife .	O	8863..9110
Neither woman was invited by CBS News to participate in a round-table discussion about Mr. Hoffman that is to follow the re-enactment .	O	9111..9245
Mr. Lieber , the actor who plays Mr. Hoffman , says he was concerned at first that the script would " misrepresent an astute political mind , one that I admired , " but that his concerns were allayed .	O	9248..9442
The producers , he says , did a good job of depicting someone " who had done so much , but who was also a manic-depressive .	O	9443..9562
