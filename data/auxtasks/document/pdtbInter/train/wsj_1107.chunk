This is in response to George Melloan's Business World column "The Housing Market Is a Bigger Mess Than You Think" (op-ed page, Sept. 26).	O	9..147
In Houston, we have seen how bad the housing problem can become.	B-restatement	150..214
Unused houses deteriorate rapidly, affecting the value of nearby homes; in a domino effect, the entire neighborhood can fall victim.	I-restatement	215..347
At this stage some people just "walk away" from homes where the mortgage exceeds current market value.	O	348..450
But most of them could have afforded to keep up their payments -- they chose not to do so.	O	451..541
The problem is so vast that we need to try innovative solutions -- in small-scale experiments.	B-cause	544..638
Here are some ideas:	I-cause	639..659
1) Foreclosed homes could be sold by the FHA for no down payment (the biggest obstacle to young buyers), but with personal liability for the mortgage (no walking away by choice).	O	662..840
2) Encourage long-term occupancy by forgiving one month's payment (off the tail end of the mortgage) for every six months paid; or perhaps have the down payment deferred to the end of the mortgage (balloon), but "forgiven" on a monthly pro-rata basis as long as the owner remains the occupant.	O	843..1136
3) Develop rental agreements with exclusive purchase options for the renter.	O	1139..1215
An occupant will, in most every case, be better for the home and neighborhood than a vacant house.	B-conjunction	1216..1314
In this way, the house is not dumped on to a glutted market.	I-conjunction	1315..1375
John F. Merrill	O	1378..1393
Houston	O	1396..1403
The Federal Housing Administration, Veterans Administration and the Department of Housing and Urban Development further aggravate the problem of affordable housing stock by "buying in" to their foreclosed properties (of which there are, alas, many) at an inflated "balance due" -- say $80,000 on a house worth $55,000 -- instead of allowing a free market to price the house for what it's really worth.	O	1406..1807
Worse, the properties then sit around deteriorating for maybe a year or so, but are resold eventually (because of the attractiveness of the low down payment, etc.) to a marginal buyer who can't afford both the mortgage and needed repairs; and having little vested interest that buyer will walk away and the vicious cycle repeats itself all over again.	O	1810..2161
Paul Padget	O	2164..2175
