Congress sent to President Bush an $ 8.5 billion military construction bill	(NS-elaboration-additional(NS-circumstance(NS-elaboration-object-attribute
that cuts spending for new installations by 16 %	(NS-circumstance
while revamping the Pentagon budget	(NS-purpose
to move more than $ 450 million from foreign bases to home-state projects .	NS-purpose)))
The fiscal 1990 measure builds on a pattern	(NS-elaboration-additional(NN-List(NS-elaboration-object-attribute
set earlier this year by House and Senate defense authorizing committees ,	NS-elaboration-object-attribute)
and	(NN-Same-Unit
-- at a time of retrenchment for the military and concern about the U.S. 's standing in the world economy --	(SN-circumstance
overseas spending is most vulnerable .	SN-circumstance)))
Total Pentagon requests for installations in West Germany , Japan , South Korea , the United Kingdom and the Philippines , for example , are cut by almost two-thirds ,	(NS-elaboration-additional(NN-Cause-Result(NS-elaboration-additional
while lawmakers added to the military budget for construction in all but a dozen states at home .	NS-elaboration-additional)
The result is that instead of the Pentagon 's proposed split of 60-40 between domestic and foreign bases , the reduced funding is distributed by a ratio of approximately 70-30 .	NN-Cause-Result)
The extra margin for bases in the U.S. enhances the power of the appropriations committees ;	(NN-List
meanwhile , lawmakers used their positions	(NS-example(NS-purpose
to garner as much	(NN-Comparison
as six times what the Pentagon had requested for their individual states .	NN-Comparison))
House Appropriations Committee Chairman Jamie Whitten	(NN-List(NN-Same-Unit(NS-elaboration-additional
( D. , Miss . )	NS-elaboration-additional)
helped secure $ 49.7 million for his state , or more than double the Pentagon 's budget .	NN-Same-Unit)
West Virginia , home of Senate Appropriations Committee Chairman Robert Byrd , would receive $ 21.5 million	(NN-List(NS-elaboration-additional
-- four times the military 's request .	NS-elaboration-additional)
Tennessee and North Carolina , home states of the two Democratic chairmen of the House and Senate military construction subcommittees , receive $ 243.2 million , or 25 % above the Pentagon 's request .	(NN-List
Though spending for Iowa and Oregon was far less ,	(SN-concession
their increases above Pentagon requests	(NS-result(NN-Same-Unit(NS-elaboration-general-specific
-- 640 % and 430 % , respectively --	NS-elaboration-general-specific)
were much greater	NN-Same-Unit)
because of the influence of Republicans at critical junctures .	NS-result))))))))))
The swift passage of the bill ,	(NS-elaboration-additional(NN-Same-Unit(NS-elaboration-additional
which cleared the Senate and House on simple voice votes last week ,	NS-elaboration-additional)
contrasts with the problems	(NS-elaboration-object-attribute
still facing a more cumbersome $ 66.8 billion measure	(NS-elaboration-object-attribute
funding housing , environmental , space and veterans programs .	NS-elaboration-object-attribute)))
By an 84-6 margin , the Senate approved the bulk of the spending Friday ,	(NS-elaboration-additional(SN-concession
but the bill was then sent back to the House	(NS-purpose
to resolve the question	(NS-elaboration-object-attribute
of how to address budget limits on credit allocations for the Federal Housing Administration .	NS-elaboration-object-attribute)))
The House Democratic leadership could seek to waive these restrictions ,	(NS-elaboration-additional(NS-explanation-argumentative(NN-Contrast
but the underlying bill is already under attack for excesses elsewhere .	NN-Contrast)
Appropriations committees have used an assortment of devices	(NS-elaboration-additional(NN-Cause-Result(NS-elaboration-object-attribute
to disguise as much as $ 1 billion in spending ,	NS-elaboration-object-attribute)
and	(NN-Same-Unit
as critics have awakened to these devices ,	(SN-circumstance
the bill can seem like a wounded caribou	(NS-elaboration-object-attribute
trying to make it past ice and wolves	(NS-purpose
to reach safer winter grazing .	NS-purpose)))))
Much of the excess spending will be pushed into fiscal 1991 ,	(NS-example(NN-List
and in some cases is temporarily parked in slow-spending accounts in anticipation	(NS-elaboration-object-attribute
of being transferred to faster-spending areas	(NS-circumstance
after the budget scorekeeping is completed .	NS-circumstance)))
For example , a House-Senate conference ostensibly increased the National Aeronautics and Space Administration budget for construction of facilities to nearly $ 592 million , or more than $ 200 million above what either chamber had previously approved .	(NS-elaboration-part-whole
Part of the increase would provide $ 90 million toward ensuring construction of a costly solid rocket-motor facility in Mr. Whitten 's Mississippi .	(NN-Contrast
But as much as $ 177 million , or nearly 30 % of the account , is marked for potential transfers to research , management and flight accounts	(NS-elaboration-additional
that are spent out at a faster clip .	NS-elaboration-additional))))))
The bill 's managers face criticism , too , for the unusual number of conditions	(NS-example(NS-elaboration-additional(NS-elaboration-object-attribute
openly imposed on where funds will be spent .	NS-elaboration-object-attribute)
Conservatives ,	(NS-example(NN-Same-Unit
embarrassed by Republican influence-peddling scandals at the Department of Housing and Urban Development ,	(SN-circumstance
have used the issue in an effort to shift blame onto a Democratic-controlled Congress .	SN-circumstance))
HUD Secretary Jack Kemp backed an unsuccessful effort	(NS-antithesis(NN-Same-Unit(NS-elaboration-object-attribute
to strike such language	NS-elaboration-object-attribute)
last week ,	NN-Same-Unit)
but received little support from the White House budget office ,	(NS-elaboration-object-attribute
which wants to protect space-station funding in the bill	(NN-List
and has tended to turn its eyes from pork-barrel amendments .	NN-List)))))
Within discretionary funds for community development grants , more than $ 3.7 million is allocated to six projects in Michigan , home state of a subcommittee chairman , Rep. Bob Traxler .	(NN-List
House Speaker Thomas Foley won $ 510,000 for a project in his district in Washington state ,	(NN-List
and $ 1.3 million ,	(NS-elaboration-additional(NN-Same-Unit(NS-elaboration-additional
earmarked by Sen. Daniel Inouye ,	NS-elaboration-additional)
amounts to a business subsidy under the title `` Hawaiian sugar mills job retention . ''	NN-Same-Unit)
The powerful Democrat had first wanted to add language	(NS-elaboration-additional(NS-elaboration-object-attribute
relaxing environmental restrictions on two mills on the Hamakua coast	(NS-elaboration-additional
that are threatening to close .	NS-elaboration-additional))
When this plan met resistance ,	(NS-attribution(SN-reason
it was agreed instead to take money from HUD	(NS-purpose
to subsidize needed improvements in two settling ponds for the mills ,	(NS-elaboration-additional
which employ an estimated 1,500 workers ,	NS-elaboration-additional)))
according to Mr. Inouye 's office .	NS-attribution))))))))))
