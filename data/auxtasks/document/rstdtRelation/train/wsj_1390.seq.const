Maidenform Inc. loves to be intimate with its customers , but not with the rest of the public .	(Summary
The 67-year-old maker of brassieres , panties , and lingerie enjoys one of the best-known brand images ,	(Background(Elaboration(Elaboration(Elaboration(Elaboration(Contrast
but its financial profile is closely guarded by members of the founding family .	Contrast)
`` There are very few companies	(Elaboration(Elaboration(Attribution(Elaboration
that can boast of such a close-knit group , ''	Elaboration)
says Robert A. Brawer , 52 years old ,	(Elaboration(Elaboration
recently named president ,	Elaboration)
succeeding Beatrice Coleman , his mother-in-law ,	(Elaboration
who remains chairman .	Elaboration)))
`` We are a vanishing breed , ''	(Attribution
he muses .	Attribution))
Mrs. Coleman , 73 ,	(Elaboration(Same-unit(Elaboration
who declined to be interviewed ,	Elaboration)
is the Maidenform strategist .	Same-unit)
Sales have tripled during her 21-year tenure to about $ 200 million in 1988 .	(Elaboration
Maidenform says	(Contrast(Attribution
it is very profitable	Attribution)
but declines to provide specifics .	Contrast)))))
The company sells image .	(Elaboration(Elaboration(Elaboration(Elaboration
Its current ad campaign ,	(Same-unit(Elaboration
on which Maidenform has spent more than $ 15 million since fall 1987 ,	Elaboration)
does n't even show its underwear products , but rather men like Christopher Reeve , star of the `` Superman '' movies ,	(Elaboration
talking about their lingerie turn-ons .	Elaboration)))
The Maidenform name `` is part of American pop culture , ''	(Elaboration(Attribution
says Joan Sinopoli , account supervisor of the campaign by Levine , Huntley , Schmidt & Beaver , a New York ad firm .	Attribution)
Maidenform generated such memorable campaigns as `` I dreamed	(Same-unit(Attribution
I . . . in my Maidenform bra , ''	Attribution)
and `` The Maidenform woman .	(Elaboration
You never know	(Attribution
where she 'll turn up . ''	Attribution)))))
`` Capitalizing on the brand is key , ''	(Attribution
says Mr. Brawer ,	(Elaboration
whose immediate plans include further international expansion and getting better control of distribution outside the U.S .	Elaboration)))
`` The intimate apparel industry is perceived to be a growth industry	(Elaboration(Attribution(Background
and clearly { Maidenform } is a force	(Elaboration
to be reckoned with , ''	Elaboration))
says David S. Leibowitz , a special situations analyst at American Securities Corp. in New York .	Attribution)
Although working women are `` forced to wear the uniform of the day ,	(Attribution(Contrast(Enablement
to retain their femininity	Enablement)
they are buying better quality , more upscale intimate apparel , ''	Contrast)
he said .	Attribution))))
Although Mr. Brawer 's appointment as president was long expected ,	(Explanation(Contrast(Explanation(Elaboration(Temporal(Contrast
the move on Sept. 25 precipitated the resignation of Alan Lesk as senior vice president of sales and merchandising .	Contrast)
Three days later , Mr. Lesk was named president and chief executive officer of Olga Co. , a competing intimate apparel division of Warnaco Inc .	(Elaboration
Warnaco also owns Warners , another major intimate apparel maker .	Elaboration))
Mr. Lesk could n't be reached to comment .	Elaboration)
But Maidenform officials say	(Attribution
that	(Same-unit
after spending 24 years at Maidenform ,	(Background
Mr. Lesk , 48 , made it clear	(Attribution
he wanted the top job .	Attribution)))))
`` If you want the presidency of the company ,	(Elaboration(Elaboration(Attribution(Condition
this is n't the firm	(Elaboration
to work for , ''	Elaboration))
says James Mogan , 45 ,	(Elaboration
who was named senior vice president of sales ,	(Elaboration
assuming some of the responsibilities of Mr. Lesk .	Elaboration)))
The company downplayed the loss of Mr. Lesk	(Joint
and split his merchandising responsibilities among a committee of four people .	Joint))
`` My style is less informal , ''	(Attribution
Mr. Brawer says .	Attribution)))
Top officers insist	(Elaboration(Elaboration(Elaboration(Elaboration(Attribution
Maidenform 's greatest strength is its family ownership .	Attribution)
`` You ca n't go anywhere in this company	(Attribution(Background
and find an organizational chart , ''	Background)
one delights .	Attribution))
`` It is fun competing as a private company , ''	(Comparison(Explanation(Attribution
Mr. Brawer says .	Attribution)
`` You can think long range . ''	Explanation)
Other major players in intimate apparel apparently feel the same way .	(Elaboration
Warnaco was taken private by Spectrum Group in 1986 for about $ 487 million .	(Joint
And last year , Playtex Holdings Inc. went private for about $ 680 million .	(Joint(Temporal
It was then split into Playtex Apparel Inc. , the intimate apparel division , and Playtex Family Products Corp. ,	(Elaboration
which makes tampons , hair-care items and other products .	Elaboration))
Publicly traded VF Corp. ,	(Same-unit(Elaboration
which owns Vanity Fair ,	Elaboration)
and Sara Lee Corp. ,	(Same-unit(Elaboration
which owns Bali Co. ,	Elaboration)
are also strong forces in intimate apparel .	Same-unit)))))))
Buy-out offers for Maidenform are n't infrequent ,	(Condition(Explanation(Contrast(Attribution
says Executive Vice President David C. Masket ,	Attribution)
but they are n't taken very seriously .	Contrast)
When he gets calls ,	(Attribution(Background
`` I do n't even have to consult '' with Mrs. Coleman ,	Background)
Mr. Masket says .	Attribution))
The company could command a good price in the market .	(Explanation
`` Over the past three and a half years , apparel companies ,	(Attribution(Same-unit(Elaboration
many of whom have strong brand names ,	Elaboration)
have been bought at about 60 % of sales , ''	Same-unit)
says Deborah Bronston , Prudential-Bache Securities Inc. apparel analyst .	Attribution))))
Mr. Brawer , along with Mrs. Coleman and her daughter , Elizabeth , an attorney	(Background(Elaboration(Elaboration(Elaboration(Elaboration(Same-unit(Elaboration
who is vice chairman ,	Elaboration)
are the family members	(Elaboration
involved in the operations of Maidenform ,	(Elaboration
which employs about 5,000 .	Elaboration)))
Mr. Brawer 's wife , Catherine , and Robert Stroup , Elizabeth 's husband , round out the five-member board .	Elaboration)
Each has an equal vote at the monthly meetings .	Elaboration)
`` We are all very amiable , ''	(Attribution
Mr. Brawer says .	Attribution))
Executives say	(Attribution
Mrs. Coleman is very involved in the day-to-day operations , especially product development .	Attribution))
In the late 1960s she designed a lightweight stretch bra	(Joint(Elaboration
that boosted sales .	Elaboration)
Her father , William Rosenthal , designed the then-dress making company 's first bra in the 1920s ,	(Joint(Elaboration
which	(Same-unit(Attribution
he said	Attribution)
gave women a `` maiden form ''	(Comparison
compared with the `` boyish form ''	(Elaboration
they got from the `` flat bandages ''	(Elaboration
used for support at the time .	Elaboration)))))
While Mr. Rosenthal introduced new undergarment designs ,	(Joint(Comparison
his wife , Ida , concentrated on sales and other financial matters .	Comparison)
The name Maidenform was coined by a third business partner , Enid Bissett .	Joint)))))))
The company has 14 plants and distribution facilities in the U.S. , Puerto Rico , other parts of the Caribbean and Ireland .	(Joint
Maidenform products are mainly sold at department stores ,	(Elaboration
but the company has quietly opened a retail store of its own in Omaha , Neb .	(Joint
, and has 24 factory outlets , with plans	(Elaboration
to add more .	Elaboration)))))
Before joining Maidenform in 1972 ,	(Elaboration(Elaboration(Temporal
Mr. Brawer ,	(Same-unit(Elaboration
who holds a doctoral degree in English from the University of Chicago ,	Elaboration)
taught at the University of Wisconsin .	Same-unit))
As a senior vice president , he has headed the company 's designer lingerie division , Oscar de la Renta , since its inception in 1988 .	(Elaboration
To maintain exclusivity of that designer line ,	(Enablement
it is n't labeled with the Maidenform name .	Enablement)))
While the company has always been family-run ,	(Elaboration(Background
Mr. Brawer is n't the first person	(Elaboration
to marry into the family	(Temporal
and subsequently head Maidenform .	Temporal)))
Mrs. Coleman 's husband , Joseph , a physician , succeeded Mrs. Rosenthal as president	(Joint
and served in that post until his death in 1968 .	Joint)))))
