Crude prices spurted upward in brisk trading on the assumption that heavy earthquake damage occurred to San Francisco area refinery complexes , but the rise quickly fizzled when it became apparent that oil operations were n't severely curtailed .	O	9..252
Trading on little specific information , market players overnight in Tokyo began bidding up oil prices .	O	255..357
The rally spread into European markets , where traders were still betting that the earthquake disrupted the San Francisco area 's large oil refining plants .	O	358..512
By yesterday morning , much of the world was still unable to reach San Francisco by telephone .	B-entrel	515..608
West Texas Intermediate was bid up more than 20 cents a barrel in many overseas markets .	I-entrel	609..697
At the opening of the New York Mercantile Exchange , West Texas Intermediate for November delivery shot up 10 cents a barrel , to $ 20.85 , still on the belief that the refineries were damaged .	O	698..887
In the San Francisco area , roughly 800,000 barrels a day of crude , about a third of all the refining capacity in California , is processed daily , according to industry data .	O	890..1062
For more than the past year , even the rumor of a major West Coast refinery shutdown has been enough to spark a futures rally because the gasoline market is so tight .	B-contrast	1063..1228
But yesterday , as the morning wore on , some major West Coast refinery operators -- including Chevron Corp. , Exxon Corp. and the Shell Oil Co. unit of Royal Dutch/Shell Group -- said their refineries were n't damaged and were continuing to operate normally .	I-contrast	1231..1486
Most said they shut down their petroleum pipeline operations as a precaution but did n't see any immediate damage .	O	1487..1600
Gasoline terminals were also largely unhurt , they said .	O	1601..1656
" It 's hard to imagine how the markets were speculating , given that nobody could get through to San Francisco , " said one amazed oil company executive .	O	1659..1808
As the news spread that the refineries were intact , crude prices plunged , ending the day at $ 20.56 a barrel , down 19 cents .	B-conjunction	1811..1934
Gasoline for November delivery was off 1.26 cents a gallon to 54.58 cents .	B-conjunction	1935..2009
Heating oil finished at 60.6 cents , down 0.45 cent .	I-conjunction	2010..2061
" The market was basically acting on two contradictory forces , " said Nauman Barakat of Shearson Lehman Hutton Inc .	O	2064..2177
" One is the panic , the earthquake in San Francisco , which is positive . "	O	2178..2249
But once that factor was eliminated , traders took profits and focused on crude oil inventories , Mr. Barakat said .	O	2252..2365
After the market closed Tuesday , the American Petroleum Institute had reported that crude stocks increased by 5.7 million barrels in the week ended Friday , which traders viewed as bearish .	O	2366..2554
But some market players still think earthquake speculation could have more impact on the oil markets .	O	2557..2658
" The problem is that while on the surface everything is all right , the question is , " said Mr. Barakat , " was there any structural damage to the pipelines or anything else . "	O	2659..2830
In other commodity markets yesterday :	O	2833..2870
COPPER :	O	2873..2880
Futures prices eased on indications of improvement in the industry 's labor situation .	B-instantiation	2881..2966
The December contract declined 1.85 cents a pound to $ 1.2645 .	B-cause	2967..3028
According to one analyst , workers at the Cananea copper mine in Mexico , which has n't been operating since it was declared bankrupt by the Mexican government in late August , are set to return to work .	I-cause	3029..3228
The analyst said it will take about two to three months before the mine begins to produce copper in significant quantities .	O	3229..3352
He added that , while there has n't been any official announcement as yet , the Highland Valley mine strike in British Columbia , which has lasted more than three months , is regarded as settled .	O	3353..3543
Another analyst said the Cananea return to operation may not be as near as some expect .	O	3544..3631
" There are still negotiations taking place on whether there will be a loss of jobs , which has been a critical issue all along , " he said .	O	3632..3768
Nevertheless , the increasing likelihood that these two major supply disruptions will be resolved weighed on the market , the analysts agreed .	O	3769..3909
Both of these mines are normally major suppliers of copper to Japan , which has been buying copper on the world market .	O	3910..4028
The first analyst said that the Japanese , as well as the Chinese , bought copper earlier in the week in London , but that this purchasing has since slackened as the supply situation , at least over the long term , appears to have improved .	B-entrel	4029..4264
" The focus for some time has been on the copper supply , and good demand has been taken for granted , " he said .	I-entrel	4265..4374
" Now that the supply situation seems to be improving , it would be best for traders to switch their concentration to the demand side . "	B-entrel	4375..4508
He noted the Commerce Department report yesterday that housing starts in September dropped 5.2 % from August to 1.26 million units on an annualized basis , the lowest level in seven years .	I-entrel	4509..4695
" Along with these factors , other economic reports suggest a slowing of the economy , which could mean reduced copper usage , " he said .	O	4696..4828
SUGAR :	O	4831..4837
Futures prices extended Tuesday 's gains .	B-cause	4838..4878
The March delivery ended with an advance of 0.16 cent a pound to 14.27 cents , for a two-day gain of 0.3 cent .	B-entrel	4879..4988
According to one dealer , Japan said it has only 40,000 tons of sugar remaining to be shipped to it this year by Cuba under current commitments .	B-cause	4989..5132
The announcement was made because of reports Tuesday that Cuba would delay shipments to Japan scheduled for later this year , into early next year .	I-cause	5133..5279
The dealer said the quantity mentioned in the Japanese announcement is so small that it 's meaningless .	B-entrel	5280..5382
One analyst said he thought the market continued to be supported to some degree by a delay in the Cuban sugar harvest caused by adverse weather .	B-contrast	5383..5527
The dealer said India might be the real factor that is keeping futures prices firm .	I-contrast	5528..5611
That country recently bought 200,000 tons of sugar and had been expected to seek a like quantity last week but did n't .	B-entrel	5612..5730
" It 's known they need the sugar , and the expectation that they will come in is apparently giving the market its principal support , " the dealer said .	I-entrel	5731..5879
LIVESTOCK AND MEATS :	O	5882..5902
The Agriculture Department is expected to announce tomorrow that the number of cattle in the 13 major ranch states slipped 4 % to 8.21 million on Oct. 1 compared with the level a year earlier , said Tom Morgan , president of Sterling Research Corp. , Arlington Heights , Ill .	O	5903..6173
Cattle prices have risen in recent weeks on speculation that the government 's quarterly report will signal tighter supplies of beef .	B-conjunction	6174..6306
Among other things , the government is expected to report that the number of young cattle placed on feedlots during the quarter slipped 3 % .	B-entrel	6307..6445
Feedlots fatten cattle for slaughter , so a drop indicates that the production of beef will dip this winter .	B-restatement	6446..6553
Indeed , some analysts expect the government to report that the movement of young cattle onto feedlots in the month of September in seven big ranch states dropped 8 % compared with the level for September 1988 .	I-restatement	6554..6762
