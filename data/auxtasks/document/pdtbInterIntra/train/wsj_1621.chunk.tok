Inland Steel Industries Inc. , battered by lower volume and higher costs , posted a 75 % drop in third-quarter earnings .	root	9..126
The nation 's fourth-largest steelmaker earned $ 18.3 million , or 43 cents a share , compared with $ 61 million , or $ 1.70 a share , a year earlier , when the industry was enjoying peak demand and strong pricing .	norel	129..334
Sales fell to $ 981.2 million from $ 1.02 billion .	conjunction	335..383
The earnings also mark a significant drop from the second quarter 's $ 45.3 million or $ 1.25 a share .	norel	386..485
Moreover , the earnings were well below analysts ' expectations of about $ 1.16 a share .	conjunction	486..571
In composite trading on the New York Stock Exchange , Inland closed yesterday at $ 35.875 a share , down $ 1 .	norel	574..679
The company attributed the earnings drop to lower volume related to seasonal demand and the soft consumer durable market , especially in the automotive sector .	norel	682..840
However , the company also lost orders because of prolonged labor talks in the second quarter .	contrast	841..934
Third-quarter shipments slipped 7 % from the year-ago period , and 17 % from this year 's second quarter .	restatement	935..1036
Profit of steel shipped for the company 's steel segment slid to $ 26 a ton , from $ 66 a ton a year earlier and $ 57 a ton a quarter earlier .	norel	1039..1176
Analysts noted that the disappointing results do n't reflect lower prices for steel products .	norel	1179..1271
Charles Bradford , an analyst with Merrill Lynch Capital Markets , said higher prices for galvanized and cold-rolled products offset lower prices for bar , hot-rolled and structural steel .	cause	1272..1457
Structural steel , which primarily serves the construction market , was especially hurt by a 15 % price drop , Mr. Bradford said .	conjunction	1458..1583
The company said its integrated steel sector was also hurt by higher raw material , repair and maintenance , and labor costs .	norel	1586..1709
The increased labor costs became effective Aug. 1 under terms of the four-year labor agreement with the United Steelworkers union .	restatement	1710..1840
Meanwhile , the company 's service center segment , which saw operating profit drop to $ 11.5 million from $ 30.7 million a year ago , experienced much of the same demand and cost problems , as well as start-up costs associated with a coil processing facility in Chicago and an upgraded computer information system .	norel	1841..2149
Inland Chairman Frank W. Luerssen said the company 's short-term outlook is " clouded by uncertainties in the economy and financial markets . "	norel	2152..2291
However , he noted that steel mill bookings are up from early summer levels , and that he expects the company to improve its cost performance in the fourth quarter .	contrast	2292..2454
In the first nine months , profit was $ 113 million , or $ 3.04 a share , on sales of $ 3.19 billion , compared with $ 204.5 million , or $ 5.76 a share , on sales of $ 3.03 billion , a year earlier .	norel	2457..2643
