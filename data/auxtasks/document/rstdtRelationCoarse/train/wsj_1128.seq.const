Soichiro Honda 's picture now hangs with Henry Ford 's in the U.S. Automotive Hall of Fame ,	(Summary-Evaluation-Topic(Summary-Evaluation-Topic(Comparison(Temporal(Joint
and the game-show `` Jeopardy '' is soon to be Sony-owned .	Joint)
But no matter how much Japan gets under our skin ,	(Comparison
we 'll still have mom and apple pie .	Comparison))
On second thought , make that just mom .	Comparison)
A Japanese apple	(Elaboration(Cause(Temporal(Elaboration(Textual-organization(Elaboration
called the Fuji	Elaboration)
is cropping up in orchards the way	(Elaboration
Hondas did on U.S. roads .	Elaboration))
By 1995 it will be planted more often than any other apple tree ,	(Attribution
according to a recent survey of six apple-industry sages by Washington State University horticulturist Robert Norton .	Attribution))
Some fruit visionaries say	(Attribution
the Fuji could someday tumble the Red Delicious from the top of America 's apple heap .	Attribution))
It certainly wo n't get there on looks .	(Joint(Comparison(Elaboration(Cause
Compared to the Red Delicious , the exemplar of apple pulchritude ,	(Elaboration(Comparison
the Fuji is decidedly more dowdy	Comparison)
-- generally smaller , less-perfectly shaped , greenish , with tinges of red .	Elaboration))
To hear most U.S. growers tell it ,	(Attribution
we 'd still be in Paradise	(Cause
if the serpent had proffered one to Eve .	Cause)))
But how sweet it is .	(Cause
It has more sugar `` than any apple	(Attribution(Elaboration
we 've ever tested , ''	Elaboration)
says Duane Greene , a University of Massachusetts pomologist , or apple scholar .	Attribution)))
It has a long shelf life	(Elaboration(Attribution(Joint
and `` does n't fool the public , ''	Joint)
says Grady Auvil , an Orondo , Wash. , grower	(Elaboration
who is planting Fujis	(Joint
and spreading the good word about them .	Joint)))
`` It does n't look nice on the outside	(Temporal
while getting mealy inside . ''	Temporal))))
Mr. Auvil , razor sharp at 83 , has picked and packed a zillion pecks of apples over the past 65 years .	(Temporal(Elaboration
He is known as the father of the U.S.-grown Granny Smith , a radically different apple	(Elaboration(Elaboration
that	(Textual-organization(Attribution
the conventional wisdom once said	Attribution)
would never catch on .	Textual-organization))
It did ,	(Elaboration
shaking the apple establishment to its roots .	Elaboration)))
Now , even more radical changes seem afoot	(Elaboration(Temporal
as the grand old maverick of American apples plays the role of Hiroshi Appleseed .	Temporal)
`` The Fuji is going to be No. 1	(Attribution(Cause
to replace the Red Delicious , ''	Cause)
he says .	Attribution)))))
The Delicious hegemony wo n't end anytime soon .	(Comparison(Elaboration(Temporal
New apple trees grow slowly ,	(Elaboration(Elaboration(Joint
and the Red Delicious is almost as entrenched as mom .	Joint)
Its roots are patriotic	(Summary-Evaluation-Topic(Cause
-- with the first trees appearing in 1872 in an orchard near Peru , Iowa ,	Cause)
to be exact .	Summary-Evaluation-Topic))
For more than 50 years , it has been the apple of our eye .	(Summary-Evaluation-Topic
A good Delicious can indeed be delicious .	Summary-Evaluation-Topic)))
More than twice as many Red Delicious apples are grown as the Golden variety , America 's No. 2 apple .	Elaboration)
But the apple industry is ripe for change .	(Elaboration(Elaboration(Elaboration(Elaboration(Cause
`` Red Delicious has been overplanted ,	(Elaboration(Attribution(Cause
and its prices have dropped below the cost of production , ''	Cause)
says Washington State 's Mr. Norton .	Attribution)
The scare over Alar , a growth regulator	(Elaboration(Textual-organization(Elaboration
that makes apples redder and crunchier	(Comparison
but may be carcinogenic ,	Comparison))
made consumers shy away from the Delicious ,	(Comparison
though they were less affected than the McIntosh .	Comparison))
The glut and consequent lower prices ,	(Textual-organization(Elaboration
combined with cancer fears ,	Elaboration)
was a very serious blow to growers .	Textual-organization))))
`` A lot of growers wo n't be around in a few years , ''	(Elaboration(Comparison(Attribution
says Mr. Norton ,	Attribution)
although they have stopped using Alar .	Comparison)
One may be William Broderick , a Sterling , Mass. , grower .	(Elaboration(Elaboration
`` This is beautiful stuff , ''	(Comparison(Attribution
he says ,	(Elaboration
looking ruefully at big boxes of just-picked Red Delicious next to his barn .	Elaboration))
`` But I 'm going to lose $ 50,000 to $ 60,000 on it .	(Cause
I 'm going to have to get another job this year	(Cause
just to eat . ''	Cause))))
Besides rotten prices , he has been hit recently by hail , a bark-nibbling horde of mice , fungi and bugs .	(Elaboration(Elaboration
Some 500 insects and 150 diseases wiggle , chew and romp through growers ' nightmares ,	(Elaboration
including maggots , mites , mildew , thrips , black rot and the flat-headed borer .	Elaboration))
Even if a grower beats them back ,	(Attribution(Comparison(Comparison
his $ 2,000 rented bees might buzz off to the neighbors ' orchards	Comparison)
instead of pollinating his ,	Comparison)
Mr. Broderick says .	Attribution)))))
Though growers ca n't always keep the worm from the apple ,	(Elaboration(Comparison
they can protect themselves against the price vagaries of any one variety	(Joint
by diversifying	(Elaboration
-- into the recently imported Gala , a sweet New Zealand native ; the Esopus Spitzenburg , reportedly Thomas Jefferson 's favorite apple ; disease-resistant kinds like the Liberty .	Elaboration)))
`` I 've ripped out a lot of Delicious ''	(Elaboration(Attribution(Joint
and grafted the trees with many different shoots ,	Joint)
says Steve Wood , a West Lebanon , N.H. , grower ,	(Elaboration
tramping through his 100-acre Poverty Lane Orchard on a crisp autumn day recently .	Elaboration))
`` I 've got 70 kinds of apples .	(Elaboration
Here 's a Waltana , ''	(Elaboration(Attribution
he exclaims ,	(Elaboration
picking one off a tree .	Elaboration))
He bites it ,	(Summary-Evaluation-Topic(Temporal
scowls	(Temporal
and throws it down .	Temporal))
`` It 's a real dog . ''	Summary-Evaluation-Topic))))))
Supermarkets are getting into the variety act , too .	(Elaboration
They still buy apples mainly for big , red good looks	(Comparison(Cause
-- that 's why so many taste like woodchucks ' punching bags .	Cause)
But freshness counts	(Elaboration(Joint(Comparison
more than it once did ,	Comparison)
and stores are expanding shelf space for unconventional , but tastier , and often pricier , apples .	Joint)
`` Rather than sell 39-cents-a-pound Delicious ,	(Attribution(Comparison
maybe we can sell 79-cents-a-pound Fujis , ''	Comparison)
says Chuck Tryon , perishables director for Super Valu Inc. , a Minneapolis supermarket chain and food distributor .	Attribution)))))
The Fuji is a product of meticulous Japanese pomological engineering ,	(Comparison(Elaboration(Temporal(Elaboration(Elaboration
which fostered it 50 years ago at a government research orchard .	Elaboration)
Japanese researchers have bred dozens of strains of Fujis	(Cause(Cause
to hone its color , taste and shelf life .	Cause)
Now the best of them age as gracefully as Grannies , the industry 's gold standard for storability .	Cause))
In the cornucopia of go-go apples , the Fuji 's track record stands out :	(Cause
During the past 15 years , it has gone from almost zilch to some 50 % of Japan 's market .	Cause))
`` The Japanese apple market is very keyed to high quality , ''	(Comparison(Cause(Attribution
says David Lane , a scientist at a Canadian horticulture research center in Summerland , British Columbia ,	Attribution)
and so apples are more of a delicacy there than a big food commodity .	Cause)
The U.S. Department of Agriculture estimates	(Attribution
that this year Americans will eat about 40 % more fresh apples per capita than the Japanese .	Attribution)))
The Fuji is still small potatoes in the U.S. ,	(Comparison(Elaboration
sold mainly in fruit boutiques .	Elaboration)
But in California ,	(Joint(Textual-organization(Attribution
says Craig Ito , a Fuji-apple grower ,	Attribution)
`` There 's a Fuji apple cult .	(Elaboration
Once somebody eats one ,	(Temporal
they get hooked . ''	Temporal)))
Mr. Auvil , the Washington grower , says	(Elaboration(Elaboration(Comparison(Elaboration(Elaboration(Attribution
that he could sell Fujis to Taiwan buyers at $ 40 a box	(Cause
if he had them .	Cause))
( Taiwan already is a big importer of Fujis from other places ,	(Attribution
he adds . )	Attribution))
But his first crop wo n't be picked till next year .	(Cause
`` I expect to see the demand exceed supply for Fujis for the next 10 to 15 years , ''	(Attribution
he adds .	Attribution)))
Washington Red Delicious , by the way , are wholesaling for less than $ 10 a box these days .	Comparison)
Mr. Auvil sees Fujis , in part ,	(Elaboration(Elaboration(Joint
as striking a blow against the perversion of U.S. apples by supermarkets .	Joint)
`` When the chain stores took over ,	(Elaboration(Temporal
there was no longer a connection between grower and consumer .	Temporal)
A guy is sitting up in an office	(Elaboration
deciding	(Attribution
what you 're going to eat . ''	Attribution))))
After all , until the 1950s even the Red Delicious was a firm , delectable morsel .	(Summary-Evaluation-Topic(Temporal(Temporal
Then ,	(Textual-organization
as growers bred them more for looks ,	(Cause(Joint
and to satisfy supermarket chains ' demands of long-term storage ,	Joint)
the Red went into decline .	Cause)))
Now , those red applelike things	(Joint(Textual-organization(Elaboration
stores sell in summer	Elaboration)
are fruitbowl lovely ,	Textual-organization)
but usually not good eating .	Joint))
They do deserve respect , however	(Cause
-- they are almost a year old , probably equal to about 106 in human years .	Cause))))
The Fuji ,	(Summary-Evaluation-Topic(Comparison(Cause(Textual-organization(Summary-Evaluation-Topic
to be sure ,	Summary-Evaluation-Topic)
has blemishes too .	Textual-organization)
It ripens later than most apples ,	(Joint
and growing it in U.S. areas with chilly autumns may be tricky .	(Joint
Moreover , the frumpy Fuji must compete with an increasingly dolledup Delicious .	(Elaboration
Mr. Broderick , the Massachusetts grower , says	(Elaboration(Attribution
the `` big boss '' at a supermarket chain even rejected his Red Delicious recently	(Cause
because they were n't waxed and brushed for extra shine .	Cause))
And he had n't used hormones ,	(Elaboration
which many growers employ	(Cause
to elongate their Delicious apples for greater eye appeal .	Cause)))))))
Still ,	(Summary-Evaluation-Topic(Textual-organization(Attribution
Mr. Auvil points out ,	Attribution)
Grannies became popular	(Temporal
without big , red looks ,	Temporal))
so why not Fujis ?	Summary-Evaluation-Topic))
He sees a shift in American values	(Elaboration(Textual-organization(Elaboration
-- at least regarding apples --	Elaboration)
toward more emphasis on substance and less on glitz .	Textual-organization)
`` Taste has finally come to the fore , ''	(Elaboration(Attribution
he says .	Attribution)
Or , for that matter , the core .	Elaboration))))))))))
