Rockwell International Corp. reported flat operating earnings for the fourth quarter ended Sept. 30 .	B-expansion	9..109
The aerospace , automotive supply , electronics and printing-press concern also indicated that the first half of fiscal 1990 could be rough .	I-expansion	112..250
In an interview , Donald Beall , chairman , said first-half profit certainly would trail the past year 's , primarily because of weakness in the heavy-truck and passenger-car markets .	O	253..431
Still , he added , if the industrial sector remains relatively stable , Rockwell should be able to recover in the second half and about equal fiscal 1989 's operating profit of $ 630.9 million .	O	432..620
For fiscal 1989 's fourth quarter , Rockwell 's net income totaled $ 126.1 million , or 50 cents a share .	B-comparison	623..723
That compares with operating earnings of $ 132.9 million , or 49 cents a share , the year earlier .	I-comparison	724..819
The prior-year period includes a one-time favorable tax adjustment on the B-1B bomber program and another gain from sale of the industrial sewing-machine business , which made net $ 185.9 million , or 70 cents a share .	B-expansion	822..1037
Sales rose 4 % to $ 3.28 billion from $ 3.16 billion .	I-expansion	1038..1088
Mr. Beall said that he was generally pleased with the latest numbers and cited a particularly strong showing by the company 's electronics segment .	O	1091..1237
Overall , pretax electronics earnings soared 12 % to $ 107.9 million from $ 96.4 million .	B-entrel	1240..1325
All four areas had higher revenue for the three months ended Sept. 30 .	I-entrel	1326..1396
For the year , electronics emerged as Rockwell 's largest sector in terms of sales and earnings , muscling out aerospace for the first time .	O	1399..1536
The graphics business , which also was singled out by the chairman as a positive , saw its operating earnings for the quarter jump 79 % to $ 42.1 million from $ 23.5 million .	O	1539..1708
For the year , bolstered by the introduction of the Colorliner newspaper-printing press , graphics earnings almost doubled .	O	1709..1830
Aerospace earnings sagged 37 % for the quarter and 15 % for the year , largely due to lower B-1B program profit ; the last of the bombers rolled out in April 1988 .	O	1833..1992
That was partially offset by the resumption of space shuttle flights and increased demand for expendable launch-vehicle engines .	O	1993..2121
The company also took hits in the fourth quarters of 1989 and 1988 on a fixed-price weapons-modernization development program -- probably the C-130 gunship , according to analysts .	O	2124..2303
For fiscal 1989 , the company posted net of $ 734.9 million , or $ 2.87 a share , down from $ 811.9 million , or $ 3.04 a share , in fiscal 1988 .	B-comparison	2306..2442
Excluding one-time additions to profit in each year , earnings per share were $ 2.47 , up 7.4 % from $ 2.30 in fiscal 1988 .	B-expansion	2443..2561
Sales for the year rose 5 % to $ 12.52 billion from $ 11.95 billion in fiscal 1988 .	I-expansion	2562..2642
