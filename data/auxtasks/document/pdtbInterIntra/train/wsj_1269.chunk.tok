Remember those bulky , thick-walled refrigerators of 30 years ago ?	root	9..74
They , or at least something less efficient than today 's thin-walled units , may soon be making a comeback .	entrel	75..180
That something , whatever it is , could add as much as $ 100 to the $ 600 or so consumers now pay for lower-priced refrigerators .	conjunction	181..306
These and other expensive changes in products ranging from auto air conditioners to foam cushioning to commercial solvents are in prospect because of something called the Montreal Protocol , signed by 24 nations in 1987 .	norel	309..528
In one of the most sweeping environmental regulatory efforts to date -- involving products with an annual value of $ 135 billion in the U.S. alone -- the signatories agreed to curtail sharply the use of chlorofluorocarbons ( CFCs ) .	synchrony	529..758
World-wide production would be cut in half by 1998 .	cause	759..810
The U.S. Senate liked the treaty so well it ratified it by a vote of 89 to 0 .	entrel	811..888
Not to be outdone , George Bush wants CFCs banished altogether by the year 2000 , a goal endorsed at an 80-nation U.N. environmental meeting in Helsinki in the spring .	conjunction	889..1054
That 's a lot of banishment , as it turns out .	norel	1057..1101
CFCs are the primary ingredient in a gas , often referred to by the Du Pont trade name Freon , which is compressed to liquid form to serve as the cooling agent in refrigeration and air-conditioning equipment .	cause	1102..1308
Gases containing CFCs are pumped into polyurethane to make the foam used in pillows , upholstery and insulation .	conjunction	1309..1420
Polyurethane foam is a highly efficient insulator , which accounts for why the walls of refrigerators and freezers can be thinner now than they were back in the days when they were insulated with glass fiber .	conjunction	1421..1628
But even though by some estimates it might cost the world as much as $ 100 billion between now and the year 2000 to convert to other coolants , foaming agents and solvents and to redesign equipment for these less efficient substitutes , the Montreal Protocol 's legions of supporters say it is worth it .	norel	1631..1930
They insist that CFCs are damaging the earth 's stratospheric ozone layer , which screens out some of the sun 's ultraviolet rays .	cause	1931..2058
Hence , as they see it , if something is n't done earthlings will become ever more subject to sunburn and skin cancer .	cause	2059..2174
Peter Teagan , a specialist in heat transfer , is running a project at Arthur D. Little Inc. , of Cambridge , Mass. , to find alternative technologies that will allow industry to eliminate CFCs .	norel	2177..2366
In addition to his interest in ozone depletion he has extensively studied the related topic of global warming , a theory that mankind 's generation of carbon dioxide through increased combustion of fossil fuels is creating a " greenhouse effect " that will work important climatic changes in the earth 's atmosphere over time .	entrel	2367..2688
" I would be the first to admit that there is not a complete consensus in the scientific community on either one of these problems , " says Mr. Teagan .	norel	2691..2839
" In the kind of literature I read I come across countervailing opinions quite frequently .	restatement	2840..2929
But the nature of the problem is such that many others feel it has to be addressed soon , before all the evidence is in .	contrast	2930..3049
We ca n't afford to wait . "	restatement	3050..3075
But does it have to be so soon ?	contrast	3078..3109
Some atmospheric scientists think that even if CFCs were released into the atmosphere at an accelerating rate , the amount of ozone depletion would be only 10 % by the middle of the next century .	cause	3110..3303
It 's easy to get something comparable by simply moving to a higher altitude in the U.S.	conjunction	3304..3391
Moreover , there are questions , particularly among atmospheric scientists who know this subject best , about the ability of anyone to know what in fact is happening to the ozone layer .	norel	3394..3576
It is generally agreed that when CFCs rise from earth to stratosphere , the chlorine in them is capable of interfering with the process through which ultraviolet rays split oxygen molecules and form ozone .	entrel	3577..3781
But ozone creation is a very large-scale natural process	contrast	3782..3838
and the importance of human-generated CFCs in reducing it is largely a matter of conjecture .	conjunction	3839..3931
The ozone layer is constantly in motion	cause	3932..3971
and thus very hard to measure .	cause	3972..4002
What scientists have known since the late 1970s is that there is a hole in the layer over Antarctica that expands or contracts from year to year .	concession	4003..4148
But it is at least worthy of some note that there are very few refrigerators in Antarctica .	concession	4149..4240
Moreover , surely someone has noticed that household refrigerators are closed systems , running for many years without either the CFC gas or the insulation ever escaping .	conjunction	4241..4409
Another argument of the environmentalists is that if substitutes are available , why not use them ?	norel	4412..4509
Mr. Teagan cites a list of substitutes	concession	4510..4548
but none , so far , match the nonflammable , nontoxic CFCs .	concession	4549..4605
Butane and propane can be used as coolants , for example , but are flammable .	instantiation	4606..4681
Moreover , new lubricants will be needed to protect compressors from the new formulations , which , as with CFCs , are solvents .	norel	4682..4806
Mr. Teagan points out as well that if the equipment designed to get along without CFCs is less efficient than current devices , energy consumption will rise and that will worsen the greenhouse effect .	conjunction	4807..5006
Folks in the Midwest who just suffered a mid-October snowstorm may wonder where the greenhouse was when they needed it	entrel	5007..5125
but let 's not be flippant about grave risks .	contrast	5126..5171
As it happens , Arthur D. Little is not at all interested in throwing cold water on ozone depletion and global warming theories .	norel	5174..5301
It is interested in making some money advising industry on how to convert to a world without CFCs .	alternative	5302..5400
There is , after all , big money in environmentalism .	cause	5401..5452
Maybe we should ask why it was that Du Pont so quickly capitulated and issued a statement , giving it wide publicity , that it was withdrawing CFCs .	norel	5455..5601
Freon , introduced in 1930 , revolutionized America by making refrigeration and air conditioning practical after all .	cause	5602..5717
One answer is that big companies are growing weary of fighting environmental movements and are trying instead to cash in on them , although they never care to put it quite that way .	norel	5718..5898
Du Pont , as it happens , has a potential substitute for CFCs .	instantiation	5899..5959
Imperial Chemical Industries of the U.K. also has one , and is building a plant in Louisiana to produce it .	conjunction	5960..6066
Japanese chemical companies are at work developing their own substitutes and hoping to conquer new markets , of course .	conjunction	6067..6185
There are still others who do n't mind seeing new crises arise .	norel	6188..6250
Environmental groups would soon go out of business were they not able to send out mailings describing the latest threat and asking for money to fight it .	instantiation	6251..6404
University professors and consultants with scientific credentials saw a huge market for their services evaporate when price decontrol destroyed the energy crisis and thus the demand for " alternative energy . "	conjunction	6405..6612
They needed new crises to generate new grants and contracts .	cause	6613..6673
In other words , environmentalism has created a whole set of vested interests that fare better when there are many problems than when there are few .	restatement	6676..6823
That tends to tilt the public debate toward " solutions	cause	6824..6878
even when some of the most knowledgeable scientists are skeptical about the seriousness of the threats and the insistence of urgency .	concession	6879..7013
There is an element of make-work involved .	cause	7014..7056
Consumers pay the bill for all this in the price of a refrigerator or an air-conditioned car .	norel	7059..7152
If they were really getting insurance against environmental disaster , the price would be cheap .	entrel	7153..7248
But if there is no impending threat , it can get to be very expensive .	contrast	7249..7318
