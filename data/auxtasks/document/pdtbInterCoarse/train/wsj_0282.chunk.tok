Sun Myung Moon , the Korean evangelist-industrialist who in 1954 founded the Unification Church , remains the mystery man behind a multimillion-dollar political and publishing operation based in this country and catering to the American right .	B-comparison	9..250
But there may be less there than meets the eye .	I-comparison	251..298
Mr. Moon planned to convert millions of Americans to his unique brand of Christianity -- in which he plays the role of Old Testament-style temporal , political messiah -- and then to make the U.S. part of a unified international theocracy .	B-entrel	301..539
His original strategy ( in itself a brilliant innovation for spreading a religion ) was to create new economic enterprises each time he wanted to extend and fund his various religious missions .	B-contingency	540..731
Tax-exempt airport and street-corner solicitations were intended only to provide start-up funds .	B-comparison	732..828
More stable industries were to build an economically viable infrastructure for the Moon movement in North America , as they had in Japan and South Korea .	B-temporal	829..981
Then he would move his movement to Europe .	B-comparison	982..1024
But that was not to be .	I-comparison	1027..1050
Throughout the 1970s and early 1980s spokesmen for both the Unification Church and its opponents in the anticult movement gave wildly exaggerated membership figures .	B-entrel	1053..1218
Their legacy lives on .	B-expansion	1219..1241
It is still common to read in the press that the church has 10,000 or more full-time American members and 25,000 " associates . "	B-expansion	1242..1368
Some estimates have gone as high as 80,000 members .	I-expansion	1369..1420
But internal church documents clearly show that at its publicity-seeking heights , as when it organized a spectacular Yankee Stadium bicentennial rally in 1976 , there actually were only about 2,000 full-time Unification Church members in the U.S.	B-temporal	1423..1668
Mr. Moon 's support for a Watergate-beleaguered Richard Nixon , the Koreagate scandal , and his prison sentence for income-tax evasion did not help the church 's recruitment efforts .	B-expansion	1669..1847
Defections , burnouts , and abduction " deprogrammings " kept member turnover high .	B-contingency	1848..1927
That the membership number has even kept close to its 1976 size is the result of the " graying " of the church .	B-expansion	1928..2037
Many of the enthusiastic young " Moonies " of the Nixon era who remained faithful to Father Moon are now parents , producing new members by procreation rather than conversion .	I-expansion	2038..2210
The reputed wealth of the Unification Church is another matter of contention .	B-contingency	2213..2290
For a while in the 1970s it seemed Mr. Moon was on a spending spree , with such purchases as the former New Yorker Hotel and its adjacent Manhattan Center ; a fishing/processing conglomerate with branches in Alaska , Massachusetts , Virginia and Louisiana ; a former Christian Brothers monastery and the Seagram family mansion ( both picturesquely situated on the Hudson River ) ; shares in banks from Washington to Uruguay ; a motion picture production company , and newspapers , such as the Washington Times , the New York City Tribune ( originally the News World ) , and the successful Spanish-language Noticias del Mundo .	B-comparison	2291..2901
Yet these purchases can be misleading .	B-expansion	2904..2942
Most were obtained with huge inputs of church money from South Korea and Japan , minimum cash downpayments and sizable mortgages .	B-contingency	2943..3071
Those teams of young fund-raisers selling flowers , peanuts or begging outright at traffic intersections brought in somewhere near $ 20 million a year during the mid-to-late 1970s , but those revenues were a pittance compared to the costs of Mr. Moon 's lavish international conferences , speaking tours , and assorted land buys .	I-contingency	3072..3395
Only his factories in Japan and Korea , employing his followers at subsistence wages and producing everything from rifles to ginseng to expensive marble vases , kept the money flowing westward .	O	3396..3587
Virginia Commonwealth University sociologist David Bromley , who more than any other researcher has delved into the complex world of Unificationist finances , has concluded that profitable operations in the U.S. have been the exceptions rather than the rule .	O	3590..3846
Likewise , journalists John Burgess and Michael Isikoff of the Washington Post have estimated that at least $ 800 million was transferred from Japan to the U.S. to deal with the church 's annual operating losses in this country .	O	3847..4072
Mr. Moon 's two English-language U.S. newspapers illustrate the scope of this financial drain .	B-expansion	4075..4168
Start-up costs for the Washington Times alone were close to $ 50 million , and the total amount lost in this journalistic black hole was estimated at $ 150 million by 1984 .	I-expansion	4169..4338
Since then , Moon 's organization has inaugurated a pair of high-quality glossy opinion magazines , The World and I and Insight , which are a further drain .	O	4339..4491
Insiders say that not even their editors know for sure how much these show-piece publications , along with the newspapers , have cost Mr. Moon .	O	4492..4633
Many American church-owned businesses , such as a $ 30 million factory to build fishing vessels , are defunct .	B-expansion	4636..4743
Some components of the American church had their budgets cut in half last year and again this year .	B-expansion	4744..4843
The relatively small academic conferences that have attracted conservative guests -- and press scrutiny -- in recent years are much more narrowly targeted and austere than the thousand-person get-togethers in fancy digs and exotic locales of years past .	B-entrel	4844..5097
I attended several of these in the dual role as a presenter of research findings as well as an investigator of my hosts .	I-entrel	5098..5218
( Mr. Moon 's Paragon House eventually even published three of my co-edited books on religion and politics . )	O	5219..5325
According to veteran watchers of Unificationist affairs , such as Dr. J. Gordon Melton , director of the Institute for the Study of American Religion , almost all operations are being drastically reduced as Mr. Moon now concentrates more on developing his empire in the Far East .	O	5328..5604
" Everything , " one non-" Moonie " senior consultant to the Unification Church recently told me in an interview , " is going back to Korea and Japan . "	B-contingency	5605..5749
( Europe had proved even less hospitable than North America .	B-contingency	5750..5809
European politicians were less reluctant to have their governments investigate and harass new religions . )	I-contingency	5810..5915
So Mr. Moon is in retreat , refocusing on the Far East .	B-expansion	5918..5972
South Korea and Japan continue to be profitable .	B-expansion	5973..6021
Moon 's Tong'Il industry conglomerate is now investing heavily in China , where church accountants have high hopes of expanding and attracting converts even in the wake of the bloody massacre in Tiananmen Square .	B-expansion	6022..6232
Panda Motors is one such investment .	I-expansion	6233..6269
According to senior consultants to the church , Mr. Moon has successfully negotiated a joint venture with the Chinese government to build an automobile-parts plant in Guangdong Province , an area of China with a substantial Korean minority .	B-expansion	6272..6510
Mr. Moon has agreed to put up $ 10 million a year for 25 years and keep the profits in China .	B-contingency	6511..6603
In return , he has the government 's blessing to build churches and spread Unificationism in that country .	I-contingency	6604..6708
Whatever respectability and ties to intellectuals and opinion-makers the publications and conferences bring really are salvage -- not the Rev. Moon 's original final goals , but the ones for which he will have to settle .	O	6711..6929
Mr. Shupe is co-author ( with David G. Bromley ) of " ' Moonies ' in America : Cult , Church , and Crusade " and " Strange Gods : The Great American Cult Scare .	O	6932..7081
