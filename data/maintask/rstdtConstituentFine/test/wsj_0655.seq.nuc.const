Nicaraguan President Daniel Ortega may have accomplished over the weekend what his U.S. antagonists have failed to do :	(NS-summary(NS-elaboration-object-attribute
revive a constituency for the Contra rebels .	NS-elaboration-object-attribute)
Lawmakers have n't publicly raised the possibility	(NS-elaboration-additional(NS-elaboration-additional(NS-circumstance(NS-elaboration-additional(NS-elaboration-additional(NS-evidence(SN-antithesis(NN-List(NS-elaboration-object-attribute
of renewing military aid to the Contras ,	NS-elaboration-object-attribute)
and President Bush parried the question at a news conference here Saturday ,	(NS-elaboration-additional
saying only	(SN-attribution
that	(NN-Same-Unit
`` if there 's an all-out military offensive ,	(SN-condition
that 's going to change the equation 180 degrees . ''	SN-condition)))))
But Mr. Ortega 's threat over the weekend	(NN-Same-Unit(NS-elaboration-object-attribute
to end a 19-month cease-fire with the rebels	(NS-elaboration-object-attribute
seeking to topple him ,	NS-elaboration-object-attribute))
effectively elevated the Contras as a policy priority	(NS-temporal-same-time
just as they were slipping from the agendas of their most ardent supporters .	NS-temporal-same-time)))
Senate Majority Leader George Mitchell	(SN-attribution(NN-Same-Unit(NS-elaboration-additional
( D. , Maine )	NS-elaboration-additional)
said yesterday on NBC-TV 's `` Meet the Press ''	NN-Same-Unit)
that Mr. Ortega 's threat was `` a very unwise move , particularly the timing of it . ''	SN-attribution))
The threat came during a two-day celebration in Costa Rica	(NS-elaboration-object-attribute(NS-elaboration-object-attribute
to highlight Central America 's progress toward democracy in the region ,	NS-elaboration-object-attribute)
attended by President Bush , Canadian Prime Minister Brian Mulroney and 14 other Western Hemisphere leaders .	(NS-elaboration-additional
Mr. Bush returned to Washington Saturday night .	NS-elaboration-additional)))
Mr. Ortega announced on Friday	(SN-antithesis(SN-attribution
that he would end the cease-fire this week in response to the periodic Contra assaults against his army .	SN-attribution)
Saturday , he amended his remarks	(NS-elaboration-additional(NS-elaboration-object-attribute
to say	(SN-attribution
that he would continue to abide by the cease-fire	(NS-condition
if the U.S. ends its financial support for the Contras .	NS-condition)))
He asked	(SN-attribution
that the remaining U.S. humanitarian aid be diverted to disarming and demobilizing the rebels .	SN-attribution))))
Not only did Mr. Ortega 's comments come in the midst of what was intended as a showcase for the region ,	(NN-List
it came	(NS-elaboration-additional(NS-temporal-same-time
as Nicaragua is under special international scrutiny in anticipation of its planned February elections .	NS-temporal-same-time)
Outside observers are gathering in Nicaragua	(NS-purpose
to monitor the registration and treatment of opposition candidates .	NS-purpose))))
And important U.S. lawmakers must decide at the end of November	(SN-circumstance(NS-background(SN-attribution
if the Contras are to receive the rest of the $ 49 million in so-called humanitarian assistance under a bipartisan agreement	(NS-elaboration-object-attribute
reached with the Bush administration in March .	NS-elaboration-object-attribute))
The humanitarian assistance ,	(NN-Same-Unit(NS-elaboration-additional
which pays for supplies such as food and clothing for the rebels	(NS-elaboration-object-attribute
amassed along the Nicaraguan border with Honduras ,	NS-elaboration-object-attribute))
replaced the military aid	(NS-elaboration-object-attribute
cut off by Congress in February 1988 .	NS-elaboration-object-attribute)))
While few lawmakers anticipated	(NS-evidence(SN-antithesis(SN-attribution
that the humanitarian aid would be cut off next month ,	SN-attribution)
Mr. Ortega 's threat practically guarantees	(SN-attribution
that the humanitarian aid will be continued .	SN-attribution))
Senate Minority Leader Robert Dole	(NS-elaboration-additional(SN-attribution(NN-Same-Unit(NS-elaboration-additional
( R. , Kan . )	NS-elaboration-additional)
said yesterday on `` Meet the Press '' :	NN-Same-Unit)
`` I would hope	(SN-attribution
after his { Mr. Ortega 's } act yesterday or the day before , we 'd have unanimous support for quick action on remaining humanitarian aid . ''	SN-attribution))
Sen. Dole also said	(SN-attribution
he hoped for unanimous support for a resolution	(NS-elaboration-object-attribute(NS-elaboration-additional
he plans to offer tomorrow	NS-elaboration-additional)
denouncing the Nicaraguan leader .	NS-elaboration-object-attribute))))))
While renewing military aid had been considered out of the question ,	(NS-elaboration-additional(NS-antithesis(NS-elaboration-additional(SN-antithesis(NN-List
rejected by Congress	(NN-List
and de-emphasized by the Bush administration ,	NN-List))
Mr. Ortega 's statement provides Contra supporters with the opportunity	(NS-elaboration-object-attribute
to press the administration on the issue .	NS-elaboration-object-attribute))
`` The administration should now state	(NS-evaluation(NS-attribution(SN-attribution
that	(NN-Same-Unit
if the { February } election is voided by the Sandinistas . . .	(SN-condition
they should call for military aid , ''	SN-condition)))
said former Assistant Secretary of State Elliott Abrams .	NS-attribution)
`` In these circumstances , I think they 'd win . ''	NS-evaluation))
Sen. Mitchell said	(NS-antithesis(SN-attribution
that congressional Democrats intend to honor the March agreement	(NS-elaboration-object-attribute
to give non-lethal support to the Contras through the February elections ,	NS-elaboration-object-attribute))
although he added	(SN-attribution
that the agreement requires	(SN-attribution
that the Contras not initiate any military action .	SN-attribution))))
Mr. Ortega 's threat	(SN-circumstance(NN-Temporal-Same-Time(NN-Same-Unit(NS-elaboration-object-attribute
to breach the cease-fire	NS-elaboration-object-attribute)
comes	NN-Same-Unit)
as U.S. officials were acknowledging	(NS-example(SN-attribution
that the Contras have at times violated it themselves .	SN-attribution)
Secretary of State James Baker ,	(NN-List(SN-attribution(NN-Same-Unit(NS-elaboration-additional
who accompanied President Bush to Costa Rica ,	NS-elaboration-additional)
told reporters Friday :	NN-Same-Unit)
`` I have no reason	(NS-elaboration-object-attribute
to deny reports	(NS-elaboration-object-attribute
that some Contras ambushed some Sandinista soldiers . ''	NS-elaboration-object-attribute)))
Mr. Baker 's assistant for inter-American affairs , Bernard Aronson ,	(NS-elaboration-additional(SN-attribution(NN-Same-Unit(NS-elaboration-additional
while maintaining	(SN-attribution
that the Sandinistas had also broken the cease-fire ,	SN-attribution))
acknowledged :	NN-Same-Unit)
`` It 's never very clear who starts what . ''	SN-attribution)
He added	(SN-attribution
that the U.S. has cut off aid to some rebel units	(NS-circumstance
when it was determined that those units broke the cease-fire .	NS-circumstance))))))
In addition to undermining arguments in favor of ending Contra aid ,	(NS-example(NN-List
Mr. Ortega 's remarks also played to the suspicions of some U.S. officials and conservatives outside the government	(NS-elaboration-object-attribute
that he is searching for ways	(NS-elaboration-object-attribute
to manipulate or void the February elections .	NS-elaboration-object-attribute)))
Administration officials	(NS-example(NN-Same-Unit(NS-elaboration-additional
traveling with President Bush in Costa Rica	NS-elaboration-additional)
interpreted Mr. Ortega 's wavering as a sign	(NS-elaboration-object-attribute
that he is n't responding to the military attacks so much	(NS-comparison
as he is searching for ways	(NS-elaboration-object-attribute
to strengthen his hand prior to the elections .	NS-elaboration-object-attribute))))
Mr. Abrams said	(NS-elaboration-additional(SN-attribution
that Mr. Ortega is seeking to demobilize the Contras prior to the elections	(NS-purpose
to remove any pressure	(NS-elaboration-object-attribute
to hold fair elections .	NS-elaboration-object-attribute)))
`` My sense is what they have in mind is an excuse	(NS-attribution(NS-elaboration-process-step
for clamping down on campaigning ''	(NS-means
by creating an atmosphere of a military emergency ,	NS-means))
he said .	NS-attribution))))))))
