Qintex Australia Ltd. encountered another setback Friday when its Los Angeles-based affiliate, Qintex Entertainment Inc., filed for protection under Chapter 11 of the U.S. Bankruptcy Code.	O	9..197
Qintex Entertainment also said David Evans, its president and chief executive, and Roger Kimmel, a director, both resigned.	B-entrel	200..323
Neither could be reached for comment.	I-entrel	324..361
Earlier this month, Qintex Australia's $1.5 billion agreement to acquire MGM/UA Communications Co. collapsed because of a dispute over a $50 million letter of credit the Australian operator of television stations and resorts was to have supplied as security in the transaction.	O	364..641
Mr. Evans had been the de facto head of MGM/UA for months.	O	642..700
Qintex Entertainment, a producer and distributor of television programs most noted for its co-production of the hit miniseries "Lonesome Dove," said it filed for Chapter 11 protection after Qintex Australia failed to provide it with $5.9 million owed to MCA Inc. in connection with the distribution of "The New Leave It to Beaver Show."	O	703..1039
Qintex Entertainment is 43% owned by Qintex Australia and said it relies on the Australian company for funding its working capital requirements.	O	1042..1186
After the announcement of the bankruptcy filing, Qintex Entertainment stock sank $2.625 in over-the-counter trading to close at $1.50 on heavy volume of more than 1.4 million shares.	B-contrast	1189..1371
The stock traded as high as $10 this past summer.	I-contrast	1372..1421
Jonathan Lloyd, executive vice president and chief financial officer of Qintex Entertainment, said Qintex Entertainment was forced to file for protection to avoid going into default under its agreement with MCA.	B-cause	1424..1635
The $5.9 million payment was due Oct. 1 and the deadline for default was Oct. 19.	B-entrel	1636..1717
Mr. Lloyd said if Qintex had defaulted it could have been required to repay $92 million in debt under its loan agreements.	I-entrel	1718..1840
MCA on Friday said that as a result of Qintex's failure to make the required payment it was terminating the distribution agreement on "The New Leave It to Beaver" as well as other MCA properties.	O	1841..2036
Qintex Australia was "saying as recently as last weekend that they would take care of the situation.	B-entrel	2039..2139
They continued to represent that to the board," said Mr. Lloyd.	I-entrel	2140..2203
"We were reassured they would stand behind the company."	O	2204..2260
Mr. Lloyd said both Qintex Entertainment and Qintex Australia had attempted to secure a loan that would allow the company to make the $5.9 million payment but the request was turned down by an unidentified lender on Oct. 14.	O	2263..2487
At that point, he said, Qintex Australia stated it would "endeavor to arrange" the financing.	O	2488..2581
However a Qintex Australia spokesman said his firm had never "promised or guaranteed" to make the payment.	B-conjunction	2584..2690
In a prepared statement from Australia, the company also said that, following the breakdown of the MGM talks, it "had been re-evaluating its position as a significant shareholder and a substantial creditor of Qintex Entertainment" and had "resolved to minimize the degree of further loans to Qintex Entertainment in excess of that previously made."	I-conjunction	2691..3039
The Qintex Australia spokesman added that his company had opposed the Chapter 11 filing.	O	3042..3130
He said the company believed Qintex Entertainment's financial problems could have been resolved by other means.	O	3131..3242
The report of the bankruptcy filing stunned Hollywood executives and investors.	B-instantiation	3245..3324
"It's a shocker," said Joseph Di Lillo, chairman of Drake Capital Securities, a brokerage firm that has an investment in Qintex Entertainment.	I-instantiation	3325..3467
Qintex Australia was "going to pay more than $1 billion for MGM/UA and then they couldn't come up with the far smaller sum of $5.9 million."	O	3468..3608
Qintex said Mr. Evans, the former president, resigned for "personal reasons" and that Mr. Kimmel, an attorney, resigned because his participation in evaluating the company's role in buying MGM/UA was no longer necessary.	B-entrel	3611..3831
Mr. Kimmel was a director of the company and a predecessor firm since 1980.	I-entrel	3832..3907
The announcement seemed to further damp prospects that talks between Qintex Australia and MGM/UA might be revived.	O	3910..4024
It's understood that MGM/UA recently contacted Rupert Murdoch's News Corp., which made two failed bids for the movie studio, to see if the company was still interested.	O	4025..4193
However, "we aren't currently doing anything.	O	4194..4239
It isn't a current topic of conversation at the company," said Barry Diller, chairman and chief executive officer of the Fox Inc. unit of News Corp.	O	4240..4388
