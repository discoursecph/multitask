Insurers may see claims	(NN-Topic-Drift(NS-summary(SN-antithesis(NS-elaboration-additional(NS-elaboration-object-attribute
resulting from the San Francisco earthquake	NS-elaboration-object-attribute)
totaling nearly $ 1 billion	(NS-comparison
-- far less than the claims	(NS-elaboration-object-attribute
they face from Hurricane Hugo --	NS-elaboration-object-attribute)))
but the recent spate of catastrophes should jolt property insurance rates in coming months .	SN-antithesis)
The property claims service division of the American Insurance Services Group estimated insured losses from the earthquake at $ 960 million .	(NS-elaboration-additional(NS-elaboration-additional(SN-antithesis(NS-elaboration-additional(NS-elaboration-additional
This estimate does n't include claims under workers ' compensation , life , health disability and liability insurance and damage to infrastructure such as bridges , highways and public buildings .	NS-elaboration-additional)
The estimated earthquake losses are low	(NS-explanation-argumentative(NS-comparison
compared with the $ 4 billion in claims	(NS-elaboration-object-attribute
that insurers face from Hurricane Hugo ,	(NS-elaboration-additional
which ripped through the Caribbean and the Carolinas last month .	NS-elaboration-additional)))
That 's because only about 30 % of California homes and businesses had earthquake insurance	(NS-elaboration-object-attribute
to cover the losses .	NS-elaboration-object-attribute)))
However , insurance brokers and executives say	(SN-attribution
that the combination of the Bay area earthquake , Hugo and last week 's explosion at the Phillips Petroleum Co. 's refinery in Pasadena , Texas , will cause property insurance and reinsurance rates to jump .	SN-attribution))
Other insurance rates such as casualty insurance ,	(SN-circumstance(NS-attribution(NN-Same-Unit(NS-elaboration-additional
which would cover liability claims ,	NS-elaboration-additional)
are n't likely to firm right away ,	NN-Same-Unit)
says Alice Cornish , an industry analyst with Northington Research in Avon , Conn .	NS-attribution)
She believes	(SN-attribution
the impact of losses from these catastrophes is n't likely to halt the growth of the industry 's surplus capital next year .	SN-attribution)))
Property reinsurance rates are likely to climb first ,	(NS-explanation-argumentative(NS-attribution
analysts and brokers believe .	NS-attribution)
`` The reinsurance market has been bloodied by disasters '' in the U.S. as well as in Great Britain and Europe ,	(NS-elaboration-additional(NS-evidence(NS-attribution
says Thomas Rosencrants , director of research at Interstate/Johnson Lane Inc. in Atlanta .	NS-attribution)
Insurers typically retain a small percentage of the risks	(SN-background(NS-elaboration-additional(NN-List(NS-elaboration-object-attribute
they underwrite	NS-elaboration-object-attribute)
and pass on the rest of the losses .	NN-List)
Insurers buy this insurance protection for themselves	(NS-example(NS-means
by giving up a portion of the premiums	(NN-Same-Unit(NS-elaboration-object-attribute
they collect on a policy	NS-elaboration-object-attribute)
to another firm	(NS-elaboration-object-attribute
-- a reinsurance company ,	(NS-elaboration-object-attribute
which , in turn , accepts a portion of any losses	(NS-elaboration-object-attribute
resulting from this policy .	NS-elaboration-object-attribute)))))
Insurers , such as Cigna Corp. , Transamerica Corp , and Aetna Life & Casualty Co. , buy reinsurance from other U.S.-based companies and Lloyd 's of London for one catastrophe at a time .	NS-example))
After Hugo hit ,	(NN-Sequence(SN-reason(SN-temporal-after
many insurers exhausted their reinsurance coverage	SN-temporal-after)
and had to tap reinsurers	(NS-hypothetical(NS-purpose
to replace that coverage	NS-purpose)
in case there were any other major disasters before the end of the year .	NS-hypothetical))
After the earthquake two weeks ago ,	(NN-Same-Unit(NS-attribution
brokers say	NS-attribution)
companies scrambled to replace reinsurance coverages again	(NS-consequence
and Lloyd 's syndicates turned to the London market excess lines for protection of their own .	NS-consequence)))))
James Snedeker , senior vice president of Gill & Roeser Inc. , a New York-based reinsurance broker , says	(NS-antithesis(SN-interpretation(SN-circumstance(NN-Contrast(SN-attribution
insurers	(NN-Same-Unit(NS-elaboration-object-attribute
who took big losses this fall	(NN-List
and had purchased little reinsurance in recent years	NN-List))
will be asked to pay some pretty hefty rates	(NS-condition
if they want to buy reinsurance for 1990 .	NS-condition)))
However , companies with few catastrophe losses this year and already big buyers of reinsurance are likely to see their rates remain flat , or perhaps even decline slightly .	NN-Contrast)
Many companies will be negotiating their 1990 reinsurance contracts in the next few weeks .	SN-circumstance)
`` It 's a seller 's market , ''	(NS-attribution
said Mr. Snedeker of the reinsurance market right now .	NS-attribution))
But some large insurers , such as State Farm Mutual Automobile Insurance Co. , do n't purchase reinsurance ,	(NS-elaboration-additional(NN-List
but fund their own program .	NN-List)
A few years ago , State Farm , the nation 's largest home insurer , stopped buying reinsurance	(NS-evidence(SN-result(NS-reason
because no one carrier could provide all the coverage	(NS-elaboration-object-attribute
that it needed	NS-elaboration-object-attribute))
and the company found it cheaper to self-reinsure .	SN-result)
The $ 472 million of losses	(NN-Same-Unit(NS-elaboration-object-attribute
State Farm expects from Hugo	NS-elaboration-object-attribute)
and an additional $ 300 million from the earthquake are less than 5 % of State Farm 's $ 16.7 billion total net worth .	NN-Same-Unit))))))))
Since few insurers have announced what amount of losses	(NS-example(NS-attribution(SN-explanation-argumentative(NS-elaboration-object-attribute
they expect to see from the earthquake ,	NS-elaboration-object-attribute)
it 's impossible to get a clear picture of the quake 's impact on fourth-quarter earnings ,	SN-explanation-argumentative)
said Herbert Goodfriend at Prudential-Bache Securities Corp .	NS-attribution)
Transamerica expects an after-tax charge of less than $ 3 million against fourth-quarter net ;	(NN-List
Hartford Insurance Group , a unit of ITT Corp. , expects a $ 15 million or 10 cents after-tax charge ;	(NN-List
and Fireman 's Fund Corp. expects a charge of no more than $ 50 million before taxes	(NS-temporal-after
and after using its reinsurance .	NS-temporal-after)))))
