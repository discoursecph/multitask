DISASTER STATES aren't jumping to raise taxes for relief and recovery.	O	9..79
Not yet, anyway.	B-contrast	82..98
Just after Hurricane Hugo battered South Carolina, some officials talked of perhaps adding a penny to the state gasoline tax or raising property taxes.	B-contrast	99..250
Gov. Campbell responded, "They're mentioning rope when there's been a hanging in the family."	I-contrast	251..344
A spokesman says the governor believes he can avoid increases by relying on federal aid and shifting funds in state programs.	O	345..470
Still, Hugo's impact may revive unsuccessful proposals to give local governments authority to levy sales taxes.	O	471..582
A spokesman for North Carolina Gov. Martin says Hugo hasn't prompted proposals for state or local increases.	O	585..693
California, where earthquake damage may top $5 billion, plans a special legislative session.	B-restatement	694..786
Property-tax relief is likely.	B-conjunction	787..817
Legislators are talking about temporary rises in sales or gasoline taxes, although Gov. Deukmejian says they should be a last resort.	I-conjunction	818..951
Needs aren't clear, and the state constitution makes increasing taxes and spending very difficult.	B-contrast	952..1050
But some legislators think the time may be ripe to revise the constitution.	I-contrast	1053..1128
THE IRS WILL PAY if its error burdens you with bank charges.	O	1131..1191
Policy statement P-5-39 sets out terms.	B-entrel	1194..1233
As a result of an erroneous IRS levy on a bank account, a taxpayer may incur administrative and overdraft charges.	B-contrast	1234..1348
If the IRS admits its error and the charges have been paid, it will reimburse a taxpayer who hasn't refused to give timely answers to IRS inquiries or hasn't contributed to continuing or compounding the error.	B-conjunction	1349..1558
The IRS recently amended the policy to cover stop-payment charges for checks lost by the IRS.	I-conjunction	1559..1652
If the IRS asks for and gets a replacement for a check that it concedes it lost in processing, it will reimburse the taxpayer for the stop-payment charge on the original.	B-contrast	1655..1825
Reimbursement claims must be filed with the IRS district or service-center director within a year after the expense accrues.	I-contrast	1826..1950
If the IRS seeks late-payment interest because of the lost check, you should request interest abatement, publisher Prentice Hall notes.	O	1951..2086
JUST FIVE ACRES MORE are all we need to feel really at home, they say.	O	2089..2159
A couple we'll call the Blandings spent nearly $800,000 on a 15-acre plot and main home and have an old $175,000 mortgage exempt from the new limit on mortgage-interest deductions.	B-entrel	2162..2342
They plan to expand the home site by buying five adjoining acres for $200,000, borrowed against a first mortgage on the five acres and also collateralized by the 15 acres.	B-conjunction	2343..2514
Their debt will be well under the $1 million limit -- on borrowing to acquire, build, or improve a home -- that qualifies for mortgage-interest deductions.	I-conjunction	2515..2670
As you can guess, the Blandings want to deduct home-mortgage interest on the $200,000 loan.	O	2673..2764
But, IRS private ruling 8940061 notes, no rule or court case bears directly on the issue of adding land to a principal residence.	O	2765..2894
So the IRS has drawn a rationale from the sale of a home site split in two and sold in different years to the same buyer; a court let the seller in that old case treat this as the sale of one residence.	B-cause	2895..3097
Thus, the IRS says, the Blandings' $200,000 loan is home-acquisition debt, and interest on it is fully deductible.	I-cause	3100..3214
EARTHQUAKE VICTIMS facing imminent filing and payment deadlines will get extensions and penalty waivers like those provided for Hugo's victims; IRS Notice 89108 has details.	O	3217..3390
Notice 89-107 offers added relief for hurricane-hit concerns that must file pension and benefit-plan returns.	O	3391..3500
REPORTS OF PAYMENTS to independent contractors for services must be filed by businesses, but don't bet that contractors' unreported income will be detected that way.	O	3503..3668
The General Accounting Office estimates that 50% of IRS audits don't spot companies that fail to file the reports.	O	3669..3783
UH HUH:	O	3786..3793
A claim by Peter Testa of New York that a stranger paid him $500 to go into a bank and change $44,400 in small bills into large bills "is unconvincing," the Tax Court found.	B-cause	3794..3967
It held that Testa is taxable on $44,400 of unreported income.	I-cause	3968..4030
WHY BE A MIDDLEMAN for charitable gifts? a retiree asks.	O	4033..4089
A retired electrical engineer we'll call Ben works part-time as a consultant, but he doesn't want to earn so much that Social Security reduces his benefits.	B-cause	4092..4248
So he has arranged for a university foundation to set up a scholarship fund for undergraduate engineering students.	I-cause	4249..4364
He plans to tell clients to pay certain fees directly to the foundation instead of to him; he would omit those fees from income reported on his return.	O	4365..4516
So he asked the IRS if the plan would work.	O	4517..4560
Well, notes IRS private ruling 8934014, "a fundamental principle" is that income must be taxed to whoever earns it.	O	4563..4678
The rule goes back at least as far as a 1930 Supreme Court decision, Robert Willens of Shearson Lehman Hutton says.	O	4679..4794
If you assign your income to another, you still have controlled its disposition and enjoyed the fruits of your labor, even if indirectly.	O	4795..4932
Ben earns any fees sent directly to charity and is taxable on them, the IRS says; of course, he also may take a charitable deduction for them.	O	4935..5077
BRIEFS:	O	5080..5087
Ways and Means veteran Gephardt (D., Mo.) moves to the House Budget Committee; Rep. Cardin (D., Md.) replaces him... .	O	5088..5206
Seattle's license fees for adult peep shows vary from those for other coin-operated amusements without serving a substantial government interest and are unconstitutional, the ninth-circuit appeals court holds for Acorn Investments Inc.	O	5207..5442
