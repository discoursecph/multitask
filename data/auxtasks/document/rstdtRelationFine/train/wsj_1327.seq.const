At age eight , Josephine Baker was sent by her mother to a white woman 's house	(TextualOrganization(elaboration-additional(elaboration-additional(comment(Sequence(purpose
to do chores in exchange for meals and a place to sleep	(elaboration-object-attribute
-- a place in the basement with the coal .	elaboration-object-attribute))
At age 19 , she was a Paris sensation ,	(elaboration-additional
transformed from unwanted child to international sex symbol in just over a decade .	elaboration-additional))
It is the stuff of dreams , but also of traumas .	(elaboration-additional
Only the bravest spirits survive such roller coasters .	(antithesis
And , for Ms. Baker , the ride was far from over .	antithesis)))
Her bare breasts , her dancing , her voice , her beauty and , perhaps most famously , her derriere , were prominent attractions ,	(elaboration-additional
but courage of a rare sort made her remarkable life possible .	elaboration-additional))
Bricktop , another American black woman	(attribution(Same-Unit(elaboration-object-attribute
who found a measure of fame in Paris ,	elaboration-object-attribute)
said :	Same-Unit)
`` I do n't think	(attribution
I 've ever known anyone with a less complicated view of life ,	(elaboration-additional(elaboration-additional(elaboration-additional(Comparison
or whose life was more complicated than Josephine 's . ''	Comparison)
Men were a constant complication .	(elaboration-additional(elaboration-additional(elaboration-additional
Baker had lots of them .	(antithesis
But she did n't trust them	(List
and did n't reward trust .	List)))
As she saw one key love affair ,	(interpretation
the problem was n't her infidelity ,	(Contrast
it was his jealousy .	Contrast)))
Her appetite for children also was large .	(explanation-argumentative
She adopted 12 of assorted races ,	(elaboration-additional(elaboration-additional
naming them the Rainbow Tribe ,	elaboration-additional)
and driving her husband first to despair and then to Argentina .	elaboration-additional))))
She made money ,	(elaboration-additional(elaboration-additional(Contrast
but spent more .	Contrast)
Friends pitched in .	elaboration-additional)
Finally , Prince Rainier and Princess Grace saved her with the offer of a house in Monaco .	elaboration-additional))
Another lifelong complication ,	(elaboration-additional(comment(elaboration-additional(elaboration-additional(Same-Unit(attribution
as Phyllis Rose makes clear in `` Jazz Cleopatra :	(elaboration-additional
Josephine Baker in Her Time ''	(elaboration-additional
( Doubleday , 321 pages , $ 22.50 ) ,	elaboration-additional)))
was racism .	Same-Unit)
Baker had the good luck	(Contrast(elaboration-additional(elaboration-additional
to arrive in 1925 Paris ,	(elaboration-object-attribute
where blacks had become exotic .	elaboration-object-attribute))
African art was in vogue	(explanation-argumentative(List
and some intellectuals were writing breathlessly of a dawning age	(elaboration-additional
to be inspired by blacks .	elaboration-additional))
To be exotic was to be patronized as well as prized ,	(elaboration-additional
but for the most part Paris was a friendly island in a racist world .	elaboration-additional)))
Baker had bitter experience of bigotry from her St. Louis childhood and her days in New York theater ,	(Contrast(elaboration-object-attribute
where she was judged too dark for an all-black chorus line	(elaboration-additional
( performing of course for all-white audiences ) .	elaboration-additional))
Paris loved her at first sight .	(comment(attribution(elaboration-additional
`` She just wiggled her fanny	(Cause-Result
and all the French fell in love with her , ''	Cause-Result))
sniffed the literary world 's Maria Jolas , not entirely inaccurately .	attribution)
`` One can hardly overemphasize the importance of her rear end , ''	(elaboration-additional(elaboration-additional(elaboration-additional(attribution
Ms. Rose writes .	attribution)
Ms. Rose ,	(elaboration-additional(Same-Unit(elaboration-object-attribute
who teaches literature at Wesleyan University ,	elaboration-object-attribute)
quickly proceeds to overemphasize ,	Same-Unit)
claiming	(attribution
that Baker 's dancing `` had uncovered a new region for desire ''	(consequence
and thereby ignoring centuries of tributes to the callipygous .	consequence))))
`` Jazz Cleopatra '' contains other , more important , false notes	(elaboration-object-attribute
that undermine what is , for the most part , a lively account of a life already familiar from earlier works .	elaboration-object-attribute))
It is easy to see why Baker , a free spirit	(Same-Unit(elaboration-object-attribute
who broke many of the restraints	(elaboration-additional
convention places on women ,	elaboration-additional))
attracts Ms. Rose , the author of `` Parallel Lives , '' a wonderful study of Victorian marriage .	Same-Unit))))))
Still , even the title raises questions about the author 's vision of her subject .	(elaboration-additional(elaboration-additional(explanation-argumentative
Baker 's art was jazz only by the widest stretch of the term .	explanation-argumentative)
To find parallels , other than sexual appeal , with Cleopatra , requires an equal stretch .	elaboration-additional)
Baker was 68 years old	(elaboration-additional(elaboration-additional(elaboration-additional
when she died in Paris , two days after the sold-out opening of her newest show :	(elaboration-additional
a movie-like ending to what was a cinematic life .	elaboration-additional))
In fact , Ms. Baker played scenes in Casablanca	(elaboration-object-attribute
that could have made it into `` Casablanca . ''	elaboration-object-attribute))
During World War II , her uncomplicated view of life led her to the conclusion	(elaboration-additional(Same-Unit(elaboration-object-attribute
that the Nazis were evil	(List
and must be resisted ,	List))
a decision	(elaboration-object-attribute
made by only about 2 % of French citizens .	elaboration-object-attribute))
She was devoted to Charles de Gaulle 's cause ,	(evidence(evidence(elaboration-additional
accepting great financial sacrifice and considerable risk	(consequence
to become first a spy and then a one-woman USO tour for the forces of Free France .	consequence))
In Humphrey Bogart 's nightclub , Victor Laszlo leads Free French sympathizers in `` La Marseillaise ''	(Comparison(purpose
to drown out the Nazis .	purpose)
The night	(Same-Unit(elaboration-object-attribute
the Germans occupied all of France ,	elaboration-object-attribute)
Baker performed in Casablanca .	Same-Unit)))
The Free French wore black arm bands ,	(List
and	(Same-Unit
when she sang `` J'ai deux amours ''	(temporal-same-time
they wept .	temporal-same-time))))))))
Ms. Rose is best on the early years and World War II .	(elaboration-additional
In her introduction , Ms. Rose writes	(comment(antithesis(attribution
that she feels	(attribution
she has much in common with Baker ,	attribution))
but as `` Jazz Cleopatra '' goes on ,	(Temporal-Same-Time
it seems more rushed ,	(evaluation
as though the author were growing less interested .	evaluation)))
It does n't help that sometimes Ms. Rose 's language fails to deliver the effect	(example(example(elaboration-object-attribute
she appears to want .	elaboration-object-attribute)
One chapter opens :	(elaboration-additional
`` World War II was not one of France 's glorious moments . ''	elaboration-additional))
Elsewhere , in an attempt	(Same-Unit(elaboration-object-attribute
to explain	(attribution(manner
without stating it plainly	manner)
that Baker had a large gay following later in her career	(Temporal-Same-Time
when she was an overdressed singer rather than underdressed dancer ,	Temporal-Same-Time)))
Ms. Rose writes :	(attribution
`` She was a female impersonator	(List
who happened to be a woman . ''	List)))))))
One devoted fan	(elaboration-additional(elaboration-additional(Same-Unit(elaboration-object-attribute
who fell under Baker 's spell in 1963	(List
and began collecting Baker memorabilia	List))
was Bryan Hammond .	Same-Unit)
In `` Josephine Baker ''	(Same-Unit(elaboration-additional
( Jonathan Cape , 304 pages , $ 35 ) ,	(elaboration-object-attribute
which was published in Britain last year	(Sequence
and distributed in the U.S. this month ,	Sequence)))
Mr. Hammond has used his collection	(purpose
to produce an album of photographs and drawings of the star .	purpose)))
The text by Patrick O'Connor is a tough read ,	(antithesis
but the pictures make her magnetism clear	(List
and help explain	(attribution
why Ernest Hemingway called Baker `` The most sensational woman	(elaboration-object-attribute
anybody ever saw .	(List
Or ever will . ''	List)))))))))))
Mr. Lescaze is foreign editor of the Journal .	TextualOrganization)
