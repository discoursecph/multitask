With economic tension between the U.S. and Japan worsening , many Japanese had feared last week 's visit from U.S. Trade Representative Carla Hills .	ROOT	0
They expected a new barrage of demands that Japan do something quickly to reduce its trade surplus with the U.S. .	COREF_PREV	1
Instead , they got a discussion of the need for the U.S. and Japan to work together and of the importance of the long - term view .	COREF_PREV	2
Mrs. Hills ' first trip to Japan as America 's chief trade negotiator had a completely different tone from last month 's visit by Commerce Secretary Robert A. Mosbacher .	COREF_PREV	3
Mr. Mosbacher called for concrete results by next spring in negotiations over fundamental Japanese business practices that supposedly inhibit free trade .	COREF_PREV	4
He said such results should be `` measurable in dollars and cents '' in reducing the U.S. trade deficit with Japan .	COREF_PREV	5
But Mrs. Hills , speaking at a breakfast meeting of the American Chamber of Commerce in Japan on Saturday , stressed that the objective `` is not to get definitive action by spring or summer , it is rather to have a blueprint for action . ''	COREF_PREV	6
She added that she expected `` perhaps to have a down payment ... some small step to convince the American people and the Japanese people that we 're moving in earnest . ''	COREF_PREV	7
How such remarks translate into policy wo n't become clear for months .	COREF_PREV	8
American and Japanese officials offered several theories for the difference in approach betwen Mr. Mosbacher and Mrs. Hills .	COREF_OTHER	9
Many called it simply a contrast in styles .	COREF_PREV	10
But some saw it as a classic negotiating tactic .	COREF_PREV	11
Others said the Bush administration may feel the rhetoric on both sides is getting out of hand .	NONE	12
And some said it reflected the growing debate in Washington over pursuing free trade with Japan versus some kind of managed trade .	COREF_OTHER	13
Asked to compare her visit to Mr. Mosbacher 's , Mrs. Hills replied : `` I did n't hear every word he spoke , but as a general proposition , I think we have a very consistent trade strategy in the Bush administration . ''	COREF_OTHER	14
Yet more than one American official who sat in with her during three days of talks with Japanese officials said her tone often was surprisingly `` conciliatory . ''	COREF_PREV	15
`` I think my line has been very consistent , '' Mrs. Hills said at a news conference Saturday afternoon .	COREF_PREV	16
`` I am painted sometimes as ferocious , perhaps because I have a ferocious list of statutes to implement .	COREF_PREV	17
I do n't feel very ferocious .	COREF_PREV	18
I do n't feel either hard or soft .	COREF_PREV	19
I feel committed to the program of opening markets and expanding trade . ''	COREF_PREV	20
When she met the local press for the first time on Friday , Mrs. Hills firmly reiterated the need for progress in removing barriers to trade in forest products , satellites and supercomputers , three areas targeted under the Super 301 provision of the 1988 trade bill .	COREF_PREV	21
She highlighted exclusionary business practices that the U.S. government has identified .	COREF_PREV	22
But her main thrust was to promote the importance of world - wide free trade and open competition .	COREF_PREV	23
She said the trade imbalance was mainly due to macroeconomic factors and should n't be tackled by setting quantitative targets .	COREF_PREV	24
At her news conference for Japanese reporters , one economics journalist summed up the Japanese sense of relief .	COREF_PREV	25
`` My impression was that you would be a scary old lady , '' he said , drawing a few nervous chuckles from his colleagues .	COREF_PREV	26
`` But I am relieved to see that you are beautiful and gentle and intelligent and a person of integrity . ''	COREF_PREV	27
Mrs. Hills ' remarks did raise questions , at least among some U.S. officials , about what exactly her stance is on U.S. access to the Japanese semiconductor market .	COREF_PREV	28
The U.S. share of the Japanese market has been stuck around 10 % for years .	COREF_PREV	29
Many Americans have interpreted a 1986 agreement as assuring U.S. companies a 20 % share by 1991 , but the Japanese have denied making any such promise .	COREF_OTHER	30
At one of her news conferences , Mrs. Hills said , `` I believe we can do much better than 20 % . ''	COREF_PREV	31
But she stressed , `` I am against managed trade .	COREF_PREV	32
I will not enter into an agreement that stipulates to a percentage of the market .	COREF_PREV	33
