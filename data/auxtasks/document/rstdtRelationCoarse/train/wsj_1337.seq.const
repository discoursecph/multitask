Tokyo stocks closed firmer Monday ,	(Summary-Evaluation-Topic(Summary-Evaluation-Topic(Joint(Temporal
with the Nikkei index making its fifth consecutive daily gain .	Temporal)
Stocks also rose in London ,	(Joint
while the Frankfurt market was mixed .	Joint))
In Tokyo , the Nikkei index added 99.14 to 35585.52 .	(Summary-Evaluation-Topic(Elaboration(Elaboration
The index moved above 35670 at midmorning ,	(Comparison(Elaboration
nearly reaching the record of 35689.98 set Sept. 28 .	Elaboration)
But the market lost part of the early gains on index-linked investment trust fund selling .	Comparison))
In early trading in Tokyo Tuesday , the Nikkei index rose 1.08 points to 35586.60 .	(Temporal
On Monday , traders noted	(Elaboration(Comparison(Attribution
that some investors took profits against the backdrop of the Nikkei 's fast-paced recovery	(Temporal
following its plunge last Monday in reaction to the Oct. 13 drop in New York stock prices .	Temporal))
But overall buying interest remained strong through Monday ,	(Elaboration
with many observers saying	(Attribution
they expect the Nikkei to continue with moderate gains this week .	Attribution)))
Turnover remained relatively small .	(Elaboration(Elaboration(Summary-Evaluation-Topic(Cause
Volume on the first section was estimated at 600 million shares , down from 1.03 billion shares Friday .	(Joint
The Tokyo stock price index of first section issues was up 7.81 at 2687.53 .	Joint))
Relatively stable foreign currency dealings Monday were viewed favorably by market players ,	(Comparison(Attribution
traders said .	Attribution)
But institutional investors may wait a little longer	(Attribution(Temporal
to appraise the direction of the U.S. monetary policy and the dollar ,	Temporal)
traders said .	Attribution)))
Hiroyuki Wada , general manager of the stock department at Okasan Securities , said	(Elaboration(Attribution
Monday 's trading was `` unfocused . ''	Attribution)
He said	(Elaboration(Attribution
investors were picking individual stocks	(Cause
based on specific incentives and the likelihood of a wider price increase over the short term .	Cause))
The selective approach blurred themes such as domestic-demand issues , large-capitalization issues or high-technology shares ,	(Attribution(Elaboration
which had been providing at least some trading direction over the past few weeks ,	Elaboration)
Mr. Wada said .	Attribution))))
Investors took profits on major construction shares ,	(Joint(Elaboration(Cause(Elaboration
which advanced last week ,	Elaboration)
shifting their attention to some midsize companies such as Aoki Corp. , Tobishima and Maeda .	Cause)
Aoki gained 60 yen to 1,480 yen	(Summary-Evaluation-Topic
( $ 10.40 ) .	Summary-Evaluation-Topic))
Some pharmaceutical shares were popular on rumors	(Elaboration(Elaboration
related to new products	(Elaboration
to be introduced at a cancer conference	(Elaboration
that opened in Nagoya .	Elaboration)))
Teijin was up 15 at 936 ,	(Joint
and Kyowa Hakko gained 30 to 1,770 .	(Joint
Mochida advanced 40 to 4,440 .	(Joint
Fujisawa continued to attract investors	(Joint(Temporal(Cause
because of strong earning prospects	(Elaboration
stemming from a new immune control agent .	Elaboration))
Fujisawa gained 50 to 2,060 .	Temporal)
Kikkoman was up 30 to 1,600 ,	(Attribution(Elaboration
receiving investor interest for its land property holdings near Tokyo ,	Elaboration)
a trader said .	Attribution)))))))))))
London prices closed modestly higher in the year 's thinnest turnover , a condition	(Summary-Evaluation-Topic(Elaboration(Elaboration(Elaboration
that underscored a lack of conviction ahead of a U.K. balance of payments report Tuesday .	Elaboration)
Limited volume ahead of the September trade data showed	(Comparison(Attribution
the market is nervous ,	Attribution)
but dealers added	(Cause(Attribution
that the day 's modest gains also signaled some support for London equities .	Attribution)
They pegged the support largely to anticipation	(Elaboration(Elaboration
that Britain 's current account imbalance ca n't be much worse than the near record deficits	(Elaboration
seen in July and August .	Elaboration))
`` It 's a case	(Elaboration(Attribution(Elaboration
of the market being too high	(Textual-organization(Cause
to buy	Cause)
and too afraid	(Cause
to sell , ''	Cause)))
a senior dealer with Kleinwort Benson Securities said .	Attribution)
`` It 's better to wait . ''	Elaboration)))))
The Financial Times 100-share index finished 10.6 points higher at 2189.7 .	(Joint
The 30-share index closed 11.6 points higher at 1772.6 .	(Joint(Elaboration
Volume was 276.8 million shares , beneath the year 's previous low of 280.5 million shares Sept. 25 , the session	(Elaboration(Elaboration
before the August trade figures were released .	Elaboration)
Analysts ' expectations suggest a September current account deficit of # 1.6 billion	(Comparison(Comparison(Summary-Evaluation-Topic
( $ 2.54 billion ) ,	Summary-Evaluation-Topic)
compared with August 's # 2.0 billion deficit .	Comparison)
Dealers , however , said	(Cause(Attribution
forecasts are broadly divergent	(Temporal
with estimates ranging between # 1 billion and # 2 billion .	Temporal))
`` The range of expectations is so broad , ''	(Cause(Attribution
a dealer at another major U.K. brokerage firm said ,	Attribution)
`` the deficit may have to be nearer or above # 2 billion	(Cause
for it to have any impact on the market . ''	Cause))))))
Lucas Industries , a British automotive and aerospace concern , rose 13 pence to 614 pence	(Temporal
after	(Textual-organization(Attribution
it said	Attribution)
its pretax profit for the year rose 28 % .	Textual-organization)))))
Share prices on the Frankfurt stock exchange closed narrowly mixed in quiet dealings	(Elaboration(Cause(Elaboration(Temporal
after recovering most of their early losses .	Temporal)
The DAX index eased 0.99 point	(Cause
to end at 1523.22	(Temporal
after falling 5.5 points early in the session .	Temporal)))
Brokers said	(Elaboration(Elaboration(Attribution
the declines early in the day were partly caused by losses of the ruling Christian-Democratic Union in communal elections in the state of Baden-Wuerttemberg .	Attribution)
The start of a weeklong conference by the IG Metall metal worker union in Berlin is drawing attention to the impending wage negotiations ,	(Comparison(Attribution(Elaboration
which could boost companies ' personnel costs next year ,	Elaboration)
they said .	Attribution)
But there was little selling pressure ,	(Joint
and even small orders at the lower levels sufficed to bring the market back to Friday 's opening levels .	Joint)))
Traders said	(Elaboration(Attribution
the thin trading volume points to continued uncertainty by most investors	(Temporal
following last Monday 's record 13 % loss .	Temporal))
The market is still 4 % short of its level before the plunge ,	(Comparison(Joint
and analysts are n't sure	(Attribution
how long it will take	(Temporal
until the DAX has closed that gap .	Temporal)))
But Norbert Braeuer , chief trader at Hessische Landesbank Girozentrale	(Attribution(Textual-organization(Elaboration
( Helaba ) ,	Elaboration)
said	Textual-organization)
he expects share prices to move upward in the coming weeks .	Attribution)))))
Banking stocks were the major gainers Monday amid hope	(Joint(Temporal(Elaboration
that interest rates have peaked ,	Elaboration)
as Deutsche Bank and Dresdner Bank added 4 marks each to 664 marks	(Joint(Textual-organization(Summary-Evaluation-Topic
( $ 357 )	Summary-Evaluation-Topic)
and 326 marks , respectively .	Textual-organization)
Commerzbank gained 1 to 252.5 .	Joint))
Auto shares were mixed ,	(Temporal
as Daimler-Benz firmed 2 to 723 ,	(Joint
Bayerische Motoren Werke lost the same amount to 554 ,	(Joint
and Volkswagen inched down 1.4 to 451.6 .	Joint))))))))
Elsewhere , prices closed higher in Amsterdam , lower in Zurich , Stockholm and Milan , mixed in Brussels and unchanged in Paris .	(Joint
Shares closed higher in Hong Kong , Singapore and Manila ,	(Joint
and were lower in Sydney , Seoul and Taipei .	(Joint
Wellington was closed .	(Joint
Here are price trends on the world 's major stock markets ,	(Elaboration(Elaboration
as calculated by Morgan Stanley Capital International Perspective , Geneva .	Elaboration)
To make them directly comparable ,	(Cause
each index is based on the close of 1969	(Elaboration(Elaboration
equaling 100 .	Elaboration)
The percentage change is since year-end .	Elaboration))))))))
