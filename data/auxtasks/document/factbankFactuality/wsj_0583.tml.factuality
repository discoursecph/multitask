Polly Peck International Inc. 's agreement to acquire 51 % of Sansui Electric Co. proves that foreign companies can acquire Japanese companies -- if the alternative for the Japanese company is extinction .	Uu	4
Polly Peck , a fast-growing British conglomerate , will pay 15.6 billion yen ( $ 110 million ) for 39 million new shares of Sansui , a well-known maker of high-fidelity audio equipment that failed to adjust to changing market conditions .	CT+	5
Japanese government officials , eager to rebut foreign criticism of Japanese investments overseas , hailed the transaction as proof foreigners can make similar investments in Japan .	CT+	6
Polly Peck 's chairman , Asil Nadir , echoed the official Japanese view of the accord , which was announced Friday .	CT+	7
" The myths that Japan is not open to concerns from outside has , I think , been demolished at a stroke , " Mr. Nadir said .	Uu	8
But analysts say Sansui is a special case .	CT+	9
It expects to post a loss of 6.4 billion yen for the year ending tomorrow and its liabilities currently exceed its assets by about 13.8 billion yen .	Uu	10
" If you find sound , healthy companies in Japan , they are not for sale , " said George Watanabe , a management-consultant at Tokyo-based Asia Advisory Services Inc.	Uu	11
But by all accounts foreign companies have bought only a relative handful of Japanese companies this year , while Japanese companies have acquired hundreds of foreign companies .	CT+	13
Nor do analysts expect the Sansui deal to touch off a fresh wave of foreign purchases .	PR-	14
If the strong yen and the high stock prices of Japanese companies were n't deterrents enough , webs of cross-shareholdings between friendly Japanese companies and fiercely independent Japanese corporate attitudes repel most would-be acquirers .	CT+	15
But in recent years , the market has moved toward less expensive " mini-component " sets , miniaturized amplifiers and receivers and software players that could be stacked on top of each other .	CT+	18
Some of Sansui 's fellow audio-specialty companies , such as Aiwa Co. and Pioneer Electric Corp. , responded to the challenge by quickly bringing out mini-component products of their own , by moving heavily into the booming compact disk businesses or by diversifying into other consumer-electronics fields , including laser disks or portable cassette players .	CT+	19
Sansui was late into the mini-component business and failed to branch into other new businesses .	CT-	20
As the yen soared in recent years , Sansui 's deepening financial problems became a vicious circle .	CT+	21
While competitors moved production offshore in response to the sagging competitiveness of Japanese factories , Sansui lacked the money to build new plants in Southeast Asia .	CT+	22
" Our company has not been able to cope very effectively with " changes in the marketplace , said Ryosuke Ito , Sansui 's president .	Uu	23
Yoshihisa Murasawa , a management consultant for Booz-Allen amp Hamilton ( Japan ) Inc. , said his firm will likely be recommending acquisitions of Japanese companies more often to foreign clients in the future .	Uu	25
" Attitudes { toward being acquired } are still negative , but they 're becoming more positive , " Mr. Murasawa said .	CT+	26
" In some industries , like pharmaceuticals , acquisitions make sense . "	Uu	27
Whether Polly Peck 's acquisition makes sense remains to be seen , but at the news conference , Mr. Nadir brimmed with self-confidence that he can turn Sansui around .	CT+	28
He said Polly Peck will greatly expand Sansui 's product line , using Sansui 's engineers to design the new products , and will move Sansui 's production of most products other than sophisticated audio gear offshore into Polly Peck 's own factories .	CT+	30
" Whatever capital it ( Sansui ) needs so it can compete and become a totally global entity capable of competing with the best in the world , that capital will be injected , " Mr. Nadir said .	Uu	31
And while Polly Peck is n't jettisoning the existent top-management structure of Sansui , it is bringing in a former Toshiba Corp. executive as executive vice president and chief operating officer .	CT-	32
He took Polly Peck , once a small fabric wholesaler , and used it at as a base to build a conglomerate that has been doubling its profits annually since 1980 .	CT+	34
In September , it announced plans to acquire the tropical-fruit business of RJR Nabisco Inc. 's Del Monte foods unit for # 557 million ( $ 878 million ) .	CT+	35
Last month , Polly Peck posted a 38 % jump in pretax profit for the first half to # 54.8 million from # 39.8 million on a 63 % rise in sales .	CT+	36
