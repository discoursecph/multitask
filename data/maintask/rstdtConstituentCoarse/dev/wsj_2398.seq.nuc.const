The chemical industry is expected to report	(NS-Elaboration(NN-Comparison(NS-Summary-Evaluation-Topic(NN-Summary-Evaluation-Topic(NS-Elaboration(NS-Temporal(SN-Attribution
that profits eroded in the third quarter	(NS-Cause
because of skidding prices in the commodity end of the business .	NS-Cause))
Producers of commodity chemicals , the basic chemicals	(NN-Textual-organization(NS-Elaboration
produced in huge volumes for other manufacturers ,	NS-Elaboration)
have seen sharp inventory cutting by buyers .	NN-Textual-organization))
Once the chief beneficiaries of the industry 's now fading boom , these producers also will be reporting against exceptionally strong performances in the 1988 third quarter .	NS-Elaboration)
`` For some of these companies , this will be the first quarter with year-to-year negative comparisons , ''	(NS-Elaboration(NS-Cause(NS-Attribution
says Leonard Bogner , a chemical industry analyst at Prudential Bache Research .	NS-Attribution)
`` This could be the first of five or six down quarters . ''	NS-Cause)
Perhaps most prominent , Dow Chemical Co. ,	(NS-Summary-Evaluation-Topic(NS-Cause(SN-Attribution(NN-Textual-organization(NS-Elaboration
which as of midyear had racked up eight consecutive record quarters ,	NS-Elaboration)
is expected to report	NN-Textual-organization)
that profit decreased in the latest quarter from a year earlier , if only by a shade .	SN-Attribution)
Though Dow has aggressively diversified into specialty chemicals and pharmaceuticals ,	(SN-Comparison
the company still has a big stake in polyethylene ,	(NS-Elaboration
which is used in packaging and housewares .	NS-Elaboration)))
Analysts ' third-quarter estimates for the Midland , Mich. , company are between $ 3.20 a share and $ 3.30 a share ,	(NS-Elaboration(NS-Comparison
compared with $ 3.36 a year ago ,	(NS-Temporal
when profit was $ 632 million on sales of $ 4.15 billion .	NS-Temporal))
A Dow spokeswoman declined to comment on the estimates .	NS-Elaboration))))
At the investment firm of Smith Barney , Harris Upham & Co. , the commodity-chemical segment is seen	(NN-Joint(NS-Cause(NS-Joint
pulling down overall profit for 20 companies representative of the whole industry by 8 % to 10 % .	NS-Joint)
`` You will find the commodities off more than the others and the diversified companies about even or slightly better , ''	(NS-Attribution
says James Wilbur , a Smith Barney analyst .	NS-Attribution))
First Boston Corp. projects	(NS-Cause(SN-Attribution
that 10 of the 15 companies it follows will report lower profit .	SN-Attribution)
Most of the 10 have big commodity-chemical operations .	NS-Cause)))
Still , some industry giants are expected to report continuing gains ,	(NS-Cause
largely because so much of their business is outside commodity chemicals .	(NS-Elaboration
Du Pont Co. is thought to have had steady profit growth in white pigments , fibers and polymers .	(NN-Joint(NS-Elaboration(NS-Summary-Evaluation-Topic(NN-Comparison(NS-Elaboration
Moreover , the Wilmington , Del. , company is helped	(NS-Cause
when prices weaken on the commodity chemicals	(NN-Textual-organization(NS-Elaboration
it buys for its own production needs ,	NS-Elaboration)
such as ethylene .	NN-Textual-organization)))
Analysts are divided	(SN-Attribution
over whether Du Pont will report much of a gain in the latest quarter from its Conoco Inc. oil company .	SN-Attribution))
The estimates for Du Pont range from $ 2.25 to $ 2.45 a share .	(NS-Comparison
In the 1988 third quarter , the company earned $ 461 million , or $ 1.91 a share , on sales of $ 7.99 billion .	NS-Comparison))
Du Pont declined to comment .	NS-Elaboration)
Monsanto Co. , too , is expected to continue reporting higher profit ,	(NS-Elaboration(NS-Summary-Evaluation-Topic(NS-Elaboration(NS-Comparison
even though its sales of crop chemicals were hurt in the latest quarter by drought in northern Europe and the western U.S .	NS-Comparison)
The St. Louis-based company is expected to report again	(NS-Summary-Evaluation-Topic(SN-Attribution
that losses in its G.D. Searle & Co. pharmaceutical business are narrowing .	SN-Attribution)
Searle continued to operate in the red through the first half of the year ,	(SN-Comparison
but Monsanto has said	(SN-Attribution
it expects Searle to post a profit for all of 1989 .	SN-Attribution))))
Most estimates for Monsanto run between $ 1.70 and $ 2 a share .	(NS-Comparison
A year ago , the company posted third-quarter profit of $ 116 million , or $ 1.67 a share , on sales of $ 2.02 billion .	NS-Comparison))
Monsanto declined to comment .	NS-Elaboration)))))
But the commodity-chemical producers are caught on the downside of a pricing cycle .	(NS-Elaboration(NS-Summary-Evaluation-Topic(NS-Cause
By some accounts on Wall Street and in the industry , the inventory reductions are near an end ,	(NN-Comparison(NS-Summary-Evaluation-Topic
which may presage firmer demand .	NS-Summary-Evaluation-Topic)
But doubters say	(SN-Attribution
growing production capacity could keep pressure on prices into the early 1990s .	SN-Attribution)))
In the latest quarter , at least , profit is expected to fall sharply .	NS-Summary-Evaluation-Topic)
For Himont Inc. , `` how far down it is ,	(NS-Elaboration(NN-Joint(NS-Elaboration(NS-Summary-Evaluation-Topic(NS-Attribution(NS-Attribution
we do n't know , ''	NS-Attribution)
says Leslie Ravitz at Salomon Brothers .	NS-Attribution)
The projections are in the neighborhood of 50 cents a share to 75 cents ,	(NS-Comparison
compared with a restated $ 1.65 a share a year earlier ,	(NS-Temporal
when profit was $ 107.8 million on sales of $ 435.5 million .	NS-Temporal)))
Himont faces lower prices for its mainstay product , polypropylene ,	(NS-Temporal(NS-Elaboration(NN-Temporal
while it goes forward with a heavy capital investment program	NN-Temporal)
to bolster its raw material supply	(NN-Joint
and develop new uses for polypropylene ,	(NS-Elaboration
whose markets include the packaging and automobile industries .	NS-Elaboration)))
The company ,	(NN-Textual-organization(NS-Elaboration
based in Wilmington , Del. ,	NS-Elaboration)
is 81 % -owned by Montedison S.p. A. , Milan ,	(NS-Elaboration
which has an offer outstanding for the Himont shares	(NS-Elaboration
it does n't already own .	NS-Elaboration)))))
At Quantum Chemical Corp. , New York , the trouble is lower prices for polyethylene , higher debt costs and the idling of an important plant due to an explosion .	(NN-Joint(NS-Elaboration(NS-Summary-Evaluation-Topic
Some analysts hedge their estimates for Quantum ,	(NN-Comparison(NS-Cause
because it is n't known when the company will book certain one-time charges .	NS-Cause)
But the estimates range from break-even to 35 cents a share .	NN-Comparison))
In the 1988 third quarter , Quantum earned $ 99.8 million , or $ 3.92 a share , on sales of $ 724.4 million .	NS-Elaboration)
Another big polyethylene producer , Union Carbide Corp. , is expected to post profit of between $ 1 a share and $ 1.25 ,	(NS-Comparison
compared with $ 1.56 a share a year earlier ,	(NS-Temporal
when the company earned $ 213 million on sales of $ 2.11 billion .	NS-Temporal))))
Himont , Quantum and Union Carbide all declined to comment .	NS-Elaboration)))
