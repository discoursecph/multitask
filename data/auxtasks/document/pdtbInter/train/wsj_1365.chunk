The Justice Department has revised certain internal guidelines and clarified others in a move that could restrict the use of criminal racketeering charges against white-collar defendants.	O	9..196
The most significant changes in department policy are new requirements that federal prosecutors avoid disrupting "the normal business functions" of companies charged under the racketeering law, a senior department official said.	O	199..427
Another important revision of department policy is a new guideline warning prosecutors "not to take steps that would harm innocent third parties" in a case brought under the racketeering law, the official, David Runkel, said.	O	430..655
The department distributed the revisions and clarifications to U.S. attorneys around the country this summer as part of a routine process of updating prosecutorial guidelines, Mr. Runkel said.	O	658..850
The changes apply to prosecutions brought under the Racketeer Influenced and Corrupt Organizations law.	O	851..954
Under that law, defendants who allegedly commit a pattern of crimes by means of a "criminal enterprise" may be charged with racketeering and forced to forfeit the proceeds of the enterprise.	O	957..1147
The RICO law has come under criticism from some defendants and defense lawyers.	O	1150..1229
They argue that the rights of RICO defendants and third parties not named in RICO indictments have been unfairly damaged.	O	1230..1351
The department's most significant clarification of existing RICO policy is a directive to prosecutors that they should seek to seize assets from defendants "in proportion" to the nature of the alleged offense, Mr. Runkel said.	O	1354..1580
"That means that if the offense deals with one part of the business, you don't attempt to seize the whole business; you attempt to seize assets related to the crime," he explained.	O	1581..1761
In general, the thrust of the department's directive is to encourage prosecutors to limit pretrial asset seizures if there are less intrusive means of protecting assets the government may subsequently be able to seize after a conviction, Mr. Runkel said.	O	1764..2018
