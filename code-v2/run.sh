#!/bin/bash

# Exit immediately if a command exits with a non-zero status.
set -e

#export PYTHONPATH=$PYTHONPATH:path_to/cnn/pycnn/ #Export pycnn in your python path if needed

# SRC=$1 #Path to the directory containing disco.py path_to/coling16/code/src/
# DATA=$2 #Path to the data directory e.g. path_to/coling16/data/
# OUTPRED=$3 #Path to the output directory
# PY=$4 #Path to your python if needed

export PYTHONPATH=$PYTHONPATH:/home/bplank/tools/cnn/pycnn/
SRC=discoursecph/multitask/code-v2/
DATA=discoursecph/multitask/data/
OUT=temp_pred_disco_mt/
PY=/home/bplank/anaconda/envs/p3k/bin/


mkdir -p ${OUT}

# Parameters used
SEED=3068836234
INDIM=64
MEM=1024
SIGMA=0.2
HLAYERS=2
HDIM=200
ITER=2

# Data sets for main task, aux views and aux tasks
TEST=${DATA}/maintask/rstdtConstituent/test/ # Dev/Test directory for the main task
TRAINRST=${DATA}/maintask/rstdtConstituent/train/ # Train directory for the main task
TRAINNUC=${DATA}/auxtasks/document/rstdtNuclearity/train/ # Train aux view Nuc
TRAINLABEL=${DATA}/auxtasks/document/rstdtRelation/train/ # Train aux view Rel
TRAINDEP=${DATA}/auxtasks/document/rstdtDependency/train/ # Train aux view Dep
RSTFINE=${DATA}/auxtasks/document/rstdtConstituentFine/train/ # Train aux view fine-grained

TRAINPDTB=${DATA}/auxtasks/document/pdtbInter/train/ # Train aux task PDTB
ASPECT=${DATA}/auxtasks/document/factbankAspect/ # Aspect
FACT=${DATA}/auxtasks/document/factbankFactuality/ # Factuality
MOD=${DATA}/auxtasks/document/factbankModality/ # Modality
TENSE=${DATA}/auxtasks/document/factbankTense/ # Tense
POL=${DATA}/auxtasks/document/factbankPolarity/ # Polarity
COREF=${DATA}/auxtasks/document/coref/ # Coreference

# EMBEDS=${DATA}/wd_embeddings/en.polyglot.txt # Pre-trained embeddings
EMBEDS=wd_embeddings/en.polyglot.txt

#OUTSCORE=experiments/conll16_expe/disco_1aux_out_indim/scores/



# Baseline system
TASK=rstc
echo OUT task:${TASK} ite:${ITER} lay:${HLAYERS} hdim:${HDIM} sigma:${SIGMA}

${PY}/python ${SRC}/disco.py --cnn-mem ${MEM} --cnn-seed ${SEED} --in_dim ${INDIM} --iters ${ITER} --h_dim ${HDIM} --sigma ${SIGMA} --embeds ${EMBEDS} --h_layers ${HLAYERS} --pred_layer 2 --train ${TRAINRST} --test ${TEST} --output ${OUT}/preds-disco.task-${TASK}.ite-${ITER}.lay-${HLAYERS}.hdim-${HDIM}.sigma-${SIGMA} --save ${OUT}/model-disco.task-${TASK}.ite-${ITER}.lay-${HLAYERS}.hdim-${HDIM}.sigma-${SIGMA}



# Best combination (all tasks are predicted on the outer most layer)
TASK=rstc-nuc-lab-rstd-mod-pdtb
echo OUT task:${TASK} ite:${ITER} lay:${HLAYERS} hdim:${HDIM} sigma:${SIGMA}

${PY}/python ${SRC}/disco.py --cnn-mem ${MEM} --cnn-seed ${SEED} --in_dim ${INDIM} --iters ${ITER} --h_dim ${HDIM} --sigma ${SIGMA} --embeds ${EMBEDS} --h_layers ${HLAYERS} --pred_layer 2 2 2 2 2 2 --train ${TRAINRST} ${TRAINNUC} ${TRAINLABEL} ${TRAINDEP} ${MOD} ${TRAINPDTB} --test ${TEST} --output ${OUT}/preds-disco.task-${TASK}.ite-${ITER}.lay-${HLAYERS}.hdim-${HDIM}.sigma-${SIGMA} --save ${OUT}/model-disco.task-${TASK}.ite-${ITER}.lay-${HLAYERS}.hdim-${HDIM}.sigma-${SIGMA}


for TASK in rstc rstc-nuc-lab-rstd-mod-pdtb
do
    python ${SRC}/parser_scorer.py --test ${TEST} --cpred ${OUT}/predsdisco.task-${TASK}.ite-${ITER}.lay-${HLAYERS}.hdim-${HDIM}.sigma-${SIGMA}
done

