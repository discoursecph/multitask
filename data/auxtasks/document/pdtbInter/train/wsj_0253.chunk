Armstrong World Industries Inc. agreed in principle to sell its carpet operations to Shaw Industries Inc.	B-entrel	9..114
The price wasn't disclosed but one analyst estimated that it was $150 million.	I-entrel	115..193
Armstrong, which has faced a takeover threat from the Belzberg family of Canada since July, said that disposing of the carpet business would improve "total financial performance."	O	196..375
The move also would allow the company to concentrate on core businesses, which include ceramic tile, floor coverings and furniture.	O	376..507
Moreover, such a sale could help Armstrong reassure its investors and deter the Belzbergs, who own a 9.85% stake in the Lancaster, Pa., company.	B-restatement	510..654
Analysts expect Armstrong to use proceeds of the sale to reduce debt, buy back stock or perhaps finance an acquisition.	I-restatement	655..774
The carpet division had 1988 sales of $368.3 million, or almost 14% of Armstrong's $2.68 billion total revenue.	B-entrel	777..888
The company has been manufacturing carpet since 1967.	B-entrel	889..942
Recently it upgraded its plants so that it could make stain-resistant products with higher quality dyes.	I-entrel	943..1047
For the past year or two, the carpet division's operating profit margins have hovered around 5%, high by industry standards, but disappointing compared with the 13% to 19% margins for two of Armstrong's chief businesses, flooring and building products.	O	1050..1302
Analysts hailed the planned transaction as being beneficial to Armstrong and Shaw, the market leader in the U.S. carpet industry, with an estimated 17% to 20% share.	B-cause	1305..1470
Shaw, based in Dalton, Ga., has annual sales of about $1.18 billion, and has economies of scale and lower raw-material costs that are expected to boost the profitability of Armstrong's brands, sold under the Armstrong and Evans-Black names.	I-cause	1471..1711
Yesterday, in composite trading on the New York Stock Exchange, Shaw's shares closed ex-dividend at $26.125, up $2.25.	B-conjunction	1714..1832
Armstrong's shares, also listed on the Big Board, closed at $39.125, up 12.5 cents.	I-conjunction	1833..1916
Yesterday, Armstrong reported flat earnings for the third quarter and nine months, worsened by the stock dilution of an employee stock ownership plan adopted earlier this year.	O	1919..2095
For the quarter, earnings were $47 million, or 92 cents a share, including a one-time gain of $5.9 million.	B-contrast	2098..2205
In the year-ago quarter, earnings were $42.9 million, or 93 cents a share.	I-contrast	2206..2280
Yesterday, Armstrong announced an agreement to sell its small Applied Color Systems unit to a subsidiary of the Swiss company, Brauerei Eichof Ltd.	B-entrel	2283..2430
The price wasn't disclosed.	I-entrel	2431..2458
Armstrong expects to close the sale of the color unit in late November and the carpet sale in December, with the gains to be applied to fourth quarter or first-quarter results.	O	2461..2637
