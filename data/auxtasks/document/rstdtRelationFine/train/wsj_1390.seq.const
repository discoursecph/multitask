Maidenform Inc. loves to be intimate with its customers , but not with the rest of the public .	(summary
The 67-year-old maker of brassieres , panties , and lingerie enjoys one of the best-known brand images ,	(background(elaboration-additional(elaboration-additional(elaboration-additional(elaboration-additional(Contrast
but its financial profile is closely guarded by members of the founding family .	Contrast)
`` There are very few companies	(elaboration-additional(elaboration-additional(attribution(elaboration-object-attribute
that can boast of such a close-knit group , ''	elaboration-object-attribute)
says Robert A. Brawer , 52 years old ,	(elaboration-object-attribute(elaboration-object-attribute
recently named president ,	elaboration-object-attribute)
succeeding Beatrice Coleman , his mother-in-law ,	(elaboration-object-attribute
who remains chairman .	elaboration-object-attribute)))
`` We are a vanishing breed , ''	(attribution
he muses .	attribution))
Mrs. Coleman , 73 ,	(elaboration-additional(Same-Unit(elaboration-additional
who declined to be interviewed ,	elaboration-additional)
is the Maidenform strategist .	Same-Unit)
Sales have tripled during her 21-year tenure to about $ 200 million in 1988 .	(elaboration-additional
Maidenform says	(antithesis(attribution
it is very profitable	attribution)
but declines to provide specifics .	antithesis)))))
The company sells image .	(elaboration-additional(elaboration-additional(elaboration-additional(example
Its current ad campaign ,	(Same-Unit(elaboration-additional
on which Maidenform has spent more than $ 15 million since fall 1987 ,	elaboration-additional)
does n't even show its underwear products , but rather men like Christopher Reeve , star of the `` Superman '' movies ,	(elaboration-object-attribute
talking about their lingerie turn-ons .	elaboration-object-attribute)))
The Maidenform name `` is part of American pop culture , ''	(elaboration-additional(attribution
says Joan Sinopoli , account supervisor of the campaign by Levine , Huntley , Schmidt & Beaver , a New York ad firm .	attribution)
Maidenform generated such memorable campaigns as `` I dreamed	(Same-Unit(attribution
I . . . in my Maidenform bra , ''	attribution)
and `` The Maidenform woman .	(elaboration-additional
You never know	(attribution
where she 'll turn up . ''	attribution)))))
`` Capitalizing on the brand is key , ''	(attribution
says Mr. Brawer ,	(elaboration-object-attribute
whose immediate plans include further international expansion and getting better control of distribution outside the U.S .	elaboration-object-attribute)))
`` The intimate apparel industry is perceived to be a growth industry	(elaboration-additional(attribution(circumstance
and clearly { Maidenform } is a force	(elaboration-object-attribute
to be reckoned with , ''	elaboration-object-attribute))
says David S. Leibowitz , a special situations analyst at American Securities Corp. in New York .	attribution)
Although working women are `` forced to wear the uniform of the day ,	(attribution(antithesis(purpose
to retain their femininity	purpose)
they are buying better quality , more upscale intimate apparel , ''	antithesis)
he said .	attribution))))
Although Mr. Brawer 's appointment as president was long expected ,	(explanation-argumentative(antithesis(explanation-argumentative(elaboration-additional(temporal-after(concession
the move on Sept. 25 precipitated the resignation of Alan Lesk as senior vice president of sales and merchandising .	concession)
Three days later , Mr. Lesk was named president and chief executive officer of Olga Co. , a competing intimate apparel division of Warnaco Inc .	(elaboration-additional
Warnaco also owns Warners , another major intimate apparel maker .	elaboration-additional))
Mr. Lesk could n't be reached to comment .	elaboration-additional)
But Maidenform officials say	(attribution
that	(Same-Unit
after spending 24 years at Maidenform ,	(circumstance
Mr. Lesk , 48 , made it clear	(attribution
he wanted the top job .	attribution)))))
`` If you want the presidency of the company ,	(elaboration-additional(elaboration-additional(attribution(condition
this is n't the firm	(elaboration-object-attribute
to work for , ''	elaboration-object-attribute))
says James Mogan , 45 ,	(elaboration-object-attribute
who was named senior vice president of sales ,	(elaboration-additional
assuming some of the responsibilities of Mr. Lesk .	elaboration-additional)))
The company downplayed the loss of Mr. Lesk	(List
and split his merchandising responsibilities among a committee of four people .	List))
`` My style is less informal , ''	(attribution
Mr. Brawer says .	attribution)))
Top officers insist	(elaboration-additional(elaboration-additional(elaboration-additional(elaboration-additional(attribution
Maidenform 's greatest strength is its family ownership .	attribution)
`` You ca n't go anywhere in this company	(attribution(circumstance
and find an organizational chart , ''	circumstance)
one delights .	attribution))
`` It is fun competing as a private company , ''	(comparison(reason(attribution
Mr. Brawer says .	attribution)
`` You can think long range . ''	reason)
Other major players in intimate apparel apparently feel the same way .	(example
Warnaco was taken private by Spectrum Group in 1986 for about $ 487 million .	(List
And last year , Playtex Holdings Inc. went private for about $ 680 million .	(List(temporal-before
It was then split into Playtex Apparel Inc. , the intimate apparel division , and Playtex Family Products Corp. ,	(elaboration-additional
which makes tampons , hair-care items and other products .	elaboration-additional))
Publicly traded VF Corp. ,	(Same-Unit(elaboration-object-attribute
which owns Vanity Fair ,	elaboration-object-attribute)
and Sara Lee Corp. ,	(Same-Unit(elaboration-object-attribute
which owns Bali Co. ,	elaboration-object-attribute)
are also strong forces in intimate apparel .	Same-Unit)))))))
Buy-out offers for Maidenform are n't infrequent ,	(otherwise(evidence(antithesis(attribution
says Executive Vice President David C. Masket ,	attribution)
but they are n't taken very seriously .	antithesis)
When he gets calls ,	(attribution(circumstance
`` I do n't even have to consult '' with Mrs. Coleman ,	circumstance)
Mr. Masket says .	attribution))
The company could command a good price in the market .	(evidence
`` Over the past three and a half years , apparel companies ,	(attribution(Same-Unit(elaboration-object-attribute
many of whom have strong brand names ,	elaboration-object-attribute)
have been bought at about 60 % of sales , ''	Same-Unit)
says Deborah Bronston , Prudential-Bache Securities Inc. apparel analyst .	attribution))))
Mr. Brawer , along with Mrs. Coleman and her daughter , Elizabeth , an attorney	(background(elaboration-set-member(elaboration-additional(elaboration-additional(elaboration-additional(Same-Unit(elaboration-additional
who is vice chairman ,	elaboration-additional)
are the family members	(elaboration-object-attribute
involved in the operations of Maidenform ,	(elaboration-additional
which employs about 5,000 .	elaboration-additional)))
Mr. Brawer 's wife , Catherine , and Robert Stroup , Elizabeth 's husband , round out the five-member board .	elaboration-additional)
Each has an equal vote at the monthly meetings .	elaboration-additional)
`` We are all very amiable , ''	(attribution
Mr. Brawer says .	attribution))
Executives say	(attribution
Mrs. Coleman is very involved in the day-to-day operations , especially product development .	attribution))
In the late 1960s she designed a lightweight stretch bra	(List(elaboration-object-attribute
that boosted sales .	elaboration-object-attribute)
Her father , William Rosenthal , designed the then-dress making company 's first bra in the 1920s ,	(List(elaboration-object-attribute
which	(Same-Unit(attribution
he said	attribution)
gave women a `` maiden form ''	(comparison
compared with the `` boyish form ''	(elaboration-object-attribute
they got from the `` flat bandages ''	(elaboration-object-attribute
used for support at the time .	elaboration-object-attribute)))))
While Mr. Rosenthal introduced new undergarment designs ,	(List(comparison
his wife , Ida , concentrated on sales and other financial matters .	comparison)
The name Maidenform was coined by a third business partner , Enid Bissett .	List)))))))
The company has 14 plants and distribution facilities in the U.S. , Puerto Rico , other parts of the Caribbean and Ireland .	(List
Maidenform products are mainly sold at department stores ,	(elaboration-additional
but the company has quietly opened a retail store of its own in Omaha , Neb .	(List
, and has 24 factory outlets , with plans	(elaboration-object-attribute
to add more .	elaboration-object-attribute)))))
Before joining Maidenform in 1972 ,	(elaboration-additional(elaboration-additional(temporal-before
Mr. Brawer ,	(Same-Unit(elaboration-additional
who holds a doctoral degree in English from the University of Chicago ,	elaboration-additional)
taught at the University of Wisconsin .	Same-Unit))
As a senior vice president , he has headed the company 's designer lingerie division , Oscar de la Renta , since its inception in 1988 .	(elaboration-additional
To maintain exclusivity of that designer line ,	(purpose
it is n't labeled with the Maidenform name .	purpose)))
While the company has always been family-run ,	(example(circumstance
Mr. Brawer is n't the first person	(elaboration-object-attribute
to marry into the family	(Sequence
and subsequently head Maidenform .	Sequence)))
Mrs. Coleman 's husband , Joseph , a physician , succeeded Mrs. Rosenthal as president	(List
and served in that post until his death in 1968 .	List)))))
