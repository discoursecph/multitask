Under attack by its own listed companies and powerful floor traders, the New York Stock Exchange is considering reinstituting a "collar" on program trading that it abandoned last year, according to people familiar with the Big Board.	O	9..242
The exchange also may step up its disclosure of firms engaged in program trading, these people said.	O	245..345
Big Board officials wouldn't comment publicly.	B-contrast	348..394
But in an interview in which he called the stock market's volatility a "national problem," Big Board Chairman John J. Phelan Jr. said, "We are going to try to do some things in the short intermediate term" to help the situation.	I-contrast	395..623
Mr. Phelan has been viewed by many exchange members as being indifferent to stock-price swings caused by program trades.	B-conjunction	626..746
He said he is "very surprised" by the furor over program trading and the exchange's role in it that has raged in recent days.	I-conjunction	747..872
Mr. Phelan said that the Big Board has been trying to deal quietly with the issue, but that banning computer-assisted trading strategies entirely, as some investors want, would be like "taking everybody out of an automobile and making them ride a horse."	O	875..1129
The exchange has a board meeting scheduled for tomorrow, and it is expected that some public announcement could be made after that.	O	1132..1263
Big Board officials have been under seige from both investors and the exchange's own floor traders since the Dow Jones Industrial Average's 190-point tumble on Oct. 13.	O	1266..1434
Mr. Phelan hasn't been making public remarks in recent days, and many people have urged him to take more of a leadership role on the program trading issue.	O	1435..1590
What the Big Board is considering is re-establishing a "collar" on program trading when the market moves significantly.	B-entrel	1593..1712
Early last year, after a 140-point, one-day drop in the Dow, the Big Board instituted the collar, which banned program trading through the Big Board's computers whenever the Dow moved 50 points up or down in a day.	B-concession	1713..1927
It didn't work.	I-concession	1928..1943
"The collar was penetrated on a number of occasions," meaning securities firms figured out ways to conduct program trades to circumvent the collar and use the Big Board's electronic trading system, Mr. Phelan said.	O	1946..2160
That was when the exchange took a new tack by publishing monthly statistics listing the top 15 program trading firms.	O	2161..2278
Exchange officials emphasized that the Big Board is considering a variety of actions to deal with program trading.	O	2281..2395
People familiar with the exchange said another idea likely to be approved is expanding the monthly reports on program trading to cover specific days or even hours of heavy program trading and who was doing it.	O	2396..2605
Meanwhile, another big Wall Street brokerage firm joined others that have been pulling back from program trading.	O	2608..2721
American Express Co. 's Shearson Lehman Hutton Inc. unit said it ceased all index-arbitrage program trading for client accounts.	B-entrel	2722..2850
In stock-index arbitrage, traders buy and sell large amounts of stock with offsetting trades in stock-index futures to profit from fleeting price discrepancies between the two markets.	I-entrel	2851..3035
Shearson, which in September was the 11th-biggest program trader on the Big Board, had already suspended stock-index arbitrage for its own account.	O	3038..3185
Also, CS First Boston Inc. 's First Boston Corp. unit, the fifth-biggest program trader in September, is "preparing a response" to the program-trading outcry, officials of the firm said.	B-entrel	3188..3374
First Boston is one of the few major Wall Street firms that haven't pulled back from program trading in recent days.	I-entrel	3375..3491
Mr. Phelan is an adroit diplomat who normally appears to be solidly in control of the Big Board's factions.	B-concession	3494..3601
But he has been getting heat from all sides over program trading.	I-concession	3602..3667
Mr. Phelan's recent remarks that investors simply must get used to the stock-market volatility from program trading have drawn criticism from both the exchange's stock specialists, who make markets in individual stocks, and from many companies that have shares listed on the Big Board.	O	3670..3955
Mr. Phelan said that his predicting continued volatility is just "how the world is.	O	3958..4041
If bringing the message is a crime, I'm guilty of it."	O	4042..4096
But he said this doesn't mean he is satisfied with the market's big swings.	O	4097..4172
We're trying to take care of a heck of a lot of constituents," Mr. Phelan said.	B-entrel	4175..4255
Each one has a different agenda.	B-instantiation	4256..4290
For example, in a special meeting Monday with Mr. Phelan, senior officials of some of the Big Board's 49 stock specialist firms complained that the exchange is no longer representing their interests.	B-instantiation	4293..4492
"We are looking for representation we haven't had," a specialist said.	I-instantiation	4493..4563
"We've had dictation."	O	4564..4586
After another session Mr. Phelan held yesterday with major brokerage firms such as Morgan Stanley & Co., Goldman, Sachs & Co., PaineWebber Group Inc. and First Boston -- all of which have engaged in program trading -- an executive of a top brokerage firm said, "Clearly, the firms want the exchange to take leadership."	O	4589..4908
Many specialist firms resent the Big Board's new "basket" product that allows institutions to buy or sell all stocks in the Standard & Poor's 500-stock index in one shot.	B-restatement	4911..5081
Ultimately, the specialists view this as yet another step toward electronic trading that could eventually destroy their franchise.	I-restatement	5082..5212
"His {Phelan's} own interests are in building an electronic marketplace," said a market maker.	O	5215..5309
The basket product, while it has got off to a slow start, is being supported by some big brokerage firms -- another member of Mr. Phelan's splintered constituency.	O	5312..5475
Mr. Phelan has had difficulty convincing the public that the Big Board is serious about curbing volatility, especially as the exchange clearly relishes its role as the home for $200 billion in stock-index funds, which buy huge baskets of stocks to mimic popular stock-market indexes like the Standard & Poor's 500, and which sometimes employ program trading.	O	5478..5836
The Big Board wants to keep such index funds from fleeing to overseas markets, but only as long as it "handles it intelligently," Mr. Phelan said.	O	5837..5983
Despite what some investors are suggesting, the Big Board isn't even considering a total ban on program trading or stock futures, exchange officials said.	O	5986..6140
Most revisions it will propose will be geared toward slowing down program trading during stressful periods, said officials working with the exchange.	O	6141..6290
Computers have made trading more rapid, but that can be fixed with some fine-tuning.	O	6291..6375
I think if you {can} speed things up, you can slow them down," Mr. Phelan said.	B-entrel	6376..6456
"That's different than wrecking them."	I-entrel	6457..6495
While volatility won't go away, he said, "Volatility is greater than program trading.	O	6498..6583
What I'm trying to say to people is, it's proper to worry about program trading, but it's only a piece of the business."	O	6584..6704
For example, Mr. Phelan said that big institutions have so much control over public investments that they can cause big swings in the market, regardless of index arbitrage.	O	6707..6879
"A lot of people would like to go back to 1970," before program trading, he said.	O	6880..6961
"I would like to go back to 1970.	O	6962..6995
But we're not going back to 1970."	O	6996..7030
Indeed, Mr. Phelan said that if stock-market volatility persists, the U.S. may lose its edge as being the best place to raise capital.	O	7033..7167
"Japan's markets are more stable," he said.	O	7168..7211
"If that continues, a significant number of {U.S.} companies will go over there to raise money."	O	7212..7308
In coming days, when the Big Board formulates its responses to the program-trading problem, Mr. Phelan may take a more public role in the issue.	O	7311..7455
Lewis L. Glucksman, vice chairman of Smith Barney, Harris Upham & Co., said: "This is a problem that's taking on a life of its own.	O	7458..7589
The program trading situation seems to have driven individual investors as well as others out of the market, and even Europeans are suspicious.	B-cause	7590..7733
The exchange should take a pro-active position."	I-cause	7734..7782
For now, however, Mr. Phelan said: "I refuse to get out there and tell everybody everything is hunky-dory.	O	7785..7891
We have a major problem, and that problem is volatility."	O	7892..7949
Craig Torres contributed to this article.	O	7952..7993
