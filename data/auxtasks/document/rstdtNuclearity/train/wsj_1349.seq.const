The House Appropriations Committee approved an estimated $ 2.85 billion in emergency funding	(NS(NS(NS(NS(NS(NS
to assist California 's recovery from last week 's earthquake	(NN
and to extend further aid to East Coast victims of Hurricane Hugo .	NN))
The package was termed excessive by the Bush administration ,	NS)
but it also provoked a struggle with influential California lawmakers	(NS
who sought unsuccessfully to add nearly $ 1 billion more and waive current restrictions	(NS
to expedite the distribution of funds .	NS)))
By a 26-7 margin , the committee scuttled the more expensive alternative ,	(NS(NN
and the debate forced a strained confrontation between Appropriations Committee Chairman Jamie Whitten	(NN(NS
( D. , Miss . )	NS)
and his party 's largest state delegation in the House .	NN))
`` I have no regrets about going forward , ''	(NS(NS
said Rep. Vic Fazio	(NS(NS
( D. , Calif. ) ,	NS)
who sought later to play down the sometimes hostile tone of the long evening meeting .	NS))
`` We are the Golden State , ''	(NN(NS
Mr. Fazio said ,	NS)
`` and there is a certain amount of jealousy . ''	NN))))
The $ 2.85 billion package incorporates $ 500 million for small-business loans , $ 1 billion in highway construction funds , and $ 1.35 billion	(NS(NS
divided between general emergency assistance and a reserve	(NS
to be available to President Bush	(NS
to meet unanticipated costs from the two disasters .	NS)))
The funding is attached to a stopgap bill	(NS(NS
to keep most of the government operating through Nov. 15 .	NS)
The measure is expected to come before the House today ,	(NN
and Congress is under pressure	(NS
to complete action before midnight EDT tomorrow ,	(NS
when the current continuing resolution expires .	NS))))))
Given California 's size and political importance ,	(NS(SN(SN
the White House is eager to appear generous .	SN)
But in private meetings yesterday , Budget Director Richard Darman argued	(NS(SN
that only $ 1.5 billion in new federal appropriations are needed	(NS
to supplement existing resources .	NS))
A White House budget office analysis estimates	(NS(NN(SN
that $ 500 million	(NS(NN(NS
-- or half the level in the committee bill --	NS)
is needed for highway assistance	NN)
to meet California 's needs ,	NS))
and the administration rejects the notion	(NN(NS
that new appropriations are needed	(NS
to finance disaster loans by the Small Business Administration .	NS))
`` Everybody appreciates	(NN(NS(SN
that it is a national disaster	(NN
and that we 've got to address it , ''	NN))
said Mr. Darman ,	(NS
who came to the Capitol	(NS
to meet with Mr. Whitten and California lawmakers before the committee session .	NS)))
`` I would hope very much	(SN
that we would n't end up in a kind of situation	(NS
where you have a bidding war and then a veto threat . ''	NS)))))
Although this White House pressure was clearly a factor among committee Republicans ,	(NS(NS(SN
no single influence was greater than Mr. Whitten .	SN)
A master of pork-barrel politics , he had crafted the $ 2.85 billion package in vintage style	(NN
and used the full force of his chairmanship	(NS
to keep the proposal intact	(NN
and dismiss any alternative .	NN))))
When Mr. Fazio offered the California-backed $ 3.84 billion plan ,	(NS(SN
Mr. Whitten insisted	(SN
that the full 14 pages be read aloud by the panel 's clerk	(NS
to underscore the range of legislative changes	(NS
also sought by the delegation .	NS))))
On the chairman 's motion , the California package was subsequently reduced to less-binding report language ,	(NN
and	(NN
even when this was accepted as such on a voice vote ,	(SN
Mr. Whitten pointedly opposed it .	SN))))))))
More important than money in many cases are waivers	(NS(NS
California is seeking on current restrictions	(NS
covering federal highway funds , such as a $ 100 million cap	(NS
on how much any single state can receive in emergency funds in a year .	NS)))
Mr. Whitten 's package appears to accomplish this purpose ,	(NS(NN
but the state faces more resistance in its bid for an extended waiver	(NS
on having to put up any matching funds on repairs	(NS
completed in the next six months .	NS)))
A member in the House leadership and skilled legislator , Mr. Fazio nonetheless found himself burdened not only by California 's needs but by Hurricane Hugo amendments	(NS(NS
he accepted in a vain effort	(NS
to build support in the panel .	NS))
The California Democrat appeared embarrassed by provisions	(NS(NN(NS
inserted on behalf of owners of private beaches in the Virgin Islands ,	NS)
and lumber interests sought to add another $ 100 million in federal aid	(NS
to plant timber on private land in North and South Carolina .	NS))
California 's high-priced real estate puts it in an awkward position , too .	(NS
One provision last night would have raised the cap on disaster loans to $ 500,000 from $ 100,000 per household	(NS
to accommodate San Francisco losses .	NS))))))))
