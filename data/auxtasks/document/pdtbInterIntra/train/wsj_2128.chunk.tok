A federal appeals court struck down a natural-gas regulation that had prevented pipeline companies from passing to customers part of $ 1 billion in costs from controversial " take-or-pay " contracts .	root	9..205
The court , in a 3-0 ruling , threw out a deadline set by the Federal Energy Regulatory Commission for settling old contract disputes over gas that the pipeline companies reserved but did n't use .	norel	208..401
FERC 's regulation had given pipelines until March 31 , 1989 , to pass on to customers as much as 50 % of the costs of buying out their broken contracts , which were made with producers when gas prices were high and supplies short .	norel	404..630
A majority of old contracts were renegotiated by the deadline and settled at steep discounts .	norel	633..726
But pipeline companies estimate they still face $ 2.4 billion in liabilities from unresolved disputes , including $ 1 billion they fear they wo n't be able to pass on to customers .	contrast	727..903
According to industry lawyers , the ruling gives pipeline companies an important second chance to resolve remaining disputes and take advantage of the cost-sharing mechanism .	norel	906..1079
The court left open whether FERC could reimpose a new deadline later .	asynchronous	1080..1149
The court , agreeing with pipeline companies , found the March 31 deadline was " arbitrary and capricious " and " highly prejudicial to the bargaining power of pipelines " that were forced to negotiate settlement of the old take-or-pay contracts to meet the deadline .	norel	1152..1413
A report last month by the Interstate Natural Gas Association of America found that pipelines ' settlement costs had jumped in the three months before the deadline to 39 cents on the dollar , from 22 cents on the dollar in 1988 .	norel	1416..1642
The court ordered FERC to justify within 60 days not only its cost-sharing deadline , but other major elements of its proposed regulation for introducing more competition into natural-gas transportation .	norel	1645..1847
The court also questioned a crediting mechanism that could be used to resolve take-or-pay liabilities .	conjunction	1848..1950
The complex regulation , known in the industry as Order 500 , has been hotly contested by all sides , including natural-gas producers , pipelines , local distribution companies and consumers .	norel	1953..2139
The court 's decision would allow FERC to change some of its provisions , but ensures it will be reviewed again quickly by the court .	entrel	2140..2271
