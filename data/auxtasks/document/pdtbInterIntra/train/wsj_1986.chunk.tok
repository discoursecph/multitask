" The New Crowd " by Judith Ramsey Ehrlich and Barry J. Rehfeld ( Little , Brown , 444 pages , $ 19.95 ) , describes the displacing of the old " our crowd " Jewish Wall Street banking grandees by such new business barons as Saul Steinberg , Carl Icahn , Sanford Weill and Bruce Wasserstein .	root	9..286
Its many lively stories include the Gutfreund-Postel holiday cheer imbroglio .	entrel	287..364
These two New Crowd families lived in the same apartment building , with the Postel penthouse perched on top of the Gutfreund duplex .	entrel	365..497
The penthouse elevator started up from the Gutfreund landing	entrel	498..558
and Susan Gutfreund used to turn off its light , to give the impression that there was no higher floor .	conjunction	559..662
Eventually , Mr. Postel broke his toe in the dark .	norel	663..712
Then the Gutfreunds determined to put up a 22-foot Christmas tree , weighing a quarter of a ton , to amaze their holiday guests .	asynchronous	715..841
For this , a crane needed to be mounted on the Postels ' terrace .	contrast	842..905
The Postels did not give permission .	contrast	906..942
But the Gutfreund workers went ahead anyway , only to be captured " in flagrante " by Joan Postel , who called the police .	concession	943..1061
Before the Gutfreunds finally left this unfriendly environment for a prodigious duplex on Fifth Avenue and an 18th-century mansion with a specially excavated $ 1 million garage in Paris , the Postels had obtained an injunction to prevent any future hoisting of trees , and in a neighborly spirit hit both the Gutfreunds and the building with a $ 35 million lawsuit .	norel	1064..1425
Nothing less , it seemed , could console them for their traumas .	cause	1426..1488
Where had all the money come from ?	norel	1491..1525
The young John Gutfreund had been discovered by Billy Salomon of Salomon Bros. when he was still a bearded liberal , and put to work as a trader , and then as a rough-and-tumble syndicator .	norel	1526..1713
" ' Get off your ... , ' he would bellow , " say the authors .	instantiation	1714..1770
Rising in the firm , he became powerful and bland , though his new wife , Susan , made him shine in the gossip columns with her profligate spending habits and flamboyant frocks .	contrast	1771..1944
After he had been head of the company for 3 1/2 years , he and his partners sold it to Phibro , a powerful commodity trading outfit , for $ 550 million in Phibro stock .	entrel	1945..2109
Limited partner Billy Salomon , whose family name had been on the firm 's door for 70 years and who had hoped it would be there forever , was not consulted .	entrel	2110..2263
Mr. Gutfreund collected $ 32 million , while Billy Salomon got $ 10 million , much less than if he had conducted the sale .	conjunction	2264..2382
" I felt betrayed , " he later said .	asynchronous	2383..2416
Worse , Salomon 's timing had been off .	contrast	2417..2454
Its profits , unlike Phibro 's , soared over the next two years , and had it held out , Salomon could have gotten an even bigger bundle .	cause	2455..2586
The book also recounts the not dissimilar maneuvers surrounding the changing of the guard at Lehman Bros. and other grand old firms .	norel	2589..2721
Often the genteel , conservative , long-term-oriented investment bankers were displaced by crude traders : " When angered , he cursed so forcefully that his face reddened and his pale-blue eyes narrowed into tiny slits , " the authors say of Lehman 's Lewis Glucksman .	restatement	2722..2982
The earlier generation of " our crowd " bankers -- Belmonts , Warburgs , Lehmans , Baches and Schiffs -- had stressed above all probity , tradition , continuity and reputation .	norel	2985..3154
They were old-fashioned elegant gentlemen , who happened to be of German Jewish extraction .	restatement	3155..3245
But in the harsh world of today 's Wall Street they have lost out to more aggressive and sometimes less scrupulous successors .	contrast	3246..3371
The cuckoo prefers the nests of other birds and heaves out their eggs .	norel	3372..3442
But the old guard hired the New Crowd people : It brought in its own cuckoos .	concession	3443..3519
So , as the Old Crowd toppled from the branch , it should n't have been too surprised .	cause	3520..3603
The old guard had every right , however , to disdain the newcomers ' new ways of making money , such as greenmail .	contrast	3606..3716
( A Fortune article on Saul Steinberg was entitled , " Fear and Loathing in the Corporate Boardrooms . " )	conjunction	3717..3817
Their other staple has been corporate takeovers , often hostile and financed by junk bonds .	norel	3818..3908
Hostile takeovers are quite a new phenomenon .	restatement	3909..3954
Sometimes they are constructive , but often not .	entrel	3955..4002
First , by making management focus on short-term results , they inhibit building for the future -- just the opposite of Japan .	norel	4003..4127
Second , a long-term shareholder of a good company need n't worry too much when the stock price drops temporarily : It will bounce back .	list	4128..4261
But if a raider takes over when the stock is weak , the shareholder never gets his recovery .	contrast	4262..4353
The raiders , meanwhile , have evolved their own pattern for spending their new millions .	norel	4356..4443
As described in " The New Crowd , " they take on ambitious new wives , move to Greenwich , Conn. , or Bedford , N.Y. , buy OK pictures , and let their wives share the wealth with decorators .	instantiation	4444..4625
Having donated heavily to museums , they demand a place on their boards .	conjunction	4626..4697
The book is patronizing about this nouveau riche struggle for respectability , which has its tawdry aspects .	norel	4700..4807
However , on balance , the charity game helps America .	contrast	4808..4860
If those who have the money do n't get involved with the museums and the charities , then City Hall will do it , badly .	cause	4861..4977
It has been rightly observed that the main thing wrong with tainted money is , t' ai nt enough of it .	restatement	4978..5076
A handful of the New Crowd operators have crossed the line from the immoral to the illegal , and have ended up in the slammer or paying huge fines :	norel	5079..5225
Ivan Boesky , Dennis Levine , Martin Siegel , Victor and Steven Posner , and now Michael Milken and perhaps Leona Helmsley .	norel	5226..5345
The glitzy office that Ivan Boesky vacated for a prison cell had previously contained commodity operators Marc Rich and " Pinky " Green , today fugitives from a potential century apiece of jail sentences .	instantiation	5346..5547
The Old Crowd is deeply concerned by the backlash from all this .	norel	5550..5614
However , the phenomenon is not specifically Jewish .	contrast	5615..5666
It has always been true that those outside the club want to climb in , and that a few will cut corners in the process .	restatement	5667..5784
Some pretty seamy stuff built the turn-of-the-century families ' Fifth Avenue and Newport palazzi and endowed their daughters ' weddings to foreign noblemen .	restatement	5785..5940
Mr. Boesky was a piker compared to Jay Gould and Jim Fiske , and Commodore Vanderbilt thought nothing of bribing judges and legislators .	conjunction	5941..6076
So who knows ?	norel	6077..6090
In a generation or two some of the New Crowd may attain true respectability , perhaps to be displaced in turn by a later flock of unscrupulous raptors .	norel	6091..6241
Or perhaps Wall Street , when it has suffered enough , will realize that finance is a service industry , and change its ethos .	alternative	6242..6365
Mr. Train is president of Train , Smith Investment Counsel , New York .	norel	6368..6436
