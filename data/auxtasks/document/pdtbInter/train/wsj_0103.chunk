The Labor Department cited USX Corp. for numerous health and safety violations at two Pennsylvania plants, and proposed $7.3 million in fines, the largest penalty ever proposed for alleged workplace violations by an employer.	O	9..234
The department's Occupational Safety and Health Administration proposed fines of $6.1 million for alleged violations at the company's Fairless Hills, Pa., steel mill; that was a record for proposed penalties at any single facility.	O	237..468
OSHA cited nearly 1,500 alleged violations of federal electrical, crane-safety, record-keeping and other requirements.	O	469..587
A second citation covering the company's Clairton, Pa., coke works involved more than 200 alleged violations of electrical-safety and other requirements, for which OSHA proposed $1.2 million in fines.	O	590..790
Labor Secretary Elizabeth Dole said, "The magnitude of these penalties and citations is matched only by the magnitude of the hazards to workers which resulted from corporate indifference to worker safety and health, and severe cutbacks in the maintenance and repair programs needed to remove those hazards."	O	793..1100
OSHA said there have been three worker fatalities at the two plants in the past two years and 17 deaths since 1972.	O	1103..1218
Gerard Scannell, the head of OSHA, said USX managers have known about many of the safety and health deficiencies at the plants for years, "yet have failed to take necessary action to counteract the hazards."	O	1219..1426
Particularly flagrant," Mrs. Dole said, "are the company's numerous failures to properly record injuries at its Fairless works, in spite of the firm promise it had made in an earlier corporate-wide settlement agreement to correct such discrepancies.	B-entrel	1429..1680
That settlement was in April 1987.	I-entrel	1681..1715
A USX spokesman said the company hadn't yet received any documents from OSHA regarding the penalty or fine.	O	1718..1825
"Once we do, they will receive very serious evaluation," the spokesman said.	O	1826..1902
"No consideration is more important than the health and safety of our employees."	O	1903..1984
USX said it has been cooperating with OSHA since the agency began investigating the Clairton and Fairless works.	O	1987..2099
He said that, if and when safety problems were identified, they were corrected.	O	2100..2179
The USX citations represented the first sizable enforcement action taken by OSHA under Mr. Scannell.	B-entrel	2182..2282
He has promised stiffer fines, though the size of penalties sought by OSHA have been rising in recent years even before he took office this year.	I-entrel	2283..2428
The big problem is that USX management has proved unwilling to devote the necessary resources and manpower to removing hazards and to safeguarding safety and health in the plants," said Linda Anku, OSHA regional administrator in Philadelphia.	B-entrel	2431..2674
USX has 15 working days to contest the citations and proposed penalties, before the independent Occupational Safety and Health Review Commission.	I-entrel	2675..2820
Before the USX case, OSHA's largest proposed fine for one employer was $4.3 million for alleged safety violations at John Morrell & Co., a meatpacking subsidiary of United Brands Co., Cincinnati.	B-entrel	2823..3018
The company is contesting the fine.	I-entrel	3019..3054
