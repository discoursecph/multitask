Interprovincial Pipe Line Co. said	(NS-circumstance(NS-evaluation(NS-explanation-argumentative(SN-attribution
it will delay a proposed two-step , 830 million Canadian-dollar	(NS-reason(NN-Same-Unit(NS-restatement
( US $ 705.6 million )	NS-restatement)
expansion of its system	NN-Same-Unit)
because Canada 's output of crude oil is shrinking .	NS-reason))
Interprovincial , Canada 's biggest oil pipeline operator and a major transporter of crude to the U.S. , said	(NS-elaboration-additional(NS-elaboration-additional(SN-attribution
revised industry forecasts indicate	(SN-attribution
that Canadian oil output will total about 1.64 million barrels a day by 1991 , 8 % lower than a previous estimate .	SN-attribution))
Canadian crude production averaged about 1.69 million barrels a day during 1989 's first half , about 1 % below the 1988 level .	NS-elaboration-additional)
`` The capability of existing fields	(NS-elaboration-additional(NS-attribution(NS-circumstance(NN-List(NN-Same-Unit(NS-elaboration-object-attribute
to deliver oil	NS-elaboration-object-attribute)
is dropping , ''	NN-Same-Unit)
and oil exploration activity is also down dramatically ,	NN-List)
as many producers shift their emphasis to natural gas ,	NS-circumstance)
said Ronald Watkins , vice president for government and industry relations with Interprovincial 's parent , Interhome Energy Inc .	NS-attribution)
Mr. Watkins said	(NS-elaboration-additional(SN-attribution
volume on Interprovincial 's system is down about 2 % since January	(NN-Cause-Result(NN-List
and is expected to fall further ,	NN-List)
making expansion unnecessary until perhaps the mid-1990s .	NN-Cause-Result))
`` There has been a swing of the pendulum back to the gas side , ''	(NS-attribution
he said .	NS-attribution)))))
Many of Canada 's oil and gas producers say	(NS-interpretation(NN-List(SN-attribution
the outlook for natural gas is better	(NN-Comparison
than it is for oil ,	NN-Comparison))
and have shifted their exploration and development budgets accordingly .	(NS-explanation-argumentative
The number of active drilling rigs in Canada is down 30 % from a year ago ,	(NS-attribution(NN-List
and the number of completed oil wells is `` down more than that , due to the increasing focus on gas exploration , ''	NN-List)
said Robert Feick , manager of crude oil with Calgary 's Independent Petroleum Association of Canada , an industry group .	NS-attribution)))
Mr. Watkins said	(NS-circumstance(NS-elaboration-additional(SN-attribution
the main reason for the production decline is shrinking output of light crude from mature , conventional fields in western Canada .	SN-attribution)
Interprovincial transports about 75 % of all crude	(NN-List(NS-elaboration-object-attribute
produced in western Canada ,	NS-elaboration-object-attribute)
and almost 60 % of Interprovincial 's total volume consists of light crude .	NN-List))
Nearly all of the crude oil	(NS-elaboration-additional(NN-Same-Unit(NS-elaboration-object-attribute
that Canada exports to the U.S .	NS-elaboration-object-attribute)
is transported on Interprovincial 's system ,	(NS-elaboration-part-whole
whose main line runs from Edmonton to major U.S. and Canadian cities in the Great Lakes region ,	(NS-elaboration-part-whole
including Chicago , Buffalo , Toronto and Montreal .	NS-elaboration-part-whole)))
Canada 's current oil exports to the U.S. total about 600,000 barrels a day , or about 9.1 % of net U.S. crude imports ,	(NS-elaboration-additional(NS-interpretation(NS-attribution
said John Lichtblau , president of the New York-based Petroleum Industry Research Foundation .	NS-attribution)
That ranks Canada as the fourth-largest source of imported crude , behind Saudi Arabia , Nigeria and Mexico .	NS-interpretation)
Mr. Lichtblau said	(NS-evaluation(SN-attribution
Canada 's declining crude output ,	(NN-Same-Unit(NS-circumstance
combined with the fast-shrinking output of U.S. crude ,	NS-circumstance)
will help intensify U.S. reliance on oil from overseas , particularly the Middle East .	NN-Same-Unit))
`` It 's very much a growing concern .	(NS-elaboration-additional(NS-attribution(NN-Contrast
But when something is inevitable ,	(SN-condition
you learn to live with it , ''	SN-condition))
he said .	NS-attribution)
Mr. Lichtblau stressed	(SN-attribution
that the delay of Interprovincial 's proposed expansion wo n't by itself increase U.S. dependence on offshore crude , however ,	(NS-rhetorical-question
since Canadian imports are limited in any case by Canada 's falling output .	NS-rhetorical-question)))))))))
Under terms of its proposed two-step expansion ,	(NS-elaboration-additional(NS-manner(NN-Same-Unit(NS-elaboration-additional
which would have required regulatory approval ,	NS-elaboration-additional)
Interprovincial intended to add 200,000 barrels a day of additional capacity to its system ,	NN-Same-Unit)
beginning with a modest expansion by 1991 .	NS-manner)
The system currently has a capacity of 1.55 million barrels a day .	NS-elaboration-additional))
