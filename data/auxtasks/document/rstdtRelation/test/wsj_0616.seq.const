As competition heats up in Spain 's crowded bank market ,	(Elaboration(Elaboration(Evaluation(Elaboration(Elaboration(Evaluation(Elaboration(Cause(Background
Banco Exterior de Espana is seeking to shed its image of a state-owned bank and move into new activities .	Background)
Under the direction of its new chairman , Francisco Luzon , Spain 's seventh largest bank is undergoing a tough restructuring	(Elaboration
that	(Same-unit(Attribution
analysts say	Attribution)
may be the first step toward the bank 's privatization .	Same-unit)))
The state-owned industrial holding company Instituto Nacional de Industria and the Bank of Spain jointly hold a 13.94 % stake in Banco Exterior .	(Joint
The government directly owns 51.4 %	(Joint
and Factorex , a financial services company , holds 8.42 % .	(Joint
The rest is listed on Spanish stock exchanges .	Joint))))
Some analysts are concerned , however ,	(Explanation(Attribution
that Banco Exterior may have waited too long	(Cause
to diversify from its traditional export-related activities .	Cause))
Catching up with commercial competitors in retail banking and financial services ,	(Condition(Condition(Same-unit(Attribution
they argue ,	Attribution)
will be difficult ,	Same-unit)
particularly if market conditions turn sour .	Condition)
If that proves true ,	(Explanation(Condition
analysts say	(Attribution
Banco Exterior could be a prime partner	(Same-unit(Elaboration
-- or even a takeover target --	Elaboration)
for either a Spanish or foreign bank	(Elaboration
seeking to increase its market share after 1992 ,	(Temporal
when the European Community plans to dismantle financial barriers .	Temporal)))))
With 700 branches in Spain and 12 banking subsidiaries , five branches and 12 representative offices abroad , the Banco Exterior group has a lot	(Elaboration
to offer a potential suitor .	Elaboration)))))
Mr. Luzon and his team , however , say	(Explanation(Comparison(Attribution
they are n't interested in a merger .	Attribution)
Instead , they are working	(Enablement
to transform Banco Exterior into an efficient bank by the end of 1992 .	Enablement))
`` I want this to be a model of the way	(Attribution(Elaboration
a public-owned company should be run , ''	Elaboration)
Mr. Luzon says .	Attribution)))
Banco Exterior was created in 1929	(Elaboration(Cause(Background(Enablement
to provide subsidized credits for Spanish exports .	Enablement)
The market for export financing was liberalized in the mid-1980s , however ,	(Temporal(Cause
forcing the bank to face competition .	Cause)
At the same time , many of Spain 's traditional export markets in Latin America and other developing areas faced a sharp decline in economic growth .	Temporal))
As a result , the volume of Banco Exterior 's export credit portfolio plunged from 824 billion pesatas	(Joint(Same-unit(Summary
( $ 7.04 billion )	Summary)
as of Dec. 31 , 1986 , to its current 522 billion pesetas .	Same-unit)
The other two main pillars of Banco Exterior 's traditional business	(Same-unit(Elaboration
-- wholesale banking and foreign currency trading --	Elaboration)
also began to crumble under the weight of heavy competition and changing client needs .	Same-unit)))
The bank was hamstrung in its efforts	(Contrast(Cause(Attribution(Elaboration
to face the challenges of a changing market by its links to the government ,	Elaboration)
analysts say .	Attribution)
Until Mr. Luzon took the helm last November ,	(Temporal
Banco Exterior was run by politicians	(Elaboration
who lacked either the skills or the will	(Elaboration
to introduce innovative changes .	Elaboration))))
But Mr. Luzon has moved swiftly	(Elaboration(Enablement
to streamline bureaucracy ,	(Joint
cut costs ,	(Joint
increase capital	(Joint
and build up new areas of business .	Joint))))
`` We 've got a lot	(Topic-Comment(Elaboration(Attribution(Elaboration
to do , ''	Elaboration)
he acknowledged .	Attribution)
`` We 've got to move quickly . ''	Elaboration)
In Mr. Luzon 's first year , the bank eliminated 800 jobs .	(Joint(Elaboration(Temporal
Now it says	(Attribution
it 'll trim another 1,200 jobs over the next three to four years .	Attribution))
The bank employs 8,000 people in Spain and 2,000 abroad .	Elaboration)
To strengthen its capital base ,	(Joint(Enablement
Banco Exterior this year issued $ 105 million in subordinated debt ,	(Joint
launched two rights issues	(Joint
and sold stock held in its treasury to small investors .	Joint)))
The bank is now aggressively marketing retail services at its domestic branches .	(Elaboration
Last year 's drop in export credit was partially offset by a 15 % surge in lending to individuals and small and medium-sized companies .	Elaboration))))))))
Though Spain has an excess of banks ,	(Contrast
analysts say	(Attribution
the country still has one of the most profitable markets in Europe ,	(Elaboration
which will aid Banco Exterior with the tough tasks	(Elaboration
it faces ahead .	Elaboration)))))
Expansion plans also include acquisitions in growing foreign markets .	(Contrast(Elaboration
The bank says	(Attribution
it 's interested in purchasing banks in Morocco , Portugal and Puerto Rico .	Attribution))
But the bank 's retail activities in Latin America are likely to be cut back .	Contrast))
Banco Exterior was one of the last banks	(Contrast(Evaluation(Temporal(Elaboration
to create a brokerage house	Elaboration)
before the four Spanish stock exchanges underwent sweeping changes in July .	Temporal)
The late start may be a handicap for the bank	(Background
as Spain continues to open up its market to foreign competition .	Background))
But Mr. Luzon contends	(Joint(Attribution
that the experienced team	(Same-unit(Elaboration
he brought with him from Banco Bilbao Vizcaya ,	(Elaboration
where he was formerly director general ,	Elaboration))
will whip the bank 's capital market division into shape by the end of 1992 .	Same-unit))
The bank also says	(Attribution
it 'll use its international network	(Enablement
to channel investment from London , Frankfurt , Zurich and Paris into the Spanish stock exchanges .	Enablement)))))
