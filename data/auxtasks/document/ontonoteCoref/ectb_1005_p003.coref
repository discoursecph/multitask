Two generations removed	ROOT	0
The second reason to look back at Huang 's early fiction is to highlight the continuity of Letting Them Go .	NONE	1
Although it has been more than ten years since Huang 's last collection of fiction , and in spite of the fact that 37 years separate " Cheng - tsai Gets off the Bus " and " Ticket Window , " there is something at the core of Huang 's fiction which has not changed .	COREF_PREV	2
In his introduction to Letting Them Go , Huang mentions his deep distress at the plight of the elderly in Taiwan today , notes that he himself is just beginning to enter his twilight years , and compares Taiwan 's callous treatment of the elderly to the way in which the elderly were left to live or die on their own in the mountains in You Shan Jie Kao .	COREF_PREV	3
All of this could very easily give the reader the mistaken impression that Huang 's vision of the elderly is a new development .	COREF_PREV	4
But this is simply not the case .	COREF_PREV	5
In Letting Them Go , what we are actually seeing is the rebirth of Huang 's original passion .	COREF_OTHER	6
If there has been any change in his portrayal of old people over the last 37 years , it is only in perspective .	COREF_PREV	7
He used to write of grandfathers from the point of view of grandsons .	COREF_PREV	8
Now he is a grandfather himself .	COREF_PREV	9
Huang Chun - ming has often discussed the fact that he was raised largely by his grandparents , and that he had an unusually close relationship to them .	COREF_PREV	10
This background accounts for the unique " generation gap " approach utilized by Huang in his fiction .	COREF_PREV	11
Each of Huang 's works describes a particular person or thing , and has an " implied reader " at which it is directed .	COREF_PREV	12
In the past , Huang 's " generation gap " separated him from the people he was writing about - people of generation , two generations removed from his grandparents 'his own .	COREF_PREV	13
Now , this " generation gap " stands between him and the " implied reader " of his fiction - children of his grandchildren 's generation who are ignorant of traditional wisdom .	COREF_PREV	14
Another aspect of Huang 's work is that it presents many of the characteristics sociology attributes to relationships between people two generations removed from one another .	COREF_PREV	15
For example , such relationships are not usually as tense , direct or hurried as those between parents and children because grandparents are not as busy as parents , and have less responsibility to supervise and teach .	NONE	16
Also , difficulties in communication between grandparents and grandchildren frequently arise because of the very different modes of expression employed by each .	NONE	17
As a result , there are typically more miscommunications in such relationships than there are between parents and their children .	NONE	18
