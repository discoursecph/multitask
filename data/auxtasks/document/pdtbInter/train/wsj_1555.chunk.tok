Hewlett-Packard Co. will announce today a software program that allows computers in a network to speed up computing tasks by sending the tasks to each other .	O	9..166
Called Task Broker , the program acts something like an auctioneer among a group of computers wired together .	B-restatement	169..277
If a machine has a big computing task , Task Broker asks other computers in the network for " bids " on the job .	I-restatement	278..387
It then determines which machine is free to do the task most quickly and sends the task to that machine .	O	388..492
Hewlett-Packard claims that the software allows a network to run three times as many tasks as conventional networks and will run each task twice as fast .	O	495..648
The new Hewlett-Packard program , said analyst John McCarthy at Forrester Research Inc. , a computer-market research company , " is a key building block as people move to this new model of distributed processing . "	O	651..860
In today 's computer networks , some machines often sit idle while others are overtaxed .	O	861..947
With the Hewlett-Packard program , he said , " You get more bang for the buck you 've spent on computers . "	O	948..1050
The program , which will be shipped in January 1990 , runs on the Unix operating system .	B-entrel	1053..1139
Hewlett-Packard will charge $ 5,000 for a license covering 10 users .	B-entrel	1140..1207
The program now works on all Hewlett-Packard and Apollo workstations and on computers made by Multiflow Computer Inc. of Branford , Conn .	I-entrel	1208..1344
Hewlett-Packard said it will sell versions later next year that run on Sun Microsystems Inc. and Digital Equipment Corp. machines .	O	1345..1475
The Task Broker differs from other programs that spread computing tasks around a network .	B-instantiation	1478..1567
A previously available program called Network Computing System , developed by Hewlett-Packard 's Apollo division , for instance , takes a task and splits it up into parts , divvying up those parts to several computers in a network for simultaneous processing .	B-concession	1568..1822
But programs in individual computers must be revised in order to work with that system .	I-concession	1823..1910
Applications wo n't have to be rewritten to work with Task Broker , Hewlett-Packard said , and the user of a computer wo n't be able to tell that another machine is doing the work .	O	1913..2089
The Task Broker " turns that network into -- as far as the user is concerned -- one giant computer , " said Bill Kay , general manager of Hewlett-Packard 's workstation group .	O	2090..2260
