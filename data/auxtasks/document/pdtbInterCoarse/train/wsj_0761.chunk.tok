Y.J. Park and her family scrimped for four years to buy a tiny apartment here , but found that the closer they got to saving the $ 40,000 they originally needed , the more the price rose .	O	9..193
By this month , it had more than doubled .	O	194..234
Now the 33-year-old housewife , whose husband earns a modest salary as an assistant professor of economics , is saving harder than ever .	B-entrel	237..371
" I am determined to get an apartment in three years , " she says .	I-entrel	372..435
" It 's all I think about or talk about . "	O	436..475
For the Parks and millions of other young Koreans , the long-cherished dream of home ownership has become a cruel illusion .	B-expansion	478..600
For the government , it has become a highly volatile political issue .	I-expansion	601..669
Last May , a government panel released a report on the extent and causes of the problem .	B-entrel	672..759
During the past 15 years , the report showed , housing prices increased nearly fivefold .	B-entrel	760..846
The report laid the blame on speculators , who it said had pushed land prices up ninefold .	I-entrel	847..936
The panel found that since 1987 , real-estate prices rose nearly 50 % in a speculative fever fueled by economic prosperity , the 1988 Seoul Olympics and the government 's pledge to rapidly develop Korea 's southwest .	B-contingency	939..1150
The result is that those rich enough to own any real estate at all have boosted their holdings substantially .	I-contingency	1151..1260
For those with no holdings , the prospects of buying a home are ever slimmer .	O	1261..1337
In 1987 , a quarter of the population owned 91 % of the nation 's 71,895 square kilometers of private land , the report said , and 10 % of the population owned 65 % of the land devoted to housing .	O	1340..1529
Meanwhile , the government 's Land Bureau reports that only about a third of Korean families own their own homes .	O	1530..1641
Rents have soared along with house prices .	O	1644..1686
Former National Assemblyman Hong Sa-Duk , now a radio commentator , says the problem is intolerable for many people .	O	1687..1801
" I 'm afraid of a popular revolt if this situation is n't corrected , " he adds .	O	1802..1878
In fact , during the past three months there have been several demonstrations at the office complex where the Land Bureau is housed , and at the National Assembly , demanding the government put a stop to real-estate speculation .	O	1879..2104
President Roh Tae Woo 's administration has been studying the real-estate crisis for the past year with an eye to partial land redistribution .	B-entrel	2107..2248
Last week , the government took three bills to the National Assembly .	B-expansion	2249..2317
The proposed legislation is aimed at rectifying some of the inequities in the current land-ownership system .	B-expansion	2318..2426
Highlights of the bills , as currently framed , are : -- A restriction on the amount of real estate one family can own , to 660 square meters in the nation 's six largest cities , but more in smaller cities and rural areas .	I-expansion	2427..2646
The government will penalize offenders , but wo n't confiscate property .	O	2647..2717
-- A tax of between 3 % and 6 % on property holdings that exceed the governmentset ceiling .	O	2720..2809
-- Taxes of between 15 % and 50 % a year on " excessive " profits from the resale of property , or the sale of idle land to the government .	B-entrel	2812..2946
The government defines excessive profits as those above the average realized for other similar-sized properties in an area .	I-entrel	2947..3070
-- Grace periods ranging from two to five years before the full scope of the penalties takes effect .	O	3073..3173
The administration says the measures would stem rampant property speculation , free more land for the government 's ambitious housing-construction program , designed to build two million apartments by 1992 -- and , perhaps , boost the popular standing of President Roh .	O	3176..3440
But opposition legislators and others calling for help for South Korea 's renters say the proposed changes do n't go far enough to make it possible for ordinary people to buy a home .	O	3443..3623
Some want lower limits on house sizes ; others insist on progressively higher taxation for larger homes and lots .	O	3624..3736
The Citizens Coalition for Economic Justice , a public-interest group leading the charge for radical reform , wants restrictions on landholdings , high taxation of capital gains , and drastic revamping of the value-assessment system on which property taxes are based .	O	3739..4002
But others , large landowners , real-estate developers and business leaders , say the government 's proposals are intolerable .	O	4005..4127
Led by the Federation of Korean Industries , the critics are lobbying for the government to weaken its proposed restrictions and penalties .	O	4128..4266
Government officials who are urging real-estate reforms balk at the arguments of business leaders and chafe at their pressure .	B-expansion	4269..4395
" There is no violation of the capitalistic principle of private property in what we are doing , " says Lee Kyu Hwang , director of the government 's Land Bureau , which drafted the bills .	I-expansion	4396..4578
But , he adds , the constitution empowers the government to impose some controls , to mitigate the shortage of land .	O	4579..4692
The land available for housing construction stands at about 46.2 square meters a person -- 18 % lower than in Taiwan and only about half that of Japan .	O	4695..4845
Mr. Lee estimates that about 10,000 property speculators are operating in South Korea .	O	4848..4934
The chief culprits , he says , are big companies and business groups that buy huge amounts of land " not for their corporate use , but for resale at huge profit . "	O	4935..5093
One research institute calculated that as much as 67 % of corporate-owned land is held by 403 companies -- and that as little as 1.5 % of that is used for business .	O	5096..5258
The government 's Office of Bank Supervision and Examination told the National Assembly this month that in the first half of 1989 , the nation 's 30 largest business groups bought real estate valued at $ 1.5 billion .	O	5259..5471
The Ministry of Finance , as a result , has proposed a series of measures that would restrict business investment in real estate even more tightly than restrictions aimed at individuals .	O	5474..5658
Under those measures , financial institutions would be restricted from owning any more real estate than they need for their business operations .	B-expansion	5661..5804
Banks , investment and credit firms would be permitted to own land equivalent in value to 50 % of their capital -- currently the proportion is 75 % .	B-expansion	5805..5950
The maximum allowable property holdings for insurance companies would be reduced to 10 % of their total asset value , down from 15 % currently .	I-expansion	5951..6091
But Mrs. Park acknowledges that even if the policies work to slow or stop speculation , apartment prices are unlikely to go down .	O	6094..6222
At best , she realizes , they will rise more slowly -- more slowly , she hopes , than her family 's income .	O	6223..6325
