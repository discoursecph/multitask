For the moment, at least, euphoria has replaced anxiety on Wall Street.	O	9..80
The Dow Jones Industrial Average jumped sharply yesterday to close at 2657.38, panic didn't sweep the world's markets, and investors large and small seemed to accept Friday's dizzying 190-point plunge as a sharp correction, not a calamity.	O	83..322
Many went bargain-hunting.	O	323..349
Among those sighing with relief was John H. Gutfreund, chairman of Salomon Brothers, who took to the firm's trading floor to monitor yesterday's events.	B-entrel	352..504
As the rally gained strength at 3:15 p.m., he smiled broadly, brandished his unlit cigar and slapped Stanley Shopkorn, his top stock trader, on the back.	I-entrel	505..658
At first, it seemed as if history might repeat itself.	B-cause	661..715
As trading opened yesterday morning on the Big Board, stocks of many of the nation's biggest companies couldn't open for trading because a wave of sell orders was overwhelming buyers.	I-cause	716..899
By 10:10, the Dow Industrials were off 63.52 points, and the stock of UAL Corp., whose troubles had kicked off Friday's plunge, still hadn't opened.	O	900..1048
But then, as quickly as the Dow had fallen, it began to turn around.	O	1051..1119
It ended with a gain of 88.12 points.	O	1120..1157
By the market's close, volume on the New York exchange totaled more than 416 million, the fourth highest on record.	B-concession	1160..1275
The Big Board handled the huge volume without any obvious strain, in sharp contrast to Black Monday of 1987.	I-concession	1276..1384
But the rally was largely confined to the blue-chip stocks, which had been hard hit during Friday's selling frenzy.	O	1385..1500
Overall, more Big Board stocks lost money than gained.	O	1501..1555
And many arbitragers, already reeling from Friday's collapse of the UAL deal, were further hurt yesterday when a proposed takeover of AMR Corp., the parent of American Airlines, collapsed.	O	1558..1746
Indeed, the Dow Jones Transportation Average plunged 102.06 points, its second-worst drop in history.	O	1747..1848
World-wide, trading was generally manageable.	B-entrel	1851..1896
The Frankfurt stock exchange was hardest hit of the major markets, with blue chips there falling 12.8%.	I-entrel	1897..2000
In London, a midday rally left the market's major index off 3.2%, and Tokyo's leading stock index fell only 1.8% in surprisingly lackluster trading.	O	2001..2149
Other, more thinly traded Asian markets were hit harder than Tokyo's, but there were no free-fall declines.	O	2150..2257
Investors big and small say they learned valuable lessons since the 1987 crash: In this age of computerized trading, huge corrections or runups in a few hours' time must be expected.	O	2260..2442
What's more, such short-term cataclysms are survivable and are no cause for panic selling.	O	2443..2533
Stephen Boesel, a major money manager for T. Rowe Price in Baltimore, says, "There was less panic than in 1987: We had been through it once.	B-entrel	2536..2677
In Somerset, Wis., Adrian Sween, who owns a supplier of nursing-home equipment and isn't active in the stock market, agrees.	I-entrel	2678..2802
"I look at it as a ho-hum matter," he says.	O	2803..2846
Many other factors played a part in yesterday's comeback.	O	2849..2906
The Federal Reserve signaled its willingness to provide liquidity; the interest rate on its loans to major banks inched downward early in the day.	B-conjunction	2907..3053
Foreign stock markets, which kicked off Black Monday with a huge selling spree, began the day off by relatively modest amounts.	B-conjunction	3054..3181
The Federal Reserve signaled its willingness to provide liquidity; the interest rate on its loans to major banks inched downward early in the day.Foreign stock markets, which kicked off Black Monday with a huge selling spree, began the day off by relatively modest amounts.The dollar, after falling sharply in overnight trading to 139.10 yen, bounced back strongly to 141.8, thus easing fears that foreigners would unload U.S. stocks.	B-conjunction	3182..3343
And the widely disseminated opinion among most market experts that a crash wasn't in store also helped calm investors.	B-instantiation	3346..3464
Many major institutions, for example, came into work yesterday ready to buy some of the blue chips they felt had been sharply undervalued on Friday.	I-instantiation	3465..3613
Still, amid all the backslapping and signs of relief over yesterday's events, some market professionals cautioned that there is nothing present in the current market system to prevent another dizzying drop such as Friday's.	O	3616..3839
"There is too much complacency," says money manager Barry Schrager.	O	3840..3907
Computers have increasingly connected securities markets world-wide, so that a buying or selling wave in one market is often passed around the globe.	O	3910..4059
So investors everywhere nervously eyed yesterday's opening in Tokyo, where the Nikkei average of 225 blue-chip stocks got off to a rocky start.	O	4060..4203
The average plunged some 600 points, or 1.7%, in the first 20 minutes of trading.	O	4204..4285
But the selling wave had no conviction, and the market first surged upward by 200 points, then drifted lower, closing down 647.	O	4286..4413
Unlike two years ago, most of Japan's major investors chose to sit this calamity out.	B-instantiation	4416..4501
In Merrill Lynch & Co. 's Tokyo trading room, some 40 traders and assistants sat quietly, with few orders to process.	I-instantiation	4502..4619
Clients "are all staying out" of the market, one Merrill trader says.	O	4620..4689
The relative calm in Tokyo proved little comfort to markets opening up in Europe.	B-instantiation	4692..4773
Frankfurt's opening was delayed a half hour because of a crush of sell orders.	I-instantiation	4774..4852
"The beginning was chaotic," says Nigel Longley, a broker for Commerzbank	O	4853..4926
In London, the view from the trading floor of an American securities firm, Jefferies & Co., also was troubling.	B-cause	4929..5040
A computer screen displaying 100 blue-chip stocks colors each one red when its price is falling.	B-entrel	5041..5137
The screen was a sea of red.	I-entrel	5138..5166
I see concern, but I don't see panic," says J. Francis Palamara, a New Yorker who runs the 15-trader office.	B-entrel	5167..5276
London's blue-chip stock index turned up just before 8 a.m. New York time, sending an encouraging message to Wall Street.	I-entrel	5277..5398
When trading opened in New York at 9:30 a.m. EDT, stocks fell sharply -- as expected.	B-asynchronous	5401..5486
Futures markets in Chicago had opened at a level suggesting the Dow would fall by about 60 points.	B-conjunction	5487..5585
With sell orders piled up from Friday, about half the stocks in the Dow couldn't open on time.	B-asynchronous	5586..5680
By 9:45, the industrial average had dropped 27 points.	B-conjunction	5681..5735
By 10 a.m., it was down 49.	B-asynchronous	5736..5763
Ten minutes later, the Dow hit bottom-down 63.52 points, another 2.5%.	I-asynchronous	5764..5834
But shortly before then, some of Wall Street's sharpest traders say, they sensed a turn.	O	5837..5925
"The first thing that caught my eye that was encouraging was Treasury bonds were off," says Austin George, head of stock trading at T. Rowe Price.	O	5926..6072
"It meant that people weren't running pell-mell to the safety of bonds."	O	6073..6145
Shortly after 10 a.m., the Major Market Index, a Chicago Board of Trade futures contract of 20 stocks designed to mimic the DJIA, exploded upward.	O	6148..6294
Stock traders were buoyed -- because an upturn in the MMI had also started the recovery in stocks on the Tuesday following Black Monday.	O	6295..6431
The MMI has gone better," shouted a trader in the London office of Shearson Lehman Hutton.	B-asynchronous	6434..6525
Shearson's London trading room went wild.	B-restatement	6526..6567
Traders shouted out as their Reuters, Quotron and Telerate screens posted an ever-narrowing loss on Wall Street.	I-restatement	6568..6680
Then, nine minutes later, Wall Street suddenly rebounded to a gain on the day.	B-cause	6681..6759
Rally, rally, rally," shouted Shearson's Andy Rosen.	B-entrel	6760..6813
This is panic buying.	B-entrel	6814..6837
Major blue-chip stocks like Philip Morris, General Motors and Proctor & Gamble led the rally.	I-entrel	6838..6931
Japanese were said to be heavy buyers.	B-conjunction	6934..6972
German and Dutch investors reportedly loaded up on Kellogg Co.	I-conjunction	6973..7035
Then, traders say, corporations with share buy-back programs kicked into high gear, triggering gains in, among other issues, Alcan Aluminium and McDonald's.	O	7036..7192
Walt Disney Co., which had one of the biggest sell-order imbalances on Friday and was one of seven stocks that halted trading and never reopened that day, opened yesterday late at 114.5, down 8.5.	O	7195..7391
But then it suddenly burst upward 7.5 as Goldman, Sachs & Co. stepped in and bought almost every share offer, traders said.	O	7392..7515
By 10:25, the Dow had turned up for the day, prompting cheers on trading desks and exchange floors.	O	7518..7617
Among Big Board specialists, the cry was "Pull your offers" -- meaning that specialists soon expected to get higher prices for their shares.	O	7618..7758
"It was bedlam on the upside," said one Big Board specialist.	O	7761..7822
"What we had was a real, old-fashioned rally."	O	7823..7869
This technical strength spurred buying from Wall Street's "black boxes," computer programs designed to trigger large stock purchases during bullish periods.	B-entrel	7872..8028
Typical, perhaps, was Batterymarch's Dean LeBaron.	B-restatement	8029..8079
Mr. LeBaron, who manages $10 billion, says, "We turned the trading system on, and it did whatever it was programmed to do."	I-restatement	8080..8203
Asked what stocks the computer bought, the money manager says, "I don't know."	O	8204..8282
Not everybody was making money.	B-instantiation	8285..8316
The carnage on the Chicago Board Options Exchange, the nation's major options market, was heavy after the trading in S&P 100 stock-index options was halted Friday.	B-cause	8317..8480
Many market makers in the S&P 100 index options contract had bullish positions Friday, and when the shutdown came they were frozen with huge losses.	B-cause	8481..8629
Over the weekend, clearing firms told the Chicago market makers to get out of their positions at any cost Monday morning.	I-cause	8630..8751
"They were absolutely killed, slaughtered," said one Chicago-based options trader.	O	8754..8836
Meanwhile, a test of the stock market's rally came at about 2 p.m., with the Dow at 2600, up 31 points on the day.	O	8839..8953
Charles Clough, a strategist at Merrill Lynch, says bargain hunting had explained the Dow's strength up to that point and that many market professionals were anticipating a drop in the Dow.	O	8954..9143
Moreover, the announcement that real estate magnate and sometime raider Donald Trump was withdrawing his offer for AMR Corp. might have been expected to rattle traders.	B-alternative	9144..9312
Instead, the rally only paused for about 25 minutes and then steamed forward as institutions resumed buying.	B-asynchronous	9315..9423
The market closed minutes after reaching its high for the day of	I-asynchronous	9424..9488
Across the country, many people took yesterday's events in stride, while remaining generally uneasy about the stock market in general.	B-instantiation	9491..9625
Says James Norman, the mayor of Ava, Mo.: "I don't invest in stocks.	I-instantiation	9626..9694
I much prefer money I can put my hands on.	B-entrel	9695..9738
While Mayor Norman found the market's performance Monday reassuring, he says, he remains uneasy.	I-entrel	9739..9835
"We have half the experts saying one thing and half the other" about the course of the economy.	O	9836..9931
Ralph Holzfaster, a farmer and farm-supply store operator in Ogallala, Neb., says of the last few days events, "If anything good comes out of this, it might be that it puts some of these LBOs on the skids."	O	9934..10140
Says Gordon Fines, a money manager at IDS Financial Services in Minneapolis, "You're on a roller coaster, and that may last.	O	10143..10267
The public is still cautious.	O	10268..10297
