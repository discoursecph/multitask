When the price of plastics took off in 1987 , Quantum Chemical Corp. went along for the ride .	O	9..101
The timing of Quantum 's chief executive officer , John Hoyt Stookey , appeared to be nothing less than inspired , because he had just increased Quantum 's reliance on plastics .	B-cause	104..276
The company outpaced much of the chemical industry as annual profit grew fivefold in two years .	B-entrel	277..372
Mr. Stookey said of the boom , " It 's going to last a whole lot longer than anybody thinks . "	I-entrel	373..463
But now prices have nose-dived and Quantum 's profit is plummeting .	O	466..532
Some securities analysts are looking for no better than break-even results from the company for the third quarter , compared with year-earlier profit of $ 99.8 million , or $ 3.92 a share , on sales of $ 724.4 million .	B-entrel	533..745
The stock , having lost nearly a quarter of its value since Sept. 1 , closed at $ 34.375 share , down $ 1.125 , in New York Stock Exchange composite trading Friday .	I-entrel	746..904
To a degree , Quantum represents the new times that have arrived for producers of the so-called commodity plastics that pervade modern life .	B-restatement	907..1046
Having just passed through one of the most profitable periods in their history , these producers now see their prices eroding .	I-restatement	1047..1172
Pricing cycles , to be sure , are nothing new for plastics producers .	B-conjunction	1175..1242
And the financial decline of some looks steep only in comparison with the heady period that is just behind them .	I-conjunction	1243..1355
" We were all wonderful heroes last year , " says an executive at one of Quantum 's competitors .	O	1356..1448
" Now we 're at the bottom of the heap . "	O	1449..1487
At Quantum , which is based in New York , the trouble is magnified by the company 's heavy dependence on plastics .	O	1490..1601
Once known as National Distillers & Chemical Corp. , the company exited the wine and spirits business and plowed more of its resources into plastics after Mr. Stookey took the chief executive 's job in 1986 .	B-entrel	1602..1807
Mr. Stookey , 59 years old , declined to be interviewed for this article , but he has consistently argued that over the long haul -- across both the peaks and the troughs of the plastics market -- Quantum will prosper through its new direction .	I-entrel	1808..2049
Quantum 's lot is mostly tied to polyethylene resin , used to make garbage bags , milk jugs , housewares , toys and meat packaging , among other items .	B-entrel	2052..2197
In the U.S. polyethylene market , Quantum has claimed the largest share , about 20 % .	I-entrel	2198..2280
But its competitors -- including Dow Chemical Co. , Union Carbide Corp. and several oil giants -- have much broader business interests and so are better cushioned against price swings .	O	2281..2464
When the price of polyethylene moves a mere penny a pound , Quantum 's annual profit fluctuates by about 85 cents a share , provided no other variables are changing .	B-entrel	2467..2629
In recent months the price of polyethylene , even more than that of other commodity plastics , has taken a dive .	B-instantiation	2630..2740
Benchmark grades , which still sold for as much as 50 cents a pound last spring , have skidded to between 35 cents and 40 cents .	I-instantiation	2741..2867
Meanwhile , the price of ethylene , the chemical building block of polyethylene , has n't dropped nearly so fast .	B-entrel	2870..2979
That discrepancy hurts Quantum badly , because its own plants cover only about half of its ethylene needs .	I-entrel	2980..3085
By many accounts , an early hint of a price rout in the making came at the start of this year .	B-restatement	3088..3181
China , which had been putting in huge orders for polyethylene , abruptly halted them .	I-restatement	3182..3266
Calculating that excess polyethylene would soon be sloshing around the world , other buyers then bet that prices had peaked and so began to draw down inventories rather than order new product .	O	3267..3458
Kenneth Mitchell , director of Dow 's polyethylene business , says producers were surprised to learn how much inventories had swelled throughout the distribution chain as prices spiraled up .	O	3461..3648
" People were even hoarding bags , " he says .	O	3649..3691
Now producers hope prices have hit bottom .	B-entrel	3694..3736
They recently announced increases of a few cents a pound to take effect in the next several weeks .	I-entrel	3737..3835
No one knows , however , whether the new posted prices will stick once producers and customers start to haggle .	O	3836..3945
One doubter is George Krug , a chemical-industry analyst at Oppenheimer & Co. and a bear on plastics stocks .	B-restatement	3948..4055
Noting others ' estimates of when price increases can be sustained , he remarks , " Some say October .	I-restatement	4056..4153
Some say November .	B-contrast	4154..4172
I say 1992 . "	I-contrast	4173..4185
He argues that efforts to firm up prices will be undermined by producers ' plans to expand production capacity .	O	4186..4296
A quick turnaround is crucial to Quantum because its cash requirements remain heavy .	B-restatement	4299..4383
The company is trying to carry out a three-year , $ 1.3 billion plant-expansion program started this year .	I-restatement	4384..4488
At the same time , its annual payments on long-term debt will more than double from a year ago to about $ 240 million , largely because of debt taken on to pay a $ 50-a-share special dividend earlier this year .	O	4489..4695
Quantum described the payout at the time as a way for it to share the bonanza with its holders , because its stock price was n't reflecting the huge profit increases .	B-contrast	4698..4862
Some analysts saw the payment as an effort also to dispel takeover speculation .	I-contrast	4863..4942
Whether a cash crunch might eventually force the company to cut its quarterly dividend , raised 36 % to 75 cents a share only a year ago , has become a topic of intense speculation on Wall Street since Mr. Stookey deflected dividend questions in a Sept. 29 meeting with analysts .	O	4945..5221
Some viewed his response -- that company directors review the dividend regularly -- as nothing more than the standard line from executives .	O	5224..5363
But others came away thinking he had given something less than his usual straight-from-the-shoulder performance .	B-entrel	5364..5476
In any case , on the day of the meeting , Quantum 's shares slid $ 2.625 to $ 36.625 in Big Board trading .	I-entrel	5477..5578
On top of everything else , Quantum confronts a disaster at its plant in Morris , Ill .	B-restatement	5581..5665
After an explosion idled the plant in June , the company progressed in September to within 12 hours of completing the drawn-out process of restarting it .	I-restatement	5666..5818
Then a second explosion occurred .	B-cause	5819..5852
Two workers died and six remain in the hospital .	I-cause	5853..5901
This human toll adds the most painful dimension yet to the sudden change in Quantum 's fortunes .	B-cause	5904..5999
Until this year , the company had been steadily lowering its accident rate and picking up trade-group safety awards .	I-cause	6000..6115
A prolonged production halt at the plant could introduce another imponderable into Quantum 's financial future .	B-cause	6118..6228
When a plant has just been running flat out to meet demand , calculating lost profit and thus claims under business-interruption insurance is straightforward .	B-contrast	6229..6386
But the numbers become trickier -- and subject to dickering between insured and insurer -- when demand is shifting .	I-contrast	6387..6502
" You say you could have sold X percent of this product and Y percent of that , " recalls Theodore Semegran , an analyst at Shearson Lehman Hutton who went through this exercise during his former career as a chemical engineer .	O	6505..6727
" And then you still have to negotiate . "	O	6728..6767
Quantum hopes the Morris plant , where limited production got under way last week , will resume full operation by year 's end .	B-entrel	6770..6893
The plant usually accounts for 20 % to 25 % of Quantum 's polyethylene production and 50 % of its ethylene production .	I-entrel	6894..7008
Not everything looks grim for Quantum .	B-cause	7011..7049
The plant expansion should strengthen the company 's sway in the polyethylene business , where market share is often taken through sheer capacity .	B-conjunction	7050..7194
By lifting ethylene production , the expansion will also lower the company 's raw material costs .	I-conjunction	7195..7290
Quantum is also tightening its grip on its one large business outside chemicals , propane marketing .	B-restatement	7293..7392
Through a venture with its investment banker , First Boston Corp. , Quantum completed in August an acquisition of Petrolane Inc. in a transaction valued at $ 1.18 billion .	B-entrel	7393..7561
Petrolane is the second-largest propane distributor in the U.S.	B-conjunction	7562..7625
The largest , Suburban Propane , was already owned by Quantum .	I-conjunction	7626..7686
Still , Quantum has a crisis to get past right now .	O	7689..7739
Some analysts speculate the weakening stock may yet attract a suitor .	O	7740..7809
The name surfacing in rumors is British Petroleum Co. , which is looking to expand its polyethylene business in the U.S.	O	7810..7929
Asked about a bid for Quantum , a BP spokesman says , " We pretty much have a policy of not commenting on rumors , and I think that falls in that category .	O	7932..8083
