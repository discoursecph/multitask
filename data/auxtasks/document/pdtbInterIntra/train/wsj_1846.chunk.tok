One of the most remarkable features of the forced marches of the ethnic Turks out of Bulgaria over the past five months has been the lack of international attention .	root	9..174
The deportation of more than 315,000 men , women and children by the Bulgarian regime adds up to one of the largest migrations seen in the postwar years .	concession	175..327
Yet some people are advancing a chilling casuistry : that what we are seeing is somehow the understandable result of the historical sins committed by the Turks in the 16th century .	concession	328..507
Today 's Turks in Bulgaria , in other words , deserve what is coming to them four centuries later .	restatement	508..603
As if this were n't enough , the Senate Judiciary Committee is getting into the act .	norel	606..688
On Tuesday it approved Senator Bob Dole 's proposed commemorative resolution designating April 24 , 1990 , as the " National Day of Remembrance of the 75th Anniversary of the Armenian Genocide of 1915-1923 , " suffered at the hands of the warring Ottoman Empire .	instantiation	689..945
There can be no quibbling that the Armenians endured terrible suffering	norel	948..1019
but one has to wonder what possible good such a resolution will achieve .	contrast	1020..1093
It puts great strain on a longstanding U.S. friendship with Turkey , a country that has been one of America 's strongest allies in NATO .	cause	1094..1228
The resolution also comes at a time when Turkey has been seeking help from the United States in resolving its Bulgarian emigration controversy and pursuing democratic reforms that may lead to membership in the European Community .	conjunction	1229..1458
Turkey has been fighting its past for years , and thus far has been only partially successful .	norel	1461..1554
Must it now accept that one of its strongest allies blames it for the genocide of another people ?	cause	1555..1652
Such sentiment only encourages the adverse feelings toward Turkey that surfaced when Turkey asked for assistance in dealing with its Bulgarian emigration crisis .	norel	1653..1814
Mr. Dole 's odd effort notwithstanding , most of Turkey 's political problems lie with the Europeans .	norel	1817..1915
Part of the problem some Europeans have with Turkey seems to stem from its location -- Turkey is n't really part of Europe .	entrel	1916..2038
Why , they wonder , should it belong to the EC ?	cause	2039..2084
Another anti-Turkish hook is the Islamic faith of the majority of the Turkish people : Turkey , we are told , is not a Christian nation ; its people simply wo n't fit in with the Western European Judeo-Christian tradition .	conjunction	2085..2302
It 's when these rationalizations fall on deaf ears that the old standby of retribution for treatment at the hands of the Ottoman Empire comes to the fore .	contrast	2303..2457
No one has to accept the sins of the Ottoman Empire to reject that argument .	norel	2460..2536
Turkey in any event is long past it .	conjunction	2537..2573
The country has in recent years accepted more than 500,000 refugees from at least four bordering nations .	instantiation	2574..2679
Kurds , suffering what many people consider to be a current extermination campaign at the hands of Syria , Iran and Iraq have inundated eastern Turkey .	instantiation	2680..2829
Now it is their fellow Turks arriving as refugees from Bulgaria .	conjunction	2830..2894
The Turkish refugee tragedy and the ongoing crisis can not be ignored and shuttled off to that notorious dustbin of history that has become so convenient recently .	norel	2897..3059
Surely , the past suffering of any people at any time can not be simply filed away and forgotten .	cause	3060..3155
But what the Senate Judiciary Committee has done in supporting the strongly worded Armenian resolution achieves no useful end	contrast	3156..3281
it merely produces more controversy and embittered memories .	restatement	3282..3343
Congress has enough difficulty dealing with the realities of the world as it currently exists .	norel	3346..3440
Bulgaria 's government has been behaving beyond the pale for months	entrel	3441..3507
and the U.S. does its values no credit by ignoring that while casting its votes into the past .	conjunction	3508..3603
