Friday , October 13 , 1989	root
The key U.S. and foreign annual interest rates below are a guide to general levels	-1_NN-Textual-organization
but do n't always represent actual transactions .	-1_NS-Contrast
PRIME RATE :	-2_NS-Elaboration
10 1/2 % .	-1_NS-Elaboration
The base rate on corporate loans at large U.S. money center commercial banks .	-2_NS-Elaboration
FEDERAL FUNDS :	-3_NN-Joint
8 13/16 % high , 8 1/2 % low , 8 5/8 % near closing bid , 8 3/4 % offered .	-1_NS-Elaboration
Reserves	-2_NS-Elaboration
traded among commercial banks for overnight use in amounts of $ 1 million or more .	-1_NS-Elaboration
Source :	-4_NS-Attribution
Fulton Prebon	-1_NS-Elaboration
( U.S.A . )	-1_NS-Elaboration
Inc .	-2_NN-Same-unit
DISCOUNT RATE :	-8_NN-Joint
7 % .	-1_NS-Elaboration
The charge on loans to depository institutions by the New York Federal Reserve Bank .	-2_NS-Elaboration
CALL MONEY :	-3_NN-Joint
9 3/4 % to 10 % .	-1_NS-Elaboration
The charge on loans to brokers on stock exchange collateral .	-2_NS-Elaboration
COMMERCIAL PAPER	-3_NN-Joint
placed directly by General Motors Acceptance Corp. :	-1_NS-Elaboration
8.60 % 30 to 44 days ; 8.55 % 45 to 59 days ; 8.375 % 60 to 79 days ; 8.50 % 80 to 89 days ; 8.25 % 90 to 119 days ; 8.125 % 120 to 149 days ; 8 % 150 to 179 days ; 7.625 % 180 to 270 days .	-2_NS-Elaboration
COMMERCIAL PAPER :	-3_NN-Joint
High-grade unsecured notes	-1_NS-Elaboration
sold through dealers by major corporations in multiples of $ 1,000 :	-1_NS-Elaboration
8.65 % 30 days ; 8.55 % 60 days ; 8.55 % 90 days .	-3_NS-Elaboration
CERTIFICATES OF DEPOSIT :	-4_NN-Joint
8.15 % one month ; 8.15 % two months ; 8.13 % three months ; 8.11 % six months ; 8.08 % one year .	-1_NS-Elaboration
Average of top rates	-2_NS-Elaboration
paid by major New York banks on primary new issues of negotiable C.D.s , usually on amounts of $ 1 million and more .	-1_NS-Elaboration
The minimum unit is $ 100,000 .	-4_NS-Elaboration
Typical rates in the secondary market :	-5_NS-Comparison
8.65 % one month ; 8.65 % three months ; 8.55 % six months .	-1_NS-Elaboration
BANKERS ACCEPTANCES :	-7_NN-Joint
8.52 % 30 days ; 8.37 % 60 days ; 8.15 % 90 days ; 7.98 % 120 days ; 7.92 % 150 days ; 7.80 % 180 days .	-1_NS-Elaboration
Negotiable , bank-backed business credit instruments	-2_NS-Elaboration
typically financing an import order .	-1_NS-Elaboration
LONDON LATE EURODOLLARS :	-4_NN-Joint
8 13/16 % to 8 11/16 % one month ; 8 13/16 % to 8 11/16 % two months ; 8 13/16 % to 8 11/16 % three months ; 8 3/4 % to 8 5/8 % four months ; 8 11/16 % to 8 9/16 % five months ; 8 5/8 % to 8 1/2 % six months .	-1_NS-Elaboration
LONDON INTERBANK OFFERED RATES	-2_NN-Joint
( LIBOR ) :	-1_NS-Summary
8 3/4 % one month ; 8 3/4 % three months ; 8 9/16 % six months ; 8 9/16 % one year .	-2_NS-Elaboration
The average of interbank offered rates for dollar deposits in the London market	-3_NS-Elaboration
based on quotations at five major banks .	-1_NS-Elaboration
FOREIGN PRIME RATES :	-5_NN-Joint
Canada 13.50 % ; Germany 8.50 % ; Japan 4.875 % ; Switzerland 8.50 % ; Britain 15 % .	-1_NS-Elaboration
These rate indications are n't directly comparable ;	-2_NS-Elaboration
lending practices vary widely by location .	-1_NS-Explanation
TREASURY BILLS :	-4_NN-Joint
Results of the Tuesday , October 10 , 1989 , auction of short-term U.S. government bills ,	-1_NS-Elaboration
sold at a discount from face value in units of $ 10,000 to $ 1 million :	-1_NS-Elaboration
7.63 % 13 weeks ; 7.60 % 26 weeks .	-3_NS-Elaboration
FEDERAL HOME LOAN MORTGAGE CORP .	-4_NN-Joint
( Freddie Mac ) :	-1_NS-Summary
Posted yields on 30-year mortgage commitments for delivery within 30 days .	-2_NS-Elaboration
9.91 % , standard conventional fixedrate mortgages ; 7.875 % , 2 % rate capped one-year adjustable rate mortgages .	-3_NS-Elaboration
Source :	-4_NS-Attribution
Telerate Systems Inc .	-1_NS-Elaboration
FEDERAL NATIONAL MORTGAGE ASSOCIATION	-6_NN-Joint
( Fannie Mae ) :	-1_NS-Summary
Posted yields on 30 year mortgage commitments for delivery within 30 days	-2_NS-Elaboration
( priced at par )	-1_NS-Elaboration
9.86 % , standard conventional fixed-rate mortgages ; 8.85 % , 6/2 rate capped one-year adjustable rate mortgages .	-4_NS-Elaboration
Source :	-5_NS-Attribution
Telerate Systems Inc .	-1_NS-Elaboration
MERRILL LYNCH READY ASSETS TRUST :	-7_NN-Joint
8.33 % .	-1_NS-Elaboration
Annualized average rate of return after expenses for the past 30 days ; not a forecast of future returns .	-2_NS-Elaboration
