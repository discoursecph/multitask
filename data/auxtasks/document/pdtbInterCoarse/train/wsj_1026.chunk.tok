West Texas Intermediate , the U.S. benchmark crude , seemed tethered again yesterday in trading on the New York Mercantile Exchange .	B-expansion	9..139
Widely expected to open 10 to 15 cents a barrel higher on the strength of statistics from the American Petroleum Institute , the December contract managed to start the session only eight cents higher .	I-expansion	140..339
In the last hour of the trading day , December contract took a tumble to end the session 10 cents lower at $ 19.62 a barrel .	B-expansion	342..464
And now that the price has fallen below $ 19.65 , which many had said showed considerable resistance , some traders and analysts figure there 's little to stop the price from going lower on technical decisions .	I-expansion	465..671
With no petroleum-related news or changes in the fundamentals to dictate price moves , " technicians are wanting to sell this stuff , " said Eric Bolling of Edge Trading Corp .	O	674..845
" And short-term , the technicians may have their way . "	O	846..899
The market quickly discounted the weekly inventory report showing a 6.3 million barrel decrease in U.S. crude oil stocks as the legacy of Hurricane Jerry .	B-contingency	902..1056
That storm hit the Gulf Coast Oct. 13 , closing the Louisiana Offshore Oil Port for a time and preventing tankers from unloading .	I-contingency	1057..1185
Next week 's report could very well show an increase in crude inventories .	O	1186..1259
Dismissing the trade group 's numbers left traders plenty of time to worry anew about the latest reports on OPEC production .	O	1262..1385
An AP-Dow Jones survey of integrated oil companies , independent refiners , and oil industry consultants indicates that the Organization of Petroleum Exporting Countries increased its production to 22.2 million barrels a day in September .	B-expansion	1386..1622
Estimates suggest October 's figure may be even higher .	I-expansion	1623..1677
That level of production is n't of much concern as long as demand continues strong , analysts said .	O	1680..1777
But the first quarter of the year is generally the weakest and OPEC production does n't seem to be slackening in anticipation of that .	B-expansion	1778..1911
Also , maintaining current demand assumes no significant slowdown in world economies .	I-expansion	1912..1996
To top off the bearish factors affecting yesterday 's trading , late October weather , especially in the Northeast U.S. , continues to be very moderate , leaving heating oil futures trading lackluster .	O	1999..2195
" We are n't seeing any cold weather here , " Mr. Bolling said from New York .	O	2196..2269
In other commodity markets yesterday :	O	2272..2309
GRAINS AND SOYBEANS :	O	2312..2332
Soybean and corn futures prices moved higher on the strength of buying from commodity pool managers trying to profit from technical price trends , as well as continued export strength .	B-expansion	2333..2516
A leveling off of farmer selling tied to the harvest also removed some of the downward pressure on futures contract prices .	I-expansion	2517..2640
Wheat futures prices fell , however , at least partly in reaction to the rumored selling of futures contracts equal to several million bushels of wheat by commodity speculator Richard Dennis .	B-comparison	2641..2830
Neither Mr. Dennis nor officials of his Chicago trading company , C&D Commodities , could be reached for comment .	I-comparison	2831..2942
As for corn and soybean futures , " a lot of commission house buying this morning and computer-driven buying " supported prices in early trading , said Steven Freed , a futures analyst with Dean Witter Reynolds Inc. in Chicago .	O	2943..3165
Soybean futures for November delivery gained 5.25 cents a bushel to close at $ 5.66 a bushel on the Chicago Board of Trade .	B-expansion	3166..3288
December corn futures added 2.25 cents a bushel to close at $ 2.4225 a bushel on the Board of Trade .	I-expansion	3289..3388
Announced and anticipated purchases from foreign countries are also supporting futures prices .	O	3389..3483
" Russian ships are arriving in the gulf and there is n't enough grain in the pipeline , " said Katharina Zimmer , a futures analyst with Merrill Lynch & Co. in New York .	O	3484..3649
The Soviet Union has purchased roughly eight million tons of grain this month , and is expected to take delivery by year end , analysts said .	O	3650..3789
COTTON :	O	3792..3799
Futures prices rose modestly , but trading volume was n't very heavy .	O	3800..3867
The December contract settled at 73.97 cents a pound , up 0.59 cent , but it rose as high as 74.20 cents .	B-entrel	3868..3971
Several cotton analysts said that the move appeared to be mostly technical .	I-entrel	3972..4047
Traders who had sold contracts earlier , in hopes of buying them back at lower prices , yesterday were buying contracts back at higher prices to limit their losses .	O	4048..4210
Floor traders also said that the market could have been helped by rumors , which have been circulating for the past two days , about China purchasing cotton .	B-entrel	4211..4366
The rumor , which has been neither confirmed nor denied , has China buying 125,000 to 200,000 bales for near-term delivery .	B-entrel	4367..4488
One floor trader said that if there were Chinese purchases , they should have had a bigger effect on the market .	I-entrel	4489..4600
Another said that if China was a buyer , it would be the earliest that country had made purchases since the 1979-80 crop year , and thus would be a bullish sign .	B-expansion	4601..4760
This trader characterized the recent price action as a contest between the fundamentalists , who see higher prices ahead , and the technicians , who are basically buying cotton toward the bottom of the current trading range , around 71 cents , and selling it when the price climbs more than 74 cents .	I-expansion	4761..5056
This trader said that he thought the market would turn aggressively bullish from a technical standpoint if the December contract was able to exceed 75.75 cents .	B-expansion	5057..5217
He also noted that stocks on Aug. 1 , 1990 , are currently projected at 3.3 million bales , the smallest end-of-season supply since 1985 .	I-expansion	5218..5352
COCOA :	O	5355..5361
The modest sell-off , which started on Tuesday , continued .	B-expansion	5362..5419
The December contract ended at $ 999 a metric ton , down $ 15 .	B-entrel	5420..5479
The market is drifting , at least partly , because of a lack of crop information out of Ghana and the Ivory Coast , the two largest African producers .	I-entrel	5480..5627
Harry Schwartz , a soft commodity specialist for Cargill Investors Services in New York , said the only report Ghana has issued about the arrival of cocoa from the interior was for 7,839 metric tons as of Oct. 12 .	O	5628..5839
By this time last year , he noted , arrivals totaling 33,270 tons had been announced .	B-entrel	5840..5923
A similar situation apparently exists in the Ivory Coast with no figures released yet this year , compared with 55,000 tons as of this time a year ago .	I-entrel	5924..6074
He said that if little cocoa actually has arrived at the ports , shipping delays could result .	B-entrel	6075..6168
This is the worry that probably brought steadiness to the market earlier in the week , he said .	I-entrel	6169..6263
There was also some fear that without Ivory Coast cocoa a large French cocoa merchant , Cie . Financiere Sucre et Denrees , might not be able to deliver cocoa against the contracts it had sold earlier for December delivery in London .	B-comparison	6264..6494
However , the French merchant has about 200,000 tons of old crop Ivory Coast cocoa stored in the Netherlands from an agreement it had negotiated with the Ivory Coast last spring .	I-comparison	6495..6672
Cargill thinks that even though the merchant has a contract stating that it wo n't bring this cocoa to market until after March 1991 , there is some evidence the contract has been modified .	O	6673..6860
This modification , apparently , would permit the French merchant to deliver this cocoa , if necessary , against existing short positions .	O	6861..6995
