The Senate 's decision to approve a bare-bones deficit-reduction bill without a capital-gains tax cut still leaves open the possibility of enacting a gains tax reduction this year .	O	9..188
Late Friday night , the Senate voted 87-7 to approve an estimated $ 13.5 billion measure that had been stripped of hundreds of provisions that would have widened , rather than narrowed , the federal budget deficit .	B-expansion	191..401
Lawmakers drastically streamlined the bill to blunt criticism that it was bloated with special-interest tax breaks and spending increases .	I-expansion	402..540
" We 're putting a deficit-reduction bill back in the category of being a deficit-reduction bill , " said Senate Budget Committee Chairman James Sasser ( D. , Tenn . ) .	O	543..703
But Senate supporters of the trimmer legislation said that other bills would soon be moving through Congress that could carry some of the measures that had been cast aside , including a capital-gains tax cut .	O	706..913
In addition , the companion deficit-reduction bill already passed by the House includes a capital-gains provision .	B-entrel	914..1027
House-Senate negotiations are likely to begin at midweek and last for a while .	I-entrel	1028..1106
" No one can predict exactly what will happen on the House side , " said Senate Minority Leader Robert Dole ( R. , Kan . ) .	O	1109..1225
But , he added , " I believe Republicans and Democrats will work together to get capital-gains reform this year . "	O	1226..1336
White House Budget Director Richard Darman told reporters yesterday that the administration would n't push to keep the capital-gains cut in the final version of the bill .	O	1339..1508
" We do n't need this as a way to get capital gains , " he said .	O	1509..1569
House Budget Committee Chairman Leon Panetta ( D. , Calif . ) said in an interview , " If that 's the signal that comes from the White House , that will help a great deal . "	O	1572..1736
The Senate 's decision was a setback for President Bush and will make approval of a capital-gains tax cut less certain this year .	B-entrel	1739..1867
Opponents of the cut are playing hardball .	B-expansion	1868..1910
Senate Majority Leader George Mitchell ( D. , Maine ) said he was " confident " that any House-Senate agreement on the deficit-reduction legislation would n't include a capital-gains tax cut .	I-expansion	1911..2096
And a senior aide to the House Ways and Means Committee , where tax legislation originates , said there are n't any " plans to produce another tax bill that could carry a gains tax cut this year . "	O	2097..2289
One obvious place to attach a capital-gains tax cut , and perhaps other popular items stripped from the deficit-reduction bill , is the legislation to raise the federal borrowing limit .	B-entrel	2292..2475
Such legislation must be enacted by the end of the month .	I-entrel	2476..2533
The Senate bill was pared back in an attempt to speed deficit-reduction through Congress .	B-comparison	2536..2625
Because the legislation has n't been completed , President Bush has until midnight tonight to enact across-the-board spending cuts mandated by the Gramm-Rudman deficit-reduction law .	I-comparison	2626..2806
Senators hope that the need to avoid those cuts will pressure the House to agree to the streamlined bill .	B-comparison	2809..2914
The House appears reluctant to join the senators .	B-expansion	2915..2964
A key is whether House Republicans are willing to acquiesce to their Senate colleagues ' decision to drop many pet provisions .	I-expansion	2965..3090
" Although I am encouraged by the Senate action , " said Chairman Dan Rostenkowski ( D. , Ill . ) of the House Ways and Means Committee , " it is uncertain whether a clean bill can be achieved in the upcoming conference with the Senate . "	O	3093..3321
Another big question hovering over the debate is what President Bush thinks .	B-entrel	3324..3400
He has been resisting a stripped-down bill without a guaranteed vote on his capital-gains tax cut .	B-comparison	3401..3499
But Republican senators saw no way to overcome a procedural hurdle and garner the 60 votes needed to win the capital-gains issue on the floor , so they went ahead with the streamlined bill .	I-comparison	3500..3688
The Senate bill was stripped of many popular , though revenue-losing , provisions , a number of which are included in the House-passed bill .	O	3691..3828
These include a child-care initiative and extensions of soon-to-expire tax breaks for low-income housing and research-and-development expenditures .	O	3829..3976
Also missing from the Senate bill is the House 's repeal of a law , called Section 89 , that compels companies to give rank-and-file workers comparable health benefits to top paid executives .	O	3977..4165
One high-profile provision that was originally in the Senate bill but was cut out because it lost money was the proposal by Chairman Lloyd Bentsen ( D. , Texas ) of the Senate Finance Committee to expand the deduction for individual retirement accounts .	B-entrel	4168..4418
Mr. Bentsen said he hopes the Senate will consider that measure soon .	I-entrel	4419..4488
To the delight of some doctors , the bill dropped a plan passed by the Finance Committee that would have overhauled the entire physician-reimbursement system under Medicare .	B-expansion	4491..4663
To the delight of some doctors , the bill dropped a plan passed by the Finance Committee that would have overhauled the entire physician-reimbursement system under Medicare .	B-comparison	4491..4663
To the detriment of many low-income people , efforts to boost Medicaid funding , especially in rural areas , also were stricken .	I-comparison	4664..4789
Asked why senators were giving up so much , New Mexico Sen. Pete Domenici , the ranking Republican on the Senate Budget Committee , said , " We 're looking like idiots .	O	4792..4954
Things had just gone too far . "	O	4955..4985
Sen. Dole said that the move required sacrifice by every senator .	O	4988..5053
It worked , others said , because there were no exceptions : all revenue-losing provisions were stricken .	O	5054..5156
The Senate also dropped a plan by its Finance Committee that would have increased the income threshold beyond which senior citizens have their Social Security benefits reduced .	O	5159..5335
In addition , the bill dropped a plan to make permanent a 3 % excise tax on long-distance telephone calls .	O	5336..5440
It no longer includes a plan that would have repealed what remains of the completed-contract method of accounting , which is used by military contractors to reduce their tax burden .	O	5441..5621
It also drops a provision that would have permitted corporations to use excess pension funds to pay health benefits for current retirees .	O	5622..5759
Also stricken was a fivefold increase in the maximum Occupational Safety and Health Administration penalties , which would have raised $ 65 million in fiscal 1990 .	O	5762..5923
A provision that would have made the Social Security Administration an independent agency was excised .	O	5924..6026
The approval of the Senate bill was especially sweet for Sen. Mitchell , who had proposed the streamlining .	O	6029..6135
Mr. Mitchell 's relations with Budget Director Darman , who pushed for a capital-gains cut to be added to the measure , have been strained since Mr. Darman chose to bypass the Maine Democrat and deal with other lawmakers earlier this year during a dispute over drug funding in the fiscal 1989 supplemental spending bill .	O	6136..6453
The deficit reduction bill contains $ 5.3 billion in tax increases in fiscal 1990 , and $ 26 billion over five years .	B-expansion	6456..6570
The revenue-raising provisions , which affect mostly corporations , would :	I-expansion	6571..6643
-- Prevent companies that have made leveraged buy-outs from getting federal tax refunds resulting from losses caused by interest payments on debt issued to finance the buy-outs , effective Aug. 2 , 1989 .	O	6646..6847
-- Require mutual funds to include in their taxable income dividends paid to them on the date that the dividends are declared rather than received , effective the day after the tax bill is enacted .	O	6850..7046
-- Close a loophole regarding employee stock ownership plans , effective June 6 , 1989 , that has been exploited by investment bankers in corporate takeovers .	B-expansion	7049..7204
The measure repeals a 50 % exclusion given to banks on the interest from loans used to acquire securities for an ESOP , if the ESOP owns less than 30 % of the employer 's stock .	I-expansion	7205..7378
-- Curb junk bonds by ending tax benefits for certain securities , such as zero-coupon bonds , that postpone cash interest payments .	O	7381..7511
-- Raise $ 851 million by suspending for one year an automatic reduction in airport and airway taxes .	O	7514..7614
-- Speed up the collection of the payroll tax from large companies , effective August 1990 .	O	7617..7707
-- Impose a tax on ozone-depleting chemicals , such as those used in air conditioners and in Styrofoam , beginning at $ 1.10 a pound starting next year .	O	7710..7859
-- Withhold income taxes from the paychecks of certain farm workers currently exempt from withholding .	O	7862..7964
-- Change the collection of gasoline excise taxes to weekly from semimonthly , effective next year .	O	7967..8065
-- Restrict the ability of real estate owners to escape taxes by swapping one piece of property for another instead of selling it for cash .	O	8068..8207
-- Increase to $ 6 a person from $ 3 the international air-passenger departure tax , and impose a $ 3-a-person tax on international departures by commercial ships .	O	8210..8369
The measure also includes spending cuts and increases in federal fees .	O	8372..8442
Among its provisions :	O	8443..8464
-- Reduction of Medicare spending in fiscal 1990 by some $ 2.8 billion , in part by curbing increases in reimbursements to physicians .	O	8467..8599
The plan would impose a brief freeze on physician fees next year .	O	8600..8665
-- Removal of the U.S. Postal Service 's operating budget from the federal budget , reducing the deficit by $ 1.77 billion .	O	8668..8788
A similar provision is in the House version .	O	8789..8833
-- Authority for the Federal Aviation Administration to raise $ 239 million by charging fees for commercial airline-landing rights at New York 's LaGuardia and John F. Kennedy International Airports , O'Hare International Airport in Chicago and National Airport in Washington .	O	8836..9109
-- Increases in Nuclear Regulatory Commission fees totaling $ 54 million .	O	9112..9184
-- Direction to the U.S. Coast Guard to collect $ 50 million from users of Coast Guard services .	O	9187..9282
-- Raising an additional $ 43 million by increasing existing Federal Communications Commission fees and penalties and establishing new fees for amateur radio operators , ship stations and mobile radio facilities .	O	9285..9495
John E. Yang contributed to this article .	O	9498..9539
