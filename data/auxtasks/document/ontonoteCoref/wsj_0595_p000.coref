Wives May Not Benefit When Men Do Chores	ROOT	0
WHEN HUSBANDS take on more housework , they tend to substitute for chores done by the kids rather than by the wife .	COREF_PREV	1
Rand Corp. researchers Linda Waite and Frances Goldscheider analyzed a large sample of married women with at least one child at home between the ages of six and 18 .	NONE	2
The women indicated which family member usually did various household chores and the approximate share each did .	COREF_PREV	3
Not unexpectedly , wives , whether working or non-working , did by far the most -- about 80 % of the shopping , laundry and cooking , and about two - thirds of housecleaning , washing dishes , child care and family paper work .	COREF_PREV	4
Only for yardwork and home maintenance did women do less than half .	COREF_PREV	5
But the researchers found that while children 's household tasks eased the mother 's burden appreciably , the husband 's helping hand `` appears to lighten the children 's load almost on a one - for - one basis and to reduce the wife 's responsibility only modestly . ''	COREF_PREV	6
This pattern was particularly evident among more highly educated couples .	NONE	7
In these families , husbands took on 80 % more chores than in couples with only grammar school education .	NONE	8
But the kids with highly educated parents did 68 % less housework than those in less - educated families .	NONE	9
`` It is clear , '' Ms. Waite says , `` that most of the effect of increasing education has been to shift who is helping the wife / mother .	COREF_OTHER	10
Her share decreases , but only slightly . ''	COREF_PREV	11
Nursing Home Patients Apt to Be Private Payers	NONE	12
FAR FEWER elderly nursing home residents bankrupt themselves than was previously believed , two recent studies declare .	NONE	13
State governments place very low ceilings on how much property people may own or how much income they may keep if they want welfare help on medical bills .	NONE	14
Conventional wisdom has long held that anywhere from one - fourth to one - half of all elderly long - term care patients are obliged to spend themselves into poverty before qualifying for Medicaid assistance .	NONE	15
But separate reports from Joshua Weiner and Denise Spence of the Brookings Institution and Korbin Liu of the Urban Institute find that `` a surprisingly small proportion '' -- only about 10 % -- of residents start out as private payers but `` spend down '' to Medicaid levels in a single nursing home stay before they die or are discharged .	COREF_PREV	16
( Another one - third are already on Medicaid when they enter the nursing homes , a considerably higher proportion than the analysts anticipated . )	COREF_PREV	17
But a remarkably high percentage -- over half -- are private payers throughout their stay , even a fairly lengthy one .	NONE	18
About one - third pay out of their own pockets , while the rest are covered throughout by Medicare , private insurers or the Veterans Administration .	NONE	19
Both reports are based on several thousand patients sampled in a 1985 nationwide government survey .	COREF_OTHER	20
The Brookings and Urban Institute authors caution , however , that most nursing home stays are of comparatively short duration , and reaching the Medicaid level is more likely with an unusually long stay or repeated stays .	COREF_OTHER	21
Moreover , they note , those who manage to pay their own way often do so only by selling their homes , using up life savings or drawing heavily on children and other relatives .	COREF_PREV	22
Reagan Era Young Hold Liberal Views	NONE	23
THE REAGAN generation young men and women reaching political maturity during Ronald Reagan 's presidency -- are firmly liberal on race and gender , according to NORC , the social science research center at the University of Chicago .	COREF_PREV	24
Many political analysts have speculated that the Reagan years would produce a staunchly conservative younger generation .	COREF_PREV	25
NORC 's most recent opinion surveys find the youngest adults indeed somewhat more pro-Reagan and pro-Republican than other adults .	COREF_OTHER	26
But , says chief investigator Tom Smith , this `` does not translate into support for conservatism in general or into conservative positions on feminist and civil rights issues . ''	COREF_PREV	27
Answers to a dozen questions in the 1986 , 1987 , 1988 and 1989 national surveys reveal that men and women in the 18 to 24 age bracket are considerably more liberal on race and gender than were the 18 to 24 year olds in NORC 's polling in the early 1970s and early 1980s .	COREF_OTHER	28
They were also as liberal or more liberal than any other age group in the 1986 through 1989 surveys .	COREF_PREV	29
For example , 66 % of the 18 to 24 year olds in the four latest surveys favored an `` open housing '' law prohibiting homeowners from refusing on racial grounds to sell to prospective buyers .	NONE	30
That compares with 58 % of the similar age group in the 1980 through 1982 surveys and 55 % in the 1972 through 1975 surveys .	COREF_PREV	31
Asked whether they agreed or disagreed with the claim that men are emotionally better suited to politics than women , 70 % of the Reagan generation disagreed , compared with under 60 % of younger men and women in the earlier years .	COREF_OTHER	32
Odds and Ends	NONE	33
SEPARATED and divorced men and women are far more likely to be smokers than married persons , the Centers for Disease Control discovers ... .	NONE	34
Graduate students are taking longer than ever to get their doctor of philosophy degrees , the National Research Council says .	NONE	35
It estimates the time between college graduation and the awarding of a Ph. D. has lengthened by 30 % over the past 20 years , with the average gap now ranging from about 7.4 years in the physical sciences to 16.2 years in education .	COREF_PREV	36
