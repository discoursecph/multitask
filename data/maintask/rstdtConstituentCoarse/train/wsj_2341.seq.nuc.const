Earnings for most of the nation 's major pharmaceutical makers are believed to have moved ahead briskly in the third quarter ,	(NS-Elaboration(NS-Cause(NS-Elaboration(NS-Comparison(NS-Elaboration
as companies with newer , big-selling prescription drugs fared especially well .	NS-Elaboration)
For the third consecutive quarter , however , most of the companies ' revenues were battered by adverse foreign-currency translations	(NS-Cause
as a result of the strong dollar abroad .	NS-Cause))
Analysts said	(NS-Comparison(SN-Attribution
that Merck & Co. , Eli Lilly & Co. , Warner-Lambert Co. and the Squibb Corp. unit of Bristol-Myers Squibb Co. all benefited from strong sales of relatively new , higher-priced medicines	(NS-Elaboration
that provide wide profit margins .	NS-Elaboration))
Less robust earnings at Pfizer Inc. and Upjohn Co. were attributed to those companies ' older products ,	(NS-Elaboration
many of which face stiffening competition from generic drugs and other medicines .	NS-Elaboration)))
Joseph Riccardo , an analyst with Bear , Stearns & Co. , said	(NS-Cause(SN-Attribution
that over the past few years most drug makers have shed their slow-growing businesses	(NN-Joint
and instituted other cost savings ,	(NS-Elaboration
such as consolidating manufacturing plants and administrative staffs .	NS-Elaboration)))
As a result , `` major new products are having significant impact , even on a company with very large revenues , ''	(NS-Attribution
Mr. Riccardo said .	NS-Attribution)))
Analysts said	(NS-Elaboration(NS-Summary-Evaluation-Topic(SN-Attribution
profit for the dozen or so big drug makers , as a group , is estimated to have climbed between 11 % and 14 % .	SN-Attribution)
While that 's not spectacular ,	(SN-Comparison
Neil Sweig , an analyst with Prudential Bache , said	(SN-Attribution
that the rate of growth will `` look especially good	(NS-Cause(NS-Comparison
as compared to other companies	NS-Comparison)
if the economy turns downward . ''	NS-Cause))))
Mr. Sweig estimated	(NN-Joint(NS-Comparison(NS-Elaboration(NS-Elaboration(SN-Attribution
that Merck 's profit for the quarter rose by about 22 % ,	(NS-Cause
propelled by sales of its line-up of fast-growing prescription drugs ,	(NS-Elaboration
including its anti-cholesterol drug , Mevacor ; a high blood pressure medicine , Vasotec ; Primaxin , an antibiotic , and Pepcid , an anti-ulcer medication .	NS-Elaboration)))
Profit climbed	(NS-Attribution(NS-Cause(NS-Comparison
even though Merck 's sales were reduced by `` one to three percentage points ''	NS-Comparison)
as a result of the strong dollar ,	NS-Cause)
Mr. Sweig said .	NS-Attribution))
In the third quarter of 1988 , Merck earned $ 311.8 million , or 79 cents a share .	NS-Elaboration)
In Rahway , N.J. , a Merck spokesman said	(SN-Attribution
the company does n't make earnings projections .	SN-Attribution))
Mr. Sweig said	(NN-Joint(NS-Comparison(NS-Elaboration(NS-Summary-Evaluation-Topic(NS-Elaboration(SN-Attribution
he estimated	(SN-Attribution
that Lilly 's earnings for the quarter jumped about 20 % ,	(NS-Cause
largely because of the performance of its new anti-depressant Prozac .	NS-Cause)))
The drug ,	(NN-Textual-organization(NS-Elaboration
introduced last year ,	NS-Elaboration)
is expected to generate sales of about $ 300 million this year .	NN-Textual-organization))
`` It 's turning out to be a real blockbuster , ''	(NS-Attribution
Mr. Sweig said .	NS-Attribution))
In last year 's third quarter , Lilly earned $ 171.4 million , or $ 1.20 a share .	NS-Elaboration)
In Indianapolis , Lilly declined comment .	NS-Comparison)
Several analysts said	(NN-Joint(NS-Elaboration(SN-Attribution
they expected Warner-Lambert 's profit also to increase by more than 20 % from $ 87.7 million , or $ 1.25 a share ,	(NS-Elaboration
it reported in the like period last year .	NS-Elaboration))
The company is praised by analysts	(NS-Elaboration(NS-Cause
for sharply lowering its costs in recent years	(NN-Joint
and shedding numerous companies with low profit margins .	NN-Joint))
The company 's lean operation ,	(NS-Elaboration(NN-Textual-organization(NS-Attribution
analysts said ,	NS-Attribution)
allowed sharp-rising sales from its cholesterol drug , Lopid ,	(NS-Cause
to power earnings growth .	NS-Cause))
Lopid sales are expected to be about $ 300 million this year , up from $ 190 million in 1988 .	(NS-Elaboration
In Morris Plains , N.J. , a spokesman for the company said	(SN-Attribution
the analysts ' projections are `` in the ballpark . ''	SN-Attribution)))))
Squibb 's profit ,	(NN-Joint(NS-Elaboration(NS-Elaboration(NN-Textual-organization(NS-Elaboration
estimated by analysts to be about 18 % above the $ 123 million , or $ 1.25 a share ,	(NS-Elaboration
it earned in the third quarter of 1988 ,	NS-Elaboration))
was the result of especially strong sales of its Capoten drug	(NS-Elaboration
for treating high blood pressure and other heart disease .	NS-Elaboration))
The company was officially merged with Bristol-Myers Co. earlier this month .	NS-Elaboration)
Bristol-Myers declined to comment .	NS-Elaboration)
Mr. Riccardo of Bear Stearns said	(NN-Joint(NS-Elaboration(NS-Elaboration(SN-Attribution
that Schering-Plough Corp. 's expected profit rise of about 18 % to 20 % , and Bristol-Meyers 's expected profit increase of about 13 % are largely because `` those companies are really managed well . ''	SN-Attribution)
ScheringPlough earned $ 94.4 million , or 84 cents a share ,	(NN-Comparison
while Bristol-Myers earned $ 232.3 million , or 81 cents a share , in the like period a year earlier .	NN-Comparison))
In Madison , N.J. , a spokesman for Schering-Plough said	(NS-Cause(SN-Attribution
the company has `` no problems '' with the average estimate by a analysts	(NS-Elaboration
that third-quarter earnings per share rose by about 19 % , to $ 1 .	NS-Elaboration))
The company expects to achieve the 20 % increase in full-year earnings per share ,	(NS-Attribution(NS-Comparison
as it projected in the spring ,	NS-Comparison)
the spokesman said .	NS-Attribution)))
Meanwhile , analysts said	(NN-Joint(NS-Elaboration(NS-Elaboration(NS-Elaboration(NS-Elaboration(SN-Attribution
Pfizer 's recent string of lackluster quarterly performances continued ,	(NS-Elaboration
as earnings in the quarter were expected to decline by about 5 % .	NS-Elaboration))
Sales of Pfizer 's important drugs , Feldene	(NS-Cause(NN-Textual-organization(NS-Elaboration
for treating arthritis ,	NS-Elaboration)
and Procardia , a heart medicine , have shrunk	NN-Textual-organization)
because of increased competition .	NS-Cause))
`` The ( strong ) dollar hurt Pfizer a lot , too , ''	(NS-Attribution
Mr. Sweig said .	NS-Attribution))
In the third quarter last year , Pfizer earned $ 216.8 million , or $ 1.29 a share .	NS-Elaboration)
In New York , the company declined comment .	NS-Elaboration)
Analysts said	(NS-Elaboration(NS-Cause(SN-Attribution
they expected Upjohn 's profit to be flat or rise by only about 2 % to 4 %	(NS-Comparison
as compared with $ 89.6 million , or 49 cents a share ,	(NS-Elaboration
it earned a year ago .	NS-Elaboration)))
Upjohn 's biggest-selling drugs are Xanax , a tranquilizer , and Halcion , a sedative .	(NN-Joint(NS-Elaboration
Sales of both drugs have been hurt by new state laws	(NN-Textual-organization(NS-Elaboration
restricting the prescriptions of certain tranquilizing medicines	NS-Elaboration)
and adverse publicity about the excessive use of the drugs .	NN-Textual-organization))
Also , the company 's hair-growing drug , Rogaine , is selling well	(NS-Attribution(SN-Comparison(NS-Elaboration
-- at about $ 125 million for the year ,	NS-Elaboration)
but the company 's profit from the drug has been reduced by Upjohn 's expensive print and television campaigns for advertising ,	SN-Comparison)
analysts said .	NS-Attribution)))
In Kalamazoo , Mich. , Upjohn declined comment .	NS-Elaboration)))))))))
