When last we left him ,	(NS(NN(NS(NS(NS(NS(NS(NS(NS(SN
FBI Agent Nick Mancuso had solved a murder mystery ,	(NN
unraveled a Washington political scandal ,	(NN
and racked up some pretty good ratings numbers in the miniseries `` Favorite Son . ''	NN)))
What next for the crusty FBI agent with the heart of gold ?	(NN
A spinoff series , of course .	NN))
There are plenty of worse inspirations for shows	(NS
-- and most of them had already made the fall lineup :	(NS
a nun	(NN(NN(NS
raising some lovable orphans .	NS)
A den mother	(NN(NS
raising some lovable teen models .	NS)
A bunch of tans and bathing suits	(NS
posing as lovable lifeguards .	NS)))
In that context , Robert Loggia 's riveting performance as the unlovable	(NN(NS
-- even crotchety --	NS)
veteran agent seems a better franchise for a series than most .	NN)))))
Week by week on `` Mancuso FBI ''	(NS(NN(NS
( NBC , Fridays , 10 p.m. ET ) ,	NS)
he pokes around the crime styles of the rich , famous and powerful of the Washington scene	(NS
-- a loose cannon on deck at the FBI .	NS))
Over the first few weeks , `` Mancuso FBI '' has sprung straight from the headlines ,	(NS(NS
which is either a commendable stab at topicality , or a lack of imagination , or both .	NS)
The opening show featured a secretary of defense designate	(NN(NS(NS(NS
accused of womanizing	NS)
( a la John Tower ) .	NS)
When his secretary is found floating dead in the pol 's pool ,	(SN
Mancuso is called in to investigate .	SN))
Last week , a young black girl claimed	(NN(NS(SN
she had been raped by a white police officer	SN)
( a la Tawana Brawley ) .	NS)
In this week 's show , there 's an unsafe nuclear weaponsmaking facility	(NS
( a la Rocky Flats ) .	NS))))))
Along the way , we 're introduced to the supporting cast :	(NS
a blond bombshell secretary	(NN(NS
( Randi Brazen	(NS
-- her real name , honest ) ,	NS))
a scheming young boss	(NN(NS
( Fredric Lehne ) ,	NS)
another blonde bombshell	(NN(NS(NS
who 's also an idealistic lawyer	NS)
( Lindsay Frost ) ,	NS)
and a forensics expert	(NS
( Charles Siebert ) .	NS))))))
If all of this seems a little stale ,	(NS(SN
it 's redeemed in part by some tricky plot twists :	SN)
The usual suspects are found to be guilty , then not guilty , then guilty	(NS(NS
-- but of a different crime .	NS)
( In last week 's rape case , for example , the girl turns out to have been a victim of incest ,	(NN
and the biggest villains are the politicians	(NS
who exploit the case . )	NS)))))
Most of all though , the show is redeemed by the character of Mancuso .	(NS
What makes the veteran FBI man so endearing is his hard-bitten cynicism	(NN(NS(NS(NS
-- earned ,	(NS(NS
we discover ,	NS)
when he was assigned to the civil rights movement back in the 1960s .	NS))
He was n't protecting the Freedom Marchers ;	(SN
he was tailing them as subversives .	SN))
This is not the `` Mississippi Burning '' scenario	(NN(NS(NS
that thrills his young colleagues :	NS)
`` Kid , you 've been reading Classic Comics too long , ''	(NN(NS
Mancuso says .	NS)
`` Back in 1964 , the FBI had five black agents .	(NS
Three were chauffeurs for J. Edgar Hoover ,	(NN
and two cleaned his house . ''	NN))))
At the core of Mr. Loggia 's Mancuso is his world-weary truculence .	(NS
He describes a reporter as `` Miss First Amendment . ''	(NN
He describes a drowned corpse as `` Esther Williams . ''	(NN
And	(NN(NN
when he 's told `` Try a little tenderness , ''	(SN
he shoots back	SN))
`` I 'm going home to try a little linguine . ''	NN))))))
Yet for all his cynicism , he 's at heart a closet idealist , a softy with a secret crush on truth , justice and the American Way .	(NS
He 's the kind of guy	(NS
who rescues trampled flags .	NS)))))
If `` Mancuso FBI '' has an intriguing central character ,	(NS(NS(NS(NN
it also has a major flaw :	(NS
It 's wildly overwritten .	NS))
Executive Producers Steve Sohmer and Jeff Bleckner	(NN(NS
( and writer/producers Ken Solarz and Steve Bello )	NS)
have revved this show up to the breaking point .	NN))
To start , there 's always a crisis	(NN(NN
-- and someone always worries ,	(SN
`` What if the press gets a hold of this ? ''	SN))
At least once an episode we see protestors	(NN(NS
marching around	(NN
screaming slogans .	NN))
At least once Mancuso 's boss yells	(NN(NN(SN
`` In here	(NN
-- now , ''	NN))
and proceeds to dress his investigator down :	(NN(NS
`` You are a dinosaur . . .	(NN
a hangover in a $ 10 suit . . .	(NN
One more word and you are out on a park bench , mister . ''	NN)))
Finally , of course , the boss gives in ,	(NN
but he 's still yelling :	(SN
`` I find myself explaining anything to Teddy Kennedy ,	(SN
you 'll be chasing stolen cars in Anchorage . ''	SN)))))
In fact , throughout `` Mancuso FBI , '' we do n't get words or lines	(NN(NS(SN
-- we get speeches .	SN)
Witnesses shout , scream , pontificate :	(NN(SN
`` . . . a dream that the planet could be saved from itself and from the sadistic dumb creatures	(NS
who try to tear down every decent man	(NS
who raises his voice . ''	NS)))
And Mancuso himself is investigating at the top of his lungs :	(SN
`` How the hell can you live with yourself ? ''	(NN(NS
he erupts at a politician .	NS)
`` You twist people 's trust .	(NN
You built your career on prejudice and hate .	(NN
The scars will be here	(NS
years after the polls close . ''	NS)))))))
In each show , Mancuso gets to unleash similar harangues :	(NN(NS
`` Where the hell are they gon na live	(NN(NS
when people like you turn the world into a big toxic waste dump ?	NS)
You 're the real criminal here . . .	(NN
and what you did was n't just a murder	(SN
-- it was a crime against humanity . ''	SN))))
And , at least once a show , someone delivers the line	(NS(SN
`` Get off that soapbox . ''	SN)
Now that 's advice	(NS
the writers should take to heart .	NS))))))))
They have a series with a good character , some interesting , even occasionally surprising plot lines ,	(NS(NN
and they 're ruining it .	NN)
Why ,	(NS(NS(NN
when a key witness disappears ,	(SN
does Mancuso trash her apartment ,	SN))
tearing down drapes ,	(NN
smashing walls ?	NN))
It 's a bizarre and totally inappropriate reaction ,	(NN(NS
all to add more pizzazz to a script	(NS
that 's already overdosing on pizzazz .	NS))
That 's not plot .	(NN(NN
That 's not character .	NN)
That 's hyperventilating .	NN))))))
There is a scene at the end of the first week 's show	(SN(SN(NS
where Mancuso attends the unveiling of the memorial to his dead partner David .	NS)
Asked to say a few words ,	(NN(SN(SN
he pulls out his crumpled piece of paper	(NN
and tries to talk ,	NN))
but he 's too choked up	(SN
to get the words out .	SN))
He bangs on the piece of paper in frustration ,	(NN
then turns	(NN
and walks away .	NN))))
It was a profoundly moving moment for series television ,	(NN
and Robert Loggia 's acting resonated in the silence .	NN)))
There 's a pretty good program inside all the noise of `` Mancuso FBI . ''	(NS
If the show 's creators could just let themselves be quiet for a little ,	(SN
they might just hear it .	SN)))
