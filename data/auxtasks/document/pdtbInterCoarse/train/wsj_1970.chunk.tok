A surprising surge in the U.S. trade deficit raised fears that the nation 's export drive has stalled , and caused new turmoil in financial markets .	O	9..155
The merchandise trade deficit widened in August to $ 10.77 billion , the Commerce Department reported , a sharp deterioration from July 's $ 8.24 billion and the largest deficit of any month this year .	B-temporal	158..354
Exports fell for the second month in a row , while imports rose to a record .	I-temporal	355..430
" This is one of the worst trade releases we 've had since the dollar troughed out in 1987 , " said Geoffrey Dennis , chief international economist at James Capel Inc .	O	433..595
Like most analysts , Mr. Dennis was hesitant to read too much into one month 's numbers ; but he said , " It indicates perhaps that the balance in the U.S. economy is not as good as we 've been led to believe . "	O	596..800
The number had a troubling effect on Wall Street , suggesting that more fundamental economic problems may underlie last Friday 's stock market slide .	O	803..950
The Dow Jones Industrial Average tumbled more than 60 points after the report 's release , before recovering to close 18.65 points lower at 2638.73 .	O	951..1097
" This bad trade number raises some deeper issues about the market decline , " said Norman Robertson , chief economist for Mellon Bank .	O	1100..1231
" It raises questions about more deep-seated problems , the budget deficit and the trade deficit and the seeming lack of ability to come to grips with them . "	O	1232..1387
The trade report drew yet another unsettling parallel to October 1987 .	B-expansion	1390..1460
On Oct. 14 of that year , the announcement of an unusually large August trade deficit helped trigger a steep market decline .	B-temporal	1461..1584
The slide continued until the record 508-point market drop on Oct. 19 .	I-temporal	1585..1655
In 1987 , however , the news was the latest in a string of disappointments on trade , while the current report comes after a period of improvement .	O	1656..1800
The bleak trade report was played down by the Bush administration .	B-expansion	1803..1869
Commerce Secretary Robert Mosbacher called the worsening trade figures " disappointing after two very good months . "	B-expansion	1870..1984
And White House spokesman Marlin Fitzwater said the deficit was " an unwelcome increase , " adding that " we 're hopeful that it simply is a one-month situation and will turn around . "	I-expansion	1985..2163
But the figures reinforced the view of many private analysts that the improvement in the U.S. trade deficit has run out of steam .	O	2166..2295
" The figures today add further evidence to support the view that the improvement in the U.S. trade deficit has essentially stalled out at a level of about a $ 110 billion annual rate , " said Jeffrey Scott , a research fellow at the Institute for International Economics here .	O	2298..2570
" That 's still an improvement over last year , but it leads one to conclude that basically we 've gotten all the mileage we can out of past dollar depreciation and past marginal cuts in the federal budget deficit . "	O	2571..2782
Exports declined for the second consecutive month in August , slipping 0.2 % to $ 30.41 billion , the Commerce Department reported .	O	2785..2912
Imports , on the other hand , leaped 6.4 % to a record $ 41.18 billion .	O	2913..2980
Not only was August 's deficit far worse than July 's , but the government revised the July figure substantially from the $ 7.58 billion deficit it had initially reported last month .	O	2983..3161
Many economists contend that deep cuts in the U.S. budget deficit are needed before further trade improvement can occur .	O	3164..3284
That 's because the budget deficit feeds an enormous appetite in this country for both foreign goods and foreign capital , overwhelming the nation 's capacity to export .	B-entrel	3285..3451
" People are sick and tired of hearing about these deficits , but the imbalances are still there and they are still a problem , " said Mr. Robertson .	I-entrel	3452..3597
In addition , the rise in the value of the dollar against foreign currencies over the past several months has increased the price of U.S. products in overseas markets and hurt the country 's competitiveness .	B-contingency	3600..3805
Since March , exports have been virtually flat .	I-contingency	3806..3852
At the same time , William T. Archey , international vice president at the U.S. Chamber of Commerce , notes : " Clearly the stronger dollar has made imports more attractive " by causing their prices to decline .	O	3853..4057
Most economists expect the slowing U.S. economy to curb demand for imports .	B-comparison	4060..4135
But they foresee little substantial progress in exports unless the dollar and the federal budget deficit come down .	I-comparison	4136..4251
" The best result we could get from these numbers would be to see the administration and Congress get serious about putting the U.S. on an internationally competitive economic footing , " said Howard Lewis , vice president of international economic affairs at the National Association of Manufacturers .	B-entrel	4254..4552
" That must start with cutting the federal budget deficit . "	I-entrel	4553..4611
August 's decline in exports reflected decreases in sales of industrial supplies , capital goods and food abroad and increases in sales of motor vehicles , parts and engines .	B-comparison	4614..4785
The jump in imports stemmed from across-the-board increases in purchases of foreign goods .	I-comparison	4786..4876
The numbers were adjusted for usual seasonal fluctuations .	O	4879..4937
Alan Murray contributed to this article .	O	4940..4980
( In billions of U.S. dollars , not seasonally adjusted )	O	4983..5037
* Newly industrialized countries : Singapore , Hong Kong , Taiwan , South Korea	O	5040..5114
