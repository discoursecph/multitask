Ripples from the strike by 55,000 Machinists union members against Boeing Co. reached air carriers Friday as America West Airlines announced it will postpone its new service out of Houston because of delays in receiving aircraft from the Seattle jet maker.	O	9..265
Peter Otradovec, vice president for planning at the Phoenix, Ariz., carrier, said in an interview that the work stoppage at Boeing, now entering its 13th day, "has caused some turmoil in our scheduling" and that more than 500 passengers who were booked to fly out of Houston on America West would now be put on other airlines.	O	268..594
Mr. Otradovec said Boeing told America West that the 757 it was supposed to get this Thursday wouldn't be delivered until Nov. 7 -- the day after the airline had been planning to initiate service at Houston with four daily flights, including three nonstops to Phoenix and one nonstop to Las Vegas.	B-cause	597..894
Now, those routes aren't expected to begin until Jan.	I-cause	895..948
Boeing is also supposed to send to America West another 757 twin-engine aircraft as well as a 737 by year's end.	B-conjunction	951..1063
Those, too, are almost certain to arrive late.	I-conjunction	1064..1110
At this point, no other America West flights -- including its new service at San Antonio, Texas; Newark, N.J.; and Palmdale, Calif. -- have been affected by the delays in Boeing deliveries.	B-pragmatic concession	1113..1302
Nevertheless, the company's reaction underscores the domino effect that a huge manufacturer such as Boeing can have on other parts of the economy.	B-conjunction	1305..1451
It also is sure to help the machinists put added pressure on the company.	I-conjunction	1452..1525
"I just don't feel that the company can really stand or would want a prolonged walkout," Tom Baker, president of Machinists' District 751, said in an interview yesterday.	O	1528..1698
"I don't think their customers would like it very much."	O	1699..1755
America West, though, is a smaller airline and therefore more affected by the delayed delivery of a single plane than many of its competitors would be.	O	1758..1909
"I figure that American and United probably have such a hard time counting all the planes in their fleets, they might not miss one at all," Mr. Otradovec said.	O	1910..2069
Indeed, a random check Friday didn't seem to indicate that the strike was having much of an effect on other airline operations.	B-instantiation	2072..2199
Southwest Airlines has a Boeing 737-300 set for delivery at the end of this month and expects to have the plane on time.	I-instantiation	2200..2320
"It's so close to completion, Boeing's told us there won't be a problem," said a Southwest spokesman.	O	2321..2422
A spokesman for AMR Corp. said Boeing has assured American Airlines it will deliver a 757 on time later this month.	B-conjunction	2425..2540
American is preparing to take delivery of another 757 in early December and 20 more next year and isn't anticipating any changes in that timetable.	I-conjunction	2541..2688
In Seattle, a Boeing spokesman explained that the company has been in constant communication with all of its customers and that it was impossible to predict what further disruptions might be triggered by the strike.	B-synchrony	2691..2906
Meanwhile, supervisors and non-striking employees have been trying to finish some 40 aircraft -- mostly 747 and 767 jumbo jets at the company's Everett, Wash., plant -- that were all but completed before the walkout.	B-entrel	2909..3125
As of Friday, four had been delivered and a fifth plane, a 747-400, was supposed to be flown out over the weekend to Air China.	I-entrel	3126..3253
No date has yet been set to get back to the bargaining table.	O	3256..3317
"We want to make sure they know what they want before they come back," said Doug Hammond, the federal mediator who has been in contact with both sides since the strike began.	O	3318..3492
The investment community, for one, has been anticipating a speedy resolution.	B-cause	3495..3572
Though Boeing's stock price was battered along with the rest of the market Friday, it actually has risen over the last two weeks on the strength of new orders.	I-cause	3573..3732
"The market has taken two views: that the labor situation will get settled in the short term and that things look very rosy for Boeing in the long term," said Howard Rubel, an analyst at Cyrus J. Lawrence Inc.	O	3733..3942
Boeing's shares fell $4 Friday to close at $57.375 in composite trading on the New York Stock Exchange.	O	3943..4046
But Mr. Baker said he thinks the earliest a pact could be struck would be the end of this month, hinting that the company and union may resume negotiations as early as this week.	O	4049..4227
Still, he said, it's possible that the strike could last considerably longer.	O	4228..4305
"I wouldn't expect an immediate resolution to anything."	O	4306..4362
Last week, Boeing Chairman Frank Shrontz sent striking workers a letter, saying that "to my knowledge, Boeing's offer represents the best overall three-year contract of any major U.S. industrial firm in recent history."	O	4365..4584
But Mr. Baker called the letter -- and the company's offer of a 10% wage increase over the life of the pact, plus bonuses -- "very weak."	O	4587..4724
He added that the company miscalculated the union's resolve and the workers' disgust with being forced to work many hours overtime.	O	4725..4856
In separate developments:   -- Talks have broken off between Machinists representatives at Lockheed Corp. and the Calabasas, Calif., aerospace company.	O	4859..5010
The union is continuing to work through its expired contract, however.	B-asynchronous	5011..5081
It had planned a strike vote for next Sunday, but that has been pushed back indefinitely.	I-asynchronous	5082..5171
-- United Auto Workers Local 1069, which represents 3,000 workers at Boeing's helicopter unit in Delaware County, Pa., said it agreed to extend its contract on a day-by-day basis, with a 10-day notification to cancel, while it continues bargaining.	O	5174..5422
The accord expired yesterday.	O	5423..5452
-- And Boeing on Friday said it received an order from Martinair Holland for four model 767-300 wide-body jetliners valued at a total of about $326 million.	O	5455..5611
The planes, long range versions of the medium-haul twin-jet, will be delivered with Pratt & Whitney PW4060 engines.	B-entrel	5614..5729
Pratt & Whitney is a unit of United Technologies Inc.	I-entrel	5730..5783
Martinair Holland is based in Amsterdam.	O	5784..5824
A Boeing spokeswoman said a delivery date for the planes is still being worked out "for a variety of reasons, but not because of the strike."	O	5827..5968
Bridget O'Brian contributed to this article.	O	5971..6015
