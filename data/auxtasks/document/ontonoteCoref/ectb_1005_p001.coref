Rural aesthetics	ROOT	0
The title of " An Irrational Old Man " makes it plain that it too is a story about an elderly person .	NONE	1
Here , however , Huang has written about a different sort of senior citizen , one who carries within himself a traditional wisdom uncorrupted by modern education .	NONE	2
This wisdom , extremely practical in nature , is part of a piecemeal oral tradition .	COREF_PREV	3
It is non-systematic and lacks mechanisms with which to check its own validity .	COREF_PREV	4
Nonetheless , when taken as a whole it presents the ideas that there are spirits in the earth and sky , and that the universe is an organic and connected whole .	COREF_PREV	5
This is the elderly 's perspective on the universe , and it gives them very different ideas about meaning and beauty .	NONE	6
In dialogue with his grandsonAh - ming , the " Irrational Old Man " passes on his old knowledge : listen to the sound of the waves the wind makes in the rice paddies to discover if the rice is ripe ; fill the bellies of grasshoppers with salt before grilling them ; sparrows are ghosts or spirits which understand human speech ; and killing the lu ti bird brings floods .	NONE	7
At the end , the old man tells the boy of the spirit of the Choshui River , which searches for people to take its place .	COREF_PREV	8
In " An Irrational Old Man , " the reader feels very strongly that Huang Chun - ming is playing two roles : that of a little boy listening intently to his grandfather and that of a mature author who longs for the past and wishes to give voice to its earthy old knowledge .	COREF_OTHER	9
Ah - sheng is the unforgettable elderly protagonist of " Drowning an Old Cat , " the next story in the collection .	NONE	10
Ah - sheng does n't have the life of ease of the " Irrational Old Man , " and he stubbornly resists the encroachment of his enemy , modern life .	COREF_PREV	11
In this tragic story , Ah - sheng is ultimately steamrollered by the massive momentum of the new .	COREF_PREV	12
But while the tragedy plays itself out , Huang makes it clear to the reader that he stands firmly on the side of Ah - sheng in this conflict .	COREF_PREV	13
Not everything in the story is bleak , however .	COREF_OTHER	14
Huang includes several comic and even farcical scenes , one of which occurs during a carnivalesque town meeting .	COREF_OTHER	15
There , Ah - sheng demonstrates a startling ability to respond to circumstances and succeeds in wrecking the meeting hall as he makes an eloquent speech on the love of one 's family and birthplace , making " the entire village jump to its feet in agitation . "	COREF_OTHER	16
And there is a farcical moment at the end as he takes off his clothes and jumps into a private swimming pool , making the ultimate protest by taking his own life .	COREF_PREV	17
