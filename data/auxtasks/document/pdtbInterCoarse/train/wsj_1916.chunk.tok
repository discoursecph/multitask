Could the collapse of I-880 have been prevented ?	O	9..57
That was the question structural engineers and California transportation officials were asking themselves yesterday as rescue workers began the gruesome task of trying to extract as many as 250 victims from beneath the concrete slabs of the double-deck Nimitz Freeway in Oakland that caved in during Tuesday 's temblor .	O	60..378
After touring the area , California Gov. George Deukmejian late yesterday called for an inquiry into the freeway 's collapse , blaming the disaster on substandard construction , the Associated Press reported .	O	381..585
The impact of the destruction of this 2.5-mile stretch of highway was tragically measured in lost lives .	B-comparison	588..692
But there are other long-term effects that raise serious questions about the ability of California 's infrastructure to withstand a major temblor .	I-comparison	693..838
It could easily be two years before the well-traveled artery that helps connect Oakland with San Francisco is reopened , and the cost to build a new stretch of highway could soar to more than $ 250 million , said Charles J. O'Connell , deputy district director in Los Angeles of the California Department of Transportation , nicknamed Caltrans .	O	841..1180
Caltrans in Sacramento said total damage from the collapsed highway is estimated at around $ 500 million .	O	1181..1285
The aftershocks of the highway tragedy are reverberating in Los Angeles as well , as local politicians spoke yesterday against plans to bring double-decking to Los Angeles freeways by 1994 .	O	1288..1476
Caltrans plans to add a second deck for buses and car pools above the median of a 2.5-mile stretch of the Harbor Freeway just south of Los Angeles , near the Memorial Coliseum .	B-comparison	1477..1652
Los Angeles County Supervisor Kenneth Hahn yesterday vowed to fight the introduction of double-decking in the area .	I-comparison	1653..1768
Caltrans abandoned double-decking in the early 1970s , following the 1971 Sylmar earthquake that destroyed freeway sections just north of Los Angeles , Mr. O'Connell explained .	O	1771..1945
That temblor measured 6.1 on the Richter scale ; Tuesday 's was	O	1946..2007
So why even consider stacking freeways now ?	O	2010..2053
" We 've run out of places to build freeways in L.A. , and the only place to go is up , " Mr. O'Connell said , although he acknowledges there are many obstacles , including cost .	O	2054..2225
But as for safety , he says double-deck freeways built today with the heavily reinforced concrete and thicker columns required after the Sylmar quake should withstand a calamitous temblor of 7.5 to 8 on the Richter scale .	O	2226..2446
Reasons for the collapse of the Nimitz Freeway were sketchy yesterday .	B-comparison	2449..2519
But most structural engineers attributed the destruction to improper reinforcement of the columns that supported the decks , and the fact that the ground beneath the highway is largely landfill and can become unstable , or " liquefy , " in a major quake .	I-comparison	2520..2769
The two-story roadway , designed in the mid-1940s and completed in 1957 , was supported by columns that apparently lacked the kind of steel reinforcement used in highways today .	O	2772..2947
While the pillars did have long metal bars running vertically through them for reinforcement , they apparently lacked an adequate number of metal " ties " that run horizontally through the column , said Leo Parker , a structural engineer in Los Angeles .	O	2948..3196
Caltrans today uses a variation of the design Mr. Parker describes , with spiraling steel rods inside .	B-comparison	3197..3298
But in the case of the Nimitz Freeway , the lack of such support caused the core of the columns to crumble and buckle under the weight of the second deck , crushing motorists who were lined up in bumper-to-bumper rush-hour traffic on the lower deck nearly 15 feet below .	I-comparison	3301..3569
Officials of the state agency did n't have any immediate explanation why the reinforcement did n't hold up .	O	3572..3677
Caltrans reinforced the highway in 1977 as part of a $ 55 million statewide project , using steel cables to tie the decks of the freeway to the columns and prevent the structure from swaying in a quake .	B-entrel	3680..3880
Caltrans spokesman Jim Drago in Sacramento declined to identify the engineering firm that did the reinforcement work .	I-entrel	3881..3998
Liability in the bridge and road collapses will revolve around whether government took " reasonable care " to build and maintain the structures , says John Messina , a Tacoma , Wash. , personal-injury attorney who specializes in highway design and maintenance cases .	O	4001..4261
The firm brought in to strengthen the structure could be liable as well .	O	4262..4334
The results of the quake certainly raise questions about whether reasonable care was taken , Mr. Messina says .	O	4337..4446
Given the seismic history of the Bay Area , " it seems to me that a 6.9 earthquake is a foreseeable event . "	O	4447..4552
Caltrans ' Mr. Drago defended the agency 's work on the Nimitz Freeway .	B-expansion	4555..4624
" The work was done properly , " he said .	I-expansion	4625..4663
" Basically , we had a severe earthquake of significant duration and it was just something the structure could n't withstand . "	O	4664..4787
Ironically , Caltrans this year began working on a second round of seismic reinforcements of freeways around the state , this time wrapping freeway columns in " steel blankets " to reinforce them .	O	4790..4982
But only bridges supported with single rows of columns were top priority , and the Nimitz Freeway , supported by double rows , was left out , Mr. Drago explained .	O	4983..5141
" The reason is that the technology is such that we 're not able to retrofit multi-column structures , " he said .	O	5142..5251
Charles McCoy in San Francisco and John R. Emshwiller in Los Angeles contributed to this article .	O	5254..5351
