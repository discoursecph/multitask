891030	NONE	1
10/30/89	NONE	2
The bolstered cellular agreement between BellSouth Corp. and LIN Broadcasting Corp. carries heightened risks and could fail to fend off McCaw Cellular Communications Inc. , the rival suitor for LIN .	POS	3
Moreover , the amended pact shows how McCaw 's persistence has pushed LIN and BellSouth into a corner , forcing huge debt on the proposed new company .	POS	4
The debt , estimated at $ 4.7 billion , could mortgage the cellular company 's future earning power in order to placate some LIN holders in the short term .	POS	5
The plan still calls for LIN to combine its cellular telephone properties with BellSouth 's and to spin off its broadcasting operations .	POS	6
But under new terms of the agreement , announced Friday , LIN holders would receive a special cash dividend of $ 42 a share , representing a payout of about $ 2.23 billion , shortly before the proposed merger .	POS	7
LIN said it expects to borrow the money to pay the dividend , but commitments from banks still have n't been obtained .	POS	8
Under previous terms , holders would have received a dividend of only $ 20 a share .	POS	9
In addition , New York-based LIN would exercise its right to buy out for $ 1.9 billion the 55 % equity interest of its partner , Metromedia Co. , in a New York cellular franchise .	POS	10
That money also would have to be borrowed .	POS	11
In effect , McCaw has forced LIN 's hand by bidding $ 1.9 billion for the stake earlier this month .	POS	12
" We 're taking on more debt than we would have liked to , " acknowledged Michael Plouf , LIN 's vice president and treasurer .	POS	13
Although he expressed confidence that the proposed new company 's cash flow would be sufficient to cover interest payments on the debt , he estimated that the company would n't be profitable until 1994 or later .	POS	14
Analyst estimate the value of the BellSouth proposal at about $ 115 to $ 125 a share .	POS	15
They value McCaw 's bid at $ 112 to $ 118 a share .	POS	16
The previous BellSouth pact was valued at about $ 98 to $ 110 a share .	POS	17
McCaw , the largest provider of cellular telephone service in the U.S. , already owns about 9.4 % of LIN 's stock .	NONE	18
In response to BellSouth 's amended pact , the Kirkland , Wash. , company extended its own offer to buy 22 million LIN shares for $ 125 apiece , which would give McCaw a 50.3 % controlling interest .	POS	19
Over the weekend , McCaw continued to call for an auction of LIN .	POS	20
Analysts said they expect McCaw to escalate the bidding again .	POS	21
" This game is n't over yet , " said Joel D. Gross , a vice president at Donaldson , Lufkin amp Jenrette Securities Corp.	NEG	22
" At some point , it will become non-economical for one company .	POS	23
But I do n't think we 're at that point yet . "	NEG	24
Under its revised proposal , Atlanta-based BellSouth would have a 50 % interest in the new cellular company and would be responsible for half of its debt .	POS	25
To sweeten the pact further -- and to ease concerns of institutional investors -- BellSouth added a provision designed to give extra protection to holders if the regional Bell company ever decides to buy the rest of the new cellular company .	POS	26
The provision , described as " back-end " protection , would require BellSouth to pay a price equivalent to what an outside party might have to pay .	POS	27
McCaw 's bid also has a similar clause .	NONE	28
Only McCaw 's proposal requires the company to begin an auction process in June 1994 for remaining shares at third-party prices .	POS	29
To mollify shareholders concerned about the long-term value of the company under the BellSouth-LIN agreement , BellSouth also agreed to pay as much as $ 10 a share , or $ 540 million , if , after five years , the trading value of the new cellular company is n't as high as the value that shareholders would have realized from the McCaw offer .	POS	30
" We 're very pleased with the new deal .	POS	31
We did n't expect BellSouth to be so responsive , " said Frederick A. Moran , president of Moran Asset Management Inc. , which holds 500,000 LIN shares .	POS	32
BellSouth 's " back-end protection was flawed previously .	NONE	33
We think this is a superior deal to McCaw 's .	POS	34
We 're surprised .	NONE	35
We did n't think a sleeping { Bell } mentality would be willing to take on dilution . "	POS	36
But Kenneth Leon , a telecommunications analyst with Bear , Stearns amp Co. , finds the BellSouth proposal still flawed because the company does n't have to wait five years to begin buying more LIN shares .	POS	37
" How many shares will be around in 1995 ? "	NONE	38
he asked .	NONE	39
" There 's nothing preventing BellSouth from buying up shares in the meanwhile . "	NONE	40
BellSouth 's revised proposal surprised many industry analysts , especially because of the company 's willingness to accept some dilution of future earnings .	POS	41
William O. McCoy , president of the company 's BellSouth Enterprises Inc. unit , said the revised agreement with LIN would dilute BellSouth earnings by about 9 % in both 1990 and 1991 and by significantly less thereafter .	POS	42
Indeed , BellSouth 's cellular operations were among the first in the country to become profitable .	NONE	43
For 1988 , BellSouth earned $ 1.7 billion , or $ 3.51 a share , on revenue of $ 13.6 billion .	POS	44
Analysts were predicting 1990 BellSouth earnings in the range of $ 3.90 a share , or $ 1.9 billion , but now those estimates are being scaled back .	POS	45
In composite trading Friday on the New York Stock Exchange , BellSouth shares fell 87.5 cents to $ 52.125 .	POS	46
In national over-the-counter trading , LIN shares soared $ 4.625 to closed at $ 112.625 , while McCaw fell $ 2.50 a share to $ 37.75 .	POS	47
The proposed BellSouth-LIN cellular company , including the newly acquired Metromedia stake , would give the new entity 55 million potential customers , including about 35 million in the nation 's top 10 markets .	POS	48
Mr. Leon of Bear Stearns speculated that McCaw , in an attempt to buy time , might consider filing an antitrust suit against BellSouth with the Justice Department and U.S. District Judge Harold Greene , who oversees enforcement of the consent decree that broke up the Bell system in 1984 .	POS	49
Indeed , McCaw seemed to hint at that option in a brief statement .	POS	50
Urging LIN directors to conduct " a fair auction on a level playing field , " McCaw asked how well the public interest would be served " with the Bell operating companies controlling over 94 % of all cellular { potential customers } in the nation 's top 10 markets . "	NONE	51
