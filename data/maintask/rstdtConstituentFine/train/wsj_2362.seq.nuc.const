British Aerospace PLC and France 's Thomson-CSF S.A. said	(NS-elaboration-additional(NS-elaboration-additional(NS-elaboration-additional(NS-elaboration-additional(NS-elaboration-additional(NS-elaboration-additional(SN-attribution
they are nearing an agreement	(NS-consequence(NS-elaboration-object-attribute
to merge their guided-missile divisions ,	NS-elaboration-object-attribute)
greatly expanding collaboration between the two defense contractors .	NS-consequence))
The 50-50 joint venture ,	(NN-List(NN-Same-Unit(NS-elaboration-additional
which may be dubbed Eurodynamics ,	NS-elaboration-additional)
would have combined annual sales of at least # 1.4 billion	(NS-restatement
( $ 2.17 billion )	NS-restatement))
and would be among the world 's largest missile makers .	NN-List))
After two years of talks , plans for the venture are sufficiently advanced	(NS-elaboration-additional(SN-enablement
for the companies to seek French and British government clearance .	SN-enablement)
The companies hope for a final agreement by year-end .	NS-elaboration-additional))
The venture would strengthen the rapidly growing ties between the two companies ,	(NS-elaboration-additional(NS-elaboration-additional(NS-circumstance(NN-List
and help make them a leading force in European defense contracting .	NN-List)
In recent months , a string of cross-border mergers and joint ventures have reshaped the once-balkanized world of European arms manufacture .	NS-circumstance)
Already , British Aerospace and French government-controlled Thomson-CSF collaborate on a British missile contract and on an air-traffic control radar system .	(NS-consequence(NN-List
Just last week they announced	(SN-attribution
they may make a joint bid	(NS-elaboration-object-attribute
to buy Ferranti International Signal PLC , a smaller British defense contractor	(NS-elaboration-additional
rocked by alleged accounting fraud at a U.S. unit .	NS-elaboration-additional))))
The sudden romance of British Aerospace and Thomson-CSF	(NN-Same-Unit(NS-elaboration-additional
-- traditionally bitter competitors for Middle East and Third World weapons contracts --	NS-elaboration-additional)
is stirring controversy in Western Europe 's defense industry .	NN-Same-Unit)))
Most threatened by closer British Aerospace-Thomson ties would be their respective national rivals ,	(NS-elaboration-additional(SN-antithesis(NS-elaboration-set-member
including Matra S.A. in France and Britain 's General Electric Co. PLC .	NS-elaboration-set-member)
But neither Matra nor GEC	(NN-Same-Unit(NS-elaboration-additional
-- unrelated to Stamford , Conn.-based General Electric Co. --	NS-elaboration-additional)
are sitting quietly by	(NS-circumstance
as their competitors join forces .	NS-circumstance)))
Yesterday , a source close to GEC confirmed	(NS-antithesis(NS-elaboration-additional(NS-elaboration-additional(NS-elaboration-additional(NS-elaboration-additional(SN-attribution
that his company may join the Ferranti fight , as part of a possible consortium	(NS-elaboration-object-attribute
that would bid against British Aerospace and Thomson-CSF .	NS-elaboration-object-attribute))
Companies	(NN-Same-Unit(NS-elaboration-object-attribute
with which GEC has had talks about a possible joint Ferranti bid	NS-elaboration-object-attribute)
include Matra , Britain 's Dowty Group PLC , West Germany 's Daimler-Benz AG , and France 's Dassault group .	NN-Same-Unit))
But it may be weeks	(NS-reason(NS-attribution(SN-temporal-after
before GEC and its potential partners decide whether to bid ,	SN-temporal-after)
the source indicated .	NS-attribution)
GEC plans first to study Ferranti 's financial accounts ,	(NS-elaboration-object-attribute
which	(NN-Same-Unit(NS-attribution
auditors recently said	NS-attribution)
included # 215 million in fictitious contracts at a U.S. unit , International Signal & Control Group ,	(NS-elaboration-object-attribute
with which Ferranti merged last year .	NS-elaboration-object-attribute)))))
Also , any GEC bid might be blocked by British antitrust regulators ;	(NN-Problem-Solution(NS-reason
Ferranti is GEC 's main competitor on several key defense-electronics contracts ,	(SN-circumstance
and its purchase by GEC may heighten British Defense Ministry worries about concentration in the country 's defense industry .	SN-circumstance))
A consortium bid , however , would diminish GEC 's direct role in Ferranti	(NS-consequence
and might consequently appease ministry officials .	NS-consequence)))
A British Aerospace spokeswoman appeared unperturbed by the prospect of a fight with GEC for Ferranti :	(NS-elaboration-additional
`` Competition is the name of the game , ''	(NS-attribution
she said .	NS-attribution)))
At least one potential GEC partner , Matra , insists	(NS-elaboration-additional(SN-attribution
it is n't interested in Ferranti .	SN-attribution)
`` We have nothing to say about this affair ,	(NS-attribution(NS-elaboration-object-attribute
which does n't concern us , ''	NS-elaboration-object-attribute)
a Matra official said Sunday .	NS-attribution))))))
The missile venture ,	(NS-elaboration-additional(NS-elaboration-additional(NN-Same-Unit(NS-attribution
the British Aerospace spokeswoman said ,	NS-attribution)
is a needed response to the `` new environment '' in defense contracting .	NN-Same-Unit)
For both Thomson and British Aerospace , earnings in their home markets have come under pressure from increasingly tight-fisted defense ministries ;	(NN-List
and Middle East sales , a traditional mainstay for both companies ' exports , have been hurt by five years of weak oil prices .	NN-List))
The venture 's importance for Thomson is great .	(NS-elaboration-additional(NS-elaboration-additional(NS-elaboration-additional
Thomson feels	(SN-attribution
the future of its defense business depends on building cooperation with other Europeans .	SN-attribution))
The European defense industry is consolidating ;	(SN-consequence(NS-example
for instance , West Germany 's Siemens AG recently joined GEC in a takeover of Britain 's Plessey Co. ,	(NN-List
and Daimler-Benz agreed to buy Messerschmitt-Boelkow Blohm G.m.b. H .	NN-List))
In missiles , Thomson is already overshadowed by British Aerospace and by its home rival , France 's Aerospatiale S.A. ;	(SN-circumstance
to better compete ,	(NS-enablement(NS-attribution
Thomson officials say ,	NS-attribution)
they need a partnership .	NS-enablement))))
To justify 50-50 ownership of the planned venture ,	(SN-purpose
Thomson would make a cash payment to British Aerospace .	SN-purpose))))
Annual revenue of British Aerospace 's missile business is about # 950 million ,	(NS-attribution
a Thomson spokesman said .	NS-attribution))
British Aerospace 's chief missile products include its 17-year-old family of Rapier surface-to-air missiles .	(NN-List
Thomson missile products , with about half British Aerospace 's annual revenue , include the Crotale surface-to-air missile family .	NN-List))
