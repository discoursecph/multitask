Americans are drinking less , but young professionals from Australia to West Germany are rushing to buy premium-brand American vodka , brandy and other spirits .	O	9..167
In particular , many are snubbing the scotch preferred by their parents and opting for bourbon , the sweet firewater from the Kentucky countryside .	O	168..313
With U.S. liquor consumption declining steadily , many American producers are stepping up their marketing efforts abroad .	O	316..436
And those efforts are paying off : Spirits exports jumped more than 2 1/2 times to $ 157.2 million in 1988 from $ 59.8 million in 1983 , according to the Distilled Spirits Council of the U.S. , a trade group .	O	437..640
" Spirits companies now view themselves as global marketers , " says Michael Bellas , president of Beverage Marketing Corp. , a research and consulting firm .	O	643..795
" If you want to be a player , you have to be in America , Europe and the Far East .	B-expansion	796..876
You must have world-class brands , a long-term perspective and deep pockets . "	I-expansion	877..953
The internationalization of the industry has been hastened by foreign companies ' acquisitions of many U.S. producers .	B-expansion	956..1073
In recent years , for example , Grand Metropolitan PLC of Britain acquired Heublein Inc. , while another British company , Guinness PLC , took over United Distillers Group and Schenley Industries Inc .	I-expansion	1074..1269
But the shift has also been fueled by necessity .	B-contingency	1272..1320
While premium-brand spirits like Smirnoff vodka and Jack Daniel 's whiskey are riding high in the U.S. , domestic spirits consumption fell 15 % to 141.1 million cases in 1988 from 166 million cases in 1979 .	I-contingency	1321..1524
In recent years , growth has come in the foreign markets .	B-expansion	1527..1583
U.S. brandy exports more than doubled last year to 360,000 proof gallons , a standard industry measure , according to Jobson Beverage Alcohol Group , an industry association .	B-expansion	1584..1755
Exports of rum surged 54 % to 814,000 proof gallons .	B-entrel	1756..1807
Mexico is the biggest importer of both rum and brandy from the U.S.	I-entrel	1808..1875
Japan , the world 's third-largest liquor market after the U.S. and Britain , helped American companies in April when it lowered its tax on imported spirits and levied a tax on many domestic products .	O	1878..2075
California wineries , benefiting from lowered trade barriers and federal marketing subsidies , are expanding aggressively into Japan , as well as Canada and Great Britain .	B-expansion	2076..2244
In Japan , the wineries are promoting their products ' Pacific roots and courting restaurant and hotel chefs , whose recommendations carry weight .	I-expansion	2245..2388
In Australia , Britain , Canada and Greece , Brown-Forman Corp. has increased its marketing of Southern Comfort Liqueur .	B-expansion	2391..2508
Using cinema , television and print ads , the company pitches Southern Comfort as a grand old drink of the antebellum American South .	B-comparison	2509..2640
The biggest foreign inroads , though , have been made by bourbon .	B-contingency	2643..2706
While U.S. makers of vodka , rum and other spirits compete against powerhouses abroad , trade agreements prohibit any other country from making bourbon .	I-contingency	2707..2857
( All bourbon comes from Kentucky , though Jack Daniel 's Tennessee whiskey often is counted as bourbon because of similarity of taste . )	O	2858..2991
Moreover , just as vodka has acquired an upscale image in the U.S. , bourbon has become fashionable in many foreign countries , a uniquely American product tied to frontier folklore .	O	2992..3171
How was the West won ?	B-expansion	3172..3193
With a six-shooter in one hand and bourbon in the other .	I-expansion	3194..3250
" We imagine with bourbon the Wild West , Western motion pictures and gunmen appearing , " says Kenji Kishimoto , vice president of Suntory International Corp. , a division of Suntory Ltd. , Japan 's largest liquor company .	B-entrel	3253..3468
Suntory distributes Brown-Forman bourbons in Japan .	I-entrel	3469..3520
Bourbon makes up just 1 % of world-wide spirits consumption , but it represented 57 % of U.S. liquor exports last year , according to Jobson ; no other category had more than 19 % .	B-contingency	3523..3697
Big U.S. distillers are fiercely vying for this market , which grew to $ 77 million last year from $ 33 million in 1987 , according to government figures .	I-contingency	3698..3848
Jim Beam Brands Co. , a division of American Brands Inc. , is the leading exporter of bourbon and produces 10 other types of liquor .	B-entrel	3851..3981
The company says it will increase its international advertising 35 % in 1990 , with bourbon representing most of that amount .	I-entrel	3982..4105
Guinness 's Schenley Industries unit has increased its TV advertising in Japan and has built partnerships with duty-free shops throughout Asia , enabling it to install prominent counter displays .	B-contingency	4108..4301
The company 's I.W. Harper brand is the leading bourbon in Japan , with 40 % of the market .	I-contingency	4302..4390
Bourbon exporters have succeeded in Japan where other industries have failed , avoiding cultural hitches in marketing and distribution by allying themselves with local agents .	O	4393..4567
Jim Beam Brands has a distribution partnership with Nikka Whiskey Co. , a distiller .	B-expansion	4568..4651
Seagram Co. , which exports Four Roses bourbon , has such a link with Kirin Brewery Co .	I-expansion	4652..4737
Some bourbon makers advertise abroad as they do at home .	B-expansion	4740..4796
To promote Jack Daniel 's overseas , Brown-Forman uses the same photos of front porches from Lynchburg , Va. , and avuncular old men in overalls and hightops .	I-expansion	4797..4951
Jim Beam print ads , however , strike different chords in different countries .	B-expansion	4954..5030
In Australia , land of the outback , a snapshot of Jim Beam lies on a strip of hand-tooled leather .	B-comparison	5031..5128
West Germans get glitz , with bourbon in the foreground and a posh Beverly Hills hotel in the background .	B-comparison	5129..5233
Ads for England are artsy and irreverent .	B-expansion	5234..5275
One ad features a huge robot carrying a voluptuous woman in a faint .	B-entrel	5276..5344
The tagline : " I only asked if she wanted a Jim Beam .	I-entrel	5345..5397
