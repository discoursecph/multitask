Senate Democrats who favor cutting the capital-gains tax are n't ready to line up behind the leading Senate proposal .	root	9..125
Their reluctance to support the proposal is another blow to the capital-gains cut , which has had a roller-coaster existence since the beginning of the year , when it was considered dead and then suddenly revived and was passed by the House .	norel	128..367
Nevertheless , Oregon Sen. Bob Packwood , the ranking GOP member on the tax-writing Senate Finance Committee , last night introduced his plan as an amendment to a pending measure authorizing U.S. aid for Poland and Hungary .	norel	370..590
Senate Majority Leader George Mitchell ( D. , Maine ) was confident he had enough votes to block the maneuver on procedural grounds , perhaps as soon as today .	norel	593..748
Mr. Packwood all but conceded defeat , telling Mr. Mitchell : " I sense at this stage you may have the votes . "	conjunction	749..856
The two lawmakers sparred in a highly personal fashion , violating usual Senate decorum .	norel	859..946
Their tone was good-natured , with Mr. Packwood saying he intended to offer the proposal again and again on future legislation and Sen. Mitchell saying he intended to use procedural means to block it again and again .	concession	947..1162
Although the proposal , authored by Mr. Packwood and Sen. William Roth ( R. , Del. ) , appears to have general backing by Republicans , their votes are n't sufficient to pass it .	norel	1165..1336
And Democrats , who are under increasing pressure from their leaders to reject the gains-tax cut , are finding reasons to say no , at least for now .	conjunction	1337..1482
A major reason is that they believe the Packwood-Roth plan would lose buckets of revenue over the long run .	entrel	1483..1590
The Packwood-Roth proposal would reduce the tax depending on how long an asset was held .	norel	1593..1681
It also would create a new individual retirement account that would shield from taxation the appreciation on investments made for a wide variety of purposes , including retirement , medical expenses , first-home purchases and tuition .	conjunction	1682..1913
" A number of us are not going to touch capital gains , IRAs or anything else unless it contributes to deficit-reduction , " said Sen. Charles Robb ( D. , Va. ) , who is one of the 10 to 20 Democrats who the Bush administration believes might favor giving preferential treatment to capital gains .	norel	1916..2204
President Bush has been hearing this kind of opposition first hand during meetings over the past two days with Democratic senators at the White House .	norel	2207..2357
And at a luncheon meeting Tuesday of Democratic senators , there was outspoken opposition to cutting the capital-gains tax this year , according to participants .	conjunction	2358..2517
The trend is making advocates of the tax cut less optimistic about success .	norel	2520..2595
" There is a one-out-of-three shot of getting it this year , " said Sen. David Boren of Oklahoma , a leading Democratic proponent of cutting the capital-gains tax .	restatement	2596..2755
He called the battle " uphill . "	conjunction	2756..2786
Other Democrats who favor a capital-gains cut are even more pessimistic .	norel	2789..2861
" There will be no capital-gains bill this year , " said Sen. Dale Bumpers ( D. , Ark . ) .	restatement	2862..2945
" I 'm probably not going to vote for any capital-gains proposal .	conjunction	2946..3009
The IRA portion ( of the Packwood-Roth plan ) is irresponsible . "	cause	3010..3072
Another significant factor in the capitalgains debate is the extent to which it has become a purely political battle between President Bush and Senate Majority Leader Mitchell .	norel	3075..3251
Mr. Mitchell has made clear to his wavering colleagues that the issue is important to him personally .	restatement	3252..3353
Today , Sen. Mitchell and other leading Democrats plan to turn up the heat again by holding a news conference to bash the proposal .	norel	3356..3486
Estimates requested by Sen. Mitchell from the Congressional Joint Taxation Committee show that the richest 100 taxpayers got an average benefit from the capital-gains differential of $ 13 million each in 1985 , the last year for which figures are available .	cause	3487..3742
White House officials acknowledged yesterday that Democrats still are reluctant to publicly express support for the Packwood-Roth capital gains proposal because they are loath to buck Sen. Mitchell .	norel	3745..3943
As a result , the officials said they are open to making a variety of deals with Senate Democrats to win their support for a capital-gains tax cut .	cause	3944..4090
Democrats asked in this week for discussions with President Bush have suggested ways of " tinkering " with the Packwood-Roth proposal , suggesting an interest in looking for a modified version they can back , one official said .	norel	4093..4316
In addition , White House aides think that there are numerous other important measures Democrats badly wanted passed -- such as the scaling back of a controversial catastrophic health-care plan for the elderly -- that might provide the president leverage in cutting deals with Democrats .	norel	4319..4605
A capital-gains tax cut might be paired with such measures to help ensure passage .	cause	4606..4688
Other possibilities include a child-care initiative and an increase in the minimum wage .	conjunction	4689..4777
If they ca n't secure immediate passage of a capital-gains plan , administration officials also are n't ruling out making a deal with Congress to put off a vote until a firm date in the future , even next year .	norel	4780..4986
But the officials insist that such a deal on a future vote would have to apply to both the House and the Senate .	comparison	4987..5099
Gerald F. Seib contributed to this article .	norel	5102..5145
Japanese immigration authorities said they found 658 more Chinese among Vietnamese boat people , bringing the number of Chinese trying to enter Japan by posing as Vietnamese refugees this year to 1,642 .	norel	5148..5349
Japan plans to send the Chinese back home and is negotiating with the Chinese government , a Justice Ministry official said .	norel	5352..5475
The Chinese were among 3,372 boat people supposedly from Vietnam who arrived in Japan this year , compared with 219 for all of 1988 , the official said .	entrel	5476..5626
The 658 Chinese , who have been in a refugee-assistance center , were sent to immigration facilities yesterday pending deportation to China , the official said .	norel	5629..5786
On Sept. 13 , Japan began a policy of screening boat people , accepting only those deemed to be political refugees .	norel	5789..5902
Francoise Verne , 52-year-old former deputy director of France 's mint , faces prison for her theft of some 67 rare coins from the mint 's collections .	norel	5905..6052
Second in command from 1979 to 1984 , Mrs. Verne told a Paris court that the " great disorder " that reigned at the agency led her into temptation .	entrel	6053..6197
Before an inventory in 1984 that showed the " disappearance " of 944 coins valued at about 2.9 million French francs ( about $ 465,000 ) , there had n't been any stock-taking since 1868 .	restatement	6198..6377
Tony Lambert , Mrs. Verne 's successor , says the mint 's losses from the theft run into the hundreds of thousands of francs .	entrel	6378..6499
El Salvador is destroying more than 1.6 million pounds of food that had rotted in government warehouses , government officials said .	norel	6502..6633
The state Supply Regulator Institute is to burn rice , corn and beans that spoiled because of neglect and corruption in the previous Christian Democrat government , a statement from the information service SISAL said .	restatement	6634..6849
During the past administration the foodstuffs were first bought by the institute , then sold at low prices to " unscrupulous businessmen " who resold them to the institute at inflated prices , the statement said .	restatement	6850..7058
A black-draped cruise liner sailed into Naples yesterday bringing 800 Libyans threatening vengeance if Italy refuses to pay compensation for more than 30 years of colonial rule .	norel	7061..7238
Another 250 Libyans were already in Italy to stage a day of mourning for victims of Italy 's colonial rule between 1911 and 1943 , when Tripoli says Rome kidnapped 5,000 Libyans and deported them as forced labor .	conjunction	7239..7449
Libya 's revolutionary Committees have threatened attacks on Italians if Rome does n't pay compensation .	restatement	7450..7552
But officials in Rome say the issue was legally resolved by a settlement between Italy and King Idris , deposed by Col. Muammar Gadhafi in 1969 .	comparison	7553..7696
Canadian Indians are taking five countries to court in a bid to stop low military flights over their homes , the Dutch Defense Ministry said .	norel	7699..7839
Representatives of the Inuit and Cree peoples living in Quebec and Labrador in northeastern Canada told the ministry of the planned action at a meeting , a ministry spokesman said .	entrel	7840..8019
They also wanted to prevent a NATO training base being built in the region , he said .	norel	8020..8104
The action , in the Canadian Federal Court , will be against Canada , the Netherlands , West Germany , Britain and the U.S. , the ministry spokesman said .	entrel	8105..8253
Japan suspended imports of French mushrooms after finding some contaminated by radiation , an official of the Ministry of Health and Welfare said .	norel	8256..8401
Japan has been testing imported food from Europe since the April 1986 Chernobyl accident in the Soviet Union , the spokesman said .	entrel	8402..8531
Since then , the ministry has announced 50 bans on food imports from European countries , including Italy , Spain , Turkey , Greece and the Soviet Union .	norel	8532..8680
The Venice city council is battling plans to tap huge gas fields off the coast that it says will speed up the city 's slow sinking into its lagoon .	norel	8683..8829
AGIP , the state-owned energy giant , made the announcement about the gas field last month .	norel	8832..8921
Located six miles northeast of Venice , the field contains 875 billion cubic feet of methane gas - one-tenth of Italy 's reserves .	entrel	8922..9048
Alarmed councilors say the project could jeopardize costly efforts to stop , or slow down , the subsidence that makes Venice subject to regular and destructive flooding .	norel	9051..9218
The council unanimously opposed the idea of AGIP pumping out the methane gas and swiftly appealed to the company and to Prime Minister Giulio Andreotti , who has yet to reply .	cause	9219..9393
AGIP refused to reconsider and says drilling is due to start early next year .	norel	9396..9473
" It 's unlikely extracting the gas will cause subsidence , " says a spokeswoman .	cause	9474..9551
Thieves stole a 12th century fresco from an abandoned church in Camerino , Italy , by removing the entire wall on which the work had been painted , police said ... .	norel	9554..9715
West Germany 's BMW commissioned the Nuremberg post office to test a prototype battery-powered car .	norel	9716..9814
The vehicle has a top speed of 65 miles an hour and requires recharging from a standard wall socket every 100 miles .	entrel	9815..9931
