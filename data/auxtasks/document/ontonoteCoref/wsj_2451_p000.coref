For 20 years , federal rules have barred the three major television networks from sharing in one of the most lucrative and fastest - growing parts of the television business .	ROOT	0
And for six years , NBC , ABC and CBS have negotiated with Hollywood studios in a futile attempt to change that .	COREF_PREV	1
But with foreign companies snapping up U.S. movie studios , the networks are pressing their fight harder than ever .	COREF_PREV	2
They hope the foreign deals will divide the Hollywood opposition and prod Congress to push for ending federal rules that prohibit the networks from grabbing a piece of rerun sales and owning part of the shows they put on the air .	COREF_PREV	3
Even network executives , however , admit privately that victory -- either in Congress or in talks with the studios -- is highly doubtful any time soon .	COREF_PREV	4
And so the networks also are pushing for new ways to sidestep the `` fin - syn '' provisions , known formally as the Financial Interest and Syndication Rules .	COREF_PREV	5
That became clear last week with the disclosure that National Broadcasting Co. , backed by the deep pockets of parent General Electric Co. , had tried to help fund Qintex Australia Ltd. 's now - scuttled $ 1.5 billion bid for MGM / UA Communications Co .	COREF_PREV	6
NBC 's interest may revive the deal , which MGM / UA killed last week when the Australian concern had trouble raising cash .	COREF_PREV	7
Even if that deal is n't revived , NBC hopes to find another .	COREF_PREV	8
`` Our doors are open , '' an NBC spokesman says .	COREF_PREV	9
NBC may yet find a way to take a passive , minority interest in a program - maker without violating the rules .	COREF_PREV	10
And any NBC effort could prompt CBS Inc. and ABC 's parent , Capital Cities / ABC Inc. , to look for ways of skirting the fin - syn regulations .	COREF_PREV	11
But the networks ' push may only aggravate an increasingly bitter rift between them and Hollywood studios .	COREF_OTHER	12
Both sides are to sit down next month for yet another meeting on how they might agree on reducing fin - syn restraints .	COREF_PREV	13
Few people privy to the talks expect the studios to budge .	COREF_OTHER	14
The networks still are `` uninhibited in their authority '' over what shows get on the air , charges Motion Picture Association President Jack Valenti , the most vociferous opponent of rescinding the rules .	COREF_OTHER	15
Studios are `` powerless '' to get shows in prime - time lineups and keep them there long enough to go into lucrative rerun sales , he contends .	COREF_PREV	16
And that 's why the rules , for the most part , must stay in place , he says .	COREF_PREV	17
Studio executives in on the talks - including officials at Paramount Communications Inc. , Fries Entertainment Inc. , Warner Communications Inc. and MCA Inc. -- declined to be interviewed .	COREF_OTHER	18
But Mr. Valenti , who represents the studios , asserts : `` The whole production industry , to a man , is on the side of preserving '' the rules .	COREF_PREV	19
Such proclamations leave network officials all the more doubtful that the studios will bend .	COREF_PREV	20
`` They do n't seem to have an incentive to negotiate , '' says one network executive .	COREF_PREV	21
`` And there 's no indication that Washington is prepared to address the rules .	COREF_OTHER	22
That 's the problem , is n't it ? ''	COREF_PREV	23
Indeed it is .	COREF_PREV	24
Congress has said repeatedly it wants no part of the mess , urging the studios and the networks , which license rights to air shows made by the studios , to work out their own compromise .	COREF_PREV	25
But recent developments have made the networks -- and NBC President Robert Wright , in particular -- ever more adamant that the networks must be unshackled to survive .	COREF_PREV	26
The latest provocation : Sony Corp. 's plan to acquire Columbia Pictures Entertainment Inc. for $ 3.4 billion , and to buy independent producer Guber Peters Entertainment Co. for $ 200 million .	NONE	27
`` I wonder what Walter Cronkite will think of the Sony / Columbia Broadcast System Trinitron Evening News with Dan Rather broadcast exclusively from Tokyo , '' wrote J.B. Holston , an ,NBC vice president in a commentary in last week 's issue of Broadcasting magazine .	COREF_OTHER	28
In his article , Mr. Holston , who was in Europe last week and unavailable , complained that the `` archaic restraints '' in fin - syn rules have `` contributed directly to the acquisition of the studios by non-U.S. enterprises .	COREF_PREV	29
'' ( He did n't mention that NBC , in the meantime , was hoping to assist Australia 's Qintex in buying	COREF_PREV	30
An NBC spokesman counters that Mr. Holston 's lament was `` entirely consistent '' with NBC plans because the U.S. rules would limit NBC 's involvement in the Qintex deal so severely as to be `` light years away from the type of unrestrained deals available to Sony -- and everyone else except the three networks . ''	COREF_PREV	31
The Big Three 's drumbeat for deregulation began intensifying in the summer when the former Time Inc. went ahead with plans to acquire Warner .	COREF_OTHER	32
Although Time already had a long - term contract to buy movies from Warner , the merger will let , Time 's largely unregulated pay - cable channelHome Box Office , own the Warner movies aired on HBO -- a vertical integration that is effectively blocked by fin - syn regulations .	COREF_PREV	33
NBC 's Mr. Wright led the way in decrying the networks ' inability to match a Time - Warner combination .	COREF_OTHER	34
He spoke up again when the Sony bid for Columbia was announced .	COREF_PREV	35
Since NBC 's interest in the Qintex bid for MGM / UA was disclosed , Mr. Wright has n't been available for comment .	COREF_PREV	36
With a Qintex deal , NBC would move into uncharted territory -- possibly raising hackles at the studios and in Washington .	COREF_PREV	37
`` It 's never really been tested , '' says William Lilley III , who as a top CBS executive spent years lobbying to have the rules lifted .	COREF_OTHER	38
He now runs Policy Communications in Washington , consulting to media companies .	COREF_PREV	39
Fin - syn rules do n't explicitly block a network from buying a passive , small stake in a company that profits from the rerun syndication networks ca n't enjoy .	COREF_PREV	40
Hence , NBC might be able to take , say , a 5 % stake in a company such as MGM / UA .	COREF_OTHER	41
If the transaction raised objections , the studio 's syndication operations could be spun off into a separate firm in which the network does n't have a direct stake .	COREF_PREV	42
But such convolutions would still block the networks from grabbing a big chunk of the riches of syndication .	COREF_OTHER	43
Under current rules , even when a network fares well with a 100 % - owned series -- ABC , for example , made a killing in broadcasting its popular crime / comedy `` Moonlighting '' -- it is n't allowed to share in the continuing proceeds when the reruns are sold to local stations .	COREF_OTHER	44
Instead , ABC will have to sell off the rights for a one - time fee .	COREF_PREV	45
The networks admit that the chances of getting the relief they want are slim -- for several years at the least .	COREF_OTHER	46
Six years ago they were tantalizingly close .	NONE	47
The Reagan - era Federal Communications Commission had ruled in favor of killing most of the rules .	COREF_OTHER	48
Various evidence , including a Brookings Institution study of some 800 series that the networks had aired and had partly owned in the 1960s , showed the networks did n't wield undue control over the studios as had been alleged .	COREF_PREV	49
But just eight days before the rules were to die , former President Ronald Reagan , a one - time actor , intervened on behalf of Hollywood .	COREF_PREV	50
The FCC effort collapsed .	NONE	51
The networks and studios have bickered ever since .	COREF_OTHER	52
Network officials involved in the studio talks may hope the foreign influx builds more support in Washington , but that seems unlikely .	COREF_OTHER	53
In Congress , the issue falters : It 's about money , not program quality , and Hollywood has lots of clout given its fund raising for senators and representatives overseeing the issue .	COREF_PREV	54
A spokesman for Rep. Edward J. Markey ( D - Mass. ) , who heads a subcommittee that oversees the FCC , says Mr. Markey feels `` the world has been forever changed by the Sony - Columbia deal . ''	COREF_PREV	55
But he said Mr. Markey hopes this pushes the networks and studios to work it out on their own .	COREF_PREV	56
And at the FCC , meanwhile , new Chairman Alfred C. Sikes has said he wants the two sides to hammer out their own plan .	COREF_PREV	57
