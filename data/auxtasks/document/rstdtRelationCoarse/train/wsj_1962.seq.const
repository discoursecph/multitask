The stock market 's dizzying gyrations during the past few days have made a lot of individual investors wish	(Summary-Evaluation-Topic(Elaboration(Elaboration(Elaboration(Cause(Attribution
they could buy some sort of insurance .	Attribution)
After all , they wo n't soon forget the stock bargains	(Elaboration
that became available after the October 1987 crash .	Elaboration))
But	(Elaboration(Comparison(Comparison(Summary-Evaluation-Topic(Textual-organization(Comparison
while they want to be on the alert for similar buying opportunities now ,	Comparison)
they 're afraid of being hammered by another terrifying plunge .	Textual-organization)
The solution , at least for some investors , may be a hedging technique	(Elaboration(Elaboration
that 's well known to players in the stock-options market .	Elaboration)
Called a `` married put , ''	(Temporal
the technique is carried out	(Joint
by purchasing a stock	(Temporal
and simultaneously buying a put option on that stock .	Temporal)))))
It 's like `` fire insurance , ''	(Elaboration(Attribution
says Harrison Roth , the senior options strategist at Cowen & Co .	Attribution)
Because a put option gives its owner the right , but not the obligation ,	(Cause(Elaboration
to sell a fixed number of shares of the stock at a stated price on or before the option 's expiration date ,	Elaboration)
the investor is protected against a sudden drop in the stock 's price .	Cause)))
But most investment advisers do n't recommend using married puts all the time .	(Cause
That 's because the cost of buying put options eats into an investor 's profit	(Elaboration(Temporal
when stock prices rise .	Temporal)
`` This is the type of fire insurance	(Summary-Evaluation-Topic(Attribution(Elaboration
you only buy	(Temporal
when the nearby woods are on fire , ''	Temporal))
says Mr. Roth .	Attribution)
`` You always want your house insured ,	(Comparison
but you do n't always feel the need	(Elaboration
for your investments to be insured . ''	Elaboration))))))
In addition to hedging new stock purchases ,	(Elaboration(Elaboration
the married-put technique can be used to protect stocks	(Elaboration
that an investor already owns .	Elaboration))
In either case , the investor faces three possible outcomes :	(Elaboration
-- If the stock goes up in price between now and the put 's expiration date ,	(Joint(Elaboration(Cause
the put will probably expire worthless .	Cause)
The investor will be out the cost of the put ,	(Cause(Elaboration
which is called the `` premium , ''	Elaboration)
and this loss will reduce the stock-market profit .	Cause))
-- If the stock stays at the same price between now and the put 's expiration date ,	(Joint(Elaboration(Cause
the investor 's loss will be limited to the cost of the put , less any amount	(Elaboration
realized from a closing sale of the put .	Elaboration))
The worst-case scenario would be if the put expires worthless .	Elaboration)
-- If the price of the stock declines ,	(Elaboration(Elaboration(Cause
the put will increase in value .	Cause)
Once the stock price is less than the exercise price , or `` strike price , '' of the put ,	(Temporal
the gain will match the loss on the stock dollar for dollar .	Temporal))
The put establishes a minimum selling price for the stock during its life .	(Elaboration
When a stock falls below the put 's strike price ,	(Elaboration(Joint(Temporal
the investor simply sells the stock at a loss	(Joint
and simultaneously sells the put at a profit .	Joint))
Or , the investor can exercise the put ,	(Elaboration(Joint
by tendering the stock to his or her broker in return for payment from another investor	(Elaboration
who has sold a put on the same stock .	Elaboration))
Brokers handle such transactions through the Options Clearing Corp. ,	(Elaboration
which guarantees all option trades .	Elaboration)))
The accompanying table shows	(Elaboration(Elaboration(Attribution
how this strategy would work for three stocks .	Attribution)
Though not reflected in the table ,	(Elaboration(Comparison
an investor should know	(Attribution
that the cost of the option insurance can be partially offset by any dividends	(Elaboration
that the stock pays .	Elaboration)))
For example , Tenneco Inc. pays a quarterly dividend of 76 cents ,	(Elaboration(Cause(Elaboration
which would be received	(Temporal
before the February option expires	Temporal))
and , thus , reduce the cost	(Textual-organization(Elaboration
of using the technique	Elaboration)
by that amount .	Textual-organization))
In this case , the investor 's risk would n't exceed 3.6 % of the total investment .	Elaboration)))
To simplify the calculations ,	(Cause
commissions on the option and underlying stock are n't included in the table .	Cause)))))))))))
There are more than 650 stocks	(Comparison(Elaboration(Elaboration(Summary-Evaluation-Topic(Temporal(Elaboration
on which options may be bought and sold ,	(Elaboration
including some over-the-counter stocks .	Elaboration))
But some investors might prefer a simpler strategy	(Comparison
then hedging their individual holdings .	Comparison))
They can do this	(Joint
by purchasing `` index puts , ''	(Elaboration
which are simply put options on indexes	(Elaboration
that match broad baskets of stocks .	Elaboration))))
For instance , the most popular index option is the S & P 100 option ,	(Elaboration(Elaboration
commonly called the OEX .	Elaboration)
It is based on the stocks	(Elaboration
that make up Standard & Poor 's 100-stock index .	Elaboration)))
Unlike options on individual issues , index options are settled only in cash ,	(Joint
and no stock is ever tendered .	Joint))
But while index options are convenient ,	(Elaboration(Elaboration(Comparison
they have several disadvantages .	Comparison)
For one thing , an investor 's portfolio might not closely match the S & P 100 .	(Cause
As a result , the OEX insurance may or may not fully protect an investor 's holdings in the event of a market decline .	Cause))
In addition , OEX options were suspended from trading last Friday afternoon ,	(Comparison(Cause(Joint(Temporal
after the stock-market sell-off got under way	Temporal)
and trading in the S & P-500 futures contract was halted .	Joint)
So an investor	(Textual-organization(Elaboration
who wanted to realize a profit on OEX puts after the trading suspension	Elaboration)
would have been out of luck .	Textual-organization))
On the other hand , only a handful of individual issues were suspended from trading on Friday .	(Elaboration
Normally ,	(Textual-organization(Temporal
once the underlying investment is suspended from trading ,	Temporal)
the options on those investments also do n't trade .	Textual-organization))))))
Ultimately , whether the insurance	(Elaboration(Elaboration(Textual-organization(Elaboration
provided	(Joint
by purchasing puts	Joint))
is worthwhile depends on the cost of the options .	Textual-organization)
That cost rises in times of high market volatility .	(Comparison
But it still might be cheaper	(Comparison
than taking a major hit .	Comparison)))
The protection from using married puts is clearly superior to that	(Elaboration(Elaboration
afforded by another options strategy	(Elaboration(Elaboration
some investors consider using during troubled times :	Elaboration)
selling call options on stocks	(Elaboration(Elaboration
the investor owns .	Elaboration)
A call option is similar to a put ,	(Comparison
except that it gives its owner the right	(Elaboration
to buy shares at a stated price until expiration .	Elaboration)))))
Selling a call option gives an investor a small buffer against a stock-market decline .	(Comparison(Cause
That 's because it reduces the cost of the stock by the amount of premium	(Elaboration
received from the sale of the call .	Elaboration))
But	(Cause(Textual-organization(Cause
if the price of the stock rises above the strike price of the option ,	Cause)
the stock is almost certain to be called away .	Textual-organization)
And in that case , the investor misses out on any major upside gain .	Cause)))))
These calculations exclude the effect of commissions paid and dividends	(Elaboration(Elaboration
received from the stock .	Elaboration)
All prices are as of Monday 's close .	Elaboration))
