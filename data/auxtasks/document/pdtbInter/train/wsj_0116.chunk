If you'd really rather have a Buick, don't leave home without the American Express card.	B-pragmatic condition	9..97
Or so the slogan might go.	B-cause	100..126
American Express Co. and General Motors Corp. 's beleaguered Buick division are joining forces in a promotion aimed at boosting Buick's sales while encouraging broader use of the American Express card.	I-cause	127..328
The companies are giving four-day vacations for two to Buick buyers who charge all or part of their down payments on the American Express green card.	B-entrel	331..480
They have begun sending letters explaining the program, which began Oct. 18 and will end Dec. 18, to about five million card holders.	B-entrel	481..614
Neither company would disclose the program's cost.	I-entrel	615..665
Buick approached American Express about a joint promotion because its card holders generally have a "good credit history" and are "good at making payments," says a spokeswoman for the division.	O	668..861
American Express also represents the upscale image "we're trying to project," she adds.	O	862..949
Buick has been seeking for the past few years to restore its reputation as "the doctor's car" -- a product for upscale professionals.	B-entrel	952..1085
Sales were roughly flat in the 1989 model year compared with a year earlier, though industry sales fell.	B-contrast	1086..1190
But since the 1990 model year began Oct. 1, Buick sales have plunged 33%.	I-contrast	1191..1264
For American Express, the promotion is part of an effort to broaden the use of its card for retail sales, where the company expects to get much of the future growth in its card business.	B-entrel	1267..1453
Traditionally, the card has been used mainly for travel and entertainment expenses.	I-entrel	1454..1537
Phillip Riese, an American Express executive vice president, says the promotion with Buick is his company's first with an auto maker, but "hopefully {will be} the first of many" in the company's effort to promote its green card as "the total car-care card."	O	1540..1797
To that end, American Express has been signing up gasoline companies, car repair shops, tire companies and car dealers to accept the card.	O	1798..1936
Many auto dealers now let car buyers charge part or all of their purchase on the American Express card, but few card holders realize this, Mr. Riese says.	O	1939..2093
Until now, however, buyers who wanted to finance part of a car purchase through General Motors Acceptance Corp. couldn't put their down payment on a charge card because of possible conflicts with truth-in-lending and state disclosure laws over finance rates, says a spokesman for the GM finance arm.	O	2096..2395
But GMAC approved the Buick program, he says, because the American Express green card requires payment in full upon billing, and so doesn't carry any finance rates.	O	2396..2560
Mr. Riese says American Express considers GM and Buick "very sophisticated direct-mail marketers," so "by joining forces with them we have managed to maximize our direct-mail capability."	O	2563..2750
In addition, Buick is a relatively respected nameplate among American Express card holders, says an American Express spokeswoman.	O	2751..2880
When the company asked members in a mailing which cars they would like to get information about for possible future purchases, Buick came in fourth among U.S. cars and in the top 10 of all cars, the spokeswoman says.	O	2881..3097
American Express has more than 24 million card holders in the U.S., and over half have the green card.	B-entrel	3100..3202
GMAC screened the card-member list for holders more than 30 years old with household incomes over $45,000 who hadn't "missed any payments," the Buick spokeswoman says.	I-entrel	3203..3370
Some 3.8 million of the five million who will get letters were preapproved for credit with GMAC.	B-conjunction	3371..3467
These 3.8 million people also are eligible to get one percentage point off GMAC's advertised finance rates, which start at 6.9% for two-year loan contracts.	I-conjunction	3468..3624
A spokesman for Visa International's U.S. subsidiary says his company is using promotions to increase use of its cards, but doesn't have plans for a tie-in similar to the American Express-Buick link.	O	3627..3826
Three divisions at American Express are working with Buick on the promotion: the establishment services division, which is responsible for all merchants and companies that accept the card; the travel division; and the merchandise sales division.	O	3829..4074
The vacation packages include hotel accommodations and, in some cases, tours or tickets to local attractions, but not meals.	B-entrel	4077..4201
Destinations are Chicago; Honolulu; Las Vegas, Nev.; Los Angeles; Miami Beach, Fla.; New Orleans; New York; Orlando, Fla.; San Francisco; and Washington, D.C.	B-entrel	4202..4360
A buyer who chooses to fly to his destination must pay for his own ticket but gets a companion's ticket free if they fly on United Airlines.	I-entrel	4361..4501
In lieu of the vacation, buyers can choose among several prizes, including a grandfather clock or a stereo videocassette recorder.	B-conjunction	4504..4634
Card holders who receive the letter also are eligible for a sweepstakes with Buick cars or a Hawaii vacation as prizes.	B-conjunction	4635..4754
If they test-drive a Buick, they get an American Express calculator.	I-conjunction	4755..4823
This isn't Buick's first travel-related promotion.	B-restatement	4826..4876
A few years ago, the company offered two round-trip tickets on Trans World Airlines to buyers of its Riviera luxury car.	I-restatement	4877..4997
The promotion helped Riviera sales exceed the division's forecast by more than 10%, Buick said at the time.	O	4998..5105
