Part of a Series }	root	9..26
Tom Panelli had a perfectly good reason for not using the $ 300 rowing machine he bought three years ago .	norel	29..133
" I ate a bad tuna sandwich , got food poisoning and had to have a shot in my shoulder , " he says , making it too painful to row .	restatement	134..259
The soreness , he admits , went away about a week after the shot .	norel	262..325
Yet the rowing machine has n't been touched since	concession	326..374
even though he has moved it across the country with him twice .	concession	375..438
A San Francisco lawyer , Mr. Panelli rowed religiously when he first got the machine	cause	439..522
but , he complains , it left grease marks on his carpet , " and it was boring .	comparison	523..598
It 's a horrible machine , actually .	restatement	599..633
I 'm ashamed I own the stupid thing . "	cause	634..670
Mr. Panelli has plenty of company .	norel	673..707
Nearly three-fourths of the people who own home exercise equipment do n't use it as much as they planned , according to The Wall Street Journal 's " American Way of Buying " survey .	restatement	708..884
The Roper Organization , which conducted the survey , said almost half of the exercise equipment owners found it duller than they expected .	cause	885..1022
It is n't just exercise gear that is n't getting a good workout .	norel	1025..1087
The fitness craze itself has gone soft , the survey found .	alternative	1088..1145
Fewer people said they were working up a sweat with such activities as jogging , tennis , swimming and aerobics .	instantiation	1146..1256
Half of those surveyed said they simply walk these days for exercise .	conjunction	1257..1326
That 's good news for marketers of walking shoes .	norel	1329..1377
The survey also detected a bit more interest in golf , a positive sign for country clubs and golf club makers .	norel	1378..1487
The survey 's findings certainly are n't encouraging for marketers of health-club memberships , tennis rackets and home exercise equipment	norel	1490..1625
but people 's good intentions , if not their actions , are keeping sales of some fitness products healthy .	contrast	1626..1730
For instance , sales of treadmills , exercise bikes , stair climbers and the like are expected to rise 8 % to about $ 1.52 billion this year , according to the National Sporting Goods Association , which sees the home market as one of the hottest growth areas for the 1990s .	instantiation	1733..2000
But even that group knows some people do n't use their machines as much as they should .	contrast	2003..2089
" The first excuse is they do n't have enough time , " says research director Thomas Doyle .	cause	2090..2177
" The second is they do n't have enough discipline . "	conjunction	2178..2228
With more than 15 million exercise bikes sold in the past five years , he adds , " a lot of garages , basements and attics must be populated with them . "	entrel	2229..2377
Still , the average price of such bikes rose last year to $ 145 .	concession	2380..2442
Mr. Doyle predicts a trend toward fewer pieces of home exercise equipment being sold at higher prices .	conjunction	2443..2545
Electronic gimmicks are key .	instantiation	2546..2574
Premark International Inc. , for example , peddles the M8.7sp Electronic Cycling Simulator , a $ 2,000 stationary cycle .	instantiation	2575..2691
On a video screen , riders can see 30 different " rides , " including urban , mountain and desert scenes , and check how many calories are burned a minute .	entrel	2692..2841
Nancy Igdaloff , who works in corporate payments at Bank of America in San Francisco , may be a good prospect for such a gizmo .	norel	2844..2969
She 's trying to sell a $ 150 exercise bike she bought about five years ago for her roommate .	cause	2970..3061
But rather than write off home fitness equipment , she traded up	concession	3062..3125
Ms. Igdaloff just paid about $ 900 for a fancier stationary bike , with a timer , dials showing average and maximum speeds and a comfortable seat that feels almost like a chair .	restatement	3126..3301
" I 'm using it a lot , " she says .	entrel	3302..3333
" I spent so much money that if I look at it , and I 'm not on it , I feel guilty . "	conjunction	3334..3413
The poll points up some inconsistencies between what people say and what they do .	norel	3416..3497
A surprising 78 % of people said they exercise regularly , up from 73 % in 1981 .	instantiation	3498..3575
This conjures up images of a nation full of trim , muscular folks , and suggests couch potatoes are out of season .	cause	3576..3688
Of course , that is n't really the case .	concession	3689..3727
The discrepancy may be	entrel	3728..3750
because asking people about their fitness regime is a bit like inquiring about their love life .	cause	3751..3846
They 're bound to exaggerate .	restatement	3847..3875
" People say they swim , and that may mean they 've been to the beach this year , " says Krys Spain , research specialist for the President 's Council on Physical Fitness and Sports .	norel	3878..4053
" It 's hard to know if people are responding truthfully .	cause	4054..4109
People are too embarrassed to say they have n't done anything . "	cause	4110..4172
While she applauds the fact that more Americans are getting up from the television to stroll or garden , she says the percentage of Americans who do " real exercise to build the heart " is only 10 % to 20 % .	norel	4175..4377
So many people fudge on answers about exercise , the president 's council now uses specific criteria to determine what is considered vigorous : It must produce contractions of large muscle groups , must achieve 60 % of maximum aerobic capacity and must be done three or more times a week for a minimum of 20 minutes .	expansion	4378..4689
One of the council 's goals , set in 1980 , was to see more than 60 % of adults under 65 years of age getting vigorous exercise by 1990 .	norel	4692..4824
That target has been revised to 30 % by the year 2000 .	contrast	4825..4878
But even that goal may prove optimistic .	concession	4881..4921
Of 14 activities , the Journal survey found that 12 -- including bicycling , skiing and swimming -- are being done by fewer Americans today than eight years ago .	cause	4922..5081
Time pressures and the ebb of the fitness fad are cited as reasons for the decline .	restatement	5082..5165
Only walking and golf increased in popularity during the 1980s -- and only slightly .	norel	5168..5252
Jeanette Traverso , a California lawyer , gave up running three times a week to play a weekly round of golf , finding it more social and serene .	instantiation	5253..5394
It 's an activity she feels she can do for life , and by pulling a golf cart , she still gets a good workout .	conjunction	5395..5501
" I 'm really wiped out after walking five hours , " she says .	restatement	5502..5560
Most people said they exercise both for health and enjoyment .	norel	5563..5624
" If you sit down all the time , you 'll go stiff , " says Joyce Hagood , a Roxboro , N.C. , homemaker who walks several miles a week .	instantiation	5625..5751
" And it 's relaxing .	conjunction	5752..5771
Sometimes , if you have a headache , you can go out and walk it right off . "	instantiation	5772..5845
Only about a quarter of the respondents said they exercise to lose weight .	norel	5848..5922
Slightly more , like Leslie Sherren , a law librarian in San Francisco who attends dance aerobics five times a week , exercise to relieve stress .	comparison	5923..6065
" Working with lawyers , " she says , " I need it . "	cause	6066..6112
But fully 90 % of those polled felt they did n't need to belong to a health club .	norel	6115..6194
" They 're too crowded , and everybody 's showing off , " says Joel Bryant , a 22-year-old student from Pasadena , Calif .	cause	6195..6308
" The guys are being macho , and the girls are walking around in little things .	instantiation	6309..6386
They 're not there to work out . "	restatement	6387..6418
But at least they show up .	contrast	6421..6447
Nearly half of those who joined health clubs said they did n't use their membership as often as they planned .	contrast	6448..6556
Feeling they should devote more time to their families or their jobs , many yuppies are skipping their once-sacred workout .	alternative	6557..6679
Even so , the Association of Quality Clubs , a health-club trade group in Boston , says membership revenues will rise about 5 % this year from last year 's $ 5 billion .	norel	6682..6844
A spokeswoman adds , however , that the group is considering offering " a behavior-modification course , similar to a smoking-cessation program , to teach people ways to stay with it . "	contrast	6845..7024
There are die-hard bodies , of course .	norel	7027..7064
The proprietor of Sante West , an aerobics studio in San Francisco 's Marina district , which was hit hard by the earthquake , says , " People were going nuts the minute we opened , " three days after the quake .	instantiation	7065..7268
" The emotional aspect is so draining , they needed a good workout . "	cause	7269..7335
Perhaps the most disturbing finding is that the bowling alley may be an endangered American institution .	norel	7338..7442
The survey reported the number of people who said they bowl regularly has fallen to just 8 % from 17 % in 1981 .	cause	7443..7552
The American Bowling Congress claims a higher percentage of the public bowls regularly , but concedes its membership has declined this decade .	restatement	7553..7694
To find out why , the group recently commissioned a study of the past 20 years of bowling-related research .	norel	7697..7803
Three reasons were pinpointed : a preference for watching bowling and other sports on television rather than actually bowling , dowdy bowling centers , and dissatisfaction with bowling itself .	cause	7804..7993
People who start bowling expecting it to be a pleasurable exercise " have been generally disappointed , " the report said .	restatement	7994..8113
But not Richard Cottrell , a San Francisco cab driver who bowls in two weekly leagues .	contrast	8116..8201
He hit the lanes three years ago on the advice of his doctor .	entrel	8202..8263
" It 's good exercise , " he says .	norel	8266..8296
" I ca n't do anything score-wise , but I like meeting the girls . "	conjunction	8297..8360
He says bowling helps him shed pounds , though that effort is sometimes thwarted by the fact that " when I 'm drinking , I bowl better . "	conjunction	8361..8493
His Tuesday night team , the Leftovers , is in first place .	entrel	8494..8551
