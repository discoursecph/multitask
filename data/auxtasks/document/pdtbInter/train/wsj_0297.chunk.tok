Everything looked good as neurosurgeon Walter Levy and colleagues carefully cut away a woman 's spinal tumor at the Cleveland Clinic in 1978 .	O	9..149
Using small electrical shocks applied to her feet , they were able to monitor sensory nerves .	B-cause	152..244
The shocks generated nerve impulses that traveled via spine to brain and showed up clearly on a brain-wave monitor , indicating no damage to the delicate spinal tissue .	I-cause	245..412
Then , says Dr. Levy , " she woke up paralyzed . "	O	415..460
The damage was to her motor nerves , which could n't be monitored along with the sensory nerves , he explains .	O	463..570
The tragedy , he adds , " galvanized me " to look for a way to prevent similar cases .	O	571..652
Dr. Levy 's answer may come with a new kind of magnetic brain probe , a device that he and dozens of U.S. researchers are studying with great hope .	B-restatement	655..800
Besides holding the promise of safer spinal surgery , the probe could improve the diagnosis of brain and nerve disorders such as strokes and multiple sclerosis .	B-conjunction	801..960
Perhaps most exciting , the device is thrusting open a window to the workings of the brain .	I-conjunction	961..1051
The probe , which is painless , non-invasive and apparently harmless , employs strong magnetic fields to induce small whirlwinds of electricity within the brain .	B-cause	1054..1212
If positioned over the brain 's motor-control area , the hand-held electromagnets generate nerve impulses that zip down motor nerves and activate muscles , making , say , a finger twitch .	B-cause	1213..1395
In principle , they will enable doctors to check the body 's motor system the way an electrician tests a home 's electrical circuits by running current through them .	I-cause	1396..1558
" Until now , we 've had no objective way of measuring motor function , " says Keith Chiappa , a neurologist conducting clinical tests with the devices at Boston 's Massachusetts General Hospital .	O	1561..1750
" All we could do was tell a patient , ' squeeze my fingers as hard as you can ' or ' raise your arm . '	O	1751..1849
" Under the best circumstances such tests are subjective ; when a patient is unconscious , they do n't work at all .	O	1850..1962
Magnetic brain tweaking started in the early 1900s , when researchers produced flashes of light in the visual field with magnets .	B-asynchronous	1965..2093
In the 1960s , Mayo Clinic researchers developed magnetic devices to stimulate motor nerves in the hand and other limbs .	I-asynchronous	2094..2213
But for brain tests , the unwieldy machines " would have required patients to stand on their heads , " says Reginald Bickford , a researcher at the University of California at San Diego .	O	2214..2395
The field took off in 1985 after scientists at Britain 's Sheffield University developed a handy , compact magnet for brain stimulation .	O	2398..2532
Since then , at least two commercial versions have been put on the U.S. market , and an estimated 500 have been sold .	B-conjunction	2533..2648
In August , a Chicago conference on such devices attracted more than 100 researchers , who reported studies on everything from brain mapping to physical therapy .	I-conjunction	2649..2808
" We do n't feel we can use { the devices } routinely in surgery yet , but we 're getting close , " says Dr. Levy , who is now with the University of Pittsburgh .	O	2811..2963
A problem , he adds , is that anesthetized brains are more resistant to magnetic stimulation than awake ones .	O	2964..3071
The devices could help indicate when surgery would help , says Charles Tator , a University of Toronto neurosurgeon .	O	3074..3188
For example , paralyzed car-crash victims occasionally have some intact spinal tissues that , if preserved by emergency surgery , enable partial recovery .	B-contrast	3189..3340
But such operations typically are n't performed because there is no sign right after an injury that surgery would be beneficial .	I-contrast	3341..3468
" The cost { of magnetic stimulators } would seem like peanuts if we could retrieve limb function " in such people , Dr. Tator says .	O	3471..3598
Scientists caution there is a chance the magnet technique might spark seizures in epileptics .	B-concession	3601..3694
But no significant problems have been reported among hundreds of people tested with the devices .	I-concession	3695..3791
The main sensation , besides feeling like a puppet jerked with invisible strings , is " like a rap on the head , " says Sam Bridgers , a neurologist who has studied the brain stimulators at Yale University .	O	3794..3994
One apparent side effect is a minor increase in a brain hormone .	B-conjunction	3995..4059
And some doctors who have conducted hours of tests on themselves report temporary headaches .	I-conjunction	4060..4152
At least two companies , Cadwell Laboratories Inc. of Kennewick , Wash. , and Novametrix Medical Systems Inc. of Wallingford , Conn. , now sell versions of the magnetic devices .	B-restatement	4155..4327
The machines , which at $ 12,500 are inexpensive by medical standards , have n't been approved in the U.S. for marketing as brain stimulators but are sold for stimulating nerves in the hand , legs and other non-brain areas .	B-contrast	4328..4546
Researchers can apply for permission to use the probes for brain studies .	I-contrast	4547..4620
At the University of Kentucky , a team led by Dean Currier , a physical therapy researcher , is testing the stimulators in conjunction with electric shocks to induce muscle contractions to help prevent wasting of thigh muscles after knee surgery .	B-conjunction	4623..4866
Similarly , a Purdue University team led by heart researcher W.A. Tacker hopes to develop ways to magnetically induce cardiac muscle contractions .	I-conjunction	4867..5012
The devices might someday serve as temporary pacemakers or restarters for stopped hearts , says Dr. Tacker , whose prototype was dubbed the " Tacker whacker . "	O	5013..5168
The devices ' most remarkable possibilities , though , involve the brain .	B-instantiation	5171..5241
Probing with the stimulators , National Institutes of Health scientists recently showed how the brain reorganizes motor-control resources after an amputation .	B-conjunction	5242..5399
Similar studies are expected to reveal how stroke patients ' brains regroup -- a first step toward finding ways to bolster that process and speed rehabilitation .	B-conjunction	5400..5560
Scientists also are exploring memory and perception with the new machines .	B-instantiation	5563..5637
At the State University of New York at Brooklyn , researchers flash two groups of different letters on a computer screen in front of human guinea pigs .	B-synchrony	5638..5788
Between flashes , certain areas in subjects ' brains are jolted with a magnetic stimulator .	B-entrel	5789..5878
When the jolt is timed just right , the subjects do n't recall seeing the first group of letters .	I-entrel	5879..5974
" Where does that first stimulus go ? " exclaims SUNY neurologist Paul Maccabee .	O	5977..6054
" Trying to answer that is suggesting all kinds of theories , " such as precisely where and how the brain processes incoming signals from the eyes .	B-entrel	6055..6199
He and others say that the machines are weak enough that they do n't jeopardize the memory .	I-entrel	6200..6290
Both the SUNY team and researchers at the National Magnet Laboratory in Cambridge , Mass. , are working with more potent magnetic brain stimulators .	B-entrel	6293..6439
Among other things , the stronger devices may be able to summon forth half-forgotten memories and induce mood changes , neurologists say .	I-entrel	6440..6575
