The oil industry 's middling profits could persist through the rest of the year .	(NS-summary
Major oil companies in the next few days are expected to report much less robust earnings	(NS-elaboration-additional(NS-elaboration-additional(NS-elaboration-additional(NS-elaboration-additional(NS-elaboration-additional(NS-elaboration-object-attribute(NS-comparison
than they did for the third quarter a year ago ,	NS-comparison)
largely reflecting deteriorating chemical prices and gasoline profitability .	NS-elaboration-object-attribute)
The gasoline picture may improve this quarter ,	(NS-consequence(NS-attribution(NN-Contrast
but chemicals are likely to remain weak ,	NN-Contrast)
industry executives and analysts say ,	NS-attribution)
reducing chances	(NS-elaboration-object-attribute
that profits could equal their year-earlier performance .	NS-elaboration-object-attribute)))
The industry is `` seeing a softening somewhat in volume and certainly in price in petrochemicals , ''	(NS-elaboration-additional(NS-elaboration-additional(NS-elaboration-additional(NS-attribution
Glenn Cox , president of Phillips Petroleum Co. , said in an interview .	NS-attribution)
`` That change will obviously impact third and fourth quarter earnings '' for the industry in general ,	(NS-example(NS-attribution
he added .	NS-attribution)
He did n't forecast Phillips 's results .	(NN-List(SN-antithesis
But securities analysts say	(SN-attribution
Phillips will be among the companies hard-hit by weak chemical prices	(NS-consequence
and will probably post a drop in third-quarter earnings .	NS-consequence)))
So , too ,	(NN-Same-Unit(NS-attribution
many analysts predict ,	NS-attribution)
will Exxon Corp. , Chevron Corp. and Amoco Corp .	NN-Same-Unit))))
Typical is what happened to the price of ethylene , a major commodity chemical	(NS-elaboration-general-specific(NS-elaboration-object-attribute
produced in vast amounts by many oil companies .	NS-elaboration-object-attribute)
It has plunged 13 % since July to around 26 cents a pound .	(NS-comparison
A year ago ethylene sold for 33 cents ,	(NS-elaboration-additional
peaking at about 34 cents last December .	NS-elaboration-additional))))
A big reason for the chemical price retreat is overexpansion .	(NS-elaboration-additional
Beginning in mid-1987 ,	(SN-result(NS-consequence(SN-circumstance
prices began accelerating	(NS-explanation-argumentative
as a growing U.S. economy and the weak dollar spurred demand .	NS-explanation-argumentative))
Companies added capacity furiously .	NS-consequence)
Now , greatly increased supplies are on the market ,	(SN-circumstance
while the dollar is stronger ,	(NN-List
and domestic economic growth is slower .	NN-List))))))
Third-quarter profits from gasoline were weaker .	(NS-antithesis(NS-explanation-argumentative(NS-elaboration-additional
`` Refining margins were so good in the third quarter of last year	(NS-attribution(SN-comparison
and generally not very good this year , ''	SN-comparison)
said William Randol , a securities analyst at First Boston Corp .	NS-attribution))
Oil company refineries ran flat out	(NS-elaboration-additional(SN-consequence(NS-purpose
to prepare for a robust holiday driving season in July and August	(NS-elaboration-object-attribute
that did n't materialize .	NS-elaboration-object-attribute))
The excess supply pushed gasoline prices down in that period .	SN-consequence)
In addition , crude oil prices were up some from a year earlier ,	(NS-consequence
further pressuring profitability .	NS-consequence)))
Refiners say	(NS-consequence(SN-attribution
margins picked up in September ,	SN-attribution)
and many industry officials believe	(NS-elaboration-additional(SN-attribution
gasoline profits will rebound this quarter , though still not to the level of 1988 's fourth quarter .	SN-attribution)
During the 1988 second half , many companies posted record gasoline and chemical profits .	NS-elaboration-additional))))
Crude oil production may turn out to be the most surprising element of companies ' earnings this year .	(NS-elaboration-additional
Prices	(SN-interpretation(NN-List(NN-Same-Unit(NS-elaboration-additional
-- averaging roughly $ 2 a barrel higher in the third quarter than a year earlier --	NS-elaboration-additional)
have stayed well above most companies ' expectations .	NN-Same-Unit)
Demand has been much stronger	(NS-consequence(NN-List(NS-comparison
than anticipated ,	NS-comparison)
and it typically accelerates in the fourth quarter .	NN-List)
`` We could see higher oil prices this year , ''	(NS-attribution
said Bryan Jacoboski , an analyst at PaineWebber Inc .	NS-attribution)))
That will translate into sharply higher production profits ,	(NS-comparison
particularly compared with last year	(NS-circumstance
when oil prices steadily fell to below $ 13 a barrel in the fourth quarter .	NS-circumstance)))))
While oil prices have been better	(NS-elaboration-additional(NS-explanation-argumentative(NS-evidence(SN-antithesis(NS-comparison
than expected ,	NS-comparison)
natural gas prices have been worse .	SN-antithesis)
In the third quarter , they averaged about 5 % less	(NS-comparison
than they were in 1988 .	NS-comparison))
The main reason remains weather .	(NS-elaboration-additional
Last summer was notable for a heat wave and drought	(SN-antithesis(NS-elaboration-object-attribute
that caused utilities to burn more natural gas	(NS-purpose
to feed increased electrical demand from air conditioning use .	NS-purpose))
This summer , on the other hand , had milder weather than usual .	SN-antithesis)))
`` We 've been very disappointed in the performance of natural gas prices , ''	(NS-elaboration-additional(NS-attribution
said Mr. Cox , Phillips 's president .	NS-attribution)
`` The lagging gas price is not going to assist fourth quarter performance	(NS-elaboration-additional(NS-comparison
as many had expected . ''	NS-comparison)
Going into the fourth quarter ,	(NS-example(SN-circumstance
natural gas prices are anywhere from 8 % to 17 % lower than a year earlier .	SN-circumstance)
For instance , natural gas	(NN-Same-Unit(NS-elaboration-object-attribute
currently produced along the Gulf Coast	NS-elaboration-object-attribute)
is selling on the spot market for around $ 1.47 a thousand cubic feet , down 13 % from $ 1.69 a thousand cubic feet a year ago .	NN-Same-Unit)))))))
