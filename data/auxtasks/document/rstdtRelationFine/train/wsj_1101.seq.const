Israel has launched a new effort	(interpretation(background(concession(elaboration-object-attribute
to prove	(means(attribution
the Palestine Liberation Organization continues to practice terrorism ,	attribution)
and thus to persuade the U.S. to break off talks with the group .	means))
U.S. officials , however , said	(attribution
they are n't buying the Israeli argument .	attribution))
Israeli counterterrorism officials provided the State Department with a 20-page list of recent terrorist incidents	(Topic-Drift(elaboration-additional(Statement-Response(elaboration-additional(elaboration-object-attribute
they attribute directly to forces	(elaboration-object-attribute
controlled by PLO Chairman Yasser Arafat .	elaboration-object-attribute))
Mr. Arafat publicly renounced terrorism Dec. 15 ,	(means
satisfying the U.S. precondition for a direct `` dialogue '' with the PLO .	means))
A U.S. counterterrorism official said	(elaboration-additional(attribution
experts are studying the Israeli list .	attribution)
`` We have no independent evidence	(elaboration-additional(elaboration-additional(Comment-Topic(attribution(elaboration-object-attribute
linking Fatah to any acts of terrorism since Dec. 15 , 1988 , ''	elaboration-object-attribute)
he said ,	attribution)
referring to the specific PLO group	(elaboration-object-attribute
that Mr. Arafat heads .	elaboration-object-attribute))
`` So far , this list does n't change our view .	elaboration-additional)
Israel wants to end the dialogue ,	(concession
but our analysts take a different view than theirs . ''	concession))))
Israeli Prime Minister Yitzhak Shamir 's top adviser on counterterrorism , Yigal Carmon , was here Monday	(rhetorical-question(List(elaboration-additional(purpose
to present the report to members of Congress , reporters and others .	purpose)
Mr. Carmon said	(attribution
he also presented the list last week to William Brown , U.S. Ambassador to Israel .	attribution))
Separately , the New York Times reported	(attribution
that the Israeli government had provided its correspondent in Jerusalem with different documents	(elaboration-object-attribute
that	(elaboration-additional(attribution(Same-Unit(attribution
Israel said	attribution)
prove	Same-Unit)
the PLO has been conducting terrorism from the occupied Arab territories .	attribution)
The State Department said	(attribution
it has n't yet seen copies of those papers .	attribution)))))
`` If the dialogue was based on the assumption	(attribution(condition(concession(elaboration-object-attribute
that Arafat or the PLO would stop terrorism ,	elaboration-object-attribute)
and we have evidence of continued terrorism ,	concession)
what would be the logical conclusion ? ''	condition)
Mr. Carmon asked .	attribution)))
Israel has long claimed	(background(attribution
Mr. Arafat never meant to renounce terrorism ,	(reason
particularly because he and his lieutenants reserved the right	(elaboration-object-attribute
to press `` armed struggle '' against the Jewish state .	elaboration-object-attribute)))
Now , Jerusalem says	(elaboration-additional(elaboration-additional(elaboration-additional(attribution
it is backing up its contention with detailed accounts of alleged terrorist acts and plans	(elaboration-object-attribute
linked to Mr. Arafat .	elaboration-object-attribute))
It blames most of these on Fatah .	elaboration-additional)
The new accusations come at a delicate time in U.S. efforts	(elaboration-object-attribute
to bring about talks between Israel and Palestinian representatives .	elaboration-object-attribute))
The State Department said	(attribution
it had received a new letter on the subject from Israeli Foreign Minister Moshe Arens ,	(elaboration-additional
restating Israel 's previous objection	(elaboration-object-attribute
to negotiating with any Palestinian	(elaboration-object-attribute
tied to the PLO .	elaboration-object-attribute))))))))
Deciding what constitutes `` terrorism '' can be a legalistic exercise .	(definition
The U.S. defines it as `` premediated , politically motivated violence	(circumstance(elaboration-object-attribute
perpetrated against noncombatant targets by subnational groups or clandestine state agents . ''	elaboration-object-attribute)
To meet the U.S. criteria ,	(elaboration-additional(concession(purpose
Israel contended	(elaboration-additional(attribution
it only listed incidents	(elaboration-object-attribute
that involved civilians	(List
and occurred inside its pre-1967 borders .	List)))
At the heart of Israel 's report is a list of a dozen incidents	(example(elaboration-object-attribute
Jerusalem attributes to Fatah ,	elaboration-object-attribute)
including the use of bombs and Molotov cocktails .	example)))
But U.S. officials say	(elaboration-additional(attribution
they are n't satisfied	(result(attribution
these incidents constitute terrorism	attribution)
because they may be offshoots of the intifadah , the Palestinian rebellion in the occupied territories ,	(elaboration-additional
which the U.S. does n't classify as terrorism .	elaboration-additional)))
In addition , the officials say	(List(attribution
Israel has n't presented convincing evidence	(elaboration-object-attribute
these acts were ordered by Fatah or by any group	(elaboration-object-attribute
Mr. Arafat controls .	elaboration-object-attribute)))
U.S. terrorism experts also say	(elaboration-general-specific(attribution
they are highly uncertain about the veracity of the separate documents	(elaboration-object-attribute
leaked to the New York Times .	elaboration-object-attribute))
The papers ,	(Same-Unit(elaboration-additional
which	(Same-Unit(attribution
Israel says	attribution)
were discovered in Israeli-occupied Gaza ,	Same-Unit))
refer to terrorist acts	(elaboration-object-attribute
to be carried out in the name of a group	(elaboration-object-attribute
called `` the Revolutionary Eagles . ''	elaboration-object-attribute)))))))
Some supporters of Israel say	(attribution
U.S. policy on Palestinian terrorism is colored by an intense desire	(concession(elaboration-object-attribute
to maintain the dialogue with the PLO .	elaboration-object-attribute)
But State Department officials accuse Israel of leaking questionable claims	(purpose
to embarrass the U.S .	purpose)))))))
