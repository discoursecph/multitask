At a private dinner Thursday , Drexel Burnham Lambert Inc. chief executive Frederick Joseph delivered a sobering message about the junk bond market to officials of Prudential Insurance Co. of America .	O	9..208
Mr. Joseph conceded the junk market was in disarray , according to people familiar with the discussion .	O	211..313
He said Drexel -- the leading underwriter of high-risk junk bonds -- could no longer afford to sell any junk offerings if they might later become troubled because Drexel risked losing its highly lucrative junk franchise .	O	314..534
The dinner was a stark confirmation that 1989 is the worst year ever for the $ 200 billion junk market .	B-expansion	537..639
And investors and traders alike say the current turmoil could take years to resolve .	I-expansion	640..724
Amid the market disorder , even Drexel , which has the widest and most loyal following of junk bond investors , is pulling in its horns .	B-contingency	727..860
Although the big investment bank still dominates the junk market , Drexel has been unable to stem the fallout from growing junk bond defaults , withdrawn new offerings , redemptions by shareholders in junk bond mutual funds and an exodus of once-devoted investors .	I-contingency	861..1122
For many money managers , the past four months have been humiliating .	O	1125..1193
" This is the worst shakeout ever in the junk market , and it could take years before it 's over , " says Mark Bachmann , a senior vice president at Standard & Poor 's Corp. , a credit rating company .	O	1194..1386
In the third quarter , for example , junk bonds -- those with less than an investment-grade rating -- showed negative returns , the only major sector of the bond market to do so .	B-expansion	1389..1564
Since the end of last year , junk bonds have been outperformed by all categories of investment-grade bonds , including ultra-safe Treasury securities .	I-expansion	1565..1713
The junk market , which mushroomed to $ 200 billion from less than $ 2 billion at the start of the decade , has been declining for months as issuers have stumbled under the weight of hefty interest payments .	B-temporal	1716..1919
The fragile market received its biggest jolt last month from Campeau Corp. , which created its U.S. retailing empire with more than $ 3 billion in junk financing .	B-temporal	1920..2080
Campeau developed a cash squeeze that caused it to be tardy on some interest payments and to put its prestigious Bloomingdale 's department store chain up for sale .	B-temporal	2081..2244
At that point , the junk market went into a tailspin as buyers disappeared and investors tried to sell .	I-temporal	2245..2347
In an interview , Mr. Joseph says his dinner discussion with the Prudential executives acknowledged problems for junk .	O	2350..2467
" What I thought I was saying is that the market is troubled but still viable and , appropriately enough , quite quality-conscious , which is not at all bad , " he says .	O	2468..2631
" Nobody 's been perfect in their credit judgments the past couple years , and we 're going to make sure our default rates are going to be in the acceptable parameters of the market . "	O	2632..2811
What has jolted many junk buyers is the sudden realization that junk bonds can not necessarily be bought and sold with the ease of common stocks and many investment-grade bonds .	B-contingency	2814..2990
Unlike the New York Stock Exchange , where buyers and sellers are quickly matched , the junk market , where risky corporate loans are traded , is sometimes closed for repairs .	I-contingency	2991..3162
At closely held Deltec Securities Corp. , junk bond money managers Amy K. Minella and Hannah H. Strasser say the problems of the junk market go deeper than a temporary malaise .	O	3165..3340
In recent months , they say , there has been heavy selling of junk bonds by some of the market 's traditional investors , while new buyers have n't materialized to replace them .	O	3341..3513
Wall Street securities firms , " the primary source of liquidity for the high yield market , " have been net sellers of junk bonds because of trading losses , Deltec said in a recent , grim report to customers .	O	3516..3720
Mutual funds have also been net sellers of junk bonds as junk 's relatively poor performance and negative press coverage have produced " above-normal " redemptions by shareholders , Deltec said .	O	3721..3911
Investors , trying to raise cash , have sold " large liquid issues " such as RJR Holdings Corp. and Kroger Co. ; declines in these benchmark issues have contributed to the market 's distress .	O	3914..4099
And , Deltec said , buying has been severely reduced because savings and loans have been restricted in their junk purchases by recently passed congressional legislation .	O	4102..4269
" In fact , savings and loans were sellers of high yield holdings throughout the quarter , " Deltec said .	O	4270..4371
Ms. Minella and Ms. Strasser say they are managing their junk portfolios defensively , building cash and selectively upgrading the overall quality .	O	4374..4520
Meanwhile , Prudential , the nation 's largest insurer and the biggest investor in junk bonds , has seen the value of its junk bond portfolio drop to $ 6.5 billion from $ 7 billion since August because of falling junk prices .	O	4523..4742
" We certainly do have a lack of liquidity here , and it 's something to be concerned about , " says James A. Gregoire , a managing director .	O	4743..4878
" I have no reason to think things will get worse , but this market has a knack for surprising us .	O	4879..4975
This market teaches us to be humble . "	O	4976..5013
The junk market 's " yield hogs are learning a real painful lesson , " he says .	O	5016..5091
Although the majority of junk bonds outstanding show no signs of default , the market has downgraded many junk issues as if they were in trouble , says Stuart Reese , manager of Aetna Life & Casualty Insurance Co. 's $ 17 billion investment-grade public bond portfolio .	O	5094..5359
" But we think the risks are there for things getting a lot worse .	O	5360..5425
And the risks are n't appropriate for us , " he says .	O	5426..5476
The big insurer , unlike Prudential , owns only about $ 150 million of publicly sold junk bonds .	O	5477..5570
The string of big junk bond defaults , which have been a major cause of the market 's problems this year , probably will persist , some analysts say .	B-expansion	5573..5718
" If anything , we 're going to see defaults increase because credit ratings have declined , " says Paul Asquith , associate professor at the Massachusetts Institute of Technology 's Sloan School of Management .	I-expansion	5719..5922
Mr. Asquith , whose study on junk bond defaults caused a furor on Wall Street when it was disclosed last April , says this year 's junk bond defaults already show a high correlation with his own findings .	O	5925..6126
His study showed that junk bonds over time had a cumulative default rate of 34 % .	O	6127..6207
One indication of a growing number of junk defaults , Mr. Asquith says , is that about half of the $ 3 billion of corporate bonds outstanding that have been lowered to a default rating by S&P this year are junk bonds sold during the market 's big issue years of 1984 through 1986 .	O	6210..6486
These bonds , now rated single-D , include junk offerings by AP Industries , Columbia Savings ( Colorado ) , First Texas Savings Association , Gilbraltar Financial Corp. , Integrated Resources Inc. , Metropolitan Broadcasting Corp. , Resorts International Inc. , Southmark Corp. and Vyquest Inc .	O	6489..6773
" Obviously , we got a lot more smoke than fire from the people who told us the market was n't so risky , " says Bradford Cornell , professor of finance at University of California 's Anderson Graduate School of Management in Los Angeles .	O	6776..7007
Mr. Cornell has just completed a study that finds that the risks and returns of junk bonds are less than on common stocks but more than on investment-grade bonds .	O	7008..7170
Mr. Cornell says : " The junk market is no bonanza as Drexel claimed , but it also is n't a disaster as the doomsayers say . "	O	7171..7291
Despite the junk market 's problems , Drexel continues to enjoy a loyalty among junk bond investors that its Wall Street rivals have n't found .	B-expansion	7294..7434
During the past three weeks , for example , Drexel has sold $ 1.3 billion of new junk bonds for Turner Broadcasting Co. , Uniroyal Chemical , Continental Air and Duff & Phelps .	I-expansion	7435..7606
Still , the list of troubled Drexel bond offerings dwarfs that of any firm on Wall Street , as does its successful offerings .	O	7609..7732
Troubled Drexel-underwritten issues include Resorts International , Braniff , Integrated Resources , SCI TV , Gillette Holdings , Western Electric and Southmark .	O	7733..7889
" Quality junk bonds will continue to trade well , " says Michael Holland , chairman of Salomon Brothers Asset Management Inc .	O	7892..8014
" But the deals that never should have been brought have now become nuclear waste .	O	8015..8096
