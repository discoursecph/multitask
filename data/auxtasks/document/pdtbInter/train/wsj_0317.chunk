Georgia-Pacific Corp. 's unsolicited $3.19 billion bid for Great Northern Nekoosa Corp. was hailed by Wall Street despite a cool reception by the target company.	O	9..170
William R. Laidig, Nekoosa's chairman, chief executive officer and president, characterized the $58-a-share bid as "uninvited" and said Nekoosa's board would consider the offer "in due course."	O	173..366
T. Marshall Hahn Jr., Georgia-Pacific's chairman and chief executive, said in an interview that all terms of the offer are negotiable.	O	369..503
He added that he had spoken with Mr. Laidig, whom he referred to as a friend, by telephone Monday evening.	B-entrel	504..610
"I'm hopeful that we'll have further discussions," Mr. Hahn said.	I-entrel	611..676
On Wall Street, takeover stock traders bid Nekoosa's stock well above the Georgia-Pacific bid, assuming that Nekoosa's will either be sold to a rival bidder or to Georgia-Pacific at a higher price -- as much as $75 a share, according to some estimates.	O	679..931
Yesterday, Nekoosa common closed in composite New York Stock Exchange trading at $62.875, up $20.125, on volume of almost 6.3 million shares.	B-conjunction	934..1075
Georgia-Pacific closed down $2.50, at $50.875 in Big Board trading.	I-conjunction	1076..1143
Takeover stock traders noted that with the junk-bond market in disarray, Georgia-Pacific's bid is an indication of where the takeover game is headed: namely, industrial companies can continue bidding for one another, but financial buyers such as leveraged buy-out firms will be at a disadvantage in obtaining financing.	O	1146..1465
"The way the world is shaping up, the strategic buyer is going to be the rule and the financial buyer is going to be the exception," said one trader.	O	1468..1617
For the paper industry specifically, most analysts said the deal will spur a wave of paper-company takeovers, possibly involving such companies as Union Camp Corp., Federal Paperboard Co. and Mead Corp.	O	1620..1822
The analysts argued that Georgia-Pacific's offer, the first hostile bid ever among major players in the paper industry, ends the unwritten taboo on hostile bids, and will push managements to look closely at the industry's several attractive takeover candidates.	O	1823..2084
Consolidation has been long overdue.	B-contrast	2087..2124
It was just the culture of the industry that kept it from happening.	I-contrast	2125..2193
The Georgia-Pacific offer has definitely changed the landscape," said Gary Palmero of Oppenheimer & Co.	O	2194..2297
Added Mark Rogers of Prudential-Bache Securities Inc.: "It's much easier to be second."	O	2298..2385
A Georgia-Pacific acquisition of Nekoosa would create the largest U.S. forest-products company.	B-cause	2388..2483
Based on 1988 sales, Georgia-Pacific ranked third at $9.51 billion, behind Weyerhaeuser Co. at $10 billion and International Paper Co. at $9.53 billion.	B-conjunction	2484..2636
Based on 1988 sales, Georgia-Pacific ranked third at $9.51 billion, behind Weyerhaeuser Co. at $10 billion and International Paper Co. at $9.53 billion.Nekoosa ranked 11th with sales of $3.59 billion.	B-cause	2637..2685
The combined company would have had 1988 sales of $13.1 billion.	B-contrast	2686..2750
But such a combination also presents great risks.	B-restatement	2753..2802
At a time when most analysts and industry consultants say pulp and paper prices are heading for a dive, adding capacity and debt could squeeze Georgia-Pacific if the industry declines more than the company expects.	B-conjunction	2803..3017
Moreover, any unexpected strengthening of the dollar would hurt Georgia-Pacific because two of Nekoosa's major product lines -- containerboard, which is used to make shipping boxes, and market pulp -- are exported in large quantities.	I-conjunction	3018..3252
"Nobody knows how deep the cycle is going to be," said Rod Young, vice president of Resource Information Systems Inc., a Bedford, Mass., economic-forecasting firm.	O	3255..3418
"Depending on how far down you go, it may be difficult to pay off that debt."	O	3419..3496
One person familiar with Georgia-Pacific said the acquisition would more than double the company's debt of almost $3 billion.	O	3499..3624
It also could be a drag on Georgia-Pacific earnings because the roughly $1.5 billion in goodwill -- the amount by which the bid exceeds Nekoosa's book value of $1.5 billion -- will have to be subtracted from earnings over a period of decades.	O	3625..3867
Georgia-Pacific's Mr. Hahn said that a combined operation would allow savings in many ways.	O	3870..3961
The two companies each produce market pulp, containerboard and white paper.	O	3962..4037
That means goods could be manufactured closer to customers, saving shipping costs, he said.	O	4038..4129
Moreover, production runs would be longer, cutting inefficiencies from adjusting machinery between production cycles.	B-conjunction	4132..4249
And Georgia-Pacific could save money in selling pulp, because the company uses its own sales organization while Nekoosa employs higher-cost agents.	I-conjunction	4250..4397
Mr. Hahn said Georgia-Pacific has accounted in its strategy for a "significant downturn" in the pulp and paper industry, an event that he said would temporarily dilute earnings.	B-contrast	4400..4577
But he said that even under those conditions, the company still would realize a savings of tens of millions of dollars in the first year following a merger.	I-contrast	4578..4734
"The fit is so good, we see this as a time of opportunity," he said.	O	4735..4803
Georgia-Pacific, which has suspended its stock-repurchase program, would finance the acquisition with all bank debt, provided by banks led by BankAmerica Corp.	B-entrel	4806..4965
Georgia-Pacific owns 349,900 Nekoosa shares and would need federal antitrust clearance to buy more than $15 million worth.	B-conjunction	4966..5088
U.S. clearance also is needed for the proposed acquisition.	I-conjunction	5089..5148
For Nekoosa, defense options may be undercut somewhat by the precarious state of the junk-bond market, which limits how much value the target could reach in a debt-financed recapitalization.	B-entrel	5151..5341
The company's chairman, Mr. Laidig, and a group of advisers met at the offices of Wachtel Lipton Rosen & Katz, a law firm specializing in takeover defense.	B-conjunction	5342..5497
Nekoosa also is being advised by Goldman, Sachs & Co.	I-conjunction	5498..5551
Georgia-Pacific's advisers are Wasserstein, Perella & Co., which stands to receive a $15 million fee if the takeover succeeds, and the law firm of Shearman & Sterling.	O	5554..5721
People familiar with Nekoosa said its board isn't likely to meet before the week after next to respond to the bid.	O	5724..5838
The board has 10 business days to respond.	O	5839..5881
In addition to the usual array of defenses, including a so-called poison pill and a staggered board, Nekoosa has another takeover defense: a Maine state law barring hostile bidders from merging acquired businesses for five years.	B-cause	5884..6113
Nekoosa is incorporated in Maine.	I-cause	6114..6147
Georgia-Pacific has filed a lawsuit in federal court in Maine challenging the poison pill and the Maine merger law.	O	6150..6265
Nekoosa's poison pill allows shareholders to vote to rescind it, but Georgia-Pacific isn't likely to pursue such a course immediately because that would take 90 to 120 days, and wouldn't affect the provisions of the Maine law.	O	6268..6494
Among companies mentioned by analysts as possible counterbidders for Nekoosa are International Paper, Weyerhaeuser, Canadian Pacific Ltd. and MacMillan Bloedel Ltd.	O	6497..6661
"I'm sure everybody else is putting pencil to paper," said Kathryn McAuley, an analyst with First Manhattan Co.	O	6662..6773
International Paper and Weyerhaeuser declined to comment.	B-conjunction	6776..6833
Canadian Pacific couldn't be reached for comment, and MacMillan Bloedel said it hasn't any plans to make a bid for Nekoosa.	I-conjunction	6834..6957
Investors were quick to spot other potential takeover candidates, all of which have strong cash flows and low-cost operations.	B-instantiation	6960..7086
Among paper company stocks that rallied on the Big Board because of the offer were Union Camp, up $2.75 to $37.75, Federal Paperboard, up $1.75 to $27.875, Mead, up $2.375 to $38.75, and Temple Inland Inc., up $3.75 to $62.25.	B-conjunction	7087..7313
In over-the-counter national trading, Bowater Inc. jumped $1.50 to $27.50.	I-conjunction	7314..7388
Some analysts argued that there won't be a flurry of takeovers because the industry's continuing capacity-expansion program is eating up available cash.	O	7391..7543
Moreover, some analysts said they expect a foreign paper company with deeper pockets than Georgia-Pacific to end up acquiring Nekoosa, signaling to the rest of the industry that hostile bids are unproductive.	O	7544..7752
This is a one-time event," said Lawrence Ross of PaineWebber Inc., referring to the Georgia-Pacific bid.	B-contrast	7755..7860
But many analysts believe that, given the attractiveness of paper companies' cash flows, as well as the frantic consolidation of the paper industry in Europe, there will be at least a few more big hostile bids for U.S. companies within the next several months.	B-entrel	7863..8123
The buyers, these analysts added, could be either foreign or other U.S.concerns.	I-entrel	8124..8204
"The Georgia-Pacific bid may open the door to a new era of consolidation" in the paper industry, said Mark Devario of Shearson Lehman Hutton Inc.	O	8207..8352
"I don't think anyone is now immune from takeover," said Robert Schneider of Duff & Phelps Inc., Chicago.	O	8355..8460
He added: "Every paper company management has to be saying to itself, `Before someone comes after me, I'm going to go after somebody. '"	O	8461..8597
Prudential-Bache's Mr. Rodgers said he doesn't see the industry's capacity-expansion program hindering takeover activity.	O	8600..8721
Several projects, he said, are still on the drawing board.	O	8722..8780
Moreover, "it's a lot cheaper and quicker to buy a plant than to build one.	B-conjunction	8781..8857
Indeed, a number of analysts said that Japanese paper companies are hungry to acquire additional manufacturing capacity anywhere in the world.	B-conjunction	8860..9002
Some predicted that Nekoosa will end up being owned by a Japanese company.	I-conjunction	9003..9077
Meanwhile, Shearson Lehman's Mr. Devario said that, to stay competitive, the U.S. paper industry needs to catch up with the European industry.	B-entrel	9080..9222
Since the most-recent wave of friendly takeovers was completed in the U.S. in 1986, there have been more than 100 mergers and acquisitions within the European paper industry, he said.	I-entrel	9223..9406
