In the long, frightening night after Tuesday's devastating earthquake, Bay Area residents searched for comfort and solace wherever they could.	B-entrel	9..151
Some found it on the screen of a personal computer.	I-entrel	152..203
Hundreds of Californians made their way to their computers after the quake, and checked in with each other on electronic bulletin boards, which link computers CB-radio-style, via phone lines.	O	206..397
Some of the most vivid bulletins came over The Well, a Sausalito, Calif., board that is one of the liveliest outposts of the electronic underground.	B-entrel	400..548
About two-thirds of the Well's 3,000 subscribers live in the Bay Area.	I-entrel	549..619
The quake knocked The Well out for six hours, but when it came back up, it teemed with emotional first-hand reports.	O	622..738
Following are excerpts from the electronic traffic that night.	B-entrel	741..803
The time is Pacific Daylight Time, and the initials or nicknames are those subscribers use to identify themselves.	I-entrel	804..918
11:54 p.m.	O	921..931
JCKC:	O	932..937
Wow!	O	938..942
Wow! I was in the avenues, on the third floor of an old building, and except for my heart (Beat, BEAT!) I'm OK.	B-entrel	943..1049
Got back to Bolinas, and everything had fallen: broken poster frames with glass on the floor, file cabinets open or dumped onto the floor.	I-entrel	1050..1188
11:59 p.m.	O	1191..1201
JKD:	O	1202..1206
I was in my favorite watering hole, waiting for the game to start.	O	1207..1273
I felt the temblor begin and glanced at the table next to mine, smiled that guilty smile and we both mouthed the words, "Earth-quake!" together.	B-entrel	1274..1418
That's usually how long it takes for the temblors to pass.	B-contrast	1419..1477
This time, it just got stronger and then the building started shaking violently up and down as though it were a child's toy block that was being tossed.	I-contrast	1478..1630
12:06 a.m.	O	1633..1643
HRH:	O	1644..1648
I was in the Berkeley Main library when it hit.	B-entrel	1649..1696
Endless seconds wondering if those huge windows would buckle and shower us with glass.	B-contrast	1697..1783
Only a few books fell in the reading room.	B-asynchronous	1784..1826
Then the auto paint shop fire sent an evil-looking cloud of black smoke into the air.	I-asynchronous	1827..1912
12:07 a.m.	O	1915..1925
ONEZIE:	O	1926..1933
My younger daughter and I are fine.	O	1934..1969
This building shook like hell and it kept getting stronger.	O	1970..2029
Except for the gas tank at Hustead's Towing Service exploding and burning in downtown Berkeley, things here are quite peaceful.	O	2030..2157
A lot of car alarms went off.	B-entrel	2158..2187
The cats are fine, although nervous.	I-entrel	2188..2224
12:15 a.m.	O	2227..2237
DHAWK:	O	2238..2244
Huge fire from broken gas main in the Marina in SF.	B-entrel	2245..2296
Areas that are made of `fill' liquefy.	B-instantiation	2297..2335
A woman in a three-story apartment was able to walk out the window of the third floor onto street level after the quake.	B-cause	2336..2456
The house just settled right down into the ground.	I-cause	2457..2507
12:38 a.m.	O	2510..2520
DAYAC:	O	2521..2527
I was driving my truck, stopped at a red light at the corner of Shattuck and Alcatraz at the Oakland-Berkeley border when it hit.	B-entrel	2528..2657
Worst part was watching power lines waving above my head and no way to drive away.	I-entrel	2658..2740
12:48 a.m.	O	2743..2753
LMEYER:	O	2754..2761
Was 300 ft. out on a pier in San Rafael.	B-entrel	2762..2802
It flopped all around, real dramatic!	B-asynchronous	2803..2840
It flopped all around, real dramatic! Many hairline cracks in the concrete slabs afterwards.	B-entrel	2841..2895
Ruined the damn fishing!	I-entrel	2896..2920
1:00 a.m.	O	2923..2932
HEYNOW:	O	2933..2940
I rode it out on the second floor of Leo's at 55th and Telegraph in Oakland.	B-synchrony	2941..3017
I heard parts of the building above my head cracking.	B-cause	3018..3071
I actually thought that I might die.	B-entrel	3072..3108
I couldn't decide if I should come home to Marin, because my house is on stilts.	B-asynchronous	3109..3189
I decided to brave the storm.	B-entrel	3190..3219
There was a horrible smell of gas as I passed the Chevron refinery before crossing the Richmond-San Rafael Bridge.	B-conjunction	3220..3334
I could also see the clouds across the bay from the horrible fire in the Marina District of San Francisco.	I-conjunction	3335..3441
I have felt many aftershocks.	B-cause	3444..3473
My back is still in knots and my hands are still shaking.	B-conjunction	3474..3531
I think a few of the aftershocks might just be my body shaking.	I-conjunction	3532..3595
1:11 a.m.	O	3598..3607
GR8FLRED:	O	3608..3617
I could see the flames from San Francisco from my house across the bay.	B-entrel	3618..3689
It's hard to believe this really is happening.	I-entrel	3690..3736
1:11 a.m.	O	3739..3748
RD:	O	3749..3752
Building on the corner severely damaged, so an old lady and her very old mother are in the guest room.	B-entrel	3753..3855
Books and software everywhere.	B-conjunction	3856..3886
This being typed in a standing position.	I-conjunction	3887..3927
1:20 a.m.	O	3930..3939
DGAULT:	O	3940..3947
Bolinas -- astride the San Andreas Fault.	B-entrel	3948..3989
Didn't feel a thing, but noticed some strange bird behavior.	B-restatement	3990..4050
Duck swarms.	I-restatement	4051..4063
3:25 a.m.	O	4066..4075
SAMURAI:	O	4076..4084
I just felt another aftershock a few seconds ago.	B-cause	4085..4134
I'm just numb.	I-cause	4135..4149
3:25 a.m.	O	4152..4161
MACPOST:	O	4162..4170
Downtown Bolinas seems to be the part of town that's worst off.	B-instantiation	4171..4234
No power, minimal phones, and a mess of mayonnaise, wine, and everything else all over the floors of the big old general store and the People's Co-op.	B-entrel	4235..4385
The quivers move through my house every few minutes at unpredictable intervals, and the mouse that's been living in my kitchen has taken refuge under my desk.	B-entrel	4386..4544
It runs out frantically now and then, and is clearly pretty distressed.	I-entrel	4545..4616
I was in Stinson Beach when the quake rolled through town.	B-synchrony	4619..4677
At first, we were unfazed.	B-asynchronous	4678..4704
Then as things got rougher, we ran for the door and spent the next few minutes outside watching the brick sidewalk under our feet oozing up and down, and the flowers waving in an eerie rhythm.	B-conjunction	4705..4897
Amazing what it does to one's heart rate and one's short-term memory.	B-entrel	4898..4967
Everyone looked calm, but there was this surreal low level of confusion as the aftershocks continued.	I-entrel	4968..5069
4:02 a.m.	O	5072..5081
SHIBUMI:	O	5082..5090
Power is back on, and UCSF {medical center} seems to have quieted down for the night (they were doing triage out in the parking lot from the sound and lights of it).	O	5091..5256
A friend of mine was in an underground computer center in downtown SF when the quake hit.	B-entrel	5257..5346
He said that one of the computers took a three-foot trip sliding across the floor.	I-entrel	5347..5429
Today should be interesting as people realize how hard life is going to be here for a while.	O	5432..5524
4:30 a.m.	O	5527..5536
KIM:	O	5537..5541
I got home, let the dogs into the house and noticed some sounds above my head, as if someone were walking on the roof, or upstairs.	B-asynchronous	5542..5673
Then I noticed the car was bouncing up and down as if someone were jumping on it.	I-asynchronous	5674..5755
I realized what was happening and screamed into the house for the dogs.	B-entrel	5756..5827
Cupboard doors were flying, the trash can in the kitchen walked a few feet, the dogs came running, and I scooted them into the dog run and stood in the doorway myself, watching the outside trash cans dance across the concrete.	I-entrel	5828..6054
When I realized it was over, I went and stood out in front of the house, waiting and praying for Merrill to come home, shivering as if it were 20 below zero until he got there.	B-conjunction	6057..6233
Never in my life have I been so frightened.	B-asynchronous	6234..6277
When I saw the pictures of 880 and the Bay Bridge, I began to cry.	I-asynchronous	6278..6344
5:09 a.m.	O	6347..6356
JROE:	O	6357..6362
The Sunset {District} was more or less like a pajama party all evening, lots of people & dogs walking around, drinking beer.	O	6363..6487
6:50 a.m.	O	6490..6499
CAROLG:	O	6500..6507
I was just sitting down to meet with some new therapy clients, a couple, and the building started shaking like crazy.	O	6508..6625
It's a flimsy structure, built up on supports, and it was really rocking around.	B-entrel	6626..6706
The three of us stopped breathing for a moment, and then when it kept on coming we lunged for the doorway.	I-entrel	6707..6813
Needless to say, it was an interesting first session!	O	6814..6867
7:13 a.m.	O	6870..6879
CALLIOPE:	O	6880..6889
Albany escaped embarrassingly unscathed.	B-entrel	6890..6930
Biggest trouble was scared family who couldn't get a phone line through, and spent a really horrible hour not knowing.	I-entrel	6931..7049
8:01 a.m.	O	7052..7061
HLR:	O	7062..7066
Judy and I were in our back yard when the lawn started rolling like ocean waves.	O	7067..7147
We ran into the house to get Mame, but the next tremor threw me in the air and bounced me as I tried to get to my feet.	O	7148..7267
We are all fine here, although Mame was extremely freaked.	O	7268..7326
Kitchen full of broken crystal.	B-entrel	7327..7358
Books and tapes all over my room.	I-entrel	7359..7392
Not one thing in the house is where it is supposed to be, but the structure is fine.	O	7393..7477
While I was standing on the lawn with Mame, waiting for another tremor, I noticed that all the earthworms were emerging from the ground and slithering across the lawn!	O	7478..7645
9:31 a.m.	O	7648..7657
GR8FLRED:	O	7658..7667
It's amazing how one second can so completely change your life.	O	7668..7731
9:38 a.m.	O	7734..7743
FIG:	O	7744..7748
I guess we're all living very tentatively here, waiting for the expected but dreaded aftershock.	B-restatement	7749..7845
It's hard to accept that it's over and only took 15 seconds.	B-entrel	7846..7906
I wonder when we'll be able to relax.	I-entrel	7907..7944
9:53 a.m.	O	7947..7956
PANDA:	O	7957..7963
Flesh goes to total alert for flight or fight.	B-conjunction	7964..8010
Nausea seems a commonplace symptom.	I-conjunction	8011..8046
Berkeley very quiet right now.	O	8047..8077
I walked along Shattuck between Delaware and Cedar at a few minutes before eight this morning.	B-entrel	8078..8172
Next to Chez Panisse a homeless couple, bundled into a blue sleeping bag, sat up, said, "Good morning" and then the woman smiled, said, "Isn't it great just to be alive?"	I-entrel	8173..8343
I agreed.	B-restatement	8344..8353
It is.	B-restatement	8354..8360
Great.	I-restatement	8361..8367
