Toni Johnson pulls a tape measure across the front of what was once a stately Victorian home .	(NN-Topic-Change(NS-Elaboration(NS-Elaboration(NS-Elaboration(NS-Elaboration(NS-Elaboration
A deep trench now runs along its north wall ,	(NN-Joint(NS-Elaboration
exposed	(NS-Cause
when the house lurched two feet off its foundation during last week 's earthquake .	NS-Cause))
A side porch was ripped away .	(NN-Joint
The chimney is a pile of bricks on the front lawn .	(NN-Joint
The remainder of the house leans precariously against a sturdy oak tree .	NN-Joint))))
The petite , 29-year-old Ms. Johnson ,	(NS-Elaboration(NN-Same-unit(NS-Elaboration
dressed in jeans and a sweatshirt	(NS-Background
as she slogs through the steady afternoon rain ,	NS-Background))
is a claims adjuster with Aetna Life & amp ; Casualty .	NN-Same-unit)
She has been on the move almost incessantly since last Thursday ,	(NS-Background
when an army of adjusters ,	(NS-Elaboration(NN-Same-unit(NS-Elaboration
employed by major insurers ,	NS-Elaboration)
invaded the San Francisco area	(NS-Enablement
to help policyholders sift through the rubble and restore some order to their lives .	NS-Enablement))
Equipped with cellular telephones , laptop computers , calculators and a pack of blank checks ,	(NS-Elaboration(NS-Elaboration(SN-Background
they parcel out money	(NS-Enablement
so their clients can find temporary living quarters ,	(NN-Joint
buy food ,	(NN-Joint
replace lost clothing ,	(NN-Joint
repair broken water heaters ,	(NN-Joint
and replaster walls .	NN-Joint))))))
Some of the funds will used	(NS-Enablement
to demolish unstable buildings	(NN-Joint
and clear sites for future construction .	NN-Joint)))
Many adjusters are authorized to write checks for amounts up to $ 100,000 on the spot .	(NS-Elaboration(NS-Elaboration
They do n't flinch at writing them .	NS-Elaboration)
`` That 's my job	(NS-Attribution(NS-Elaboration
-- get { policyholders } what they 're entitled to , ''	NS-Elaboration)
says Bill Schaeffer , a claims supervisor	(NS-Elaboration
who flew in from Aetna 's Bridgeport , Conn. , office .	NS-Elaboration))))))))
The Victorian house	(NN-Temporal(SN-Contrast(NN-Same-unit(NS-Elaboration
that Ms. Johnson is inspecting	NS-Elaboration)
has been deemed unsafe by town officials .	NN-Same-unit)
But she asks a workman	(NN-Temporal(NN-Same-unit(NS-Elaboration
toting the bricks from the lawn	NS-Elaboration)
to give her a boost through an open first-floor window .	NN-Same-unit)
Once inside , she spends nearly four hours	(NS-Elaboration(NS-Elaboration(NS-Manner-Means
measuring and diagramming each room in the 80-year-old house ,	NS-Manner-Means)
gathering enough information	(SN-Enablement
to estimate what it would cost to rebuild it .	SN-Enablement))
She snaps photos of the buckled floors and the plaster	(NS-Elaboration
that has fallen away from the walls .	NS-Elaboration))))
While she works inside ,	(NS-Elaboration(SN-Temporal
a tenant returns with several friends	(NS-Enablement
to collect furniture and clothing .	NS-Enablement))
One of the friends sweeps broken dishes and shattered glass from a countertop	(NN-Joint(NN-Joint
and starts to pack what can be salvaged from the kitchen .	NN-Joint)
Others grab books , records , photo albums , sofas and chairs ,	(NS-Elaboration
working frantically in the fear	(NS-Elaboration
that an aftershock will jolt the house again .	NS-Elaboration))))))
The owners , William and Margie Hammack , are luckier than many others .	(NS-Explanation
A few years ago , Mrs. Hammack insisted on buying earthquake insurance for this house ,	(NN-Joint(NS-Contrast(NS-Elaboration
which had been converted into apartments .	NS-Elaboration)
Only about 20 % of California home and business owners carried earthquake coverage .	NS-Contrast)
The Hammacks ' own home , also in Los Gatos , suffered comparatively minor damage .	NN-Joint)))
Ms. Johnson ,	(NS-Elaboration(NS-Elaboration(NN-Same-unit(NS-Elaboration
who works out of Aetna 's office in Walnut Creek , an East Bay suburb ,	NS-Elaboration)
is awed by the earthquake 's destructive force .	NN-Same-unit)
`` It really brings you down to a human level , ''	(NS-Attribution
she says .	NS-Attribution))
`` It 's hard to accept all the suffering	(SN-Evaluation(NN-Contrast(NS-Elaboration
people are going through ,	NS-Elaboration)
but you have to .	NN-Contrast)
If you do n't ,	(SN-Condition
you ca n't do your job . ''	SN-Condition))))
For Aetna and other insurers , the San Francisco earthquake hit	(NN-Topic-Change(NS-Elaboration(NS-Elaboration(NS-Elaboration(NS-Elaboration(NS-Elaboration(NS-Elaboration(SN-Background(NS-Condition(SN-Background(NS-Temporal
when resources in the field already were stretched .	NS-Temporal)
Most companies still are trying to sort through the wreckage	(NS-Elaboration(NS-Elaboration
caused by Hurricane Hugo in the Carolinas last month .	NS-Elaboration)
Aetna ,	(NS-Elaboration(NN-Same-unit(NS-Elaboration
which has nearly 3,000 adjusters ,	NS-Elaboration)
had deployed about 750 of them in Charlotte , Columbia , and Charleston .	NN-Same-unit)
Adjusters	(SN-Attribution(NN-Same-unit(NS-Elaboration
who had been working on the East Coast	NS-Elaboration)
say	NN-Same-unit)
the insurer will still be processing claims from that storm through December .	SN-Attribution))))
It could take six to nine months to handle the earthquake-related claims .	NS-Condition)
When the earthquake rocked northern California last week ,	(SN-Temporal
Aetna senior claims executives from the San Francisco area were at the company 's Hartford , Conn. , headquarters for additional training	(NS-Elaboration
on how to handle major catastrophes ,	(NS-Elaboration
including earthquakes .	NS-Elaboration))))
Since commercial airline flights were disrupted ,	(SN-Explanation
the company chartered three planes	(NS-Enablement
to fly these executives back to the West Coast	(NN-Joint
and bring along portable computers , cellular phones and some claims adjusters .	NN-Joint))))
Because of the difficulty	(NS-Elaboration(NN-Contrast(SN-Explanation(NS-Elaboration
of assessing the damages	(NS-Elaboration
caused by the earthquake ,	NS-Elaboration))
Aetna pulled together a team of its most experienced claims adjusters from around the country .	SN-Explanation)
Even so , few had ever dealt with an earthquake .	NN-Contrast)
Some adjusters , like Alan Singer of San Diego , had been working in Charleston for nearly four weeks .	(SN-Temporal
He returned home last Thursday ,	(NN-Joint
packed a bag with fresh clothes	(NN-Joint
and reported for duty Friday in Walnut Creek .	NN-Joint)))))
Offices were set up in San Francisco and San Jose .	NS-Elaboration)
In a few instances , Aetna knew	(NS-Elaboration(SN-Attribution
it would probably be shelling out big bucks ,	(NS-Temporal
even before a client called or faxed in a claim .	NS-Temporal))
For example , officials at Walnut Creek office learned	(NN-Temporal(SN-Attribution
that the Amfac Hotel near the San Francisco airport ,	(NN-Same-unit(NS-Elaboration
which is insured by Aetna ,	NS-Elaboration)
was badly damaged	NN-Same-unit))
when they saw it on network television news .	NN-Temporal)))
`` The secret	(NS-Elaboration(NS-Attribution(NN-Same-unit(NS-Elaboration
to being a good adjuster	NS-Elaboration)
is counting , ''	NN-Same-unit)
says Gerardo Rodriguez , an Aetna adjuster from Santa Ana .	NS-Attribution)
`` You have to count everything . ''	(NS-Elaboration(NS-Elaboration
Adjusters must count the number of bathrooms , balconies , fireplaces , chimneys , microwaves and dishwashers .	NS-Elaboration)
But they must also assign a price to each of these items as well as to floors , wallcoverings , roofing and siding ,	(NS-Elaboration(NS-Enablement
to come up with a total value for a house .	NS-Enablement)
To do that ,	(SN-Enablement
they must think in terms of sheetrock by the square foot , carpeting by the square yard , wallpaper by the roll , molding by the linear foot .	SN-Enablement)))))
Using a calculator and a unit-price guide for such jobs as painting , plumbing and roofing in each major region of the country ,	(NN-Contrast(SN-Background
adjusters can figure out the value of a home in today 's market and what it would cost	(NS-Elaboration
to rebuild it .	NS-Elaboration))
Sometimes repairs are out of the question .	(NS-Elaboration
When Aetna adjuster Bill Schaeffer visited a retired couple in Oakland last Thursday ,	(NS-Elaboration(NS-Elaboration(SN-Background
he found them living in a mobile home	(NS-Elaboration
parked in front of their yard .	NS-Elaboration))
The house itself ,	(NN-Same-unit(NS-Elaboration
located about 50 yards from the collapsed section of double-decker highway Interstate 880 ,	NS-Elaboration)
was pushed about four feet off its foundation	(NN-Temporal
and then collapsed into its basement .	NN-Temporal)))
The next day , Mr. Schaeffer presented the couple with a check for $ 151,000	(NS-Elaboration(NS-Enablement
to help them build a new home in the same neighborhood .	NS-Enablement)
He also is working with a real-estate agent	(NS-Enablement
to help find them an apartment	(NS-Elaboration
to rent	(NS-Temporal
while their home is being built .	NS-Temporal))))))))
Many of the adjusters	(NS-Elaboration(NS-Elaboration(SN-Background(SN-Background(NN-Temporal(SN-Cause(SN-Background(NS-Elaboration(SN-Contrast(NN-Same-unit(NS-Elaboration
employed by Aetna and other insurers	NS-Elaboration)
have some experience with construction work or carpentry .	NN-Same-unit)
But such skills were alien to Toni Johnson .	SN-Contrast)
Four years ago , she was managing a film-processing shop	(NN-Joint
and was totally bored .	NN-Joint))
A friend mentioned	(SN-Attribution
that she might want to look into a position at Aetna ,	(NS-Condition
if she was interested in a job	(NS-Elaboration
that would constantly challenge her .	NS-Elaboration))))
She signed up ,	(NS-Elaboration
starting as an `` inside '' adjuster ,	(NS-Elaboration
who settles minor claims	(NN-Joint
and does a lot of work by phone .	NN-Joint))))
A year later , she moved to the commercial property claims division .	(NN-Temporal
She spent a month at an Aetna school in Gettysburg , Pa. ,	(NN-Temporal(NS-Elaboration
learning all about the construction trade ,	(NS-Elaboration
including masonry , plumbing and electrical wiring .	NS-Elaboration))
That was followed by three months at the Aetna Institute in Hartford ,	(NS-Elaboration
where she was immersed in learning how to read and interpret policies .	NS-Elaboration))))
Her new line of work has some perils .	(NS-Explanation
Recently , a contractor saved her from falling three stories	(NS-Elaboration(NS-Background
as she investigated what remained of an old Victorian house	(NS-Elaboration
torched by an arsonist .	NS-Elaboration))
`` I owe that contractor .	(NS-Elaboration
I really do , ''	(NS-Attribution
she says .	NS-Attribution)))))
As Ms. Johnson stands outside the Hammack house	(NS-Evaluation(NN-Temporal(NS-Cause(NS-Explanation(NN-Temporal(SN-Temporal(NS-Temporal
after winding up her chores there ,	NS-Temporal)
the house begins to creak and sway .	SN-Temporal)
The ground shakes underneath her .	NN-Temporal)
It is an aftershock , one of about 2,000 since the earthquake ,	NS-Explanation)
and it makes her uneasy .	NS-Cause)
The next day ,	(NN-Same-unit
as she prepares a $ 10,000 check for the Hammacks ,	(SN-Background(NS-Elaboration
which will cover the cost	(NS-Elaboration
of demolishing the house	(NN-Joint
and clearing away the debris ,	NN-Joint)))
she jumps at the slightest noise .	SN-Background)))
On further reflection , she admits	(SN-Attribution
that venturing inside the Hammacks ' house the previous day was n't '' such a great idea . ''	SN-Attribution)))
During her second meeting with the Hammacks , Ms. Johnson reviews exactly what their policy covers .	(NS-Elaboration
They would like to retrieve some appliances on the second floor ,	(SN-Cause(SN-Background(NN-Topic-Comment(NN-Contrast
but wonder	(SN-Attribution
if it 's safe to venture inside .	SN-Attribution))
Ms. Johnson tells them	(SN-Attribution
that ,	(NN-Same-unit
if the appliances ca n't be salvaged ,	(SN-Condition
their policy covers the replacement cost .	SN-Condition))))
Mr. Hammack is eager to know	(NN-Topic-Comment(NS-Elaboration(SN-Attribution
what Aetna will pay for the house ,	(NS-Elaboration
which has to come down .	NS-Elaboration))
`` When will I get that check for a million dollars ? ''	(NS-Attribution
he jokes .	NS-Attribution))
The adjuster had n't completed all the calculations ,	(SN-Contrast
but says	(SN-Attribution
: `` We 're talking policy limits . ''	(NS-Elaboration
In this case , that 's about $ 250,000 .	NS-Elaboration)))))
It suddenly dawns on Mr. Hammack	(NS-Elaboration(NS-Condition(SN-Attribution
that rebuilding the house in Los Gatos , an affluent community in Santa Clara County , may cost	(NS-Comparison
more than Aetna 's policy will pay .	NS-Comparison))
`` We can lose money on this , ''	(NS-Attribution
he says .	NS-Attribution))
`` And you did n't want me to buy earthquake insurance , ''	(NS-Elaboration(NS-Attribution
says Mrs. Hammack ,	(NS-Elaboration
reaching across the table	(NN-Joint
and gently tapping his hand .	NN-Joint)))
Earthquake insurance costs about $ to $ annually for every $ 1,000 of value ,	(NS-Elaboration(SN-Explanation(NN-Joint
and high deductibles mean it generally pays	(NS-Condition
only when there is a catastrophe .	NS-Condition))
So , many Californians believe	(SN-Attribution
they can get by	(NS-Background
without it .	NS-Background)))
Even Ms. Johnson herself made that assumption .	(NN-Contrast(NS-Elaboration
`` I always knew	(NS-Attribution(SN-Attribution
that the ` Big One ' was coming ,	(NN-Contrast
but not during my lifetime , ''	NN-Contrast))
she says .	NS-Attribution))
Now she says	(SN-Attribution
she 's thinking of contacting her own insurance agent .	SN-Attribution))))))))
For Ms. Johnson , dealing with the earthquake has been more than just a work experience .	(NS-Explanation(NS-Elaboration(NS-Elaboration
She lives in Oakland , a community	(NS-Explanation(NS-Elaboration
hit hard by the earthquake .	NS-Elaboration)
She did n't have hot water for five days .	(NN-Joint
The apartment	(NS-Contrast(NN-Joint(NN-Same-unit(NS-Elaboration
she shares with a 12-year-old daughter and her sister	NS-Elaboration)
was rattled ,	NN-Same-unit)
books and crystal hit the floor ,	NN-Joint)
but nothing was severely damaged .	NS-Contrast))))
Her sister , Cynthia , wishes	(NS-Elaboration(SN-Attribution
Toni had a different job .	SN-Attribution)
`` We worry about her out there , ''	(NS-Attribution
Cynthia says .	NS-Attribution)))
Last Sunday , Ms. Johnson finally got a chance	(NN-Joint(NS-Explanation(SN-Contrast(NS-Elaboration
to water her plants ,	NS-Elaboration)
but stopped abruptly .	SN-Contrast)
`` I realized	(SN-Attribution
I could n't waste this water	(NS-Background
when there are people in Watsonville	(NS-Elaboration
who do n't have fresh water	(NS-Elaboration
to drink . ''	NS-Elaboration)))))
She has n't played any music	(NN-Same-unit(NS-Temporal
since the earthquake hit ,	NS-Temporal)
out of respect for those	(NS-Elaboration
who died on Interstate 880	(NS-Elaboration
where the roadway collapsed .	NS-Elaboration))))))))
