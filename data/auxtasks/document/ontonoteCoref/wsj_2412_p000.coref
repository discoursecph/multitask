The debate over National Service has begun again .	ROOT	0
After a decade in which more than 50 localities established their own service or conservation corps and dozens of school systems made community service a prerequisite to high - school graduation , the focus has shifted to Washington .	NONE	1
At least 10 bills proposing one or another national program were introduced in Congress this spring .	NONE	2
One , co-sponsored by Sen. Sam Nunn ( D. , Ga . ) and Rep. Dave McCurdy ( D. , Okla. ) , would have restricted federal college subsidies to students who had served .	NONE	3
An omnibus bill assembled by Sen. Edward Kennedy ( D. , Mass. ) , and including some diluted Nunn - McCurdy provisions along with proposals by fellow Democratic Sens. Claiborne Pell , Barbara Mikulski and Christopher Dodd , has been reported out of the Senate Labor Committee .	NONE	4
It might well win Senate passage .	COREF_PREV	5
President Bush has outlined his own Youth Entering Service ( YES ) plan , though its details remain to be specified .	NONE	6
What is one to think of all this ?	NONE	7
Doctrine and special interests govern some responses .	NONE	8
People eager to have youth `` pay their dues to society '' favor service proposals -- preferably mandatory ones .	NONE	9
So do those who seek a `` re-energized concept of citizenship , '' a concept imposing stern obligations as well as conferring rights .	NONE	10
Then there are instinctive opponents .	NONE	11
To libertarians , mandatory service is an abomination and voluntary systems are illegitimate uses of tax money .	NONE	12
Devotees of the market question the value of the work national service would perform :	NONE	13
If the market wo n't pay for it , they argue , it ca n't be worth its cost .	COREF_PREV	14
Elements of the left are also reflexively opposed ; they see service as a cover for the draft , or fear the regimentation of youth , or want to see rights enlarged , not obligations .	COREF_PREV	15
But what about those of us whose views are not predetermined by formula or ideology ?	NONE	16
How should we think about national service ?	COREF_PREV	17
Let 's begin by recognizing a main source of confusion -- `` national service '' has no agreed meaning .	NONE	18
Would service be voluntary or compulsory ?	NONE	19
Short or long ?	NONE	20
Part - time or full - time ?	NONE	21
Paid or unpaid ?	NONE	22
Would participants live at home and work nearby or live in barracks and work on public lands ?	NONE	23
What kinds of work would they do ?	NONE	24
What does `` national '' mean ?	COREF_OTHER	25
Would the program be run by the federal government , by local governments , or by private voluntary organizations ?	NONE	26
And who would serve ?	NONE	27
Only males , as with the draft , or both sexes ?	NONE	28
Youth only or all ages ?	COREF_OTHER	29
Middle - class people , or poor people , or a genuine cross-section ?	NONE	30
Many or few ?	NONE	31
Those are not trivial questions , and the label `` national service '' answers none of them .	COREF_OTHER	32
Then how should we think about national service ?	COREF_PREV	33
As a starting point , here are five propositions : 1 . Consider the ingredients , not the name .	NONE	34
Ignore `` national service '' in the abstract ; consider specific proposals .	NONE	35
They will differ in crucial ways .	NONE	36
2 . `` Service '' should be service .	COREF_PREV	37
As commonly understood , service implies sacrifice .	NONE	38
It involves accepting risk , or giving up income , or deferring a career .	NONE	39
It follows that proposals like Nunn - McCurdy , whose benefits to enrollees are worth some $ 17,500 a year , do not qualify .	NONE	40
There is a rationale for such bills : Federal subsidies to college students amount to `` a GI Bill without the GI '' ; arguably those benefits should be earned , not given .	COREF_PREV	41
But the earnings exceed by 20 % the average income of young high - school graduates with full - time jobs .	NONE	42
Why call that service ?	NONE	43
3 . Encouragement is fine ; compulsion is not .	NONE	44
Compelled service is unconstitutional .	NONE	45
It is also unwise and unenforceable .	NONE	46
( Who will throw several hundred thousand refusers in jail each year ? )	COREF_PREV	47
But through tax policy and in other ways the federal government encourages many kinds of behavior .	COREF_OTHER	48
It should also encourage service -- preferably by all classes and all ages .	COREF_PREV	49
Its encouragement should strengthen and not undercut the strong tradition of volunteering in the U.S. , should build on the service programs already in existence , and should honor local convictions about which tasks most need doing .	COREF_PREV	50
4 . Good programs are not cheap .	NONE	51
Enthusiasts assume that national service would get important work done cheaply : forest fires fought , housing rehabilitated , students tutored , day - care centers staffed .	NONE	52
There is important work to be done , and existing service and conservation corps have shown that even youths who start with few skills can do much of it well -- but not cheaply .	NONE	53
Good service programs require recruitment , screening , training and supervision -- all of high quality .	COREF_PREV	54
They involve stipends to participants .	COREF_PREV	55
Full - time residential programs also require housing and full - time supervision ; they are particularly expensive -- more per participant than a year at Stanford or Yale .	COREF_OTHER	56
Non-residential programs are cheaper , but good ones still come to some $ 10,000 a year .	NONE	57
Are they worth that ?	NONE	58
Evaluations suggest that good ones are -- especially so if the effects on participants are counted .	COREF_OTHER	59
But the calculations are challengeable .	NONE	60
5 . Underclass youth are a special concern .	NONE	61
Are such expenditures worthwhile , then ?	NONE	62
Yes , if targeted .	NONE	63
People of all ages and all classes should be encouraged to serve , but there are many ways for middle - class kids , and their elders , to serve at little public cost .	NONE	64
They can volunteer at any of thousands of non-profit institutions , or participate in service programs required by high schools or encouraged by colleges or employers .	COREF_PREV	65
Underclass youth do n't have those opportunities .	NONE	66
They are not enrolled in high school or college .	NONE	67
They are unlikely to be employed .	COREF_PREV	68
And they have grown up in unprecedentedly grim circumstances , among family structures breaking down , surrounded by self - destructive behaviors and bleak prospects .	COREF_PREV	69
But many of them can be quite profoundly reoriented by productive and disciplined service .	COREF_PREV	70
Some wo n't accept the discipline ; others drop out for other reasons .	NONE	71
But some whom nothing else is reaching are transformed .	NONE	72
Learning skills , producing something cooperatively , feeling useful , they are no longer dependent -- others now depend on them .	NONE	73
Even if it is cheaper to build playgrounds or paint apartments or plant dune - grass with paid professionals , the effects on the young people providing those services alter the calculation .	COREF_PREV	74
Strictly speaking , these youth are not performing service .	NONE	75
They are giving up no income , deferring no careers , incurring no risk .	COREF_PREV	76
But they believe themselves to be serving , and they begin to respect themselves ( and others ) , to take control of their lives , to think of the future .	COREF_PREV	77
That is a service to the nation .	NONE	78
It is what federal support should try hardest to achieve .	COREF_OTHER	79
Mr. Szanton , a Carter administration budget official , heads his own Washington - based strategic planning firm .	COREF_PREV	80
He is a co-author of `` National Service : What Would It Mean ? ''	COREF_PREV	81
( Lexington Books , 1986 ) .	COREF_PREV	82
