Short interest in Nasdaq over-the-counter stocks rose 6 % as of mid-October , its biggest jump since 6.3 % last April .	(TextualOrganization(summary
The most recent OTC short interest statistics were compiled Oct. 13 , the day	(elaboration-additional(elaboration-additional(elaboration-additional(elaboration-additional(elaboration-object-attribute
the Nasdaq composite index slid 3 %	(List
and the New York Stock Exchange tumbled 7 % .	List))
The coincidence might lead to the conclusion	(antithesis(comment(elaboration-object-attribute
that short-sellers bet heavily on that day	(attribution
that OTC stocks would decline further .	attribution))
As it happens ,	(circumstance
the Nasdaq composite did continue to fall for two days after the initial plunge .	circumstance))
However , the short interest figures	(elaboration-additional(attribution(Same-Unit(elaboration-object-attribute
reported by brokerage and securities clearing firms to the National Association of Securities Dealers	elaboration-object-attribute)
include only those trades	(elaboration-object-attribute
completed , or settled , by Oct. 13 , rather than trades	(elaboration-object-attribute
that occurred on that day ,	elaboration-object-attribute)))
according to Gene Finn , chief economist for the NASD .	attribution)
Generally , it takes five business days to transfer stock and to take the other steps	(elaboration-object-attribute
necessary to settle a trade .	elaboration-object-attribute))))
The total short interest in Nasdaq stocks as of mid-October was 237.1 million shares , up from 223.7 million in September but well below the record level of 279 million shares	(elaboration-additional(elaboration-object-attribute
established in July 1987 .	elaboration-object-attribute)
The sharp rise in OTC short interest compares with the 4.2 % decline in short interest on the New York Stock Exchange and the 3 % rise on the American Stock Exchange during the September-October period .	elaboration-additional))
Generally , a short seller expects a fall in a stock 's price	(antithesis(elaboration-additional(background(List(List
and aims to profit	(means
by selling borrowed shares	(elaboration-object-attribute
that are to be replaced later ;	elaboration-object-attribute)))
the short seller hopes	(attribution
the replacement shares	(Same-Unit(elaboration-object-attribute
bought later	elaboration-object-attribute)
will cost less than those	(elaboration-object-attribute
that were sold .	elaboration-object-attribute))))
Short interest ,	(Same-Unit(elaboration-object-attribute
which represents the number of shares	(elaboration-object-attribute
borrowed and sold ,	(List
but not yet replaced ,	List)))
can be a bad-expectations barometer for many stocks .	Same-Unit))
Among 2,412 of the largest OTC issues , short interest rose to 196.8 million shares , from 185.7 million in 2,379 stocks in September .	(elaboration-additional
Big stocks with large short interest gains as of Oct. 13 included First Executive , Intel , Campeau and LIN Broadcasting .	(elaboration-additional(elaboration-additional
Short interest in First Executive , an insurance issue , rose 55 % to 3.8 million .	(Comparison
Intel 's short interest jumped 42 % ,	(elaboration-additional(Comparison
while Campeau 's increased 62 % .	Comparison)
Intel makes semiconductors	(List
and Campeau operates department-store chains	(List
and is strained for cash .	List)))))
Meritor Savings again had the dubious honor	(comparison(elaboration-additional(elaboration-object-attribute
of being the OTC stock with the biggest short interest position on Nasdaq .	elaboration-object-attribute)
Meritor has headed the list since May .	elaboration-additional)
First Executive and troubled Valley National Corp. of Arizona were next in line .	comparison))))
Short selling is n't necessarily bad for the overall market .	(explanation-argumentative
Shorted shares must eventually be replaced	(example(List(means
through buying .	means)
In addition , changes in short interest in some stocks may be caused by arbitrage .	List)
For example , an investor may seek to profit during some takeover situations	(example(means
by buying stock in one company involved	(List
and shorting the stock of the other .	List))
Two big stocks	(antithesis(elaboration-general-specific(Same-Unit(elaboration-object-attribute
involved in takeover activity	elaboration-object-attribute)
saw their short interest surge .	Same-Unit)
Short interest in the American depositary receipts of Jaguar , the target of both Ford Motor and General Motors , more than doubled .	elaboration-general-specific)
Nasdaq stocks	(Same-Unit(elaboration-object-attribute
that showed a drop in short interest	elaboration-object-attribute)
included Adobe Systems , Class A shares of Tele-Communications and takeover targets Lyphomed and Jerrico .	Same-Unit)))))))
The NASD ,	(background(elaboration-set-member(Same-Unit(elaboration-object-attribute
which operates the Nasdaq computer system	(elaboration-object-attribute
on which 5,200 OTC issues trade ,	elaboration-object-attribute))
compiles short interest data in two categories :	Same-Unit)
the approximately two-thirds , and generally biggest , Nasdaq stocks	(List(elaboration-object-attribute
that trade on the National Market System ;	elaboration-object-attribute)
and the one-third , and generally smaller , Nasdaq stocks	(elaboration-object-attribute
that are n't a part of the system .	elaboration-object-attribute)))
Short interest in 1,327 non-NMS securities totaled 40.3 million shares ,	(List(comparison
compared with almost 38 million shares in 1,310 issues in September .	comparison)
The October short interest represents 1.04 days of average daily trading volume in the smaller stocks in the system for the reporting period ,	(List(comparison
compared with 0.94 day a month ago .	comparison)
Among bigger OTC stocks , the figures represent 2.05 days of average daily volume ,	(comparison
compared with 2.14 days in September .	comparison))))))
The adjacent tables show the issues	(elaboration-additional(elaboration-object-attribute
in which a short interest position of at least 50,000 shares existed as of Oct. 13	(Disjunction
or in which there was a short position change of at least 25,000 shares since Sept. 15	Disjunction))
( see accompanying tables	(attribution
-- WSJ Oct. 25 , 1989 ) .	attribution)))
