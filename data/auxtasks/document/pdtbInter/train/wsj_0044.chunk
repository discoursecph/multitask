Cathryn Rice could hardly believe her eyes.	B-synchrony	9..52
While giving the Comprehensive Test of Basic Skills to ninth graders at Greenville High School last March 16, she spotted a student looking at crib sheets.	I-synchrony	53..208
She had seen cheating before, but these notes were uncanny.	B-instantiation	211..270
"A stockbroker is an example of a profession in trade and finance... .	I-instantiation	271..341
At the end of World War II, Germany surrendered before Japan... .	O	342..407
The Senate-House conference committee is used when a bill is passed by the House and Senate in different forms."	O	408..520
Virtually word for word, the notes matched questions and answers on the social-studies section of the test the student was taking.	O	523..653
In fact, the student had the answers to almost all of the 40 questions in that section	B-entrel	654..741
The student surrendered the notes, but not without a protest.	I-entrel	742..803
"My teacher said it was OK for me to use the notes on the test," he said.	O	804..877
The teacher in question was Nancy Yeargin -- considered by many students and parents to be one of the best at the school.	O	880..1001
Confronted, Mrs. Yeargin admitted she had given the questions and answers two days before the examination to two low-ability geography classes.	O	1002..1145
She had gone so far as to display the questions on an overhead projector and underline the answers.	O	1146..1245
Mrs. Yeargin was fired and prosecuted under an unusual South Carolina law that makes it a crime to breach test security	B-asynchronous	1248..1368
In September, she pleaded guilty and paid a $500 fine	B-contrast	1369..1423
Her alternative was 90 days in jail.	I-contrast	1424..1460
Her story is partly one of personal downfall.	O	1463..1508
She was an unstinting teacher who won laurels and inspired students, but she will probably never teach again.	O	1509..1618
In her wake she left the bitterness and anger of a principal who was her friend and now calls her a betrayer; of colleagues who say she brought them shame; of students and parents who defended her and insist she was treated harshly; and of school-district officials stunned that despite the bald-faced nature of her actions, she became something of a local martyr.	O	1619..1983
Mrs. Yeargin's case also casts some light on the dark side of school reform, where pressures on teachers are growing and where high-stakes testing has enhanced the temptation to cheat.	O	1986..2170
The 1987 statute Mrs. Yeargin violated was designed to enforce provisions of South Carolina's school-improvement laws	B-entrel	2171..2289
Prosecutors alleged that she was trying to bolster students' scores to win a bonus under the state's 1984 Education Improvement Act.	I-entrel	2290..2422
The bonus depended on her ability to produce higher student-test scores.	O	2423..2495
"There is incredible pressure on school systems and teachers to raise test scores," says Walt Haney, an education professor and testing specialist at Boston College.	O	2498..2663
"So efforts to beat the tests are also on the rise."	O	2664..2716
And most disturbing, it is educators, not students, who are blamed for much of the wrongdoing.	O	2717..2811
A 50-state study released in September by Friends for Education, an Albuquerque, N.M., school-research group, concluded that "outright cheating by American educators" is "common."	O	2814..2993
The group says standardized achievement test scores are greatly inflated because teachers often "teach the test" as Mrs. Yeargin did, although most are never caught.	O	2994..3159
Evidence of widespread cheating has surfaced in several states in the last year or so	B-instantiation	3162..3248
California's education department suspects adult responsibility for erasures at 40 schools that changed wrong answers to right ones on a statewide test	B-conjunction	3249..3401
After numerous occurrences of questionable teacher help to students, Texas is revising its security practices.	I-conjunction	3402..3512
And sales of test-coaching booklets for classroom instruction are booming	B-restatement	3515..3589
These materials, including Macmillan/McGraw-Hill School Publishing Co. 's Scoring High and Learning Materials -- are nothing short of sophisticated crib sheets, according to some recent academic research.	I-restatement	3590..3794
By using them, teachers -- with administrative blessing -- telegraph to students beforehand the precise areas on which a test will concentrate, and sometimes give away a few exact questions and answers	B-entrel	3795..3997
Use of Scoring High is widespread in South Carolina and common in Greenville County, Mrs. Yeargin's school district.	I-entrel	3998..4114
Experts say there isn't another state in the country where tests mean as much as they do in South Carolina.	O	4117..4224
Under the state's Education Improvement Act, low test scores can block students' promotions or force entire districts into wrenching, state-supervised "interventions" that can mean firings	B-contrast	4227..4416
High test scores, on the other hand, bring recognition and extra money -- a new computer lab for a school, grants for special projects, a bonus for the superintendent.	I-contrast	4417..4584
And South Carolina says it is getting results.	O	4587..4633
Since the reforms went in place, for example, no state has posted a higher rate of improvement on the Scholastic Aptitude Test than South Carolina, although the state still posts the lowest average score of the about 21 states who use the SAT as the primary college entrance examination.	O	4634..4921
Critics say South Carolina is paying a price by stressing improved test scores so much	B-restatement	4922..5009
Friends of Education rates South Carolina one of the worst seven states in its study on academic cheating	B-restatement	5010..5116
Says the organization's founder, John Cannell, prosecuting Mrs. Yeargin is "a way for administrators to protect themselves and look like they take cheating seriously, when in fact they don't take it seriously at all."	I-restatement	5117..5334
Paul Sandifer, director of testing for the South Carolina department of education, says Mr. Cannell's allegations of cheating "are purely without foundation," and based on unfair inferences.	O	5337..5527
Partly because of worries about potential abuse, however, he says the state will begin keeping closer track of achievement-test preparation booklets next spring.	O	5528..5689
South Carolina's reforms were designed for schools like Greenville High School	B-entrel	5692..5771
Standing on a shaded hill in a run-down area of this old textile city, the school has educated many of South Carolina's best and brightest, including the state's last two governors, Nobel Prize winning physicist Charles Townes and actress Joanne Woodward.	I-entrel	5772..6027
But by the early 1980s, its glory had faded like the yellow bricks of its broad facade.	O	6028..6115
"It was full of violence and gangs and kids cutting class," says Linda Ward, the school's principal.	O	6118..6218
"Crime was awful, test scores were low, and there was no enrollment in honors programs."	O	6219..6307
Mrs. Ward took over in 1986, becoming the school's seventh principal in 15 years	B-entrel	6310..6391
Her immediate predecessor suffered a nervous breakdown	B-entrel	6392..6447
Prior to his term, a teacher bled to death in the halls, stabbed by a student.	I-entrel	6448..6526
Academically, Mrs. Ward says, the school was having trouble serving in harmony its two disparate, and evenly split, student groups: a privileged white elite from old monied neighborhoods and blacks, many of them poor, from run-down, inner city neighborhoods.	O	6527..6785
Mrs. Ward resolved to clean out "deadwood" in the school's faculty and restore safety, and she also had some new factors working in her behalf.	O	6788..6931
One was statewide school reform, which raised overall educational funding and ushered in a new public spirit for school betterment.	O	6932..7063
Another was Nancy Yeargin, who came to Greenville in 1985, full of the energy and ambitions that reformers wanted to reward.	O	7064..7188
"Being a teacher just became my life," says the 37-year-old Mrs. Yeargin, a teacher for 12 years before her dismissal. "	O	7191..7311
I loved the school, its history.	O	7311..7343
I even dreamt about school and new things to do with my students."	O	7344..7410
While Mrs. Ward fired and restructured staff and struggled to improve curriculum, Mrs. Yeargin worked 14-hour days and fast became a student favorite.	O	7413..7563
In 1986-87 and 1987-88, she applied for and won bonus pay under the reform law.	O	7564..7643
Encouraged by Mrs. Ward, Mrs. Yeargin taught honor students in the state "teacher cadet" program, a reform creation designed to encourage good students to consider teaching as a career.	O	7644..7829
She won grant money for the school, advised cheerleaders, ran the pep club, proposed and taught a new "Cultural Literacy" class in Western Civilization and was chosen by the school PTA as "Teacher of the Year."	O	7830..8040
"She was an inspirational lady; she had it all together," says Laura Dobson, a freshman at the University of South Carolina who had Mrs. Yeargin in the teacher-cadet class last year.	O	8043..8225
She says that because of Mrs. Yeargin she gave up ambitions in architecture and is studying to become a teacher.	O	8226..8338
Mary Beth Marchand, a Greenville 11th grader, also says Mrs. Yeargin inspired her to go into education.	O	8339..8442
"She taught us more in Western Civilization than I've ever learned in other classes," says Kelli Green, a Greenville senior.	O	8443..8567
In the classroom, students say, Mrs. Yeargin distinguished herself by varying teaching approaches -- forcing kids to pair up to complete classroom work or using college-bowl type competitions.	O	8570..8762
On weekends, she came to work to prepare study plans or sometimes, even to polish the furniture in her classroom.	O	8763..8876
"She just never gave it up," says Mary Marchand, Mary Beth's mother.	O	8879..8947
"You'd see her correcting homework in the stands at a football game."	O	8948..9017
Some fellow teachers, however, viewed Mrs. Yeargin as cocky and too yielding to students.	O	9020..9109
Mrs. Ward says she often defended her to colleagues who called her a grandstander.	O	9110..9192
Pressures began to build	B-conjunction	9193..9218
Friends told her she was pushing too hard.	I-conjunction	9219..9261
Because of deteriorating hearing, she told colleagues she feared she might not be able to teach much longer.	O	9262..9370
Mrs. Yeargin's extra work was also helping her earn points in the state's incentive-bonus program.	O	9373..9471
But the most important source of points was student improvement on tests	B-restatement	9472..9545
Huge gains by her students in 1987 and 1988 meant a total of $5,000 in bonuses over two years -- a meaningful addition to her annual salary of $23,000.	I-restatement	9546..9697
Winning a bonus for a third year wasn't that important to her, Mrs. Yeargin insists.	O	9700..9784
But others at Greenville High say she was eager to win -- if not for money, then for pride and recognition.	O	9785..9892
Mary Elizabeth Ariail, another social-studies teacher, says she believed Mrs. Yeargin wanted to keep her standing high so she could get a new job that wouldn't demand good hearing.	O	9893..10073
Indeed, Mrs. Yeargin was interested in a possible job with the state teacher cadet program.	O	10074..10165
Last March, after attending a teaching seminar in Washington, Mrs. Yeargin says she returned to Greenville two days before annual testing feeling that she hadn't prepared her low-ability geography students adequately.	O	10168..10385
When test booklets were passed out 48 hours ahead of time, she says she copied questions in the social studies section and gave the answers to students.	O	10386..10538
Mrs. Yeargin admits she made a big mistake but insists her motives were correct.	O	10541..10621
"I was trying to help kids in an unfair testing situation," she says.	O	10622..10691
"Only five of the 40 questions were geography questions.	O	10692..10748
The rest were history, sociology, finance -- subjects they never had."	O	10749..10819
Mrs. Yeargin says that she also wanted to help lift Greenville High School's overall test scores, usually near the bottom of 14 district high schools in rankings carried annually by local newspapers.	O	10822..11021
Mostly, she says, she wanted to prevent the damage to self-esteem that her low-ability students would suffer from doing badly on the test.	O	11022..11160
"These kids broke my heart," she says.	O	11161..11199
"A whole day goes by and no one even knows they're alive.	O	11200..11257
They desperately needed somebody who showed they cared for them, who loved them.	O	11258..11338
The last thing they needed was another drag-down blow."	O	11339..11394
School officials and prosecutors say Mrs. Yeargin is lying.	O	11397..11456
They found students in an advanced class a year earlier who said she gave them similar help, although because the case wasn't tried in court, this evidence was never presented publicly.	O	11457..11642
"That pretty much defeats any inkling that she was out to help the poor underprivileged child," says Joe Watson, the prosecutor in the case, who is also president of Greenville High School's alumni association.	O	11643..11853
Mrs. Yeargin concedes that she went over the questions in the earlier class, adding: "I wanted to help all" students.	O	11854..11971
Mr. Watson says Mrs. Yeargin never complained to school officials that the standardized test was unfair.	O	11974..12078
"Do I have much sympathy for her?" Mr. Watson asks.	O	12079..12130
"Not really.	O	12131..12143
I believe in the system	B-conjunction	12144..12168
I believe you have to use the system to change it.	I-conjunction	12169..12219
What she did was like taking the law into your own hands."	O	12220..12278
Mrs. Ward says that when the cheating was discovered, she wanted to avoid the morale-damaging public disclosure that a trial would bring.	O	12281..12418
She says she offered Mrs. Yeargin a quiet resignation and thought she could help save her teaching certificate.	O	12419..12530
Mrs. Yeargin declined.	O	12531..12553
"She said something like `You just want to make it easy for the school. '	O	12554..12627
I was dumbfounded," Mrs. Ward recalls.	O	12628..12666
"It was like someone had turned a knife in me."	O	12667..12714
To the astonishment and dismay of her superiors and legal authorities -- and perhaps as a measure of the unpopularity of standardized tests -- Mrs. Yeargin won widespread local support	B-instantiation	12717..12902
The school-board hearing at which she was dismissed was crowded with students, teachers and parents who came to testify on her behalf	B-conjunction	12903..13037
Supportive callers decried unfair testing, not Mrs. Yeargin, on a local radio talk show on which she appeared.	I-conjunction	13038..13148
The show didn't give the particulars of Mrs. Yeargin's offense, saying only that she helped students do better on the test.	O	13151..13274
"The message to the board of education out of all this is we've got to take a serious look at how we're doing our curriculum and our testing policies in this state," said the talk-show host.	O	13275..13465
Editorials in the Greenville newspaper allowed that Mrs. Yeargin was wrong, but also said the case showed how testing was being overused.	O	13466..13603
The radio show "enraged us," says Mrs. Ward.	O	13606..13650
Partly because of the show, Mr. Watson says, the district decided not to recommend Mrs. Yeargin for a first-time offenders program that could have expunged the charges and the conviction from her record.	O	13651..13854
And legal authorities cranked up an investigation worthy of a murder case	B-restatement	13855..13929
Over 50 witnesses, mostly students, were interviewed.	I-restatement	13930..13983
At Greenville High School, meanwhile, some students -- especially on the cheerleading squad -- were crushed.	O	13986..14094
"It's hard to explain to a 17-year-old why someone they like had to go," says Mrs. Ward.	O	14095..14183
Soon, T-shirts appeared in the corridors that carried the school's familiar red-and-white GHS logo on the front.	O	14184..14296
On the back, the shirts read, "We have all the answers."	O	14297..14353
Many colleagues are angry at Mrs. Yeargin.	O	14354..14396
"She did a lot of harm," says Cathryn Rice, who had discovered the crib notes.	O	14397..14475
"We work damn hard at what we do for damn little pay, and what she did cast unfair aspersions on all of us."	O	14476..14584
But several teachers also say the incident casts doubt on the wisdom of evaluating teachers or schools by using standardized test scores.	O	14587..14724
Says Gayle Key, a mathematics teacher, "The incentive pay thing has opened up a can of worms.	O	14725..14818
There may be others doing what she did."	O	14819..14859
Mrs. Yeargin says she pleaded guilty because she realized it would no longer be possible to win reinstatement, and because she was afraid of further charges.	O	14862..15019
Mrs. Ward, for one, was relieved.	O	15020..15053
Despite the strong evidence against Mrs. Yeargin, popular sentiment was so strong in her favor, Mrs. Ward says, that "I'm afraid a jury wouldn't have convicted her.	O	15054..15218
