Yields on certificates of deposit at major banks were little changed in the latest week .	(NS-elaboration-additional(NS-elaboration-additional(NS-elaboration-additional(NS-antithesis(NS-elaboration-additional(NS-elaboration-additional(NS-evidence
The average yield on six-month CDs of $ 50,000 and less slipped to 7.96 % from 8.00 % ,	(NS-elaboration-additional(NN-List(NS-attribution
according to Banxquote Money Markets , an information service	(NS-elaboration-additional
based here .	NS-elaboration-additional))
On one-year CDs of $ 50,000 and less , the average slid to 8.02 % from 8.06 % .	NN-List)
Both issues are among the most popular with individual investors .	NS-elaboration-additional))
`` Because of shrinkage in the economy ,	(NS-elaboration-additional(NS-attribution(SN-consequence
rates can be expected to decline over a one-year horizon , ''	SN-consequence)
said Norberto Mehl , chairman of Banxquote .	NS-attribution)
`` It 's unclear how much rates can fall and how soon . ''	NS-elaboration-additional))
Changes in CD yields in the week	(NS-example(NS-elaboration-additional(NN-Same-Unit(NS-elaboration-object-attribute
ended Tuesday	NS-elaboration-object-attribute)
were in line with blips up and down within a fairly narrow range for the last two months .	NN-Same-Unit)
Interest rates generally began declining last spring	(NS-temporal-after
after moving steadily upward for more than a year .	NS-temporal-after))
The average yield on small-denomination three-month CDs moved up two-hundredths of a percentage point in the latest week to 7.85 % .	(NN-Comparison
Long-term CDs declined just a fraction .	(NN-Comparison
The average yield on both two-year CDs and five-year CDs was 7.98 % .	NN-Comparison))))
Only CDs	(NS-explanation-argumentative(NS-example(NS-elaboration-additional(NN-Same-Unit(NS-elaboration-object-attribute
sold by major brokerage firms	NS-elaboration-object-attribute)
posted significant increases in average yields in the latest week ,	NN-Same-Unit)
reflecting increased yields on Treasury bills	(NS-elaboration-object-attribute
sold at Monday 's auction .	NS-elaboration-object-attribute))
The average yield on six-month broker-sold CDs rose to 8.29 % from 8.05 %	(NN-List
and on one-year CDs the average yield rose to 8.30 % from 8.09 % .	NN-List))
The brokerage firms ,	(SN-attribution(NN-Same-Unit(NS-elaboration-object-attribute
which negotiate rates with the banks and thrifts	(NS-elaboration-object-attribute
whose CDs	(NS-elaboration-object-attribute
they sell ,	NS-elaboration-object-attribute)))
generally feel	NN-Same-Unit)
they have to offer clients more	(NS-comparison
than they can get on T-bills or from banks and thrifts directly .	NS-comparison))))
T-bills	(NS-example(SN-circumstance(NS-elaboration-additional(NN-Same-Unit(NS-elaboration-object-attribute
sold at Monday 's auction	NS-elaboration-object-attribute)
yielded 7.90 % for six months and 7.77 % for three months , up from 7.82 % and 7.61 % , respectively , the week before .	NN-Same-Unit)
So-called jumbo CDs , typically in denominations of $ 90,000 and up , also usually follow T-bills and interest rate trends in general more than those	(NS-elaboration-object-attribute
aimed at small investors .	NS-elaboration-object-attribute))
Some jumbos posted fractional changes in average yields this week , both up and down .	SN-circumstance)
The average yield on threemonth jumbos rose to 8.00 % from 7.96 % ,	(NN-List
while the two-year average fell by the same amount to 7.89 % .	(NN-List
Six-month and oneyear yields were unchanged , on average .	NN-List))))
`` The ( CD ) market is unsettled right now , ''	(SN-circumstance(NS-interpretation(NS-attribution
said Banxquote 's Mr. Mehl .	NS-attribution)
`` It 's very easily influenced by changes in the stock market and the junk bond market . ''	NS-interpretation)
The small changes in averages reflect generally unchanged yields at many major banks .	(NN-Contrast
Some , however , lowered yields significantly .	(NS-example
At Chase Manhattan Bank in New York , for example , the yield on a small denomination six-month CD fell about a quarter of a percentage point to 8.06 % .	(NN-List
In California , Bank of America dropped the yield on both six-month and one-year `` savings '' CDs to 8.33 % from 8.61 % .	NN-List)))))
Yields on money-market deposits were unchanged at an average 6.96 % for $ 50,000 and less and down just a hundredth of a percentage point to 7.41 % for jumbo deposits .	NS-elaboration-additional)
