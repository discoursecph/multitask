Wall Street would like UAL Corp. 's board to change its mind about keeping the company independent .	root	9..108
But what happens next in the continuing takeover drama may depend more on the company 's two most powerful and fractious unions : the pilots and machinists .	contrast	109..263
Some people familiar with the situation believe that the collapse of the previous $ 6.79 billion buy-out , if anything , may have strengthened the hands of these two labor groups .	norel	266..442
As a result , both may now have virtual veto power over any UAL transaction .	cause	443..518
One reason : banks -- likely to react to any new UAL deal with even more caution than the first time around -- probably will insist on labor harmony	norel	521..668
before they agree to put up cash for any new bid or even a less-ambitious recapitalization plan .	asynchronous	669..765
" United pilots have shown on a number of occasions they are willing and able to strike , " said an executive at Fuji Bank , one of UAL 's large lenders .	norel	768..916
" If you have both { labor } groups on strike , you 've got no revenue and that 's a very scary thing for a bank to be looking at . "	cause	917..1042
Just this past week , a leading Japanese bank asked for a meeting with the machinists ' union leaders to determine where the union would stand if another bid or recapitalization became possible .	norel	1045..1237
Another reason : Emboldened by their success in helping to scuttle the previous transaction , the machinists are likely to be more aggressive if a second buy-out attempt occurs .	norel	1240..1415
The two unions already had significant leverage	norel	1418..1465
simply because their employer has yet to settle with either on new contracts .	cause	1466..1543
That gives them both the threat of a strike and the ability to resist any wage concessions that may be necessary to make a transaction work .	cause	1544..1684
Thus , even investors who are pushing for the board to do a recapitalization that would pay shareholders a special dividend and possibly grant employees an ownership stake acknowledge that the unions are key .	cause	1687..1894
" There 's less likelihood of creating and completing a transaction without the unions ' cooperation and wage concessions , " said Richard Nye , of Baker , Nye Investments , a New York takeover stock-trader .	cause	1895..2094
Mr. Nye thinks the UAL board should be ousted if it does n't move soon to increase shareholder value .	entrel	2095..2195
Both the pilots and machinists have made it clear that they intend to block any transaction they do n't like .	norel	2198..2306
" The pilots will be involved in any transaction that takes place around here , " pilot union chairman Frederick C. Dubinsky declared yesterday .	restatement	2307..2448
But whether the pilots can team up with their longtime adversaries , the machinists , is another question .	contrast	2449..2553
The pilots ' Mr. Dubinsky says his union would like majority ownership for employees .	norel	2556..2640
At the very least , the pilots want some form of control over the airline , perhaps through super-majority voting rights .	restatement	2641..2760
On the other hand , the machinists have always opposed majority ownership in principle , saying they do n't think employees should be owners .	norel	2763..2901
Still , in recent days , machinists ' union leaders have shown some new flexibility .	contrast	2904..2985
" We may be able to reach a tradeoff where we can accommodate { the pilot union 's } concerns and ours , " said Brian M. Freeman , the machinists ' financial adviser .	restatement	2986..3144
Mr. Freeman said machinists ' union advisers plan to meet this week to try to draw up a blueprint for some form of recapitalization that could include a special dividend for shareholders , an employee stake and , perhaps , an equity investment by a friendly investor .	norel	3147..3410
If a compromise ca n't be reached , the pilots maintain they can do a transaction without the support of the machinists .	norel	3413..3531
But at this point , that may just be wishful thinking .	contrast	3532..3585
The machinists lobbied heavily against the original bid from UAL pilots and management for the company .	norel	3588..3691
Their opposition helped scare off some Japanese banks .	entrel	3692..3746
The pilots ' insistence on majority ownership also may make the idea of a recapitalization difficult to achieve .	conjunction	3749..3860
" Who wants to be a public shareholder investing in a company controlled by the pilots ' union ? " asks Candace Browning , an analyst at Wertheim Schroeder & Co .	cause	3861..4017
" Who would the board be working for -- the public shareholders or the pilots ? " she adds .	cause	4018..4106
Ms. Browning says she believes a recapitalization involving employee ownership would succeed only if the pilots relent on their demand for control .	norel	4109..4256
She also notes that even if the pilots accept a minority stake now , they still could come back at a later time and try to take control .	conjunction	4257..4392
Another possibility is for the pilots ' to team up with an outside investor who might try to force the ouster of the board through the solicitation of consents .	norel	4395..4554
In that way , the pilots may be able to force the board to approve a recapitalization that gives employees a majority stake , or to consider the labor-management group 's latest proposal .	cause	4555..4739
The group did n't make a formal offer	norel	4742..4778
but instead told UAL 's advisers before the most-recent board meeting that it was working on a bid valued at between $ 225 and $ 240 a share .	contrast	4779..4918
But again , they may need the help of the machinists .	concession	4919..4971
" I think the dynamics of this situation are that something 's got to happen , " said one official familiar with the situation .	norel	4974..5097
The board and UAL 's management , he says , " ca n't go back " to business as usual .	restatement	5098..5176
" The pilots wo n't let them .	cause	5177..5204
