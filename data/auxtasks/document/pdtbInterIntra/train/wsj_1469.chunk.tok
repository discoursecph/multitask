After several years of booming business with China , foreign traders are bracing for the biggest slump in a decade .	root	9..123
The imposition of austerity measures , starting last October , already had begun to pinch	norel	126..213
when the massacre in Tiananmen Square on June 4 and subsequent events tugged the belt far tighter .	synchrony	214..312
Foreign lending has been virtually suspended since then , choking liquidity and hobbling many projects .	asynchronous	313..415
And Beijing has pulled back on domestic loans and subsidies , leaving many domestic buyers and export-oriented plants strapped for cash .	conjunction	416..551
Givaudan Far East Ltd. , a Swiss concern that sells chemicals to shampoo and soap factories in China , typifies the problems .	norel	554..677
Last year 's retrenchment dried up the working capital of Chinese factories .	entrel	678..753
The company 's sales flattened during 1989 's first half .	cause	754..809
The June killings magnified the problems .	norel	812..853
In Canton , Givaudan 's representative office received no orders in June .	conjunction	854..925
At first it attributed the slump to temporary business disruptions	entrel	926..992
but when no orders were logged in August and September , manager Donald Lai became convinced that business would be bad for many months .	comparison	993..1129
" Things have grown worse since June 4 , " Mr. Lai says .	norel	1132..1185
He predicts that sales will drop between 30 % and 40 % from last year 's $ 3 million .	conjunction	1186..1267
The consumer-products and light-industrial sectors are bearing the brunt of China 's austerity measures	norel	1270..1372
and foreign companies such as Givaudan that deal with those industries are being hit the hardest .	conjunction	1373..1471
But in general , all foreign-trading companies are feeling the pinch .	comparison	1472..1540
" The import pie will shrink , " says John Kamm , first vice president of the American Chamber of Commerce in Hong Kong and a China trade specialist .	norel	1543..1688
" On the down side , sales could fall as much as 90 % for some companies ; on the upper side , sales will be flat . "	cause	1689..1799
China 's foreign trade has gone in cycles during the past decade .	entrel	1800..1864
The last time that traders experienced a trough was during 1985-86 , when Beijing imposed tough measures to curb imports and conserve foreign exchange .	entrel	1865..2015
The current trough is expected to be much deeper , because Beijing has cut off domestic funds from factories for the first time to slow inflation .	comparison	2016..2161
In addition , the suspension of loans and export credits from foreign governments and institutions following the June killings have been a big setback .	conjunction	2162..2312
" The freeze on new lending is dealing the single biggest blow to trading , " says Raymond Wong , China manager for Mannesmann AG , a West German machinery-trading company .	norel	2315..2482
Import growth from the year-earlier months slowed to 16 % in July and 7.1 % in August , compared with an average growth rate of 26 % in the first half .	norel	2485..2632
In the first eight months of 1989 , imports grew 21 % , to $ 38.32 billion , down slightly from a growth rate of 23 % a year earlier .	cause	2633..2760
The picture for China 's exports is just as bleak , mainly because of the domestic credit squeeze .	norel	2763..2859
Exports in the first eight months grew only 9 % , to $ 31.48 billion , compared with a growth rate of 25 % a year earlier , according to Chinese customs figures .	restatement	2860..3015
The threat to China 's balance of payments is further aggravated by the plunge in its foreign-exchange reserves , excluding gold holdings .	norel	3018..3154
The reserves dropped for the first time in recent years , to $ 14 billion in June from $ 19 billion in April .	restatement	3155..3261
The trend has prompted Beijing to intensify efforts to curb imports .	norel	3264..3332
In recent weeks , China 's leaders have recentralized trading in wool and scores of chemical products and commodities .	cause	3333..3449
The Ministry of Foreign Economic Relations and Trade set up a special bureau last month to monitor the issue of import and export licenses .	conjunction	3450..3589
Beijing 's periodic clampdowns on imports have taught many trading companies that the best way to get through the drought is by helping China export .	norel	3592..3740
For example , Nissho Iwai Corp. , one of the biggest Japanese trading houses , now buys almost twice as many goods from China as it sells to that country .	instantiation	3741..3892
Three years ago , the ratio was reversed .	contrast	3893..3933
But the strategy is n't helping much this time .	norel	3936..3982
" Both sectors of imports and exports look just as bad , " says Masahiko Kitamura , general manager of Nissho Iwai 's Canton office .	cause	3983..4110
He expects the company 's trading business to drop as much as 40 % this year .	cause	4111..4186
For a short time after June 4 , it appeared that the trade picture would remain fairly bright .	norel	4189..4282
Many foreign trading offices in Hong Kong were swamped with telexes and telephone calls from Chinese trade officials urging them not to sever ties .	synchrony	4283..4430
Even the Bank of China , which normally took weeks to process letters of credit , was settling the letters at record speed to dispel rumors about the bank 's financial health .	conjunction	4431..4603
But when foreign traders tried to do business , they discovered that the eagerness of Chinese trade officials was just a smokescreen .	comparison	4606..4738
The suspension of foreign loans has weakened the buying power of China 's national trading companies , which are among the country 's biggest importers .	cause	4739..4888
Business is n't any better on the provincial or municipal level , foreign traders say .	norel	4891..4975
Shanghai Investment & Trust Co. , known as Sitco , is the city 's main financier for trading business .	norel	4976..5075
Sitco had customarily tapped the Japanese bond market for funds	entrel	5076..5139
but it ca n't do that any longer .	comparison	5140..5173
Foreign traders say the company is strapped for cash .	cause	5174..5227
" It has difficulties paying its foreign debts , " says a Hong Kong executive who is familiar with Sitco 's business .	norel	5230..5343
" How can it make available funds for purchases ? "	cause	5344..5392
Foreign traders also say many of China 's big infrastructural projects have been canceled or postponed because of the squeeze on domestic and foreign credit .	norel	5395..5551
Albert Lee , a veteran trader who specializes in machinery sales , estimates that as many as 70 % of projects that had obtained approval to proceed have been canceled in recent months .	restatement	5552..5733
" There are virtually no new projects , and that means no new business for us , " he says .	cause	5734..5820
Even when new lending resumes , foreign exchange would still be tight because Beijing will likely try to rein in foreign borrowing , which has grown between 30 % and 40 % in the past few years .	norel	5823..6012
And foreign creditors are likely to be more cautious about extending new loans because China is nearing a peak repayment period as many loans start falling due in the next two to five years .	conjunction	6013..6203
Another reason for the intensity of the trade problems is that Beijing has extended the current clampdown on imports beyond the usual target of consumer products to include steel , chemical fertilizers and plastics .	norel	6206..6420
These have been among the country 's leading imports , particularly last year when there were shortages that led many traders to buy heavily and pay dearly .	asynchronous	6421..6575
But the shortages also spawned rampant speculation and spiraling prices .	comparison	6578..6650
To stem speculation , Beijing imposed ceiling prices that went into effect earlier this year .	cause	6651..6743
Traders who had bought the goods at prices above the ceiling do n't want to take a loss on resales and are holding onto their stock .	cause	6744..6875
The resulting stockpiling has depressed the market .	cause	6876..6927
But Beijing ca n't cut back on such essential imports as raw materials for too long without hampering the country 's export business .	norel	6930..7061
Mr. Kamm , the China trade expert , estimates that as much as 50 % of Guangdong 's exports is made up of processed imported raw materials .	cause	7062..7196
