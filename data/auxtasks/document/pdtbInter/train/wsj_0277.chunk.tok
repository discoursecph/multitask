Buying 51 % of Rockefeller Group Inc. is right up Mitsubishi Estate Co. 's alley in one sense : The huge Japanese real estate company is entering a long-term relationship with a similarly conservative U.S. owner of tony urban property .	O	9..242
But in another sense , the $ 846 million purchase is uncharacteristically nervy , industry analysts say .	O	245..346
The usually cautious giant will become the majority owner of the company that owns New York 's beloved Rockefeller Center at a time when tensions over Japanese purchases of U.S. property are at an all-time high .	O	347..557
Officials of Rockefeller Group and Mitsubishi Estate prefer to focus on the affinities , nearly dismissing the threat of a backlash from the U.S. public .	B-instantiation	560..712
" We think there will be positive as well as negative reactions , " says Raymond Pettit , senior vice president and chief financial officer of Rockefeller Group .	I-instantiation	713..870
" On balance , we think it will be positive . "	B-contrast	871..914
But some Japanese government officials and businessmen worry that the prominent purchase is just the sort of deal that should be avoided for the time being .	I-contrast	917..1073
In particular , they criticize the timing , coming as it does on the heels of Sony Corp. 's controversial purchase of Columbia Pictures Entertainment Inc .	O	1074..1226
" Officially , yes , we encourage the free flow of direct investment , " says a Foreign Ministry official .	O	1229..1330
" But they did n't have to choose this particular moment . "	O	1331..1387
During the past year , government officials and leading business organizations have repeatedly urged Japanese companies to refrain from flashy real estate purchases in the	O	1390..1560
Since the mid-1980s , Japan 's other major real estate purchases in the U.S. include Dai-Ichi Seimei America Corp. 's $ 670 million purchase of an office building at 153 East 53rd St. in Manhattan in 1987 and Mitsui Fudosan Inc. 's $ 610 million purchase of the Exxon Building , part of Rockefeller Center , in 1986 .	B-conjunction	1563..1873
In Los Angeles , Arco Plaza was sold to Shuwa Corp. for $ 620 million in 1986 , and Sumitomo Life Insurance Co. paid $ 300 million for Atlanta 's IBM Tower last year .	I-conjunction	1874..2035
Altogether , annual Japanese investment in U.S. commercial real estate grew from about $ 1.3 billion in 1985 to about $ 7.1 billion in 1988 .	O	2038..2175
Many Japanese companies have taken the warnings by the country 's leaders to heart and sought development partnerships rather than landmark properties .	O	2178..2328
Critics say Mitsubishi Estate 's decision to buy into Rockefeller reflects the degree to which companies are irritated by the pressure to act for the good of Japan .	O	2329..2492
" Those who have no money and are n't buying think it 's right to refrain , but those with money who want to buy for themselves pay no attention , " says an official of the Japan-U.S . Business Council .	O	2493..2688
But to Mitsubishi Estate , the acquisition has just the elements that should win support from both sides .	O	2691..2795
First of all , it is a friendly acquisition in which Rockefeller sought out Mitsubishi Estate and asked it to buy a majority share .	O	2796..2926
Secondly , the two companies found a similarity in their business and development philosophies and intend to cooperate in a range of activities from real estate to telecommunications .	B-list	2927..3109
Finally , Mitsubishi Estate has no plans to interfere with Rockefeller 's management beyond taking a place on the board .	I-list	3110..3228
" We 'll continue to work with them , in keeping with the reputation of the company , and we 'll rely very much on their leadership , " says Mitsubishi Estate President Jotaro Takagi .	O	3231..3407
Rockefeller may well have found its match in Mitsubishi Estate , a company of long history , strong government ties and sound resources .	B-instantiation	3410..3544
In asset terms , Mitsubishi Estate is the largest real estate firm in Japan .	B-restatement	3545..3620
The core of its holdings is 190,000 square meters of incredibly expensive property in the Marunouchi district , the business and financial center of Tokyo , often jokingly called " Mitsubishi Village . "	I-restatement	3621..3819
The Mitsubishi family company acquired that property from the government some 100 years ago when it was a portion of samurai residential land running from the moat of the Imperial Palace east toward the hodgepodge of tiny shops and twisted alleys that made up the merchants ' district .	O	3822..4106
At the time , Japan had just opened its doors to the world after about 250 years of isolation and needed a Western-style business center .	O	4107..4243
Mitsubishi built the government 's dream development , the story goes , in exchange for the official decision to locate Tokyo 's central railway station there .	O	4244..4399
That was just an early step in a relationship with government that has earned the Mitsubishi group the dubious moniker of " seisho , " literally government-business , a title that has the pejorative connotation of doing the government 's bidding , but also suggests the clout inherent in maintaining such close ties .	O	4402..4712
Mitsubishi Estate is one of the dozens of companies in today 's Mitsubishi group .	O	4715..4795
It 's known for its cautiousness in part because it has had little need for bold overseas ventures : In the year ended March 31 , 57.4 % of its total revenue came from office building management .	O	4796..4987
Its earnings can rise 10 % to 12 % annually simply from the natural turnover of tenants and automatic rent increases , says Graeme McDonald , an industry analyst at James Capel Pacific Ltd .	O	4988..5173
For the latest fiscal year , the company 's net income jumped a robust 19 % to 35.5 billion yen ( $ 250.2 million ) .	O	5174..5284
For Mitsubishi Estate , the Rockefeller purchase will catapult it firmly into the overseas real estate business , the one area where it has lagged notably behind Japanese competitors such as Mitsui , which had purchased the Exxon Building .	O	5287..5523
" Japanese companies need to invest in overseas real estate for diversification , " says Yoshio Shima , an industry analyst at Goldman Sachs ( Japan ) Corp .	O	5526..5676
Rockefeller is n't the first overseas purchase for Mitsubishi Estate -- it has already played a leading role in designing Los Angeles 's Citicorp Plaza .	B-contrast	5679..5829
But the Rockefeller investment is its largest .	I-contrast	5830..5876
Nonetheless , it will barely make a dent in Mitsubishi Estate 's finances , analysts say .	O	5877..5963
Mitsubishi Estate has n't decided how it will raise the funds for the purchase , which are due in cash next April , but the Marunouchi holdings alone are estimated to have a market value of as much as 10 trillion yen to 11 trillion yen .	O	5966..6199
Moreover , as a member of the Mitsubishi group , which is headed by one of Japan 's largest banks , it is sure to win a favorable loan .	O	6200..6331
Analysts say the company also could easily issue new convertible bonds or warrants .	O	6332..6415
Meanwhile , at home , Mitsubishi has control of some major projects .	B-instantiation	6418..6484
It is the largest private-sector landowner of the Minato-Mirai 21 project , a multibillion-yen development in the port city of Yokohama , about an hour outside Tokyo .	B-entrel	6485..6649
The project is one of a select group of public projects opened to U.S. firms under a U.S.-Japan construction trade agreement reached last year .	B-entrel	6650..6793
The centerpiece of that complex , the Landmark Tower , will be Japan 's tallest building when it is completed in 1993 .	I-entrel	6794..6909
Mitsubishi is also pushing ahead with a controversial plan to redevelop Marunouchi into a business center of high-tech buildings , a project budgeted for 30 years and six trillion yen .	O	6912..7095
