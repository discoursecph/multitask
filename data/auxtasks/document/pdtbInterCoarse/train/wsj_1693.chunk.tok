New York Times Co. 's third-quarter earnings report is reinforcing analysts ' belief that newspaper publishers will be facing continued poor earnings comparisons through 1990 .	O	9..183
The publisher was able to register soaring quarter net income because of a onetime gain on the sale of its cable-TV system .	B-comparison	186..309
However , operating profit fell 35 % to $ 16.4 million .	B-expansion	310..362
The decline reflected the expense of buying three magazines , lower earnings from the forest-products group , and what is proving to be a nagging major problem , continued declines in advertising linage at the New York Times , the company 's flagship daily newspaper .	I-expansion	363..625
In composite trading on the American Stock Exchange , New York Times closed at $ 28.125 a share , down 37.5 cents .	O	628..739
Analysts said the company 's troubles mirror those of the industry .	O	742..808
Retail advertising , which often represents half of the advertising volume at most daily newspapers , largely is n't rebounding in the second half from extended doldrums as expected .	O	809..988
At the same time , newspapers are bedeviled by lagging national advertising , especially in its financial component .	B-expansion	989..1103
Dow Jones & Co. recently reported net fell 9.9 % , a reflection , in part , of continued softness in financial advertising at The Wall Street Journal and Barron 's magazine .	I-expansion	1104..1272
" We expect next year to be a fairly soft year in newspaper-industry advertising , " said John Morton , an analyst for Lynch , Jones & Ryan .	O	1275..1410
" Next year , earnings will hold steady , but we just do n't see a big turnaround in the trend in advertising . "	O	1411..1518
John S. Reidy , an analyst for Drexel Burnham Lambert Inc. , said , " The Times faces the same problem of other publishers : linage is down .	O	1521..1656
It will be hard to do handstands until real linage starts heading back up . "	O	1657..1732
In the quarterly report , Arthur Ochs Sulzberger , New York Times Co. chairman and chief executive officer , said negative factors affecting third-quarter earnings will continue .	B-entrel	1735..1910
Analysts agreed with company expectations that operating profit will be down this year and in 1990 .	I-entrel	1911..2010
Mr. Sulzberger said the scheduled opening of a new color-printing plant in Edison , N.J. , in 1990 would involve heavy startup and depreciation costs .	O	2011..2159
" With the Edison plant coming on line next summer , the Times is facing some tough earnings comparison in the future , " said Peter Appert , an analyst with C.J. Lawrence , Morgan Grenfell .	O	2162..2346
" But many newspapers are facing similar comparisons . "	O	2347..2400
The sale of the company 's cable franchise brought an after-tax gain of $ 193.3 million , part of which will be used to reduce debt .	B-expansion	2403..2532
The company also has a stock-repurchase plan .	I-expansion	2533..2578
Analysts said they were impressed by the performance of the company 's newspaper group , which consists of the Times , 35 regional newspapers and a one-third interest in the International Herald Tribune ; group operating profit for the quarter increased slightly to $ 34.9 million from $ 34.5 million on flat revenue .	O	2581..2892
Drexel Burnham 's Mr. Reidy pointed out that " profits held up in a tough revenue environment .	O	2895..2987
That 's a good sign when profits are stable during a time revenue is in the trough .	O	2988..3070
