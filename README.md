# README #

Data for multitask experiments: sequence prediction on RST DT, chunking for PDTB

### Data ###

* pdtb_chunk/
    * inter sentential chunks
    * test/ and train/
    * files .chunk.tok: gold tokenization from PTB 
    * Format: text\tlabel\tspan
        * text: tokens split on space
        * label: B-relation or I-relation or O
        * span: span of the unit in the original raw file
* rstdt_constituent/
    * constituent form, 16 discourse relations (+ev nuclearity information)
    * test/ and train/
    * files:
        * .seq.const relations without nuclearity information
        * .seq.nuc.const relations with nuclearity information (eg NN-label, NS-label or SN-label)
    * Format: text\tlabel
        * text: for now no pre tokenization
        * label: for ex. : (relation(relation or relation)) or (NN-relation(SN-relation or NS-relation)
* rstdt_dependency/
    * dependency form, 16 discourse relations (+ev nuclearity information)
    * test/ and train/
    * files:
        * .seq.dep relations without nuclearity information
        * .seq.nuc.dep relations with nuclearity information (eg NN-label, NS-label or SN-label)
    * Format: text\tlabel
        * text: for now no pre tokenization
        * label: for ex. : root or -1_sameunit or -5_NS-elaboration (with -X index of the head of the unit)
            * root: first nucleus EDU in the document
            * left headed: if an argument contains more than one EDU, we choose the most left nucleus (ie first EDU of the saliency set)
 

### TODO ###

* make evaluation script
* Add intra sentential chunk for PDTB
* Add gold tokenization for RST DT
* Modify LSTM MT code

### EXPERIMENTS ###

* start with LSTM MT
* check if trees are well formed (ie span metrics)
* turn the gold into dependency tree (check Li metrics)
* compare both using dependency metrics and then compare to soa with constituent trees
* use the relation mapping proposed by Webber (ISO paper) 