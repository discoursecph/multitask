Despite one of the most devastating droughts on record, net cash income in the Farm Belt rose to a new high of $59.9 billion last year.	O	9..144
The previous record was $57.7 billion in 1987, according to the Agriculture Department.	O	147..234
Net cash income -- the amount left in farmers' pockets after deducting expenses from gross cash income -- increased in 33 states in 1988, as the drought cut into crop yields and drove up commodity prices, the department's Economic Research Service reported yesterday.	O	237..504
Most of those states set farm income records.	O	505..550
The worst crop damage occurred in the Midwestern Corn Belt and the northern Great Plains.	B-entrel	553..642
What saved many farmers from a bad year was the opportunity to reclaim large quantities of grain and other crops that they had "mortgaged" to the government under price-support loan programs.	I-entrel	643..834
With prices soaring, they were able to sell the reclaimed commodities at "considerable profit," the agency's 240-page report said.	O	835..965
In less parched areas, meanwhile, farmers who had little or no loss of production profited greatly from the higher prices.	O	968..1090
To the surprise of some analysts, net cash income rose in some of the hardest-hit states, including Indiana, Illinois, Nebraska and the Dakotas.	B-entrel	1093..1237
Analysts attributed the increases partly to the $4 billion disaster-assistance package enacted by Congress.	I-entrel	1238..1345
Last year's record net cash income confirms the farm sector's rebound from the agricultural depression of the early 1980s.	B-conjunction	1348..1470
It also helps explain the reluctance of the major farm lobbies and many lawmakers to make any significant changes in the 1985 farm program next year.	I-conjunction	1471..1620
Commodity prices have been rising in recent years, with the farm price index hitting record peaks earlier this year, as the government curtailed production with land-idling programs to reduce price-depressing surpluses.	O	1623..1842
At the same time, export demand for U.S. wheat, corn and other commodities strengthened, said Keith Collins, a department analyst.	O	1843..1973
Farmers also benefited from strong livestock prices, as the nation's cattle inventory dropped close to a 30-year low.	O	1974..2091
"All of these forces came together in 1988 to benefit agriculture," Mr. Collins said.	O	2094..2179
California led the nation with $6.5 billion in net cash income last year, followed by Texas, $3.9 billion; Iowa, $3.4 billion; Florida, $3.1 billion; and Minnesota, $2.7 billion.	B-entrel	2182..2360
Iowa and Minnesota were among the few major farm states to log a decline in net cash income.	I-entrel	2361..2453
Despite federal disaster relief, the drought of 1988 was a severe financial setback for an estimated 10,000 to 15,000 farmers, according to the department.	B-restatement	2456..2611
Many lost their farms.	I-restatement	2612..2634
Department economists don't expect 1989 to be as good a year as 1988 was.	B-restatement	2637..2710
Indeed, net cash income is likely to fall this year as farm expenses rise and government payments to farmers decline.	I-restatement	2711..2828
At the same time, an increase of land under cultivation after the drought has boosted production of corn, soybeans and other commodities, causing a fall in prices that has been only partly cushioned by heavy grain buying by the Soviets.	O	2829..3065
Last year, government payments to farmers slipped to less than $14.5 billion from a record $16.7 billion in 1987.	B-contrast	3068..3181
Payments are expected to range between $9 billion and $12 billion this year.	I-contrast	3182..3258
