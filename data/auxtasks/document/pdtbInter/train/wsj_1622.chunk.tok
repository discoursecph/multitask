The " seismic " activity of a financial market bears a resemblance to the seismic activity of the earth .	B-restatement	9..111
When things are quiet ( low volatility ) , the structures on which markets stand can be relatively inefficient and still perform their functions adequately .	B-contrast	112..265
However , when powerful forces start shaking the market 's structure , the more " earthquake-resistant " it is , the better its chance for survival .	I-contrast	266..408
America 's financial markets do not yet have all the required modern features required to make them fully " aftershock-resistant . "	O	411..539
Investors lack equal access to the markets ' trading arena and its information .	O	540..618
That structural lack is crucial because investors are the only source of market liquidity .	B-conjunction	621..711
And liquidity is what markets need to damp quakes and aftershocks .	B-contrast	712..778
In today 's markets , specialists ( on the New York Stock Exchange ) and " upstairs " market makers ( in the over-the-counter market ) are the only market participants allowed to play a direct role in the price-determination process .	B-cause	779..1004
When they halt trading , all market liquidity is gone .	B-conjunction	1005..1058
And when any component of the market -- cash , futures or options -- loses liquidity , the price discovery system ( the way prices are determined ) becomes flawed or is lost entirely for a time .	I-conjunction	1059..1249
Last Friday the 13th ( as well as two years ago this week ) the markets became unlinked .	O	1252..1338
When that happened , " seismic " tremors of fear -- much like the shock waves created by an earthquake -- coursed through the market and increased the market 's volatility .	O	1339..1507
Lack of important , needed information can cause fear .	B-entrel	1508..1561
Fear is the father of panic .	B-entrel	1562..1590
Panic frequently results in irrational behavior .	B-conjunction	1591..1639
And in financial markets , irrational behavior is sometimes translated into catastrophe .	I-conjunction	1640..1727
When market tremors start , it is crucial that as much information about transaction prices and the supply-demand curve ( buy and sell orders at various prices ) be made available to all , not just to market makers .	O	1730..1941
Because of a lack of information and access , many investors -- including the very ones whose buying power could restore stability and damp volatility -- are forced to stand on the sidelines when they are most needed , because of their ignorance of important market information .	B-cause	1942..2218
To add aftershock-damping power to America 's markets , a modern , electronic trading system should be implemented that permits equal access to the trading arena ( and the information that would automatically accompany such access ) by investors -- particularly institutional investors .	I-cause	2219..2500
Contrary to some opinions , the trading activities of specialists and other market makers do not provide liquidity to the market as a whole .	B-contrast	2503..2642
What market makers provide is immediacy , a very valuable service .	B-contrast	2643..2708
Liquidity is not a service .	I-contrast	2709..2736
It is a market attribute -- the ability to absorb selling orders without causing significant price changes in the absence of news .	O	2737..2867
Market makers buy what investors wish to sell ; their business is reselling these unwanted positions as quickly as possible to other investors , and at a profit .	O	2870..3029
As a result , while any one customer may purchase immediacy by selling to a market maker ( which is micro-liquidity for the investor ) , the market as a whole remains in the same circumstances it was before the transaction : The unwanted position is still an unwanted position ; only the identity of the seller has changed .	O	3030..3347
In fact it can be argued that increasing capital commitments by market makers ( a result of some post-1987 crash studies ) also increases market volatility , since the more securities are held by market makers at any given time , the more selling pressure is overhanging the market .	O	3348..3626
In an open electronic system , any investor wishing to pay for real-time access to the trading arena through a registered broker-dealer would be able to see the entire supply-demand curve ( buy and sell orders at each price ) entered by dealers and investors alike , and to enter and execute orders .	B-cause	3629..3924
Current quotations would reflect the combined financial judgment of all market participants -- not just those of intermediaries who become extremely risk-averse during times of crisis .	I-cause	3925..4109
Investors and professionals alike would compete on the level playing field Congress sought and called a " national market system " ( not yet achieved ) almost 15 years ago when it passed the Securities Reform Act of 1975 .	O	4112..4329
Last Friday 's market gyrations did not result in severe " aftershocks . "	O	4332..4402
Were we smart or just lucky ?	O	4403..4431
I 'm not certain .	B-contrast	4432..4448
But I am sure we need to maximize our " earthquake " protection by making certain that our market structures let investors add their mighty shock-damping power to our nation 's markets .	I-contrast	4449..4631
Mr. Peake is chairman of his own consulting company in Englewood , N.J .	O	4634..4704
