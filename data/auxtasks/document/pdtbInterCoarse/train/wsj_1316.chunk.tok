Ingersoll Publications Co. agreed to buy the New Haven Register in a transaction valued at $ 275 million from Goodson Newspaper Group Inc .	O	9..146
As part of the agreement , Goodson also terminated the contract under which Ingersoll manages Goodson 's 66 newspapers , ending a long association between the two companies that has turned increasingly bitter recently .	O	149..364
Goodson has accused Ingersoll of paying less attention to its properties and more to such ventures as the recent launch of the St. Louis Sun .	O	365..506
Under the terms of the accord , Ingersoll will pay about $ 255 million for the Register , a daily that Goodson bought for about $ 170 million in 1986 .	B-expansion	509..655
Goodson will pay the additional $ 20 million in settlement of the management contract .	I-expansion	656..741
Goodson also announced that it hired the former president and senior vice president of Ingersoll to run the Goodson papers .	B-entrel	742..865
Both executives left the company after clashes with Chairman Ralph Ingersoll Jr .	I-entrel	866..946
Goodson , which is based here , will use part of the proceeds to pay down debt associated with its purchase of the Morristown Daily Record for $ 155 million in 1987 .	B-entrel	949..1111
The New Jersey paper , like the New Haven , Conn. , paper , was purchased by Ingersoll on Goodson 's behalf as part of the management contract .	I-entrel	1112..1250
Industry analysts have said that the purchase price for the paper was too high , causing a strain on Goodson 's finances .	O	1251..1370
Investment bankers familiar with the company said Goodson is seeking a new bank credit line of $ 190 million and may have to sell additional newspapers .	O	1371..1522
David N. Hurwitz , president and chief operating officer of Goodson , said in a telephone interview that the company does n't currently have any plans to sell additional newspapers .	O	1525..1703
Goodson said David Carr , former president of Ingersoll Publications , and Ray Cockburn , former senior vice president , would head the new in-house management team at Goodson , which had revenue of $ 225 million in 1988 .	O	1706..1921
The association between the two companies stretches back thirty years to a friendship between television producer Mark Goodson and Ingersoll founder Ralph Ingersoll .	O	1924..2089
The latter 's son , Ralph Ingersoll Jr. , took over the company and has been managing the Goodson properties and acting as an agent in the purchase of newspapers for Goodson .	B-comparison	2090..2261
But in recent years , Mr. Ingersoll began focusing more on expanding his own newspaper empire in partnership with investment banking firm Warburg , Pincus & Co .	B-entrel	2262..2420
Ingersoll has 28 dailies and 200 other non-daily papers in the U.S. and Europe .	I-entrel	2421..2500
The company said its revenue will exceed $ 750 million this year .	O	2501..2565
Ingersoll President Robert M. Jelenic said in a statement that the company is " delighted by the conclusion of the Goodson relationship " and will be able to " concentrate all our energies " on Ingersoll 's own papers .	B-comparison	2568..2781
Mr. Goodson , in his own statement , was less upbeat , saying " unfortunately over the past few years , it has become increasingly clear that Ralph and I have different agendas , " and that he feels " more comfortable with a management team whose sole interest and responsibility is in the Goodson papers .	I-comparison	2782..3079
