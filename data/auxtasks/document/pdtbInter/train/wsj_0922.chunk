Western European leaders who favor speedy economic and monetary union are adding a new argument to their arsenal: the dizzying political changes under way in Eastern Europe.	O	9..182
French President Francois Mitterrand, European Community Commission President Jacques Delors, Spanish Prime Minister Felipe Gonzalez and others have begun linking the rapid changes in the East to the need to speed up changes in the West.	B-restatement	185..422
They are stressing that the best way for the West to help the East is to move faster towards Western European economic and monetary unity.	B-entrel	423..561
This would make the market-oriented system more attractive to Eastern countries, they argue, and allow greater economic aid and technological know-how to flow from West to East.	I-entrel	562..739
"The only response to the challenge being presented to us by the East," Mr. Mitterrand told the European Parliament in Strasbourg yesterday, "is to reinforce and accelerate the union and cohesion of the European Community."	O	742..965
Mr. Mitterrand proposed that a conference be convened next fall to write a new treaty for the EC allowing a European central bank, and that the treaty be ratified by 1992.	B-conjunction	966..1137
Mr. Mitterrand also proposed a separate "Bank for Europe" that would channel development money to the East.	I-conjunction	1138..1245
One basis for linking change in the East and change in the West is the notion that integrating 110 million Eastern Europeans with 320 million Western Europeans is primarily the task of Europeans, despite the U.S.'s obvious strategic and economic interest.	O	1248..1503
Says a European strategist: "The U.S. tends to look at Eastern Europe {not including the Soviet Union} as Europe looks at Latin America: important but far away.	O	1504..1664
But for us in Western Europe, these are Europeans next door."	O	1665..1726
A reintegrated Europe implies big changes in 40-year-old military and economic policies.	O	1729..1817
There is likely to be a natural division of labor, says Francois Heisbourg, director of the International Institute for Strategic Studies in London, with the U.S. more engaged in strategic issues with the Soviet Union, and Western Europe more involved with specific aid measures for the East.	O	1818..2110
The designation at July's economic summit of major industrialized nations of the EC Commission as coordinator of Western aid to Poland and Hungary was a first step.	B-entrel	2113..2277
In part, this division is dictated by economics: West Germany is a net exporter of capital while the U.S. isn't.	I-entrel	2278..2390
While American aid efforts have been limited by budget problems, yesterday France announced a three-year, four billion French franc ($650 million) aid plan for Poland.	O	2391..2558
Despite sudden changes in the strategic equation, some Western European leaders, especially British Prime Minister Margaret Thatcher, remain skeptical about European political and economic unity, and are unlikely to let East-West concerns change their minds.	B-contrast	2561..2819
But British analysts are beginning to link the issues.	I-contrast	2820..2874
"We need a Western Ostpolitik," says John Roper, of the Royal Institute of International Affairs in London, referring to West Germany's longstanding policy of a diplomatic opening to the East.	O	2877..3069
"For Poland and Hungary we need to think about a stick-and-carrot economic approach that would force them to price things realistically in return for removing all our tariff barriers."	O	3070..3254
He notes that the Marshall Plan of U.S. aid to Europe "didn't just throw money at post-war Europe, it also liberalized and opened up those markets."	O	3255..3403
The French analysis goes further.	B-restatement	3406..3439
"Most of the West's leaders have finally concluded that we all want perestroika {Soviet leader Mikhail Gorbachev's policy of economic restructuring} to succeed," says Hubert Vedrine, security adviser to Mr. Mitterrand.	I-restatement	3440..3658
"But they haven't yet drawn the operational policy conclusions."	O	3659..3723
He adds that with communism collapsing and Mr. Gorbachev scrambling to rejuvenate the Soviet economy, "Our interest lies in a controlled transformation, a contained nuclear reaction, so we need to help him, and not just with words."	O	3726..3958
Managing change, he adds, will require a lot more aid and a prominent role for the EC, especially in dealing with the question of German reunification.	O	3959..4110
Thierry de Montbrial, director of the French Institutue for International Relations in Paris, says it isn't clear what, exactly, West Germany wants.	O	4113..4261
Any push for a Gorbachev vision of a "common European home," implying the eventual dissolution of the EC, a Soviet-German partnership and the withdrawal of U.S. forces, "would be a very, very serious problem," he says.	O	4262..4480
He doubts a Bismarckian super state will emerge that would dominate Europe, but warns of "a risk of profound change in the heart of the European Community from a Germany that is too strong, even if democratic."	O	4481..4691
He adds: "We, and the rest of the EC have to talk to the Germans, now, frankly and raise these future risks with them."	O	4694..4813
While many commentators, particularly French ones, worry that hasty and emotional reaction to the changes in the East might lead to dangerous pressures for a denuclearized Europe or the speeded-up withdrawal of American troops, Mr. Roper in London sees a more positive scenario.	O	4816..5094
"There seems to be a message from Moscow there's a deal on offer," he says.	O	5095..5170
"They want reassurance we won't try to undermine or destroy the Warsaw Pact... .	O	5171..5251
In return, the U.K. and France could keep their nuclear weapons.	O	5252..5316
He adds: "Once both sides feel comfortable, it should be that much easier to make more progress toward the economic and social reforms that are now starting in the East.	O	5317..5486
