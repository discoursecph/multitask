The Transportation Department , responding to pressure from safety advocates , took further steps to impose on light trucks and vans the safety requirements used for automobiles .	O	9..185
The department proposed requiring stronger roofs for light trucks and minivans , beginning with 1992 models .	B-expansion	188..295
It also issued a final rule requiring auto makers to equip light trucks and minivans with lap-shoulder belts for rear seats beginning in the 1992 model year .	B-expansion	296..453
Such belts already are required for the vehicles ' front seats .	I-expansion	454..516
" Today 's action , " Transportation Secretary Samuel Skinner said , " represents another milestone in the ongoing program to promote vehicle occupant safety in light trucks and minivans through its extension of passenger car standards . "	O	519..750
In September , the department had said it will require trucks and minivans to be equipped with the same front-seat headrests that have long been required on passenger cars .	O	753..924
The Big Three auto makers said the rule changes were n't surprising because Bush administration officials have long said they planned to impose car safety standards on light trucks and vans .	O	927..1116
Safety advocates , including some members of Congress , have been urging the department for years to extend car-safety requirements to light trucks and vans , which now account for almost one-third of all vehicle sales in the U.S.	O	1119..1346
They say that many vehicles classed as commercial light trucks actually carry more people than cargo and therefore should have the same safety features as cars .	O	1347..1507
They did n't have much luck during the Reagan administration .	O	1510..1570
" But now , there seems to be a fairly systematic effort to address the problem , " said Chuck Hurley , vice president of communications for the Insurance Institute for Highway Safety .	O	1571..1750
" We 're in a very different regulatory environment . "	O	1751..1802
Sen. John Danforth ( R. , Mo . ) praised the department 's actions , noting that rollover crashes account for almost half of all light-truck deaths .	O	1803..1945
" We could prevent many of these fatalities with minimum roof-crush standards , " he said .	O	1946..2033
Sen. Danforth and others also want the department to require additional safety equipment in light trucks and minivans , including air bags or automatic seat belts in front seats and improved side-crash protection .	O	2036..2248
The department 's roof-crush proposal would apply to vehicles weighing 10,000 pounds or less .	B-entrel	2251..2343
The roofs would be required to withstand a force of 1.5 times the unloaded weight of the vehicle .	B-expansion	2344..2441
During the test , the roof could n't be depressed more than five inches .	I-expansion	2442..2512
In Detroit , a Chrysler Corp. official said the company currently has no rear-seat lap and shoulder belts in its light trucks , but plans to begin phasing them in by the end of the 1990 model year .	O	2515..2710
He said Chrysler fully expects to have them installed across its light-truck line by the Sept. 1 , 1991 , deadline .	O	2711..2824
Chrysler said its trucks and vans already meet the roof-crush resistance standard for cars .	O	2825..2916
John Leinonen , executive engineer of Ford Motor Co. 's auto-safety office , said Ford trucks have met car standards for roof-crush resistance since 1982 .	O	2919..3071
Ford began installing the rear-seat belts in trucks with its F-series Crew Cab pickups in the 1989 model year .	B-expansion	3072..3182
The new Explorer sport-utility vehicle , set for introduction next spring , will also have the rear-seat belts .	I-expansion	3183..3292
Mr. Leinonen said he expects Ford to meet the deadline easily .	O	3293..3355
