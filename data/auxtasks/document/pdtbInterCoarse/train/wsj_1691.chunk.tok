What if it happened to us ?	O	9..35
In the wake of the earthquake in California and the devastation of Hurricane Hugo , many companies in disaster-prone areas are pondering the question of preparedness .	O	38..203
Some , particularly in West Coast earthquake zones , are dusting off their evacuation plans , checking food stocks and reminding employees of what to do if emergency strikes .	O	204..375
Others say they feel confident that steps they 've already taken would see them through a disaster .	O	376..474
Preparedness involves more than flashlights and fire alarms these days .	B-expansion	477..548
Some big companies have teams of in-house experts focusing on safety and business resumption .	B-expansion	549..642
Many companies in the path of potential disaster have set up contingency offices in safe regions , hoping they can transport employees there and resume operations quickly .	B-contingency	643..813
That means making sure that copies of vital computer software and company records are out of harm 's way .	I-contingency	814..918
Some businesses -- like Disneyland -- claim that even if they became isolated in a crisis , they would be able to feed and care for their people for as long as five days .	O	921..1090
" Self-sufficiency has to be the cornerstone of your plan , " says Stephanie Masaki-Schatz , manager of corporate emergency planning at Atlantic Richfield Co. in Los Angeles .	O	1093..1263
" If you do n't save your critical people , you wo n't be able to bring up your vital business functions . "	O	1264..1366
Although ARCO 's head office , more than 300 miles from the epicenter , was n't affected by this week 's tremors , Ms. Masaki-Schatz used the occasion to distribute a three-page memo of " Earthquake Tips " to 1,200 ARCO employees .	O	1369..1591
" You need to capitalize on these moments when you have everyone 's attention , " she says .	O	1594..1681
" It was a good reminder that we all need to prepare prior to an event . "	O	1682..1753
The ARCO memo urges employees to keep certain supplies at work , such as solid shoes and " heavy gloves to clear debris . "	B-expansion	1756..1875
It also recommends that employees be aware of everyday office items that could be used for emergency care or shelter .	I-expansion	1876..1993
Among the suggestions : Pantyhose and men 's ties could be used for slings , while removable wooden shelves might aid in " breaking through office walls . "	O	1994..2144
ARCO maintains an office in Dallas that would take over if payroll operations in Pasadena were disrupted .	B-expansion	2147..2252
Two months ago the company set up a toll-free number , based outside California , to handle queries from employees about when they should report back to work after an earthquake or other disaster .	I-expansion	2253..2447
The ARCO plan takes into account such details as which aspects of business are busier at certain times of the year .	B-contingency	2450..2565
This way , depending on when a quake might strike , priorities can be assigned to departments that should be brought back on line first .	I-contingency	2566..2700
At Hewlett-Packard Co. , the earthquake came just as the company was reviewing its own emergency procedures .	O	2703..2810
" We were talking about scheduling a practice drill for November , " says Joan Tharp , a spokeswoman .	O	2811..2908
" Then we had a real one in the afternoon . "	O	2909..2951
The Palo Alto , Calif. , computer maker scrambled to set up a special phone line to tell manufacturing and support staff to stay home Wednesday .	O	2954..3096
Sales and service employees were asked to report to work to help Bay area clients who called with computer problems .	B-expansion	3097..3213
Hewlett-Packard also called in its systems experts to restore its own computer operations .	I-expansion	3214..3304
" That means we can accept orders " and begin getting back to normal , says Ms. Tharp .	O	3305..3388
Prompted by an earlier California earthquake , as well as a fire in a Los Angeles office tower , Great Western Bank in the past year hired three emergency planners and spent $ 75,000 equipping a trailer with communications gear to serve as an emergency headquarters .	O	3391..3654
Although officials of the savings and loan , a unit of Great Western Financial Corp. , used some of their new plans and equipment during this week 's quake , they still lost touch for more than 24 hours with 15 branches in the affected areas , not knowing if employees were injured or vaults were broken open .	O	3657..3961
" Some people flat out did n't know what to do , " says Robert G. Lee , vice president for emergency planning and corporate security at Great Western .	O	3964..4109
As it turned out , bank employees were n't hurt and the vaults withstood the jolts .	O	4112..4193
Still , says Mr. Lee : " We need to educate people that they need to get to a phone somehow , some way , to let someone know what their status is . "	O	4194..4336
Some companies are confident that they 're prepared .	B-expansion	4339..4390
Occidental Petroleum Corp. holds regular evacuation drills and stocks food , oxygen and non-prescription drugs at checkpoints in its 16-story headquarters .	B-expansion	4391..4545
The company also maintains rechargeable flashlights in offices and changes its standby supply of drinking water every three months .	I-expansion	4546..4677
" We feel we are doing everything we can , " an Occidental spokesman says .	O	4680..4751
Walt Disney Co. 's Disneyland in Anaheim , Calif. , stocks rescue equipment , medical supplies , and enough food and water to feed at least 10,000 visitors for as long as five days in the event that a calamity isolates the theme park .	O	4754..4984
The park also has emergency centers where specially trained employees would go to coordinate evacuation and rescue plans using walkie-talkies , cellular phones , and a public-address system .	B-expansion	4987..5175
The centers are complete with maps detailing utility lines beneath rides and " safe havens " where people can be assembled away from major structures .	I-expansion	5176..5324
Vista Chemical Co. , with three chemical plants in and near Lake Charles , La. , " prepares for every hurricane that enters the Gulf of Mexico , " says Keith L. Fogg , a company safety director .	O	5327..5514
Hurricane Hugo , an Atlantic storm , did n't affect Vista .	B-comparison	5515..5570
But two other major storms have threatened operations so far this year , most recently Hurricane Jerry this week .	I-comparison	5571..5683
Because hurricanes can change course rapidly , the company sends employees home and shuts down operations in stages -- the closer a storm gets , the more complete the shutdown .	B-expansion	5686..5860
The company does n't wait until the final hours to get ready for hurricanes .	I-expansion	5861..5936
" There are just tons of things that have to be considered , " Mr. Fogg says .	O	5937..6011
" Empty tank cars will float away on you if you get a big tidal surge . "	O	6012..6082
Still , Vista officials realize they 're relatively fortunate .	O	6085..6145
" With a hurricane you know it 's coming .	O	6146..6185
You have time to put precautionary mechanisms in place , " notes a Vista spokeswoman .	O	6186..6269
" A situation like San Francisco is so frightening because there 's no warning .	O	6270..6347
