The dollar finished mostly stronger yesterday, boosted by a modest recovery in share prices.	O	9..101
The Dow Jones Industrial Average climbed 6.76 points in a spate of bargain-hunting following last week's declines.	O	104..218
"Attention is fixed on the stock market for lack of anything else to sink our teeth into," said Robert White, a vice president at First Interstate of California.	O	221..382
Some analysts predict that in the absence of market-moving news to push the U.S. unit sharply higher or lower, the currency is likely to drift below 1.80 marks this week.	B-contrast	385..555
But others reject the view, and forecast the dollar will continue to hold its current tight trading pattern.	I-contrast	558..666
They argue that weakness in both the yen and sterling have helped offset bearish U.S. economic news and have lent support to the dollar.	O	667..803
In late New York trading yesterday, the dollar was quoted at 1.8340 marks, up from 1.8300 marks late Friday, and at 141.90 yen, up from 141.65 yen late Friday.	B-conjunction	806..965
Sterling was quoted at $1.5820, up from $1.5795 late Friday.	I-conjunction	966..1026
The dollar rose against the Swiss and French francs.	O	1029..1081
In Tokyo Tuesday, the U.S. currency opened for trading at 142.32 yen, up from Monday's Tokyo close of 142.17 yen.	O	1084..1197
Last week, the surprise resignation of British Chancellor of the Exchequer Nigel Lawson sent the British pound into a tailspin.	O	1200..1327
While sterling bounced back from session lows in a bout of short-covering yesterday, foreign exchange dealers said that any hopes that the pound would soon post significant gains have evaporated.	O	1330..1525
Traders said that statements made over the weekend to quell concern about the stability of Prime Minister Margaret Thatcher's government and the future of her economic program largely failed to reassure investors and bolster the flagging British unit.	O	1528..1779
In her first televised interview following Mr. Lawson's resignation, Mrs. Thatcher reiterated her desire to keep sterling strong and warned again that full entry into the European Monetary System's exchange rate mechanism would provide no easy solution to Britain's economic troubles.	O	1782..2066
She said that the timing of the United Kingdom's entry would depend on the speed with which other members liberalize their economies.	O	2069..2202
Mrs. Thatcher's remarks were seen as a rebuff to several leading members of her own Conservative Party who have called for a more clear-cut British commitment to the EMS.	O	2205..2375
At the same time, a recent poll shows that Mrs. Thatcher has hit the lowest popularity rating of any British leader since polling began 50 years ago.	O	2378..2527
Comments by John Major, who has succeeded Mr. Lawson, also failed to damp market concern, despite his pledge to maintain relatively high British interest rates.	O	2530..2690
According to one London-based analyst, even higher interest rates won't help the pound if Britain's government continues to appear unstable.	B-concession	2693..2833
One U.S. trader, however, dismissed sterling doomsayers while acknowledging there is little immediate upside potential for the U.K. unit.	I-concession	2836..2973
"There is no question that the situation is bad, but we may be painting a gloomier picture than we should," he said.	O	2976..3092
He predicts the pound will continue to trade in a very volatile fashion, with "fits of being oversold and overbought" before recovering its losses.	B-conjunction	3095..3242
Dealers also note that the general lack of enthusiasm for the yen has helped bolster the U.S. dollar.	I-conjunction	3245..3346
They observe that persistent Japanese investor demand for dollars for both portfolio and direct investment has kept a base of support for the dollar at around 140 yen.	O	3349..3516
The dollar began yesterday on a firm note in Tokyo, closing higher in late trade.	B-list	3519..3600
In Europe, the dollar closed slightly up in a market dominated by cross trades.	I-list	3601..3680
On the Commodity Exchange in New York, gold for current delivery settled at $377.80 an ounce, down 70 cents.	B-conjunction	3683..3791
Estimated volume was a moderate 3.5 million ounces.	I-conjunction	3792..3843
In early trading in Hong Kong Tuesday, gold was quoted at $376.80 an ounce.	O	3846..3921
