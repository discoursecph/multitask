Billions of investors ' dollars are pouring out of the nation 's junk-bond mutual funds , undermining a pillar of support in the already reeling junk market .	PRESPART	4
Last week alone , an eye-popping $ 1.6 billion flowed out of the junk funds , or nearly 5 % of their total assets , according to estimates by Dalbar Financial Services Inc. , a Boston research firm .	PAST	5
In the past two months the nation 's 88 junk funds have lost a total of about $ 6 billion -- more than 15 % of assets -- through sales or transfers of junk-fund shares , Dalbar says .	NONE	6
Interviews with three major fund groups -- Fidelity Investments , Vanguard Group Inc. and T. Rowe Price Associates Inc. -- confirm the trend .	NONE	8
Their junk funds combined have had net outflows totaling nearly $ 500 million , or about 13 % of their junk fund assets , in the past two months .	PRESENT	9
Some fund managers say negative publicity has exacerbated investors ' concern about recent declines in junk-bond prices .	PRESENT	10
" People have been seeing headline after headline after headline and saying : ` I ca n't take it anymore -- I 'm getting out , ' " says Kurt Brouwer of Brouwer amp Janachowski , a San Francisco investment adviser .	PRESENT	11
The withdrawals could spell trouble for the $ 200 billion junk market .	NONE	12
If the heavy outflows continue , fund managers will face increasing pressure to sell off some of their junk to pay departing investors in the weeks ahead .	INFINITIVE	13
Such selling could erode prices of high-yield junk bonds , already weakened by a rash of corporate credit problems .	NONE	14
Mutual fund groups have n't lost control of much of the outgoing money , says Louis Harvey , Dalbar 's president .	PRESENT	15
Mutual fund officials say that investors have transferred most of it into their money market accounts , and to a lesser extent , government-bond funds .	PRESENT	16
So the impact on the $ 950 billion mutual fund industry as a whole probably will be slight .	NONE	17
But tremors are likely in the junk-bond market , which has helped to finance the takeover boom of recent years .	INFINITIVE	18
The $ 1.5 billion Fidelity High Income Fund has had a net outflow of about $ 150 million in the past two months .	PRESENT	22
About $ 60 million streamed out last week alone , double the level of the week following last month 's Campeau Corp. credit squeeze .	NONE	23
About 98 % of the outflow was transferred to other Fidelity funds , says Neal Litvack , a Fidelity vice president , marketing , with most going into money market funds .	PAST	24
" You get a news item , it hits , you have strong redemptions that day and for two days following -- then go back to normal , " says Mr. Litvack .	PRESENT	25
The fund , with a cash cushion of more than 10 % , has " met all the redemptions without having to sell one thing , " Mr. Litvack says .	PRESENT	26
He adds : " Our fund has had { positive } net sales every month for the last three years -- until this month . "	NONE	27
Vanguard 's $ 1 billion High Yield Bond Portfolio has seen $ 161 million flow out since early September ;	PAST	28
$ 14 million of that seeped out Friday Oct. 13 alone .	PAST	29
Still , two-thirds of the outflow has been steered into other Vanguard portfolios , says Brian Mattes , a vice president .	PRESENT	30
The fund now holds a cash position of about 15 % .	PRESENT	31
At the $ 932 million T. Rowe Price High Yield Fund , investors yanked out about $ 182 million in the past two months .	PAST	32
Those withdrawals , most of which were transferred to other T. Rowe Price funds , followed little change in the fund 's sales picture this year through August .	NONE	33
" The last two months have been the whole ball game , " says Steven Norwitz , a vice president .	PRESENT	34
Junk-fund holders have barely broken even this year , as fat interest payments barely managed to offset declining prices .	PASTPART	35
Through Oct. 19 , high-yield funds had an average 0.85 % total return ( the price change plus dividends on fund shares ) , according to Lipper Analytical Services Inc.	PAST	36
Fidelity 's junk fund has fallen 2.08 % this year through Oct. 19 , Lipper says ;	PRESENT	38
the Vanguard fund rose 1.84 % ;	PAST	39
and the T. Rowe Price fund edged up 0.66 % .	PAST	40
People who remain in junk funds now could get hit again , some analysts and fund specialists say .	PRESENT	41
Many funds in recent weeks and months have been selling their highest-quality junk issues , such as RJR Nabisco , to raise cash to meet expected redemptions .	INFINITIVE	42
Funds might be forced to accept lower prices if they expand their selling to the securities of less-creditworthy borrowers .	NONE	43
And then , asset values of the funds could plunge more than they have so far .	NONE	44
