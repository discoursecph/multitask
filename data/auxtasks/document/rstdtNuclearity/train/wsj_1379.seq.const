It 's a California crime saga worthy of an Erle Stanley Gardner title :	(NS(NS
The Case of the Purloined Palm Trees .	NS)
Edward Carlson awoke one morning last month	(NS(NS(SN(NN(SN
to find eight holes in his front yard	(NS
where his prized miniature palms ,	(NN(NS
called cycads ,	NS)
once stood .	NN)))
Days later , the thieves returned	(NS(NS(NN
and dug out more ,	NN)
this time adding insult to injury .	NS)
`` The second time , ''	(NN(NS
he says ,	NS)
`` they left the shovel . ''	NN)))
No garden-variety crime , palm-tree rustling is sprouting up all over Southern California ,	(NS
bringing big bucks to crooks	(NS
who know their botany .	NS)))
Cycads ,	(NS(NS(NN(NS
the most popular of which is the Sago Palm ,	NS)
are doll-sized versions of California 's famous long-necked palms , with stubby trunks and fern-like fronds .	NN)
Because the Sago is relatively rare	(NS(NS(SN(NN
and grows only a couple of inches a year ,	NN)
it 's a pricey lawn decoration :	SN)
A two-foot tall Sago can retail for $ 1,000 ,	(NN
and taller ones often fetch $ 3,000 or more .	NN))
`` Evidently , somebody has realized	(NS(SN
it 's easy money to steal these things , ''	SN)
says Loran Whitelock , a research associate	(NN(NS
specializing in cycads	NS)
at the Los Angeles State and County Arboretum .	NN))))
Just last week , would-be thieves damaged three Sagos at Mr. Whitelock 's home in the Eagle Rock section	(NS(NS
before something frightened them off , foiled .	NS)
`` It 's hard to think someone is raping your garden , ''	(NS
he says .	NS))))
Police suspect	(NN(NS(NS(SN
that the criminals ,	(NN(NS
who dig up the plants in the dead of night ,	NS)
are selling them to nurseries or landscapers .	NN))
The Sago has become a popular accent in tony new housing tracts ,	(NS
apparently giving the rustlers a ready market for their filched fronds .	NS))
Thieves are going to find `` anybody	(NS(NS
who has enough bucks	(SN
to plant these things in their front yard , ''	SN))
says William Morrissey , an investigator with the police department in Garden Grove , Calif. ,	(NS
where five such thefts have been reported in the past several weeks .	NS)))
The department is advising residents to plant Sagos ,	(NN(NN(NN(NS
if they must ,	NS)
in the back yard	NN)
and telling nurseries to be on the lookout for anyone	(NS
trying to palm one off .	NS))
But for those Californians	(NN(NS
who want exotic gardens out front	(NS
where neighbors can appreciate them ,	NS))
there 's always Harold Smith 's approach .	(NS
After three Sagos were stolen from his home in Garden Grove ,	(NS(NS(SN
`` I put a big iron stake in the ground	(NN
and tied the tree to the stake with a chain , ''	NN))
he says proudly .	NS)
`` And you ca n't cut this chain with bolt cutters . ''	NS)))))))
