It can be hoped that Spanish Prime Minister Felipe Gonzalez will draw the right conclusion from his narrow election victory Sunday .	B-cause	9..140
A strong challenge from the far left , the Communist coalition Izquierda Unida , failed to topple him .	B-cause	141..241
He should consider his victory a mandate to continue his growth-oriented economic reforms and not a demand that he move further left .	B-conjunction	242..375
If he follows the correct path , he may be able to look back on this election as the high-water mark of far-left opposition .	I-conjunction	376..499
The far left had some good issues even if it did not have good programs for dealing with them .	B-cause	502..596
It could point to plenty of ailments that the Spanish economic rejuvenation so far has failed to cure .	I-cause	597..699
Unemployment still is officially recorded at 16.5 % , the highest rate in Europe , although actual joblessness may be lower .	O	700..821
Housing is scarce and public services -- the court system , schools , mail service , telephone network and the highways -- are in disgraceful condition .	B-conjunction	822..971
Large pockets of poverty still exist .	I-conjunction	972..1009
The left also is critical of the style of the Socialist government -- a remarkable parallel to the situation in Britain .	O	1012..1132
Mr. Gonzalez and his colleagues , particularly the finance minister , Carlos Solchaga , are charged with having abandoned their socialist principles and with having become arrogant elitists who refuse even to go on television ( controlled by the state ) to face their accusers .	B-contrast	1133..1405
In response to this , the Socialist prime minister has simply cited his free-market accomplishments .	I-contrast	1406..1505
They are very considerable : Since 1986 , when Spain joined the European Community , its gross domestic product has grown at an annual average of 4.6 % -- the fastest in the EC .	O	1508..1681
In that time more than 1.2 million jobs have been created and the official jobless rate has been pushed below 17 % from 21 % .	B-list	1682..1805
A 14 % inflation rate dropped below 5 % .	B-list	1806..1844
Net foreign investment through August this year has been running at a pace of $ 12.5 billion , about double the year-earlier rate .	I-list	1845..1973
Mr. Gonzalez also has split with the left in reaffirming Spain 's NATO commitment and in renewing a defense treaty with the U.S.	O	1974..2101
Mr. Gonzalez is not quite a closet supply-side revolutionary , however .	O	2104..2174
He did not go as far as he could have in tax reductions ; indeed he combined them with increases in indirect taxes .	B-contrast	2175..2289
Yet the best the far-left could do was not enough to deter the biggest voting bloc -- nearly 40 % -- from endorsing the direction Spain is taking .	B-cause	2292..2437
Now he can go further .	B-instantiation	2438..2460
He should do more to reduce tax rates on wealth and income , in recognition of the fact that those cuts yield higher , not lower , revenues .	I-instantiation	2461..2598
He could do more to cut public subsidies and transfers , thus making funds available for public services starved of money for six years .	O	2599..2734
The voters delivered Mr. Gonzalez a third mandate for his successes .	B-restatement	2737..2805
They , as well as numerous Latin American and East European countries that hope to adopt elements of the Spanish model , are supporting the direction Spain is taking .	B-cause	2806..2970
It would be sad for Mr. Gonzalez to abandon them to appease his foes .	I-cause	2971..3040
