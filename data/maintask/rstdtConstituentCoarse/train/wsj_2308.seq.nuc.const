Ripples from the strike by 55,000 Machinists union members against Boeing Co. reached air carriers Friday	(NN-Textual-organization(NN-Summary-Evaluation-Topic(NN-Summary-Evaluation-Topic(NS-Cause(NN-Comparison(NS-Summary-Evaluation-Topic(NS-Summary-Evaluation-Topic(NS-Cause
as America West Airlines announced	(NS-Cause(SN-Attribution
it will postpone its new service out of Houston	SN-Attribution)
because of delays	(NS-Elaboration
in receiving aircraft from the Seattle jet maker .	NS-Elaboration)))
Peter Otradovec , vice president for planning at the Phoenix , Ariz. , carrier , said in an interview	(NS-Cause(SN-Attribution
that the work stoppage at Boeing ,	(NS-Elaboration(NN-Textual-organization(NS-Elaboration
now entering its 13th day ,	NS-Elaboration)
`` has caused some turmoil in our scheduling ''	NN-Textual-organization)
and that more than 500 passengers	(NN-Textual-organization(NS-Elaboration
who were booked	(NS-Cause
to fly out of Houston on America West	NS-Cause))
would now be put on other airlines .	NN-Textual-organization)))
Mr. Otradovec said	(NS-Elaboration(NS-Cause(SN-Attribution
Boeing told America West	(SN-Attribution
that the 757	(NN-Textual-organization(NS-Elaboration
it was supposed to get this Thursday	NS-Elaboration)
would n't be delivered until Nov. 7	(NS-Elaboration
-- the day after the airline had been planning to initiate service at Houston with four daily flights ,	(NS-Elaboration
including three nonstops to Phoenix and one nonstop to Las Vegas .	NS-Elaboration)))))
Now , those routes are n't expected to begin until Jan .	NS-Cause)
Boeing is also supposed to send to America West another 757 twin-engine aircraft as well as a 737 by year 's end .	(NS-Summary-Evaluation-Topic
Those , too , are almost certain to arrive late .	NS-Summary-Evaluation-Topic))))
At this point , no other America West flights	(SN-Comparison(NN-Textual-organization(NS-Elaboration
-- including its new service at San Antonio , Texas ; Newark , N.J. ; and Palmdale , Calif. --	NS-Elaboration)
have been affected by the delays in Boeing deliveries .	NN-Textual-organization)
Nevertheless , the company 's reaction underscores the domino effect	(NS-Elaboration(NS-Elaboration
that a huge manufacturer such as Boeing can have on other parts of the economy .	NS-Elaboration)
It also is sure to help the machinists put added pressure on the company .	(NS-Cause
`` I just do n't feel	(NS-Cause(NS-Attribution(SN-Attribution
that the company can really stand	(NN-Joint
or would want a prolonged walkout , ''	NN-Joint))
Tom Baker , president of Machinists ' District 751 , said in an interview yesterday .	NS-Attribution)
`` I do n't think	(SN-Attribution
their customers would like it very much . ''	SN-Attribution))))))
America West , though , is a smaller airline	(NS-Cause(NS-Summary-Evaluation-Topic(SN-Cause
and therefore more affected by the delayed delivery of a single plane	(NS-Comparison
than many of its competitors would be .	NS-Comparison))
`` I figure	(NS-Attribution(SN-Attribution
that American and United probably have such a hard time	(SN-Cause(NS-Joint
counting all the planes in their fleets ,	NS-Joint)
they might not miss one at all , ''	SN-Cause))
Mr. Otradovec said .	NS-Attribution))
Indeed , a random check Friday did n't seem to indicate	(NS-Elaboration(SN-Attribution
that the strike was having much of an effect on other airline operations .	SN-Attribution)
Southwest Airlines has a Boeing 737-300	(NN-Joint(NS-Elaboration(NN-Joint(NS-Elaboration
set for delivery at the end of this month	NS-Elaboration)
and expects to have the plane on time .	NN-Joint)
`` It 's so close to completion ,	(NS-Attribution(SN-Cause
Boeing 's told us	(SN-Attribution
there wo n't be a problem , ''	SN-Attribution))
said a Southwest spokesman .	NS-Attribution))
A spokesman for AMR Corp. said	(NS-Elaboration(SN-Attribution
Boeing has assured American Airlines	(SN-Attribution
it will deliver a 757 on time later this month .	SN-Attribution))
American is preparing to take delivery of another 757 in early December and 20 more next year	(NN-Temporal
and is n't anticipating any changes in that timetable .	NN-Temporal))))))
In Seattle , a Boeing spokesman explained	(SN-Attribution
that the company has been in constant communication with all of its customers	(NN-Joint
and that it was impossible to predict what further disruptions might be triggered by the strike .	NN-Joint)))
Meanwhile , supervisors and non-striking employees have been trying to finish some 40 aircraft	(NN-Summary-Evaluation-Topic(SN-Cause(NS-Elaboration(NS-Elaboration
-- mostly 747 and 767 jumbo jets at the company 's Everett , Wash. , plant --	NS-Elaboration)
that were all but completed before the walkout .	NS-Elaboration)
As of Friday , four had been delivered	(NS-Elaboration
and a fifth plane , a 747-400 , was supposed to be flown out over the weekend to Air China .	NS-Elaboration))
No date has yet been set	(NS-Summary-Evaluation-Topic(NS-Summary-Evaluation-Topic(NS-Cause(NS-Cause
to get back to the bargaining table .	NS-Cause)
`` We want to make sure	(NS-Attribution(SN-Attribution
they know	(NS-Temporal(SN-Attribution
what they want	SN-Attribution)
before they come back , ''	NS-Temporal))
said Doug Hammond , the federal mediator	(NS-Elaboration
who has been in contact with both sides	(NS-Temporal
since the strike began .	NS-Temporal))))
The investment community , for one , has been anticipating a speedy resolution .	(NN-Comparison(NS-Elaboration(NS-Elaboration(NS-Elaboration
Though Boeing 's stock price was battered along with the rest of the market Friday ,	(SN-Comparison
it actually has risen over the last two weeks on the strength of new orders .	SN-Comparison))
`` The market has taken two views :	(NS-Attribution(NS-Elaboration
that the labor situation will get settled in the short term	(NN-Joint
and that things look very rosy for Boeing in the long term , ''	NN-Joint))
said Howard Rubel , an analyst at Cyrus J. Lawrence Inc .	NS-Attribution))
Boeing 's shares fell $ 4 Friday	(NS-Cause
to close at $ 57.375 in composite trading on the New York Stock Exchange .	NS-Cause))
But Mr. Baker said	(NS-Summary-Evaluation-Topic(SN-Comparison(NS-Joint(SN-Attribution
he thinks	(SN-Attribution
the earliest	(NN-Textual-organization(NS-Elaboration
a pact could be struck	NS-Elaboration)
would be the end of this month ,	NN-Textual-organization)))
hinting	(SN-Attribution
that the company and union may resume negotiations as early as this week .	SN-Attribution))
Still ,	(NN-Textual-organization(NS-Attribution
he said ,	NS-Attribution)
it 's possible that the strike could last considerably longer .	NN-Textual-organization))
`` I would n't expect an immediate resolution to anything . ''	NS-Summary-Evaluation-Topic)))
Last week , Boeing Chairman Frank Shrontz sent striking workers a letter ,	(NN-Summary-Evaluation-Topic(NS-Elaboration
saying	(SN-Attribution
that `` to my knowledge , Boeing 's offer represents the best overall three-year contract of any major U.S. industrial firm in recent history . ''	SN-Attribution))
But Mr. Baker called the letter	(NS-Elaboration(NN-Textual-organization(NS-Elaboration
-- and the company 's offer of a 10 % wage increase over the life of the pact , plus bonuses --	NS-Elaboration)
`` very weak . ''	NN-Textual-organization)
He added	(SN-Attribution
that the company miscalculated the union 's resolve and the workers ' disgust	(NS-Comparison
with being forced to work many hours overtime .	NS-Comparison)))))))
In separate developments :	(NN-Textual-organization
-- Talks have broken off between Machinists representatives at Lockheed Corp. and the Calabasas , Calif. , aerospace company .	(NN-Joint(SN-Comparison
The union is continuing to work through its expired contract , however .	(NN-Joint
It had planned a strike vote for next Sunday ,	(NN-Comparison
but that has been pushed back indefinitely .	NN-Comparison)))
-- United Auto Workers Local 1069 ,	(NN-Joint(NS-Elaboration(SN-Attribution(NN-Textual-organization(NS-Elaboration
which represents 3,000 workers at Boeing 's helicopter unit in Delaware County , Pa. ,	NS-Elaboration)
said	NN-Textual-organization)
it agreed to extend its contract on a day-by-day basis , with a 10-day notification	(NS-Temporal(NS-Elaboration
to cancel ,	NS-Elaboration)
while it continues bargaining .	NS-Temporal))
The accord expired yesterday .	NS-Elaboration)
-- And Boeing on Friday said	(NS-Elaboration(NS-Cause(NS-Elaboration(SN-Attribution
it received an order from Martinair Holland for four model 767-300 wide-body jetliners	(NS-Summary-Evaluation-Topic
valued at a total of about $ 326 million .	NS-Summary-Evaluation-Topic))
The planes , long range versions of the medium-haul twin-jet , will be delivered with Pratt & Whitney PW4060 engines .	NS-Elaboration)
Pratt & Whitney is a unit of United Technologies Inc .	(NN-Joint
Martinair Holland is based in Amsterdam .	NN-Joint))
A Boeing spokeswoman said	(SN-Attribution
a delivery date for the planes is still being worked out `` for a variety of reasons ,	(NS-Temporal
but not because of the strike . ''	NS-Temporal)))))))
Bridget O'Brian contributed to this article .	NN-Textual-organization)
