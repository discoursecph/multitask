UAL Corp. 's board quashed any prospects for an immediate revival of a labor-management buy-out , saying United Airlines ' parent should remain independent for now .	root	9..171
As a result , UAL 's chairman , Stephen M. Wolf , pulled out of the buy-out effort to focus on running the company .	cause	174..285
The two developments put the acquisition attempt back to square one and leaves the airline with an array of unresolved matters , including an unsettled labor situation and a management scrambling to restore its damaged credibility .	norel	288..518
The effort to create the nation 's largest employee-owned company began unraveling Oct. 13 when the labor-management group was unable to obtain financing for its $ 300-a-share , $ 6.79 billion offer .	norel	521..716
Just last week it suffered another major setback	asynchronous	717..765
when British Airways PLC , the largest equity investor in the labor-management bid , withdrew its support .	synchrony	766..870
Takeover stock traders , focusing on the company 's intention to stay independent , took the announcement as bad news .	norel	873..988
UAL , which had risen $ 9.875 to $ 178.375 in composite trading on the New York Stock Exchange on reports of a new bid being prepared by the group , reversed course and plummeted in off-exchange trading after the 5:09 p.m. EDT announcement .	norel	991..1227
Among the first trades reported by the securities firm of Jefferies & Co. , which makes a market in UAL after the exchange is closed , were 10,000 shares at $ 170 , 6,000 shares at $ 162 , 2,500 at $ 162 , and 10,000 at $ 158 .	norel	1230..1447
The rebound in UAL stock during regular trading hours Monday was its first daily gain	norel	1450..1535
after six consecutive losses left the price 41 % below its level before Oct. 13 , the day the group announced the bank financing could n't be obtained for the original deal .	asynchronous	1536..1706
Twelve of UAL 's outside directors met at a five-hour meeting yesterday in Chicago to consider an informal proposal from the buy-out group for a revised bid .	norel	1709..1865
But the board said it was n't interested for now .	contrast	1866..1914
That proposal , valued at between $ 225 and $ 240 a share , would have transferred majority ownership to employees	entrel	1915..2025
while leaving some stock in public hands .	conjunction	2026..2067
The buy-out group had no firm financing for the plan .	norel	2070..2123
And , with no other offers on the table , the board apparently felt no pressure to act on it .	conjunction	2124..2215
The directors signaled , however , that they would be willing to consider future offers or take some other action to maximize shareholder value , saying they would continue to explore " all strategic and financial alternatives . "	contrast	2218..2442
But it was clear that for the time being , the board wants the company to return to normalcy .	contrast	2445..2537
The board said it concluded that " the welfare of the company , its shareholders , its employees and the broader public ... can best be enhanced by continued development of UAL as a strong , viable , independent company . "	cause	2538..2754
Mr. Wolf urged all employees to " now turn their full attention " to operating the airline .	norel	2757..2846
He also vowed to " make every effort to nurture ... a constructive new relationship that has been forged with participating employee groups . "	conjunction	2847..2987
But Mr. Wolf faces a monumental task in pulling the company back together again .	contrast	2990..3070
Labor problems top the list .	restatement	3071..3099
For a brief time , the buy-out effort seemed to solve his problems with United 's pilot union .	contrast	3100..3192
In return for an ownership stake in the company , the pilots were willing to agree to a seven-year contract that included a no-strike clause and significant wage concessions and productivity gains the union previously resisted .	cause	3193..3419
That contract was tied to the success of the buy-out .	norel	3422..3475
As a " good-will measure , " the pilots had been working four extra hours a month and had agreed to fly UAL 's two new Boeing 747-400 aircraft .	conjunction	3476..3615
It 's uncertain if the pilots will continue to do so without a contract settlement .	contrast	3616..3698
The union said late last night that it is still committed to majority employee ownership and that the labor disputes that faced the company prior to the buy-out effort " still need to be addressed . "	norel	3701..3898
The buy-out effort also worsened already-strained relations between United 's pilot and machinist unions .	conjunction	3901..4005
The machinists ' criticisms of the labor-management bid and their threats of a strike unless they received substantial wage increases this year helped cool banks ' interest in financing the transaction .	conjunction	4006..4206
The machinists previously had shown themselves to be an ally to Mr. Wolf , but he lost much of his credibility with that group when he teamed up with the pilot union .	norel	4209..4374
The machinists criticized the terms Mr. Wolf and management received in the buy-out .	cause	4375..4459
They paid $ 15 million for a 1 % stake and received an additional 9 % of the company at no additional cost .	restatement	4460..4564
His credibility is also on the line in the investment community .	norel	4567..4631
Until the collapse of this bid , Mr. Wolf was regarded as one of the nation 's savviest airline executives after engineering turnarounds of Tiger International Inc. and Republic Airlines .	cause	4632..4817
But he and his chief financial officer , John Pope , sowed some of the seeds for the deal 's failure by insisting banks accept low financing fees and interest rates , while they invested in the transaction only a small fraction of the $ 114.3 million they stood to gain from sale of their UAL stock and options .	contrast	4820..5126
The board 's actions leave takeover stock traders nursing some $ 700 million in losses and eager to respond to anyone who might make a new offer .	norel	5129..5272
It also inevitably leaves a residue of shareholder lawsuits .	conjunction	5273..5333
Arbitragers said they were disappointed the company did n't announce some recapitalization or other plan to maximize value .	norel	5336..5458
One takeover expert noted that arbitragers could force a recapitalization through the written consent process under which holders may oust the board by a majority vote .	conjunction	5459..5627
The machinists union has suggested it may propose a recapitalization that includes a special dividend for holders and a minority ownership stake for employees .	conjunction	5628..5787
Los Angeles investor Marvin Davis , whose $ 240-a-share offer for UAL in August triggered a bidding war , says he remains interested in the airline .	norel	5790..5935
However , he is restricted from making certain hostile moves by an agreement he signed to obtain confidential UAL data .	contrast	5936..6054
Essentially , he ca n't make any hostile moves unless he makes a tender offer at least $ 300 a share .	restatement	6055..6153
