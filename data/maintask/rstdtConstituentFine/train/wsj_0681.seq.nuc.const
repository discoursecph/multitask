Wham !	(NN-TextualOrganization(NS-elaboration-additional(NS-elaboration-additional(SN-circumstance(NS-circumstance(SN-circumstance(SN-comment(NN-List
Bam !	NN-List)
Twice in two weeks the unraveling of the on-again , off-again UAL buy-out slammed the stock market .	SN-comment)
Now , stock prices seem to be in a general retreat .	SN-circumstance)
Since peaking at 2791.41 on Oct. 9 ,	NS-circumstance)
the Dow Jones Industrial Average has lost 194.69 points , or 7 % ,	(NS-cause
closing Friday at 2596.72 , down 17.01 .	NS-cause))
The number of issues	(NN-List(NN-Same-Unit(NS-elaboration-object-attribute
falling on the New York Stock Exchange each day	NS-elaboration-object-attribute)
is eclipsing the number of gainers .	NN-Same-Unit)
And the number of stocks	(NN-Same-Unit(NS-elaboration-object-attribute
hitting new lows	NS-elaboration-object-attribute)
far outstrips the number	(NS-elaboration-object-attribute
setting new highs .	NS-elaboration-object-attribute))))
But why should an iffy $ 6.79 billion leveraged buy-out deal shake the foundations of the entire stock market ?	(NN-Question-Answer
Opinions vary	(NS-explanation-argumentative(SN-antithesis(NS-elaboration-object-attribute
about how important the UAL deal was to the market 's health ,	NS-elaboration-object-attribute)
but analysts generally agree	(SN-attribution
that the market gyrations	(NN-Same-Unit(NS-elaboration-object-attribute
created as the UAL plan crumbled	NS-elaboration-object-attribute)
revealed a fundamental change in investor psychology .	NN-Same-Unit)))
`` If this had happened a few months ago	(NN-List(NS-attribution(NN-Same-Unit(NS-elaboration-additional
when the atmosphere was still very positive	NS-elaboration-additional)
it would n't have been greeted with anything like the impact it has had over the past two weeks , ''	NN-Same-Unit)
says Dennis Jarrett , a market strategist at Kidder Peabody .	NS-attribution)
There are , of course , analysts	(NS-evaluation(NS-elaboration-object-attribute
who view the near-panic	(NN-Same-Unit(NS-elaboration-object-attribute
that briefly swept through investors on Oct. 13 and again on Oct. 24	NS-elaboration-object-attribute)
as momentary lapses of good judgment	(NS-elaboration-object-attribute
that have only temporarily undermined a healthy stock market .	NS-elaboration-object-attribute)))
Sure , price action is volatile	(NS-explanation-argumentative(NS-attribution(SN-concession(NN-List
and that 's scary ,	NN-List)
but all-in-all stocks are still a good place to be ,	SN-concession)
they suggest .	NS-attribution)
The reaction to the UAL debacle `` is mindless , ''	(NN-List(NS-elaboration-additional(NS-attribution
says John Connolly , chief market strategist at Dean Witter .	NS-attribution)
`` UAL is a small deal	(NS-explanation-argumentative(NS-circumstance
as far as the overall market is concerned .	NS-circumstance)
The only way	(NS-example(NN-Same-Unit(NS-elaboration-object-attribute
you can make it a big deal	NS-elaboration-object-attribute)
is to draw linkages	(NS-elaboration-object-attribute
that just do n't make sense . ''	NS-elaboration-object-attribute))
He suggests , for example ,	(NS-elaboration-additional(SN-attribution
that investors may have assumed	(SN-attribution
that	(NN-Same-Unit
just because UAL could n't get financing ,	(SN-circumstance
no leveraged buy-outs can get financing .	SN-circumstance))))
Carried even further ,	(NS-explanation-argumentative(SN-elaboration-additional
some investors assumed	(SN-attribution
that	(NN-Same-Unit
since leveraged buy-outs are the only thing	(NS-condition(SN-circumstance(NS-elaboration-object-attribute
propping up stock prices ,	NS-elaboration-object-attribute)
the market would collapse	SN-circumstance)
if no more LBOs could be done .	NS-condition))))
`` There will still be deals , ''	(NS-elaboration-additional(NS-elaboration-additional(NS-attribution
argues Mr. Connolly .	NS-attribution)
`` There may not be as many	(NN-Contrast(NN-List
and the buyers may not get away with some of the things	(NS-elaboration-object-attribute
they 've done in the past ,	NS-elaboration-object-attribute))
but deals wo n't disappear . ''	NN-Contrast))
He forecasts	(NS-elaboration-additional(SN-attribution
that the emphasis in mergers and acquisitions may soon return to what he calls `` strategic deals ,	(NS-elaboration-additional
in which somebody is taking over a company	(NS-reason(NS-purpose
not to milk the cash flow ,	NS-purpose)
but because it 's a good fit . ''	NS-reason)))
And	(NS-example(NN-Same-Unit
even without deals ,	(SN-circumstance
Mr. Connolly figures	(SN-attribution
the market would remain healthy .	SN-attribution)))
He notes , for instance ,	(NS-explanation-argumentative(SN-attribution
that there has n't been a merger or acquisition among the 30 stocks in the Dow Jones Industrial Average since 1986 ,	(SN-antithesis
yet that average only three weeks ago hit a record high .	SN-antithesis))
`` Those stocks are up	(NS-elaboration-additional(NS-attribution(NN-Cause-Result
because their earnings are up	(NN-List
and their dividends are up , ''	NN-List))
he says .	NS-attribution)
Even the volatility	(NS-elaboration-additional(NN-Same-Unit(NS-elaboration-object-attribute
created by stock index arbitrage and other computer-driven trading strategies	NS-elaboration-object-attribute)
is n't entirely bad , in Mr. Connolly 's view .	NN-Same-Unit)
For the long-term investor	(NS-rhetorical-question(NN-Same-Unit(NS-elaboration-object-attribute
who picks stocks carefully ,	NS-elaboration-object-attribute)
the price volatility can provide welcome buying opportunities	(NS-circumstance
as short-term players scramble frantically	(NS-purpose
to sell stocks in a matter of minutes .	NS-purpose)))
`` Who can make the better decision , the guy	(NN-List(NS-attribution(NN-Same-Unit(NS-elaboration-object-attribute
who has 10 seconds	(SN-circumstance
to decide what to do	SN-circumstance))
or the guy with all the time in the world ? ''	NN-Same-Unit)
he says .	NS-attribution)
`` What on earth does the UAL deal have to do with the price of Walmart ,	(NS-elaboration-additional
which I was able to buy on Oct. 16 at a very attractive price ? ''	NS-elaboration-additional))))))))))))))
Kidder Peabody 's Mr. Jarrett also sees some benefits to the stock market 's recent drop .	(NS-explanation-argumentative
`` We 've run into a market	(NN-Contrast(NS-elaboration-additional(NS-attribution(NS-elaboration-object-attribute
that was beginning to run out of steam	(NN-List
and get frothy , ''	NN-List))
he says .	NS-attribution)
`` The balloon had been blown up so big	(NS-elaboration-additional(NS-consequence
that	(NN-Same-Unit
when somebody came along with a pin	(SN-circumstance(NS-elaboration-general-specific
-- in this case the UAL deal --	NS-elaboration-general-specific)
we got a little pop . ''	SN-circumstance)))
The pop sobered up investors	(NS-explanation-argumentative(NS-attribution(NS-elaboration-object-attribute
who had been getting a little too ebullient ,	NS-elaboration-object-attribute)
says Mr. Jarrett .	NS-attribution)
`` It provided an excuse	(NS-elaboration-object-attribute
for people to get back to reality	(NN-List
and to look at the economic data , especially the third-quarter economic numbers ,	(NN-List
and to realize	(SN-attribution
that we ca n't continue to gloss over what is going on in the junk bond market . ''	SN-attribution)))))))
But he figures	(NS-elaboration-additional(SN-attribution
that at current levels the stock market is comfortably valued ,	(NS-explanation-argumentative(NS-circumstance
even with the economy obviously slowing .	NS-circumstance)
`` Just because we 've got some realism back in the market does n't mean it 's going lower from here , ''	(NS-elaboration-additional(NS-attribution
he says .	NS-attribution)
`` The bottom line is that it 's healthy to have this kind of sideways activity , especially after a 30 % gain in stock values over the past 12 months . ''	NS-elaboration-additional)))
He 's now estimating	(NS-comment(SN-attribution
that after a period of consolidation , the Dow Jones Industrial Average will once again forge new highs .	SN-attribution)
Maybe , maybe not .	(NS-explanation-argumentative
Abby Joseph Cohen , a market strategist at Drexel Burnham Lambert , is n't nearly so sanguine about the market 's chances	(NS-explanation-argumentative(NS-elaboration-object-attribute
of surging to new highs anytime soon .	NS-elaboration-object-attribute)
Her view is that stock prices have three major props :	(NS-evaluation(NS-explanation-argumentative(NS-evaluation(NS-elaboration-set-member
merger and buy-out proposals , earnings and the economic outlook .	NS-elaboration-set-member)
At current levels of economic activity and earnings , stocks are fairly valued ,	(SN-antithesis(NS-attribution
she says .	NS-attribution)
But any chance	(NN-Same-Unit(NS-elaboration-object-attribute
for prices to surge above fair value	NS-elaboration-object-attribute)
lies in the speculation	(NS-circumstance(NS-elaboration-object-attribute
that accompanies a vigorous merger and buy-out business ,	NS-elaboration-object-attribute)
and UAL has obviously put a damper on that .	NS-circumstance))))
`` Stocks are n't cheap anymore ,	(NS-elaboration-additional(NS-attribution(NN-List
there have been some judicial and legislative changes in the merger area	(NN-List
and all of this changes the arithmetic of deals , ''	NN-List))
she says .	NS-attribution)
`` I 'm not saying	(SN-antithesis(SN-attribution
they 've stopped altogether ,	SN-attribution)
but future deals are going to be structured differently	(SN-result
and bids probably wo n't be as high . ''	SN-result))))
But that 's not the only problem for stocks .	(NS-elaboration-additional(NS-explanation-argumentative
The other two props	(NN-Same-Unit(NS-elaboration-set-member
-- earnings and the economic outlook --	NS-elaboration-set-member)
are troubling , too .	NN-Same-Unit))
`` M & A is getting all the headlines right now ,	(NN-List(NS-attribution(NN-Contrast
but these other things have been building up more gradually , ''	NN-Contrast)
she says .	NS-attribution)
Third-quarter earnings have been generally disappointing	(NN-List(SN-circumstance
and with economic data	(NN-Same-Unit(NS-elaboration-object-attribute
showing a clear slowing ,	NS-elaboration-object-attribute)
the outlook for earnings in the fourth quarter and all of 1990 is getting worse .	NN-Same-Unit))
`` There are a lot more downward than upward revisions	(NS-elaboration-additional(NS-attribution(NN-List
and it looks like people are questioning corporate profits as a means of support for stock prices , ''	NN-List)
she says .	NS-attribution)
With all this , can stock prices hold their own ?	(NN-Question-Answer
`` The question is unanswerable at this point , ''	(NS-elaboration-additional(SN-attribution
she says .	SN-attribution)
`` It depends on what happens .	(NS-elaboration-additional
If the economy slips into a recession ,	(SN-condition
then this is n't a level that 's going to hold . ''	SN-condition))))))))))))))))))))))
Friday 's Market Activity	(NN-TextualOrganization
Stock prices tumbled for a third consecutive day	(NS-explanation-argumentative(NS-circumstance
as earnings disappointments , a sluggish economy and a fragile junk bond market continued to weigh on investors .	NS-circumstance)
The Dow Jones Industrial Average fell 17.01 points to 2596.72 in active trading .	(NN-List
Volume on the New York Stock Exchange totaled 170,330,000 shares .	(NN-List
Declining issues on the Big Board were far ahead of gainers , 1,108 to 416 .	(NN-List
For the week the Dow Jones Industrial Average sank 92.42 points , or 3.4 % .	(NN-List
Oil stocks escaped the brunt of Friday 's selling	(NN-List(NS-elaboration-additional(NS-elaboration-additional(NN-List
and several were able to post gains ,	(NS-example
including Chevron ,	(NS-elaboration-additional
which rose 5/8 to 66 3/8 in Big Board composite trading of 2.4 million shares .	NS-elaboration-additional)))
The stock 's advance reflected ongoing speculation	(NS-attribution(NS-elaboration-object-attribute
that Pennzoil is accumulating a stake in the company ,	NS-elaboration-object-attribute)
according to Dow Jones Professional Investor Report .	NS-attribution))
Both companies declined to comment on the rumors ,	(SN-antithesis
but several industry analysts told the Professional Investor Report	(SN-attribution
they believed	(SN-attribution
it was plausible that Pennzoil may be buying Chevron shares as a prelude	(NS-elaboration-object-attribute
to pushing for a restructuring of the company .	NS-elaboration-object-attribute)))))
USX gained 1/2 to 33 3/8 on a report in Business Week magazine	(NN-List(NS-elaboration-additional(NS-elaboration-object-attribute
that investor Carl Icahn is said to have raised his stake in the oil and steel company to just about 15 % .	NS-elaboration-object-attribute)
Earlier this month , Mr. Icahn boosted his USX stake to 13.4 % .	NS-elaboration-additional)
Elsewhere in the oil sector , Exxon rallied 7/8 to 45 3/4 ;	(NN-List
Amoco rose 1/8 to 47 ;	(NN-List
Texaco was unchanged at 51 3/4 ,	(NN-List
and Atlantic Richfield fell 1 5/8 to 99 1/2 .	(NN-List
Mobil ,	(NN-List(NN-Same-Unit(NS-elaboration-additional
which said	(SN-attribution
it plans to cut its exploration and production work force by about 8 % in a restructuring ,	SN-attribution))
dropped 5/8 to 56 1/8 .	NN-Same-Unit)
The precious metals sector outgained other Dow Jones industry groups by a wide margin for the second consecutive session .	(NN-List(NS-elaboration-additional(NS-elaboration-general-specific
Hecla Mining rose 5/8 to 14 ;	(NN-List
Battle Mountain Gold climbed 3/4 to 16 3/4 ;	(NN-List
Homestake Mining rose 1 1/8 to 16 7/8 ;	(NN-List
Lac Minerals added 5/8 to 11 ;	(NN-List
Placer Dome went up 7/8 to 16 3/4 ,	(NN-List
and ASA Ltd. jumped 3 5/8 to 49 5/8 .	NN-List))))))
Gold mining stocks	(NS-elaboration-general-specific(NN-Same-Unit(NS-elaboration-object-attribute
traded on the American Stock Exchange	NS-elaboration-object-attribute)
also showed strength .	NN-Same-Unit)
Echo Bay Mines rose 5/8 to 15 7/8 ;	(NN-List
Pegasus Gold advanced 1 1/2 to 12 ,	(NN-List
and Corona Class A gained 1/2 to 7 1/2 .	NN-List))))
Unisys dropped 3/4 to 16 1/4	(NN-List(NS-example(SN-antithesis(NS-circumstance
after posting a third-quarter loss of $ 4.25 a share ,	(NS-example
including restructuring charges ,	NS-example))
but other important technology issues were mixed .	SN-antithesis)
Compaq Computer ,	(NN-List(NN-Same-Unit(NS-elaboration-additional
which had lost 8 5/8 Thursday	(NS-circumstance
following a disappointing quarterly report ,	NS-circumstance))
gained 5/8 to 100 5/8 .	NN-Same-Unit)
International Business Machines dropped 7/8 to 99 7/8 .	(NN-List
Digital Equipment tacked on 1 1/8 to 89 1/8 ,	(NN-List
and Hewlett-Packard fell 3/8 to 49 3/8 .	NN-List))))
Dividend-related trading swelled volume in Merrill Lynch ,	(NN-List(NS-elaboration-additional(NS-elaboration-additional
which closed unchanged at 28 3/8	(NS-circumstance
as 2.7 million shares changed hands .	NS-circumstance))
The stock has a 3.5 % dividend yield	(NN-List
and goes ex-dividend today .	NN-List))
Erbamont advanced 1 1/8 to 36 1/2 on 1.9 million shares .	(NN-List(NS-elaboration-additional
Montedison ,	(NS-elaboration-additional(NN-Same-Unit(NS-elaboration-additional
which owns about 72 % of the company 's common stock ,	NS-elaboration-additional)
agreed to buy the rest for $ 37 a share .	NN-Same-Unit)
Himont , another majority-owned unit of Montedison , added 1 1/4 to 47 1/8 .	NS-elaboration-additional))
Milton Roy jumped 2 to 18 3/8 .	(NN-List(NS-elaboration-additional(NS-elaboration-additional
Crane said	(SN-attribution
it holds an 8.9 % stake in the company	(SN-circumstance
and may seek control .	SN-circumstance)))
Crane dropped 1 1/8 to 21 1/8 .	NS-elaboration-additional)
Comprehensive Care ,	(NS-elaboration-additional(NN-Same-Unit(NS-elaboration-additional
which terminated its agreement	(NS-elaboration-object-attribute
to merge with First Hospital ,	NS-elaboration-object-attribute))
dropped 7/8 to 3 7/8 .	NN-Same-Unit)
The company 's decision was made	(NS-circumstance
after First Hospital failed to obtain financing for its offer .	NS-circumstance)))))))))))))))))))))
