The	O	0
group	O	1
also	O	2
included	O	3
two	B-QUANTITY	4
members	O	5
of	O	6
the	O	7
banned	O	8
Knights	B-ORG	9
of	I-ORG	10
the	I-ORG	11
Great	I-ORG	12
Islamic	I-ORG	13
Orient	I-ORG	14
Front	I-ORG	15
which	O	16
claimed	O	17
responsibility	O	18
for	O	19
the	O	20
Istanbul	B-GPE	21
attacks	O	22
,	O	23
including	O	24
the	O	25
bombing	O	26
of	O	27
a	O	28
Jewish	B-NORP	29
temple	O	30
and	O	31
attacks	O	32
on	O	33
the	B-FAC	34
British	I-FAC	35
Consulate	I-FAC	36
and	O	37
the	B-FAC	38
British	I-FAC	39
HSBC	I-FAC	40
bank	I-FAC	41
in	O	42
November	B-DATE	43
2003	I-DATE	44
.	O	45
