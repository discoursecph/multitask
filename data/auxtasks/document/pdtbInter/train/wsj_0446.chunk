From a reading of the somewhat scant English-language medical literature on RU-486, the French abortion pill emerges as one of the creepiest concoctions around.	B-cause	9..169
This is not only because it kills the unborn, a job at which it actually is not outstandingly efficient, zapping only 50% to 85% of them depending on which study you read (prostaglandin, taken in conjunction with the pill, boosts the rate to 95%).	I-cause	170..417
By contrast, surgical abortion is 99% effective.	O	418..466
Abortion via the pill is far more of an ordeal than conventional surgical abortion.	B-restatement	469..552
It is time-consuming (the abortion part alone lasts three days, and the clinical part comprises a week's worth of visits), bloody (one woman in a Swedish trial required a transfusion, although for most it resembles a menstrual period, with bleeding lasting an average of 10 days), and painful (many women require analgesic shots to ease them through)	B-conjunction	553..904
Nausea and vomiting are other common side effects.	I-conjunction	905..955
Timing is of the essence with RU-486.	B-restatement	958..995
It is most effective taken about a week after a woman misses her menstrual period up through the seventh week of pregnancy, when it is markedly less effective.	I-restatement	996..1155
That is typically about a three-week window.	O	1156..1200
So far, all the studies have concluded that RU-486 is "safe.	B-concession	1203..1264
But "safe," in the definition of Marie Bass of the Reproductive Health Technologies Project, means "there's been no evidence so far of mortality.	B-contrast	1265..1411
No one has researched the long-term effects of RU-486 on a woman's health or fertility.	I-contrast	1412..1499
The drug seems to suppress ovulation for three to seven months after it is taken.	O	1502..1583
Some women clearly have no trouble eventually conceiving again: The studies have reported repeaters in their programs.	B-concession	1584..1702
But there are no scientific data on this question.	I-concession	1703..1753
Rather ominously, rabbit studies reveal that RU-486 can cause birth defects, Lancet, the British medical journal, reported in 1987.	O	1756..1887
However, Dr. Etienne-Emile Baulieu, the French physician who invented RU-486, wrote in a Science magazine article last month that the rabbit-test results could not be duplicated in rats and monkeys.	B-entrel	1888..2086
The drug has a three-dimensional structure similar to that of DES, the anti-miscarriage drug that has been linked to cervical and vaginal cancer in some of the daughters of the women who took it.	I-entrel	2087..2282
All the published studies recommend that women on whom the drug proves ineffective not carry the pregnancy to term but undergo a surgical abortion.	O	2285..2432
A risk of birth defects, a sure source of lawsuits, is one reason the U.S. pharmaceutical industry is steering clear of RU-486.	O	2433..2560
One might well ask: Why bother with this drug at all?	O	2563..2616
Some abortion advocates have been asking themselves this very question.	B-instantiation	2617..2688
RU-486 "probably represents a technical advance in an area where none is needed, or at least not very much," said Phillip Stubblefield, president of the National Abortion Federation, at a reproductive health conference in 1986.	B-conjunction	2689..2916
Many physicians have expressed concern over the heavy bleeding, which occurs even if the drug fails to induce an abortion.	I-conjunction	2917..3039
It typically takes from eight to 10 years to obtain the Food and Drug Administration's approval for a new drug, and the cost of testing and marketing a new drug can range from $30 million to $70 million.	O	3042..3245
The Health and Human Services Department currently forbids the National Institutes of Health from funding abortion research as part of its $8 million contraceptive program.	B-contrast	3248..3420
But the Population Council, a 37-year-old, $20 million nonprofit organization that has the backing of the Rockefeller and Mellon foundations and currently subsidizes most U.S. research on contraceptives, has recently been paying for U.S. studies of RU-486 on a license from its French developer, Roussel-Uclaf, a joint subsidiary of the German pharmaceutical company Hoechst and the French government.	I-contrast	3421..3822
In the year since the pill went on the French market, the National Organization for Women and its offshoot, former NOW President Eleanor Smeal's Fund for a Feminist Majority, have been trying to browbeat the U.S. pharmaceutical industry into getting involved.	B-instantiation	3825..4084
(Its scare-tactic prediction: the pill "will be available in the U.S., either legally or illegally, in no more than 2-5 years.")	I-instantiation	4085..4213
Following the feminist and population-control lead has been a generally bovine press.	B-instantiation	4216..4301
A June 1988 article in Mother Jones magazine is typical of the general level of media ignorance.	B-restatement	4302..4398
"For a woman whose period is late, using RU-486 means no waiting, no walking past picket lines at abortion clinics, and no feet up in stirrups for surgery," burbles health writer Laura Fraser.	I-restatement	4399..4591
For a woman whose period is late, using RU-486 means no waiting, no walking past picket lines at abortion clinics, and no feet up in stirrups for surgery," burbles health writer Laura Fraser. "It also means she will never have to know whether she had actually been pregnant.	B-restatement	4592..4675
Wrong on all counts, Miss Fraser.	I-restatement	4676..4709
RU-486 is being administered in France only under strict supervision in the presence of a doctor.	B-conjunction	4712..4809
(Roussel reportedly has every pill marked and accounted for to make sure none slips into the black market.)	I-conjunction	4810..4917
Thus, a woman who used RU-486 to have an abortion would have to make three trips to the clinic past those picket lines; an initial visit for medical screening (anemics and those with previous pregnancy problems are eliminated) and to take the pill, a second trip 48 hours later for the prostaglandin, administered either via injection or vaginal suppository, and a third trip a week later to make sure she has completely aborted.	B-conjunction	4918..5347
Furthermore, because timing is so critical with RU-486, she will learn, via a pelvic examination and ultrasound, not only that she is pregnant, but just how pregnant she is.	I-conjunction	5350..5523
Furthermore, because timing is so critical with RU-486, she will learn, via a pelvic examination and ultrasound, not only that she is pregnant, but just how pregnant she is.No doctor who fears malpractice liability would likely expose a non-pregnant patient to the risk of hemorrhaging.	B-conjunction	5524..5637
Many women may even see the dead embryo they have expelled, a sight the surgical-abortion industry typically spares them.	B-conjunction	5638..5759
At seven weeks, an embryo is about three-fourths of an inch long and recognizably human.	I-conjunction	5760..5848
At the behest of pro-choice members of Congress, a four-year reauthorization bill for Title X federal family-planning assistance now contains a $10 million grant for "development, evaluation and bringing to the marketplace of new improved contraceptive devices, drugs and methods."	O	5851..6132
If this passes -- a Senate version has already been cleared for a floor vote that is likely early next year -- it would put the federal government into the contraceptive marketing business for the first time.	O	6133..6341
It also could put the government into the RU-486 business, which would please feminists dismayed at what they view as pusillanimity in the private-sector drug industry.	O	6342..6510
We do not know whether RU-486 will be as disastrous as some of the earlier fertility-control methods released to unblinking, uncritical cheers from educated people who should have known better.	B-instantiation	6513..6706
(Remember the Dalkon Shield and the early birth-control pills?)	I-instantiation	6707..6770
We will not know until a first generation of female guinea pigs -- all of whom will be more than happy to volunteer for the job -- has put the abortion pill through the clinical test of time.	O	6771..6962
Mrs. Allen is a senior editor of Insight magazine.	B-conjunction	6965..7015
This article is adapted from one in the October American Spectator.	I-conjunction	7016..7083
