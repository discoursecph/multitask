Although bullish dollar sentiment has fizzled ,	(NN(NN(NS(NS(NS(NS(SN
many currency analysts say	(SN
a massive sell-off probably wo n't occur in the near future .	SN))
While Wall Street 's tough times and lower U.S. interest rates continue to undermine the dollar ,	(SN
weakness in the pound and the yen is expected to offset those factors .	SN))
`` By default , '' the dollar probably will be able to hold up pretty well in coming days ,	(NS(NS
says Francoise Soares-Kemp , a foreign-exchange adviser at Credit Suisse .	NS)
`` We 're close to the bottom '' of the near-term ranges ,	(NS
she contends .	NS)))
In late Friday afternoon New York trading , the dollar was at 1.8300 marks and 141.65 yen , off from late Thursday 's 1.8400 marks and 142.10 yen .	(NS(NS(NN(NN
The pound strengthened to $ 1.5795 from $ 1.5765 .	NN)
In Tokyo Monday , the U.S. currency opened for trading at 141.70 yen , down from Friday 's Tokyo close of 142.75 yen .	NN)
The dollar began Friday on a firm note ,	(NN(NS(NS
gaining against all major currencies in Tokyo dealings and early European trading	(NS
despite reports	(NS
that the Bank of Japan was seen unloading dollars around 142.70 yen .	NS)))
The rise came	(NS
as traders continued to dump the pound after the sudden resignation Thursday of British Chancellor of the Exchequer Nigel Lawson .	NS))
But	(NN(NS(NN
once the pound steadied with help from purchases by the Bank of England and the Federal Reserve Bank of New York ,	(SN
the dollar was dragged down ,	SN))
traders say ,	NS)
by the stock-market slump	(NS
that left the Dow Jones Industrial Average with a loss of 17.01 points .	NS))))
With the stock market wobbly and dollar buyers	(NN(SN(NN(NS
discouraged by signs of U.S. economic weakness and the recent decline in U.S. interest rates	(NS
that has diminished the attractiveness of dollar-denominated investments ,	NS))
traders say	NN)
the dollar is still in a precarious position .	SN)
`` They 'll be looking at levels	(NS(NS(NS
to sell the dollar , ''	NS)
says James Scalfaro , a foreign-exchange marketing representative at Bank of Montreal .	NS)
While some analysts say	(SN(SN
the dollar eventually could test support at 1.75 marks and 135 yen ,	SN)
Mr. Scalfaro and others do n't see the currency decisively sliding under support at 1.80 marks and 140 yen soon .	SN)))))
Predictions for limited dollar losses are based largely on the pound 's weak state after Mr. Lawson 's resignation and the yen 's inability	(NS(NS(NS
to strengthen substantially	NS)
when there are dollar retreats .	NS)
With the pound and the yen lagging behind other major currencies ,	(NS(SN
`` you do n't have a confirmation ''	(NS
that a sharp dollar downturn is in the works ,	NS))
says Mike Malpede , senior currency analyst at Refco Inc. in Chicago .	NS)))
As far as the pound goes ,	(NN(NS(NS(NS(NS(SN
some traders say	(SN
a slide toward support at $ 1.5500 may be a favorable development for the dollar this week .	SN))
While the pound has attempted to stabilize ,	(SN
currency analysts say	(SN
it is in critical condition .	SN)))
Sterling plunged about four cents Thursday	(NS(NN
and hit the week 's low of $ 1.5765	NN)
when Mr. Lawson resigned from his six-year post	(NN(NS
because of a policy squabble with other cabinet members .	NS)
He was succeeded by John Major ,	(NS(NS
who Friday expressed a desire for a firm pound	(NN
and supported the relatively high British interest rates	(NS
that	(NN(NS
he said	NS)
`` are working exactly as intended ''	(NS
in reducing inflation .	NS)))))
But the market remains uneasy about Mr. Major 's policy strategy and the prospects for the pound ,	(NS
currency analysts contend .	NS)))))
Although the Bank of England 's tight monetary policy has fueled worries	(NS(NS(SN(NS
that Britain 's slowing economy is headed for a recession ,	NS)
it is widely believed that Mr. Lawson 's willingness	(NN(NS
to prop up the pound with interest-rate increases	NS)
helped stem pound selling in recent weeks .	NN))
If there are any signs	(SN(NS
that Mr. Major will be less inclined to use interest-rate boosts	(NS
to rescue the pound from another plunge ,	NS))
that currency is expected to fall sharply .	SN))
`` It 's fair to say there are more risks for the pound under Major	(NN(NS(NS(NS
than there were under Lawson , ''	NS)
says Malcolm Roberts , a director of international bond market research at Salomon Brothers in London .	NS)
`` There 's very little upside to sterling , ''	(SN(NS
Mr. Roberts says ,	NS)
but he adds	(SN
that near-term losses may be small	(NS
because the selling wave	(NN(NS
that followed Mr. Major 's appointment	NS)
apparently has run its course .	NN)))))
But some other analysts have a stormier forecast for the pound ,	(NS(NS
particularly because Britain 's inflation is hovering at a relatively lofty annual rate of about 7.6 %	(NN
and the nation is burdened with a struggling government and large current account and trade deficits .	NN))
The pound likely will fall in coming days	(NS(NS(NS
and may trade as low as 2.60 marks within the next year ,	NS)
says Nigel Rendell , an international economist at James Capel & Co. in London .	NS)
The pound was at 2.8896 marks late Friday , off sharply from 2.9511 in New York trading a week earlier .	(NS
If the pound falls closer to 2.80 marks ,	(NS(NS(SN
the Bank of England may raise Britain 's base lending rate by one percentage point to 16 % ,	SN)
says Mr. Rendell .	NS)
But such an increase ,	(NN(NS
he says ,	NS)
could be viewed by the market as `` too little too late . ''	NN))))))))
The Bank of England indicated its desire	(NS(NS(NS
to leave its monetary policy unchanged Friday	NS)
by declining to raise the official 15 % discount-borrowing rate	(NS
that it charges discount houses ,	NS))
analysts say .	NS))
Pound concerns aside , the lack of strong buying interest in the yen is another boon for the dollar ,	(NS(NS
many traders say .	NS)
The dollar has a `` natural base of support '' around 140 yen	(NS(NS(NS
because the Japanese currency has n't been purchased heavily in recent weeks ,	NS)
says Ms. Soares-Kemp of Credit Suisse .	NS)
The yen 's softness ,	(NN(NS
she says ,	NS)
apparently stems from Japanese investors ' interest	(NN(NS
in buying dollars against the yen	(NS
to purchase U.S. bond issues	NS))
and persistent worries about this year 's upheaval in the Japanese government .	NN))))))
On New York 's Commodity Exchange Friday , gold for current delivery jumped $ 5.80 , to $ 378.30 an ounce , the highest settlement since July 12 .	(NN(NS
Estimated volume was a heavy seven million ounces .	NS)
In early trading in Hong Kong Monday , gold was quoted at $ 378.87 an ounce .	NN))
