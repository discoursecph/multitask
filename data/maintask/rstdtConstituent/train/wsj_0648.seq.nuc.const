Insurers may see claims	(NN-Topic-Change(NS-Summary(SN-Contrast(NS-Elaboration(NS-Elaboration
resulting from the San Francisco earthquake	NS-Elaboration)
totaling nearly $ 1 billion	(NS-Comparison
-- far less than the claims	(NS-Elaboration
they face from Hurricane Hugo --	NS-Elaboration)))
but the recent spate of catastrophes should jolt property insurance rates in coming months .	SN-Contrast)
The property claims service division of the American Insurance Services Group estimated insured losses from the earthquake at $ 960 million .	(NS-Elaboration(NS-Elaboration(SN-Contrast(NS-Elaboration(NS-Elaboration
This estimate does n't include claims under workers ' compensation , life , health disability and liability insurance and damage to infrastructure such as bridges , highways and public buildings .	NS-Elaboration)
The estimated earthquake losses are low	(NS-Explanation(NS-Comparison
compared with the $ 4 billion in claims	(NS-Elaboration
that insurers face from Hurricane Hugo ,	(NS-Elaboration
which ripped through the Caribbean and the Carolinas last month .	NS-Elaboration)))
That 's because only about 30 % of California homes and businesses had earthquake insurance	(NS-Elaboration
to cover the losses .	NS-Elaboration)))
However , insurance brokers and executives say	(SN-Attribution
that the combination of the Bay area earthquake , Hugo and last week 's explosion at the Phillips Petroleum Co. 's refinery in Pasadena , Texas , will cause property insurance and reinsurance rates to jump .	SN-Attribution))
Other insurance rates such as casualty insurance ,	(SN-Background(NS-Attribution(NN-Same-unit(NS-Elaboration
which would cover liability claims ,	NS-Elaboration)
are n't likely to firm right away ,	NN-Same-unit)
says Alice Cornish , an industry analyst with Northington Research in Avon , Conn .	NS-Attribution)
She believes	(SN-Attribution
the impact of losses from these catastrophes is n't likely to halt the growth of the industry 's surplus capital next year .	SN-Attribution)))
Property reinsurance rates are likely to climb first ,	(NS-Explanation(NS-Attribution
analysts and brokers believe .	NS-Attribution)
`` The reinsurance market has been bloodied by disasters '' in the U.S. as well as in Great Britain and Europe ,	(NS-Elaboration(NS-Explanation(NS-Attribution
says Thomas Rosencrants , director of research at Interstate/Johnson Lane Inc. in Atlanta .	NS-Attribution)
Insurers typically retain a small percentage of the risks	(SN-Background(NS-Elaboration(NN-Joint(NS-Elaboration
they underwrite	NS-Elaboration)
and pass on the rest of the losses .	NN-Joint)
Insurers buy this insurance protection for themselves	(NS-Elaboration(NS-Manner-Means
by giving up a portion of the premiums	(NN-Same-unit(NS-Elaboration
they collect on a policy	NS-Elaboration)
to another firm	(NS-Elaboration
-- a reinsurance company ,	(NS-Elaboration
which , in turn , accepts a portion of any losses	(NS-Elaboration
resulting from this policy .	NS-Elaboration)))))
Insurers , such as Cigna Corp. , Transamerica Corp , and Aetna Life & Casualty Co. , buy reinsurance from other U.S.-based companies and Lloyd 's of London for one catastrophe at a time .	NS-Elaboration))
After Hugo hit ,	(NN-Temporal(SN-Explanation(SN-Temporal
many insurers exhausted their reinsurance coverage	SN-Temporal)
and had to tap reinsurers	(NS-Condition(NS-Enablement
to replace that coverage	NS-Enablement)
in case there were any other major disasters before the end of the year .	NS-Condition))
After the earthquake two weeks ago ,	(NN-Same-unit(NS-Attribution
brokers say	NS-Attribution)
companies scrambled to replace reinsurance coverages again	(NS-Cause
and Lloyd 's syndicates turned to the London market excess lines for protection of their own .	NS-Cause)))))
James Snedeker , senior vice president of Gill & Roeser Inc. , a New York-based reinsurance broker , says	(NS-Contrast(SN-Evaluation(SN-Background(NN-Contrast(SN-Attribution
insurers	(NN-Same-unit(NS-Elaboration
who took big losses this fall	(NN-Joint
and had purchased little reinsurance in recent years	NN-Joint))
will be asked to pay some pretty hefty rates	(NS-Condition
if they want to buy reinsurance for 1990 .	NS-Condition)))
However , companies with few catastrophe losses this year and already big buyers of reinsurance are likely to see their rates remain flat , or perhaps even decline slightly .	NN-Contrast)
Many companies will be negotiating their 1990 reinsurance contracts in the next few weeks .	SN-Background)
`` It 's a seller 's market , ''	(NS-Attribution
said Mr. Snedeker of the reinsurance market right now .	NS-Attribution))
But some large insurers , such as State Farm Mutual Automobile Insurance Co. , do n't purchase reinsurance ,	(NS-Elaboration(NN-Joint
but fund their own program .	NN-Joint)
A few years ago , State Farm , the nation 's largest home insurer , stopped buying reinsurance	(NS-Explanation(SN-Cause(NS-Explanation
because no one carrier could provide all the coverage	(NS-Elaboration
that it needed	NS-Elaboration))
and the company found it cheaper to self-reinsure .	SN-Cause)
The $ 472 million of losses	(NN-Same-unit(NS-Elaboration
State Farm expects from Hugo	NS-Elaboration)
and an additional $ 300 million from the earthquake are less than 5 % of State Farm 's $ 16.7 billion total net worth .	NN-Same-unit))))))))
Since few insurers have announced what amount of losses	(NS-Elaboration(NS-Attribution(SN-Explanation(NS-Elaboration
they expect to see from the earthquake ,	NS-Elaboration)
it 's impossible to get a clear picture of the quake 's impact on fourth-quarter earnings ,	SN-Explanation)
said Herbert Goodfriend at Prudential-Bache Securities Corp .	NS-Attribution)
Transamerica expects an after-tax charge of less than $ 3 million against fourth-quarter net ;	(NN-Joint
Hartford Insurance Group , a unit of ITT Corp. , expects a $ 15 million or 10 cents after-tax charge ;	(NN-Joint
and Fireman 's Fund Corp. expects a charge of no more than $ 50 million before taxes	(NS-Temporal
and after using its reinsurance .	NS-Temporal)))))
