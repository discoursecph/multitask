With airline deals in a tailspin, legendary Wall Street trader Michael Steinhardt could have trouble parachuting out of USAir Group, traders say.	O	9..154
Only a week ago, when airline buy-out fever was already winding down, Mr. Steinhardt was engaged in a duel with USAir.	O	157..275
He was threatening to take over the carrier, after spending an estimated $167 million to build an 8.4% USAir stake for his investment clients.	O	276..418
The would-be raider even hired an investment banker to give teeth to his takeover threat, which was widely interpreted as an effort to flush out an acquirer for USAir, or for his own stake.	O	419..608
In fighting USAir, Mr. Steinhardt was pitted against another investor, billionnaire Warren Buffett, who bought into USAir to help fend off Mr. Steinhardt.	B-entrel	611..765
Mr. Buffett's firm, Berkshire Hathaway, holds a much bigger stake in the carrier than Mr. Steinhardt's firm, Steinhardt Partners.	I-entrel	766..895
Now, in the wake of UAL's troubles in financing its buy-out, the airline raiding game has been grounded.	B-cause	898..1002
Instead of hoping to sell his USAir stake at analysts' estimated buy-out price of $80 a share, Mr. Steinhardt is stuck with roughly 3.7 million USAir shares that cost him $45, on average, but yesterday closed at 40 1/2, up 1/4, in New York Stock Exchange composite trading.	I-cause	1003..1276
"It doesn't make sense to parachute out at this price," Mr. Steinhardt says, though he has stopped his takeover talk and now commends USAir managers' "operating skills."	O	1279..1448
At the current price, the USAir holding represents 9% of all the assets that Mr. Steinhardt manages.	B-entrel	1451..1551
A week ago, USAir stock briefly soared above 52 after a report in USA Today that Mr. Steinhardt might launch a hostile bid for the carrier, though takeover speculators say they were skeptical.	I-entrel	1552..1744
"If USAir is worth 80 as a takeover and the stock went to 52, the market was saying Steinhardt's presence wasn't worth anything, in terms of getting a deal done," says a veteran takeover speculator.	O	1747..1945
Traders say this all goes to show that even the smartest money manager can get infected with crowd passions.	O	1948..2056
In trying to raid USAir, Mr. Steinhardt abandoned his usual role as a passive investor, and ran into snags.	B-conjunction	2057..2164
Moreover, unlike Mr. Buffett, who often holds big stakes in companies for years, Mr. Steinhardt hasn't in the past done much long-term investing.	I-conjunction	2165..2310
Mr. Steinhardt, who runs about $1.7 billion for Steinhardt Partners, made his name as a gunslinging trader, moving in and out of stocks with agility -- enriching himself and his investment clients.	O	2313..2510
Mr. Steinhardt, who runs about $1.7 billion for Steinhardt Partners, made his name as a gunslinging trader, moving in and out of stocks with agility -- enriching himself and his investment clients.Meanwhile, his big losses, for instance in 1987's crash, generally have been trading losses.	B-cause	2511..2603
So, some see a special irony in the fact that Mr. Steinhardt, the trader, now is encumbered with a massive, illiquid airline holding.	I-cause	2606..2739
Analysts say USAir stock might lose four or five points if the Steinhardt stake was dumped all at once.	O	2740..2843
As a result, Mr. Steinhardt must reconcile himself to selling USAir at a loss, or to holding the shares as an old-fashioned investment.	O	2844..2979
"Long-term investing -- that's not Steinhardt's style," chuckles an investor who once worked at Steinhardt Partners.	O	2982..3098
"He doesn't usually risk that much unless he thinks he has an ace in the hole," adds another Steinhardt Partners alumnus.	O	3099..3220
In recent days, traders say USAir has been buying its own shares, as part of a program to retire about eight million USAir shares, though the carrier won't discuss its buy-back program.	O	3223..3408
If USAir stepped up its share purchases, that might be a way for Mr. Steinhardt to get out, says Timothy Pettee, a Merrill Lynch analyst.	O	3411..3548
But USAir might not want to help Mr. Steinhardt, he adds.	O	3549..3606
In 1987, USAir Chairman Edwin Colodny stonewalled when Trans World Airlines Chairman Carl Icahn threatened to take over the carrier.	B-asynchronous	3609..3741
Mr. Icahn, a much more practiced raider than Mr. Steinhardt, eventually sold a big USAir stake at a tiny profit through Bear, Stearns.	B-entrel	3742..3876
Mr. Icahn, a much more practiced raider than Mr. Steinhardt, eventually sold a big USAir stake at a tiny profit through Bear, Stearns.	B-conjunction	3742..3876
Mr. Steinhardt also could take that route.	B-cause	3877..3919
He confers big trading commissions on Wall Street firms.	I-cause	3920..3976
However, with airline stocks cratering, he might not get a very good price for his shares, traders say.	O	3977..4080
Especially galling for Mr. Steinhardt, say people close to him, is that USAir's Mr. Colodny won't even take his telephone calls.	B-entrel	4083..4211
While USAir isn't considered absolutely takeover-proof, its defenses, including the sale in August of a 12% stake in the company to Mr. Buffett's Berkshire Hathaway, are pretty strong.	I-entrel	4212..4396
USAir's deal with Mr. Buffett "wasn't exactly a shining example of shareholder democracy," Mr. Steinhardt says.	B-entrel	4399..4510
Since last April, the investor has made seven so-called 13D filings in USAir, as he bought and sold the company's stock.	B-entrel	4511..4631
Such disclosures of big holdings often are used by raiders to try to scare a company's managers, and to stir interest in the stock.	B-concession	4632..4763
But of course it would be highly unusual for an investment fund such as Steinhardt Partners to take over a company.	I-concession	4764..4879
USAir and Mr. Buffett won't talk about Mr. Steinhardt at all.	O	4882..4943
Analysts say USAir has great promise.	O	4946..4983
By the second half of 1990, USAir stock could hit 60, says Helane Becker of Shearson Lehman Hutton.	O	4984..5083
She thinks traders should buy the stock if it tumbles to 35.	O	5084..5144
But meanwhile, USAir is expected to show losses or lackluster profit for several quarters as it tries to digest Piedmont Airlines, which it acquired.	B-conjunction	5147..5296
Moreover, some investors think a recession or renewed airfare wars will pummel airline stocks in coming months.	I-conjunction	5297..5408
However, Mr. Steinhardt says he's "comfortable holding USAir as an investment."	O	5411..5490
While he has bought and sold some USAir shares in recent days, he says that contrary to rumors, he hasn't tried to unload his holding.	O	5491..5625
Mr. Steinhardt adds that he bought USAir stock earlier this year as "part of a fundamental investment in the airline group."	O	5626..5750
In 1989, Mr. Steinhardt says he made money trading in Texas Air, AMR and UAL.	O	5753..5830
Overall, his investments so far this year are showing gains of about 20%, he adds.	O	5831..5913
Does Mr. Steinhardt regret his incursion into the takeover-threat game?	O	5916..5987
People close to the investor say that was an experiment he is unlikely to repeat.	O	5988..6069
"I don't think you'll find I'm making a radical change in my traditional investment style," Mr. Steinhardt says.	O	6070..6182
