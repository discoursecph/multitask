The turmoil on Wall Street may benefit some retailers attempting to lead leveraged buy-outs of their specialty and department-store chains , investment bankers and retailers said .	root	9..187
Managers at five chains have said in recent weeks that they intend to bid for their companies .	norel	190..284
The chains include Bloomingdale 's , owned by Campeau Corp. , Toronto ; Saks Fifth Avenue and Marshall Field 's , owned by B.A.T Industries PLC , London ; and B. Altman & Co. and Sakowitz Inc. , owned by Hooker Corp. , which is now being managed by a court-appointed provisional liquidator .	instantiation	285..565
Hooker is based in Sydney , Australia .	entrel	566..603
The combination of so many chains available for sale , the recent failures of such retailing LBO 's as Miller & Rhoads Inc. and declining investor confidence will drive down prices , retailing observers said .	norel	606..811
" The pricing will become more realistic , which should help management , " said Bruce Rosenthal , a New York investment banker with Nathan S. Jonas & Co .	norel	814..963
" Investors are n't going to be throwing money at any of the proposed LBOs , but doing deals on the basis of ridiculous assumptions never made sense , either . "	cause	964..1119
Earlier this year , bankers and other investors were willing to provide financing because they assumed there would be major gains in both profitability and sales , Mr. Rosenthal added .	norel	1122..1304
Those days are over now , he believes .	contrast	1305..1342
" Competition from third parties who have cash and are prepared to buy has always existed and will continue , " added Mr. Rosenthal .	norel	1345..1474
" But when prices were crazy , it was even harder to do an LBO .	concession	1475..1536
Bankers believed in the greater-fool theory that says somebody else is always willing to pay more .	cause	1537..1635
This is no longer true today . "	contrast	1636..1666
At Saks Fifth Avenue , Paul Leblang , senior vice president , marketing , agreed that lower prices will help his management team in their proposed LBO .	norel	1669..1816
" Having to take on less debt would certainly be an advantage , " said Mr. Leblang .	norel	1819..1899
" It would also help us in our search for equity partners .	conjunction	1900..1957
To make an LBO work , now we are going to need more than just junk bonds .	cause	1958..2030
" None believe the proposed management LBOs will be easy to complete , especially at B. Altman & Co. , which is under Chapter 11 bankruptcy protection .	entrel	2030..2179
Not only could the Wall Street gyrations damp Christmas sales if consumers lose confidence in the economy	cause	2180..2285
but potential junk-bond buyers are sure to demand even stronger covenants and greater management equity participation .	conjunction	2286..2405
Further , many institutions today holding troubled retailers ' debt securities will be reticent to consider additional retailing investments .	norel	2408..2547
" It 's called bad money driving out good money , " said one retailing observer .	restatement	2548..2624
" Institutions that usually buy retail paper have to be more concerned . "	cause	2625..2696
However , the lower prices these retail chains are now expected to bring should make it easier for managers to raise the necessary capital and pay back the resulting debt .	norel	2699..2869
In addition , the fall selling season has generally been a good one , especially for those retailers dependent on apparel sales for the majority of their revenues .	conjunction	2870..3031
" What 's encouraging about this is that retail chains will be sold on the basis of their sales and earnings , not liquidation values , " said Joseph E. Brooks , chairman and chief executive officer of Ann Taylor Inc. , a specialty chain .	norel	3034..3265
" Retailers who had good track records of producing profits will have a better chance to buy back their companies . "	cause	3266..3380
Still , most retailing observers expect that all the proposed retailing LBOs will depend partly on the sale of junk bonds , a market already in tumult , in part because of concerns associated with bonds issued by the Federated and Allied units of Campeau .	norel	3383..3635
" Prices for retail chains are lower today than they were last week , which will help management , " said Gilbert Harrison , chairman of Financo Inc. , an investment-banking firm specializing in retailing acquisitions .	norel	3638..3850
" But the hurdle of financing still has to be resolved .	concession	3851..3905
Potential bondholders will either look for greater equity participation on behalf of management , or insist the equity component of the deals be substantially greater than in the past .	restatement	3906..4089
