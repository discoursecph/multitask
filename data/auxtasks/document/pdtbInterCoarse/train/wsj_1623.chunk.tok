NOW YOU SEE IT , now you do n't .	O	9..39
The recession , that is .	B-contingency	42..65
The economy 's stutter steps leave investors wondering whether things are slowing down or speeding up .	B-expansion	66..167
So often are government statistics revised that they seem to resemble a spinning weather vane .	I-expansion	168..262
For the past seven years , investors have had the wind at their backs , in the form of a generally growing economy .	O	265..378
Some may have forgotten -- and some younger ones may never have experienced -- what it 's like to invest during a recession .	O	379..502
Different tactics are called for , as losing money becomes easier and making money becomes tougher .	O	503..601
For those investors who believe -- or fear -- that 1990 will be a recession year , many economists and money managers agree on steps that can be taken to lower the risks in a portfolio .	O	604..788
In a nutshell , pros advise investors who expect a slowdown to hold fewer stocks than usual and to favor shares of big companies in " defensive " industries .	B-expansion	791..945
A heavy dose of cash is prescribed , along with a heavier-than-usual allotment to bonds -- preferably government bonds .	I-expansion	946..1064
It 's tempting to think these defensive steps can be delayed until a recession is clearly at hand .	B-comparison	1067..1164
But that may not be possible , because recessions often take investors by surprise .	I-comparison	1165..1247
" They always seem to come a bit later than you expect .	O	1248..1302
When they do hit , they hit fast , " says David A. Wyss , chief financial economist at the Data Resources division of McGraw-Hill Inc .	O	1303..1433
Though he himself does n't expect a recession soon , Mr. Wyss advises people who do that " the best thing to be in is long that is , 20-year to 30-year Treasury bonds . "	O	1436..1600
The reason is simple , Mr. Wyss says : " Interest rates almost always decline during recession . "	O	1603..1696
As surely as a seesaw tilts , falling interest rates force up the price of previously issued bonds .	B-expansion	1697..1795
They are worth more because they pay higher interest than newly issued bonds do .	I-expansion	1796..1876
That effect holds true for both short-term and long-term bonds .	B-comparison	1879..1942
But short-term bonds ca n't rise too much , because everyone knows they will be redeemed at a preset price fairly soon .	I-comparison	1943..2060
Long-term bonds , with many years left before maturity , swing more widely in price .	B-comparison	2061..2143
But not just any bonds will do .	I-comparison	2146..2177
Corporate bonds " are usually not a good bet in a recession , " Mr. Wyss says .	O	2178..2253
As times get tougher , investors fret about whether companies will have enough money to pay their debts .	B-contingency	2254..2357
This hurts the price of corporate bonds .	I-contingency	2358..2398
Also , he notes , " most corporate bonds are callable . "	O	2399..2451
That means that a corporation , after a specified amount of time has passed , can buy back its bonds by paying investors the face value ( plus , in some cases , a sweetener ) .	O	2454..2623
When interest rates have dropped , it makes sense for corporations to do just that ; they then save on interest costs .	O	2624..2740
But the investors are left stranded with money to reinvest at a time when interest rates are puny .	O	2741..2839
If corporate bonds are bad in recessions , junk bonds are likely to be the worst of all .	O	2842..2929
It 's an " absolute necessity " to get out of junk bonds when a recession is in the offing , says Avner Arbel , professor of finance at Cornell University .	O	2930..3080
" Such bonds are very sensitive to the downside , and this could be a disaster . "	O	3081..3159
Municipal bonds are generally a bit safer than corporate bonds in a recession , but not as safe as bonds issued by the federal government .	B-contingency	3162..3299
During an economic slump , local tax revenues often go down , raising the risks associated with at least some municipals .	I-contingency	3300..3419
And , like corporates , many municipal bonds are callable .	O	3420..3476
But a few experts , going against the consensus , do n't think bonds would help investors even if a recession is in the offing .	O	3479..3603
One of these is Jeffrey L. Beach , director of research for Underwood Neuhaus & Co. , a brokerage house in Houston , who thinks that " we 're either in a recession or about to go into one . "	O	3604..3788
What 's more , he thinks this could be a nastier recession than usual : " Once the downturn comes , it 's going to be very hard to reverse . "	O	3789..3923
Investors , he advises , " should be cautious , " holding fewer stocks than usual and also shunning bonds .	O	3926..4027
Because he sees a " 5 % to 6 % base rate of inflation in the economy , " he doubts that interest rates will fall much any time soon .	O	4028..4155
Instead , Mr. Beach says , investors " probably should be carrying a very high level of cash , " by which he means such so-called cash equivalents as money-market funds and Treasury bills .	O	4158..4341
Greg Confair , president of Sigma Financial Inc. in Allentown , Pa. , also recommends that investors go heavily for cash .	O	4342..4460
He is n't sure a recession is coming , but says the other likely alternative -- reignited inflation -- is just as bad .	O	4461..4577
" This late in an expansion , " the economy tends to veer off either into damaging inflation or into a recession , Mr. Confair says .	O	4580..4708
The Federal Reserve Board 's plan for a " soft landing , " he says , requires the Fed to navigate " an ever-narrowing corridor . "	O	4709..4831
A soft landing is n't something that can be achieved once and for all , Mr. Confair adds .	O	4834..4921
It has to be engineered over and over again , month after month .	O	4922..4985
He believes that the task facing Fed Chairman Alan Greenspan is so difficult that it resembles " juggling a double-bladed ax and a buzz saw . "	B-expansion	4986..5126
And , in a sense , that 's the kind of task individuals face in deciding what to do about stocks -- the mainstay of most serious investors ' portfolios .	I-expansion	5129..5277
It comes down to a question of whether to try to " time " the market .	O	5278..5345
For people who can ride out market waves through good times and bad , stocks have been rewarding long-term investments .	O	5348..5466
Most studies show that buy-and-hold investors historically have earned an annual return from stocks of 9 % to 10 % , including both dividends and price appreciation .	B-entrel	5467..5629
That 's well above what bonds or bank certificates have paid .	I-entrel	5630..5690
Moreover , because no one knows for sure just when a recession is coming , some analysts think investors should n't even worry too much about timing .	O	5693..5839
" Trying to time the economy is a mistake , " says David Katz , chief investment officer of Value Matrix Management Inc. in New York .	O	5840..5969
Mr. Katz notes that some economists have been predicting a recession for at least two years .	O	5970..6062
Investors who listened , and lightened up on stocks , " have just hurt themselves , " he says .	O	6063..6152
Mr. Katz adds that people who jump in and out of the stock market need to be right about 70 % of the time to beat a buy-and-hold strategy .	O	6155..6292
Frequent trading runs up high commission costs .	B-expansion	6293..6340
And the in-and-outer might miss the sudden spurts that account for much of the stock market 's gains over time .	B-comparison	6341..6451
Still , few investors are able to sit tight when they are convinced a recession is coming .	B-contingency	6454..6543
After all , in all five recessions since 1960 , stocks declined .	B-expansion	6544..6606
According to Ned Davis , president of Ned Davis Research Inc. in Nokomis , Fla. , the average drop in the Dow Jones Industrial Average was about 21 % , and the decrease began an average of six months before a recession officially started .	I-expansion	6607..6840
By the time a recession is " official " ( two consecutive quarters of declining gross national product ) , much of the damage to stocks has already been done - and , in the typical case , the recession is already half over .	O	6843..7057
About six months before a recession ends , stocks typically begin to rise again , as investors anticipate a recovery .	O	7058..7173
The average recession lasts about a year .	B-comparison	7176..7217
Unfortunately , though , recessions vary enough in length so that the average ca n't reliably be used to guide investors in timing stock sales or purchases .	B-comparison	7218..7371
But whatever their advice about timing , none of these experts recommend jettisoning stocks entirely during a recession .	B-entrel	7374..7493
For the portion of an investor 's portfolio that stays in stocks , professionals have a number of suggestions .	I-entrel	7494..7602
Mr. Katz advocates issues with low price-earnings ratios -- that is , low prices in relation to the company 's earnings per share .	O	7603..7731
" Low P-E " stocks , he says , vastly outperform others " during a recession or bear market . "	O	7732..7820
In good times , he says , they lag a bit , but overall they provide superior performance .	O	7821..7907
Prof. Arbel urges investors to discard stocks in small companies .	O	7910..7975
Small-company shares typically fall more than big-company stocks in a recession , he says .	B-expansion	7976..8065
And in any case , he argues , stocks of small companies are " almost as overpriced as they were Sept. 30 , 1987 , just before the crash . "	I-expansion	8066..8198
For example , Mr. Arbel says , stocks of small companies are selling for about 19 times cash flow .	B-entrel	8201..8297
( Cash flow , basically earnings plus depreciation , is one common gauge of a company 's financial health . )	I-entrel	8298..8401
That ratio is dangerously close to the ratio of 19.7 that prevailed before the 1987 stock-market crash , Mr. Arbel says .	O	8402..8521
And it 's way above the ratio ( 7.5 times cash flow ) that bigger companies are selling for .	O	8522..8611
Another major trick in making a portfolio recession-resistant is choosing stocks in " defensive " industries .	B-entrel	8614..8721
Food , tobacco , drugs and utilities are the classic examples .	B-contingency	8722..8782
Recession or not , people still eat , smoke , and take medicine when they 're sick .	I-contingency	8783..8862
George Putnam III , editor of Turnaround Letter in Boston , offers one final tip for recession-wary investors .	B-expansion	8865..8973
" Keep some money available for opportunities , " he says .	I-expansion	8974..9029
" If the recession does hit , there will be some great investment opportunities just when things seem the blackest . "	O	9030..9144
Mr. Dorfman covers investing issues from The Wall Street Journal 's New York bureau .	O	9147..9230
Some industry groups consistently weather the storm better than others .	B-entrel	9233..9304
The following shows the number of times these industries outperformed the Standard & Poor 's 500-Stock Index during the first six months of the past seven recessions .	I-entrel	9305..9470
