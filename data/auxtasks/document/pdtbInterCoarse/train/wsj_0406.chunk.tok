( During its centennial year , The Wall Street Journal will report events of the past century that stand as milestones of American business history . )	O	9..156
MAY 1 , 1975 , SIGNALED A DISTRESSFUL May Day for securities houses , which were forced to end 183 years of charging fixed commissions .	B-entrel	159..291
It scared brokers , but most survived .	I-entrel	292..329
It took effect after seven years of bitter debate between the Securities and Exchange Commission and traders and exchanges .	O	332..455
Despite warnings from such leaders as former Federal Reserve Board Chairman William McChesney Martin that unfixed commissions would undo the industry , the SEC in September 1973 said full competition must start May l , 1975 .	O	456..678
The timing for change was right .	B-contingency	681..713
Institutions had become active market players in the early 1970s and sought exchange seats to handle their own trades .	B-expansion	714..832
And the industry was rife with brokers trying to secure big client orders by using kickbacks , gifts , women and junkets .	I-expansion	833..952
Within three weeks of the 1975 end to fixed rates there were all-out price wars among brokers fighting for institutional business , with rate slashes of 35 % to 60 % below pre-May 1 levels .	O	955..1141
Ray Garrett Jr. , SEC chairman , said the " breadth and depth of the discounting is more than I expected . "	O	1142..1245
Even a federal measure in June allowing houses to add research fees to their commissions did n't stop it .	O	1246..1350
Longer term , the impact is unclear .	B-contingency	1353..1388
The change prompted the rise of discount brokers and a reduction in securities research firms .	B-comparison	1389..1483
But there are currently more exchange members than in 1975 , with the bigger houses gaining a larger share of total commissions .	I-comparison	1484..1611
Commissions , however , account for a smaller share of investment-house business as takeover advisory fees have soared .	O	1612..1729
Foreign stock markets , with which the U.S. is entwined , also have ended fixed commissions in recent years .	O	1732..1838
It came in London 's " Big Bang " 1986 deregulation ; and Toronto 's " Little Bang " the same year .	B-expansion	1839..1931
Paris is currently phasing out fixed commissions under its " Le Petit Bang " plan .	I-expansion	1932..2012
