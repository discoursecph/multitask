After	O	0
a	O	1
show	O	2
of	O	3
combat	O	4
skills	O	5
in	O	6
the	O	7
techniques	O	8
of	O	9
karate	O	10
and	O	11
Kung	O	12
Fu	O	13
,	O	14
presented	O	15
by	O	16
Al	B-ORG	17
-	I-ORG	18
Azhar	I-ORG	19
University	I-ORG	20
's	I-ORG	21
hooded	O	22
students	O	23
wearing	O	24
black	O	25
uniforms	O	26
during	O	27
a	O	28
demonstration	O	29
at	O	30
the	O	31
University	O	32
the	O	33
day	O	34
before	O	35
yesterday	O	36
in	O	37
protest	O	38
of	O	39
the	O	40
expulsion	O	41
of	O	42
their	O	43
colleagues	O	44
who	O	45
are	O	46
members	O	47
of	O	48
the	O	49
so	O	50
-	O	51
called	O	52
"	O	53
Union	B-ORG	54
of	I-ORG	55
Free	I-ORG	56
Students	I-ORG	57
,	O	58
"	O	59
the	B-ORG	60
Leftist	I-ORG	61
Tagammu	I-ORG	62
Party	I-ORG	63
accused	O	64
the	O	65
Moslem	B-ORG	66
Brotherhood	I-ORG	67
group	O	68
of	O	69
returning	O	70
to	O	71
"	O	72
secret	O	73
organizing	O	74
.	O	75
"	O	76
