Benjamin Jacobson & Sons has been the New York Stock Exchange specialist firm in charge of trading stock in UAL Corp. and its predecessors since the early 1930s .	(Summary(Explanation(Background
But the firm has never had a day like yesterday .	Background)
At first UAL did n't open	(Background(Temporal(Explanation
because of an order imbalance .	Explanation)
When it did a half-hour into the session ,	(Temporal(Background
it was priced at $ 150 a share , down more than $ 28 from Monday 's close .	Background)
It sank further to as low as $ 145 ,	(Contrast
but a big rally developed in the last half hour ,	(Elaboration
pushing the stock back up to close at $ 170 , down just $ 8.375 from Monday .	Elaboration))))
In the process , 4.9 million shares traded ,	(Cause
making UAL the second most active issue on the Big Board .	Cause)))
Munching pizza	(Elaboration(Cause(Elaboration(Elaboration(Elaboration(Temporal(Joint(Background
when they could	Background)
and yelling	(Background
until their voices gave out ,	Background))
the two Benjamin Jacobson specialists at the Big Board 's UAL trading post yesterday presided over what can only be described as a financial free-for-all .	Temporal)
`` It was chaotic .	(Elaboration
But we like to call it 'controlled chaos , ' ''	(Attribution
said 47-year-old Robert J. Jacobson Jr. , grandson of the firm 's founder .	Attribution)))
He manned the UAL post yesterday with Christopher Bates , 33 , an energetic Long Islander	(Elaboration
who 's a dead ringer for actor Nicolas Cage .	Elaboration))
Who was doing all the selling ?	(Elaboration(Attribution(Topic-Comment
`` Options traders , arbitrage traders	(Summary
-- everyone , ''	Summary))
said Mr. Bates ,	(Elaboration
cooling down with a carton of apple juice after the close yesterday .	Elaboration))
Added Mr. Jacobson ,	(Attribution
`` There were some pretty bad losses in the stock . ''	Attribution)))
Big Board traders said	(Temporal(Attribution
a 200,000-share buy order at $ 150 a share	(Same-unit(Elaboration
entered by Bear , Stearns & Co. ,	(Elaboration
which was active in UAL stock all day ,	Elaboration))
is what set off the UAL crowd in the late afternoon .	Same-unit))
A subsequent rally in UAL helped the staggering stock market stage an astonishing recovery from an 80-point deficit	(Elaboration
to finish only slightly below Monday 's close .	Elaboration)))
Both Jacobson traders ,	(Temporal(Elaboration(Same-unit(Elaboration
who had been hoping	(Attribution
UAL trading would get back to normal ,	Attribution))
read the news about the unraveling of UAL takeover plans on the train into work yesterday morning .	Same-unit)
The news told them	(Attribution
it would be a while longer	(Background
before UAL resumed trading like a regular airline stock after months of gyrations .	Background)))
When Mr. Jacobson walked into the office at 7:30 a.m. EDT ,	(Temporal(Background
he announced :	(Attribution
`` OK , buckle up . ''	Attribution))
Messrs. Jacobson and Bates walked on the Big Board floor at about 8:45 a.m. yesterday	(Temporal(Elaboration(Elaboration(Background
and immediately spotted trouble .	Background)
Already entered in the Big Board 's computers and transmitted to their post were sell orders for 65,000 UAL shares .	Elaboration)
The UAL news had already caused a selling furor in the so-called third market ,	(Elaboration(Elaboration
in which firms buy and sell stock away from the exchange floor .	Elaboration)
UAL ,	(Elaboration(Same-unit(Elaboration
which closed on the Big Board Monday at $ 178.375 a share ,	Elaboration)
traded in the third market afterward as low as $ 158 a share .	Same-unit)
There were rumors of $ 148-a-share trades .	Elaboration)))
In the 45 minutes before the 9:30 opening bell , the Jacobson specialists kept getting sell orders , heavier than they imagined .	(Temporal
And at 9:15 , they posted a $ 135 to $ 155 `` first indication , '' or the price range	(Temporal(Temporal(Elaboration
in which the stock would probably open .	Elaboration)
That range was quickly narrowed to $ 145 to $ 155 ,	(Contrast
although traders	(Same-unit(Elaboration
surrounding the post	Elaboration)
were told that $ 148 to $ 150 would be the likely target .	Same-unit)))
When UAL finally opened a half hour late ,	(Temporal(Evaluation(Evaluation(Background
some 400,000 shares traded at $ 150 .	Background)
There was `` selling pressure from everyone , ''	(Attribution
said one trader .	Attribution))
This month 's Friday-the-13th market plunge	(Explanation(Same-unit(Elaboration
spurred by UAL news	Elaboration)
was n't as bad for the Jacobson specialists as yesterday 's action .	Same-unit)
On that earlier day , the stock 's trading was halted at a critical time	(Contrast(Enablement
so the specialists could catch their breath .	Enablement)
Not yesterday .	(Explanation
Mr. Jacobson ,	(Joint(Contrast(Same-unit(Elaboration
his gray hair flying ,	Elaboration)
did n't wear out his red-white-and-blue sneakers ,	Same-unit)
but he sweat so much	(Explanation
he considered sending out for a new shirt .	Explanation))
Mr. Bates usually handles day-to-day UAL trading on his own .	(Explanation(Contrast
But yesterday , the heavy trading action eventually consumed not only Messrs. Jacobson and Bates but four other Jacobson partners ,	(Elaboration
all doing their specialist-firm job	(Elaboration
of tugging buyers and sellers together	(Joint
and adjusting prices	(Enablement
to accommodate the market .	Enablement)))))
About 30 floor traders crammed near the UAL post most of the day ,	(Joint(Elaboration(Joint
and probably hundreds more came and went	Joint)
-- a `` seething mass , ''	(Attribution
as one trader described it .	Attribution))
The 4.9 million-share volume	(Same-unit(Elaboration
flowing through the Jacobson specialist operation	Elaboration)
was about five times normal for the stock .	Same-unit))))))))
The heavy buying in the last half hour led the specialists to take special steps .	(Temporal(Cause(Cause(Elaboration(Elaboration
The Bear Stearns order	(Attribution(Same-unit(Elaboration
that marked the late-day turnaround	Elaboration)
caused a `` massive buying effort ''	(Cause
as UAL jumped $ 20 a share to $ 170 in the last half hour ,	Cause))
said Mr. Bates .	Attribution))
With 15 seconds of trading to go ,	(Elaboration(Attribution(Same-unit(Background
Mr. Jacobson , with what voice	(Elaboration
he had left ,	Elaboration))
announced to the trading mob :	Same-unit)
`` We 're going to trade one price on the bell . ''	Attribution)
That meant no trading would occur in the final seconds , as a way	(Elaboration
of making sure that last-second orders are n't subjected to a sudden price swing	(Elaboration
that would upset customers .	Elaboration))))
About 11,000 shares sold at $ 170 on the bell ,	(Attribution(Elaboration
representing about eight to 10 late orders ,	Elaboration)
the specialists estimate .	Attribution))
Big Board traders praised the Jacobson specialists for getting through yesterday without a trading halt .	(Contrast
In Chicago , a UAL spokesman , `` by way of policy , '' declined to comment on the company 's stock or the specialists ' performance .	Contrast))
Leaving the exchange at about 5 p.m. ,	(Elaboration(Background
the Jacobson specialists made no predictions	(Elaboration
about how trading might go today .	Elaboration))
Said Earl Ellis , a Jacobson partner	(Attribution(Elaboration
who got involved in the UAL action ,	Elaboration)
`` It all starts all over again '' today .	Attribution)))))))))))
