Time Warner Inc. is considering a legal challenge to Tele-Communications Inc. 's plan to buy half of Showtime Networks Inc. , a move that could lead to all-out war between the cable industry 's two most powerful players .	O	9..227
Time is also fighting the transaction on other fronts , by attempting to discourage other cable operators from joining Tele-Communications as investors in Showtime , cable-TV industry executives say .	B-entrel	230..427
Time officials declined to comment .	I-entrel	428..463
Last week , Tele-Communications agreed to pay Viacom Inc. $ 225 million for a 50 % stake in its Showtime subsidiary , which is a distant second to Time 's Home Box Office in the delivery of pay-TV networks to cable subscribers .	O	466..688
Tele-Communications , the U.S. 's largest cable company , said it may seek other cable partners to join in its investment .	O	689..808
Tele-Communications is HBO 's largest customer , and the two have a number of other business relationships .	O	811..916
Earlier this year , Time even discussed bringing Tele-Communications in as an investor in HBO , executives at both companies said .	O	917..1045
The purchase of the Showtime stake is " a direct slap in our face , " said one senior Time executive .	O	1046..1144
Time is expected to mount a legal challenge in U.S. District Court in New York , where Viacom in May filed a $ 2.5 billion antitrust suit charging Time and HBO with monopolizing the pay-TV business and trying to crush competition from Showtime .	O	1147..1389
Executives involved in plotting Time 's defense say it is now preparing a countersuit naming both Viacom and Tele-Communications as defendants .	O	1390..1532
The executives say Time may seek to break up the transaction after it is consummated , or may seek constraints that would prevent Tele-Communications from dropping HBO in any of its cable systems in favor of Showtime .	O	1533..1749
Viacom officials declined to comment .	B-conjunction	1752..1789
Jerome Kern , Tele-Communications ' chief outside counsel , said he was n't aware of Time 's legal plans .	I-conjunction	1790..1890
But he said that any effort by Time to characterize the Tele-Communications investment in Showtime as anti-competitive would be " the pot calling the kettle black . "	O	1891..2054
" It 's hard to see how an investment by the largest { cable operator } in the weaker of the two networks is anti-competitive , when the stronger of the two networks is owned by the second largest " cable operator , Mr. Kern said .	O	2057..2280
In addition to owning HBO , with 22 million subscribers , Time Warner separately operates cable-TV system serving about 5.6 million cable-TV subscribers .	B-contrast	2283..2434
Tele-Communications controls close to 12 million cable subscribers , and Viacom has about one million .	B-entrel	2435..2536
In its suit against Time , Viacom says the ownership of both cable systems and cable-programming networks gives the company too much market power .	I-entrel	2537..2682
Time argues that in joining up with Tele-Communications , Viacom has potentially more power , particularly since Viacom also owns cable networks MTV , VH-1 and Nick at Nite .	O	2683..2853
Ironically , Tele-Communications and Time have often worked closely in the cable business .	O	2854..2943
Together , they control nearly 40 % of Turner Broadcasting Systems Inc. ; Tele-Communications has a 21.8 % stake , while Time Warner has a 17.8 % stake .	O	2944..3090
But since Time 's merger with Warner Communications Inc. , relations between the two have become strained .	B-cause	3091..3195
Each company worries that the other is becoming too powerful and too vertically integrated .	I-cause	3196..3287
Meanwhile , some legal observers say the Tele-Communications investment and other developments are weakening Viacom 's antitrust suit against Time .	O	3290..3435
Viacom accuses Time in its suit of refusing to carry Showtime or a sister service , The Movie Channel , on Time 's Manhattan Cable TV system , one of the nation 's largest urban systems .	B-contrast	3436..3617
But yesterday , Manhattan Cable announced it will launch Showtime on Nov. 1 to over 230,000 subscribers .	I-contrast	3618..3721
Showtime has also accused HBO of locking up the lion 's share of Hollywood 's movies by signing exclusive contracts with all the major studios .	B-contrast	3724..3865
But Showtime has continued to sign new contracts with Hollywood studios , and yesterday announced it will buy movies from Columbia Pictures Entertainment Inc. , which currently has a non-exclusive arrangement with HBO .	I-contrast	3866..4082
