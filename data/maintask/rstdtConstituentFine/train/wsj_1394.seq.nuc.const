Ralph Brown was 31,000 feet over Minnesota	(NN-Topic-Drift(NS-elaboration-additional(NN-Contrast(NS-analogy(SN-example(NS-explanation-argumentative(NN-Contrast(NN-Sequence(NS-temporal-same-time
when both jets on his Falcon 20 flamed out .	NS-temporal-same-time)
At 18,000 feet ,	(NN-Sequence(NN-Same-Unit(NS-attribution
he says ,	NS-attribution)
he and his co-pilot `` were looking for an interstate or a cornfield ''	(NS-purpose
to land .	NS-purpose))
At 13,000 feet , the engines restarted .	NN-Sequence))
But	(NS-antithesis(NN-Sequence(NN-Same-Unit
knowing	(SN-reason(SN-attribution
that mechanics would probably ground him for repairs ,	SN-attribution)
Mr. Brown skipped his stop in nearby Chicago	SN-reason))
and set course to get his load	(NN-Same-Unit(NS-elaboration-general-specific
-- a few hundred parcels --	NS-elaboration-general-specific)
to the Memphis package-sorting hub on time .	NN-Same-Unit))
Had he been a little less gung-ho ,	(SN-condition
`` I 'd have gotten the thing on the ground	(NS-attribution(NN-Sequence
and headed for the nearest bar , ''	NN-Sequence)
Mr. Brown says .	NS-attribution))))
But he flies for Federal Express Corp. , perhaps the closest thing in corporate America to the Green Berets .	NS-explanation-argumentative)
Federal 's employees work long hours	(NN-List
and seem to thrive on the stress	(NS-elaboration-object-attribute
of racing the clock .	NS-elaboration-object-attribute)))
Like Mr. Brown , they sometimes go to surprising lengths	(NS-interpretation(NS-purpose
to meet that overarching corporate goal :	(NS-elaboration-general-specific
delivering the goods on time .	NS-elaboration-general-specific))
They are a tribute to Federal 's management	(NS-comment(NS-elaboration-additional
which , since the company 's founding 16 years ago , has had its way with its work force	NS-elaboration-additional)
-- an unusual feat in the contentious transportation industry .	NS-comment)))
That may soon change .	(NS-interpretation(NS-explanation-argumentative
This month , Federal 's 2,048 pilots ,	(NS-interpretation(SN-consequence(NN-Same-Unit(NS-elaboration-set-member
including some 961	(NS-elaboration-object-attribute
acquired along with Tiger International Inc. in February ,	NS-elaboration-object-attribute))
will decide whether to elect the powerful Air Line Pilots Association as their bargaining agent .	NN-Same-Unit)
The election ,	(NN-List(NN-Same-Unit(NS-elaboration-additional
which would bring the first major union to Federal 's U.S. operations ,	NS-elaboration-additional)
has pitted new hires against devoted veterans such as Mr. Brown .	NN-Same-Unit)
It has also rattled Federal 's strongly anti-union management ,	(NS-elaboration-additional
which is already contending with melding far-flung operations and with falling profits .	NS-elaboration-additional)))
`` A union , sooner or later , has to have an adversary ,	(NS-elaboration-additional(NS-attribution(NN-List
and it has to have a victory , ''	NN-List)
Frederick W. Smith , Federal 's chairman and chief executive , says with disdain .	NS-attribution)
`` In our formula , we do n't have any losers except the competition . ''	NS-elaboration-additional)))
What managers really fear is that the pro-union movement could spread beyond the pilots .	(NS-explanation-argumentative
Under federal transportation law , a government mediator is attempting to reconcile the melding of Tiger 's job classifications into Federal 's .	(NS-interpretation(SN-hypothetical
Depending on the outcome ,	(SN-elaboration-set-member(SN-contingency
the merged company may face union elections this fall among airplane mechanics , ramp workers , stock clerks and flight dispatchers .	SN-contingency)
These groups constitute up to 10 % of its work force .	SN-elaboration-set-member))
`` Unions would have a profound effect on the whole culture of the company , ''	(NS-interpretation(NS-attribution
says Bernard La Londe , a professor at Ohio State University at Columbus and a Federal consultant .	NS-attribution)
That culture ,	(NN-Same-Unit(NS-elaboration-additional
carefully crafted by Mr. Smith ,	NS-elaboration-additional)
leaves little , if any , room for unions .	NN-Same-Unit))))))
Since founding the company ,	(NS-elaboration-additional(SN-result(NN-List(NS-elaboration-general-specific(SN-circumstance
the charismatic Vietnam vet ,	(NN-Same-Unit(NS-elaboration-additional
who is still only 46 years old ,	NS-elaboration-additional)
has fostered an ethos of combat .	NN-Same-Unit))
Flights are `` missions . ''	(NN-List
Mr. Smith 's managers have , at times , been called `` Ho Chi Minh 's Guerrillas . ''	(NN-List
The Bravo Zulu award , the Navy accolade for a `` job well done , '' is bestowed on Federal 's workers	(NN-List(NS-elaboration-object-attribute
who surpass the call of duty .	NS-elaboration-object-attribute)
Competitors are known as the `` enemy . ''	NN-List))))
To reinforce employees ' dedication ,	(SN-purpose
Mr. Smith pays well .	(NN-List
He also lets workers vent steam through an elaborate grievance procedure	(NN-List
and , as a perk , fly free in empty cockpit seats .	(NN-List
He gives pep talks in periodic `` family briefings ''	(NS-elaboration-object-attribute
beamed internationally on `` FXTV , '' the company 's own television network .	NS-elaboration-object-attribute))))))
And , with many of his 70,000 workers , Mr. Smith 's damn-the-torpedoes attitude has caught on .	(NS-example
James Cleveland , a courier	(NN-List(NS-elaboration-additional(NN-Same-Unit(NS-elaboration-object-attribute
who earned a Bravo Zulu	(NS-consequence
for figuring out how to get a major customer 's 1,100-parcel-a-week load to its doorstep by 8 a.m. ,	NS-consequence))
considers himself far more than a courier .	NN-Same-Unit)
`` We do n't just hand the customer the package .	(NS-elaboration-additional(NS-attribution(NN-List
That 's just the beginning , ''	NN-List)
he says .	NS-attribution)
`` In essence , we run the show . ''	NS-elaboration-additional))
David Sanders , a longtime pilot , bristles at the mere suggestion	(NS-elaboration-additional(NS-elaboration-object-attribute
that a union might tamper with his flight schedule .	NS-elaboration-object-attribute)
`` This is America , ''	(NS-elaboration-additional(NS-attribution
he says .	NS-attribution)
`` Nobody has the right	(NS-elaboration-object-attribute
to tell me	(SN-attribution
how much I can work . ''	SN-attribution)))))))
Such attitudes have given Federal flexibility ,	(NN-List(NS-elaboration-object-attribute
not only to rapidly implement new technology	(NN-List
but to keep its work force extraordinarily lean .	(NS-elaboration-general-specific
The company deliberately understaffs ,	(SN-consequence(NS-purpose
stretching employees ' schedules to the limit .	NS-purpose)
But	(SN-comparison(NN-Same-Unit
though couriers work as many as 60 hours a week during the autumn rush ,	(NN-Contrast
they leave early during slack times	(SN-concession
while still being assured of a minimum paycheck .	SN-concession)))
Pilots , as well , routinely fly overtime	(NS-purpose
to ensure	(SN-attribution
that none are furloughed during seasonal lows .	SN-attribution)))))))
The operational freedom has also given Federal a leg up on archrival United Parcel Service Inc. , the nation 's largest employer of United Brotherhood of Teamsters members .	(NS-elaboration-general-specific
UPS wo n't discuss its labor practices ,	(NN-Contrast
but ,	(SN-circumstance(NN-Same-Unit(NS-attribution
according to Mr. Cleveland , a former UPS employee , and others ,	NS-attribution)
union work rules prohibit UPS drivers from doing more than carrying packages between customers and their vans .	NN-Same-Unit)
Because UPS drivers are n't permitted to load their own vehicles at the depot ,	(NN-Cause-Result(NS-attribution
say these couriers ,	NS-attribution)
packages often get buried in the load	(NN-Consequence
and are delivered late .	NN-Consequence))))))))
Labor problems are the last thing	(NN-Topic-Drift(NS-interpretation(NS-interpretation(NS-antithesis(NS-consequence(NS-explanation-argumentative(NS-elaboration-additional(NS-elaboration-object-attribute
Mr. Smith needs right now .	NS-elaboration-object-attribute)
Although the Tiger acquisition has brought Federal a long way toward becoming the global player	(NS-elaboration-general-specific(SN-antithesis(NS-elaboration-object-attribute
it wants to be ,	NS-elaboration-object-attribute)
it also has brought problems .	SN-antithesis)
It more than doubled Federal 's long-term debt to $ 1.9 billion ,	(NN-List
thrust the company into unknown territory	(NN-List(NS-elaboration-general-specific
-- heavy cargo --	NS-elaboration-general-specific)
and suddenly expanded its landing rights to 21 countries from four .	NN-List))))
Federal , on its own , had n't been doing very well overseas .	(NS-elaboration-general-specific
It had hemorrhaged in its attempt to get into Asia ,	(NN-List(NS-elaboration-additional
where treaty restrictions forced it to fly some planes half-empty on certain routes .	NS-elaboration-additional)
On routes to South America , the company had no backup jets	(NN-List(NS-purpose
to ensure delivery	(NS-temporal-same-time
when planes were grounded .	NS-temporal-same-time))
In Europe , business suffered	(NS-antithesis(NS-circumstance
as Federal bought several local companies ,	NS-circumstance)
only to have the managers quit .	NS-antithesis)))))
These and other problems squeezed Federal 's profit margins last year to 8 % , down from more than 13 % annually in the first half of the decade .	(NN-List(NN-Sequence
Earnings have plummeted , too , in each of the past three quarters .	NN-Sequence)
In the fiscal first period	(NN-Same-Unit(NS-elaboration-object-attribute
ended Aug. 31 ,	NS-elaboration-object-attribute)
profit fell 54 % to $ 30.4 million , or 58 cents a share ,	(NS-attribution(NS-consequence
mostly because of the Tiger merger ,	NS-consequence)
Federal says .	NS-attribution))))
Federal 's stock price , however , has held up well ,	(NS-evidence(NS-attribution(NS-explanation-argumentative
driven in part by the general run-up of airline stocks ,	NS-explanation-argumentative)
analysts say .	NS-attribution)
Since trading as low as $ 42.25 a share in May ,	(NS-elaboration-additional(SN-circumstance
Federal 's shares have rallied as high as $ 57.87 in New York Stock Exchange composite trading .	SN-circumstance)
They closed Friday at $ 53.25 , down 50 cents on the day .	NS-elaboration-additional)))
There 's a certain irony in the fact	(NS-explanation-argumentative(NS-elaboration-object-attribute
that Federal Express faces its first union problems	(NS-result
as a result of its Tiger purchase .	NS-result))
Tiger itself was founded by a band of gungho airmen	(SN-antithesis(NS-analogy(NS-elaboration-object-attribute
who had airlifted supplies `` over the Hump '' from India to China during World War II .	NS-elaboration-object-attribute)
In the early 1970s , Mr. Smith modeled his fledgling company on Tiger 's innovation of hub-and-spoke and containerized-cargo operations .	NS-analogy)
But from early on , Tiger 's workers unionized ,	(NN-Contrast
while Federal 's never have .	NN-Contrast))))
Federal Express officials acknowledge mistakes in their drive overseas	(NN-List(SN-antithesis
but say	(SN-attribution
it will pay off eventually .	SN-attribution))
Analysts expect Federal 's earnings to improve again in its fiscal third quarter	(NS-circumstance(NS-temporal-same-time(NS-elaboration-object-attribute
ending Feb. 28 ,	NS-elaboration-object-attribute)
when the company should begin benefiting from Tiger 's extra flights , back-up planes and landing rights .	NS-temporal-same-time)
Until then , they expect the cost	(NN-Same-Unit(NS-elaboration-object-attribute
of integrating the two carriers	NS-elaboration-object-attribute)
to continue crimping profits .	NN-Same-Unit))))
For now , the union issue is the most nettlesome of Federal 's Tiger problems ,	(NS-comment(NS-evaluation(NS-evaluation(NN-Problem-Solution(NS-elaboration-additional(NS-elaboration-additional(NS-attribution
management believes .	NS-attribution)
Although encouraging dialogue between managers and workers ,	(NN-Contrast(NS-evidence(SN-concession
Mr. Smith does n't countenance what he considers insubordination .	SN-concession)
When a large group of pilots once signed petitions	(SN-concession(SN-circumstance(NS-elaboration-object-attribute
opposing work-rule and compensation changes ,	NS-elaboration-object-attribute)
he called a meeting in a company hangar	(NN-List
and dressed them down	(NS-consequence
for challenging his authority .	NS-consequence)))
He then made most of the changes ,	(NS-attribution
pilots say .	NS-attribution)))
That sort of approach , however , has n't worked since the addition of Tiger .	(NS-elaboration-general-specific
Its 6,500 workers ,	(NN-Sequence(NS-temporal-same-time(NN-Same-Unit(NS-elaboration-additional
who had battled Tiger 's management for years over givebacks ,	NS-elaboration-additional)
were union members until the day of the merger ,	NN-Same-Unit)
when most of their unions were automatically decertified .	NS-temporal-same-time)
Soon after the merger , moreover , Federal 's management asked Tiger 's pilots to sign an agreement	(NN-Question-Answer(NS-elaboration-object-attribute
stating	(SN-attribution
that they could be fired any time ,	(NS-manner
without cause or notice .	NS-manner)))
When the pilots refused ,	(SN-circumstance
the company retracted it .	SN-circumstance))))))
Mr. Smith angered Federal 's pilots , too .	(NS-problem-solution(NS-explanation-argumentative(NS-manner
In his haste	(NN-Same-Unit(NS-elaboration-object-attribute
to seal the deal with Tiger Chairman Saul Steinberg last August ,	NS-elaboration-object-attribute)
Mr. Smith ignored a promise	(NS-elaboration-general-specific(NS-elaboration-object-attribute
that he had made to his own pilots three years ago :	NS-elaboration-object-attribute)
that any fliers	(NS-definition(NN-Same-Unit(NS-elaboration-object-attribute
acquired in future mergers	NS-elaboration-object-attribute)
would be `` end-tailed ''	NN-Same-Unit)
-- put at the bottom of the pilot seniority list	(NS-elaboration-object-attribute
that determines work schedules , pay and career options .	NS-elaboration-object-attribute)))))
The Tiger merger agreement stipulated	(SN-attribution
that the lists be combined on the basis of tenure .	SN-attribution))
Mr. Smith is trying hard to allay the anger .	(NS-elaboration-additional
And even some pro-union pilots say	(NS-interpretation(SN-attribution
his charisma and popularity among the many former military fliers could be tough to beat .	SN-attribution)
`` A lot of people are identifying a vote for representation as a vote against Fred Smith , ''	(NS-attribution
says J.X. Gollich , a Tiger-turned-Federal pilot and union activist .	NS-attribution)))))
Mr. Smith and other top Federal executives have met with Tiger workers in Los Angeles , Ohio , New York , Alaska , Asia and Europe .	(NN-List
Recently , they have appeared every few weeks in talk-show type videos ,	(NS-example(NS-purpose
countering pro-union arguments .	NS-purpose)
In one video , Mr. Smith defended his agreement to merge the pilot-seniority lists .	(NS-elaboration-additional
He said	(NN-List(SN-attribution
Mr. Steinberg had insisted	(SN-attribution
that the merger talks move quickly .	SN-attribution))
Regulators , as well , might have quashed the deal	(NN-List(NS-attribution(NS-condition
if Tiger 's pilots had n't been protected ,	NS-condition)
he said .	NS-attribution)
Furthermore ,	(NS-comment(NN-Same-Unit(NS-attribution
Mr. Smith added ,	NS-attribution)
`` our contract with our pilots says	(SN-attribution
that we will manage our fleet operations with their advice .	SN-attribution))
It does n't give any particular group the ability	(NS-elaboration-object-attribute
to veto change . ''	NS-elaboration-object-attribute))))))))
Already , the fight has been costly .	(NN-List(NS-explanation-argumentative
The seniority-list controversy , along with the job-classification dispute , has been turned over to the mediator .	(NN-Temporal-Same-Time
Meanwhile , the company is operating with two separate pilot groups and seniority lists ,	(NN-Consequence
and that is costing Federal `` a big number , ''	(NS-attribution
says James Barksdale , executive vice president and chief operating officer .	NS-attribution))))
The issue has also cost Federal management a lot of good will among its old pilots .	(NS-interpretation(NS-evidence
`` They were willing to mistreat us	(NN-List(NS-attribution(NS-circumstance
because we had n't shown any moxie , any resistance , ''	NS-circumstance)
says William Queenan , a DC-10 pilot and 14-year Federal veteran .	NS-attribution)
Adds John Poag , a 727 captain and past chairman of the company-sponsored Flight Advisory Board :	(SN-attribution
`` They 've made all these magnanimous gestures to the Flying Tiger pilots , and for us , nothing . ''	SN-attribution)))
Such animosity could prove pivotal in the union vote .	(NS-elaboration-additional
A large majority of the 961 former Tiger fliers support the union ,	(NN-Comparison(NS-attribution
according to a union study .	NS-attribution)
But	(NN-Same-Unit
though most of the 1,087 Federal pilots are believed opposed ,	(SN-concession
it is unclear just how much their loyalty to Mr. Smith has been eroded .	SN-concession)))))))
The fight has turned ugly	(NS-evidence(SN-consequence
and , among pilots at least , has shattered the esprit de corps	(NS-elaboration-object-attribute
that Mr. Smith worked so hard	(NS-purpose
to build .	NS-purpose)))
Anti-union pilots have held ballot-burning parties .	(NN-List
Some younger pilots say	(SN-attribution
they have had to endure anti-union harangues by senior pilots	(NS-temporal-same-time
while flying across the country .	NS-temporal-same-time)))))
And for now , at least , the competition is n't the only enemy .	(NS-evidence
Barney Barnhardt , a 727 captain and leader of the pro-union forces , said	(NS-elaboration-additional(SN-attribution
he has received two anonymous death threats	(NN-Temporal-Same-Time
and been challenged to a fight with tire irons by a colleague .	NN-Temporal-Same-Time))
`` The pilots are either for us or extremely against us , ''	(NS-attribution
he says with a sigh .	NS-attribution))))))
