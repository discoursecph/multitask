In a startling turnabout , Members of the Senate Intelligence Committee are complaining	(List(explanation-argumentative(example(background(comment(elaboration-additional(cause
that someone in the executive branch is leaking on them .	cause)
David Boren , the Intelligence Committee chairman , is upset	(reason
that someone leaked a letter to the committee from the Reagan administration	(elaboration-additional
suggesting	(attribution
that the U.S. would undertake to warn Panamanian thug Manuel Noriega if it got wind of an impending coup	(elaboration-object-attribute
that might result in his assassination .	elaboration-object-attribute)))))
With due respect to `` highly classified correspondence '' and other buzzwords , the leakers are performing a public service .	(comment(elaboration-additional
If the CIA has become a protection service for Mr. Noriega ,	(elaboration-additional(circumstance
the American people ought to know .	circumstance)
What went wrong in Panama is a fitting subject for public and congressional inquiry .	elaboration-additional))
Naturally , Senator Boren and his committee would like free rein	(antithesis(temporal-same-time(purpose
to blame the executive branch	purpose)
while stamping `` top secret '' on their own complicity .	temporal-same-time)
But there 's no danger	(List(manner(elaboration-object-attribute
of exposing sources and methods	elaboration-object-attribute)
in disclosing the debate running up and down Pennsylvania Avenue .	manner)
And	(Same-Unit
if Congress is going to assume authority	(condition(purpose
to micromanage foreign policy ,	purpose)
it 's going to have to take some of the responsibility too .	condition))))))
The President of the United States urged the Panamanian armed forces to move against Mr. Noriega .	(result
When they did ,	(result(temporal-same-time
his commanders did n't have the initiative	(elaboration-object-attribute
to do more than block a couple of roads .	elaboration-object-attribute))
The executive branch bears the first responsibility for timidity .	(comment
But what kind of initiative can you expect	(circumstance
given the climate set by Congress ?	circumstance)))))
For example , what exactly did the CIA tell Major Giroldi and his fellow coup plotters about U.S. laws and executive orders on assassinations ?	(result
What part did U.S. warnings play in the major 's unwillingness	(hypothetical(Contrast(temporal-same-time(elaboration-object-attribute
to pull the trigger	elaboration-object-attribute)
when he had General Noriega in custody ,	(antithesis
but was under attack by pro-Noriega troops ?	antithesis))
Mr. Noriega did n't suffer from any hesitation	(condition
once he had the pistol .	condition))
Maybe we need a CIA version of the Miranda warning :	(Disjunction(restatement
You have the right	(reason(elaboration-object-attribute
to conceal your coup intentions ,	elaboration-object-attribute)
because we may rat on you .	reason))
Or maybe a Surgeon General 's warning :	(restatement
Confiding in the United States may be fatal .	restatement)))))
CIA chief William Webster , hardly a Washington malcontent , got the debate started last week	(restatement(elaboration-additional(means
by noting	(explanation-argumentative
that the executive order	(Same-Unit(elaboration-object-attribute
banning assassinations	elaboration-object-attribute)
had contributed to U.S. paralysis during the coup .	Same-Unit)))
The CIA 's Deputy Director of Operations , Richard Stoltz , tried to smooth things over a few days later ,	(antithesis
but instead simply underlined Mr. Webster 's point .	(explanation-argumentative
`` The interpretation '' of the executive order ,	(Same-Unit(attribution
Mr. Stoltz said ,	attribution)
`` and the way	(Same-Unit(elaboration-object-attribute
in which the various committees have over time interpreted it ,	elaboration-object-attribute)
has led in my view to a proper caution on the part of operators ,	(elaboration-part-whole
including me . ''	elaboration-part-whole))))))
In other words , Congress wo n't let the CIA do much of anything anymore ,	(List
and that 's fine with the CIA .	(elaboration-additional
The pay 's the same ,	(List
and the duty 's lighter .	List)))))
And of course , doing anything	(summary(explanation-argumentative(result(elaboration-object-attribute
that might be second-guessed by Congress	elaboration-object-attribute)
carries heavy penalties .	(evidence
Witness the Walsh prosecution of Ollie North .	(elaboration-additional
The Intelligence Committee 's ranking Republican , Senator William Cohen , joined with Senator George Mitchell	(comment(purpose
to write a best seller about Iran-Contra ,	(elaboration-object-attribute
deploring `` Men of Zeal . ''	elaboration-object-attribute))
No doubt many people in the CIA , the Pentagon and the National Security Council have read it .	(interpretation
What kind of initiative should anyone expect from people out on the line	(comment(elaboration-object-attribute
who 've read all this	(List
and know what can happen	(condition
if they fail ?	condition)))
Who wants to end up as the protagonist in a Bill Cohen morality play ?	comment))))))
The order against assassinations is another artifact of the same congressional mind-set , a product of the 1970s Vietnam syndrome against any executive action .	(concession(comment
President Bush would do himself and the country a favor	(means
by rescinding the order as an ambiguous intrusion on his ability	(elaboration-object-attribute
to defend America 's national security .	elaboration-object-attribute)))
There are of course good reasons	(elaboration-additional(antithesis(elaboration-object-attribute
the U.S. should n't get into the assassination business ,	elaboration-object-attribute)
but rescinding the executive order is not the same thing	(comparison
as saying the U.S. should start passing out exploding cigars .	comparison))
The world being the nasty place	(concession(elaboration-object-attribute
it is ,	elaboration-object-attribute)
we want Presidents to have the freedom	(purpose
to order operations	(explanation-argumentative(elaboration-object-attribute
in which someone might get killed .	elaboration-object-attribute)
In such situations , you can not write rules in advance ,	(concession
you can only make sure	(enablement
the President takes the responsibility .	enablement))))))))
The executive order and the reported agreements with the Intelligence Committee are neither sensible nor moral .	(elaboration-additional
As it now stands ,	(interpretation(example
the U.S. can bomb Tripoli ,	(List(Contrast
but ca n't `` assassinate '' Colonel Gadhafi .	Contrast)
It can send a fighter squadron to strafe terrorist hideouts in the Bekaa Valley ,	(Contrast
but ca n't shoot Abu Nidal .	Contrast)))
Both the assassination order and the quality of debate in Washington are telling the world	(elaboration-additional
that the only way	(Same-Unit(elaboration-object-attribute
the U.S. will kill a madman	elaboration-object-attribute)
is by making sure	(manner
we take some innocent civilians with him .	manner)))))))
