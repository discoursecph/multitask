A #320 million ($508 million) British Airways PLC rights issue flopped badly -- the victim of recent market turbulence and the collapse of the buy-out bid for United Airlines' parent, UAL Corp.	O	9..202
The United Kingdom carrier had planned the issue to help finance its $750 million purchase of a 15% stake in UAL.	B-concession	205..318
But British Airways withdrew from the UAL labor-management buy-out plan last Friday, after the group failed to get bank financing for its $6.79 billion buy-out.	I-concession	319..479
British Airways said its shareholders accepted only 6.3% of the convertible capital bonds, but that the rest of the issue will be taken up by underwriters.	O	482..637
Analysts said that 6.3% level marked the poorest showing for any major British rights issue since the 1987 global stock market crash.	O	638..771
"It is close to being a record undersubscription," said Bob Bucknell, an analyst with London broker Smith New Court Securities.	O	774..901
Fund managers don't like to have rights issues that don't have an obvious reason.	B-contrast	902..984
The obvious reason was (for British Air) to buy a stake in United Airlines."	I-contrast	985..1061
In a statement, British Air Chairman Lord King said the company was "obviously disappointed that the issue was not taken up, but it would have been unreasonable to expect a better result given the volatility of the stock market since the launch of the issue."	O	1064..1323
But except for the embarrassment, British Air will emerge relatively unscathed from the flopped issue.	B-cause	1326..1428
Underwriters led by Lazard Brothers & Co. will pick up the rest of the airline's offer of four convertible capital bonds for every nine common shares.	B-contrast	1429..1579
Lazard and other primary underwriters have reduced or eliminated their exposure by sub-underwriting the issue among U.K. institutional investors.	I-contrast	1580..1725
"The (paper) loss here is very small" for these sub-underwriters, observed John Nelson, a Lazard managing director.	O	1726..1841
In any case, he added, "most institutions probably won't sell" the bonds.	B-conjunction	1842..1915
And instead of buying the UAL stake, the U.K. carrier will be able to reduce its high debt level and build an acquisition war chest.	I-conjunction	1916..2048
"From a cash flow point of view, British Airways is better off not being in United Airlines in the short term," said Andy Chambers, an analyst at Nomura Research Institute in London.	O	2049..2231
Added another U.K. analyst: "It gives them some cash in the back pocket for when they want to do something."	O	2232..2340
For instance, British Air is continuing to negotiate with KLM Royal Dutch Airlines about each acquiring a 20% stake in Sabena World Airlines, the air transport subsidiary of the Belgian national airline.	B-entrel	2343..2546
A definitive agreement had been expected by the end of July.	I-entrel	2547..2607
The failed rights issue also should have a modest impact on British Air shares.	B-entrel	2610..2689
The airline's share price already is far below the 210 pence ($3.33) level seen after the company announced the rights issue in late September.	B-restatement	2690..2833
In late trading on London's Stock Exchange yesterday, the shares were off three pence at 194 pence.	B-conjunction	2834..2933
And because British Air is issuing convertible bonds rather than ordinary shares, the share price won't be directly hurt by any surplus left with underwriters after they try to sell the issue in the open market.	I-conjunction	2934..3145
But British Air's withdrawal from the UAL buy-out could have further repercussions.	O	3148..3231
Some analysts speculated yesterday that the move has set off a board room split, which may lead to the resignation of Sir Colin Marshall, the carrier's chief executive officer.	O	3232..3408
"The stories are rubbish," a British Air spokesman said.	O	3409..3465
"There is no difference of opinion between (Chairman) Lord King and Sir Colin on any aspect of company policy.	O	3466..3576
