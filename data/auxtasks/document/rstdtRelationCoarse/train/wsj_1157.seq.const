Insiders have been selling shares in Dun & Bradstreet Corp. , the huge credit-information concern .	(Summary-Evaluation-Topic(Elaboration(Elaboration(Elaboration(Elaboration(Temporal(Elaboration(Elaboration(Elaboration
Six top executives at the New York-based company sold shares in August and September .	(Elaboration
Four of those insiders sold more than half their holdings .	Elaboration))
The stock , in New York Stock Exchange composite trading yesterday , closed at $ 51.75 , up 62.5 cents , well below the $ 56.13 to $ 60 a share	(Elaboration(Elaboration
the insiders received for their shares .	Elaboration)
Much of the recent slide in Dun & Bradstreet 's stock came late last week , after negative comments by analysts at Merrill Lynch & Co. and Goldman , Sachs & Co .	Elaboration))
A company spokesman declined to comment	(Joint
and said	(Attribution
that the officials	(Textual-organization(Elaboration
who sold shares	Elaboration)
would n't comment .	Textual-organization))))
One of Dun & Bradstreet 's chief businesses is compiling reports	(Elaboration(Elaboration
that rate the credit-worthiness of millions of American companies .	Elaboration)
It also owns Moody 's Investors Service ,	(Textual-organization(Elaboration
which assigns credit-ratings to bonds and preferred stock ;	Elaboration)
A.C. Nielsen ,	(Textual-organization(Elaboration
known for its data on television-viewing patterns ,	Elaboration)
and Yellow-pages publisher Donnelley .	Textual-organization))))
Last March , this newspaper reported on widespread allegations	(Temporal(Elaboration
that the company misled many customers into purchasing more credit-data services than needed .	Elaboration)
In June , the company agreed to settle for $ 18 million several lawsuits	(Temporal(Temporal(Elaboration
related to its sales practices ,	Elaboration)
without admitting or denying the charges .	Temporal)
An investigation by U.S. Postal inspectors is continuing .	Temporal)))
Among the insider sales , Charles Raikes , the firm 's general counsel , sold 12,281 shares in August ,	(Joint(Elaboration(Elaboration
representing 46 % of his holdings in the company .	Elaboration)
He received $ 724,579 for the shares ,	(Attribution
according to insider filings with the Securities and Exchange Commission .	Attribution))
John C. Holt , an executive vice president and Dun & Bradstreet director , sold 10,000 shares on Aug. 31 for $ 588,800 ,	(Joint(Elaboration(Attribution
filings show .	Attribution)
He retains 9,232 shares .	Elaboration)
William H.J. Buchanan , the firm 's secretary and associate general counsel , sold 7,000 shares in two separate sales in September for $ 406,000 .	(Joint(Elaboration
The shares represented 66 % of his Dun & Bradstreet holdings ,	(Attribution
according to the company .	Attribution))
The other insiders , all senior or executive vice presidents , sold between 2,520 and 6,881 shares ,	(Attribution(Elaboration
representing between 8 % and 70 % of their holdings ,	Elaboration)
according to SEC filings .	Attribution)))))
Dun & Bradstreet 's stock price began its recent spiral downward last Wednesday ,	(Cause(Cause
when the company reported third-quarter results .	Cause)
Net income rose to 83 cents a share from 72 cents a share the year-earlier period .	(Comparison
But analysts focused more on the drop in revenue , to $ 1.04 billion from $ 1.07 billion ,	(Elaboration(Elaboration
reflecting in part a continuing drop in sales of the controversial credit-reporting services .	Elaboration)
Last Thursday , Merrill Lynch securities analyst Peter Falco downgraded his investment rating on the firm ,	(Joint(Cause(Elaboration(Elaboration(Elaboration(Attribution
according to Dow Jones Professional Investors Report ,	Attribution)
citing a slowdown in the credit-reporting business .	Elaboration)
He cut his rating to a short-term hold from above-average performer	(Joint
and reduced his 1990 earnings estimate .	Joint))
Mr. Falco continues to rank the stock a longterm buy .	Elaboration)
The stock slid $ 1.875 on more than four times average daily volume .	Cause)
The stock received another blow on Friday ,	(Cause(Cause
when Goldman Sachs analyst Eric Philo advised	(Attribution
that investors with short-term horizons should avoid Dun & Bradstreet stock	(Cause
because it is unlikely to outperform the market .	Cause)))
The stock fell 75 cents .	Cause))))))
Insider selling is not unusual at Dun & Bradstreet ;	(Temporal(Comparison(Elaboration
in fact , the recent pace of selling is just about average for the company ,	(Attribution
according to figures	(Elaboration
compiled by Invest/Net , a North Miami , Fla. , firm	(Elaboration
that specializes in tracking and analyzing SEC insider filings .	Elaboration))))
But previous sales have often been sales of shares	(Attribution(Elaboration
purchased through the exercise of stock options	(Temporal
and sold six months later ,	(Temporal
as soon as allowed ,	Temporal)))
said Robert Gabele , president of Invest/Net .	Attribution))
The most recent sales do n't appear to be option-related ,	(Attribution
he said .	Attribution)))
TASTY PROFITS :	(Summary-Evaluation-Topic(Textual-organization
Michael A. Miles , chief executive officer of Philip Morris Cos. ' Kraft General Foods unit , bought 6,000 shares of the company on Sept. 22 for $ 157 each .	(Elaboration(Elaboration(Temporal(Elaboration
The $ 942,000 purchase raised his holdings to 74,000 shares .	Elaboration)
The stock split four-for-one on Oct. 10 .	(Cause
Mr. Miles 's newly purchased shares are now worth $ 1,068,000 ,	(Elaboration
based on Philip Morris 's closing price of $ 44.50 , up 62.5 cents , in composite trading on the New York Stock Exchange yesterday .	Elaboration)))
A spokesman for Mr. Miles said	(Attribution
he bought the shares	(Cause
because he felt	(Attribution
they were `` a good investment . ''	Attribution))))
The executive made his purchases	(Temporal
shortly before being named to his current chief executive officer 's position ;	(Elaboration
formerly he was Kraft General Foods ' chief operating officer .	Elaboration))))
SHEDDING GLITTER :	(Textual-organization
Two directors of Pegasus Gold Inc. , a Spokane , Wash. , precious-metals mining firm , sold most of their holdings in the company Aug. 31 .	(Elaboration(Elaboration(Elaboration(Elaboration(Elaboration(Elaboration
John J. Crabb sold 4,500 shares for $ 11.13 each ,	(Joint(Elaboration(Elaboration
leaving himself with a stake of 500 shares .	Elaboration)
He received $ 50,085 .	Elaboration)
Peter Kutney sold 5,000 shares , all of his holdings , for $ 11.38 a share , or $ 56,900 .	Joint))
Gary Straub , corporate counsel for the company , said	(Attribution
the directors sold for `` personal financial reasons . ''	Attribution))
Both insiders declined to comment .	Elaboration)
On Wall Street , Merrill Lynch & Co. analyst Daniel A. Roling rates the stock `` neutral ''	(Joint
and Drexel Burnham Lambert Inc. lists it as a `` buy . ''	Joint))
Pegasus Gold `` has been on a lot of recommended lists as a junior growth company	(Elaboration(Attribution(Elaboration
stepping into the big leagues , ''	Elaboration)
says Marty McNeill , metals analyst at Dominick & Dominick , a New York investment firm .	Attribution)
`` It 's a good company ,	(Summary-Evaluation-Topic(Joint
and growing ;	Joint)
there 's nothing	(Elaboration
that would warrant that it be sold . ''	Elaboration))))
Yesterday , in composite trading on the American Stock Exchange , Pegasus closed at $ 10.125 , up 12.5 cents .	Elaboration))))
