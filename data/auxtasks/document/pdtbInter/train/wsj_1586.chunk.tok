When his Seventh Avenue fur business here was flying high 20 years ago , Jack Purnick had 25 workers and a large factory .	B-contrast	9..129
Now his half-dozen employees work in an eighth-floor shop that he says is smaller than his old storage room .	B-conjunction	130..238
He also says he is losing money now .	I-conjunction	239..275
He blames imports .	B-contrast	276..294
But just down Seventh Avenue , where about 75 % of U.S. fur garments are made , Larry Rosen has acquired two retail outlets , broadened his fur-making line and expanded into leather .	B-cause	297..475
He credits imports .	I-cause	476..495
The difference lies in how the two entrepreneurial furriers reacted to the foreign competition and transformation of their industry over the past 10 years .	B-restatement	498..653
One stuck to old-line business traditions , while the other embraced the change .	I-restatement	654..733
" The small , good fur salon is not what it used to be , " says Mr. Purnick , 75 years old .	O	736..822
" We make the finest product in the world , and the Americans are being kicked around . "	B-contrast	823..908
Mr. Rosen , though , believes imports have reinvigorated the industry in which he has worked for most of his 57 years .	I-contrast	911..1027
" You 've got some minds here that wo n't think progressively , " he says .	O	1028..1097
Import competition for U.S. furs has risen sharply since furriers started aggressively marketing " working-girl mink " and similar lower-priced imported furs in recent years .	O	1100..1272
Merchants discovered a consumer largely ignored by higher-priced furriers : the younger woman -- even in her late 20s -- who never thought she could buy a mink .	B-cause	1273..1432
The new market helped boost U.S. fur sales to about $ 1.8 billion a year now , triple the level in the late 1970s .	B-conjunction	1433..1545
It also opened the door to furs made in South Korea , China , Hong Kong and other countries .	I-conjunction	1546..1636
Jindo Furs , a large South Korean maker , says it operates 35 retail outlets in the U.S. and plans to open 15 more by the end of next year .	O	1637..1774
Mr. Purnick and other old-line furriers call many of the the imports unstylish and poorly made .	O	1775..1870
High-end U.S. furriers say these imports have n't squeezed them .	O	1871..1934
But low-priced and middle-priced furriers like Mr. Purnick , who once saturated the five-block Seventh Avenue fur district , say imports have cut their sales .	O	1935..2091
A woman who once would have saved for two or three seasons to buy a U.S.-made mink can now get an imported mink right away for less than $ 2,000 .	O	2092..2236
Yet Mr. Rosen has turned the import phenomenon to his advantage .	B-restatement	2239..2303
Early in the decade he saw that fur workers in many foreign countries were willing to work longer hours at lower wages than their American counterparts and were more open to innovation .	B-cause	2304..2489
In 1982 , he started a factory in Greece .	B-asynchronous	2490..2530
Two years later , he opened one in West Germany .	I-asynchronous	2531..2578
He also noticed that foreign makers were introducing many variations on the traditional fur , and he decided to follow suit .	B-cause	2581..2704
By combining his strengths in innovation and quality control with the lower costs of production abroad , he says he has been able to produce high-quality goods at low cost .	I-cause	2705..2876
To maintain control over production and avoid overdependence on foreign sources , he says he still makes most of his furs in the U.S.	O	2877..3009
But six years ago he also began importing from the Far East .	O	3010..3070
Inspired by imports , Mr. Rosen now makes fur muffs , hats and flings .	B-conjunction	3073..3141
This year he produced a men 's line and offers dyed furs in red , cherry red , violet , royal blue and forest green .	B-list	3142..3254
He has leather jackets from Turkey that are lined with eel skin and topped off with raccoon-skin collars .	B-list	3255..3360
From Asia , he has mink jackets with floral patterns made by using different colored furs .	B-asynchronous	3361..3450
From Asia , he has mink jackets with floral patterns made by using different colored furs .	B-list	3361..3450
Next he will be testing pictured embroidery ( called kalega ) made in the Far East .	B-entrel	3451..3532
He plans to attach the embroidery to the backs of mink coats and jackets .	I-entrel	3533..3606
Besides adding to sales , leathers also attract retailers who may buy furs later , he adds .	O	3607..3696
Other furriers have also benefited from leathers .	B-instantiation	3699..3748
Seymour Schreibman , the 65-year-old owner of Schreibman Raphael Furs Inc. , treats the reverse side of a Persian lambskin to produce a reversible fur-and-leather garment .	I-instantiation	3749..3918
He says it accounts for 25 % of total sales .	O	3919..3962
Mr. Rosen is also pushing retail sales .	B-instantiation	3965..4004
This year he bought two stores , one in Brooklyn and one in Queens .	B-conjunction	4005..4071
Other furriers have also placed more weight on retailing .	I-conjunction	4072..4129
Golden Feldman Furs Inc. began retailing aggressively eight years ago , and now retail sales account for about 20 % of gross income .	O	4132..4262
In other moves , Mr. Rosen says he bought a truck three years ago to reach more retailers .	B-asynchronous	4265..4354
Since then he has expanded his fleet and can now bring his furs to the front door of retailers as far away as the Midwest .	I-asynchronous	4355..4477
Small retailers who ca n't afford to travel to his New York showroom have become fair game .	O	4478..4568
Such moves have helped Mr. Rosen weather the industry slump of recent years .	B-entrel	4571..4647
The industry enjoyed six prosperous years beginning in 1980 , but since 1986 sales have languished at their $ 1.8 billion peak .	B-restatement	4648..4773
Large furriers such as Antonovich Inc. , Fur Vault Inc. and Evans Inc. all reported losses in their latest fiscal years .	B-cause	4774..4893
Aftereffects of the 1987 stock market crash head the list of reasons .	B-conjunction	4894..4963
In addition , competition has glutted the market with both skins and coats , driving prices down .	B-conjunction	4964..5059
The animal-rights movement has n't helped sales .	I-conjunction	5060..5107
Warm winters over the past two years have trimmed demand , too , furriers complain .	B-list	5108..5189
And those who did n't move some production overseas suffer labor shortages .	I-list	5192..5266
" The intensive labor needed to manufacture furs { in the U.S. } is not as available as it was , " says Mr. Schreibman , who is starting overseas production .	O	5267..5418
But even those who have found a way to cope with the imports and the slump , fear that furs are losing part of their allure .	B-instantiation	5421..5544
" People are promoting furs in various ways and taking the glamour out of the fur business , " says Stephen Sanders , divisional merchandise manager for Marshall Field 's department store in Chicago .	I-instantiation	5545..5739
" You ca n't make a commodity out of a luxury , " insists Mr. Purnick , the New York furrier .	O	5742..5830
He contends that chasing consumers with low-priced imports will harm the industry in the long run by reducing the prestige of furs .	B-contrast	5831..5962
But Mr. Rosen responds : " Whatever people want to buy , I 'll sell .	I-contrast	5965..6029
The name of the game is to move goods .	O	6030..6068
