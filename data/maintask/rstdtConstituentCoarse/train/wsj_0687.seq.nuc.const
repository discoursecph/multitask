Dennis Farney 's Oct. 13 page-one article `` River of Despair , '' about the poverty along the Mississippi , fanned childhood memories	(NN-Textual-organization(NN-Textual-organization(SN-Cause(NS-Elaboration(NS-Elaboration
of when my parents were sharecroppers in southeastern Arkansas , only a few miles from the river .	NS-Elaboration)
Although we were white ,	(SN-Temporal(NS-Comparison(SN-Comparison
the same economic factors affected us	SN-Comparison)
as affects the black people	(NS-Elaboration
Mr. Farney writes about .	NS-Elaboration))
Fortunately , an aunt with a college degree bought a small farm	(NN-Temporal
and moved us 50 miles north to good schools and an environment	(NN-Temporal(NS-Elaboration
that opened the world of opportunity for me as an eight-year-old .	NS-Elaboration)
Though I 've been blessed with academic degrees and some success in the materialistic world ,	(NN-Temporal(SN-Comparison
I 've never forgotten or lost contact with those memories of the 1930s .	SN-Comparison)
Most of the land in that and other parts of the Delta are now owned by second , third or fourth generations of the same families .	(NS-Elaboration(NS-Elaboration
These are the families	(NS-Elaboration
who used	(NN-Textual-organization(NN-Joint
-- and sometime abused	NN-Joint)
-- their sharecroppers , people	(NS-Elaboration
who had no encouragement and little access to an education or training for a better life .	NS-Elaboration))))
Following World War II ,	(NN-Comparison(NN-Cause(SN-Temporal(SN-Temporal
when one family with mechanized equipment could farm crops	(NS-Comparison
formerly requiring 20 families ,	NS-Comparison))
the surplus people were dumped into the mainstream of society with no Social Security , no skills in the workplace , no hope for their future except welfare .	SN-Temporal)
And today , many of their children , grandchildren and great-grandchildren remain on welfare .	NN-Cause)
In the meantime , the landowners continue receiving generous subsidies	(NN-Joint(NS-Elaboration
that began during New Deal days .	NS-Elaboration)
Or those	(NN-Textual-organization(NS-Elaboration
who choose not to farm	NS-Elaboration)
can lease their lands and crop allotments for handsome sums .	NN-Textual-organization)))))))))
Farmers in the Midwest and other areas have suffered ,	(NN-Comparison
but those along the Mississippi continue to prosper with holdings	(NS-Elaboration(NS-Elaboration
that were built with the sweat of men and women	(NS-Elaboration
living in economic slavery .	NS-Elaboration))
And when they were no longer needed ,	(SN-Temporal
they were turned loose	(NN-Joint
unprepared to build lives of their own .	NN-Joint)))))
Denton Harris	(NN-Joint
Chairman	(NN-Joint
Metro Bank	(NN-Joint
Atlanta	NN-Joint))))
Because the cycle of poverty along the lower Mississippi goes back so many generations ,	(NN-Textual-organization(NN-Textual-organization(NS-Summary-Evaluation-Topic(NS-Summary-Evaluation-Topic(NS-Cause(SN-Summary-Evaluation-Topic(NS-Elaboration(SN-Cause
breaking this cycle will be next to impossible .	SN-Cause)
Sadly , the cycle appears not as waves but as a downward spiral .	NS-Elaboration)
Yet the evidence	(NN-Textual-organization(NS-Elaboration
that we have not hit bottom	NS-Elaboration)
is found in the fact	(NS-Elaboration
that we are not yet helping ourselves .	NS-Elaboration)))
The people of the Delta are waiting for that big factory to open , river traffic to increase , government spending to fund job-training programs or public schools to educate apathetic students .	(NN-Joint
Because we refuse to face the tough answers ,	(NN-Joint(SN-Cause
the questions continue as fodder for the commissions and committees , for the media and politicians .	SN-Cause)
Coffee-shop chatter does not lend itself to solving the problems of racism , teen pregnancy or lack of parental support or guidance .	NN-Joint)))
Does the Delta deserve government help	(NN-Joint(NS-Temporal(NS-Elaboration
in attracting industry	NS-Elaboration)
when the majority of residents , black and white , do not realize	(SN-Attribution
racism alienates potential employers ?	SN-Attribution))
Should we focus on the region 's infant-mortality rate	(NS-Temporal
when the vocal right-wingers and the school boards , mayors and legislators prohibit schools from teaching the two ways	(NN-Textual-organization(NS-Elaboration
( abstinence or contraceptives )	NS-Elaboration)
of decreasing teen pregnancy ?	NN-Textual-organization))))
Delta problems are difficult , not impossible , to solve	(SN-Comparison
-- I am just not convinced	(SN-Attribution
that we are ready to solve them yet .	SN-Attribution)))
Leslie Falls Humphries	(NN-Joint
Little Rock , Ark .	NN-Joint))
I would like to issue a challenge to corporate America .	(NN-Textual-organization(NN-Textual-organization(NS-Summary-Evaluation-Topic(NS-Elaboration
The next time expansion plans are mentioned at the old company	(NN-Summary-Evaluation-Topic(NN-Temporal
and somebody says ,	(SN-Attribution
`` Aw heck , guys , nobody can do it like Japan or South Korea , ''	SN-Attribution))
I wish you	(NN-Temporal(SN-Attribution
would butt in	SN-Attribution)
and say ,	(NN-Temporal(SN-Attribution
`` Hold it , fellas ,	SN-Attribution)
why do n't we compare prices	(NN-Temporal
and use our own little Third World country .	(NS-Cause
We would even save on freight . ''	NS-Cause))))))
There is no mystery	(NS-Elaboration
why the Delta originated `` Singin ' the Blues . ''	NS-Elaboration))
Eugene S. Clarke IV	(NN-Joint
Hollandale , Miss .	NN-Joint))
Your story is an insult to the citizens of the Mississippi Delta .	(NN-Textual-organization(NS-Summary-Evaluation-Topic(NS-Cause
Many of the problems	(NS-Elaboration(NS-Elaboration(NS-Cause(NN-Textual-organization(NS-Elaboration
you presented	NS-Elaboration)
exist in every part of this country .	NN-Textual-organization)
Poverty is only two blocks from President Bush 's residence .	NS-Cause)
For years , we tried to ignore the problem of poverty ,	(NN-Joint
and	(NN-Textual-organization
now that it has gotten out of hand	(SN-Cause
it 's a new crusade for the media and our Democratic Congress .	SN-Cause))))
Nobody should have to live in such poor conditions as in `` Sugar Ditch , ''	(NN-Joint
but	(NS-Cause(NN-Textual-organization
when you travel to Washington , Boston , Chicago or New York ,	(SN-Temporal
the same problems exist .	SN-Temporal))
The only difference is , in those cities the poor are housed in high-rise-project apartments	(NS-Elaboration
each consisting of one room , with rusty pipes	(NN-Joint(NS-Elaboration
called plumbing ,	NS-Elaboration)
rodents and cockroaches everywhere and nonworking elevators	(NN-Joint
-- and with the building patrolled by gangs and drug dealers .	NN-Joint)))))))
Many middle-class people would love free food , Medicaid insurance , utilities and rent .	(NN-Comparison(SN-Temporal
Then maybe I could stay home	(NN-Joint
and have seven children	(NN-Joint
and watch Oprah Winfrey , like Beulah in the article ,	NN-Joint)))
instead of having one child	(NN-Joint
and working constantly	(NS-Cause
just to stay above water , like so many families in this country .	NS-Cause))))
Dee Ann Wilson	(NN-Joint
Greenville , Miss .	NN-Joint)))))
