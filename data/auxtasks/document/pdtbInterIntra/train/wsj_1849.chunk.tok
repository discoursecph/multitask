Natural upheavals , and most particularly earthquakes , are not only horrible realities in and of themselves , but also symbols through which the state of a society can be construed .	root	9..188
The rubble after the Armenian earthquake a year ago disclosed , quite literally , a city whose larger structures had been built with sand .	instantiation	189..325
The extent of the disaster stemmed from years of chicanery and bureaucratic indifference .	entrel	326..415
The larger parallel after the earthquake centered south of San Francisco is surely with the state of the U.S. economy .	norel	418..536
Did the stock-market tremors of Friday , Oct. 13 , presage larger fragility , far greater upheavals ?	instantiation	537..634
Are the engineering and architecture of the economy as vulnerable as the spans of the Bay Bridge ?	norel	635..732
The eerie complacency of the Reagan-Bush era has produced Panglossian paeans about the present perfection of U.S. economic and social arrangements .	norel	735..882
A licensed government intellectual , Francis Fukuyama , recently announced in The National Interest that history is , so to speak , at an end since the course of human progress has now culminated in the glorious full stop of American civilization .	instantiation	883..1126
His observations were taken seriously .	entrel	1127..1165
But we are , in reality , witnessing the continuing decline of the political economy of capitalism : not so much the end of history but the history of the end .	comparison	1166..1322
The financial equivalent of the sand used by those Armenian contractors is junk bonds and the leveraged buy-outs associated with them .	norel	1325..1459
Builders get away with using sand and financiers junk	entrel	1460..1513
when society decides it 's okay , necessary even , to look the other way .	condition	1514..1584
And by the early 1980s U.S. capitalists had ample reason to welcome junk bonds , to look the other way .	conjunction	1585..1687
By that time they found extremely low profit rates from non-financial corporate investment .	norel	1690..1781
Government statistics in fact show that the profit rate -- net pretax profits divided by capital stock -- peaked in 1965 at 17.2 % .	restatement	1782..1912
That same calculation saw profit rates fall to 4.6 % in the recession year 1982	asynchronous	1913..1991
and the supposed miracle that followed has seen the profit rate rise only to 8.1 % in 1986 and 8 % in 1987 .	conjunction	1992..2097
Corresponding to the fall in profit rates was -- in the early 1980s -- the drop in the number arrived at if you divide the market value of firms by the replacement costs of their assets , the famous Q ratio associated with Prof. James Tobin .	norel	2100..2340
In theory , the value attached to a firm by the market and the cost of replacing its assets should be the same .	entrel	2341..2451
But of course the market could decide that the firm 's capital stock -- its assets -- means nothing if the firm is not producing profits .	concession	2452..2588
This is indeed what the market decided .	restatement	2589..2628
By 1982 the ratio was 43.5 % , meaning that the market was valuing every dollar 's worth of the average firm 's assets at 43 cents .	restatement	2629..2756
From the history of capitalism we can take it as a sound bet that if it takes only 43 cents to buy a dollar 's worth of a firm 's capital stock , an alert entrepreneur wo n't look the other way .	norel	2759..2949
His assumption is that the underlying profitability rate will go up and the capital assets he bought on the cheap will soon be producing profits , thus restoring the market 's faith in them .	cause	2950..3138
Hence the LBO craze .	norel	3139..3159
But here is where the entrepreneur made a very risky bet , and where society was maybe foolish to look the other way .	norel	3162..3278
The profit rate is still low	restatement	3279..3307
and the Q ratio was only 65 % in 1987 and 68.9 % in 1988 .	conjunction	3308..3363
Result : a landscape littered with lemons , huge debt burdens crushing down upon the arch and spans of corporate America .	cause	3364..3483
The mounting risks did not go unobserved , even in the mid-1980s .	norel	3486..3550
But there were enough promoters announcing the end of history ( in this case suspension of normal laws of economic gravity ) for society to continue shielding its eyes .	contrast	3551..3717
Mainstream economists and commentators , craning their necks up at the great pyramids of junk financing , swiveling their heads to watch the avalanche of leveraged buy-outs , claimed the end result would be a leaner , meaner corporate America , with soaring productivity and profits and the weaker gone to the wall .	instantiation	3718..4028
But this is not where the rewards of junk financing were found .	concession	4029..4092
The beneficiaries were those financiers whose icon was the topic figure of '80s capitalism , Michael Milken 's $ 517 million salary in one year .	alternative	4093..4234
Left-stream economists I associate with -- fellows in the Union of Radical Political Economists , most particularly Robert Pollin of the economics faculty at the University of California at Riverside -- were not hypnotized in the manner of their pliant colleagues .	norel	4237..4500
All along they have been noting the tremors and pointing out the underlying realities .	alternative	4501..4587
Profit rates after the great merger wave are no higher	instantiation	4588..4642
and now we have an extremely high-interest burden relative to cash flow .	conjunction	4643..4716
The consequences of building empires with sand are showing up .	norel	4719..4781
In contrast to previous estimates reckoning the default rate on junk bonds at 2 % or 3 % , a Harvard study published in April of this year ( and discussed in a lead story in The Wall Street Journal for Sept. 18 ) found the default rate on these junk bonds is 34 % .	instantiation	4782..5040
What is the consequence of a high-interest burden , high default rates and continued low profitability ?	norel	5043..5145
Corporations need liquidity , in the form of borrowed funds .	norel	5146..5205
Without liquidity from the junk-bond market or cash flow from profits , they look to the government , which obediently assists the natural motions of the capitalist economy with charity in the form of cuts in the capital-gains tax rate or bailouts .	cause	5206..5452
The consequence can be inflation , brought on as the effect of a desperate bid to avoid the deflationary shock of a sudden crash .	contrast	5453..5581
Attacks on inflation come with another strategy of capital of a very traditional sort : an assault on wages .	norel	5584..5691
Mr. Fukuyama , peering through binoculars at the end of history , said in his essay that " the class issue has actually been successfully resolved in the West ...	instantiation	5692..5851
the egalitarianism of modern America represents the essential achievement of the classless society envisioned by Marx . "	norel	5852..5971
Mr. Fukuyama might want to consult some American workers on the subject of class and egalitarianism .	norel	5974..6074
From its peak in 1972 of $ 198.41 , the average American weekly wage had fallen to $ 169.28 in 1987 -- both figures being expressed in 1977 dollars .	cause	6075..6220
In other words , after the glory boom of the Reagan years , wages had sunk from the post World War II peak by 16 % as capitalists , helped by the government , turned down the screws or went offshore .	restatement	6221..6415
But there are signs now -- the strikes by miners , Boeing workers , telephone workers , etc. -- that this attack on wages is being more fiercely resisted .	contrast	6418..6569
These are long-term Richter readings on American capitalism .	norel	6572..6632
The whole structure is extremely shaky .	restatement	6633..6672
Governments have become sophisticated in handling moments of panic ( a word the London Times forbade my father to use when he was reporting the Wall Street crash in 1929 ) .	contrast	6673..6843
But sophistication has its limits .	contrast	6844..6878
The S&L bailout could cost $ 300 billion , computing interest on the government 's loans .	instantiation	6879..6965
These are real costs .	restatement	6966..6987
Under what weights will the Federal Deposit Insurance Corporation totter ?	cause	6988..7061
Capitalism may now be engineered to withstand sudden shocks , but there are fault lines -- the crisis in profits , the assault on wages , the structural inequity of the system -- that make fools of those who claim that the future is here and that history is over .	norel	7062..7322
Mr. Cockburn is a columnist for The Nation and LA Weekly .	norel	7325..7382
