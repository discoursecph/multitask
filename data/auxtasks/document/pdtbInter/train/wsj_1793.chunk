Corporate efforts to control health-care costs by requiring evaluations prior to planned hospitalization and surgery haven't been sweeping enough to reduce the long-term rate of cost increases, according to a study by the Institute of Medicine.	O	9..253
In the last decade, many corporations have embraced the "utilization management" cost containment strategy as a way to control health-care costs for employees.	B-entrel	256..415
These programs vary widely, but often require second opinions on proposed surgery, preadmission reviews of elective hospitalizations and reviews of treatment during illnesses or recovery periods.	B-entrel	416..611
Between 50% and 75% of today's workers are covered by such plans, up from 5% five years ago.	I-entrel	612..704
"Although it probably has reduced the level of expenditures for some purchasers, utilization management -- like most other cost containment strategies -- doesn't appear to have altered the long-term rate of increase in health-care costs," the Institute of Medicine, an affiliate of the National Academy of Sciences, concluded after a two-year study.	O	707..1056
"Employers who saw a short-term moderation in benefit expenditures are seeing a return to previous trends."	O	1057..1164
While utilization management frequently reduces hospitalization costs, these savings are often offset by increases in outpatient services and higher administrative costs, according to the report by a panel of health-care experts.	O	1167..1396
The report suggested that current review programs are too narrow.	O	1399..1464
"The unnecessary and inappropriate use of the hospital, and not the actual need for a particular procedure, has been the main focus," the panel said.	O	1465..1614
"As a general rule, prior-review programs have not made case-by-case assessments of the comparative costs of alternative treatments or sites of care."	O	1615..1765
The report said that utilization management should have more of an impact as federal research on the effectiveness of medical treatments helps lead to medical practice guidelines.	O	1766..1945
Howard Bailit, a panel member and a vice president of Aetna Life & Casualty, said that utilization management will also do a better job of containing costs as it spreads to cover medical services delivered outside of hospitals.	B-entrel	1948..2175
"There's pretty good evidence that utilization management has reduced inappropriate hospitalization," he said.	I-entrel	2176..2286
But at the same time, spending on physician services and ambulatory care have mushroomed.	O	2287..2376
"It's like squeezing a balloon," Dr. Bailit said.	O	2377..2426
David Rahill of A. Foster Higgins & Co. said that clients of his consulting firm report that utilization management reduces their hospital care bills by about 5%, but he agreed that for the health-care system as whole, some of these savings are offset by administrative and outpatient care costs.	O	2429..2725
Jerome Grossman, chairman of the panel, agrees that administrative costs of utilization management programs can be high.	O	2728..2848
"You have a whole staff standing ready" to evaluate the appropriateness of recommended treatment, he said.	O	2849..2955
Dr. Grossman, who also is president of New England Medical Center Hospitals in Boston, noted that the hospitals he runs deal with more than 100 utilization management firms and that many of them have different procedures and requirements.	O	2956..3194
The panel urged greater efforts to reduce the complexity, paperwork and cost of utilization review.	O	3195..3294
"Utilization management needs to better demonstrate that it reduces the wasteful use of resources, improves the appropriateness of patient care and imposes only reasonable burdens on patients and providers," the panel concluded.	O	3297..3525
