Many skittish mutual fund investors picked up the phone yesterday , but decided not to cash in their chips after all .	O	9..125
As the stock market bounced back , withdrawals of money from stock funds amounted to a mere trickle compared with Black Monday , when investors dumped $ 2.3 billion , or about 2 % of stock-fund assets .	O	128..324
Fidelity Investments , the nation 's largest fund company , said phone volume was more than double its typical level , but still half that of Oct. 19 , 1987 .	B-entrel	327..479
Net outflows from Fidelity 's stock funds stood at less than $ 300 million , or below 15 % of the $ 2 billion cash position of the firm 's stock portfolios .	B-expansion	480..630
Much of the money was switched into the firm 's money market funds .	I-expansion	631..697
Outflows since the close of trading Friday remain below one-third their level of two years ago , Fidelity said .	O	698..808
Other mutual fund companies reported even lighter withdrawal requests .	B-expansion	811..881
And some investors at Fidelity and elsewhere even began buying stock funds during the day .	I-expansion	882..972
" Two years ago , there was a lot of redemption activity and trouble with people getting through on the phone , " said Kathryn McGrath , head of the investment management division of the Securities and Exchange Commission .	O	975..1192
This time , " We do n't have that at all . "	O	1193..1232
Of course , the relative calm could be jolted if the market plunges again .	O	1235..1308
And any strong surge in redemptions could force some funds to dump stocks to raise cash , as some did during Black Monday .	O	1309..1430
But funds generally are better prepared this time around .	O	1433..1490
As a group , their cash position of 10.2 % of assets in August -- the latest figure available -- is 14 % higher than two years earlier .	O	1491..1623
Many fund managers have boosted their cash levels in recent weeks .	O	1624..1690
The biggest flurry of investor activity came early in the day .	B-expansion	1693..1755
Vanguard Group Inc. saw heavy exchanges from stock funds into money market funds after the telephone lines opened at 8:30 a.m .	I-expansion	1756..1882
" In the first hour , the real nervous folks came along , " a spokesman said .	O	1883..1956
" But the horrendous pace of call volume in the first half-hour slowed considerably . "	O	1957..2041
At Scudder , Stevens & Clark Inc. , phone calls came in at 40 % more than the normal pace through early afternoon .	B-comparison	2044..2155
Most of that increase came in the first hour after the phone lines opened at 8 a.m .	B-expansion	2156..2239
As stocks rose , in fact , some investors changed course and reversed their sell orders .	B-entrel	2242..2328
Many funds allow investors to void orders before the close of trading .	I-entrel	2329..2399
At Scudder and at the smaller Ivy funds group in Hingham , Mass. , for instance , some shareholders called early in the morning to switch money from stock funds to money market funds , but later called back to reverse the switches .	O	2402..2629
Because mutual fund trades do n't take effect until the market close -- in this case , at 4 p.m. -- these shareholders effectively stayed put .	O	2630..2770
At Fidelity 's office in downtown Boston , Gerald Sherman walked in shortly after 7:30 a.m. and placed an order to switch his retirement accounts out of three stock funds and into a money market fund .	O	2773..2971
But by 3:15 p.m. , with the market comfortably ahead for the day , Mr. Sherman was preparing to undo his switch .	O	2972..3082
" It 's a nice feeling to know that things stabilized , " said Mr. Sherman , the 51-year-old co-owner of a discount department store .	O	3085..3213
But some investors continued to switch out of high-risk , high-yield junk funds despite yesterday 's rebound from that market 's recent price declines .	O	3216..3364
Shareholders have been steadily bailing out of several big junk funds the past several weeks as the $ 200 billion market was jolted by a cash crunch at Campeau Corp. and steadily declining prices .	B-entrel	3367..3562
Much of the money has been switched into money market funds , fund executives say .	I-entrel	3563..3644
Instead of selling bonds to meet redemptions , however , some funds have borrowed from banks to meet withdrawal requests .	B-contingency	3647..3766
This avoids knocking down prices further .	I-contingency	3767..3808
The $ 1.1 billion T. Rowe Price High Yield Fund was among the funds that borrowed during the Campeau crisis , says George J. Collins , president of T. Rowe Price Associates Inc .	O	3809..3983
That way , Mr. Collins says , " We did n't have to sell securities in a sloppy market . "	O	3986..4069
When the market stabilized , he added , the firm sold the bonds and quickly paid the loans back .	O	4070..4164
Tom Herman contributed to this article .	O	4167..4206
