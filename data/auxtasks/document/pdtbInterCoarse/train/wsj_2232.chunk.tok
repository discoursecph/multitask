First , there was a death watch .	B-temporal	9..40
Then exhilaration .	I-temporal	41..59
Spurred by waves of large-scale buying in blue-chip stocks , the Dow Jones Industrial Average rallied yesterday and erased about a half of Friday 's 190.58-point plunge , gaining 88.12 to 2657.38 .	O	62..255
It was the fourth-biggest advance for the average of 30 blue chips , on frenetic New York Stock Exchange volume of 416,290,000 shares -- the highest since the days after the 1987 crash .	O	256..440
While the advance cheered investors who feared a 1987-style crash would occur yesterday , it was strictly a big-stock rally fed by huge buying by bargain-hunting institutions and program traders .	O	443..637
A troubling sign : Declining stocks on the Big Board outnumbered advancers , 975 to 749 , and the over-the-counter market that includes many smaller stocks suffered aftershocks of Friday 's late Big Board plunge .	O	638..846
The Nasdaq OTC index closed down 6.31 to 460.98 .	O	847..895
Meanwhile , in a divergence in two of the market 's most important indicators , the Dow industrials ' sister average , the 20-stock Dow Jones Transportation Average , tumbled 102.06 to 1304.23 -- its second-worst decline next to the 164.78-point fall during the 1987 crash .	O	898..1165
Transports plunged on takeover disappointments in two airline stocks , UAL and AMR , which each fell more than 20 % when they reopened for trading yesterday after being suspended Friday afternoon .	B-expansion	1168..1361
UAL , the takeover stock at the center of Friday 's 190.58-point market plunge , fell 56 7/8 to 222 7/8 on nearly 2.3 million shares .	I-expansion	1362..1492
Overall , " this is a pleasant rally but it 's very selective , " said Arthur Cashin Jr. , a veteran PaineWebber Inc. trader at the Big Board .	O	1495..1631
" Everyone was a little concerned about the general narrowness of the rally and failure of the OTC market to get into plus territory .	B-contingency	1632..1764
It 's just a strange feeling .	I-contingency	1765..1793
I do n't think anyone left the place whistling Dixie . "	O	1794..1847
The rally gave credence , at least for now , to the pre-trading declaration of Big Board Chairman John J. Phelan Jr. that Friday 's market debacle was " an abnormal condition , and not a disaster . "	O	1850..2042
But to traders , it looked like disaster on the 9:30 a.m. opening bell .	O	2045..2115
The Dow Jones Industrial Average opened down 1.64 shortly after 9:30 .	O	2118..2187
But most of the 30 blue-chip stocks in the average , including Eastman Kodak and General Motors , could n't trade because of the heavy backlog of sell orders left over from Friday 's late-afternoon rout .	B-entrel	2188..2387
At 9:45 , Procter & Gamble -- one of the most important Dow bellwethers of late -- opened down 2 3/4 to 117 .	I-entrel	2388..2495
The Dow dropped to a quick 27-point loss , and to many traders it looked as if stocks were headed for yet another big tumble .	O	2498..2622
More stocks opened over the ensuing half hour , as the 49 Big Board specialist firms in charge of keeping the market orderly groped to find buy orders from major brokerage firms to match the selling flood .	O	2625..2829
Then , to make matters worse , computerized sell programs kicked in , hammering stocks into steeper losses .	O	2832..2936
There was heavy stock-index arbitrage , as traders sold big baskets of stock and bought stock-index futures to profit from the price discrepancies between the two markets .	B-comparison	2937..3107
This was a hangover from Friday , when Standard & Poor 's 500-stock index futures had closed at a sharp discount to stocks .	B-entrel	3108..3229
The onslaught of the program selling dashed any hopes that some of the big program trading firms would hold off until the market stabilized .	B-expansion	3230..3370
They did n't .	B-contingency	3371..3383
The Dow accelerated its slide , losing 63.52 in the first 40 minutes of trading .	I-contingency	3384..3463
With program traders seemingly in charge , buyers backed away from the market and watched stocks fall .	O	3466..3567
Then at 10:15 the Dow suddenly started to rebound , and when it shot upward it did so even faster than the early-morning fall .	B-expansion	3570..3695
And this time , it was n't just the program traders who were responsible .	B-contingency	3696..3767
All the selling had pushed stocks to such cheap values that big investment banks and major money management firms started buying stocks heavily .	I-contingency	3768..3912
The program traders were in there , too , of course .	O	3913..3963
But according to one trader , the programmers " did n't look as dominant on the upside as on the downside because there was { also } a lot of bargain-hunting " by institutions .	O	3964..4134
Roland M. Machold , director of the New Jersey Division of Investment , which oversees $ 29 billion in investments , said the " first thing we did was to double our orders " yesterday morning .	O	4137..4323
" With the market down like this , we 'll probably take another $ 50 million and put it in " the market .	O	4324..4423
Trading in Walt Disney Co. particularly caught traders ' eyes .	O	4426..4487
According to Big Board officials , Disney had one of the biggest sell-order imbalances on Friday ; it was one of the seven stocks that could n't finish trading that day .	O	4488..4654
The stock opened late at 114 1/2 , down 8 1/2 .	O	4655..4700
But then it shot upward 7 1/2 as Goldman , Sachs & Co. stepped in and bought , traders said .	O	4701..4791
However , Disney specialist Robert Fagenson said : " I would be surprised if Goldman represented 4 % of the opening volume . "	O	4792..4912
Around Wall Street , trading desks were relieved that they could at least play the market yesterday , in contrast to Friday 's gridlock .	B-expansion	4915..5048
At Donaldson , Lufkin & Jenrette Inc. , head equity trader Dudley Eppel said : " I think the opening was constructive .	I-expansion	5049..5163
It was orderly .	B-expansion	5164..5179
We put some orders together .	B-expansion	5180..5208
There was n't a lot of panic selling , either domestically or internationally ... .	B-expansion	5209..5289
Not like Friday where they just took { the market } apart . "	I-expansion	5289..5346
Still , the market had n't yet crossed into positive territory , and traders were glum .	O	5349..5433
But in another dramatic burst , the Dow tacked on 42 points in five minutes , and at 10:25 the index showed a gain of 5.74 .	O	5434..5555
On the Big Board floor and on trading desks , traders yelped their approval .	B-expansion	5558..5633
Grinned Griffith Peck , a trader in Shearson Lehman Hutton Inc. 's OTC department : " I tell you , this market acts healthy . "	I-expansion	5634..5755
Around him , scores of traders seemed to get a burst of energy ; their boss broke out bottles of Perrier water to cool them off .	O	5756..5882
Among Big Board specialists , the cry was " Pull your offers " -- meaning that specialists soon expected to get higher prices for their shares .	O	5885..6025
" It was bedlam on the upside , " said one Big Board specialist .	O	6028..6089
But not everybody was making money .	B-expansion	6092..6127
The carnage on the Chicago Board Options Exchange , the nation 's major options market , was heavy after the trading in S&P 100 stock-index options was halted Friday .	B-expansion	6128..6291
Many market makers in the S&P 100 index options contract had bullish positions Friday , and when the shutdown came they were frozen with huge losses .	B-temporal	6292..6440
Over the weekend , clearing firms told the Chicago market makers to get out of their positions at any cost Monday morning .	I-temporal	6441..6562
" They were absolutely killed , slaughtered , " said one Chicago-based options trader .	O	6565..6647
Some traders said that the closely watched Major Market Index , whose 20 stocks mimic the Dow industrials , did n't lead yesterday 's big rally .	O	6650..6790
James Gallagher , a partner at specialist Fowler & Rosenau , said , " The difference between today and two years ago " -- " Terrible Tuesday , " Oct. 20 , 1987 -- " is that then we needed a savior to go into the Major Market Index , spend $ 2 million and get the program rally started .	O	6793..7066
This time { institutions } saw the programs coming and backed away and backed away .	B-temporal	7067..7148
Then when the market was at a technical level to buy , they came in with a vengeance . "	B-comparison	7149..7234
However , according to one analyst , the timing of Major Market Index futures buying just before the turnaround was similar to that of Terrible Tuesday .	B-expansion	7237..7387
" Futures were pulling the stock market higher , " said Donald Selkin , head of stock-index futures research at Prudential-Bache Securities Inc .	I-expansion	7388..7528
Although the Big Board 's specialist firms struggled through another highly volatile trading session , their performance yesterday was better than during Friday 's late-afternoon chaos , according to traders and brokers who work with them .	O	7531..7766
Specialists were criticized for their inability to maintain orderly markets during the Friday plunge .	O	7769..7870
But yesterday , even with halts in such major blue-chip stocks as Merck , " we expected ( the halts ) and it was n't too bad , " said Donaldson 's Mr. Eppel , who had been critical of the specialists ' performance on Friday .	B-entrel	7871..8084
According to a Big Board official , while many stocks opened late , there were subsequent trading halts in only three issues -- AMR , Merck and Valero Energy .	B-entrel	8085..8240
Merck is one of the most important stocks in the Major Market Index .	I-entrel	8241..8309
No sector of the market has been spared during the past two days ' gyrations .	O	8312..8388
Yet from the Dow industrials ' high on Oct. 9 through Friday 's plunge , relatively good performances have been turned in by real-estate , utilities , precious metals and life insurance stocks .	B-expansion	8389..8577
And yesterday , the top performing industry group was oil field equipment issues .	B-expansion	8578..8658
For example , Halliburton jumped 3 1/4 to 38 , Schlumberger rose 2 1/2 to 43 1/4 and Baker Hughes rose 1 1/8 to 22 .	I-expansion	8659..8772
Because of the UAL and AMR tumbles , airlines were the weakest sector of the market yesterday .	O	8775..8868
Philip Morris was the Big Board 's most active issue , rising 2 1/4 to 43 1/4 on nearly eight million shares .	O	8871..8978
Among other major issues , Coca-Cola Co. closed up 2 at 66 3/4 on 1.7 million shares and American Telephone & Telegraph rose 3 1/4 to 43 on nearly 7.8 million shares .	B-expansion	8981..9146
Shares of International Business Machines , which reported earnings yesterday , finished at 103 , up 1 , after slipping below 100 during Friday 's session for the first time in five years .	I-expansion	9147..9330
Shares of three brokerage firms rose after they reported earnings .	B-expansion	9333..9399
Merrill Lynch added 1 3/4 to 28 , PaineWebber rose 3/4 to 18 1/2 and Bear Stearns rose 3/8 to 14 1/4 .	I-expansion	9400..9500
Federal National Mortgage Association , a recently hot stock , climbed 4 to 124 on nearly 1.6 million shares .	O	9503..9610
At a news conference after the close of trading yesterday , the Big Board 's Mr. Phelan and other exchange officials praised the performance of their computers and personnel .	O	9613..9785
Mr. Phelan said that program trading strategies were n't responsible for triggering Friday 's decline despite a jump in the use of the computer-driven strategies in recent months .	O	9788..9965
Some 24 million of the more than 100 million shares traded in the final 90 minutes of Friday 's session , when the plunge in stock prices was concentrated , were program-related , he said .	O	9966..10150
Program trades make up 10 % of the exchange 's volume on an average day , but despite the increase Friday , it was " certainly not something you would say precipitated the market decline , " Mr. Phelan said .	O	10153..10353
Mr. Phelan expressed relief that the market rebounded yesterday .	B-expansion	10356..10420
" Obviously , every time we get this kind of reaction , it 's going to make everybody nervous , including me , " he said .	I-expansion	10421..10535
He said that exchange officials had conversations with Wall Street firms throughout the weekend and that " all the participants behaved very , very responsibly today . "	O	10536..10701
Meanwhile , Peter DaPuzzo , Shearson 's head of retail equity trading , praised institutional investors in the OTC market , who were heavy buyers of the Nasdaq 's biggest technology issues yesterday amid a flood of selling by other investors .	B-expansion	10704..10940
" The institutions ca n't be criticized for their behavior , " Mr. DaPuzzo said in an interview .	I-expansion	10941..11033
" It was the opposite of what happened on Oct. 19 .	B-expansion	11034..11083
They used their judgment .	B-expansion	11084..11109
They did n't panic during the first round of selling this morning .	B-expansion	11110..11175
Instead , they bought on weakness and sold into the strength , which kept the market orderly .	B-entrel	11176..11267
Maybe they learned from experience . "	I-entrel	11268..11304
Mr. Phelan said the performance of specialists during Friday 's plunge was admirable , because out of 1,640 Big Board common stocks traded during the day only seven were closed and were n't reopened before the close .	O	11307..11520
" They did an excellent job , " Mr. Phelan said of the specialists .	B-entrel	11521..11585
Wall Street traders on Friday had complained about the trading suspensions .	I-entrel	11586..11661
James A. White and Sonja Steptoe contributed to this article .	O	11664..11725
