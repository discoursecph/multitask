The Voting Rights Act of 1965 was enacted	(NN-Textual-organization(NN-Contrast(NS-Elaboration(NS-Evaluation(SN-Background(NS-Enablement
to keep the promise of the Fifteenth Amendment	(NN-Joint
and enable Southern blacks to go to the polls ,	(NS-Manner-Means
unhindered by literacy tests and other exclusionary devices .	NS-Manner-Means)))
Twenty-five years later , the Voting Rights Act has been transformed by the courts and the Justice Department into a program of racial gerrymandering	(NS-Enablement(NS-Elaboration
designed to increase the number of blacks and other minorities	(NS-Elaboration(NS-Elaboration
-- Hispanics , Asians and native Americans --	NS-Elaboration)
holding elective office .	NS-Elaboration))
In the 1980s , the Justice Department and lower federal courts	(NS-Elaboration(NS-Elaboration(NN-Same-unit(NS-Elaboration
that enforce the Voting Rights Act	NS-Elaboration)
have required state legislatures and municipal governments to create the maximum number of `` safe '' minority election districts	(NS-Elaboration
-- districts	(NS-Elaboration
where minorities form between 65 % and 80 % of the voting population .	NS-Elaboration)))
The program has even been called upon	(NS-Enablement
to create `` safe '' white electoral districts in municipalities	(NS-Elaboration
where whites are the minority .	NS-Elaboration)))
Although Section 2 of the act expressly disclaims requiring	(SN-Contrast(NS-Elaboration
that minorities win a proportional share of elective offices ,	NS-Elaboration)
few municipal and state government plans achieve preclearance by the Justice Department	(NS-Condition(NN-Joint
or survive the scrutiny of the lower federal courts	NN-Joint)
unless they carve out as many solidly minority districts as possible .	NS-Condition)))))
The new goal of the Voting Rights Act	(NN-Same-unit(NS-Elaboration
-- more minorities in political office --	NS-Elaboration)
is laudable .	NN-Same-unit))
For the political process to work ,	(NS-Enablement(SN-Enablement
all citizens ,	(NN-Same-unit(NS-Contrast
regardless of race ,	NS-Contrast)
must feel represented .	NN-Same-unit))
One essential indicator	(NN-Same-unit(NS-Elaboration
that they are	NS-Elaboration)
is that members of minority groups get elected to public office with reasonable frequency .	NN-Same-unit)))
As is ,	(NN-Topic-Comment(SN-Background
blacks constitute 12 % of the population , but fewer than 2 % of elected leaders .	SN-Background)
But racial gerrymandering is not the best way	(NS-Explanation(NS-Evaluation(NS-Elaboration
to accomplish that essential goal .	NS-Elaboration)
It is a quick fix for a complex problem .	NS-Evaluation)
Far from promoting a commonality of interests among black , white , Hispanic and other minority voters ,	(NS-Elaboration(SN-Contrast
drawing the district lines	(NS-Evaluation(SN-Attribution(NN-Same-unit(NS-Background
according to race	NS-Background)
suggests	NN-Same-unit)
that race is the voter 's and the candidate 's most important trait .	SN-Attribution)
Such a policy implies	(SN-Attribution
that only a black politician can speak for a black person ,	(NN-Joint
and that only a white politician can govern on behalf of a white one .	NN-Joint))))
Examples of the divisive effects of racial gerrymandering can be seen in two cities	(NS-Elaboration(NS-Elaboration(NS-Elaboration
-- New York and Birmingham , Ala .	NS-Elaboration)
When they reapportion their districts after the 1990 census ,	(SN-Background
every other municipality and state in the country will face this issue .	SN-Background))
New York City :	(NN-Joint(NN-Textual-organization
Racial gerrymandering has been a familiar policy in New York City since 1970 ,	(NS-Elaboration(NS-Background
when Congress first amended the Voting Rights Act	(NS-Enablement
to expand its reach beyond the Southern states .	NS-Enablement))
In 1972 , the Justice Department required	(NN-Temporal(NS-Contrast(SN-Attribution
that the electoral map in the borough of Brooklyn be redrawn	(NS-Enablement
to concentrate black and Hispanic votes ,	NS-Enablement))
despite protests	(NS-Elaboration
that the new electoral boundaries would split a neighborhood of Hasidic Jews into two different districts .	NS-Elaboration))
This year , a commission	(NN-Temporal(NS-Elaboration
appointed by the mayor	(NS-Cause(NN-Same-unit(NS-Enablement
to revise New York 's system of government	NS-Enablement)
completed a new charter ,	NN-Same-unit)
expanding the City Council to 51 from 35 members .	NS-Cause))
Sometime in 1991 ,	(NS-Explanation(NS-Elaboration(NN-Same-unit
as soon as the 1990 census becomes available ,	(SN-Condition
a redistricting panel will redraw the City Council district lines .	SN-Condition))
The Charter Revision Commission has made it clear	(SN-Attribution
that	(NS-Enablement(NN-Same-unit(NS-Background
in response to the expectations of the Justice Department and the commission 's own commitment	(NS-Elaboration
to enhancing minority political leadership ,	NS-Elaboration))
the new district lines will be drawn	NN-Same-unit)
to maximize the number of solidly minority districts .	NS-Enablement)))
Blacks and Hispanics currently make up 38 % of the city 's population	(NN-Contrast(NS-Elaboration(NN-Contrast
and hold only 25 % of the seats on the council .	NN-Contrast)
Several of the city 's black leaders ,	(NS-Enablement(NN-Same-unit(NS-Elaboration
including Democratic mayoral nominee David Dinkins ,	NS-Elaboration)
have spoken out for racial gerrymandering	NN-Same-unit)
to accord blacks and Hispanics `` the fullest opportunity for representation . ''	NS-Enablement))
In this connection , it is important to note that several members of New York 's sitting City Council represent heterogeneous districts	(NS-Elaboration(NS-Elaboration(NS-Elaboration
that bring together sizable black , Hispanic , and non-Hispanic white populations	NS-Elaboration)
-- Carolyn Maloney 's 8th district in northern Manhattan and the south Bronx and Susan Alter 's 25th district in Brooklyn , for example .	NS-Elaboration)
To win their seats on the council ,	(NS-Elaboration(SN-Enablement
these political leaders have had to listen to all the voices in their district and devise public policies	(NS-Elaboration
that would benefit all .	NS-Elaboration))
Often they have found	(SN-Attribution
that the relevant issue is not race , but rather housing , crime prevention or education .	SN-Attribution)))))))))
Birmingham , Ala. :	(NN-Textual-organization
The unusual situation in Birmingham vividly illustrates the divisive consequences	(NS-Evaluation(NS-Elaboration(NS-Elaboration
of carving out safe districts for racial minorities .	NS-Elaboration)
In Birmingham ,	(SN-Cause(SN-Background(NN-Same-unit(NS-Elaboration
which is 57 % black ,	NS-Elaboration)
whites are the minority .	NN-Same-unit)
Insisting	(NS-Explanation(SN-Background(SN-Attribution
that they are protected by the Voting Rights Act ,	SN-Attribution)
a group of whites brought a federal suit in 1987	(NS-Enablement
to demand	(SN-Attribution
that the city abandon at-large voting for the nine member City Council	(NN-Temporal
and create nine electoral districts ,	(NS-Elaboration
including four safe white districts .	NS-Elaboration)))))
The white group argued	(NS-Cause(SN-Attribution
that whites were not fully and fairly represented ,	SN-Attribution)
because in city-wide elections only black candidates or white candidates	(NN-Same-unit(NS-Elaboration
who catered to `` black interests ''	NS-Elaboration)
could win .	NN-Same-unit))))
No federal court has ruled	(SN-Contrast(SN-Attribution
that the Voting Rights Act protects a white minority ,	SN-Attribution)
but in June the Justice Department approved a districting plan for Birmingham	(NS-Elaboration
that carves out three white-majority districts and six black-majority districts .	NS-Elaboration))))
Richard Arrington , Birmingham 's black mayor , lamented the consequences .	(NS-Elaboration(NS-Elaboration
`` In the past , people	(NN-Contrast(NS-Attribution(NS-Explanation(NN-Same-unit(NS-Elaboration
who had to run for office	NS-Elaboration)
had to moderate their views	NN-Same-unit)
because they could n't afford to offend blacks or whites , ''	NS-Explanation)
he said .	NS-Attribution)
`` Now you go to districts ,	(SN-Condition
you 're likely to get candidates	(NS-Elaboration
whose views are more extreme , white and black , on racial issues . ''	NS-Elaboration))))
Two hundred years ago , critics of the new United States Constitution warned	(SN-Background(SN-Contrast(NS-Explanation(SN-Attribution
that the electoral districts for Congress were too large	(NN-Joint
and encompassed too many different economic interests .	NN-Joint))
A small farmer and a seaport merchant could not be represented by the same spokesman ,	(NS-Attribution
they said .	NS-Attribution))
But James Madison refuted that argument in one of the most celebrated political treatises	(NS-Explanation(NN-Same-unit(NS-Elaboration
ever written ,	NS-Elaboration)
No. 10 of the Federalist Papers .	NN-Same-unit)
Madison explained	(NS-Elaboration(SN-Attribution
that a representative 's duty was to speak not for the narrow interests of one group but instead for the common good .	SN-Attribution)
Large , heterogeneous election districts would encourage good government ,	(NS-Cause(NS-Attribution
said Madison ,	NS-Attribution)
because a representative would be compelled to serve the interests of all his constituents and be servile to none .	NS-Cause))))
Madison 's noble and unifying vision of the representative still can guide us .	(NN-Contrast(NS-Explanation
As long as we believe	(SN-Condition(SN-Attribution
that all Americans , of every race and ethnic background , have common interests	(NN-Joint
and can live together cooperatively ,	NN-Joint))
our political map should reflect our belief .	SN-Condition))
Racial gerrymandering	(SN-Attribution(NN-Same-unit(NS-Elaboration
-- creating separate black and white districts --	NS-Elaboration)
says	NN-Same-unit)
that we have discarded that belief in our ability	(NS-Elaboration
to live together	(NN-Joint
and govern ourselves as one people .	NN-Joint))))))))))))))
Ms. McCaughey is a constitutional scholar at the Center for the Study of the Presidency in New York .	NN-Textual-organization)
