General Motors Corp. and Ford Motor Co. are now going head to head in the markets for shares of Jaguar PLC ,	(NS-Explanation(NS-Evaluation(NS-Cause(NS-Elaboration(NS-Background
as GM got early clearance from the Federal Trade Commission	(NS-Elaboration
to boost its stake in the British luxury car maker .	NS-Elaboration))
GM confirmed Friday	(NN-Temporal(NN-Temporal(SN-Attribution
that it received permission late Thursday from U.S. antitrust regulators	(NS-Elaboration
to increase its Jaguar holdings past the $ 15 million level .	NS-Elaboration))
Ford got a similar go-ahead earlier in October ,	NN-Temporal)
and on Friday , Jaguar announced	(NS-Contrast(SN-Attribution
that the No. 2 U.S. auto maker had raised its stake to 13.2 % , or 24.2 million shares , from 12.4 % earlier in the week .	SN-Attribution)
A spokesman for GM , the No. 1 auto maker , declined to say	(SN-Attribution
how many Jaguar shares that company owns .	SN-Attribution))))
In late trading Friday , Jaguar shares bucked the downward tide in London 's stock market	(NN-Temporal(NS-Cause(NN-Temporal
and rose five pence to 725 pence	(NS-Summary
( $ 11.44 ) .	NS-Summary))
Trading volume was a moderately heavy 3.1 million shares .	NS-Cause)
In the U.S. , Jaguar 's American depositary receipts were among the most active issues Friday in national over-the-counter trading	(NS-Elaboration
where they closed at $ 11.625 each , up 62.5 cents .	NS-Elaboration)))
Analysts expect	(SN-Attribution
that the two U.S. auto giants will move quickly to buy up 15 % stakes in Jaguar ,	(NS-Cause
setting up a potential bidding war for the prestigious Jaguar brand .	NS-Cause)))
British government restrictions prevent any single shareholder from going beyond 15 % before the end of 1990	(NS-Elaboration(NS-Summary(SN-Background(NS-Elaboration(NS-Condition
without government permission .	NS-Condition)
The British government ,	(NN-Same-unit(NS-Elaboration
which owned Jaguar until 1984 ,	NS-Elaboration)
still holds a controlling `` golden share '' in the company .	NN-Same-unit))
With the golden share as protection , Jaguar officials have rebuffed Ford 's overtures ,	(NN-Contrast
and moved instead to forge an alliance with GM .	NN-Contrast))
Jaguar officials have indicated	(NS-Elaboration(NN-Contrast(NS-Contrast(SN-Attribution
they are close to wrapping up a friendly alliance with GM	(NS-Elaboration
that would preserve Jaguar 's independence ,	NS-Elaboration))
but no deal has been announced .	NS-Contrast)
Ford , on the other hand , has said	(NS-Evaluation(NS-Condition(SN-Attribution
it 's willing to bid for all of Jaguar ,	(NS-Contrast
despite the objections of Jaguar chairman Sir John Egan .	NS-Contrast))
Analysts continued to speculate late last week	(NN-Joint(NS-Contrast(SN-Attribution
that Ford may try to force the issue	(NS-Manner-Means
by calling for a special shareholder 's meeting	(NN-Temporal
and urging	(SN-Attribution
that the government and Jaguar holders remove the barriers to a full bidding contest before December 1990 .	SN-Attribution))))
But a Ford spokeswoman in Dearborn said Friday	(SN-Attribution
the company has n't requested such a meeting yet .	SN-Attribution))
Individuals close to the situation believe	(SN-Attribution
Ford officials will seek a meeting this week with Sir John	(NS-Elaboration
to outline their proposal for a full bid .	NS-Elaboration))))
Any discussions with Ford could postpone the Jaguar-GM deal ,	(NS-Elaboration(NS-Elaboration
headed for completion within the next two weeks .	NS-Elaboration)
The GM agreement is expected to retain Jaguar 's independence	(NS-Manner-Means
by involving an eventual 30 % stake for the U.S. auto giant as well as joint manufacturing and marketing ventures .	NS-Manner-Means))))
Jaguar and GM hope to win Jaguar shareholders approval for the accord	(NS-Explanation(NS-Manner-Means
partly by structuring it in a way	(NS-Elaboration
that would n't preclude a full Ford bid	(NS-Temporal
once the golden share expires .	NS-Temporal)))
`` There 's either a minority { stake } package capable of getting Jaguar shareholder approval	(NS-Cause(NS-Attribution(NN-Joint
or there is n't , ''	NN-Joint)
said one knowledgeable individual .	NS-Attribution)
`` If there is n't ,	(SN-Condition
{ the deal } wo n't be put forward '' to shareholders .	SN-Condition)))))
Union sentiment also could influence shareholder reaction to a Jaguar-GM accord .	(NS-Elaboration(NS-Elaboration
GM 's U.K. unit holds crucial talks today with union officials about its consideration of an Ellesmere Port site for its first major engine plant in Britain .	NS-Elaboration)
One auto-industry union leader said ,	(SN-Attribution
`` If they try to build it somewhere else { in Europe } besides the U.K. ,	(SN-Condition
they are going to be in big trouble '' with unionists over any Jaguar deal .	SN-Condition)))))
