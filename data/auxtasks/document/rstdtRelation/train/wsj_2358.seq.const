September 's steep rise in producer prices shows	(Summary(Cause(Attribution
that inflation still persists ,	Attribution)
and the pessimism over interest rates	(Same-unit(Elaboration
caused by the new price data	Elaboration)
contributed to the stock market 's plunge Friday .	Same-unit))
After falling for three consecutive months ,	(Elaboration(Elaboration(Elaboration(Elaboration(Cause(Explanation(Attribution(Contrast
the producer price index for finished goods shot up 0.9 % last month ,	Contrast)
the Labor Department reported Friday ,	Attribution)
as energy prices jumped	(Contrast
after tumbling through the summer .	Contrast))
Although the report ,	(Explanation(Contrast(Same-unit(Elaboration
which was released	(Temporal
before the stock market opened ,	Temporal))
did n't trigger the 190.58-point drop in the Dow Jones Industrial Average ,	Same-unit)
analysts said	(Attribution
it did play a role in the market 's decline .	Attribution))
Analysts immediately viewed the price data , the grimmest inflation news in months , as evidence	(Elaboration
that the Federal Reserve was unlikely to allow interest rates to fall	(Background
as many investors had hoped .	Background))))
Further fueling the belief	(Elaboration(Elaboration(Background(Elaboration
that pressures in the economy were sufficient	(Enablement
to keep the Fed from easing credit ,	Enablement))
the Commerce Department reported Friday	(Attribution
that retail sales grew 0.5 % in September , to $ 145.21 billion .	Attribution))
That rise came on top of a 0.7 % gain in August ,	(Evaluation
and suggested	(Attribution
there is still healthy consumer demand in the economy .	Attribution)))
`` I think	(Attribution(Attribution
the Friday report ,	(Same-unit(Elaboration
combined with the actions of the Fed ,	Elaboration)
weakened the belief	(Elaboration
that there was going to be an imminent easing of monetary policy , ''	Elaboration)))
said Robert Dederick , chief economist at Northern Trust Co. in Chicago .	Attribution)))
But economists were divided over the extent of the inflation threat	(Elaboration(Elaboration(Elaboration
signaled by the new numbers .	Elaboration)
`` The overall 0.9 % increase is serious in itself ,	(Contrast(Attribution(Comparison
but what is even worse is that	(Same-unit
excluding food and energy ,	(Background
the producer price index still increased by 0.7 % , ''	Background)))
said Gordon Richards , an economist at the National Association of Manufacturers .	Attribution)
But Sung Won Sohn , chief economist at Norwest Corp. in Minneapolis , blamed rising energy prices and the annual autumn increase in car prices for most of the September jump .	(Elaboration
`` I would say	(Explanation(Attribution(Attribution
this is not bad news ;	(Elaboration
this is a blip , ''	Elaboration))
he said .	Attribution)
`` The core rate is not really out of line . ''	Explanation))))
All year , energy prices have skewed the producer price index ,	(Explanation(Elaboration
which measures changes in the prices	(Elaboration
producers receive for goods .	Elaboration))
Inflation unquestionably has fallen back from its torrid pace last winter ,	(Background
when a steep run-up in world oil prices sent the index surging at double-digit annual rates .	(Temporal
Energy prices then plummeted through the summer ,	(Elaboration
causing the index to decline for three consecutive months .	Elaboration))))))
Overall , the index has climbed at a 5.1 % compound annual rate since the start of the year ,	(Elaboration(Elaboration(Attribution
the Labor Department said .	Attribution)
While far more restrained than the pace at the beginning of the year ,	(Contrast
that is still a steeper rise than the 4.0 % increase for all of 1988 .	Contrast))
Moreover , this year 's good inflation news may have ended last month ,	(Elaboration(Background
when energy prices zoomed up 6.5 %	(Temporal
after plunging 7.3 % in August .	Temporal))
Some analysts expect oil prices to remain relatively stable in the months ahead ,	(Elaboration(Background(Elaboration
leaving the future pace of inflation uncertain .	Elaboration)
Analysts had expected	(Contrast(Attribution
that the climb in oil prices last month would lead to a substantial rise in the producer price index ,	Attribution)
but the 0.9 % climb was higher	(Comparison
than most anticipated .	Comparison)))
`` I think	(Contrast(Elaboration(Attribution(Attribution
the resurgence { in inflation } is going to continue for a few months , ''	Attribution)
said John Mueller , chief economist at Bell Mueller Cannon , a Washington economic forecasting firm .	Attribution)
He predicted	(Explanation(Attribution
that inflation will moderate next year ,	Attribution)
saying	(Attribution
that credit conditions are fairly tight world-wide .	Attribution)))
But Dirk Van Dongen , president of the National Association of Wholesaler-Distributors , said	(Explanation(Attribution
that last month 's rise `` is n't as bad an omen ''	(Comparison
as the 0.9 % figure suggests .	Comparison))
`` If you examine the data carefully ,	(Elaboration(Attribution(Condition
the increase is concentrated in energy and motor vehicle prices ,	(Contrast
rather than being a broad-based advance in the prices of consumer and industrial goods , ''	Contrast))
he explained .	Attribution)
Passenger car prices jumped 3.8 % in September ,	(Joint(Elaboration(Temporal
after climbing 0.5 % in August	(Temporal
and declining in the late spring and summer .	Temporal))
Many analysts said	(Elaboration(Attribution
the September increase was a one-time event ,	(Explanation
coming	(Background
as dealers introduced their 1990 models .	Background)))
Although all the price data were adjusted for normal seasonal fluctuations ,	(Contrast
car prices rose beyond the customary autumn increase .	Contrast)))
Prices for capital equipment rose a hefty 1.1 % in September ,	(Joint
while prices for home electronic equipment fell 1.1 % .	(Joint
Food prices declined 0.6 % ,	(Joint(Temporal
after climbing 0.3 % in August .	Temporal)
Meanwhile , the retail sales report showed	(Joint(Contrast(Attribution
that car sales rose 0.8 % in September to $ 32.82 billion .	Attribution)
But at least part of the increase could have come from higher prices ,	(Attribution
analysts said .	Attribution))
Sales at general merchandise stores rose 1.7 %	(Joint(Temporal
after declining 0.6 % in August ,	Temporal)
while sales of building materials fell 1.8 %	(Joint(Temporal
after rising 1.7 % .	Temporal)
Producer prices for intermediate goods grew 0.4 % in September ,	(Joint(Temporal
after dropping for three consecutive months .	Temporal)
Prices for crude goods , an array of raw materials , jumped 1.1 %	(Temporal
after declining 1.9 % in August	(Temporal
and edging up 0.2 % in July .	Temporal)))))))))))))))))
Here are the Labor Department 's producer price indexes	(Same-unit(Elaboration
( 1982=100 )	Elaboration)
for September , before seasonal adjustment , and the percentage changes from September , 1988 .	Same-unit)))
