Insiders have been selling shares in Dun & Bradstreet Corp. , the huge credit-information concern .	O	9..106
Six top executives at the New York-based company sold shares in August and September .	B-expansion	109..194
Four of those insiders sold more than half their holdings .	I-expansion	195..253
The stock , in New York Stock Exchange composite trading yesterday , closed at $ 51.75 , up 62.5 cents , well below the $ 56.13 to $ 60 a share the insiders received for their shares .	B-entrel	256..432
Much of the recent slide in Dun & Bradstreet 's stock came late last week , after negative comments by analysts at Merrill Lynch & Co. and Goldman , Sachs & Co .	I-entrel	433..590
A company spokesman declined to comment and said that the officials who sold shares would n't comment .	O	593..694
One of Dun & Bradstreet 's chief businesses is compiling reports that rate the credit-worthiness of millions of American companies .	B-expansion	697..827
It also owns Moody 's Investors Service , which assigns credit-ratings to bonds and preferred stock ; A.C. Nielsen , known for its data on television-viewing patterns , and Yellow-pages publisher Donnelley .	I-expansion	828..1029
Last March , this newspaper reported on widespread allegations that the company misled many customers into purchasing more credit-data services than needed .	B-temporal	1032..1187
In June , the company agreed to settle for $ 18 million several lawsuits related to its sales practices , without admitting or denying the charges .	B-expansion	1188..1332
An investigation by U.S. Postal inspectors is continuing .	I-expansion	1333..1390
Among the insider sales , Charles Raikes , the firm 's general counsel , sold 12,281 shares in August , representing 46 % of his holdings in the company .	B-entrel	1393..1540
He received $ 724,579 for the shares , according to insider filings with the Securities and Exchange Commission .	I-entrel	1541..1651
John C. Holt , an executive vice president and Dun & Bradstreet director , sold 10,000 shares on Aug. 31 for $ 588,800 , filings show .	B-entrel	1654..1784
He retains 9,232 shares .	I-entrel	1785..1809
William H.J. Buchanan , the firm 's secretary and associate general counsel , sold 7,000 shares in two separate sales in September for $ 406,000 .	B-entrel	1812..1953
The shares represented 66 % of his Dun & Bradstreet holdings , according to the company .	I-entrel	1954..2040
The other insiders , all senior or executive vice presidents , sold between 2,520 and 6,881 shares , representing between 8 % and 70 % of their holdings , according to SEC filings .	O	2043..2217
Dun & Bradstreet 's stock price began its recent spiral downward last Wednesday , when the company reported third-quarter results .	B-comparison	2220..2348
Net income rose to 83 cents a share from 72 cents a share the year-earlier period .	B-comparison	2349..2431
But analysts focused more on the drop in revenue , to $ 1.04 billion from $ 1.07 billion , reflecting in part a continuing drop in sales of the controversial credit-reporting services .	I-comparison	2432..2612
Last Thursday , Merrill Lynch securities analyst Peter Falco downgraded his investment rating on the firm , according to Dow Jones Professional Investors Report , citing a slowdown in the credit-reporting business .	O	2615..2826
He cut his rating to a short-term hold from above-average performer and reduced his 1990 earnings estimate .	B-comparison	2827..2934
Mr. Falco continues to rank the stock a longterm buy .	B-entrel	2935..2988
The stock slid $ 1.875 on more than four times average daily volume .	I-entrel	2989..3056
The stock received another blow on Friday , when Goldman Sachs analyst Eric Philo advised that investors with short-term horizons should avoid Dun & Bradstreet stock because it is unlikely to outperform the market .	B-contingency	3059..3272
The stock fell 75 cents .	I-contingency	3273..3297
Insider selling is not unusual at Dun & Bradstreet ; in fact , the recent pace of selling is just about average for the company , according to figures compiled by Invest/Net , a North Miami , Fla. , firm that specializes in tracking and analyzing SEC insider filings .	O	3300..3561
But previous sales have often been sales of shares purchased through the exercise of stock options and sold six months later , as soon as allowed , said Robert Gabele , president of Invest/Net .	O	3564..3754
The most recent sales do n't appear to be option-related , he said .	O	3755..3820
TASTY PROFITS :	O	3823..3837
Michael A. Miles , chief executive officer of Philip Morris Cos . ' Kraft General Foods unit , bought 6,000 shares of the company on Sept. 22 for $ 157 each .	B-entrel	3838..3991
The $ 942,000 purchase raised his holdings to 74,000 shares .	I-entrel	3992..4051
The stock split four-for-one on Oct. 10 .	O	4054..4094
Mr. Miles 's newly purchased shares are now worth $ 1,068,000 , based on Philip Morris 's closing price of $ 44.50 , up 62.5 cents , in composite trading on the New York Stock Exchange yesterday .	O	4095..4283
A spokesman for Mr. Miles said he bought the shares because he felt they were " a good investment . "	O	4286..4384
The executive made his purchases shortly before being named to his current chief executive officer 's position ; formerly he was Kraft General Foods ' chief operating officer .	O	4385..4557
SHEDDING GLITTER :	O	4560..4577
Two directors of Pegasus Gold Inc. , a Spokane , Wash. , precious-metals mining firm , sold most of their holdings in the company Aug. 31 .	B-expansion	4578..4712
John J. Crabb sold 4,500 shares for $ 11.13 each , leaving himself with a stake of 500 shares .	I-expansion	4713..4805
He received $ 50,085 .	B-expansion	4806..4826
Peter Kutney sold 5,000 shares , all of his holdings , for $ 11.38 a share , or $ 56,900 .	I-expansion	4827..4911
Gary Straub , corporate counsel for the company , said the directors sold for " personal financial reasons . "	B-entrel	4914..5019
Both insiders declined to comment .	I-entrel	5020..5054
On Wall Street , Merrill Lynch & Co. analyst Daniel A. Roling rates the stock " neutral " and Drexel Burnham Lambert Inc. lists it as a " buy . "	O	5057..5196
Pegasus Gold " has been on a lot of recommended lists as a junior growth company stepping into the big leagues , " says Marty McNeill , metals analyst at Dominick & Dominick , a New York investment firm .	O	5197..5395
" It 's a good company , and growing ; there 's nothing that would warrant that it be sold . "	O	5396..5483
Yesterday , in composite trading on the American Stock Exchange , Pegasus closed at $ 10.125 , up 12.5 cents .	O	5486..5591
