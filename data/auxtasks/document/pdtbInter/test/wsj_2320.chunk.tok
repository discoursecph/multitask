Measuring cups may soon be replaced by tablespoons in the laundry room .	O	9..80
Procter & Gamble Co. plans to begin testing next month a superconcentrated detergent that will require only a few spoonfuls per washload .	O	83..220
The move stems from lessons learned in Japan where local competitors have had phenomenal success with concentrated soapsuds .	B-conjunction	223..347
It also marks P&G 's growing concern that its Japanese rivals , such as Kao Corp. , may bring their superconcentrates to the U.S.	I-conjunction	348..474
The Cincinnati consumer-products giant got clobbered two years ago in Japan when Kao introduced a powerful detergent , called Attack , which quickly won a 30 % stake in the Japanese markets .	O	477..664
" They do n't want to get caught again , " says one industry watcher .	O	665..730
Retailers in Phoenix , Ariz. , say P&G 's new powdered detergent -- to be called Cheer with Color Guard -- will be on shelves in that market by early November .	B-entrel	733..889
A P&G spokeswoman confirmed that shipments to Phoenix started late last month .	I-entrel	890..968
She said the company will study results from this market before expanding to others .	O	969..1053
Superconcentrates are n't entirely new for P&G .	B-cause	1056..1102
The company introduced a superconcentrated Lemon Cheer in Japan after watching the success of Attack .	I-cause	1103..1204
When Attack hit the shelves in 1987 , P&G 's share of the Japanese market fell to about 8 % from more than 20 % .	B-contrast	1205..1313
With the help of Lemon Cheer , P&G 's share is now estimated to be 12 % .	I-contrast	1314..1383
While the Japanese have embraced the compact packaging and convenience of concentrated products , the true test for P&G will be in the $ 4 billion U.S. detergent market , where growth is slow and liquids have gained prominence over powders .	B-entrel	1386..1623
The company may have chosen to market the product under the Cheer name since it 's already expanded its best-selling Tide into 16 different varieties , including this year 's big hit , Tide with Bleach .	I-entrel	1624..1822
With superconcentrates , however , it is n't always easy to persuade consumers that less is more ; many people tend to dump too much detergent into the washing machine , believing that it takes a cup of powder to really clean the laundry .	O	1823..2056
In the early 1980s , P&G tried to launch here a concentrated detergent under the Ariel brand name that it markets in Europe .	B-contrast	2059..2182
But the product , which was n't as concentrated as the new Cheer , bombed in a market test in Denver and was dropped .	B-conjunction	2183..2297
P&G and others also have tried repeatedly to hook consumers on detergent and fabric softener combinations in pouches , but they have n't sold well , despite the convenience .	I-conjunction	2300..2470
But P&G contends the new Cheer is a unique formula that also offers an ingredient that prevents colors from fading .	O	2473..2588
And retailers are expected to embrace the product , in part because it will take up less shelf space .	O	2589..2689
" When shelf space was cheap , bigger was better , " says Hugh Zurkuhlen , an analyst at Salomon Bros .	O	2690..2787
But with so many brands vying for space , that 's no longer the case .	B-cause	2788..2855
If the new Cheer sells well , the trend toward smaller packaging is likely to accelerate as competitors follow with their own superconcentrates .	I-cause	2856..2999
Then retailers " will probably push the { less-established } brands out altogether , " he says .	O	3000..3090
Competition is bound to get tougher if Kao introduces a product like Attack in the U.S.	B-concession	3093..3180
To be sure , Kao would n't have an easy time taking U.S. market share away from the mighty P&G , which has about 23 % of the market .	I-concession	3181..3309
Kao officials previously have said they are interested in selling detergents in the U.S. , but so far the company has focused on acquisitions , such as last year 's purchase of Andrew Jergens Co. , a Cincinnati hand-lotion maker .	O	3312..3537
It also has a product-testing facility in California .	O	3538..3591
Some believe P&G 's interest in a superconcentrated detergent goes beyond the concern for the Japanese .	B-instantiation	3594..3696
" This is something P&G would do with or without Kao , " says Mr. Zurkuhlen .	I-instantiation	3697..3770
