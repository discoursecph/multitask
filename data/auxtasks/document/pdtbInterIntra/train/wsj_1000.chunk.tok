Kemper Financial Services Inc. , charging that program trading is ruining the stock market , cut off four big Wall Street firms from doing any of its stock-trading business .	root	9..180
The move is the biggest salvo yet in the renewed outcry against program trading , with Kemper putting its money -- the millions of dollars in commissions it generates each year -- where its mouth is .	norel	183..381
The Kemper Corp. unit and other critics complain that program trading causes wild swings in stock prices , such as on Tuesday and on Oct. 13 and 16 , and has increased chances for market crashes .	norel	384..577
Over the past nine months , several firms , including discount broker Charles Schwab & Co. and Sears , Roebuck & Co. 's Dean Witter Reynolds Inc. unit , have attacked program trading as a major market evil .	restatement	578..780
Several big securities firms backed off from program trading a few months after the 1987 crash .	norel	783..878
But most of them , led by Morgan Stanley & Co. , moved back in earlier this year .	contrast	879..958
The most volatile form of program trading is index arbitrage -- the rapid-fire , computer-guided buying and selling of stocks offset with opposite trades in stock-index futures and options .	entrel	959..1147
The object is to capture profits from fleeting price discrepancies between the futures and options and the stocks themselves .	entrel	1148..1273
Index arbitrage recently has accounted for about half of all program trading on the New York Stock Exchange .	norel	1276..1384
Last month , program trading accounted for 20.9 million shares a day , or a record 13.8 % of the Big Board 's average daily volume .	conjunction	1385..1512
On Tuesday afternoon , Kemper told Bear , Stearns & Co. , General Electric Co. 's Kidder , Peabody & Co. unit , Morgan Stanley and Oppenheimer & Co. that it will no longer do business with them because of their commitment to index arbitrage , officials inside and outside these firms confirmed .	norel	1515..1803
Kemper officials declined to identify the firms but acknowledged a long-simmering dispute with four securities firms and said the list of brokers it wo n't do business with may be lengthened in the months ahead .	contrast	1804..2014
" We 've been opposed to " index arbitrage " for a long time , " said Stephen B. Timbers , chief investment officer at Kemper , which manages $ 56 billion , including $ 8 billion of stocks .	norel	2017..2195
" Index arbitrage does n't work , and it scares natural buyers " of stock .	cause	2196..2266
While Mr. Timbers explained he 's " not totally convinced index arbitrage changes the overall level of the stock market , " he said that " on an intraday basis , it has major effects .	norel	2269..2446
We 've talked to proponents of index arbitrage and told them to cool it because they 're ruining the market .	cause	2447..2553
They said , ' Too bad , ' so we finally said we 're not going to do business with them . "	contrast	2554..2638
Kemper also blasted the Big Board for ignoring the interests of individual and institutional holders .	norel	2641..2742
" The New York Stock Exchange has vested interests " in its big member securities firms " that cloud its objectivity , " Mr. Timbers said .	restatement	2743..2876
" It has never been interested in what we think .	cause	2877..2924
The Big Board also has a terrible communication problem with individual investors , " he added .	conjunction	2925..3018
Small investors perceive that " big operators " dominate the market , said Thomas O'Hara , chairman of the National Association of Investors and head of the exchange 's Individual Investors Advisory Committee set up after the 1987 crash .	norel	3021..3253
" The impression I 've got is they 'd love to do away with it { program trading } , but they { the exchange } ca n't do it , " he said .	cause	3254..3378
Big Board Chairman John J. Phelan said in a recent interview that he has no inclination to eliminate program trading .	norel	3381..3498
He said the market 's volatility disturbs him , but that all the exchange can do is " slow down the process " by using its circuit breakers and shock absorbers .	restatement	3499..3655
Mr. Timbers countered that " the mere fact they put in circuit breakers is an admission of their problems . "	norel	3658..3764
Morgan Stanley and Kidder Peabody , the two biggest program trading firms , staunchly defend their strategies .	norel	3767..3875
" We continue to believe the position we 've taken is reasonable , " a Morgan Stanley official said .	restatement	3876..3972
" We would stop index arbitrage when the market is under stress , and we have recently , " he said , citing Oct. 13 and earlier this week .	restatement	3973..4106
Michael Carpenter , president and chief executive officer at Kidder Peabody , said in a recent interview , " We do n't think that index arbitrage has a negative impact on the market as a whole . "	conjunction	4107..4296
According to Lawrence Eckenfelder , a securities industry analyst at Prudential-Bache Securities Inc. , " Kemper is the first firm to make a major statement with program trading . "	norel	4299..4475
He added that " having just one firm do this is n't going to mean a hill of beans .	contrast	4476..4556
But if this prompts others to consider the same thing , then it may become much more important .	concession	4557..4651
