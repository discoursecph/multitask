Next to Kohlberg Kravis Roberts 's megabillion RJR Nabisco deal , SCI Television is small fry .	ROOT	0
But the troubles of SCI TV are a classic tale of the leveraged buy - out excesses of the 1980s , especially the asset - stripping game .	COREF_PREV	1
SCI TV , which expects to release a plan to restructure $ 1.3 billion of debt in the next day or so , is n't just another LBO that went bad after piling on debt -- though it did do that .	COREF_PREV	2
The cable and TV station company was an LBO of an LBO , a set of assets that were leveraged twice , enabling the blue - chip buy - out king Henry Kravis in 1987 to take more than $ 1 billion of cash out of the com - pany .	COREF_PREV	3
SCI TV 's buy - out was an ace in the hole for Mr. Kravis and for investors in KKR partnerships .	COREF_PREV	4
But it has left holders of SCI TV 's junk bonds holding the bag , including some heavyweights that KKR might need to finance future deals , such as Kemper Financial Services , First Executive , Columbia Savings -AMP- Loan and Prudential Insurance Co. of America .	COREF_PREV	5
Some junk - holders are said to be considering legal action against KKR or moves to force SCI TV into bankruptcy court .	COREF_PREV	6
And KKR 's majority partner in , Nashville , Tenn. , entrepreneur George Gillett ,SCI TV 's buy - out also is said to be very unhappy .	COREF_PREV	7
SCI TV 's six stations once were part of Storer Communications .	COREF_PREV	8
KKR loaded up the cable and television company with debt in an 1985 buy - out , then later sold Storer 's cable operations at a fat profit .	COREF_PREV	9
In 1987 , KKR for the second time piled debt onto Storer 's TV stations , selling them for $ 1.3 billion to a new entity that was 45 % - owned by KKR and 55 % - owned by Gillett Corp. , which now operates the SCI TV stations .	COREF_PREV	10
In this second LBO , KKR with one hand took more than $ 1 billion of cash out of the TV company 's assets and moved it into the Storer cable operations , making them more valuable in a 1988 sale .	COREF_PREV	11
( Storer also took $ 125 million of junior SCI TV bonds as partial payment for the TV assets . )	COREF_PREV	12
With the other hand , KKR put back into SCI TV less than 10 % of the cash it had taken out , buying SCI TV common and preferred shares .	COREF_PREV	13
So , while KKR today has an estimated $ 250 million sunk in now - shaky SCI TV , including equity and debt , the LBO firm still is $ 1 billion ahead on the SCI TV buy - out after taking cash up front .	COREF_PREV	14
On Storer as a whole , KKR racked up compound annual returns of 60 % in the three years it owned Storer .	COREF_PREV	15
Meanwhile , Mr. Gillett risks losing his entire equity investment of about $ 100 million in SCI TV if the company ca n't be restructured .	COREF_PREV	16
Overall , , Mr. Gillett 's holding companyGillett Holdings , is heavily indebted and , except for its Vail Mountain resorts , is n't doing very well .	COREF_PREV	17
With the TV business falling on hard times in recent years , analysts say that if SCI TV had to be liquidated today , it might fetch 30 % less than in the 1987 buy - out , wiping out most of the company 's junk - holders and its stockholders .	COREF_OTHER	18
Meanwhile , SCI TV can barely pay its cash interest bill , and to stay out of bankruptcy court it must soon reschedule a lot of bank loans and junk bonds that have fallen due .	COREF_PREV	19
SCI TV 's grace period for paying its bills is due to expire Nov. 16 .	COREF_PREV	20
It now is quietly circulating among creditors a preliminary plan to restructure debt .	COREF_PREV	21
Negotiations `` have started con - structively , but that 's not to say we like this particular offer , '' says Wilbur Ross of Rothschild Inc. , adviser to SCI TV junk - holders .	COREF_PREV	22
No major player in the SCI TV deal will talk publicly .	COREF_PREV	23
But it 's understood that Mr. Kravis is disappointed that Mr. Gillett did n't manage to boost SCI TV 's operating profit after the buy - out .	COREF_PREV	24
Mr. Kravis apparently thinks SCI TV can survive if lenders extend its debt payments until TV stations rise in value again , allowing SCI TV to sell assets to pay debt .	COREF_PREV	25
Mr. Gillett is said to be proud of his operating record ; he has lifted some stations ' ratings and turned around a Detroit station .	COREF_OTHER	26
As for junk - holders , they 're discovering it can be a mistake to take the other side of a trade by KKR .	NONE	27
The bonds of SCI TV now are being quoted at prices ranging from only five cents to about 60 cents on the dollar , according to R.D. Smith -AMP- Co. in New York , which trades distressed securities .	COREF_OTHER	28
People who have seen SCI TV 's restructuring plan say it offers concessions by KKR and Gillett Corp .	COREF_PREV	29
They would both give part of their combined $ 50 million in common equity in SCI TV to holders of SCI TV 's $ 488 million of junk bonds , as a carrot to persuade them to accept new bonds that might reduce the value of their claims on the company .	COREF_PREV	30
But some militant SCI TV junk - holders say that 's not enough .	COREF_PREV	31
They contend that SCI TV 's equity now is worthless .	COREF_PREV	32
They add that it is n't costing KKR anything to give up equity because of its big up - front cash profit on the buy - out , which they think contributed to SCI TV 's current problems .	COREF_PREV	33
Kemper , the biggest holder of senior ,SCI TV bonds has refused to join the bond - holders committee and is said to be reviewing its legal options .	COREF_OTHER	34
To protect their claims , some junk - holders want KKR and perhaps Mr. Gillett to invest new money in SCI TV , perhaps $ 50 million or more .	COREF_PREV	35
One investment banker who is n't involved in the deal says SCI TV needs at least $ 50 million of new equity to survive .	COREF_PREV	36
Junk - holders say they have a stick to beat KKR with : `` The threat of bankruptcy is a legitimate tool '' to extract money from KKR , says one big SCI TV holder .	COREF_OTHER	37
This could be the first major bankruptcy - law proceeding for KKR , he adds .	COREF_OTHER	38
A big bankruptcy - court case might tarnish KKR 's name , and provide new fuel for critics of LBOs in Washington and elsewhere .	COREF_PREV	39
But others say junk - holders have nothing to gain by putting SCI TV into bankruptcy - law proceedings .	COREF_OTHER	40
While KKR does n't control SCI TV which is unusual for a KKR investment -- it clearly has much deeper pockets than Mr. Gillett .	COREF_PREV	41
Bankruptcy specialists say Mr. Kravis set a precedent for putting new money in sour LBOs recently when KKR restructured foundering Seaman Furniture , doubling KKR 's equity stake .	COREF_PREV	42
But with Seaman , KKR was only trying to salvage its original investment , says bankruptcy investor James Rubin of Sass Lamle Rubin in New York .	COREF_PREV	43
By contrast , KKR probably has already made all the money it can on SCI TV .	COREF_PREV	44
And people who know Mr. Kravis say he is n't in a hurry to pour more money into SCI TV .	COREF_PREV	45
