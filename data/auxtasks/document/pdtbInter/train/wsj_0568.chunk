Unisys Corp. 's announcement Friday of a $648.2 million loss for the third quarter showed that the company is moving even faster than expected to take write-offs on its various problems and prepare for a turnaround next year.	O	9..234
At the same time, the sheer size of the loss, coupled with a slowing of orders, made some securities analysts wonder just how strong that turnaround will be at the computer maker and defense-electronics concern.	O	237..448
"Unisys is getting clobbered.	O	451..480
Just clobbered," said Ulric Weil, an analyst at Weil & Associates who had once been high on the company.	O	481..585
"The quarter was terrible, and the future looks anything but encouraging."	O	586..660
Unisys, whose revenue inched up 3.7% in the quarter to $2.35 billion from $2.27 billion in the year-earlier quarter, had an operating loss of about $30 million.	O	663..823
On top of that, the Blue Bell, Pa., concern took a $230 million charge related to the layoffs of 8,000 employees.	B-entrel	824..937
That is at the high end of the range of 7,000 to 8,000 employees that Unisys said a month ago would be laid off.	I-entrel	938..1050
Unisys said that should help it save $500 million a year in costs, again at the high end of the previously reported range of $400 million to $500 million.	O	1051..1205
The company also took a write-off of $150 million to cover losses on some fixed-price defense contracts, as some new managers took a hard look at the prospects for that slow-growing business.	O	1208..1399
In addition, Unisys set up an unspecified reserve -- apparently $60 million to $70 million -- to cover the minimum amount it will have to pay the government because of its involvement in the defense-procurement scandal.	B-conjunction	1400..1619
Unisys also noted that it paid $78.8 million in taxes during the quarter, even though tax payments normally would be minimal in a quarter that produced such a big loss.	I-conjunction	1620..1788
The tax payments will leave Unisys with $225 million in loss carry-forwards that will cut tax payments in future quarters.	O	1791..1913
In addition, Unisys said it reduced computer inventories a further $100 million during the quarter, leaving it within $100 million of its goal of a reduction of $500 million by the end of the year.	O	1914..2111
Still, Unisys said its European business was weak during the quarter, a worrisome sign given that the company has relied on solid results overseas to overcome weakness in the U.S. over the past several quarters.	B-conjunction	2114..2325
The company also reported slower growth in another important business: systems that use the Unix operating system.	I-conjunction	2328..2442
That would be a huge problem if it were to continue, because Unisys is betting its business on the assumption that customers want to move away from using operating systems that run on only one manufacturer's equipment and toward systems -- mainly Unix -- that work on almost anyone's machines.	O	2443..2736
In addition, Unisys must deal with its increasingly oppressive debt load.	B-restatement	2739..2812
Debt has risen to around $4 billion, or about 50% of total capitalization.	B-cause	2813..2887
That means Unisys must pay about $100 million in interest every quarter, on top of $27 million in dividends on preferred stock.	I-cause	2888..3015
Jim Unruh, Unisys's president, said he is approaching next year with caution.	O	3018..3095
He said the strength of the world-wide economy is suspect, and doesn't see much revenue growth in the cards.	B-conjunction	3096..3204
He also said that the price wars flaring up in parts of the computer industry will continue through next year.	I-conjunction	3207..3317
He said the move toward standard operating systems means customers aren't locked into buying from their traditional computer supplier and can force prices down.	O	3318..3478
That, he said, is why Unisys is overhauling its whole business: It needs to prepare for a world in which profit margins will be lower than computer companies have been used to.	O	3479..3655
"We've approached this not as a response to a temporary condition in the industry but as a fundamental change the industry is going through," Mr. Unruh said.	O	3658..3815
The information-systems industry is still going to be a high-growth business, and we're confident that we have tremendous assets as a company.	B-contrast	3816..3959
But we don't minimize the challenges of the near term."	I-contrast	3960..4015
Securities analysts were even more cautious, having been burned repeatedly on Unisys this year.	O	4018..4113
Some had predicted earnings of more than $4 a share for this year, up from last year's fully diluted $3.27 a share on earnings of $680.6 million.	B-concession	4114..4259
But the company said Friday that it had losses of $673.3 million through the first nine months, compared with earnings a year earlier of $382.2 million, or $2.22 a share fully diluted, as revenue inched up 1.4% to $7.13 billion from $7.03 billion.	I-concession	4260..4507
And Unisys is expected to do little better than break even in the fourth quarter.	O	4508..4589
So Steve Milunovich at First Boston said he is cutting his earnings estimate for next year to $2 a share from $3.	O	4592..4705
"I was feeling like I was too high to begin with," he said.	O	4706..4765
Mr. Weil of Weil & Associates said he will remain at $1 a share for next year but said he wonders whether even that low target is at risk.	O	4768..4906
"The break-even point for next year is much lower, but is it low enough?" he asked.	O	4907..4990
Reflecting the concern, Unisys stock fell a further 75 cents to $16.25 in composite trading Friday on the New York Stock Exchange.	O	4993..5123
