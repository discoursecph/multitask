The stock market 's dizzying gyrations during the past few days have made a lot of individual investors wish they could buy some sort of insurance .	O	9..155
After all , they wo n't soon forget the stock bargains that became available after the October 1987 crash .	B-contrast	158..262
But while they want to be on the alert for similar buying opportunities now , they 're afraid of being hammered by another terrifying plunge .	I-contrast	263..402
The solution , at least for some investors , may be a hedging technique that 's well known to players in the stock-options market .	B-restatement	405..532
Called a " married put , " the technique is carried out by purchasing a stock and simultaneously buying a put option on that stock .	I-restatement	533..661
It 's like " fire insurance , " says Harrison Roth , the senior options strategist at Cowen & Co .	O	664..756
Because a put option gives its owner the right , but not the obligation , to sell a fixed number of shares of the stock at a stated price on or before the option 's expiration date , the investor is protected against a sudden drop in the stock 's price .	B-concession	757..1005
But most investment advisers do n't recommend using married puts all the time .	B-cause	1008..1085
That 's because the cost of buying put options eats into an investor 's profit when stock prices rise .	I-cause	1086..1186
" This is the type of fire insurance you only buy when the nearby woods are on fire , " says Mr. Roth .	O	1189..1288
" You always want your house insured , but you do n't always feel the need for your investments to be insured . "	O	1289..1397
In addition to hedging new stock purchases , the married-put technique can be used to protect stocks that an investor already owns .	B-asynchronous	1400..1530
In either case , the investor faces three possible outcomes :	I-asynchronous	1531..1590
-- If the stock goes up in price between now and the put 's expiration date , the put will probably expire worthless .	O	1593..1708
The investor will be out the cost of the put , which is called the " premium , " and this loss will reduce the stock-market profit .	O	1709..1836
-- If the stock stays at the same price between now and the put 's expiration date , the investor 's loss will be limited to the cost of the put , less any amount realized from a closing sale of the put .	B-cause	1839..2038
The worst-case scenario would be if the put expires worthless .	I-cause	2039..2101
-- If the price of the stock declines , the put will increase in value .	B-restatement	2104..2174
Once the stock price is less than the exercise price , or " strike price , " of the put , the gain will match the loss on the stock dollar for dollar .	B-entrel	2175..2320
The put establishes a minimum selling price for the stock during its life .	I-entrel	2321..2395
When a stock falls below the put 's strike price , the investor simply sells the stock at a loss and simultaneously sells the put at a profit .	B-alternative	2398..2538
Or , the investor can exercise the put , by tendering the stock to his or her broker in return for payment from another investor who has sold a put on the same stock .	B-entrel	2539..2703
Brokers handle such transactions through the Options Clearing Corp. , which guarantees all option trades .	I-entrel	2704..2808
The accompanying table shows how this strategy would work for three stocks .	B-entrel	2811..2886
Though not reflected in the table , an investor should know that the cost of the option insurance can be partially offset by any dividends that the stock pays .	I-entrel	2887..3045
For example , Tenneco Inc. pays a quarterly dividend of 76 cents , which would be received before the February option expires and , thus , reduce the cost of using the technique by that amount .	B-cause	3048..3237
In this case , the investor 's risk would n't exceed 3.6 % of the total investment .	B-entrel	3238..3317
To simplify the calculations , commissions on the option and underlying stock are n't included in the table .	I-entrel	3318..3424
There are more than 650 stocks on which options may be bought and sold , including some over-the-counter stocks .	B-contrast	3427..3538
But some investors might prefer a simpler strategy then hedging their individual holdings .	I-contrast	3539..3629
They can do this by purchasing " index puts , " which are simply put options on indexes that match broad baskets of stocks .	B-instantiation	3632..3752
For instance , the most popular index option is the S&P 100 option , commonly called the OEX .	B-entrel	3753..3844
It is based on the stocks that make up Standard & Poor 's 100-stock index .	B-entrel	3845..3918
Unlike options on individual issues , index options are settled only in cash , and no stock is ever tendered .	B-contrast	3919..4026
But while index options are convenient , they have several disadvantages .	I-contrast	4029..4101
For one thing , an investor 's portfolio might not closely match the S&P 100 .	B-cause	4102..4177
As a result , the OEX insurance may or may not fully protect an investor 's holdings in the event of a market decline .	B-conjunction	4178..4294
In addition , OEX options were suspended from trading last Friday afternoon , after the stock-market sell-off got under way and trading in the S&P-500 futures contract was halted .	I-conjunction	4297..4474
So an investor who wanted to realize a profit on OEX puts after the trading suspension would have been out of luck .	O	4475..4590
On the other hand , only a handful of individual issues were suspended from trading on Friday .	B-entrel	4591..4684
Normally , once the underlying investment is suspended from trading , the options on those investments also do n't trade .	I-entrel	4685..4803
Ultimately , whether the insurance provided by purchasing puts is worthwhile depends on the cost of the options .	B-entrel	4806..4917
That cost rises in times of high market volatility .	B-contrast	4918..4969
But it still might be cheaper than taking a major hit .	I-contrast	4970..5024
The protection from using married puts is clearly superior to that afforded by another options strategy some investors consider using during troubled times : selling call options on stocks the investor owns .	B-entrel	5027..5233
A call option is similar to a put , except that it gives its owner the right to buy shares at a stated price until expiration .	I-entrel	5234..5359
Selling a call option gives an investor a small buffer against a stock-market decline .	B-cause	5362..5448
That 's because it reduces the cost of the stock by the amount of premium received from the sale of the call .	I-cause	5449..5557
But if the price of the stock rises above the strike price of the option , the stock is almost certain to be called away .	B-conjunction	5558..5678
And in that case , the investor misses out on any major upside gain .	I-conjunction	5679..5746
These calculations exclude the effect of commissions paid and dividends received from the stock .	B-entrel	5749..5845
All prices are as of Monday 's close .	I-entrel	5846..5882
