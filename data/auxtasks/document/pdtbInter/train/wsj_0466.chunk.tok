Many investors give Michael Foods about as much chance of getting it together as Humpty Dumpty .	B-contrast	9..104
But now at least there 's a glimmer of hope for the stock .	I-contrast	105..162
Burger King , which breaks thousands of fresh eggs each morning , is quietly switching over to an alternative egg product made by Michael Foods .	B-entrel	165..307
Known as Easy Eggs , the product has disappointed investors .	B-cause	308..367
When the company this month announced lower-than-forecast sales of Easy Eggs , the stock dropped nearly 19 % .	I-cause	368..475
Michael wo n't confirm the identities of any Easy Egg customers , nor will it say much of anything else .	B-cause	478..580
Two Minneapolis shareholder suits in the past month have accused top officers of making " various untrue statements . "	B-restatement	581..697
These federal-court suits accuse the officers of failing to disclose that Easy Eggs were unlikely to sell briskly enough to justify all of Michael 's production capacity .	B-contrast	698..867
But at least Burger King has signed on , and says that by year end it wo n't be using any shell eggs .	I-contrast	870..969
The Miami fast-food chain , owned by Grand Metropolitan of Britain , expects to consume roughly 34 million pounds of liquefied eggs annually .	B-cause	970..1109
So there is reason to believe that Michael 's hopes for a bacteria-free , long-shelf-life egg were n't all hype .	B-entrel	1112..1221
( Easy Eggs are pasteurized in a heat-using process . )	I-entrel	1222..1274
Still , caution is advisable .	O	1275..1303
A company official says Michael 's break-even volume on Easy Eggs is around 60 million pounds a year -- apparently well above current shipments and a far cry from what the company once suggested was a billion-pound market waiting for such a product .	B-entrel	1304..1552
Perhaps to debunk the analysts ' talk of over-capacity , Michael today will take some of the skeptics on a tour of its new Gaylord , Minn. , plant .	I-entrel	1553..1696
There has been no announcement of the Burger King arrangement by either party , possibly for fear that McDonald 's and other fast-food rivals would seize on it in scornful advertising .	B-contrast	1699..1881
But Burger King operators independently confirm using Michael 's product .	B-conjunction	1882..1954
Other institutional users reportedly include Marriott , which is moving away from fresh eggs on a region-by-region basis .	B-contrast	1955..2075
The extent of Marriott 's use is n't known , and Marriott officials could n't be reached for comment .	I-contrast	2076..2173
Michael Foods has attracted a good many short-sellers , the people who sell borrowed shares in a bet that a stock price will drop and allow the return of cheaper shares to the lender .	O	2176..2358
Many analysts question management 's credibility .	O	2359..2407
" The stock , in my opinion , is going to go lower , not only because of disappointing earnings but { because } the credibility gap is certainly not closing , " says L. Craig Carver of Dain Bosworth .	O	2410..2601
Mr. Carver says that at a recent Dain-sponsored conference in New York , he asked Michael 's chief executive officer if the fourth quarter would be down .	B-entrel	2604..2755
The CEO , Richard G. Olson , replied " yes , " but would n't elaborate .	B-entrel	2756..2821
( The company did n't put out a public announcement .	B-contrast	2822..2872
A spokesman said later that Mr. Olson was being " conservative " in his estimate .	B-concession	2873..2952
But the spokesman added that while Michael will earn less than last year 's $ 1.20 a share , it thinks Street estimates of $ 1 or so are low . )	I-concession	2953..3091
Analyst Robin Young of John Kinnard & Co. , Minneapolis , calls himself " the last remaining bull on the stock . "	O	3094..3203
He argues that Michael Foods is misunderstood : " This is a growth company in the packaged food industry -- a rare breed , like finding a white rhino . "	O	3204..3352
Earnings are n't keeping pace , he says , because of heavy investments in the egg technologies and drought-related costs in its potato business .	O	3353..3494
Mr. Carver , however , believes the company 's egg product wo n't help the bottom line in the short run , even though it " makes sense -- it 's more convenient " and justifies its price , which is higher than shell eggs , because of health and sanitation concerns .	O	3497..3751
Prospective competition is one problem .	B-cause	3754..3793
Last week a closely held New Jersey concern , Papetti High-Grade Egg Products Co. , rolled out an aseptically packaged liquefied item called Table Ready .	B-entrel	3794..3945
Company President Steve Papetti says Marriott will be among his clients as well .	I-entrel	3946..4026
Michael shares closed at 13 3/4 yesterday in national over-the-counter trading .	O	4029..4108
Says New York-based short seller Mark Cohodes , " In my mind this is a $ 7 stock . "	B-entrel	4109..4188
Michael late yesterday announced a $ 3.8 million stock buy-back program .	I-entrel	4189..4260
Michael , which also processes potatoes , still relies on spuds for about a fourth of its sales and nearly half its pretax profit .	B-contrast	4263..4391
But dry growing conditions in the Red River Valley of Minnesota and North Dakota are pushing spot prices of potatoes beyond what Michael contracted to pay last spring .	B-cause	4392..4559
Company lawyers recently sent letters to growers saying that Michael " would take very seriously any effort ... to divert its contracted-for potatoes to other outlets . "	I-cause	4560..4727
Still , analysts believe that profit margins in the potato business will be down again this year .	O	4728..4824
