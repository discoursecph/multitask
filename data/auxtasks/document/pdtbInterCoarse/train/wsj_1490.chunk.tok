The collapse of a $ 6.79 billion buy-out of United Airlines parent UAL Corp. has handed Wall Street 's takeover stock speculators their worst loss ever on a single deal .	O	9..176
Their $ 700 million-plus in estimated paper losses easily tops the $ 400 million in paper losses the takeover traders , known as arbitragers , suffered in 1982 when Gulf Oil Co. dropped a $ 4.8 billion offer for Cities Service Co .	O	179..404
In the six trading days since the UAL labor-management buy-out group failed to get bank financing , culminating Friday with the withdrawal of its partner British Airways PLC , UAL stock has plummeted by 41 % to 168 1/2 from 285 1/4 .	O	407..636
The arbs may recoup some of their paper losses if the UAL deal gets patched up again , as they did in 1982 when Occidental Petroleum Co. rescued them with a $ 4 billion takeover of Cities Service .	O	639..833
In the meantime , the question faced by investors is : What is UAL stock worth ?	O	836..913
The short answer , on a fundamental basis , is that airline analysts say the stock is worth somewhere between $ 135 and $ 150 a share .	O	916..1046
That 's based on a multiple of anywhere between 8.5 to 10 times UAL earnings , which are estimated to come in somewhere around $ 16 a share this year .	O	1047..1194
Airline stocks typically sell at a discount of about one-third to the stock market 's price-earnings ratio -- which is currently about 13 times earnings .	O	1197..1349
That 's because airline earnings , like those of auto makers , have been subject to the cyclical ups-and-downs of the economy .	O	1350..1473
That analysis matches up with stock traders ' reports that , despite the huge drop in the stock , UAL has n't returned to the level at which it could attract buying by institutions solely on the basis of earnings .	O	1476..1685
So anyone buying the stock now is betting on some special transaction such as a recapitalization or takeover , and must do so using some guesswork about the likelihood of such an event .	O	1688..1872
One analyst , who asked not to be identified , said he believes that the UAL pilots and management can put together a bid " in the $ 225 area , " but that it could take three to four months to close .	O	1875..2068
At that level , and given the uncertainty , he believes UAL stock should trade closer to	O	2069..2155
Other observers note that UAL 's board , having accepted a bid of $ 300 a share , might hold out for a new bid much closer to the original level -- even if it means that the management goes back to running the company for a while and lets things return to normal .	O	2158..2417
By that logic , the closing of a deal could be much further away than three to four months , even though the eventual price might be higher .	O	2418..2556
Investment bankers following UAL agree that the strongest impetus for an eventual deal is that the pilots have been attempting a buy-out for more than two years , and are n't likely to stop , having come so close to success .	O	2559..2780
The pilots have a strong financing tool in their willingness to cut their annual compensation by $ 200 million , and to commit $ 200 million from their retirement funds .	B-expansion	2783..2949
The pilots have a strong financing tool in their willingness to cut their annual compensation by $ 200 million , and to commit $ 200 million from their retirement funds .	B-expansion	2783..2949
On Friday , they also persuaded the UAL flight attendants to join them .	I-expansion	2950..3020
However , investment bankers say that banks are n't likely to lend the almost $ 5 billion that would be necessary for a takeover even at a lower price without someone putting up a hefty wad of cash -- probably even greater than the 17 % in cash put up by investors in the leveraged takeover of Northwest Airlines parent NWA Corp. in July .	O	3023..3357
Banks want to see someone putting up real cash at risk , that is , subordinate to the bank debt in any deal .	O	3360..3466
That way , they figure , someone else has an even stronger motivation to make sure the deal is going to work , because they would be losing their money before the banks lost theirs .	O	3467..3645
Banks also want to be able to call someone on the telephone to fix a problem with a deal that goes bad -- preferably someone other than a union leader .	O	3648..3799
That leaves the pilots still in need of cash totaling around $ 1 billion -- far more than either they or the flight attendants can lay their hands on from retirement funds alone .	O	3802..3979
One obstacle to the pilots ' finding such a huge amount of cash is their insistence on majority ownership .	B-contingency	3982..4087
Investors such as Marvin Davis of Los Angeles who have sought airline ownership this year have insisted they , not the pilots , must have control .	I-contingency	4088..4232
One way out of that dilemma could be a partial recapitalization in which the pilots would wind up sharing the value of their concessions with public shareholders .	B-temporal	4235..4397
The pilots could borrow against the value of their concessions , using the proceeds to buy back stock from the public and give themselves the majority control they have been seeking .	B-comparison	4398..4579
But it is n't clear that banks would lend sufficient money to deliver a big enough price to shareholders .	B-contingency	4582..4686
The lack of any new cash probably would still leave the banks dissatisfied .	B-entrel	4687..4762
In advising the UAL board on the various bids for the airline , starting with one for $ 240 a share from Mr. Davis , the investment bank of First Boston came up with a wide range of potential values for the company , depending on appraisal methods and assumptions .	I-entrel	4763..5023
Using the the NWA takeover as a benchmark , First Boston on Sept. 14 estimated that UAL was worth $ 250 to $ 344 a share based on UAL 's results for the 12 months ending last June 30 , but only $ 235 to $ 266 based on a management estimate of results for 1989 .	B-contingency	5026..5279
First Boston 's estimates had been higher before management supplied a 1989 projection .	I-contingency	5280..5366
Using estimates of the company 's future earnings under a variety of scenarios , First Boston estimated UAL 's value at $ 248 to $ 287 a share if its future labor costs conform to Wall Street projections ; $ 237 to $ 275 if the company reaches a settlement with pilots similar to one at NWA ; $ 98 to $ 121 under an adverse labor settlement , and $ 229 to $ 270 under a pilot contract imposed by the company following a strike .	B-expansion	5369..5782
And using liquidation value assuming the sale of all UAL assets , First Boston estimated the airline is worth $ 253 to $ 303 a share .	I-expansion	5785..5915
Unfortunately , all those estimates came before airline industry fundamentals deteriorated during the past month .	B-expansion	5918..6030
American Airlines parent AMR and USAir Group , both subject to takeover efforts themselves , have each warned of declining results .	I-expansion	6031..6160
Some analysts do n't expect a quick revival of any takeover by the pilots .	O	6163..6236
The deal has , as one takeover expert puts it , " so many moving parts .	O	6237..6305
I do n't see anybody who 's sophisticated getting his name associated with this mess until the moving parts stop moving . "	O	6306..6425
In addition to the need for another cash equity investor , the other moving parts include : the pilots themselves , who can scuttle rival deals by threatening to strike ; the machinists union , the pilots ' longtime rivals who helped scuttle the pilots ' deal ; and regulators in Washington , whose opposition to foreign airline investment helped throw the deal into doubt .	O	6428..6792
In the meantime , the arbs are bleeding .	B-entrel	6795..6834
Wall Street traders and analysts estimate that takeover stock traders own UAL stock and options equal to as many as 6.5 million shares , or about 30 % of the total outstanding .	I-entrel	6835..7009
Frank Gallagher , an analyst with Phoenix Capital Corp. in New York , estimates that the arbs paid an average of about $ 280 a share for their UAL positions .	O	7010..7164
That would indicate that the arbs have paper losses on UAL alone totalling $ 725 million .	O	7165..7253
UAL Corp . ( NYSE ; Symbol : UAL )	O	7256..7287
Business : Airline	O	7290..7307
Year ended Dec. 31 , 1988 :	O	7310..7335
Sales : $ 8.98 billion	O	7338..7358
Net income * : $ 599.9 million ; or $ 20.20 a share	O	7361..7407
Second quarter , June 30 , 1989 : Per-share earnings : $ 6.52 vs. $ 5.77	O	7410..7476
Average daily trading volume : 881,969 shares	O	7479..7525
Common shares outstanding : 21.6 million	O	7528..7567
