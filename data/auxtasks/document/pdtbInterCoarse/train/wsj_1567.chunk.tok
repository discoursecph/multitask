WHEN JAMES SCHWARTZ was just a lad his father gave him a piece of career advice .	O	9..89
" He told me to choose an area where just by being mediocre I could be great , " recalls Mr. Schwartz , now 40 .	O	90..197
He tried management consulting , traded in turquoise for a while , and even managed professional wrestlers .	B-comparison	198..303
Now he has settled into a career that fits the bill -- financial planning .	I-comparison	304..378
It should be noted that Mr. Schwartz , who operates out of Englewood , Colo. , is a puckish sort who likes to give his colleagues the needle .	B-comparison	381..519
But in this case the needle has a very sharp point .	B-contingency	520..571
Though it 's probably safe to assume that the majority of financial planners are honest and even reasonably competent , the fact remains that , as one wag puts it , " anybody who can fog a mirror " can call himself a financial planner .	I-contingency	572..801
Planners now influence the investment of several hundred billion dollars , but in effect they operate in the dark .	O	804..917
There is no effective regulation of planners , no accepted standard for admission into their ranks -- a dog got into one trade group -- no way to assess their performance , no way even to know how many of them there are ( estimates range from 60,000 to 450,000 ) .	O	918..1177
All anyone need do is hang up a shingle and start planning .	O	1178..1237
So it should come as no shock that the profession , if that 's what it is , has attracted a lot of people whose principal talents seem to be frittering away or flat-out stealing their clients ' money .	B-contingency	1240..1436
Alarmed , state and federal authorities are trying to devise ways to certify and regulate planners .	I-contingency	1437..1535
Industry groups and reputable planners who are members of them want comprehensive standards , too ; they 're tired of seeing practitioners depicted collectively in the business press as dumber than chimpanzees and greedier than a herd of swine .	O	1536..1777
But reform has n't taken hold yet .	O	1780..1813
" The industry is still pretty much in its Wild West days , " says Scott Stapf , director of investor education for the North American Securities Administrators Association .	O	1814..1983
An admittedly limited survey by NASAA , whose members are state securities-law regulators , found that between 1986 and 1988 " fraud and abuse " by financial planners cost 22,000 investors $ 400 million .	O	1986..2184
The rogues ' gallery of planners involved includes some convicted felons , a compulsive gambler or two , various businessmen who had planned their own previous ventures right into bankruptcy , and one man who scammed his wife 's grandmother .	O	2185..2421
What 's more , the losses they and the others caused " are just what we are stumbling over , " says Mr. Stapf , adding that the majority of misdeeds probably go undetected .	O	2422..2588
So do just about all the losses that could be attributed to the sheer incompetence of unqualified planners .	B-expansion	2591..2698
Nobody can estimate the toll , but John Gargan , a Tampa , Fla. , planner and head of one trade group , the International Association of Registered Financial Planners , thinks the danger to investors from incompetence is " humongous , " far greater than that from crookery .	I-expansion	2699..2963
His group , like others , wants minimum standards applied to all who call themselves financial planners .	O	2964..3066
Surveying all this , some people now think the best planner might be no planner at all .	O	3069..3155
For most investors " the benefits just are n't worth the risks , " says Barbara Roper , who follows financial-planning issues for the Consumer Federation of America , a consumer-advocacy organization based in Washington .	O	3156..3370
She concedes that such a position is " unfair " to the thousands of conscientious and qualified people plying the trade , but as a consumer advocate she feels impelled to take it .	O	3373..3549
She says her group used to give tips on selecting planners -- check educational and experience credentials , consult regulators and Better Business Bureaus -- but found that even some people who took these steps " were still getting ripped off . "	O	3550..3793
The bad news , however , has n't been bad enough to kill the growing demand for financial planning .	B-comparison	3796..3892
The Tax Reform Act of 1986 , which eliminated many tax shelters peddled by planners , and the stock market crash the next year did cause a sharp slump in such demand , and many planners had to make an unplanned exit from the business .	B-comparison	3893..4124
But membership in the International Association of Financial Planners ( IAFP ) , the industry 's biggest trade group , is still nearly triple what it was in 1980 , and it 's believed that the ranks of planners who do n't belong to any group have soared as well .	I-comparison	4125..4378
An estimated 10 million Americans are now using financial planners , and the pool of capital they influence is enormous .	O	4381..4500
A survey of 54,000 of them conducted by the IAFP in April showed that these practitioners alone had controlled or guided the investment of $ 154 billion of their clients ' money in the previous 12 months .	O	4501..4703
The sheer number of planners makes the business extremely difficult , if not impossible , to regulate .	O	4706..4806
Even the minority of them who must register with the Securities and Exchange Commission as " investment advisers " -- people who are in the business of counseling others on the buying and selling of securities specifically -- have been enough to swamp the agency 's capacity .	O	4807..5079
The SEC has only about 200 staffers assigned to keep tabs on investment advisers -- about the same as in 1980 -- even though the number of advisers has tripled to about 15,000 over the past decade .	B-contingency	5080..5277
Currently , a registered investment adviser can expect an SEC audit only once every 12 years .	B-comparison	5278..5370
A lot of bad things can happen in 12 years .	I-comparison	5371..5414
" It does n't take a rocket scientist to figure out our problem , " says Kathryn McGrath , director of the SEC 's division of investment management .	O	5415..5557
So the SEC has proposed to Congress that much of the job of oversight be turned over to an industry-funded , self-regulatory organization patterned on the National Association of Securities Dealers , which operates in the brokerage business .	O	5560..5799
Such an organization could , among other things , set minimum standards for competence , ethics and finances and punish those investment advisers who broke the rules .	O	5800..5963
The proposal has set off a lively debate within an industry that was far from united to begin with .	B-expansion	5966..6065
Mr. Schwartz , the puckish planner from Englewood , Colo. , says that allowing the business to police itself would be " like putting Dracula in charge of the blood bank . "	B-comparison	6066..6232
Mr. Gargan , the Tampa planner who heads one trade group , favors simply assessing the industry and giving the money to the SEC to hire more staff .	I-comparison	6233..6378
( Mr. Gargan 's views are not greeted with wild enthusiasm over at the IAFP , the major industry organization .	B-expansion	6381..6488
When the IAFP recently assembled other industry groups to discuss common standards that might be applied to planners , Mr. Gargan 's group was excluded .	B-contingency	6489..6639
That may be because Mr. Gargan , smarting at what he considered slurs on his membership standards made by the rival group , enrolled his dog , Beauregard , as a member of the IAFP .	I-contingency	6640..6816
Then he sent the pooch 's picture with the certificate of membership -- it was made out to " Boris ' Bo ' Regaard " -- to every newspaper he could think of . )	O	6817..6969
The states have their own ideas about regulation and certification .	B-expansion	6972..7039
NASAA , the organization of state securities regulators , is pushing for a model regulatory statute already adopted in eight states .	B-expansion	7040..7170
It requires financial planners to register with states , pass competency tests and reveal to customers any conflicts of interest .	I-expansion	7171..7299
The most common conflict involves compensation .	O	7302..7349
NASAA estimates that nearly 90 % of planners receive some or all of their income from sales commissions on securities , insurance and other financial products they recommend .	O	7350..7522
The issue : Is the planner putting his clients into the best investments , or the ones that garner the biggest commissions ?	O	7523..7644
In 1986 the New York attorney general 's office got an order from a state court in Albany shutting down First Meridian Corp. , an Albany financial-planning firm that had invested $ 55 million on behalf of nearly 1,000 investors .	B-contingency	7647..7872
In its notice of action , the attorney general said the company had promised to put clients into " balanced " investment portfolios ; instead , the attorney general alleged , the company consistently shoved unwary customers into high-risk investments in paintings , coins and Florida condos .	I-contingency	7873..8157
Those investments paid big commissions to First Meridian , payments investors were never told about , the attorney general alleged .	O	8158..8287
Investors were further assured that only those with a minimun net worth would be accepted .	O	8290..8380
In practice , the attorney general alleged in an affidavit , if an investor had access to cash " the chances of being turned down by First Meridian were about as probable as being rejected by the Book-of-the-Month Club . "	O	8381..8598
And , the attorney general added , First Meridian 's president , Roger V. Sala , portrayed himself as a " financial expert " when his qualifications largely consisted of a high-school diploma , work as a real-estate and insurance salesman , and a stint as supervisor at a highway toll booth .	O	8601..8883
First Meridian and its officials are currently under investigation for possible criminal wrongdoing , according to a spokeswoman for the attorney general .	O	8884..9037
Harry Manion , Mr. Sala 's attorney , says his client denies any wrongdoing and adds that the attorney general 's contentions about First Meridian 's business practices are incorrect .	O	9040..9218
As for Mr. Sala 's qualifications , " the snooty attorneys for the state of New York decided Mr. Sala was n't qualified because he did n't have a Harvard degree , " says Mr. Manion .	O	9219..9393
Civil suits against planners by clients seeking recovery of funds are increasingly common .	O	9396..9486
Two such actions , both filed earlier this year in Georgia state court in Atlanta , could be particularly embarrassing to the industry : both name J. Chandler Peterson , an Atlanta financial planner who is a founder and past chairman of the IAFP , as defendant .	O	9487..9743
One suit , filed by more than three dozen investors , charges that Mr. Peterson misused much of the $ 9.7 million put into a limited partnership that he operated and promoted , spending some of it to pay his own legal bills and to invest in other companies in which he had an interest .	O	9746..10027
Those companies , in turn , paid Mr. Peterson commissions and fees , the suit alleges .	O	10028..10111
The other suit was filed by two men in a dispute over $ 100,000 investments each says he made with Mr. Peterson as part of an effort to purchase the Bank of Scottsdale in Scottsdale , Ariz .	O	10114..10301
One plaintiff , a doctor , testified in an affidavit that he also gave Mr. Peterson $ 50,000 to join a sort of investment club which essentially gave the physician " the privilege of making additional investments " with Mr. Peterson .	O	10302..10530
In affidavits , each plaintiff claims Mr. Peterson promised the bank purchase would be completed by the end of 1988 or the money returned .	O	10533..10670
Mr. Peterson took the plaintiffs ' and other investors ' money to a meeting of the bank 's directors .	O	10671..10769
Wearing a business suit and western-style hat and boots , he opened up his briefcase and dumped $ 1 million in cash on a table in front of the directors , says Myron Diebel , the bank 's president .	O	10772..10964
" He said he wanted to show the color of his money , " recalls Mr. Diebel .	O	10965..11036
Bank officials , however , showed him the door , and the sale never came off .	O	11039..11113
According to the suit , Mr. Peterson has yet to return the plaintiffs ' investment .	B-comparison	11114..11195
They want it back .	I-comparison	11196..11214
Mr. Peterson declines to comment on specific allegations in the two suits , saying he prefers to save such responses for court .	B-comparison	11215..11341
But he does say that all of his activities have been " entirely proper . "	I-comparison	11342..11413
On the suit by the limited partners , he says he is considering a defamation suit against the plaintiffs .	O	11416..11520
The suit , he adds , " is almost in the nature of a vendetta by a handful of disgruntled people . "	O	11521..11615
Rearding the suit over the bank bid , Mr. Peterson says it is filled with " inflammatory language and half truths . "	B-comparison	11618..11731
He declines to go into specifics .	I-comparison	11732..11765
Mr. Peterson says the suits against him are less a measure of his work than they are a " sign of the times " in which people generally are more prone to sue .	O	11768..11923
" I do n't know anybody in the industry who has n't experienced litigation , " he says .	O	11924..12006
Mr. Peterson also says he does n't consider himself a financial planner anymore .	O	12009..12088
He now calls himself an " investment banker . "	O	12089..12133
In many scams or alleged scams involving planners , it 's plain that only a modicum of common sense on the part of the investors would have kept them out of harm 's way .	B-contingency	12136..12302
Using it , would n't a proessional hesitate to pay tens of thousands of dollars just for a chance to invest witha planner ?	I-contingency	12303..12423
Other cases go to show that an old saw still applies : If it sounds too good to be true , it probably is .	B-expansion	12426..12529
Certificates of deposit do n't pay 23 % a year , for example , but that did n't give pause to clients of one Alabama planner .	B-contingency	12530..12650
Now they 're losers and he 's in jail in Mobile County .	B-entrel	12651..12704
CDs yielding 40 % are even more implausible -- especially when the issuing " bank " in the Marshall Islands is merely a mail drop watched over by a local gas-station operator -- but investors fell for that one too .	B-expansion	12705..12916
And the Colorado planner who promised to make some of his clients millionaires on investments of as litle as $ 100 ?	I-expansion	12919..13033
Never mind .	B-contingency	13034..13045
You already know the answer .	I-contingency	13046..13074
Mr. Emshwiller is a staff reporter in The Wall Street Journal 's Los Angeles bureau .	O	13077..13160
