The House Aviation Subcommittee approved a bill that would give the transportation secretary authority to review and approve leveraged buy-outs of major U.S. airlines.	O	9..176
The collapsed plan to acquire UAL Corp., parent of United Airlines, spurred quick action on the legislation, introduced Wednesday and approved by the subcommittee on a voice vote yesterday.	B-entrel	179..368
The bill is expected to be taken up by the Public Works and Transportation Committee tomorrow, and a floor vote by next week will be urged.	I-entrel	369..508
The measure drew criticism from the Bush administration and a parting shot from financier Donald Trump, who yesterday withdrew his takeover bid for AMR Corp., the parent of American Airlines.	O	511..702
In a letter to subcommittee Chairman James Oberstar (D., Minn.), Mr. Trump criticized the bill as an explicit effort to thwart his bid for AMR, and said it contributed to the collapse of the deal.	B-conjunction	703..899
Elaine Chao, deputy transportation secretary, also sent a letter to express the administration's opposition to the bill "in its present form."	I-conjunction	902..1044
Rep. Oberstar brushed off Mr. Trump's allegations as an "excuse for his own deal failing.	B-conjunction	1047..1137
He also said the fact that the other letter hadn't come from Transportation Secretary Samuel Skinner indicated there is "wiggle room" in the administration's position.	I-conjunction	1138..1305
Mr. Oberstar and other committee members repeatedly stressed that the legislation wasn't a response to any particular market situation.	B-contrast	1308..1443
But they cited the UAL and AMR examples as reasons to move quickly to enact this legislation.	I-contrast	1444..1537
Aides both in the House and Senate said the withdrawal of the Trump bid for AMR isn't likely to deflate efforts to push the legislation.	O	1540..1676
"It's still on the fast track and we still want to do it," said one Senate aide.	O	1677..1757
The bill is aimed at addressing the concern that an airline might sacrifice costly safety measures to pay off the debt incurred in a leveraged buy-out.	O	1760..1911
Currently, the transportation secretary doesn't have clearly established authority to block mergers, but can take the drastic step of revoking the operating certificate of any carrier the official considers unfit.	B-entrel	1914..2127
Supporters of the legislation view the bill as an effort to add stability and certainty to the airline-acquisition process, and to preserve the safety and fitness of the industry.	I-entrel	2128..2307
In general, the bill would give the Transportation Department a 30-day review period before 15% or more of the voting stock of a major U.S. air carrier could be acquired.	B-conjunction	2310..2480
It also would require the acquiring party to notify the transportation secretary and to provide all information relevant to determining the intent of the acquisition.	I-conjunction	2481..2647
The bill would allow the secretary to reject a buy-out if sufficient information hasn't been provided, or if the buy-out is likely to weaken the carrier financially, result in a substantial reduction in size of the airline through disposal of assets, or give control to a foreign interest.	B-conjunction	2650..2939
If more information is needed, the secretary would have authority to extend the review period 20 days.	I-conjunction	2940..3042
All the witnesses, both congressmen and industry experts, expressed support for the bill in order to prevent profiteers from cashing in on airline profits at the expense of safe, cost-effective service.	B-contrast	3045..3247
But several committee members disapproved, some backing Mr. Trump's claim that the threat of regulation caused the failure of the UAL deal and the stock-market plunge.	I-contrast	3248..3415
One of the major concerns expressed by the dissenters was that large airlines would be prohibited from divesting themselves of smaller entities and producing independent spin-off companies.	O	3418..3607
