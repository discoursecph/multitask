(During its centennial year, The Wall Street Journal will report events of the past century that stand as milestones of American business history.)	O	9..156
LUTHER BURBANK CROSS-BRED PLANTS to produce the billion-dollar Idaho potato.	B-entrel	159..235
Bioengineers set out to duplicate that feat -- scientifically and commercially -- with new life forms.	I-entrel	236..338
In 1953, James Watson and his colleagues unlocked the double helix of DNA (deoxyribonucleic acid), the genetic key to heredity.	B-asynchronous	341..468
Twenty years later, two California academics, Stanley Cohen and Herbert Boyer, made "recombinant" DNA, transplanting a toad's gene into bacteria, which then reproduced toad genes.	I-asynchronous	469..648
When Boyer met Robert Swanson, an M.I.T.-trained chemist-turned-entrepreneur in 1976, they saw dollar signs.	B-cause	651..759
With $500 apiece and an injection of outside capital, they formed Genentech Inc.	B-synchrony	760..840
Commercial gene-splicing was born.	I-synchrony	841..875
Genentech's first product, a brain protein called somatostatin, proved its technology.	B-asynchronous	878..964
The next to be cloned, human insulin, had market potential and Genentech licensed it to Eli Lilly, which produced 80% of the insulin used by 1.5 million U.S. diabetics.	I-asynchronous	965..1133
Their laboratory credentials established, Boyer and Swanson headed for Wall Street in 1980.	O	1136..1227
At the time, Genentech had only one profitable year behind it (a modest $116,000 on revenue of $2.6 million in 1979) and no product of its own on the market.	B-concession	1228..1385
Nonetheless, the $36 million issue they floated in 1980 opened at $35 and leaped to $89 within 20 minutes.	I-concession	1386..1492
The trip from the test tube was not without snags.	B-instantiation	1495..1545
Boyer and Cohen, for instance, both still university researchers, had to be talked into applying for a patent on their gene-splicing technique -- and then the Patent Office refused to grant it.	I-instantiation	1546..1739
That judgment, in turn, was reversed by the U.S. Supreme Court, leaving Cohen and Boyer holding the first patents for making recombinant DNA (now assigned to their schools).	O	1740..1913
Gene-splicing now is an integral part of the drug business.	B-instantiation	1916..1975
Genentech's 1988 sales were $335 million, both from licensing and its own products.	I-instantiation	1976..2059
