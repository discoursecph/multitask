When Nucor Corp. begins shipping steel from the world 's first thin-slab plant this month , it will begin testing the competitive mettle of its giant competitors .	root	9..169
The new technology , which creates a very thin piece of steel , radically reduces the costs of making flat-rolled sheets .	norel	172..291
An ebullient Kenneth Iverson , Nucor 's chairman , says the company 's plant eventually will make a ton of steel in 1.5 man hours , compared with four to six man hours at a conventional mill .	restatement	292..478
" We 've had the Russians and Chinese , and people from India visiting us , " Mr. Iverson beams .	norel	481..572
" Everyone in the world is watching us very closely . "	restatement	573..625
Especially his neighbors , the major U.S. steelmakers .	norel	628..681
Already , USX Corp. and Armco Inc. are studying Nucor 's technology to see if they can adopt it .	restatement	682..776
Says the chief executive officer of a major Midwest steel company : " It 's damn worrisome . "	restatement	777..866
The once-staid steel industry is about to be turned topsy-turvy by a 1990s technology revolution .	norel	869..966
New , efficient and sophisticated processes make it easier for smaller , less cash-rich companies to make steel at a fraction of what Big Steel paid decades ago .	cause	967..1126
It also enables minimills finally to get a toehold in the flat-rolled steel market -- the major steelmakers ' largest , most prized , and until now , untouchable , market .	conjunction	1127..1293
But such thin-slab technology is only the beginning .	concession	1296..1348
Eager engineers espouse direct-steelmaking and direct casting , which by the end of the 1990s will enable production without coke ovens and blast furnaces .	cause	1349..1503
Those massive structures , while posing cost and environmental headaches , effectively locked out all but deep-pocketed giants from steelmaking .	asynchronous	1504..1646
" There 's a revolution ahead of us that will ultimately change the way we market and distribute steel , " says William Dennis , vice president , manufacturing and technology , for the American Iron Ore and Steel Institute .	norel	1649..1865
It is n't that major steelmakers have blithely ignored high technology .	norel	1868..1938
In fact , they 've spent billions of dollars to boost the percentage of continously cast steel to 60.9 % in 1988 , from 39.6 % five years before .	norel	1939..2079
Moreover , their balance sheets are rich with diversity , their old plants shuttered , and work forces lean .	conjunction	2080..2185
But that wo n't suffice .	concession	2188..2211
" It 's no longer enough to beat the guy down the street .	cause	2212..2267
You have to beat everyone around the world , " says Mr. Dennis .	alternative	2268..2329
He wants to see steelmakers more involved in computers and artificial intelligence .	cause	2330..2413
The problem : They 're saddled with huge plants that require costly maintenance .	norel	2416..2494
And try plying new dollars free in a market that is softening , hurt by a strong dollar and concerned about overcapacity -- the industry 's Darth Vadar .	conjunction	2495..2645
" The technology revolution is going to be very threatening to established producers , " says Peter Marcus , an analyst with PaineWebber Inc .	norel	2648..2785
" They 've got too much invested in the old stuff and they ca n't get their workers to be flexible . "	cause	2786..2883
No one expects minimills to eclipse major integrated steelmakers , who remain the undisputed kings of highest-quality steel used for autos and refrigerators .	norel	2886..3042
Nucor 's plant in Crawfordsville , Ind. , ultimately will produce only one million tons annually , a drop in the 40-million-ton-a-year flat-rolled steel bucket	cause	3043..3198
and it will be years before such plants can compete in the high-profit market .	conjunction	3199..3278
Still , flat-rolled is the steel industry 's bread and butter , representing about half of the 80 million tons of steel expected to be shipped this year .	contrast	3279..3429
Moreover , the process is n't without its headaches .	norel	3432..3482
Because all operations are connected , one equipment failure forces a complete plant shutdown .	instantiation	3483..3576
On some days , the Nucor plant does n't produce anything .	cause	3577..3632
" At this point , the minimill capacity wo n't make a great dent in the integrated market , but it does challenge them to develop new markets , " says James McCall , vice president , materials , at Battelle , a technology and management-research giant based in Columbus , Ohio .	norel	3635..3901
Indeed , with demand for steel not growing fast enough to absorb capacity , steelmakers will have to change the way they do business .	conjunction	3904..4035
In the past , says Armco 's chief economist John Corey , steelmakers made a product and set it out on the loading dock .	asynchronous	4036..4152
" We said : ' We 've got a product : if you want it , you can buy it , ' " he says , adding : " Now we 're figuring out what people need , and are going back to make it . "	restatement	4153..4310
Armco 's sales representatives visit the General Motors Corp. 's Fairfax assembly plant in Kansas City , Mo. , two or three days a week .	norel	4313..4446
When they determined that GM needed parts more quickly , Armco convinced a steel service center to build a processing plant nearby so shipments could be delivered within 15 minutes .	entrel	4447..4627
Cementing such relationships with major clients -- car and appliance makers -- is a means of survival	norel	4630..4731
especially when those key clients are relying on a smaller pool of producers and flirting with plastic and aluminum makers .	synchrony	4732..4856
For example , when Detroit began talking about plastic-bodied cars , the American Iron and Steel Institute began a major lobbying effort to show auto makers how they could use steel more efficiently by simply redesigning how a car door is assembled .	instantiation	4859..5106
But steelmakers must also find new markets .	norel	5109..5152
After letting aluminum-makers take the recycling lead , a group of the nation 's largest steelmakers started a recycling institute to promote steel cans to an environmentally conscious nation .	instantiation	5153..5343
Battelle 's Mr. McCall thinks steelmakers should concentrate more on construction .	norel	5346..5427
Weirton Steel Corp. , Weirton , W. Va. , for example , is touting to homeowners fashionable steel doors , with leaded glass inserts , as a secure and energy-efficient alternative to wooden or aluminum ones .	instantiation	5428..5628
Other steelmakers envision steel roofs covering suburbia .	synchrony	5629..5686
Still others are looking at overseas markets .	norel	5689..5734
USX is funneling drilling pipe to steel-hungry Soviet Union .	instantiation	5735..5795
This year , the nation 's largest steelmaker reactivated its overseas sales operation .	restatement	5796..5880
Producers also are trying to differentiate by concentrating on higher-profit output , such as coated and electrogalvanized products , which remain beyond the reach of minimills .	norel	5883..6058
Almost all capital-improvement programs announced by major steelmakers within the past year involve building electrogalvanizing lines , used to produce steel for such products as household appliances and car doors .	restatement	6059..6272
But unfortunately , that segment is much smaller than the bread-and-butter flat-rolled steel .	contrast	6275..6367
" It 's like everyone climbing out of the QE II and getting into a lifeboat , " says John Jacobson , an analyst with AUS Consultants .	restatement	6368..6496
" After a while , someone has to go over the side . "	cause	6497..6546
Although he does n't expect any bankruptcies , he does see more plants being sold or closed .	cause	6547..6637
Robert Crandall , with the Brookings Institute , agrees .	norel	6640..6694
" Unless there is an enormous rate of economic growth or a further drop in the dollar , it 's unlikely that consumption of U.S. produced steel will grow sufficiently to offset the growth of minimills . "	restatement	6695..6893
Not to mention the incursion of imports .	norel	6896..6936
Japanese and European steelmakers , which have led the recent technology developments , are anxiously awaiting the lifting of trade restraints in 1992 .	pragmatic cause	6937..7086
Moreover , the U.S. can expect more competition from low-cost producing Pacific Rim and Latin American countries .	conjunction	7087..7199
A Taiwanese steelmaker recently announced plans to build a Nucor-like plant .	instantiation	7200..7276
" People think of the steel business as an old and mundane smokestack business , " says Mr. Iverson .	norel	7279..7376
" They 're dead wrong . "	contrast	7377..7398
* USX , LTV , Bethlehem , Inland , Armco , National Steel	norel	7401..7452
** Projected	norel	7455..7466
