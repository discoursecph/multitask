The bond market ,	(SN-Summary-Evaluation-Topic(NN-Textual-organization(NS-Elaboration
which sometimes thrives on bad news ,	NS-Elaboration)
cheered yesterday 's stock market sell-off and perceptions	(NS-Elaboration
that the economy is growing weaker .	NS-Elaboration))
Early in the day , bonds rose modestly on economists ' forecasts	(NN-Summary-Evaluation-Topic(NN-Temporal(NS-Elaboration(NS-Summary-Evaluation-Topic(NS-Elaboration
that this week 's slate of economic data will portray an economy headed for trouble .	NS-Elaboration)
Such news is good for bonds	(SN-Summary-Evaluation-Topic
because economic weakness sometimes causes the Federal Reserve to lower interest rates in an effort	(NS-Elaboration
to stimulate the economy	(NN-Joint
and stave off a recession .	NN-Joint))))
For example , today the Department of Commerce is scheduled to release the September durable goods report .	(NS-Elaboration
The consensus forecast of 14 economists	(NS-Temporal(NN-Textual-organization(NS-Elaboration
surveyed by Dow Jones Capital Markets Report	NS-Elaboration)
is for a 1.2 % drop in September orders .	NN-Textual-organization)
That would follow a 3.9 % advance in August .	NS-Temporal)))
Bonds received a bigger boost later in the day	(NS-Elaboration(NS-Elaboration(NS-Temporal
when stock prices moved broadly lower .	NS-Temporal)
The Dow Jones Industrial Average fell 26.23 points to 2662.91 .	NS-Elaboration)
`` Bond investors have been watching stocks closely , ''	(NN-Joint(NS-Attribution
said Joel Marver , chief fixed-income analyst at Technical Data Global Markets Group .	NS-Attribution)
`` When you get a big sell-off in equities ,	(NS-Attribution(SN-Temporal
money starts to shift into bonds , ''	(NS-Elaboration
which are considered safer ,	NS-Elaboration))
he said .	NS-Attribution))))
The Treasury 's benchmark 30-year bond ended about 1/2 point higher , or up about $ 5 for each $ 1,000 face amount ,	(NN-Summary-Evaluation-Topic(NN-Joint(NN-Comparison
while the yield slid to 7.93 % from 7.98 % Friday .	NN-Comparison)
Municipals ended mixed ,	(NN-Joint(NN-Joint
while mortgage-backed and investment-grade corporate bonds rose .	NN-Joint)
Prices of high-yield , high-risk corporate securities ended unchanged .	(NN-Joint
In more evidence of the growing division between `` good '' and `` bad '' junk bonds , a $ 150 million issue by Imo Industries Inc. was snapped up by investors	(NN-Joint(NN-Comparison
while underwriters for Beatrice Co. 's $ 350 million issue are considering restructuring the deal	(NS-Cause
to attract buyers .	NS-Cause))
In the Treasury market , analysts expect bond prices to trade in narrow ranges this week	(NN-Joint(NS-Summary-Evaluation-Topic(NS-Temporal
as the market takes in positive and negative news .	NS-Temporal)
`` On the negative side , the market will be affected by constant supply in all sectors of the market , ''	(NN-Comparison(NS-Attribution
said William M. Brachfeld , economist at Daiwa Securities America Inc .	NS-Attribution)
`` On the other hand , we have economic news	(NN-Joint(NS-Elaboration
that is { expected to be } relatively positive for the bond market .	NS-Elaboration)
We will go back and forth with a tilt toward slightly lower yields , ''	(NS-Attribution
he said .	NS-Attribution))))
Today , the Treasury will sell $ 10 billion of new two-year notes .	(NN-Joint
Tomorrow , Resolution Funding Corp. , a division of a new government agency	(NN-Joint(NS-Summary-Evaluation-Topic(NS-Elaboration(NN-Textual-organization(NS-Elaboration
created	(NS-Cause
to bail out the nation 's troubled thrifts ,	NS-Cause))
will hold its first bond auction	(NS-Elaboration
at which it will sell $ 4.5 billion of 30-year bonds .	NS-Elaboration))
So far , money managers and other bond buyers have n't shown much interest in the Refcorp bonds .	NS-Elaboration)
Analysts have mixed views about the two-year note auction .	(NS-Elaboration
While some say	(NN-Comparison(SN-Attribution
the auction should proceed smoothly ,	SN-Attribution)
others contend	(SN-Attribution
that yesterday 's sale of $ 2.58 billion of asset-backed securities by Ford Motor Credit Corp. may have siphoned some potential institutional buyers from the government 's note sale .	SN-Attribution))))
The division of auto maker Ford Motor Co. made its debut in the asset-backed securities market with the second-largest issue in the market 's four-year history .	(NS-Comparison(NS-Elaboration(NS-Elaboration
The company offered securities	(NS-Elaboration(NS-Elaboration
backed by automobile loans through an underwriting group	(NS-Elaboration
headed by First Boston Corp .	NS-Elaboration))
The issue yields 8.90 %	(NS-Elaboration(NN-Joint
and carries a guarantee	(NS-Elaboration
covering 9 % of the deal from the company .	NS-Elaboration))
First Boston sweetened the terms from the original yield estimate in an apparent effort	(NS-Elaboration
to place the huge offering .	NS-Elaboration))))
The issue was offered at a yield nearly one percentage point above the yield on two-year Treasurys .	NS-Elaboration)
The only asset-backed deal larger than Ford 's was a $ 4 billion offering by General Motors Acceptance Corp. in 1986 .	NS-Comparison))))))))
Treasury Securities	(NN-Textual-organization(NN-Textual-organization
Treasury bonds were 1/8 to 1/2 point higher yesterday in light trading .	(NS-Elaboration
The benchmark 30-year bond ended at a price of 102 3/32 ,	(NS-Elaboration(NS-Elaboration(NN-Joint(NN-Comparison
compared with 101 17/32 Friday .	NN-Comparison)
The latest 10-year notes were quoted late at 100 17/32	(NN-Joint(NN-Joint(NN-Comparison(NS-Cause
to yield 7.90 % ,	NS-Cause)
compared with 100 3/32	(NS-Cause
to yield 7.97 % .	NS-Cause))
The latest two-year notes were quoted late at 100 28/32	(NS-Cause
to yield 7.84 % .	NS-Cause))
Short-term rates rose yesterday at the government 's weekly Treasury bill auction ,	(NN-Joint(NN-Comparison
compared with the previous bill sale .	NN-Comparison)
The Treasury sold $ 7.81 billion of three-month bills with an average discount rate of 7.52 % , the highest since the average of 7.63 % at the auction on Oct. 10 .	(NN-Joint
The $ 7.81 billion of six-month Treasury bills were sold with an average discount rate of 7.50 % , the highest since the average of 7.60 % at the Oct. 10 auction .	NN-Joint))))
The rates were up from last week 's auction ,	(NN-Comparison
when they were 7.37 % and 7.42 % , respectively .	NN-Comparison))
Here are auction details :	(NS-Elaboration
Rates are determined by the difference between the purchase price and face value .	(NN-Joint(NS-Summary-Evaluation-Topic
Thus , higher bidding narrows the investor 's return	(NN-Comparison
while lower bidding widens it .	NN-Comparison))
The percentage rates are calculated on a 360-day year ,	(NN-Joint(NN-Comparison
while the coupon-equivalent yield is based on a 365-day year .	NN-Comparison)
Both issues are dated Oct. 26 .	(NS-Elaboration
The 13-week bills mature Jan. 25 , 1990 ,	(NN-Comparison
and the 26-week bills mature April 26 , 1990 .	NN-Comparison))))))))
Corporate Issues	(NN-Textual-organization(NN-Textual-organization
Investment-grade corporates closed about 1/4 point higher in quiet trading .	(NN-Joint
In the junk bond market , Imo Industries ' issue of 12-year debentures ,	(NS-Elaboration(NS-Elaboration(NS-Elaboration(NN-Textual-organization(NS-Elaboration
considered to be one of the market 's high-quality credits ,	NS-Elaboration)
was priced at par to yield 12 % .	NN-Textual-organization)
Peter Karches , managing director at underwriter Morgan Stanley & Co. , said	(NS-Cause(SN-Attribution
the issue was oversubscribed .	SN-Attribution)
`` It 's a segmented market ,	(NS-Attribution(SN-Temporal
and	(NN-Textual-organization
if you have a good , strong credit ,	(SN-Cause
people have an appetite for it , ''	SN-Cause)))
he said .	NS-Attribution)))
Morgan Stanley is expected to price another junk bond deal , $ 350 million of senior subordinated debentures by Continental Cablevision Inc. , next Tuesday .	NS-Elaboration)
In light of the recent skittishness in the high-yield market , junk bond analysts and traders expect other high-yield deals to be sweetened or restructured	(NS-Temporal(NS-Elaboration(SN-Cause
before they are offered to investors .	SN-Cause)
In the case of Beatrice , Salomon Brothers Inc. is considering restructuring the reset mechanism on the $ 200 million portion of the offering .	NS-Elaboration)
Under the originally contemplated terms of the offering , the notes would have been reset annually at a fixed spread above Treasurys .	(NN-Comparison
Under the new plan	(NS-Elaboration(NN-Cause(NN-Textual-organization(NS-Elaboration
being considered ,	NS-Elaboration)
the notes would reset annually at a rate	NN-Textual-organization)
to maintain a market value of 101 .	NN-Cause)
Price talk calls for the reset notes to be priced at a yield of between 13 1/4 % and 13 1/2 % .	NS-Elaboration))))))
Mortgage-Backed Securities	(NN-Textual-organization(NN-Textual-organization
Activity in derivative markets was strong with four new real estate mortgage investment conduits announced and talk of several more deals in today 's session .	(NS-Elaboration(NS-Elaboration
The Federal National Mortgage Association offered $ 1.2 billion of Remic securities in three issues ,	(NS-Cause(NN-Joint
and the Federal Home Loan Mortgage Corp. offered a $ 250 million Remic	(NS-Elaboration
backed by 9 % 15-year securities .	NS-Elaboration))
Part of the reason for the heavy activity in derivative markets is that underwriters are repackaging mortgage securities	(NN-Joint(NS-Elaboration
being sold by thrifts .	NS-Elaboration)
Traders said	(NS-Temporal(SN-Attribution
thrifts have stepped up their mortgage securities sales	SN-Attribution)
as the bond market has risen in the past two weeks .	NS-Temporal))))
In the mortgage pass-through sector , active issues rose	(NN-Joint(NN-Comparison
but trailed gains in the Treasury market .	NN-Comparison)
Government National Mortgage Association 9 % securities for November delivery were quoted late yesterday at 98 10/32 , up 10/32 ;	(NS-Elaboration(NN-Joint
and Freddie Mac 9 % securities were at 97 1/2 , up 1/4 .	NN-Joint)
The Ginnie Mae 9 % issue was yielding 8.36 % to a 12-year average life assumption ,	(NN-Joint
as the spread above the Treasury 10-year note widened slightly to 1.46 percentage points .	NN-Joint)))))
Municipals	(NN-Textual-organization(NN-Summary-Evaluation-Topic
A $ 575 million San Antonio , Texas , electric and gas system revenue bond issue dominated the new issue sector .	(NN-Summary-Evaluation-Topic(NS-Elaboration
The refunding issue ,	(SN-Comparison(NN-Textual-organization(NS-Elaboration
which had been in the wings for two months ,	NS-Elaboration)
was one of the chief offerings	(NS-Elaboration
overhanging the market	(NN-Joint
and limiting price appreciation .	NN-Joint)))
But alleviating that overhang failed to stimulate much activity in the secondary market ,	(NS-Cause(NS-Elaboration
where prices were off 1/8 to up 3/8 point .	NS-Elaboration)
An official with lead underwriter First Boston said	(NS-Elaboration(SN-Attribution
orders for the San Antonio bonds were `` on the slow side . ''	SN-Attribution)
He attributed that to the issue 's aggressive pricing and large size , as well as the general lethargy in the municipal marketplace .	(NN-Joint
In addition ,	(NN-Textual-organization(NS-Attribution
he noted ,	NS-Attribution)
the issue would normally be the type	(SN-Comparison(NS-Elaboration
purchased by property and casualty insurers ,	NS-Elaboration)
but recent disasters , such as Hurricane Hugo and the Northern California earthquake , have stretched insurers ' resources	(NN-Joint
and damped their demand for bonds .	NN-Joint))))))))
A $ 137.6 million Maryland Stadium Authority sports facilities lease revenue bond issue appeared to be off to a good start .	(NN-Summary-Evaluation-Topic(NS-Elaboration
The issue was oversubscribed	(NS-Attribution(NN-Joint
and `` doing very well , ''	NN-Joint)
according to an official with lead underwriter Morgan Stanley .	NS-Attribution))
Activity quieted in the New York City bond market ,	(NS-Temporal
where heavy investor selling last week drove yields on the issuer 's full faith and credit backed bonds up as much as 0.50 percentage point .	NS-Temporal))))
Foreign Bonds	(NN-Textual-organization
Japanese government bonds ended lower	(NN-Summary-Evaluation-Topic(NS-Elaboration(NS-Temporal
after the dollar rose modestly against the yen .	(NS-Cause
The turnaround in the dollar fueled bearish sentiment about Japan 's bond market .	NS-Cause))
The benchmark No. 111 4.6 % bond due 1998 ended on brokers ' screens at a price of 95.39 , off 0.28 .	(NS-Elaboration
The yield rose to 5.38 % .	NS-Elaboration))
West German bond prices ended lower after a day of aimless trading .	(NN-Summary-Evaluation-Topic(NS-Elaboration
The benchmark 7 % bond due October 1999 fell 0.20 point to 99.80	(NN-Joint(NS-Cause
to yield 7.03 % ,	NS-Cause)
while the 6 3/4 % notes due July 1994 fell 0.10 to 97.65	(NS-Cause
to yield 7.34 % .	NS-Cause)))
British government bonds ended slightly higher in quiet trading	(NS-Elaboration(NN-Temporal
as investors looked ahead to today 's British trade report .	NN-Temporal)
The benchmark 11 3/4 % Treasury bond due 2003/2007 rose 1/8 to 111 21/32	(NN-Joint(NS-Cause
to yield 10.11 % ,	NS-Cause)
while the 12 % issue of 1995 rose 3/32 to 103 23/32	(NS-Cause
to yield 11.01 % .	NS-Cause)))))))))))))
