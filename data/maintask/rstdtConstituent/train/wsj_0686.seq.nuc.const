Congress sent to President Bush an $ 8.5 billion military construction bill	(NS-Elaboration(NS-Background(NS-Elaboration
that cuts spending for new installations by 16 %	(NS-Background
while revamping the Pentagon budget	(NS-Enablement
to move more than $ 450 million from foreign bases to home-state projects .	NS-Enablement)))
The fiscal 1990 measure builds on a pattern	(NS-Elaboration(NN-Joint(NS-Elaboration
set earlier this year by House and Senate defense authorizing committees ,	NS-Elaboration)
and	(NN-Same-unit
-- at a time of retrenchment for the military and concern about the U.S. 's standing in the world economy --	(SN-Background
overseas spending is most vulnerable .	SN-Background)))
Total Pentagon requests for installations in West Germany , Japan , South Korea , the United Kingdom and the Philippines , for example , are cut by almost two-thirds ,	(NS-Elaboration(NN-Cause(NS-Elaboration
while lawmakers added to the military budget for construction in all but a dozen states at home .	NS-Elaboration)
The result is that instead of the Pentagon 's proposed split of 60-40 between domestic and foreign bases , the reduced funding is distributed by a ratio of approximately 70-30 .	NN-Cause)
The extra margin for bases in the U.S. enhances the power of the appropriations committees ;	(NN-Joint
meanwhile , lawmakers used their positions	(NS-Elaboration(NS-Enablement
to garner as much	(NN-Comparison
as six times what the Pentagon had requested for their individual states .	NN-Comparison))
House Appropriations Committee Chairman Jamie Whitten	(NN-Joint(NN-Same-unit(NS-Elaboration
( D. , Miss . )	NS-Elaboration)
helped secure $ 49.7 million for his state , or more than double the Pentagon 's budget .	NN-Same-unit)
West Virginia , home of Senate Appropriations Committee Chairman Robert Byrd , would receive $ 21.5 million	(NN-Joint(NS-Elaboration
-- four times the military 's request .	NS-Elaboration)
Tennessee and North Carolina , home states of the two Democratic chairmen of the House and Senate military construction subcommittees , receive $ 243.2 million , or 25 % above the Pentagon 's request .	(NN-Joint
Though spending for Iowa and Oregon was far less ,	(SN-Contrast
their increases above Pentagon requests	(NS-Cause(NN-Same-unit(NS-Elaboration
-- 640 % and 430 % , respectively --	NS-Elaboration)
were much greater	NN-Same-unit)
because of the influence of Republicans at critical junctures .	NS-Cause))))))))))
The swift passage of the bill ,	(NS-Elaboration(NN-Same-unit(NS-Elaboration
which cleared the Senate and House on simple voice votes last week ,	NS-Elaboration)
contrasts with the problems	(NS-Elaboration
still facing a more cumbersome $ 66.8 billion measure	(NS-Elaboration
funding housing , environmental , space and veterans programs .	NS-Elaboration)))
By an 84-6 margin , the Senate approved the bulk of the spending Friday ,	(NS-Elaboration(SN-Contrast
but the bill was then sent back to the House	(NS-Enablement
to resolve the question	(NS-Elaboration
of how to address budget limits on credit allocations for the Federal Housing Administration .	NS-Elaboration)))
The House Democratic leadership could seek to waive these restrictions ,	(NS-Elaboration(NS-Explanation(NN-Contrast
but the underlying bill is already under attack for excesses elsewhere .	NN-Contrast)
Appropriations committees have used an assortment of devices	(NS-Elaboration(NN-Cause(NS-Elaboration
to disguise as much as $ 1 billion in spending ,	NS-Elaboration)
and	(NN-Same-unit
as critics have awakened to these devices ,	(SN-Background
the bill can seem like a wounded caribou	(NS-Elaboration
trying to make it past ice and wolves	(NS-Enablement
to reach safer winter grazing .	NS-Enablement)))))
Much of the excess spending will be pushed into fiscal 1991 ,	(NS-Elaboration(NN-Joint
and in some cases is temporarily parked in slow-spending accounts in anticipation	(NS-Elaboration
of being transferred to faster-spending areas	(NS-Background
after the budget scorekeeping is completed .	NS-Background)))
For example , a House-Senate conference ostensibly increased the National Aeronautics and Space Administration budget for construction of facilities to nearly $ 592 million , or more than $ 200 million above what either chamber had previously approved .	(NS-Elaboration
Part of the increase would provide $ 90 million toward ensuring construction of a costly solid rocket-motor facility in Mr. Whitten 's Mississippi .	(NN-Contrast
But as much as $ 177 million , or nearly 30 % of the account , is marked for potential transfers to research , management and flight accounts	(NS-Elaboration
that are spent out at a faster clip .	NS-Elaboration))))))
The bill 's managers face criticism , too , for the unusual number of conditions	(NS-Elaboration(NS-Elaboration(NS-Elaboration
openly imposed on where funds will be spent .	NS-Elaboration)
Conservatives ,	(NS-Elaboration(NN-Same-unit
embarrassed by Republican influence-peddling scandals at the Department of Housing and Urban Development ,	(SN-Background
have used the issue in an effort to shift blame onto a Democratic-controlled Congress .	SN-Background))
HUD Secretary Jack Kemp backed an unsuccessful effort	(NS-Contrast(NN-Same-unit(NS-Elaboration
to strike such language	NS-Elaboration)
last week ,	NN-Same-unit)
but received little support from the White House budget office ,	(NS-Elaboration
which wants to protect space-station funding in the bill	(NN-Joint
and has tended to turn its eyes from pork-barrel amendments .	NN-Joint)))))
Within discretionary funds for community development grants , more than $ 3.7 million is allocated to six projects in Michigan , home state of a subcommittee chairman , Rep. Bob Traxler .	(NN-Joint
House Speaker Thomas Foley won $ 510,000 for a project in his district in Washington state ,	(NN-Joint
and $ 1.3 million ,	(NS-Elaboration(NN-Same-unit(NS-Elaboration
earmarked by Sen. Daniel Inouye ,	NS-Elaboration)
amounts to a business subsidy under the title `` Hawaiian sugar mills job retention . ''	NN-Same-unit)
The powerful Democrat had first wanted to add language	(NS-Elaboration(NS-Elaboration
relaxing environmental restrictions on two mills on the Hamakua coast	(NS-Elaboration
that are threatening to close .	NS-Elaboration))
When this plan met resistance ,	(NS-Attribution(SN-Explanation
it was agreed instead to take money from HUD	(NS-Enablement
to subsidize needed improvements in two settling ponds for the mills ,	(NS-Elaboration
which employ an estimated 1,500 workers ,	NS-Elaboration)))
according to Mr. Inouye 's office .	NS-Attribution))))))))))
