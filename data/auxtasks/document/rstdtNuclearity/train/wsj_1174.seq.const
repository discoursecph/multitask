If bluebloods wo n't pay high prices for racehorses anymore ,	(NS(NS(NN(SN
who will ?	SN)
Breeders are betting on the common folk .	NN)
The Thoroughbred Owners and Breeders Association , a Lexington , Ky.-based trade group , has launched `` seminars '' for `` potential investors '' at race tracks around the country .	(NS
The group ,	(NN(NS
which has held half a dozen seminars so far ,	NS)
also is considering promotional videos and perhaps a pitch to Wall Street investment bankers .	NN)))
`` People in this business have been insulated , ''	(NS(NS(SN(NS
says Josh Pons , a horse breeder from Bel Air , Md .	NS)
`` But the real future of this game is in a number of people	(NS
owning a few horses . ''	NS))
At the Laurel race track , the breeders are romancing people like Tim Hulings , a beer packaging plant worker .	(NS(NS
Right now , Mr. Hulings is waving his racing program ,	(NS
cheering for Karnak on the Nile , a sleek thoroughbred	(NS
galloping down the home stretch .	NS)))
Mr. Hulings gloats	(NS(SN
that he sold all his stocks	(SN(NS
a week before the market plummeted 190 points on Oct. 13 ,	NS)
and he is using the money	(NS
to help buy a 45-acre horse farm .	NS)))
`` Just imagine how exciting that would be	(NS(NS
if that 's your horse , ''	NS)
he says .	NS))))
But experts caution	(NS(NS(NS(SN
that this is n't a game for anyone with a weak stomach or wallet .	SN)
`` It 's a big-risk business , ''	(NS
warns Charles C. Mihalek , a Lexington attorney and former Kentucky state securities commissioner .	NS))
`` You have to go into it	(NS(NS
firmly believing	(SN
that it 's the kind of investment	(NS
where you can lose everything . ''	NS)))
And many have done just that .	(NS(NS
Consider Spendthrift Farm , a prominent Lexington horse farm	(NS(NS
that went public in 1983	(SN
but hit hard times	(SN
and filed for bankruptcy-court protection last year .	SN)))
A group of investors recently bought the remaining assets of Spendthrift ,	(NS
hoping to rebuild it .	NS)))
Other investors have lost millions in partnerships	(NS(NS
that bought thoroughbred racehorses or stallion breeding rights .	NS)
One big problem has been the thoroughbred racehorse market .	(NS
From 1974 to 1984 , prices for the best yearlings at the summer sales rose 918 % to an average of $ 544,681 .	(NN
Since then , prices have slumped , to an average of $ 395,374 this summer .	(NS
But that 's for the best horses ,	(NS
with most selling for much less	(NS
-- as little as $ 100 for some pedestrian thoroughbreds .	NS)))))))))
Even while they move outside their traditional tony circle ,	(NS(NS(NS(SN(SN(NS(SN
racehorse owners still try to capitalize on the elan of the sport .	SN)
Glossy brochures	(NS(NN(NS
circulated at racetracks	NS)
gush about the limelight of the winner 's circle and high-society schmoozing .	NN)
One handout promises :	(SN
`` Pedigrees , parties , post times , parimutuels and pageantry . ''	SN)))
`` It 's just a matter of marketing and promoting ourselves , ''	(NS
says Headley Bell , a fifth-generation horse breeder from Lexington .	NS))
Maybe it 's not that simple .	SN)
For starters , racehorse buyers have to remember the basic problem of such ventures :	(NS(NS
These beasts do n't come with warranties .	(NS
And for every champion , there are plenty of nags .	NS))
Katherine Voss , a veteran trainer at the Laurel , Md. , track , offers neophytes a sobering tour of a horse barn ,	(NS(NS(NS
noting	(SN
that only three of about a dozen horses have won sizable purses .	SN))
One brown two-year-old filly was wheezing from a cold ,	(SN(NN
while another had splints on its legs ,	NN)
keeping both animals from the racetrack .	SN))
`` You can see the highs and lows of the business all under one roof , ''	(NS(NS
she tells the group .	NS)
`` There are n't too many winners . ''	NS))))
Perhaps the biggest hurdle	(NS(NN(NS
owners face	NS)
is convincing newcomers	(SN
that this is a reputable business .	SN))
Some badly managed partnerships have burned investors ,	(SN(SN(NS
sometimes after they received advice from industry `` consultants . ''	NS)
So owners have developed a `` code of ethics , ''	(NS
outlining rules for consultants and agents , and disclosure of fees and any conflicts of interest .	NS))
But some are skeptical of the code 's effectiveness .	(NS
`` The industry is based on individual honesty , ''	(NS
says Cap Hershey , a Lexington horse farmer and one of the investors	(NS
who bought Spendthrift .	NS))))))
Despite the drop in prices for thoroughbreds , owning one still is n't cheap .	(NS(NS
At the low end , investors can spend $ 15,000 or more	(NN(NS
to own a racehorse in partnership with others .	NS)
At a yearling sale , a buyer can go solo	(SN(NN
and get a horse for a few thousand dollars .	NN)
But that means paying the horse 's maintenance ;	(NS
on average , it costs $ 25,000 a year	(NS
to raise a horse .	NS)))))
For those	(SN(NS
looking for something between a minority stake and total ownership ,	NS)
the owners ' group is considering a special sale	(NS
where established horse breeders would sell a 50 % stake in horses to newcomers .	NS)))))))
