GTE Corp. and MCI Communications Corp. reported strong earnings gains to record levels for the third quarter .	B-comparison	9..118
Southwestern Bell Corp. and Cincinnati Bell posted slight declines .	I-comparison	119..186
GTE Corp .	O	189..198
GTE said net income rose 18 % , aided by higher long-distance calling volumes and an increase in telephone lines in service .	B-entrel	201..323
Pretax operating profit from telephone operations rose 8.2 % but profits from telecommunications products and electrical products were flat .	I-entrel	324..463
Revenues rose 8.8 % to $ 4.35 billion from $ 4.0 billion .	O	466..520
The company said the quarter included a 10 % increase in local-exchange usage for long-distance calling and a 5 % increase in the number of access lines in service .	O	523..685
Earlier rate reductions in Texas and California reduced the quarter 's revenue and operating profit $ 55 million ; a year earlier , operating profit in telephone operations was reduced by a similar amount as a result of a provision for a reorganization .	O	686..935
Revenue in the telecommunications products and services unit rose 27 % to $ 728.8 million , but operating profit was unchanged at $ 26.3 million , partly because of start-up expenses .	B-expansion	938..1116
Electrical products ' sales fell to $ 496.7 million from $ 504.5 million with higher world-wide lighting volume offset by lower domestic prices and the impact of weaker currencies in Europe and South America .	B-expansion	1117..1322
Operating profit of $ 37.2 million was unchanged .	I-expansion	1323..1371
In composite trading on the New York Stock Exchange , GTE rose $ 1.25 to $ 64.125 .	O	1374..1453
MCI Communications Corp .	O	1454..1478
MCI , which stepped up efforts to sell long-distance telephone service to residential customers , reported a 59 % jump in earnings .	B-expansion	1481..1609
Revenue rose 23 % to $ 1.67 billion from $ 1.36 billion .	I-expansion	1610..1663
Operating profit grew 57 % to $ 269 million from $ 171 million , while operating margins rose to 16.1 % from 15.9 % the previous quarter and 12.6 % a year ago .	O	1666..1818
Daniel Akerson , MCI chief financial officer , said the company sees further improvements in operating margins .	O	1819..1928
" We think we can take it to the 18 % range over next 18 to 24 months , " he said .	O	1929..2007
In national over-the-counter trading , MCI fell $ 2.625 to $ 42.375 .	O	2010..2075
Charles Schellke , an analyst with Smith Barney , Harris Upham & Co. , said some investors apparently expected slightly better revenue growth .	O	2076..2215
The company said that residential traffic grew faster than business traffic and attributed that to its new PrimeTime calling plan that competes with American Telephone & Telegraph 's Reach Out America plan .	B-entrel	2218..2423
MCI claims about 12 % of the overall long-distance telephone market but just under 10 % of the $ 23 billion residential market .	B-contingency	2424..2548
It has been trying to improve its share of the residential market .	B-contingency	2549..2615
The company wants its business mix to more closely match that of AT&T -- a step it says will help prevent cross subsidization .	I-contingency	2616..2742
Mr. Akerson said MCI recorded " another solid cash positive quarter , " its fourth in a row , but declined to comment on whether the company is considering a dividend or is planning any acquisition .	O	2745..2939
The current quarter , he said , " looks fine .	O	2940..2982
We think revenue will continue to grow and that we can control costs and thus improve profitability . "	O	2983..3084
Southwestern Bell Corp .	O	3087..3110
Southwestern Bell Corp. said net dropped 8.7 % , mainly the result of four extraordinary items : a franchise tax refund that its Southwestern Bell Telephone Co. unit received last year ; a production shift of several Yellow Pages directories to the fourth quarter from the third ; a rate refund in Missouri and a one-time adjustment to phone company revenues .	O	3113..3467
Revenue slipped 1.2 % to $ 2.21 billion from $ 2.23 billion .	O	3470..3527
The earnings drop had been expected .	B-entrel	3530..3566
Chairman Zane E. Barnes said Southwestern Bell 's " businesses are healthy and are continuing to grow . "	I-entrel	3567..3668
The company reported a 3.1 % increase in the number of access lines in service , and also said its Southwestern Bell Mobile Systems unit added 30,000 new customers , with a current total of about 333,000 .	O	3671..3872
Southwestern shares fell 50 cents to $ 55.875 in composite trading on the New York Stock Exchange .	O	3875..3972
Cincinnati Bell Inc .	O	3975..3995
Cincinnati Bell Inc. said net declined 1.8 % .	O	3998..4042
The company noted that the year-ago period was particularly strong , with an increase of nearly 70 % .	B-entrel	4043..4142
Revenue jumped nearly 17 % to $ 223.3 million from $ 191.4 million .	B-expansion	4143..4207
In composite trading on the New York Stock Exchange , Cincinnati Bell fell 25 cents to $ 29 .	I-expansion	4208..4298
The company said that the number of access lines dropped slightly in the quarter , a decline attributed to seasonal fluctuations .	O	4301..4429
For the year , however , access lines in service have increased 5.5 % .	O	4430..4497
Chairman D.H. Hibbard said the company has set a new five year goal of doubling revenues to about $ 1.8 billion while steadily increasing net .	O	4500..4641
