Although bullish dollar sentiment has fizzled , many currency analysts say a massive sell-off probably wo n't occur in the near future .	O	9..142
While Wall Street 's tough times and lower U.S. interest rates continue to undermine the dollar , weakness in the pound and the yen is expected to offset those factors .	O	145..311
" By default , " the dollar probably will be able to hold up pretty well in coming days , says Francoise Soares-Kemp , a foreign-exchange adviser at Credit Suisse .	O	312..470
" We 're close to the bottom " of the near-term ranges , she contends .	O	471..537
In late Friday afternoon New York trading , the dollar was at 1.8300 marks and 141.65 yen , off from late Thursday 's 1.8400 marks and 142.10 yen .	B-list	540..683
The pound strengthened to $ 1.5795 from $ 1.5765 .	I-list	684..731
In Tokyo Monday , the U.S. currency opened for trading at 141.70 yen , down from Friday 's Tokyo close of 142.75 yen .	O	734..848
The dollar began Friday on a firm note , gaining against all major currencies in Tokyo dealings and early European trading despite reports that the Bank of Japan was seen unloading dollars around 142.70 yen .	B-synchrony	851..1057
The rise came as traders continued to dump the pound after the sudden resignation Thursday of British Chancellor of the Exchequer Nigel Lawson .	I-synchrony	1058..1201
But once the pound steadied with help from purchases by the Bank of England and the Federal Reserve Bank of New York , the dollar was dragged down , traders say , by the stock-market slump that left the Dow Jones Industrial Average with a loss of 17.01 points .	O	1204..1461
With the stock market wobbly and dollar buyers discouraged by signs of U.S. economic weakness and the recent decline in U.S. interest rates that has diminished the attractiveness of dollar-denominated investments , traders say the dollar is still in a precarious position .	O	1464..1735
" They 'll be looking at levels to sell the dollar , " says James Scalfaro , a foreign-exchange marketing representative at Bank of Montreal .	O	1736..1872
While some analysts say the dollar eventually could test support at 1.75 marks and 135 yen , Mr. Scalfaro and others do n't see the currency decisively sliding under support at 1.80 marks and 140 yen soon .	O	1875..2078
Predictions for limited dollar losses are based largely on the pound 's weak state after Mr. Lawson 's resignation and the yen 's inability to strengthen substantially when there are dollar retreats .	O	2081..2277
With the pound and the yen lagging behind other major currencies , " you do n't have a confirmation " that a sharp dollar downturn is in the works , says Mike Malpede , senior currency analyst at Refco Inc. in Chicago .	O	2278..2490
As far as the pound goes , some traders say a slide toward support at $ 1.5500 may be a favorable development for the dollar this week .	O	2493..2626
While the pound has attempted to stabilize , currency analysts say it is in critical condition .	B-cause	2629..2723
Sterling plunged about four cents Thursday and hit the week 's low of $ 1.5765 when Mr. Lawson resigned from his six-year post because of a policy squabble with other cabinet members .	I-cause	2724..2905
He was succeeded by John Major , who Friday expressed a desire for a firm pound and supported the relatively high British interest rates that he said " are working exactly as intended " in reducing inflation .	O	2908..3113
But the market remains uneasy about Mr. Major 's policy strategy and the prospects for the pound , currency analysts contend .	O	3114..3237
Although the Bank of England 's tight monetary policy has fueled worries that Britain 's slowing economy is headed for a recession , it is widely believed that Mr. Lawson 's willingness to prop up the pound with interest-rate increases helped stem pound selling in recent weeks .	B-contrast	3240..3514
If there are any signs that Mr. Major will be less inclined to use interest-rate boosts to rescue the pound from another plunge , that currency is expected to fall sharply .	I-contrast	3515..3686
" It 's fair to say there are more risks for the pound under Major than there were under Lawson , " says Malcolm Roberts , a director of international bond market research at Salomon Brothers in London .	O	3689..3886
" There 's very little upside to sterling , " Mr. Roberts says , but he adds that near-term losses may be small because the selling wave that followed Mr. Major 's appointment apparently has run its course .	B-contrast	3889..4089
But some other analysts have a stormier forecast for the pound , particularly because Britain 's inflation is hovering at a relatively lofty annual rate of about 7.6 % and the nation is burdened with a struggling government and large current account and trade deficits .	I-contrast	4092..4358
The pound likely will fall in coming days and may trade as low as 2.60 marks within the next year , says Nigel Rendell , an international economist at James Capel & Co. in London .	O	4361..4538
The pound was at 2.8896 marks late Friday , off sharply from 2.9511 in New York trading a week earlier .	O	4539..4641
If the pound falls closer to 2.80 marks , the Bank of England may raise Britain 's base lending rate by one percentage point to 16 % , says Mr. Rendell .	O	4644..4792
But such an increase , he says , could be viewed by the market as " too little too late . "	O	4793..4879
The Bank of England indicated its desire to leave its monetary policy unchanged Friday by declining to raise the official 15 % discount-borrowing rate that it charges discount houses , analysts say .	O	4880..5076
Pound concerns aside , the lack of strong buying interest in the yen is another boon for the dollar , many traders say .	O	5079..5196
The dollar has a " natural base of support " around 140 yen because the Japanese currency has n't been purchased heavily in recent weeks , says Ms. Soares-Kemp of Credit Suisse .	O	5199..5372
The yen 's softness , she says , apparently stems from Japanese investors ' interest in buying dollars against the yen to purchase U.S. bond issues and persistent worries about this year 's upheaval in the Japanese government .	O	5373..5594
On New York 's Commodity Exchange Friday , gold for current delivery jumped $ 5.80 , to $ 378.30 an ounce , the highest settlement since July 12 .	B-entrel	5597..5736
Estimated volume was a heavy seven million ounces .	I-entrel	5737..5787
In early trading in Hong Kong Monday , gold was quoted at $ 378.87 an ounce .	O	5790..5864
