reminiscent of those during the 1987 crash -- that as stock prices plummeted and trading activity escalated , some phone calls to market makers in over-the-counter stocks went unanswered .	root	9..195
" We could n't get dealers to answer their phones , " said Robert King , senior vice president of OTC trading at Robinson-Humphrey Co. in Atlanta .	norel	198..339
" It was { like } the Friday before Black Monday " two years ago .	cause	340..401
Whether unanswered phone calls had any effect or not , Nasdaq stocks sank far less than those on the New York and American exchanges .	norel	404..536
Nonetheless , the Nasdaq Composite Index suffered its biggest point decline of the year and its sixth worst ever , diving 14.90 , or 3 % , to 467.29 .	concession	537..681
Ten points of the drop occurred during the last 45 minutes of trading .	restatement	682..752
By comparison , the New York Stock Exchange Composite tumbled 5.8 % Friday	norel	753..825
and the American Stock Exchange Composite fell 4 % .	list	826..876
On Oct. 16 , 1987 , the Nasdaq Composite fell 16.18 points , or 3.8 % , followed by its devastating 46.12-point , or 11 % slide , three days later .	norel	879..1018
Nasdaq volume Friday totaled 167.7 million shares , which was only the fifth busiest day so far this year .	norel	1021..1126
The single-day record of 288 million shares was set on Oct. 21 ,	entrel	1127..1190
" There was n't a lot of volume because it was just impossible to get stock moved , " said E.E. " Buzzy " Geduld , president of Herzog , Heine , Geduld , a New York company that makes markets in thousands of OTC issues .	norel	1193..1402
Most of the complaints about unanswered phone calls came from regional brokers rather than individual investors .	norel	1405..1517
Mr. King of Robinson-Humphrey and others were quick to add that they believe the problem stemmed more from traders ' inability to handle the volume of calls , rather than a deliberate attempt to avoid making trades .	contrast	1518..1731
The subject is a sore one for Nasdaq and its market-making companies , which were widely criticized two years ago following complaints from investors who could n't reach their brokers or trade in the chaos of the crash .	norel	1734..1951
Peter DaPuzzo , head of retail equity trading at Shearson Lehman Hutton , declared : " It was the last hour of trading on a Friday .	norel	1954..2081
There were too many phones ringing and too many things happening to expect market makers to be as efficient as robots .	conjunction	2082..2200
It was n't intentional , we were all busy . "	restatement	2201..2242
James Tarantino , head of OTC trading at Hambrecht & Quist in San Francisco , said , " It was just like two years ago .	norel	2245..2359
Everybody was trying to do the same thing at the same time . "	cause	2360..2420
Jeremiah Mullins , the OTC trading chief at Dean Witter Reynolds in New York , said proudly that his company executed every order it received by the close of trading .	norel	2423..2587
But , he added , " you can only take one call at a time . "	contrast	2588..2642
Market makers keep supplies of stock on hand to maintain orderly trading when imbalances occur .	norel	2645..2740
On days like Friday , that means they must buy shares from sellers when no one else is willing to .	cause	2741..2838
When selling is so frenzied , prices fall steeply and fast .	conjunction	2839..2897
Two years ago , faced with the possibility of heavy losses on the stocks in their inventories , market makers themselves began dumping shares , exacerbating the slide in OTC stock prices .	restatement	2898..3082
On Friday , some market makers were selling again , traders said .	norel	3085..3148
But , with profits sagging on Wall Street since the crash , companies have kept smaller share stockpiles on hand .	contrast	3149..3260
Mr. Tarantino of Hambrecht & Quist said some prices fell without trades taking place , as market makers kept dropping the prices at which they would buy shares .	norel	3263..3422
" Everyone was hitting everyone else 's bid , " he said .	restatement	3423..3475
So , while OTC companies incurred losses on Friday , trading officials said the damage was n't as bad as it was in 1987 .	norel	3478..3595
" Two years ago we were carrying huge inventories and that was the big culprit .	cause	3596..3674
I do n't know of anyone carrying big inventories now , " said Mr. King of Robinson-Humphrey .	contrast	3675..3764
Tony Cecin , head of equity trading at Piper , Jaffray & Hopwood in Minneapolis , said that Piper Jaffray actually made money on Friday .	norel	3767..3900
It helped that his inventory is a third smaller now than it was two years ago , he said .	contrast	3901..3988
Joseph Hardiman , president of the National Association of Securities Dealers , which oversees the Nasdaq computerized trading system , said that despite the rush of selling , he never considered the situation an " emergency . "	norel	3991..4212
" The pace of trading was orderly , " he said .	norel	4215..4258
Nasdaq 's Small Order Execution System " worked beautifully , " as did the automated system for larger trades , according to Mr. Hardiman .	conjunction	4259..4392
Nevertheless , the shock of another steep plunge in stock prices undoubtedly will shake many investors ' confidence .	norel	4395..4509
In the past , the OTC market thrived on a firm base of small-investor participation .	entrel	4510..4593
Because Nasdaq 's trading volume has n't returned to pre-crash levels , traders and OTC market officials hope the damage wo n't be permanent .	cause	4594..4731
But they are worried .	contrast	4734..4755
" We were just starting to get the public 's confidence back , " lamented Mr. Mullins of Dean Witter .	instantiation	4756..4853
More troubling is the prospect that the overall collapse in stock prices could permanently erode the base of small-investor support the OTC market was struggling to rebuild in the wake of the October 1987 crash .	norel	4856..5067
Mr. Cecin of Piper Jaffray says some action from government policy makers would allay investor fears .	norel	5070..5171
It wo n't take much more to " scare the hell out of retail investors , " he says .	contrast	5172..5249
The sellers on Friday came from all corners of the OTC market -- big and small institutional investors , as well as individual investors and market makers .	norel	5252..5406
But grateful traders said the sell orders generally ranged from 20,000 shares to 50,000 shares , compared with blocks of 500,000 shares or more two years ago .	contrast	5407..5564
Shearson 's Mr. DaPuzzo said retail investors nervously sold stock Friday and never returned to bargain-hunt .	norel	5567..5675
Institutional investors , which had been selling stock throughout last week to lock in handsome gains made through the third quarter , were calmer .	contrast	5676..5821
" We had a good amount of selling from institutions , but not as much panic , " Mr. DaPuzzo said .	norel	5824..5917
" If they could n't sell , some of them put the shares back on the shelf . "	cause	5918..5989
In addition , he said , some bigger institutional investors placed bids to buy some OTC stocks whose prices were beaten down .	conjunction	5990..6113
In addition , Mr. DaPuzzo said computer-guided program selling of OTC stocks in the Russell Index of 2000 small stocks and the Standard & Poor 's 500-stock Index sent occasional " waves " through the market .	norel	6116..6320
Nasdaq 's biggest stocks were hammered .	norel	6323..6361
The Nasdaq 100 Index of the largest nonfinancial issues , including the big OTC technology issues , tumbled 4.2 % , or 19.76 , to 449.33 .	restatement	6362..6494
The Nasdaq Financial Index of giant insurance and banking stocks dropped 2 % , or 9.31 , to 462.98 .	conjunction	6495..6591
The OTC market has only a handful of takeover-related stocks .	norel	6594..6655
But they fell sharply .	contrast	6656..6678
McCaw Cellular Communications , for instance , has offered to buy LIN Broadcasting as well as Metromedia 's New York City cellular telephone interests , and in a separate transaction , sell certain McCaw properties to Contel Cellular .	instantiation	6679..6908
McCaw lost 8 % , or 3 1/2 , to 40 .	entrel	6909..6940
LIN Broadcasting , dropped 5 1/2 , or 5 % , to 107 1/2 .	contrast	6941..6992
The turnover in both issues was roughly normal .	contrast	6993..7040
On a day when negative takeover-related news did n't sit well with investors , Commercial Intertech , a maker of engineered metal parts , said Haas & Partners advised it that it does n't plan to pursue its previously reported $ 27.50-a-share bid to buy the company .	norel	7043..7302
Commercial Intertech plummeted 6 to 26 .	cause	7303..7342
The issues of companies with ties to the junk bond market also tumbled Friday .	norel	7345..7423
On the OTC market , First Executive , a big buyer of the high-risk , high-yield issues , slid 2 to 12 1/4 .	instantiation	7424..7526
Among other OTC issues , Intel , dropped 2 1/8 to 33 7/8 ; Laidlaw Transportation lost 1 1/8 to 19 1/2 ; the American depositary receipts of Jaguar were off 1/4 to 10 1/4 ; MCI Communications slipped 2 1/4 to 43 1/2 ; Apple Computer fell 3 to 45 3/4	norel	7529..7772
and Nike dropped 2 1/4 to 66 3/4 .	list	7773..7806
