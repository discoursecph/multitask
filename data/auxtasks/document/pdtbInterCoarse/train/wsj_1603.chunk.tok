For Vietnamese , these are tricky , often treacherous , times .	B-contingency	9..68
After years of hesitation , economic and political reform was embraced at the end of 1986 , but ringing declarations have yet to be translated into much action .	I-contingency	69..227
Vietnam is finding that turning a stagnant socialist order into a dynamic free market does n't come easy .	B-entrel	230..334
Here is how three Vietnamese are coping with change :	I-entrel	335..387
The Tire King	O	390..403
Nguyen Van Chan is living proof that old ways die hard .	O	406..461
Mr. Chan used to be an oddity in Hanoi : a private entrepreneur .	B-comparison	464..527
His business success made him an official target in pre-reform days .	B-entrel	528..596
Mr. Chan , now 64 years old , invented a fountain pen he and his family produced from plastic waste .	B-temporal	597..695
Later , he marketed glue .	B-expansion	696..720
Both products were immensely popular .	B-comparison	721..758
For his troubles , Mr. Chan was jailed three times between 1960 and 1974 .	B-expansion	759..831
Though his operation was registered and used only scrap , he was accused of conducting illegal business and possessing illegal materials .	B-expansion	832..968
Once he was held for three months without being charged .	I-expansion	969..1025
Things were supposed to change when Vietnam 's economic reforms gathered pace , and for awhile they did .	B-entrel	1028..1130
After years of experimenting , Mr. Chan produced a heavy-duty bicycle tire that outlasted its state-produced rival .	B-expansion	1131..1245
By 1982 , he was selling thousands of tires .	B-temporal	1246..1289
Newspapers published articles about him , and he was hailed as " the tire king . "	B-expansion	1290..1368
His efforts earned a gold medal at a national exhibition -- and attracted renewed attention from local authorities .	I-expansion	1369..1484
District police in 1983 descended on his suburban home , which he and his large family used as both residence and factory , and demanded proof the house and equipment were his .	O	1487..1661
He produced it .	O	1662..1677
" That was the first time they lost and I won , " he says .	B-entrel	1678..1733
He was further questioned to determine if he was " a real working man or an exploiter . "	I-entrel	1734..1820
Says Mr. Chan : " When I showed it was from my own brain , they lost for the second time . "	O	1821..1908
But a few days later the police accused him of stealing electricity , acquiring rubber without permission and buying stolen property .	B-contingency	1911..2043
Warned he was to be jailed again , he fled to the countryside .	B-expansion	2044..2105
His family was given three hours to leave before the house and contents were confiscated .	B-contingency	2106..2195
With only the clothes they were wearing , family members moved to a home owned by one of Mr. Chan 's sons .	I-contingency	2196..2300
After six months on the run , Mr. Chan learned the order for his arrest had been canceled .	B-contingency	2303..2392
He rejoined his family in January 1984 and began the long struggle for justice , pressing everyone from Hanoi municipal officials to National Assembly deputies for restoration of his rights .	I-contingency	2393..2582
He and his family kept afloat by repairing bicycles , selling fruit and doing odd jobs .	O	2583..2669
Mr. Chan achieved a breakthrough in 1987 -- and became a minor celebrity again -- when his story was published in a weekly newspaper .	O	2672..2805
In 1988 , 18 months after the sixth congress formally endorsed family-run private enterprise , district authorities allowed Mr. Chan to resume work .	B-expansion	2808..2954
By late last year he was invited back as " the tire king " to display his products at a national exhibition .	B-expansion	2955..3061
National leaders stopped by his stand to commend his achievements .	I-expansion	3062..3128
Mr. Chan now produces 1,000 bicycle and motorbike tires a month and 1,000 tins of tire-patching glue in the son 's small house .	B-entrel	3131..3257
Eighteen people pack the house 's two rooms -- the Chans , four of their 10 children with spouses , and eight of 22 grandchildren .	B-entrel	3258..3385
Most sleep on the floor .	I-entrel	3386..3410
Come daybreak , eight family members and two other workers unroll a sheet of raw rubber that covers the floor of the house and spills out onto the street .	B-expansion	3413..3566
The primitive operations also burst out the back door into a small courtyard , where an ancient press squeezes rubber solution into a flat strip and newly made tires are cooled in a bathtub filled with water .	I-expansion	3567..3774
Mr. Chan talks optimistically of expanding , maybe even moving into the import-export field .	B-comparison	3777..3868
First , however , he has unfinished business .	B-contingency	3869..3912
When district authorities allowed him to resume manufacturing , they released only one of his machines .	B-expansion	3913..4015
They did n't return the rubber stocks that represent his capital .	I-expansion	4016..4080
Nor did they return his house and contents , which he values at about $ 44,000 .	B-comparison	4081..4158
He wants to recover more than just his property , though .	I-comparison	4159..4215
" I want my dignity back , " he says .	O	4218..4252
The Editor	O	4255..4265
Nguyen Ngoc seemed an obvious choice when the Vietnamese Writers Association was looking for a new editor to reform its weekly newspaper , Van Nghe .	O	4268..4415
After the sixth congress , journalists seized the opportunity provided by the liberalization to probe previously taboo subjects .	O	4418..4545
Mr. Ngoc , 57 years old , had solid reformist credentials : He had lost his official position in the association in he early 1980s because he questioned the intrusion of politics into literature .	O	4546..4738
Appointed editor in chief in July 1987 , Mr. Ngoc rapidly turned the staid Van Nghe into Vietnam 's hottest paper .	O	4739..4851
Circulation soared as the weekly went way beyond standard literary themes to cover Vietnamese society and its ills .	B-expansion	4854..4969
Readers were electrified by the paper 's audacity and appalled by the dark side of life it uncovered .	I-expansion	4970..5070
One article recounted a decade-long struggle by a wounded soldier to prove , officially , he was alive .	B-expansion	5073..5174
Another described how tax-collection officials in Thanh Hoa province one night stormed through homes and confiscated rice from starving villagers .	I-expansion	5175..5321
The newspaper also ran a series of controversial short stories by Nguyen Huy Thiep , a former history teacher , who stirred debate over his interpretation of Vietnamese culture and took a thinly veiled swipe at writers who had blocked his entry into their official association .	O	5322..5597
Van Nghe quickly made influential enemies .	O	5600..5642
" Those who manage ideology and a large number of writers reacted badly " to the restyled paper , says Lai Nguyen An , a literary critic .	O	5643..5776
After months of internal rumblings , Mr. Ngoc was fired last December .	B-entrel	5777..5846
His dismissal triggered a furor among intellectuals that continues today .	I-entrel	5847..5920
" Under Mr. Ngoc , Van Nghe protected the people instead of the government , " says Nguyen Duy , a poet who is the paper 's bureau chief for southern Vietnam .	O	5923..6075
" The paper reflected the truth .	B-comparison	6076..6107
For the leadership , that was too painful to bear . "	I-comparison	6108..6158
The ' Billionaire '	O	6161..6178
Nguyen Thi Thi is Vietnam 's entrepreneur of the 1980s .	B-entrel	6181..6235
Her challenge is to keep her fledgling empire on top in the 1990s .	I-entrel	6236..6302
Mrs. Thi did n't wait for the reforms to get her start .	B-comparison	6305..6359
She charged ahead of the government and the law to establish Hochiminh City Food Co. as the biggest rice dealer in the country .	I-comparison	6360..6487
Her success , which included alleviating an urban food shortage in the early 1980s , helped persuade Hanoi to take the reform path .	B-expansion	6488..6617
Her story is becoming part of local folklore .	I-expansion	6618..6663
A lifelong revolutionary with little education who fought both the French and the U.S.-backed Saigon regime , she switched effortlessly to commerce after the war .	O	6666..6827
Her instincts were capitalistic , despite her background .	B-entrel	6828..6884
As she rode over regulations , only her friendship with party leaders , including Nguyen Van Linh , then Ho Chi Minh City party secretary , kept her out of jail .	I-entrel	6885..7042
Following Mr. Linh 's appointment as secretary-general of the party at the sixth congress , Mrs. Thi has become the darling of " doi moi " , the Vietnamese version of perestroika .	O	7045..7219
The authorities have steered foreign reporters to her office to see an example of " the new way of thinking . "	O	7220..7328
Foreign publications have responded with articles declaring her Vietnam 's richest woman .	O	7331..7419
" Some people call me the communist billionaire , " she has told visitors .	O	7420..7491
Actually , 67-year-old Mrs. Thi is about as poor as almost everyone else in this impoverished land .	B-expansion	7494..7592
She has indeed turned Hochiminh City Food into a budding conglomerate , but the company itself remains state-owned .	B-entrel	7593..7707
She manages it with the title of general-director .	I-entrel	7708..7758
The heart of the business is the purchase of rice and other commodities , such as corn and coffee , from farmers in the south , paying with fertilizer , farm tools and other items .	O	7761..7937
Last year , Hochiminh City Food says it bought two million metric tons of unhusked rice , more than 10 % of the country 's output .	O	7938..8064
The company operates a fleet of trucks and boats to transport the commodities to its warehouses .	B-temporal	8067..8163
A subsidiary company processes commodities into foods such as instant noodles that are sold with the rice through a vast retail network .	I-temporal	8164..8300
In recent years , Mrs. Thi has started to diversify the company , taking a 20 % stake in newly established , partly private Industrial and Commercial Bank , and setting up Saigon Petro , which owns and operates Vietnam 's first oil refinery .	O	8303..8537
Mrs. Thi says Hochiminh City Food last year increased pretax profit 60 % to the equivalent of about $ 2.7 million on sales of $ 150 million .	O	8540..8677
She expects both revenue and profit to gain this year .	O	8678..8732
She is almost cavalier about the possibility Vietnam 's reforms will create rivals on her home turf .	B-expansion	8735..8834
" I do n't mind the competition inside the country , " she says .	I-expansion	8835..8895
" I am only afraid that with Vietnam 's poor-quality products we ca n't compete with neighboring countries .	O	8896..9000
