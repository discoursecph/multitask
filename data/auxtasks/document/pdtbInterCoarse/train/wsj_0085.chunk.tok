For 10 years , Genie Driskill went to her neighborhood bank because it was convenient .	B-entrel	9..94
A high-balance customer that banks pine for , she did n't give much thought to the rates she was receiving , nor to the fees she was paying .	B-comparison	95..232
But in August , First Atlanta National Bank introduced its Crown Account , a package designed to lure customers such as Ms. Driskill .	B-entrel	235..366
Among other things , it included checking , safe deposit box and credit card -- all for free -- plus a good deal on installment loans .	I-entrel	367..499
All she had to do was put $ 15,000 in a certificate of deposit , or qualify for a $ 10,000 personal line of credit .	O	500..612
" I deserve something for my loyalty , " she says .	O	615..662
She took her business to First Atlanta .	O	663..702
So it goes in the competitive world of consumer banking these days .	B-temporal	705..772
For nearly a decade , banks have competed for customers primarily with the interest rates they pay on their deposits and charge on their loans .	B-temporal	773..915
The competitive rates were generally offset by hefty fees on various services .	I-temporal	916..994
But many banks are turning away from strict price competition .	B-expansion	997..1059
Instead , they are trying to build customer loyalty by bundling their services into packages and targeting them to small segments of the population .	I-expansion	1060..1207
" You 're dead in the water if you are n't segmenting the market , " says Anne Moore , president of Synergistics Research Corp. , a bank consulting firm in Atlanta .	O	1210..1367
NCNB Corp. of Charlotte , N.C. , recently introduced its Financial Connections Program aimed at young adults just starting careers .	B-expansion	1370..1499
The program not only offers a pre-approved car loan up to $ 18,000 , but throws in a special cash-flow statement to help in saving money .	I-expansion	1500..1635
In September , Union Planters Corp. of Memphis , Tenn. , launched The Edge account , a package designed for the " thirtysomething " crowd with services that include a credit card and line of credit with no annual fees , and a full percentage point off on installment loans .	B-contingency	1638..1904
The theory : Such individuals , many with young children , are in their prime borrowing years -- and , having borrowed from the bank , they may continue to use it for other services in later years .	I-contingency	1905..2097
For some time , banks have been aiming packages at the elderly , the demographic segment with the highest savings .	B-temporal	2100..2212
Those efforts are being stepped up .	B-expansion	2213..2248
Judie MacDonald , vice president of retail sales at Barnett Banks Inc. of Jacksonville , Fla. , says the company now targets sub-segments within the market by tailoring its popular Seniors Partners Program to various life styles .	I-expansion	2249..2475
" Varying age , geography and life-style differences create numerous sub-markets , " Ms. MacDonald says .	O	2478..2578
She says individual Barnett branches can add different benefits to their Seniors Partners package -- such as athletic activities or travel clubs -- to appeal to local market interests .	O	2579..2763
" An active 55-year-old in Boca Raton may care more about Senior Olympic games , while a 75-year-old in Panama City may care more about a seminar on health , " she says .	O	2764..2929
Banks have tried packaging before .	B-expansion	2932..2966
In 1973 , Wells Fargo & Co. of San Francisco launched the Gold Account , which included free checking , a credit card , safe-deposit box and travelers checks for a $ 3 monthly fee .	I-expansion	2967..3142
The concept begot a slew of copycats , but the banks stopped promoting the packages .	B-contingency	3145..3228
One big reason : thin margins .	B-entrel	3229..3258
Many banks , particularly smaller ones , were slow to computerize and could n't target market niches that would have made the programs more profitable .	B-temporal	3259..3407
As banks ' earnings were squeezed in the mid-1970s , the emphasis switched to finding ways to cut costs .	I-temporal	3408..3510
But now computers are enabling more banks to analyze their customers by age , income and geography .	B-expansion	3513..3611
They are better able to get to those segments in the wake of the deregulation that began in the late 1970s .	B-expansion	3612..3719
Deregulation has effectively removed all restrictions on what banks can pay for deposits , as well as opened up the field for new products such as high-rate CDs .	B-contingency	3720..3880
Where a bank once offered a standard passbook savings account , it began offering money-market accounts , certificates of deposit and interest-bearing checking , and staggering rates based on the size of deposits .	I-contingency	3881..4091
The competition has grown more intense as bigger banks such as Norwest Corp. of Minneapolis and Chemical Banking Corp. of New York extend their market-share battles into small towns across the nation .	O	4094..4294
" Today , a banker is worrying about local , regional and money-center { banks } , as well as thrifts and credit unions , " says Ms. Moore at Synergistics Research .	O	4295..4451
" So people who were n't even thinking about targeting 10 years ago are scrambling to define their customer base . "	O	4452..4564
The competition has cultivated a much savvier consumer .	O	4567..4622
" The average household will spread 19 accounts over a dozen financial institutions , " says Michael P. Sullivan , who runs his own bank consulting firm in Charlotte , N.C .	B-entrel	4623..4790
" This much fragmentation makes attracting and keeping today 's rate-sensitive customers costly . "	I-entrel	4791..4886
Packages encourage loyalty by rewarding customers for doing the bulk of their banking in one place .	B-expansion	4889..4988
For their troubles , the banks get a larger captive audience that is less likely to move at the drop of a rate .	I-expansion	4989..5099
The more accounts customers have , Mr. Sullivan says , the more likely they are to be attracted to a package -- and to be loyal to the bank that offers it .	O	5100..5253
That can pay off down the road as customers , especially the younger ones , change from borrowers to savers/investors .	O	5254..5370
Packaging has some drawbacks .	B-expansion	5373..5402
The additional technology , personnel training and promotional effort can be expensive .	B-expansion	5403..5489
Chemical Bank spent more than $ 50 million to introduce its ChemPlus line , several packages aimed at different segments , in 1986 , according to Thomas Jacob , senior vice president of marketing .	I-expansion	5490..5681
" It 's not easy to roll out something that comprehensive , and make it pay , " Mr. Jacob says .	O	5682..5772
Still , bankers expect packaging to flourish , primarily because more customers are demanding that financial services be tailored to their needs .	O	5775..5918
" These days , banking customers walk in the door expecting you to have a package especially for them , " Ms. Moore says .	O	5919..6036
Some banks are already moving in that direction , according to Alvin T. Sale , marketing director at First Union Corp. in Charlotte .	O	6037..6167
First Union , he says , now has packages for seven customer groups .	O	6168..6233
Soon , it will split those into 30 .	O	6234..6268
Says Mr. Sale : " I think more banks are starting to realize that we have to be more like the department store , not the boutique . "	O	6271..6399
IRAs .	O	6402..6407
