Times may be tough on Wall Street for some , but a few bosses are making as much as ever -- or more .	O	9..108
At Bear Stearns Cos. , for example , the 15 executive officers led by Chairman Alan " Ace " Greenberg got a pay increase to $ 35.9 million for the 14-month period ended June 30 from $ 22.9 million for the 12 months ended April 30 , 1988 .	B-expansion	111..341
The figures do n't include substantial dividends on holdings of Bear Stearns stock .	B-entrel	342..424
Mr. Greenberg himself was paid $ 4.5 million , before an estimated $ 1.5 million in dividends , up from $ 2.4 million the year before .	I-entrel	425..554
The increase is noted in the brokerage firm 's latest proxy statement filed with the Securities and Exchange Commission .	B-entrel	557..676
Because it operates on a fiscal year , Bear Stearns 's yearly filings are available much earlier than those of other firms .	B-expansion	677..798
The latest period includes 14 months instead of 12 because Bear Stearns changed to a fiscal year ending in June instead of April .	I-expansion	799..928
Meanwhile , Bear Stearns 's 650 stock and bond salesmen saw thinner paychecks over the past year , which the company says reflected lower commission revenue caused by a decline in investor activity in the markets .	O	931..1141
However , Bear Stearns on Monday reported improved earnings for its first quarter , ended Sept. 29 , partly because of a 31 % increase in commissions during the quarter .	O	1142..1307
William J. Montgoris , chief financial officer , defended the lofty salaries at Bear Stearns .	B-expansion	1310..1401
" All of us are on a base salary of $ 200,000 if the firm makes nothing -- and that 's pretty low as far as Wall Street goes , " Mr. Montgoris said .	I-expansion	1402..1545
However , Bear Stearns has never had an unprofitable year since its founding 65 years ago .	O	1546..1635
Four Bear Stearns executives besides the 62-year-old Mr. Greenberg were paid $ 3 million or more before dividends for the 14 months ended in June .	O	1638..1783
According to the proxy statement , James E. Cayne , 55 , Bear Stearns 's president , made $ 3.9 million ; an executive vice president , Michael L. Tarnopol , 53 , made nearly $ 3.4 million ; and two executive vice presidents , Vincent J. Mattone , 44 , and William J. Michaelcheck , 42 , made about $ 3.3 million each .	O	1786..2086
Mr. Montgoris said the firm has a " straight mathematical formula " for determining compensation , based on the firm 's earnings .	O	2089..2214
" Just because a particular element of the firm is down , " such as stockbrokerage , " does n't mean the executive committee should be paid less , " he said .	O	2215..2364
