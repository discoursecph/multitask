When the Soviets announced their last soldier had left Afghanistan in February , the voices of skepticism were all but drowned out by an international chorus of euphoria .	ROOT	0
It was `` the Soviets ' Vietnam . ''	COREF_PREV	1
The Kabul regime would fall .	NONE	2
Millions of refugees would rush home .	NONE	3
A resistance government would walk into Kabul .	NONE	4
Those who bought that illusion are now bewildered .	NONE	5
Eight months after Gen. Boris Gromov walked across the bridge into the U.S.S.R. , a Soviet - controlled regime remains in Kabul , the refugees sit in their camps , and the restoration of Afghan freedom seems as far off as ever .	COREF_OTHER	6
But there never was a chance that the Afghan resistance would overthrow the Kabul regime quickly and easily .	COREF_PREV	7
Soviet leaders said they would support their Kabul clients by all means necessary -- and did .	COREF_PREV	8
The U.S. said it would fully support the resistance -- and did n't .	COREF_OTHER	9
With the February 1987 U.N. accords `` relating to Afghanistan , '' the Soviet Union got everything it needed to consolidate permanent control .	COREF_OTHER	10
The terms of the Geneva accords leave Moscow free to provide its clients in Kabul with assistance of any kind -- including the return of Soviet ground forces -- while requiring the U.S. and Pakistan to cut off aid .	COREF_PREV	11
The only fly in the Soviet ointment was the last - minute addition of a unilateral American caveat , that U.S. aid to the resistance would continue as long as Soviet aid to Kabul did .	COREF_OTHER	12
But as soon as the accords were signed , American officials sharply reduced aid .	COREF_OTHER	13
In February 1989 , when the Soviets said they had completed their pullout , the U.S. cut it further .	COREF_OTHER	14
Not so the Soviets .	COREF_PREV	15
Gen. Gromov himself said Soviet troops expected to leave behind more than $ 1 billion of military equipment and installations for the Kabul regime .	COREF_OTHER	16
Since the troop withdrawal , Moscow has poured in an additional $ 200 to $ 300 million worth per month -- nearly $ 2 billion since February , equivalent to the total U.S. aid to the resistance in nine years .	COREF_OTHER	17
This includes what Deputy Foreign Minister Yuli Vorontsov fetchingly called `` new peaceful long - range weapons , '' including more than 800 SCUD missiles .	COREF_PREV	18
By early May , Moscow had delivered , for example , 1,000 trucks , about 100 tanks , artillery and hundreds of other combat vehicles .	COREF_OTHER	19
Later that month , it added an entire tank brigade , including 120 T - 72 tanks and more than 40 BMP state - of - the - art infantry fighting vehicles .	COREF_PREV	20
By September , a new Reinforced Motorized Rifle Brigade with an additional 300 combat vehicles , 1,000 more trucks and 10,000 Soviet - trained Afghan troops had arrived in Kandahar .	NONE	21
In the last few weeks , Moscow has added FROG - 7B missiles , the bomber version of the An - 12 , MiG - 23BN high - altitude aircraft , MiG - 29s , which can outfly Pakistan 's U.S. - built F16s , and Sukhoi SU - 27 fighter - bombers , which can outfly the MiG - 29s .	COREF_OTHER	22
Moscow claims this is all needed to protect the Kabul regime against the guerrilla resistance .	COREF_PREV	23
It is well - known that the regular Afghan infantry is filled with reluctant conscripts .	NONE	24
But this is not the entire Afghan army , and it is no longer Kabul 's only military force .	COREF_OTHER	25
Complete units have been trained and indoctrinated in the U.S.S.R. and other East bloc nations ; 30,000 to 40,000 of these troops have returned .	COREF_OTHER	26
In addition , the regime has established well - paid paramilitary forces totaling more than 100,000 , including 35,000 Soviet - trained troops of the Interior Ministry ( KHAD / WAD ) , which still is directed by 1,500 Soviet KGB officers .	COREF_OTHER	27
Even if not all these forces are committed to the regime , they are now dependent on it .	COREF_PREV	28
And thousands of Afghan children have been taken to the Soviet Union , where they are hostage for the behavior of their families .	COREF_PREV	29
Since 1981 , Indian military advisers have been assisting the Kabul regime .	COREF_PREV	30
In preparation for the withdrawal , Moscow , Kabul and New Delhi signed two agreements for several hundred newly civilian Indian experts to replace some of the more visible Soviet military personnel .	COREF_OTHER	31
Cuban military personnel also have been active in Afghanistan since 1979 .	NONE	32
The Soviets cut a deal with Iran : a future Iranian role in Afghanistan in exchange for Iranian support of Soviet policy .	COREF_OTHER	33
The deal was symbolized by the restoration of the Shi'ite Sultan Ali Keshtmand to the Afghan prime ministry .	COREF_PREV	34
Moreover , serious questions have been raised about the claimed withdrawal of Soviet forces .	COREF_OTHER	35
Before his assassination in 1988 , President Zia of Pakistan repeatedly stated that fresh Soviet troops were being inserted into Afghanistan even as others were ostentatiously withdrawn .	COREF_OTHER	36
Rep. Bill McCollum ( R. , Fla. ) reports that these included 20,000 to 30,000 Soviet Central Asian KGB Border Guards , ethnically indistinguishable from Afghans and wearing unmarked uniforms .	COREF_PREV	37
Meanwhile , the Kabul regime is increasingly successful at portraying the resistance as bloody - minded fanatics .	COREF_OTHER	38
In this they are aided by years of American , European , Pakistani and Saudi support for the most extreme factions -- radical Islamic fanatics with leaders whose policies are anathema to the Afghan public .	COREF_PREV	39
This heavy outside support for the worst has undermined better , moderate leaders .	NONE	40
In autumn last year , for example , the regime garrison at Kandahar was prepared to surrender the city to resistance moderates .	COREF_OTHER	41
At the last minute , however , Pakistani officials sent in Gulbuddin Hekhmatyar , perhaps the most hated and feared of the extremists , with a demand that the surrender be made to his forces .	COREF_PREV	42
The deal fell through , and Kandahar remains a major regime base .	COREF_PREV	43
The resistance lacks not only air power , armor and expertise but often such essentials as maps , mine detectors , or even winter gloves .	COREF_OTHER	44
Experienced resistance commanders wanted to use guerrilla action and siege tactics to wear down the regime .	NONE	45
Instead , they were pressured by Pakistan 's ISI , the channel for their support , into attacking Jalalabad .	COREF_PREV	46
They took more than 25 % casualties ; journalists report that they faced minefields without mine detectors .	COREF_PREV	47
The wonder is not that the resistance has failed to topple the Kabul regime , but that it continues to exist and fight at all .	COREF_PREV	48
Last summer , in response to congressional criticism , the State Department and the CIA said they had resumed military aid to the resistance months after it was cut off ; but it is not clear how much is being sent or when it will arrive .	COREF_PREV	49
For months the resistance has been defenseless against air attack .	COREF_PREV	50
Thus far there is no indication that they have been re-supplied with Stingers or other anti-aircraft weapons .	COREF_PREV	51
Indeed , U.S. officials have indicated to the press that the continuation of aid depends on what success the weakened resistance achieves by the end of this year .	COREF_PREV	52
Moscow and Kabul must have found that information useful .	NONE	53
For a decade U.S. policy has been incoherent , based on miscalculation and the defense of bureaucratic and political turf .	COREF_OTHER	54
No settlement negotiated by others can force the Afghan people to give up their struggle .	NONE	55
A cutoff of U.S. military aid would merely abandon them to die in vain .	NONE	56
Creation of a new , realistic U.S. policy is long overdue .	COREF_PREV	57
Ms. Klass , editor and co-author of `` Afghanistan : The Great Game Revisited '' ( Freedom House ) , directs the Freedom House program on Afghanistan / Southwest Asia .	NONE	58
