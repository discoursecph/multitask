Few people are aware	(NN-Textual-organization(NN-Topic-Comment(NS-Background(SN-Attribution
that the federal government lends almost as much money	(NN-Comparison
as it borrows .	NN-Comparison))
From 1980 to 1988 ,	(NN-Joint(NS-Elaboration(NN-Same-unit(NS-Background
while federal budget deficits totaled $ 1.41 trillion ,	NS-Background)
the government issued $ 394 billion of new direct loans and an additional $ 756 billion of new primary loan guarantees .	NN-Same-unit)
These figures omit secondary guarantees , deposit insurance , and the activities of Government-Sponsored Enterprises	(NS-Elaboration
( a huge concern in its own right ,	(NS-Attribution
as detailed on this page May 3 ) .	NS-Attribution)))
Federal credit programs date back to the New Deal ,	(NN-Joint(NN-Joint
and were meant to break even financially .	NN-Joint)
Since the 1950s , federal lending has experienced extraordinary growth in credit volume , subsidy rates , and policy applications ,	(NS-Elaboration(NS-Elaboration
spurred on by the growth of government in general and budget gimmicks and deceptive management in particular .	NS-Elaboration)
As we will see ,	(NS-Elaboration(SN-Contrast(SN-Attribution
many of these obligations do n't show up as part of the federal deficit .	SN-Attribution)
But recent events indicate	(SN-Attribution
that federal credit is out of control .	SN-Attribution))
Student loan defaults remain high at about 12 % ,	(NN-Joint(NN-Joint
and the program has been rocked by allegations of fraud and mismanagement .	NN-Joint)
Farmers Home Administration	(NN-Joint(NN-Joint(NN-Same-unit(NS-Summary
( FmHA )	NS-Summary)
loans have turned into de facto giveaway programs ;	NN-Same-unit)
losses over the next three years are expected to exceed $ 20 billion .	NN-Joint)
Defaults on Veterans Affairs loan guarantees have quadrupled in the past eight years .	(NN-Joint
Last month , the General Accounting Office reported	(NS-Evaluation(SN-Attribution
that defaults in Federal Housing Administration guarantees were five times as high	(NN-Joint(NN-Comparison
as previously estimated ,	NN-Comparison)
and that FHA 's equity fell to minus $ 2.9 billion .	NN-Joint))
GAO 's findings are particularly troubling	(NS-Elaboration(SN-Cause
because the FHA has about $ 300 billion in obligations outstanding	(NN-Contrast(NN-Joint
and had previously been considered one of the most financially secure credit programs .	NN-Joint)
Scores of other credit programs ,	(NN-Same-unit(NS-Elaboration
subsidizing agriculture , small business , exporters , defense , energy , transportation and others ,	NS-Elaboration)
are less visible but in no better shape .	NN-Same-unit)))
If the programs continue their present path ,	(NS-Explanation(SN-Condition
the potential government losses are staggering :	SN-Condition)
The federal government holds $ 222 billion in direct loans outstanding	(NS-Evaluation(NS-Elaboration(NN-Joint
and backs an additional $ 550 billion in primary guarantees .	NN-Joint)
( Secondary guarantees of pools of FHA- and VA-backed loans by the agency	(NN-Same-unit(NS-Elaboration
known as Ginnie Mae	NS-Elaboration)
currently exceed $ 330 billion . )	NN-Same-unit))
Although external events have contributed to the morass ,	(SN-Contrast
the principal causes of the current crisis are internal and generic to all programs .	SN-Contrast)))))))))))))
To reduce the risks	(NN-Topic-Comment(SN-Enablement(SN-Contrast
while still retaining the legitimate benefits	(NS-Elaboration
these programs can provide ,	NS-Elaboration))
credit policy must :	SN-Enablement)
1. Use credit	(NN-Joint(NS-Explanation(NS-Enablement
to improve the operation of capital markets ,	(NS-Evaluation(NN-Joint
not to provide subsidies .	NN-Joint)
There is a fundamental conflict	(NS-Elaboration
between providing a subsidy	(NN-Joint
and maintaining the integrity of a credit program .	NN-Joint))))
If the program is meant to provide a subsidy ,	(NS-Elaboration(NN-Contrast(NN-Topic-Comment(SN-Background(SN-Condition
collecting the debt defeats the original goal .	SN-Condition)
Thus , subsidized loans tend to turn into giveaway programs , with increasing subsidy and default rates over time .	SN-Background)
To avoid this problem ,	(NS-Condition(SN-Enablement
government should issue credit	SN-Enablement)
only if it intends to use every legal method	(NS-Elaboration
to collect .	NS-Elaboration)))
In contrast , credit programs can be appropriate tools	(NS-Elaboration
to improve the operation of capital markets .	NS-Elaboration))
For example , legal restrictions on interstate banking once inhibited the supply of credit to the agricultural sector .	(SN-Evaluation(NN-Topic-Comment
Farm lending was enacted	(NS-Enablement
to correct this problem	(NS-Manner-Means
by providing a reliable flow of lendable funds .	NS-Manner-Means)))
However , this in no way justifies the huge government subsidies and losses on such loans .	(NS-Explanation
Credit policy should separate these two competing objectives	(NS-Elaboration(NN-Joint
and eliminate aspects	(NS-Elaboration
that provide the subsidy .	NS-Elaboration))
For example , student loans currently attempt to subsidize college attendance and mitigate problems	(NN-Contrast(NS-Elaboration(NS-Elaboration
created by the fact	(NS-Elaboration
that students ' future earnings are not accepted as collateral .	NS-Elaboration))
The program provides highly subsidized loans to any student	(NN-Joint(NS-Elaboration
whose family earns less than a particular amount .	NS-Elaboration)
High default rates , a low interest rate , and government coverage of all interest costs	(NN-Joint(NN-Same-unit(NS-Background
while the student is in school	NS-Background)
make program costs extremely high .	NN-Same-unit)
Families	(NS-Manner-Means(NN-Same-unit(NS-Elaboration
that do not need the loan	NS-Elaboration)
can make money	NN-Same-unit)
simply by putting the loan in the bank	(NN-Joint
and paying it back	(NS-Background
when the student graduates .	NS-Background))))))
In contrast , a student loan program	(NS-Elaboration(NN-Same-unit(NS-Elaboration
that was meant solely to correct capital-market imperfections	NS-Elaboration)
would allow loans for any student ,	(NN-Same-unit(NS-Contrast
regardless of family income ,	NS-Contrast)
at market or near-market rates .	NN-Same-unit))
While the student was in school ,	(NN-Joint(SN-Background
interest costs would either be paid by the student	(NN-Condition
or added to the loan balance .	NN-Condition))
This program ,	(NN-Same-unit(NS-Background
combined with cash grants to needy students ,	NS-Background)
would reduce program costs	(NN-Joint
and much more effectively target the intended beneficiaries .	NN-Joint))))))))))
2. Provide better incentives .	(NN-Joint(NS-Evaluation(NS-Explanation
Given the structure of most credit programs ,	(NS-Elaboration(SN-Background
it is surprising that default rates are not even higher .	SN-Background)
Guarantee rates are typically 100 % ,	(NN-Topic-Comment(SN-Background
giving lenders little reason	(NS-Elaboration
to screen customers carefully .	NS-Elaboration))
Reducing those rates moderately	(NN-Contrast(NN-Same-unit(NS-Elaboration
( say , to 75 % )	NS-Elaboration)
would still provide substantial assistance to borrowers .	NN-Same-unit)
But it would also encourage lenders to choose more creditworthy customers ,	(NS-Elaboration(NN-Joint
and go a long way toward reducing defaults .	NN-Joint)
For example , the Small Business Administration has had reasonable success	(NS-Elaboration
in reducing both guarantee rates and default rates in its Preferred Lenders ' Program .	NS-Elaboration))))))
Borrowers ' incentives are equally skewed .	(NS-Explanation
Since the government has a dismal record	(NN-Topic-Comment(NN-Joint(SN-Explanation(NS-Elaboration
of collecting bad debts ,	NS-Elaboration)
the costs to the borrower of defaulting are usually low .	SN-Explanation)
In addition , it is often possible to obtain a new government loan	(NS-Contrast
even if existing debts are not paid off .	NS-Contrast))
Simple policy prescriptions in this case would be to improve debt collection	(NS-Elaboration(NN-Joint(NS-Cause
( taking the gloves off contracted collection agencies )	NS-Cause)
and to deny new credit to defaulters .	NN-Joint)
These provisions would be difficult to enforce for a program	(NN-Contrast(NS-Elaboration
intended to provide a subsidy ,	NS-Elaboration)
but would be reasonable and effective devices for programs	(NS-Elaboration
that attempt to offset market imperfections .	NS-Elaboration))))))
3. Record the true costs of credit programs in the federal budget .	(NN-Joint(NS-Background(NS-Explanation
Since the budget measures cash flow ,	(NN-Topic-Comment(NN-Joint(SN-Background
a new $ 1 direct loan is treated as a $ 1 expenditure ,	(NS-Contrast
even though at least part of the loan will be paid back .	NS-Contrast))
Loan guarantees do n't appear at all	(NN-Cause(NS-Background
until the borrower defaults ,	NS-Background)
so new guarantees do not raise the deficit ,	(NS-Contrast
even though they create future liabilities for the government .	NS-Contrast)))
By converting an expenditure or loan to a guarantee ,	(SN-Manner-Means
the government can ensure the same flow of resources	(NN-Joint
and reduce the current deficit .	NN-Joint))))
Predictably , guarantees outstanding have risen by $ 130 billion since 1985 ,	(NS-Elaboration(NN-Joint
while direct loans outstanding have fallen by $ 30 billion .	NN-Joint)
The true budgetary cost of a credit subsidy is the discounted value of the net costs to government .	(NN-Joint(NS-Elaboration
This figure could be estimated	(NS-Evaluation(NN-Condition(NS-Manner-Means
using techniques	(NS-Elaboration
employed by private lenders	(NS-Enablement
to forecast losses ,	NS-Enablement)))
or determined	(NS-Manner-Means
by selling loans to private owners	(NS-Elaboration
( without federal guarantees ) .	NS-Elaboration)))
Neither technique is perfect ,	(NS-Elaboration(SN-Contrast
but both are better than the current system ,	SN-Contrast)
which misstates the costs of new credit programs by amounts	(NS-Attribution(NS-Elaboration
that vary substantially	(NN-Joint
and average about $ 20 billion annually ,	NN-Joint))
according to the Congressional Budget Office .	NS-Attribution))))
A budget	(NN-Same-unit(NS-Elaboration
that reflected the real costs of lending	NS-Elaboration)
would eliminate incentives	(NN-Joint(NS-Elaboration
to convert spending or lending programs to guarantees	NS-Elaboration)
and would let taxpayers know	(SN-Attribution
what Congress is committing them to .	SN-Attribution))))))
4. Impose standard accounting and administrative practices .	(NS-Evaluation(NS-Evaluation(NS-Explanation
Creative accounting is a hallmark of federal credit .	(NS-Explanation
Many agencies roll over their debt ,	(NN-Joint(NS-Summary(NS-Manner-Means
paying off delinquent loans	(NN-Condition(NS-Manner-Means
by issuing new loans ,	NS-Manner-Means)
or converting defaulted loan guarantees into direct loans .	NN-Condition))
In any case , they avoid having to write off the loans .	NS-Summary)
Some agencies simply keep bad loans on the books ;	(NS-Elaboration
as late as 1987 , the Export-Import Bank held in its portfolio at face value loans	(NN-Joint(NS-Elaboration
made to Cuba in the 1950s .	NS-Elaboration)
More seriously , FmHA has carried several billion dollars of defaulted loans at face value .	(NS-Background
Until GAO 's recent audit , FHA books had not been subject to a complete external audit in 15 years .	NS-Background))))))
The administration of federal credit should closely parallel private lending practices ,	(SN-Enablement(NS-Elaboration
including the development of a loan loss reserve and regular outside audits .	NS-Elaboration)
Establishing these practices would permit earlier identification of emerging financial crises ,	(NN-Joint
provide better information for loan sales and budgeting decisions ,	(NN-Joint
and reduce fraud .	NN-Joint))))
Government lending was not intended to be a way	(NN-Joint(NS-Elaboration
to obfuscate spending figures ,	(NN-Joint
hide fraudulent activity ,	(NN-Joint
or provide large subsidies .	NN-Joint)))
The reforms	(NS-Evaluation(NN-Same-unit(NS-Elaboration
described above	NS-Elaboration)
would provide a more limited , but clearer , safer and ultimately more useful role for government as a lender .	NN-Same-unit)
Without such reforms ,	(SN-Background
credit programs will continue to be a large-scale , high-risk proposition for taxpayers .	SN-Background)))))))))
Mr. Gale is an assistant professor of economics at UCLA .	NN-Textual-organization)
