The Federal Reserve System is the standard object of suggestions for organizational and institutional changes , for two reasons .	root	9..136
First , its position in the government is anomalous .	norel	139..190
It has an unusual kind of independence from elected officials	restatement	191..252
and still has authority over one of the most powerful of government 's instruments -- the control of the money supply .	contrast	253..370
Thus we have a condition that is easily described as undemocratic .	cause	371..437
Second , the responsibilities of the Federal Reserve as guardian of the currency , which means as guardian of the stability of the price level , sometimes lead it to take measures that are unpopular .	entrel	438..634
As former Fed Chairman William McChesney Martin used to say , they would have to take the punch bowl away just as the party is getting interesting .	restatement	635..781
So the Federal Reserve is an attractive target for complaint by politicians .	norel	784..860
The Fed is easily assigned the blame for unpleasantness , like high interest rates or slow economic growth	cause	861..966
while the politicians can escape responsibility by pointing to the Fed 's independence .	conjunction	967..1054
This leads to proposals for " reform " of the Fed , which have the common feature of making the Fed more responsive to the administration , to the Congress and to public opinion -- without , however , any assumption of additional responsibility by the politicians .	norel	1057..1315
These proposals include changing the term of the chairman , shortening the terms of the members , eliminating the presidents of the Federal Reserve Banks from the decision-making process , putting the Secretary of the Treasury on the Federal Reserve Board , having the Fed audited by an arm of Congress ( the General Accounting Office ) , putting the Fed 's expenditures in the budget , and requiring prompt publication of the Fed 's minutes .	restatement	1316..1748
Some of these ideas are again under consideration in Congress .	entrel	1749..1811
But these proposals do not rest on a view of what the Fed 's problem is	norel	1814..1884
or , if they do , they rest on an incorrect view .	alternative	1885..1932
They would not solve the problem	cause	1933..1965
they would make it worse .	alternative	1966..1992
The problem is not that the Fed is too unresponsive to the public interest .	entrel	1993..2068
On the contrary , it is too responsive to an incorrect view of the public interest .	contrast	2069..2151
The price level in the U.S. is now about 4 1/4 times as high as it was 30 years ago .	norel	2154..2238
On average , something that cost $ 100 30 years ago now costs $ 425 .	restatement	2239..2304
Or , a wage that was $ 100 30 years ago would buy only $ 23.53 worth of stuff today .	restatement	2305..2386
On two occasions the inflation rate rose to more than 10 % a year .	entrel	2387..2452
In each case the ending of this unsustainable inflation caused a severe recession -- the two worst of the postwar period .	entrel	2453..2574
The enormous inflation over the past 30 years was largely due to monetary policy .	norel	2577..2658
At least , it would not have happened without the support of monetary policy that provided for a 10-fold increase in the money supply during the same period .	restatement	2659..2815
And that increase in the money supply would not have happened without the consent of the Federal Reserve .	conjunction	2816..2921
The basic problem of monetary policy , to which reform of the Fed should be addressed , is to prevent a recurrence of this experience .	norel	2924..3056
There were two general reasons for the mistaken monetary policy of the past 30 years :	norel	3059..3144
1 . To some extent the Federal Reserve shared the popular but incorrect view that expansionary monetary policy could yield a net improvement in employment and output .	norel	3147..3312
2 . Even where the Fed did not share this view it felt the need to accommodate to it .	norel	3315..3399
Despite all the formal provisions for its independence , the Fed seems constantly to feel that if it uses its independence too freely it will lose it .	cause	3400..3549
The common proposals for reforming the Fed would only make the situation worse	norel	3552..3630
if they had any effect at all .	condition	3631..3662
Putting the Secretary of the Treasury on the Board of Governors , one of the leading proposals today , is an example .	instantiation	3663..3778
The secretary is the world 's biggest borrower of money .	cause	3779..3834
He has a built-in , constant longing for lower interest rates .	cause	3835..3896
Moreover , he is a political agent of a political president , who naturally gives extraordinary weight to the way the economy will perform before the next election , and less to its longer-run health .	norel	3897..4094
These days , the secretary suffers the further disqualification that he is a member of a club of seven finance ministers who meet occasionally to decide what exchange rates should be , which is a diversion from the real business of the Federal Reserve to stabilize the price level .	conjunction	4095..4374
How should a reasonable member of the Federal Reserve Board interpret a congressional decision to put the secretary on the board ?	norel	4377..4506
Could he plausibly interpret it as encouragement for the Fed to give primary emphasis to stabilizing the price level ?	norel	4507..4624
Or would he interpret it as instruction to give more weight to these other objectives that the secretary represents -- low interest rates , short-run economic expansion , and stabilization of exchange rates at internationally managed levels ?	alternative	4625..4864
The answer seems perfectly clear .	norel	4865..4898
( True , a succession of Fed chairmen has given color to the notion that the Secretary of the Treasury belongs on the Fed .	norel	4901..5021
By their constant readiness to advise all and sundry about federal budgetary matters the chairmen have encouraged the belief that fiscal policy and monetary policy are ingredients of a common stew , in which case it is natural that the Fed and the Treasury , and probably also the Congress , should be jointly engaged in stirring the pot .	cause	5022..5357
The Fed 's case for its own independence would be a little stronger if it were more solicitous of the independence of the rest of the government . )	cause	5358..5503
The Fed 's problem is not that it is too independent , or too unpolitical .	norel	5506..5578
The Fed is responsive to , and can not help being responsive to , the more overtly political part of the government .	restatement	5579..5692
The Fed exercises a power given to it by Congress and the president .	cause	5693..5761
But Congress and the president accept no responsibility for the exercise of the power they have given the Fed .	contrast	5762..5872
Critics of the present arrangement are correct to say that it is undemocratic .	norel	5875..5953
What is undemocratic is the unwillingness of the more political parts of the government to take the responsibility for deciding the basic question of monetary policy , which is what priority should be given to stabilizing the price level .	restatement	5954..6191
To leave this decision to an " independent " agency is not only undemocratic .	contrast	6192..6267
It also prevents the conduct of a policy that has a long-term rationale	conjunction	6268..6339
because it leaves the Fed guessing about what are the expectations of its masters , the politicians , who have never had to consider the long-term consequences of monetary policy .	cause	6340..6518
The greatest contribution Congress could make at this time would be to declare that stabilizing the price level is the primary responsibility of the Federal Reserve System .	norel	6521..6693
Legislation to this effect has been introduced in Congress in this session by Rep. Stephen Neal ( D. , N.C . ) .	conjunction	6694..6801
It is not the kind of thing that is likely to be enacted , however .	contrast	6802..6868
Congress would be required to make a hard decision	cause	6869..6919
and Congress would much prefer to leave the hard decision to the Fed and retain its rights of complaint after the fact .	contrast	6920..7040
People will say that the nation and the government have other objectives , in addition to stabilizing the price level , which is true .	norel	7043..7175
But that is not the same as saying that the Federal Reserve has other objectives .	contrast	7176..7257
The government has other agencies and instruments for pursuing these other objectives .	cause	7258..7344
But it has only the Fed to pursue price-level stability .	contrast	7345..7401
And the Fed has at most very limited ability to contribute to the achievement of other objectives by means other than by stabilizing the price level .	conjunction	7402..7551
The two objectives most commonly thought to be legitimate competitors for the attention of the Fed are high employment and rapid real growth .	norel	7554..7695
But the main lesson of economic policy in the past 30 years is that if the Fed compromises with the price-stability objective in the pursuit of these other goals , the result is not high employment and rapid growth but is inflation .	concession	7696..7927
A former chairman of the president 's Council of Economic Advisers , Mr. Stein is an American Enterprise Institute fellow .	norel	7930..8050
