British Aerospace PLC and France 's Thomson-CSF S.A. said	(NS(NS(NS(NS(NS(NS(SN
they are nearing an agreement	(NS(NS
to merge their guided-missile divisions ,	NS)
greatly expanding collaboration between the two defense contractors .	NS))
The 50-50 joint venture ,	(NN(NN(NS
which may be dubbed Eurodynamics ,	NS)
would have combined annual sales of at least # 1.4 billion	(NS
( $ 2.17 billion )	NS))
and would be among the world 's largest missile makers .	NN))
After two years of talks , plans for the venture are sufficiently advanced	(NS(SN
for the companies to seek French and British government clearance .	SN)
The companies hope for a final agreement by year-end .	NS))
The venture would strengthen the rapidly growing ties between the two companies ,	(NS(NS(NS(NN
and help make them a leading force in European defense contracting .	NN)
In recent months , a string of cross-border mergers and joint ventures have reshaped the once-balkanized world of European arms manufacture .	NS)
Already , British Aerospace and French government-controlled Thomson-CSF collaborate on a British missile contract and on an air-traffic control radar system .	(NS(NN
Just last week they announced	(SN
they may make a joint bid	(NS
to buy Ferranti International Signal PLC , a smaller British defense contractor	(NS
rocked by alleged accounting fraud at a U.S. unit .	NS))))
The sudden romance of British Aerospace and Thomson-CSF	(NN(NS
-- traditionally bitter competitors for Middle East and Third World weapons contracts --	NS)
is stirring controversy in Western Europe 's defense industry .	NN)))
Most threatened by closer British Aerospace-Thomson ties would be their respective national rivals ,	(NS(SN(NS
including Matra S.A. in France and Britain 's General Electric Co. PLC .	NS)
But neither Matra nor GEC	(NN(NS
-- unrelated to Stamford , Conn.-based General Electric Co. --	NS)
are sitting quietly by	(NS
as their competitors join forces .	NS)))
Yesterday , a source close to GEC confirmed	(NS(NS(NS(NS(NS(SN
that his company may join the Ferranti fight , as part of a possible consortium	(NS
that would bid against British Aerospace and Thomson-CSF .	NS))
Companies	(NN(NS
with which GEC has had talks about a possible joint Ferranti bid	NS)
include Matra , Britain 's Dowty Group PLC , West Germany 's Daimler-Benz AG , and France 's Dassault group .	NN))
But it may be weeks	(NS(NS(SN
before GEC and its potential partners decide whether to bid ,	SN)
the source indicated .	NS)
GEC plans first to study Ferranti 's financial accounts ,	(NS
which	(NN(NS
auditors recently said	NS)
included # 215 million in fictitious contracts at a U.S. unit , International Signal & Control Group ,	(NS
with which Ferranti merged last year .	NS)))))
Also , any GEC bid might be blocked by British antitrust regulators ;	(NN(NS
Ferranti is GEC 's main competitor on several key defense-electronics contracts ,	(SN
and its purchase by GEC may heighten British Defense Ministry worries about concentration in the country 's defense industry .	SN))
A consortium bid , however , would diminish GEC 's direct role in Ferranti	(NS
and might consequently appease ministry officials .	NS)))
A British Aerospace spokeswoman appeared unperturbed by the prospect of a fight with GEC for Ferranti :	(NS
`` Competition is the name of the game , ''	(NS
she said .	NS)))
At least one potential GEC partner , Matra , insists	(NS(SN
it is n't interested in Ferranti .	SN)
`` We have nothing to say about this affair ,	(NS(NS
which does n't concern us , ''	NS)
a Matra official said Sunday .	NS))))))
The missile venture ,	(NS(NS(NN(NS
the British Aerospace spokeswoman said ,	NS)
is a needed response to the `` new environment '' in defense contracting .	NN)
For both Thomson and British Aerospace , earnings in their home markets have come under pressure from increasingly tight-fisted defense ministries ;	(NN
and Middle East sales , a traditional mainstay for both companies ' exports , have been hurt by five years of weak oil prices .	NN))
The venture 's importance for Thomson is great .	(NS(NS(NS
Thomson feels	(SN
the future of its defense business depends on building cooperation with other Europeans .	SN))
The European defense industry is consolidating ;	(SN(NS
for instance , West Germany 's Siemens AG recently joined GEC in a takeover of Britain 's Plessey Co. ,	(NN
and Daimler-Benz agreed to buy Messerschmitt-Boelkow Blohm G.m.b. H .	NN))
In missiles , Thomson is already overshadowed by British Aerospace and by its home rival , France 's Aerospatiale S.A. ;	(SN
to better compete ,	(NS(NS
Thomson officials say ,	NS)
they need a partnership .	NS))))
To justify 50-50 ownership of the planned venture ,	(SN
Thomson would make a cash payment to British Aerospace .	SN))))
Annual revenue of British Aerospace 's missile business is about # 950 million ,	(NS
a Thomson spokesman said .	NS))
British Aerospace 's chief missile products include its 17-year-old family of Rapier surface-to-air missiles .	(NN
Thomson missile products , with about half British Aerospace 's annual revenue , include the Crotale surface-to-air missile family .	NN))
