Earnings for most of the nation 's major pharmaceutical makers are believed to have moved ahead briskly in the third quarter ,	(elaboration-additional(explanation-argumentative(example(antithesis(elaboration-additional
as companies with newer , big-selling prescription drugs fared especially well .	elaboration-additional)
For the third consecutive quarter , however , most of the companies ' revenues were battered by adverse foreign-currency translations	(result
as a result of the strong dollar abroad .	result))
Analysts said	(comparison(attribution
that Merck & Co. , Eli Lilly & Co. , Warner-Lambert Co. and the Squibb Corp. unit of Bristol-Myers Squibb Co. all benefited from strong sales of relatively new , higher-priced medicines	(elaboration-object-attribute
that provide wide profit margins .	elaboration-object-attribute))
Less robust earnings at Pfizer Inc. and Upjohn Co. were attributed to those companies ' older products ,	(elaboration-object-attribute
many of which face stiffening competition from generic drugs and other medicines .	elaboration-object-attribute)))
Joseph Riccardo , an analyst with Bear , Stearns & Co. , said	(consequence(attribution
that over the past few years most drug makers have shed their slow-growing businesses	(List
and instituted other cost savings ,	(example
such as consolidating manufacturing plants and administrative staffs .	example)))
As a result , `` major new products are having significant impact , even on a company with very large revenues , ''	(attribution
Mr. Riccardo said .	attribution)))
Analysts said	(elaboration-set-member(interpretation(attribution
profit for the dozen or so big drug makers , as a group , is estimated to have climbed between 11 % and 14 % .	attribution)
While that 's not spectacular ,	(antithesis
Neil Sweig , an analyst with Prudential Bache , said	(attribution
that the rate of growth will `` look especially good	(condition(comparison
as compared to other companies	comparison)
if the economy turns downward . ''	condition))))
Mr. Sweig estimated	(List(antithesis(elaboration-additional(elaboration-additional(attribution
that Merck 's profit for the quarter rose by about 22 % ,	(explanation-argumentative
propelled by sales of its line-up of fast-growing prescription drugs ,	(elaboration-set-member
including its anti-cholesterol drug , Mevacor ; a high blood pressure medicine , Vasotec ; Primaxin , an antibiotic , and Pepcid , an anti-ulcer medication .	elaboration-set-member)))
Profit climbed	(attribution(result(concession
even though Merck 's sales were reduced by `` one to three percentage points ''	concession)
as a result of the strong dollar ,	result)
Mr. Sweig said .	attribution))
In the third quarter of 1988 , Merck earned $ 311.8 million , or 79 cents a share .	elaboration-additional)
In Rahway , N.J. , a Merck spokesman said	(attribution
the company does n't make earnings projections .	attribution))
Mr. Sweig said	(List(antithesis(elaboration-additional(evaluation(elaboration-additional(attribution
he estimated	(attribution
that Lilly 's earnings for the quarter jumped about 20 % ,	(result
largely because of the performance of its new anti-depressant Prozac .	result)))
The drug ,	(Same-Unit(elaboration-additional
introduced last year ,	elaboration-additional)
is expected to generate sales of about $ 300 million this year .	Same-Unit))
`` It 's turning out to be a real blockbuster , ''	(attribution
Mr. Sweig said .	attribution))
In last year 's third quarter , Lilly earned $ 171.4 million , or $ 1.20 a share .	elaboration-additional)
In Indianapolis , Lilly declined comment .	antithesis)
Several analysts said	(List(elaboration-additional(attribution
they expected Warner-Lambert 's profit also to increase by more than 20 % from $ 87.7 million , or $ 1.25 a share ,	(elaboration-object-attribute
it reported in the like period last year .	elaboration-object-attribute))
The company is praised by analysts	(elaboration-additional(reason
for sharply lowering its costs in recent years	(List
and shedding numerous companies with low profit margins .	List))
The company 's lean operation ,	(elaboration-additional(Same-Unit(attribution
analysts said ,	attribution)
allowed sharp-rising sales from its cholesterol drug , Lopid ,	(consequence
to power earnings growth .	consequence))
Lopid sales are expected to be about $ 300 million this year , up from $ 190 million in 1988 .	(elaboration-additional
In Morris Plains , N.J. , a spokesman for the company said	(attribution
the analysts ' projections are `` in the ballpark . ''	attribution)))))
Squibb 's profit ,	(List(elaboration-additional(elaboration-additional(Same-Unit(elaboration-additional
estimated by analysts to be about 18 % above the $ 123 million , or $ 1.25 a share ,	(elaboration-object-attribute
it earned in the third quarter of 1988 ,	elaboration-object-attribute))
was the result of especially strong sales of its Capoten drug	(elaboration-object-attribute
for treating high blood pressure and other heart disease .	elaboration-object-attribute))
The company was officially merged with Bristol-Myers Co. earlier this month .	elaboration-additional)
Bristol-Myers declined to comment .	elaboration-additional)
Mr. Riccardo of Bear Stearns said	(List(elaboration-additional(elaboration-additional(attribution
that Schering-Plough Corp. 's expected profit rise of about 18 % to 20 % , and Bristol-Meyers 's expected profit increase of about 13 % are largely because `` those companies are really managed well . ''	attribution)
ScheringPlough earned $ 94.4 million , or 84 cents a share ,	(Comparison
while Bristol-Myers earned $ 232.3 million , or 81 cents a share , in the like period a year earlier .	Comparison))
In Madison , N.J. , a spokesman for Schering-Plough said	(explanation-argumentative(attribution
the company has `` no problems '' with the average estimate by a analysts	(elaboration-object-attribute
that third-quarter earnings per share rose by about 19 % , to $ 1 .	elaboration-object-attribute))
The company expects to achieve the 20 % increase in full-year earnings per share ,	(attribution(comparison
as it projected in the spring ,	comparison)
the spokesman said .	attribution)))
Meanwhile , analysts said	(List(elaboration-additional(elaboration-additional(elaboration-additional(example(attribution
Pfizer 's recent string of lackluster quarterly performances continued ,	(elaboration-additional
as earnings in the quarter were expected to decline by about 5 % .	elaboration-additional))
Sales of Pfizer 's important drugs , Feldene	(explanation-argumentative(Same-Unit(elaboration-additional
for treating arthritis ,	elaboration-additional)
and Procardia , a heart medicine , have shrunk	Same-Unit)
because of increased competition .	explanation-argumentative))
`` The ( strong ) dollar hurt Pfizer a lot , too , ''	(attribution
Mr. Sweig said .	attribution))
In the third quarter last year , Pfizer earned $ 216.8 million , or $ 1.29 a share .	elaboration-additional)
In New York , the company declined comment .	elaboration-additional)
Analysts said	(elaboration-additional(explanation-argumentative(attribution
they expected Upjohn 's profit to be flat or rise by only about 2 % to 4 %	(comparison
as compared with $ 89.6 million , or 49 cents a share ,	(elaboration-object-attribute
it earned a year ago .	elaboration-object-attribute)))
Upjohn 's biggest-selling drugs are Xanax , a tranquilizer , and Halcion , a sedative .	(List(elaboration-additional
Sales of both drugs have been hurt by new state laws	(Same-Unit(elaboration-object-attribute
restricting the prescriptions of certain tranquilizing medicines	elaboration-object-attribute)
and adverse publicity about the excessive use of the drugs .	Same-Unit))
Also , the company 's hair-growing drug , Rogaine , is selling well	(attribution(concession(elaboration-additional
-- at about $ 125 million for the year ,	elaboration-additional)
but the company 's profit from the drug has been reduced by Upjohn 's expensive print and television campaigns for advertising ,	concession)
analysts said .	attribution)))
In Kalamazoo , Mich. , Upjohn declined comment .	elaboration-additional)))))))))
