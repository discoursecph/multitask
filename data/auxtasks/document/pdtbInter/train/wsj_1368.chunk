William D. Forrester, president of the U.S.-U.S.S.R. Trade and Economic Council, has a warning for U.S. companies trying to do business in the Soviet Union.	O	9..165
"It's an extremely complex market, and you have to be prepared to make a big commitment," Mr. Forrester says.	O	166..275
"We are not trying to encourage everyone."	O	276..318
Undeterred by such words of caution, corporate America is flocking to Moscow, lured by a huge untapped market and Mikhail Gorbachev's attempt to overhaul the Soviet economy.	B-conjunction	321..494
Doing business with the Russians, once the pursuit of a handful of hardened veterans, has become the goal of such major companies as General Motors Corp., Federal Express Corp. and Procter & Gamble Co., as well as a cluster of smaller firms.	B-conjunction	495..736
Reflecting the new-found interest, more than 140 U.S. companies are taking part in a Moscow exhibition organized by Mr. Forrester's trade group.	I-conjunction	737..881
But while U.S. interest may be big and growing, the difficulties that have stymied deals in the past show no sign of abating.	B-instantiation	884..1009
Alongside the old problems of a non-convertible currency and an inpenetrable bureaucracy, Western business executives must now grapple with new complexities linked to perestroika, the restructuring of the Soviet economy.	I-instantiation	1010..1230
Executives say Mr. Gorbachev's moves to break up the government's foreign trade monopoly have created uncertainties as well as opportunities.	O	1233..1374
Changing legislation has opened the field to thousands of inexperienced Soviet players, many who promise more than they can deliver.	B-conjunction	1375..1507
And some foreign firms are finding that even when they manage to overcome such hurdles, their ventures now have to be endorsed by such unpredictable bodies as the Soviet parliament and the governments of the nation's republics.	I-conjunction	1508..1735
You have to go out to all your constituents," says James H. Giffen, who is spearheading the most ambitious attempt by U.S. firms to break into the Soviet market, involving investment of more than $5 billion in some two dozen joint ventures.	B-entrel	1738..1979
As part of that attempt, by the American Trade Consortium, Mr. Giffen says he spends a lot of time lobbying.	I-entrel	1980..2088
Growing public fears about the Soviet environment is one new factor affecting some joint-venture plans.	B-restatement	2091..2194
Over the past two years, Soviet ministries have been talking to international firms, including Occidental Petroleum Co. and Combustion Engineering Inc. of the U.S., Montedison S.p.A. of Italy and several Japanese groups, about jointly building and operating several big petrochemical plants	B-contrast	2195..2486
The plans have come under fire from Soviet environmentalists, and officials say many are likely to be scaled back or dropped.	I-contrast	2487..2612
Whatever the difficulties, Mr. Gorbachev remains committed to increasing foreign trade	B-conjunction	2615..2702
For political as well as economic reasons, U.S. companies are at the top of his priorities -- a point he underscored by spending two hours walking around the U.S. trade show last week.	I-conjunction	2703..2887
Talking to a small group of U.S. executives afterwards, Mr. Gorbachev appeared impatient for a big expansion in U.S.-Soviet trade, which now amounts to a meager $3 billion annually	B-conjunction	2890..3071
The U.S. ranks fourth of countries that have concluded joint ventures, behind West Germany, Finland and Italy.	I-conjunction	3072..3182
According to several people present at the meeting, Mr. Gorbachev also supported the idea of concluding several commercial accords with the U.S., possibly at his next summit meeting with President Bush.	O	3183..3385
Judging by the crush at the exhibition, deprived Soviet consumers are more than ready for U.S. products	B-instantiation	3388..3492
Hundreds of people lined up every day at the Colgate-Palmolive Co. stand to receive a free tube of toothpaste, a commodity in chronically short supply here	B-list	3493..3649
And unruly crowds at RJR Nabisco Inc. 's booth almost knocked over a glass showcase in the rush to get a free Camel cigarette sticker.	I-list	3650..3784
Some U.S. products are filtering into the Soviet market under an emergency import program	B-instantiation	3787..3877
Both Colgate and Procter & Gamble have received big orders for toothpaste, soap and detergents.	I-instantiation	3878..3973
The American Trade Consortium says it is planning to ship some $500 million of consumer goods, financed by bank credits, in the first few months of next year.	O	3974..4132
But the current Soviet purchasing spree may be a one-time affair	B-cause	4135..4200
The goal of most U.S. firms -- joint ventures -- remains elusive	B-contrast	4201..4266
Because the Soviet ruble isn't convertible into dollars, marks and other Western currencies, companies that hope to set up production facilities here must either export some of the goods to earn hard currency or find Soviet goods they can take in a counter-trade transaction.	I-contrast	4267..4542
International competition for the few Soviet goods that can be sold on world markets is heating up, however.	O	4545..4653
Shelley M. Zeiger, an entrepreneur from New Jersey who buys Soviet porcelain and "matryoshka" nesting dolls for export to the U.S., says West German companies already have snapped up much of the production of these items.	O	4654..4875
Seeking to overcome the currency problems, Mr. Giffen's American Trade Consortium, which comprises Chevron Corp., RJR, Johnson & Johnson, Eastman Kodak Co., and Archer-Daniels-Midland Co., has concocted an elaborate scheme to share out dollar earnings, largely from the revenues of a planned Chevron oil project	B-conjunction	4878..5190
Several medical concerns, including Pfizer Inc., Hewlett-Packard Co., Colgate and Abbott Laboratories intend to pursue a similar consortium approach.	I-conjunction	5191..5340
"It's hard to invest capital here on the same basis as investing in other countries," says Dennis A. Sokol, president of Medical Service Partners Inc., who is putting the medical consortium together.	O	5343..5542
Some U.S. entrepreneurs operate on a smaller scale	B-instantiation	5545..5596
One group seeks to publish a U.S.-Soviet medical journal in conjunction with the U.S.S.R. Ministry of Health	B-conjunction	5597..5706
According to Richard P. Mills, a Boston-based official of the U.S. partner, 10,000 copies of the quarterly will be printed in Russian from next year	B-entrel	5707..5856
It will be financed by advertisements from U.S. companies and by simultaneous publication of an English-language journal containing details of Soviet medical advancements.	I-entrel	5857..6028
"We found a market niche," Mr. Mills boasts.	O	6031..6075
"It's truly entrepreneurial.	O	6076..6104
