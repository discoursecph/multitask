Secretary of State Baker , we read , decided to kill a speech that Robert Gates , deputy national security adviser and a career Soviet expert , was going to give to a student colloquium , the National Collegiate Security Conference .	root	9..236
We keep wondering what Mr. Gates wanted to say .	contrast	237..284
Perhaps he might have cited Mr. Gorbachev 's need for " a stable currency , free and competitive markets , private property and real prices " and other pie-in-the-sky reforms .	norel	287..457
Perhaps he 'd have called for " a decentralized political and economic system " without a dominant communist party .	alternative	458..570
Or political arrangements " to alleviate the grievances and demands of Soviet ethnic minorities and republics . "	norel	571..681
Why , a Bob Gates might even have said , " Nor are Soviet problems susceptible to rescue from abroad through abundant Western credits . "	norel	682..814
If Mr. Gates had been allowed to say these things , we would now be hearing about " discord " and " disarray " on foreign policy .	norel	817..941
Dark hints would be raised that parts of the administration hope Mr. Gorbachev would fail , just as they were when Vice President Quayle voiced similar sentiments .	conjunction	942..1104
It 's somehow OK for Secretary Baker himself , however , to say all the same things .	norel	1105..1186
In fact , he did	conjunction	1187..1202
the quotes above are from Mr. Baker 's speech of two weeks ago .	restatement	1203..1266
So far as we can see , there is no disagreement among Mr. Baker , Mr. Quayle , the Mr. Gates we 've read , or for that matter President Bush .	norel	1269..1405
They all understand point one : Nothing the U.S. can do will make much difference in whether Mr. Gorbachev succeeds with perestroika .	restatement	1406..1538
Perhaps Mr. Gates would emphasize more than Mr. Baker the many hurdles the Soviet leader must leap if he is going to succeed .	contrast	1539..1664
But everyone agrees that Mr. Gorbachev 's problems result from the failure of his own system .	contrast	1665..1757
They can be relieved only by changing that system , not by pouring Western money into it .	cause	1758..1846
GATT membership will not matter to Donbas coal miners short of soap	restatement	1847..1914
nor will a START treaty make any difference to Ukrainian nationalists .	conjunction	1915..1986
On the other hand , so long as Mr. Gorbachev is easing his grip on his empire , everyone we 've heard agrees that the U.S. can benefit by engaging him .	norel	1989..2137
If a deal can be made to cut the world 's Ortegas loose from Moscow , why not ?	cause	2138..2214
We do n't expect much good from nuclear-arms control	conjunction	2215..2266
but conventional-arms talks might demilitarize Eastern Europe .	contrast	2267..2330
There 's nothing in the least contradictory in all this	norel	2333..2387
and it would be nice to think that Washington could tolerate a reasonably sophisticated , complex view .	conjunction	2388..2491
Yet much of the political culture seems intent on castigating the Bush administration for not " helping " Mr. Gorbachev .	concession	2492..2610
So every time a Bush official raises a doubt about Mr. Gorbachev , the Washington community shouts " Cold War " and " timidity , " and an administration spokesman is quickly trotted out to reply , " Mr. Bush wants perestroika to succeed . "	cause	2611..2841
Mr. Baker seems especially sensitive to the Washington ailment known as Beltway-itis .	norel	2844..2929
Its symptoms include a cold sweat at the sound of debate , clammy hands in the face of congressional criticism , and fainting spells when someone writes the word " controversy . "	entrel	2930..3104
As one unidentified official clearly in the late stages of the disease told the Times : " Baker just felt that there were some lines in the speech that could be misinterpreted and seized by the press . "	entrel	3105..3304
In short , the problem is not intra-administration disagreement , but preoccupation with the prospect that perestrokia might fail , and its political opponents will ask " Who lost Gorbachev ? "	restatement	3305..3492
Mr. Baker may want to avoid criticism from Senate Majority Leader George Mitchell	norel	3495..3576
but as Secretary of State his audience is the entire Free World , not just Congress .	contrast	3577..3661
In any case , he 's likely to find that the more he muzzles his colleagues , the more leaks will pop up all around Washington , a lesson once learned by Henry Kissinger .	conjunction	3662..3827
Letting officials express their own nuances can be educational .	conjunction	3828..3891
We note that in Rome yesterday Defense Secretary Cheney said that European euphoria over Mr. Gorbachev is starting to be tempered by a recognition of " the magnitude of the problems he was trying to deal with . "	instantiation	3892..4101
It is in the Western interest to see Mr. Gorbachev succeed .	norel	4104..4163
The odds are against him , as he himself would no doubt tell you .	contrast	4164..4228
The ultimate outcome depends on what he does , not on what we do .	entrel	4229..4293
Even if the press is ready to seize and misinterpret , these are not very complicated thoughts .	entrel	4294..4388
Surely there is someone in the administration , maybe Bob Gates , who could explain them to college students , or even schoolchildren .	cause	4389..4520
