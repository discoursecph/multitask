When Warren Winiarski , proprietor of Stag 's Leap Wine Cellars in Napa Valley , announced a $ 75 price tag for his 1985 Cask 23 Cabernet this fall , few wine shops and restaurants around the country balked .	root	9..211
" This is the peak of my wine-making experience , " Mr. Winiarski declared when he introduced the wine at a dinner in New York , " and I wanted to single it out as such . "	cause	212..377
It is in my estimation the best wine Stag 's Leap has produced	norel	380..441
and with fewer than 700 cases available , it is sure to sell quickly .	conjunction	442..511
The price is a new high for California Cabernet Sauvignon	conjunction	512..569
but it is not the highest .	contrast	570..597
Diamond Creek 1985 Lake Vineyard Cabernet weighed in this fall with a sticker price of $ 100 a bottle .	pragmatic cause	598..699
One of the fastest growing segments of the wine market is the category of superpremiums -- wines limited in production , of exceptional quality ( or so perceived , at any rate ) , and with exceedingly high prices .	norel	702..910
For years , this group included a stable of classics -- Bordeaux first growths ( Lafite-Rothschild , Latour , Haut-Brion , Petrus ) , Grand Cru Burgundies ( Romanee-Conti and La Tache ) deluxe Champagnes ( Dom Perignon or Roederer Cristal ) , rarefied sweet wines ( Chateau Yquem or Trockenbeerenauslesen Rieslings from Germany , and Biondi-Santi Brunello Riserva from Tuscany ) .	entrel	911..1275
These first magnitude wines ranged in price from $ 40 to $ 125 a bottle .	entrel	1276..1346
In the last year or so , however , this exclusive club has taken in a host of flashy new members .	norel	1349..1444
The classics have zoomed in price to meet the competition	conjunction	1445..1502
and it almost seems that there 's a race on to come up with the priciest single bottle , among current releases from every major wine region on the globe .	conjunction	1503..1656
France can boast the lion 's share of high-priced bottles .	norel	1659..1716
Bordeaux 's first growths from 1985 and 1986 are $ 60 to $ 80 each ( except for the smallest in terms of production , Chateau Petrus , which costs around $ 250 ! ) .	instantiation	1717..1872
These prices seem rather modest , however , in light of other French wines from current vintages .	contrast	1873..1968
Chateau Yquem , the leading Sauternes , now goes for well over $ 100 a bottle for a lighter vintage like 1984	restatement	1969..2075
the spectacularly rich 1983 runs $ 179 .	conjunction	2076..2115
In Champagne , some of the prestige cuvees are inching toward $ 100 a bottle .	norel	2118..2193
The first Champagne to crack that price barrier was the 1979 Salon de Mesnil Blanc de Blancs .	entrel	2194..2287
The '82 Salon is $ 115 .	conjunction	2288..2310
Roederer Cristal at $ 90 a bottle sells out around the country	conjunction	2311..2372
and Taittinger 's Comtes de Champagne Blanc de Blancs is encroaching upon that level .	conjunction	2373..2457
The great reds of the Rhone Valley have soared in price as well .	norel	2458..2522
E. Guigal 's 1982 Cote Rotie La Landonne , for example , is $ 120 .	instantiation	2523..2585
None of France 's wine regions can steal a march on Burgundy , however .	contrast	2588..2657
The six wines of the Domaine de la Romanee-Conti , 72 of the most precious acres of vineyard anywhere in the world , have commanded three-digit price tags for several years now .	restatement	2658..2833
With the 1985 vintage , they soared higher : La Tache , $ 195 ; Richebourg , $ 180 ; Romanee-Conti , $ 225 .	conjunction	2834..2931
Another small Burgundy estate , Coche-Dury , has just offered its 1987 Corton-Charlemagne for $ 155 .	conjunction	2932..3029
From Italy there is Angelo Gaja Barbaresco at $ 125 a bottle , Piero Antinori 's La Solaia , a $ 90 Cabernet from Tuscany , and Biondi-Santi Brunello at $ 98 .	norel	3032..3183
Spain 's Vega Secilia Unico 1979 ( released only in its 10th year ) is $ 70 , as is Australia 's Grange Hermitage 1982 .	conjunction	3184..3297
" There are certain cult wines that can command these higher prices , " says Larry Shapiro of Marty 's , one of the largest wine shops in Dallas .	norel	3300..3440
" What 's different is that it is happening with young wines just coming out .	contrast	3441..3516
We 're seeing it partly because older vintages are growing more scarce . "	norel	3517..3588
Wine auctions have almost exhausted the limited supply of those wines , Mr. Shapiro continued : " We 've seen a dramatic decrease in demand for wines from the '40s and '50s , which go for $ 300 to $ 400 a bottle .	norel	3591..3796
Some of the newer wines , even at $ 90 to $ 100 a bottle or so , almost offer a bargain . "	contrast	3797..3882
Take Lake Vineyard Cabernet from Diamond Creek .	norel	3885..3932
It 's made only in years when the grapes ripen perfectly ( the last was 1979 ) and comes from a single acre of grapes that yielded a mere 75 cases in 1987 .	entrel	3933..4085
Owner Al Brownstein originally planned to sell it for $ 60 a bottle	entrel	4086..4152
but when a retailer in Southern California asked , " Is that wholesale or retail ? " he re-thought the matter .	contrast	4153..4260
Offering the wine at roughly $ 65 a bottle wholesale ( $ 100 retail ) , he sent merchants around the country a form asking them to check one of three answers : 1 ) no , the wine is too high ( 2 responses ) ; 2 ) yes , it 's high but I 'll take it ( 2 responses ) ; 3 ) I 'll take all I can get ( 58 responses ) .	asynchronous	4261..4550
The wine was shipped in six-bottle cases instead of the usual 12 , but even at that it was spread thin , going to 62 retailers in 28 states .	asynchronous	4551..4689
" We thought it was awfully expensive , " said Sterling Pratt , wine director at Schaefer 's in Skokie , Ill. , one of the top stores in suburban Chicago , " but there are people out there with very different opinions of value .	norel	4692..4910
We got our two six-packs -- and they 're gone . "	cause	4911..4957
Mr. Pratt remarked that he thinks steeper prices have come about because producers do n't like to see a hit wine dramatically increase in price later on .	norel	4960..5112
Even if there is consumer resistance at first , a wine that wins high ratings from the critics will eventually move .	cause	5113..5228
" There may be sticker-shock reaction initially , " said Mr. Pratt , " but as the wine is talked about and starts to sell , they eventually get excited and decide it 's worth the astronomical price to add it to their collection . "	restatement	5229..5451
" It 's just sort of a one-upsmanship thing with some people , " added Larry Shapiro .	norel	5454..5535
" They like to talk about having the new Red Rock Terrace { one of Diamond Creek 's Cabernets } or the Dunn 1985 Cabernet , or the Petrus .	restatement	5536..5669
Producers have seen this market opening up and they 're now creating wines that appeal to these people . "	cause	5670..5773
That explains why the number of these wines is expanding so rapidly .	norel	5776..5844
But consumers who buy at this level are also more knowledgeable than they were a few years ago .	pragmatic contrast	5845..5940
" They wo n't buy if the quality is not there , " said Cedric Martin of Martin Wine Cellar in New Orleans .	cause	5941..6043
" Or if they feel the wine is overpriced and they can get something equally good for less . "	alternative	6044..6134
Mr. Martin has increased prices on some wines ( like Grgich Hills Chardonnay , now $ 32 ) just to slow down movement	entrel	6135..6247
but he is beginning to see some resistance to high-priced red Burgundies and Cabernets and Chardonnays in the $ 30 to $ 40 range .	contrast	6248..6376
Image has , of course , a great deal to do with what sells and what does n't	norel	6379..6452
and it ca n't be forced .	conjunction	6453..6477
Wine merchants ca n't keep Roederer Cristal in stock , but they have to push Salon le Mesnil , even lowering the price from $ 115 to $ 90 .	instantiation	6478..6611
It 's hardly a question of quality -- the 1982 Salon is a beautiful wine , but , as Mr. Pratt noted , people have their own ideas about value .	concession	6612..6750
It 's interesting to find that a lot of the expensive wines are n't always walking out the door .	norel	6753..6847
In every major market in the U.S. , for instance , you can buy '86 La Tache or Richebourg , virtually all of the first growth Bordeaux ( except Petrus ) , as well as Opus One and Dominus from California and , at the moment , the Stag 's Leap 1985 Cask 23 .	instantiation	6848..7094
With the biggest wine-buying period of the year looming as the holidays approach , it will be interesting to see how the superpremiums fare .	norel	7097..7236
By January it should be fairly clear what 's hot -- and what 's not .	restatement	7237..7303
Ms. Ensrud is a free-lance wine writer in New York .	norel	7306..7357
