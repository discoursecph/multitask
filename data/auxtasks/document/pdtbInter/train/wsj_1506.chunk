The U.S. is required to notify foreign dictators if it knows of coup plans likely to endanger their lives, government officials said.	O	9..142
The notification policy was part of a set of guidelines on handling coups outlined in a secret 1988 exchange of letters between the Reagan administration and the Senate Intelligence Committee.	O	145..337
The existence of the guidelines has become known since President Bush disclosed them privately to seven Republican senators at a White House meeting last Monday.	O	340..501
Officials familiar with the meeting said Mr. Bush cited the policy as an example of the sort of congressional requirements the administration contends contribute to the failure of such covert actions as this month's futile effort to oust Panamanian dictator Manuel Noriega.	O	502..775
According to the officials, Mr. Bush even read to the senators selections from a highly classified letter from the committee to the White House discussing the guidelines.	O	778..948
They said the president conceded the notification requirement didn't affect his decision to lend only minor support to this month's Panama coup effort.	O	949..1100
No notification was ever considered, officials said, apparently because the U.S. didn't think the coup plotters intended to kill Mr. Noriega, but merely sought to imprison him.	O	1101..1277
What's more, both administration and congressional officials hint that the notification requirement is likely to be dropped from the guidelines on coup attempts that are being rewritten by the panel and the White House.	O	1280..1499
The rewriting was launched at a meeting between Mr. Bush and intelligence committee leaders Oct. 12, a few days before the meeting at which the president complained about the rules.	O	1500..1681
However, the disclosure of the guidelines, first reported last night by NBC News, is already being interpreted on Capitol Hill as an unfair effort to pressure Congress.	O	1684..1852
It has reopened the bitter wrangling between the White House and Congress over who is responsible for the failure to oust Mr. Noriega and, more broadly, for difficulties in carrying out covert activities abroad.	O	1853..2064
A statement issued by the office of the committee chairman, Sen. David Boren (D., Okla.), charged that the disclosure is part of a continuing effort to shift the criticism for the failure of the recent coup attempt in Panama.	O	2067..2292
The statement added, "Someone has regrettably chosen to selectively summarize portions of highly classified correspondence between the two branches of government.	O	2293..2455
Not only does this come close to a violation of law, it violates the trust we have all worked to develop."	O	2456..2562
Sen. Boren said, "It's time to stop bickering and work together to develop a clear and appropriate policy to help the country in the future.	O	2565..2705
I've invited the president to send his suggestions to the committee."	O	2706..2775
Republican Sen. William Cohen of Maine, the panel's vice chairman, said of the disclosure that "a text torn out of context is a pretext, and it is unfair for those in the White House who are leaking to present the evidence in a selective fashion."	O	2778..3025
Sen. Boren said the committee couldn't defend itself by making the documents public because that would violate classification rules.	B-contrast	3028..3160
But the chairman and other committee members stressed that the notification guideline wasn't imposed on the White House by a meddling Congress.	I-contrast	3163..3306
Instead, both congressional and administration officials agreed, it grew out of talks about coup-planning in Panama that were initiated by the administration in July 1988 and stretched into last October.	O	3307..3510
The guideline wasn't a law, but a joint interpretation of how the U.S. might operate during foreign coups in light of the longstanding presidential order banning a U.S. role in assassinations.	O	3513..3705
In fact, yesterday the administration and Congress were still differing on what had been agreed to.	B-instantiation	3706..3805
One administration official said notification was required even if the U.S. "gets wind" of somebody else's coup plans that seem likely to endanger a dictator's life.	I-instantiation	3806..3971
But a congressional source close to the panel said the rule only covered coup plans directly involving the U.S.	O	3972..4083
Although the notification guideline wasn't carried out in this month's coup attempt, some administration officials argue that it may have led to hesitation and uncertainty on the part of U.S. intelligence and military operatives in Panama.	O	4086..4325
One senior administration official called the guideline "outrageous" and said it could make U.S. operatives reluctant to even listen to coup plans for fear they may get into legal trouble.	O	4326..4514
The issue came to a head last year, officials recalled, partly because the Reagan administration had sought unsuccessfully to win committee approval of funding for new Panama coup efforts.	O	4517..4705
In addition, both administration and congressional officials said the need for guidelines on coups and assassinations was partly spurred by a White House desire to avoid nasty overseas surprises during the election campaign.	O	4706..4930
Though the assassination ban is a White House order that Congress never voted on, the intelligence committees can exercise influence over its interpretation.	O	4933..5090
Last week, Central Intelligence Agency Director William Webster publicly called on Congress to provide new interpretations of the assassination order that would permit the U.S. more freedom to act in coups.	O	5091..5297
The administration has reacted to criticism that it mishandled the latest coup attempt by seeking to blame Congress for restrictions the White House said have hampered its freedom of action.	B-contrast	5300..5490
However, last week Mr. Webster's two top CIA deputies said congressional curbs hadn't hampered the spy agency's role in the coup attempt in Panama.	I-contrast	5491..5638
Nevertheless, the administration's criticisms appeared to have made some headway with Sens.Boren and Cohen after their Oct. 12 meeting with the president.	B-cause	5641..5796
The three men agreed to rewrite the guidelines, without changing the basic assassination ban, to clear up any ambiguities that may have hampered U.S. encouragement of coups against anti-American leaders.	B-contrast	5797..6000
The new argument over the notification guideline, however, could sour any atmosphere of cooperation that existed.	I-contrast	6003..6116
Gerald F. Seib contributed to this article.	O	6119..6162
