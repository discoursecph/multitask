Iverson Technology Corp. was one of the fastest-growing small companies in America -- until last year .	O	9..111
The McLean , Va. , company modifies computers to keep sensitive military data out of unfriendly hands .	B-entrel	114..214
From 1984 to 1987 , its earnings soared six-fold , to $ 3.8 million , on a seven-fold increase in revenue , to $ 44.1 million .	I-entrel	215..335
But in 1988 , it ran into a buzz saw : a Defense Department spending freeze .	B-contingency	338..412
Iverson 's earnings plunged 70 % to $ 1.2 million .	B-temporal	413..460
The troubles continued in this year 's first half , when profit plunged 81 % to $ 302,000 .	I-temporal	461..547
Iverson Technology is one of many small defense contractors besieged by the slowdown in defense spending .	B-contingency	550..655
Unlike larger contractors with a broad enough base to weather the downturn easily , these companies are suffering big drops in business as once-lucrative specialty niches in the massive military market erode or even disappear .	I-contingency	656..881
Companies that only recently were thriving find themselves scrambling to survive .	O	884..965
As their varied strategies suggest , there is more than one way to respond to a disaster -- though it 's too soon to tell whether the changes will pay off .	O	966..1119
For many companies , the instinctive first response is to cut costs .	B-comparison	1120..1187
Others are trying to find specialty defense work spared by the slowdown or new niches created by budget-cutting .	B-comparison	1188..1300
More venturesome businesses are applying their skills in commercial fields .	I-comparison	1301..1376
ERC International Inc. , which provides professional and technical services to the military , is refining its defense niche , not retreating from it .	B-entrel	1379..1525
After quadrupling annual earnings over four years to $ 6.8 million in 1988 , the Fairfax , Va. , company , posted a 23 % drop in earnings for this year 's first half .	I-entrel	1526..1685
In the belief that development of advanced military technology will remain a top Defense Department priority , ERC last year acquired W.J. Schafer Associates , a technical and scientific analysis company with contracts under the Strategic Defense Initiative .	B-contingency	1688..1944
While the SDI anti-missile program recently awarded W.J. Schafer two contracts totaling $ 13.4 million , ERC 's chairman and founder , Jack Aalseth , says he bought the company " more for its technology than its customer . "	I-contingency	1945..2161
UNC Inc. , an Annapolis , Md. , contractor that earned $ 23.8 million on revenue of $ 400.4 million in 1988 , has gone even further in realigning its military business .	B-entrel	2164..2326
As orders for its aircraft and submarine parts dwindled , three years of steady growth ended with a 69 % drop in income in this year 's first half .	B-temporal	2327..2471
The company hit on a new strategy : If the Defense Department is so intent on saving money , why not make money off that trend ?	I-temporal	2472..2597
Among the company 's current efforts : repairing old parts at 25 % of the cost of replacing them .	B-expansion	2600..2694
UNC also is selling new parts , if needed , directly to the military instead of through a prime contractor .	B-expansion	2695..2800
At as little as one-third of the government 's cost , the company is running a program to train Army helicopter pilots .	B-expansion	2801..2918
It is also taking over the maintenance of certain Navy aircraft with 40 % fewer people than the military used .	I-expansion	2919..3028
In another approach , tiny Iverson Technology is trying to resume its growth by braving the new world of commercial products .	O	3031..3155
Donald Iverson , chairman , says he hopes the company can eventually get up to half of its revenue from commercial markets .	O	3156..3277
For now , he says , " we 're looking at buying some small companies with niche markets in the personal-computer business . "	O	3278..3396
Earlier this month , Mr. Iverson agreed to buy exclusive rights to a software system developed by Visher Systems Inc. , Salt Lake City .	B-entrel	3399..3532
The product automates an array of functions performed at small to medium-size printing companies .	B-entrel	3533..3630
Mr. Iverson says there are 5,000 potential customers for the software in the Washington , D.C. , area alone .	I-entrel	3631..3737
QuesTech Inc. , Falls Church , Va. , also has acquired some companies outside the military market .	B-expansion	3740..3835
Moreover , it 's trying to transfer its skill at designing military equipment to commercial ventures .	B-expansion	3836..3935
A partnership with a Williamsburg , Va. , unit of Shell Oil Co. recently patented a process for producing plastic food containers that wo n't melt in microwave ovens .	I-expansion	3936..4099
" We 're trying to take the imagination and talent of our engineers and come up with new processes for industry , " says Vincent Salvatori , QuesTech 's chief executive .	O	4102..4265
" It is an effort to branch out from the government , which is very difficult for a defense contractor . "	O	4266..4368
Mr. Salvatori should know .	B-contingency	4371..4397
Instead of helping his company in the defense spending slowdown , Dynamic Engineering Inc. , a troubled subsidiary that makes wind tunnels for the space industry , contributed to much of QuesTech 's $ 3.3 million loss on $ 55.6 million in revenue last year .	B-contingency	4398..4649
In January , Mr. Salvatori sold the unit .	B-entrel	4650..4690
" It was our first acquisition , " he says , " and it was a mistake . "	I-entrel	4691..4755
Some companies are cutting costs and hoping for the best .	B-expansion	4758..4815
Telos Corp. , a Santa Monica , Calif. , provider of software-development and hardware-maintenance services to the military , enjoyed steady growth until this year .	B-temporal	4816..4975
Following a tripling of earnings , to $ 3.9 million , on a doubling of revenue , to $ 116 million , over four years , earnings in the company 's fiscal first quarter , which ended June 30 , plunged 90 % to $ 45,000 .	I-temporal	4976..5179
A one-time write-off for booking nonexistent revenue was partly to blame , but so were lower profits from a stingier contract with the Army and delays in getting paid .	O	5180..5346
Telos responded by combining three of its five divisions to reduce expenses and " bring more focus to potentially fewer bidding opportunities , " says Lin Conger , Telos chairman and controlling shareholder .	O	5349..5552
" It 's evident we 're entering a more competitive era , " he says .	O	5553..5615
TransTechnology Corp. , a Sherman Oaks , Calif. , defense contractor that earned $ 9.2 million on revenue of $ 235.2 million in 1988 , provides a more dramatic example of cost-cutting .	O	5618..5796
The company not only merged three military-electronics manufacturing operations , but also closed an unrelated plant that makes ordnance devices used in fighter planes and missiles .	B-entrel	5797..5977
The closing contributed to a $ 3.4 million loss in the fiscal first quarter ended July 31 -- its first quarterly loss since 1974 .	I-entrel	5978..6106
" Our ordnance business has been hurt very badly by the slowdown , " says Arch Scurlock , TransTechnology 's chairman .	O	6109..6222
" I would n't say we 're out of the business .	O	6223..6265
But we 're not making as many { pyrotechnic devices } as we used to .	O	6266..6331
