For all of this year 's explosive run-up in stock prices , Renaissance Investment Management Inc. 's computer sat on the sidelines .	B-comparison	9..138
Now it 's on the fence .	I-comparison	139..161
Renaissance , a Cincinnati-based money manager , began buying stocks again this week with half of the $ 1.8 billion that it oversees for clients , according to people familiar with the firm 's revised strategy .	B-expansion	164..369
It was the first time since January that Renaissance has thought stocks are worth owning .	I-expansion	370..459
Renaissance declined to confirm the move , but its stock purchases were thought to have begun Tuesday , timed to coincide with the maturity this week of Treasury bills owned by the firm .	O	462..646
The other half of its portfolio is expected to remain invested in Treasury bills for the time being .	O	647..747
Wall Street executives said they believed that Renaissance 's $ 900 million buy program was carried out by PaineWebber Inc .	O	750..871
As reported , PaineWebber bought shares Tuesday as part of a customer strategy shift , although the broker 's client was said then to have been Japanese .	O	872..1022
Yesterday , PaineWebber declined comment .	O	1023..1063
When it owns stocks , Renaissance 's portfolio typically is composed of about 60 large-capitalization issues ; to make buy or sell moves , the firm solicits Wall Street brokerage houses a day or so in advance , looking for the best package price to carry out the trades .	O	1066..1331
The broker winning the business does n't charge commissions , but instead profits by buying or selling for less than the overall package price .	O	1332..1473
That puts the broker at risk if it 's trying to buy stock in a rising market .	B-expansion	1476..1552
In Tuesday 's gyrating session , the Dow Jones Industrial Average fell by 80 points early in the day , but finished with less than a four-point loss .	I-expansion	1553..1699
Renaissance 's last portfolio shift , carried out by Goldman , Sachs & Co. , was a highly publicized decision last January to sell its entire stock portfolio and buy Treasury bills .	B-entrel	1702..1879
The sell signal , which sent a bearish chill through the stock market , came when Renaissance 's computer found that stocks were overpriced compared with bonds and Treasury bills .	I-entrel	1880..2056
At the time , the Dow Jones Industrial Average stood at about 2200 .	B-comparison	2059..2125
The Dow average now stands more than 20 % higher , while Renaissance 's portfolio of Treasurys produced a return of about 6 % through the first three quarters of the year .	B-contingency	2126..2293
The computer 's miscalculation has been painful for Renaissance .	I-contingency	2294..2357
Almost any money manager holding stocks has turned in better results , while Renaissance has played it safe with Treasury bills .	O	2358..2485
So why does Renaissance 's computer like stocks with the Dow at 2653.28 , where it closed yesterday , when it did n't with the Dow at 2200 ?	O	2488..2623
" With the decline in stock prices and continued low or stable interest rates , stocks are representing a better value all the time , " Renaissance President Frank W. Terrizzi said yesterday .	O	2626..2813
Three-month T-bill yields have fallen to 7.8 % from about 9 % at the start of the year .	B-comparison	2816..2901
Stock prices , meanwhile , are about 140 points lower than the peak of 2791.41 reached on the Dow industrial average Oct. 9 .	I-comparison	2902..3024
Are those declines enough to signal a partial return to stocks ?	O	3025..3088
Mr. Terrizzi wo n't say specifically , explaining that if there was such a move , it would take about three days to complete the loose ends of the transaction .	O	3091..3247
During that time , a buyer with the clout of a Renaissance could end up driving up the price of stocks it was trying to buy if it tipped its hand .	O	3248..3393
But everything is relative to Mr. Terrizzi , so stocks in his view can become more attractive in comparison with bonds or T-bills , even if shares are more expensive than when they were sold in January .	O	3396..3596
" Our { computer } model has a certain trigger point , " he said .	O	3597..3657
When the computer says switch , Renaissance switches .	O	3658..3710
The firm has made 17 previous shifts from one type of asset to another in its 10-year history .	B-expansion	3713..3807
Almost all have involved at least half and often the firm 's entire portfolio , as the computer searches for the most undervalued investment category , following a money-management style called tactical asset allocation .	I-expansion	3808..4025
Competing asset-allocation firms march to their own computer models , so some have been partly or fully invested in stocks this year while Renaissance has sat on the sidelines .	O	4028..4203
As a result , competitors say Renaissance has been looking for any opportunity to return to the stock market , rather than risk losing business by continuing to remain fully invested in Treasury bills .	O	4204..4403
Mr. Terrizzi confirms some clients have left Renaissance , but no major ones , and the firm has added new accounts .	O	4406..4519
