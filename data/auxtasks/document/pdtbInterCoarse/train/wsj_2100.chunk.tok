Consumers may want to move their telephones a little closer to the TV set .	O	9..83
Couch-potato jocks watching ABC 's " Monday Night Football " can now vote during halftime for the greatest play in 20 years from among four or five filmed replays .	B-expansion	86..246
Two weeks ago , viewers of several NBC daytime consumer segments started calling a 900 number for advice on various life-style issues .	B-expansion	247..380
And the new syndicated " reality " show " Hard Copy " records viewers ' opinions for possible airing on the next day 's show .	I-expansion	381..500
Interactive telephone technology has taken a new leap in sophistication , and television programmers are racing to exploit the possibilities .	O	503..643
Eventually viewers may grow bored with the technology and resent the cost .	B-comparison	644..718
But right now programmers are figuring that viewers who are busy dialing up a range of services may put down their remote control zappers and stay tuned .	I-comparison	719..872
" We 've been spending a lot of time in Los Angeles talking to TV production people , " says Mike Parks , president of Call Interactive , which supplied technology for both ABC Sports and NBC 's consumer minutes .	O	875..1080
" With the competitiveness of the television market these days , everyone is looking for a way to get viewers more excited . "	O	1081..1203
One of the leaders behind the expanded use of 900 numbers is Call Interactive , a joint venture of giants American Express Co. and American Telephone & Telegraph Co .	B-entrel	1206..1370
Formed in August , the venture weds AT&T 's newly expanded 900 service with 200 voice-activated computers in American Express 's Omaha , Neb. , service center .	I-entrel	1371..1525
Other long-distance carriers have also begun marketing enhanced 900 service , and special consultants are springing up to exploit the new tool .	O	1528..1670
Blair Entertainment , a New York firm that advises TV stations and sells ads for them , has just formed a subsidiary -- 900 Blair -- to apply the technology to television .	O	1671..1840
The use of 900 toll numbers has been expanding rapidly in recent years .	O	1843..1914
For a while , high-cost pornography lines and services that tempt children to dial ( and redial ) movie or music information earned the service a somewhat sleazy image , but new legal restrictions are aimed at trimming excesses .	O	1915..2139
The cost of a 900 call is set by the originator -- ABC Sports , for example -- with the cheapest starting at 75 cents .	B-entrel	2142..2259
Billing is included in a caller 's regular phone bill .	B-entrel	2260..2313
From the fee , the local phone company and the long-distance carrier extract their costs to carry the call , passing the rest of the money to the originator , which must cover advertising and other costs .	I-entrel	2314..2515
In recent months , the technology has become more flexible and able to handle much more volume .	B-temporal	2518..2612
Before , callers of 900 numbers would just listen and not talk , or they 'd vote " yes " or " no " by calling one of two numbers .	B-entrel	2613..2735
( People in the phone business call this technology " 900 click . " )	B-entrel	2736..2800
Now , callers are led through complex menus of choices to retrieve information they want , and the hardware can process 10,000 calls in 90 seconds .	I-entrel	2801..2946
Up to now , 900 numbers have mainly been used on local TV stations and cable channels .	B-expansion	2949..3034
MTV used one to give away the house that rock star Jon Bon Jovi grew up in .	I-expansion	3035..3110
For several years , Turner Broadcasting System 's Cable News Network has invited viewers to respond nightly to topical issues ( " Should the U.S. military intervene in Panama ? " ) , but even the hottest controversies on CNN log only about 10,000 calls .	O	3111..3356
The newest uses of the 900-interactive technology demonstrate the growing variety of applications .	B-expansion	3359..3457
Capital Cities/ABC Inc. , CBS Inc. and General Electric Co. 's National Broadcasting Co. unit are expected to announce soon a joint campaign to raise awareness about hunger .	B-expansion	3458..3630
The subject will be written into the plots of prime-time shows , and viewers will be given a 900 number to call .	I-expansion	3631..3742
Callers will be sent educational booklets , and the call 's modest cost will be an immediate method of raising money .	O	3743..3858
Other network applications have very different goals .	B-expansion	3861..3914
ABC Sports was looking for ways to lift deflated halftime ratings for " Monday Night Football . "	I-expansion	3915..4009
Kurt Sanger , ABC Sports 's marketing director , says that now " tens of thousands " of fans call its 900 number each week to vote for the best punt return , quarterback sack , etc .	O	4010..4184
Profit from the calls goes to charity , but ABC Sports also uses the calls as a sales tool : After thanking callers for voting , Frank Gifford offers a football videotape for $ 19.95 , and 5 % of callers stay on the line to order it .	O	4187..4414
Jackets may be sold next .	O	4415..4440
Meanwhile , NBC Sports recently began " Scores Plus , " a year-round , 24-hour 900 line providing a complex array of scores , analysis and fan news .	O	4443..4585
A spokesman said its purpose is " to bolster the impression that NBC Sports is always there for people . "	O	4586..4689
NBC 's " On-Line " consumer minutes have increased advertiser spending during the day , the network 's weakest period .	O	4692..4805
Each weekday matches a sponsor and a topic : On Mondays , Unilever N.V. 's Lever Bros. sponsors tips on diet and exercise , followed by a 30-second Lever Bros. commercial .	O	4806..4973
Viewers can call a 900 number for additional advice , which will be tailored to their needs based on the numbers they punch ( " Press one if you 're pregnant , " etc . ) .	B-expansion	4976..5138
If the caller stays on the line and leaves a name and address for the sponsor , coupons and a newsletter will be mailed , and the sponsor will be able to gather a list of desirable potential customers .	I-expansion	5139..5338
Diane Seaman , an NBC-TV vice president , says NBC has been able to charge premium rates for this ad time .	O	5341..5445
She would n't say what the premium is , but it 's believed to be about 40 % above regular daytime rates .	B-entrel	5446..5546
" We were able to get advertisers to use their promotion budget for this , because they get a chance to do couponing , " says Ms. Seaman .	I-entrel	5547..5680
" And we were able to attract some new advertisers because this is something new . "	O	5681..5762
Mr. Parks of Call Interactive says TV executives are considering the use of 900 numbers for " talk shows , game shows , news and opinion surveys . "	O	5765..5908
Experts are predicting a big influx of new shows in 1990 , when a service called " automatic number information " will become widely available .	B-contingency	5909..6049
This service identifies each caller 's phone number , and it can be used to generate instant mailing lists .	I-contingency	6050..6155
" Hard Copy , " the new syndicated tabloid show from Paramount Pictures , will use its 900 number for additional purposes that include research , says executive producer Mark B. von S. Monsky .	O	6158..6345
" For a piece on local heroes of World War II , we can ask people to leave the name and number of anyone they know who won a medal , " he says .	O	6346..6485
" That 'll save us time and get people involved . "	O	6486..6533
But Mr. Monsky sees much bigger changes ahead .	O	6536..6582
" These are just baby steps toward real interactive video , which I believe will be the biggest thing yet to affect television , " he says .	O	6583..6718
Although it would be costly to shoot multiple versions , TV programmers could let audiences vote on different endings for a movie .	O	6719..6848
Fox Broadcasting experimented with this concept last year when viewers of " Married ... With Children " voted on whether Al should say " I love you " to Peg on Valentine 's Day .	O	6849..7021
Someday , viewers may also choose different depths of news coverage .	O	7024..7091
" A menu by phone could let you decide , ' I 'm interested in just the beginning of story No. 1 , and I want story No. 2 in depth , " Mr. Monsky says .	O	7092..7235
" You 'll start to see shows where viewers program the program .	O	7236..7297
