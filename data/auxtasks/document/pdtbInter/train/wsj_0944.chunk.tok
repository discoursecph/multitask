Two recent decisions by federal courts cast judges in the odd role of telling authors how they should write history and biography .	B-entrel	9..139
These decisions deserve more attention than they have received from scholars , and from journalists as well .	I-entrel	140..247
Russell Miller 's " Bare-Faced Messiah : The True Story of L. Ron Hubbard " is a biography of the founder of the Church of Scientology .	B-entrel	250..381
Mr. Hubbard , who died in 1986 , bequeathed the copyrights on his writings to his church , which licensed them to New Era Publications , a Danish corporation .	B-asynchronous	382..536
In 1988 New Era sought a permanent injunction to restrain Henry Holt & Co. from publishing " Bare-Faced Messiah " on the ground that Mr. Miller 's quotations from Mr. Hubbard infringed the copyrights .	B-contrast	537..734
The publisher argued in response that the " fair use " statute permits quotation " for purposes such as criticism , comment , news reporting , teaching , ... scholarship , or research . "	I-contrast	735..912
District Court Judge Pierre Leval denied the injunction on the ground that New Era had failed to make its claim within a reasonable time -- the doctrine lawyers call " laches . "	O	915..1090
As for the merits , Judge Leval said that Mr. Miller had written " a serious book of responsible historical criticism . "	B-entrel	1091..1208
Verbatim quotation , the judge believed , was justified in order to prove points the author had asserted about Mr. Hubbard -- mendacity , bigotry , paranoia and other unlovely traits that could not be persuasively demonstrated without use of Mr. Hubbard 's own words .	I-entrel	1209..1471
" The biographer/critic , " Judge Leval wrote , " should not be required simply to express ... conclusions without defending them by example .	O	1472..1608
" In such circumstances , free-speech interests outweighed the interests of the copyright owner .	O	1608..1703
But Judge Leval felt constrained by an earlier decision of the Second Circuit Court forbidding a biographer of J.D. Salinger to quote from Mr. Salinger 's personal letters .	O	1706..1877
He distinguished the two cases : In Salinger , Judge Leval noted , the quotations were for the purpose of enlivening the biography rather than of proving points about the subject .	O	1878..2054
Still the Salinger decision created a strong presumption against fair use of unpublished materials .	O	2055..2154
Judge Leval reluctantly concluded that a few of Mr. Miller 's quotations from Mr. Hubbard 's unpublished writings , because they were not necessary to prove historical points , failed the fair-use test and therefore infringed copyright .	O	2155..2387
But the proper remedy , Judge Leval said , lay in a suit for damages , not in an injunction .	O	2388..2477
The case went on appeal to the Second Circuit .	O	2480..2526
In a decision in April of this year , Judge Roger Miner , joined by Judge Frank Altimari , agreed on denying the injunction and did not doubt that " Bare-Faced Messiah " was a serious work but rejected Judge Leval 's argument that the public interest in scholarship could outweigh the sanctity of copyright .	O	2527..2828
" We conclude , " the two judges wrote , " that laches is the sole bar to the issuance of an injunction . "	O	2829..2929
Had the suit been filed in time , they said , " Bare-Faced Messiah " would have been suppressed .	O	2930..3022
This was too much for James Oakes , the court 's chief judge .	O	3025..3084
In a powerful separate opinion , Judge Oakes further distinguished the Salinger case by pointing out that a living person , like Mr. Salinger , had privacy rights that did not apply to a dead man , like Mr. Hubbard .	O	3085..3296
" I thought that Salinger might by being taken in another factual context come back to haunt us .	O	3297..3392
This case realizes that concern . "	O	3393..3426
Decisions by the Second Circuit itself , Judge Oakes continued , had recognized that public interest in the subject matter and the indispensability in particular cases of verbatim quotations are vital components of fair use .	O	3429..3651
And the injunction Judges Miner and Altimari would so readily have granted had New Era sued in time ?	O	3652..3752
Suppression of the book , Judge Oakes observed , would operate as a prior restraint and thus involve the First Amendment .	O	3753..3872
Moreover , and here Judge Oakes went to the heart of the question , " Responsible biographers and historians constantly use primary sources , letters , diaries , and memoranda .	O	3875..4045
Indeed , it would be irresponsible to ignore such sources of information . "	O	4046..4119
Now , scholars in fulfilling their responsibility do not claim the right to invade every collection of papers that bears upon their topics of investigation .	O	4122..4277
And of course they agree that people can impose restrictions on the use of their papers , whether in their own possession or as donated or sold to libraries .	O	4278..4434
But in the " Bare-Faced Messiah " case the author found most of his material in court records or via the Freedom of Information Act .	O	4435..4565
And when responsible scholars gain legitimate access to unpublished materials , copyright should not be permitted to deny them use of quotations that help to establish historical points .	O	4566..4751
Judges Oakes and Leval understand the requirements of historical scholarship .	B-contrast	4754..4831
Judges Miner and Altimari do not appear to have a clue .	I-contrast	4832..4887
Yet at the moment they are the judges who are making the law .	O	4888..4949
As matters stand , the Salinger ruling , torn from context and broadly construed , is controlling .	O	4950..5045
If an author quotes " more than minimal amounts " of unpublished copyrighted materials , as the Salinger decision had it , " he deserves to be enjoined . "	O	5046..5194
The courts have not defined " minimal amounts , " but publishers , I understand , take it to mean about 50 words .	O	5195..5303
The " Bare-Faced Messiah " decision strikes a blow against the whole historical enterprise .	B-conjunction	5304..5393
A second decision , handed down in August by the Court of Appeals for the Ninth Circuit , is another blow against scholarship .	I-conjunction	5394..5518
Janet Malcolm , a professional writer on psychiatric matters , wrote a series of articles for the New Yorker , later published in book form by Knopf under the title " In the Freud Archives . "	O	5519..5705
The articles were largely based on interviews Ms. Malcolm had taped with Jeffrey Masson , a psychoanalyst who had served as projects director of the Freud Archives .	O	5706..5869
Mr. Masson then brought a libel suit against Ms. Malcolm , the New Yorker and Knopf .	O	5872..5955
As a public figure , Mr. Masson had to prove malice and , as proof of malice , Mr. Masson contended that defamatory quotations ascribed to him by Ms. Malcolm were in fact fabricated .	O	5956..6135
The quotes could not be found on the tapes , and the two judges who decided the case for Ms. Malcolm and her publishers conceded that , for the purpose of their decision , " we assume the quotations were deliberately altered . "	O	6136..6358
For all historians and most journalists , this admission would have been sufficient to condemn the Malcolm articles .	O	6361..6476
But Judge Arthur Alarcon , joined by Judge Cynthia Holcomb Hall , took the astonishing position that it is perfectly OK to fabricate quotations so long as a judge finds that the fabrications do not alter substantive content or are rational interpretations of ambiguous remarks .	O	6477..6752
In his eloquent dissent , Judge Alex Kozinski observed that when a writer uses quotation marks in reporting what someone has said , the reader assumes that these are the speaker 's precise words or at least his words purged of " uh " and " you know " and grammatical error .	O	6755..7021
While judges have an obligation under the First Amendment to safeguard freedom of the press , " the right to deliberately alter quotations is not , in my view , a concomitant of a free press . "	O	7022..7210
Ms. Malcolm , for example , wrote that Mr. Masson described himself as " the greatest analyst who ever lived . "	O	7213..7320
No such statement appears on the tapes .	B-contrast	7321..7360
The majority cited Mr. Masson 's remark " It 's me alone ... against the rest of the analytic world " as warrant for the Malcolm fabrication .	I-contrast	7361..7498
But , as Judge Kozinski noted , the context shows that Mr. Masson 's " me alone " remark referred not to his alleged pre-eminence in his profession but to the fact that his position on a particular issue was not shared by anyone else .	O	7499..7728
Ms. Malcolm had Mr. Masson describing himself as " an intellectual gigolo . "	O	7731..7805
Again , no such statement appears on the tapes .	O	7806..7852
The majority decision contended that the phrase was a rational interpretation of Mr. Masson 's description of himself as a " private asset but a public liability " to Anna Freud and that in any case it was not defamatory .	O	7853..8071
Judge Kozinski found the derivation entirely strained and writes that " for an academic to refer to himself as an intellectual gigolo is ... a devastating admission of professional dishonesty . "	O	8072..8264
These were only two of a series of fabrications that had , in Judge Kozinski 's words , the cumulative effect of making Mr. Masson " appear more arrogant , less sensitive , shallower , more self-aggrandizing , and less in touch with reality than he appears from his own statements . "	O	8267..8541
As Robert Coles wrote in a review of Ms. Malcolm 's book , Mr. Masson emerges " as a grandiose egotist ... and , in the end , a self-destructive fool .	O	8542..8687
But it is not Janet Malcolm who calls him such : his own words reveal this psychological profile . "	O	8688..8785
We now know that the words were not always his own .	O	8786..8837
" There is one sacred rule of journalism , " John Hersey has said .	O	8840..8903
" The writer must not invent . "	O	8904..8933
Should the green light Judges Alarcon and Hall have given to the fabrication of quotations become standard practice , it will notably reduce the value of journalism for historians -- and for citizens .	O	8934..9133
As Judge Kozinski put it : " To invoke the right to deliberately distort what someone else has said is to assert the right to lie in print ... .	O	9134..9275
Masson has lost his case , but the defendants , and the profession to which they belong , have lost far more . "	O	9276..9383
The historical profession will survive these decisions .	B-entrel	9386..9441
Perhaps in time the Supreme Court will correct them .	I-entrel	9442..9494
But writing history is tough enough without judges gratuitously throwing obstacles in the scholar 's path .	O	9495..9600
Mr. Schlesinger is Albert Schweitzer professor of the humanities at the City University of New York and a winner of Pulitzer Prizes in history and biography .	O	9603..9760
