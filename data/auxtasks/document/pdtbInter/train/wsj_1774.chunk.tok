Dun & Bradstreet Corp. posted a 15 % rise in third-quarter earnings .	B-contrast	9..76
But revenue declined more than 2 % , reflecting in part a continuing drop in sales of credit services in the wake of controversy over the company 's sales practices .	I-contrast	79..241
The information company also cited the stronger dollar , the sale last year of its former Official Airline Guides unit and other factors .	O	242..378
Net income rose to a record $ 155.3 million , or 83 cents a share , from $ 134.8 million , or 72 cents a share .	B-synchrony	381..487
Revenue fell to $ 1.04 billion from $ 1.07 billion .	I-synchrony	488..537
In composite trading on the New York Stock Exchange , Dun & Bradstreet closed yesterday at $ 53.75 , down 25 cents a share .	O	540..660
Analysts said the results were as expected , but several added that the earnings masked underlying weaknesses in several businesses .	B-instantiation	663..794
" The quality of earnings was n't as high as I expected , " said Eric Philo , an analyst for Goldman , Sachs & Co .	I-instantiation	795..903
For example , he noted , operating profit was weaker than he had anticipated , but nonoperating earnings of $ 14.6 million and a lower tax rate helped boost net income .	O	906..1070
Dun & Bradstreet said operating earnings rose 8 % , excluding the sale of Official Airline Guides .	O	1073..1169
Third-quarter sales of U.S. credit services were " disappointingly below sales " of a year earlier , Dun & Bradstreet said .	O	1172..1292
As previously reported , those sales have been declining this year in the wake of allegations that the company engaged in unfair sales practices that encouraged customers to overpurchase services .	O	1293..1488
The company has denied the allegations but has negotiated a proposed $ 18 million settlement of related lawsuits .	B-synchrony	1489..1601
Analysts predict the sales impact will linger .	B-instantiation	1602..1648
" There is n't much question there will continue to be a ripple effect , " said John Reidy , an analyst with Drexel Burnham Lambert Inc .	I-instantiation	1649..1780
Dun & Bradstreet noted that price competition in its Nielsen Marketing Research , Nielsen Clearing House and Donnelley Marketing businesses also restrained revenue growth .	B-conjunction	1783..1953
It cited cyclical conditions in its Moody 's Investors Service Inc. and D&B Plan Services units .	I-conjunction	1954..2049
For the nine months , net income rose 19 % to $ 449 million , or $ 2.40 a share , from $ 375.9 million , or $ 2.01 a share , a year earlier .	B-conjunction	2052..2182
Year-earlier earnings reflected costs of $ 14.3 million related to the acquisition of IMS International .	B-entrel	2183..2286
Revenue rose slightly to $ 3.16 billion from $ 3.13 billion .	I-entrel	2287..2345
