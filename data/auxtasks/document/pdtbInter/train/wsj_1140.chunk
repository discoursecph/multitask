Short interest in Nasdaq over-the-counter stocks rose 6% as of mid-October, its biggest jump since 6.3% last April.	O	9..124
The most recent OTC short interest statistics were compiled Oct. 13, the day the Nasdaq composite index slid 3% and the New York Stock Exchange tumbled 7%.	B-cause	127..282
The coincidence might lead to the conclusion that short-sellers bet heavily on that day that OTC stocks would decline further.	I-cause	283..409
As it happens, the Nasdaq composite did continue to fall for two days after the initial plunge.	O	412..507
However, the short interest figures reported by brokerage and securities clearing firms to the National Association of Securities Dealers include only those trades completed, or settled, by Oct. 13, rather than trades that occurred on that day, according to Gene Finn, chief economist for the NASD.	O	508..806
Generally, it takes five business days to transfer stock and to take the other steps necessary to settle a trade.	O	807..920
The total short interest in Nasdaq stocks as of mid-October was 237.1 million shares, up from 223.7 million in September but well below the record level of 279 million shares established in July 1987.	B-contrast	923..1123
The sharp rise in OTC short interest compares with the 4.2% decline in short interest on the New York Stock Exchange and the 3% rise on the American Stock Exchange during the September-October period.	I-contrast	1124..1324
Generally, a short seller expects a fall in a stock's price and aims to profit by selling borrowed shares that are to be replaced later; the short seller hopes the replacement shares bought later will cost less than those that were sold.	O	1327..1564
Short interest, which represents the number of shares borrowed and sold, but not yet replaced, can be a bad-expectations barometer for many stocks.	O	1565..1712
Among 2,412 of the largest OTC issues, short interest rose to 196.8 million shares, from 185.7 million in 2,379 stocks in September.	B-restatement	1715..1847
Big stocks with large short interest gains as of Oct. 13 included First Executive, Intel, Campeau and LIN Broadcasting.	I-restatement	1848..1967
Short interest in First Executive, an insurance issue, rose 55% to 3.8 million.	B-list	1970..2049
Intel's short interest jumped 42%, while Campeau's increased 62%.	B-entrel	2050..2115
Intel makes semiconductors and Campeau operates department-store chains and is strained for cash.	I-entrel	2116..2213
Meritor Savings again had the dubious honor of being the OTC stock with the biggest short interest position on Nasdaq.	B-conjunction	2216..2334
Meritor has headed the list since May.	B-entrel	2335..2373
First Executive and troubled Valley National Corp. of Arizona were next in line.	I-entrel	2374..2454
Short selling isn't necessarily bad for the overall market.	B-cause	2457..2516
Shorted shares must eventually be replaced through buying.	B-conjunction	2517..2575
In addition, changes in short interest in some stocks may be caused by arbitrage.	B-instantiation	2576..2657
For example, an investor may seek to profit during some takeover situations by buying stock in one company involved and shorting the stock of the other.	I-instantiation	2658..2810
Two big stocks involved in takeover activity saw their short interest surge.	B-restatement	2813..2889
Short interest in the American depositary receipts of Jaguar, the target of both Ford Motor and General Motors, more than doubled.	I-restatement	2890..3020
Nasdaq stocks that showed a drop in short interest included Adobe Systems, Class A shares of Tele-Communications and takeover targets Lyphomed and Jerrico.	O	3023..3178
The NASD, which operates the Nasdaq computer system on which 5,200 OTC issues trade, compiles short interest data in two categories: the approximately two-thirds, and generally biggest, Nasdaq stocks that trade on the National Market System; and the one-third, and generally smaller, Nasdaq stocks that aren't a part of the system.	O	3181..3512
Short interest in 1,327 non-NMS securities totaled 40.3 million shares, compared with almost 38 million shares in 1,310 issues in September.	O	3515..3655
The October short interest represents 1.04 days of average daily trading volume in the smaller stocks in the system for the reporting period, compared with 0.94 day a month ago.	O	3658..3835
Among bigger OTC stocks, the figures represent 2.05 days of average daily volume, compared with 2.14 days in September.	O	3838..3957
The adjacent tables show the issues in which a short interest position of at least 50,000 shares existed as of Oct. 13 or in which there was a short position change of at least 25,000 shares since Sept. 15 (see accompanying tables -- WSJ Oct. 25, 1989).	O	3960..4213
