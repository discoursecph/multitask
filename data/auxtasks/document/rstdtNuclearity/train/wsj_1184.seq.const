General Electric Co. executives and lawyers provided `` misleading and false '' information to the Pentagon in 1985 in an effort	(NS(NS(NS
to cover up `` longstanding fraudulent '' billing practices ,	NS)
federal prosecutors alleged in legal briefs .	NS)
The government 's startling allegations ,	(NS(NN(NS(NS(NN(NN(NS
filed only days before the scheduled start of a criminal overcharge trial against GE in Philadelphia federal district court ,	NS)
challenge the motives and veracity of the nation 's third-largest defense contractor .	NN)
In a strongly worded response	(NS(SN(NN(NS
summarizing a filing made in the same court yesterday ,	NS)
GE asserted that	NN)
`` prosecutors have misstated the testimony of witnesses ,	(NN
distorted documents	(NN
and ignored important facts . ''	NN)))
The company attacked the government 's allegations as `` reckless and baseless mudslinging , ''	(NN
and said	(SN
its management `` promptly and accurately reported '' to the Pentagon all relevant information about billing practices .	SN))))
The case strikes at the corporate image of GE ,	(NS(NN(NS
which provides the military with everything from jet engines and electronic warfare equipment to highly classified design work on the Strategic Defense Initiative ,	NS)
and could cause a loss of future defense contracts	(NS
if Pentagon and Justice Department officials take a tough stance .	NS))
The company has been considered an industry leader	(SN(NS
in advocating cooperation and voluntary disclosures of improper or inflated billing practices .	NS)
But the government now claims	(SN
that a group of company managers and lawyers engaged in an elaborate strategy over five years	(NS
to obscure from federal authorities the extent and details of `` widespread '' fraudulent billing practices .	NS)))))
The problems were uncovered during a series of internal investigations of the company 's Space Systems division ,	(NS(NS(NS(NS
which has been the focus of two separate overcharge prosecutions by the government since 1985 .	NS)
The dispute stems from pretrial maneuvering in the pending court case ,	(NS(NS
in which prosecutors have been demanding access to a host of internal company memos , reports and documents .	NS)
Last November , a federal grand jury indicted GE on charges of fraud and false claims in connection with an alleged scheme	(NS
to defraud the Army of $ 21 million on a logistics computer contract .	NS)))
The company , for its part , maintains	(SN
that many of the disputed documents are privileged attorney-client communications	(NS
that should n't be turned over to prosecutors .	NS)))
A hearing is scheduled on the issue today .	NS))
The government 's 136-page filing covers events	(NS(NS
leading up to the current case and an earlier indictment in March 1985 ,	(NS
when GE was accused of defrauding the Pentagon	(NN(NS
by illegally claiming cost overruns on Minuteman missile contracts .	NS)
GE pleaded guilty	(NS
and paid a fine of more than $ 1 million in the Minuteman case ,	(NS
which involved some of the same individuals and operations	(NS
that are at the center of the dispute in the Philadelphia court .	NS))))))
In order to show	(NN(SN
that all of its units had corrected billing problems	(SN(SN
and therefore should become eligible again for new contracts ,	SN)
prosecutors contend that	(SN
`` high-level GE executives '' and company lawyers provided `` misleading statements '' to then-Air Force Secretary Verne Orr and other Pentagon officials during a series of meetings in 1985 .	SN)))
Overall , the government contends	(NN(SN
that GE 's disclosure efforts largely were intended to `` curry favor '' with Pentagon officials	(NS
without detailing the extent of the management lapses and allegedly pervasive billing irregularities	(NS
uncovered by company investigations .	NS)))
Prosecutors depict a company	(NN(NS(NS
that allegedly sat on damaging evidence of overcharges from 1983 to 1985 ,	(NS
despite warnings from an internal auditor .	NS))
When GE finally disclosed the problems ,	(NN
prosecutors contend	(SN
that Mr. Orr `` was erroneously informed that the { suspected } practices had only just been discovered '' by GE management .	SN)))
In its brief , the government asserted	(NN(SN
that it needs the internal GE documents	(NS(NS
to rebut anticipated efforts by GE during the trial	NS)
to demonstrate `` its good corporate character . ''	NS))
GE ,	(NS(NS(SN(NN(NS
which was surprised by the last-minute subpoena for more than 100 boxes and file cabinets of documents ,	NS)
countered	NN)
that senior GE managers did n't find out about questionable billing practices until 1985 ,	(NN
and that the information was passed on quickly to Mr. Orr at his first meeting with company representatives .	NN))
Subsequent meetings ,	(NS(NN(NS
initiated after the company and two of its units were briefly suspended from federal contracts ,	NS)
were held	(NS
to familiarize Mr. Orr with the company 's self-policing procedures	(NN
and to disclose additional information ,	NN)))
according to GE .	NS))
GE 's filing contends	(NS(SN
that the billing practices at the heart of the current controversy involved technical disputes rather than criminal activity .	SN)
The company 's conduct `` does not even raise a question of wrongful corporate intent , ratification or cover-up , ''	(SN(NS
GE 's brief asserts .	NS)
`` On the contrary , it shows a corporation	(NS
reacting swiftly and aggressively to very difficult issues in largely uncharted waters . ''	NS))))))))))
Mr. Orr could n't be reached for comment yesterday .	NS))
