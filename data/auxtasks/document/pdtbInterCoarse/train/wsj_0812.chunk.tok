The 1986 Tax Reform Act has nearly eliminated the number of large , profitable corporations that do n't pay federal income tax , according to Citizens for Tax Justice , a nonprofit , labor-funded research and lobbying group .	O	9..228
In a study of 250 of the nation 's richest companies , the group found that only seven managed to avoid paying federal income taxes last year compared with 40 in 1986 , the last year the old tax rules were in effect , and 16 in 1987 , when some of the new tax provisions went into effect .	O	231..514
Moreover , 41 companies that paid no federal income tax from 1981 through 1985 -- despite billions of dollars of profits -- ended up paying an average of 27.9 % of their income in federal taxes in 1988 .	O	517..717
The report , released yesterday , comes as Congress is considering a number of special tax breaks only three years after the sweeping tax-revision legislation abolished or curtailed many loopholes .	O	720..915
In the corporate realm , the 1986 law abolished the investment-tax credit , scaled back use of an accounting method that allowed large contractors to defer taxes until a project was completed and strengthened the so-called alternative minimum tax , a levy to ensure all money-making businesses pay some federal tax .	O	918..1230
The combination of lower rates and fewer loopholes has meant that the so-called average effective tax rate -- the rate actually paid -- of the 250 corporations surveyed reached 26.5 % in 1988 , compared with 14.3 % in the years from 1981 through 1985 , according to the study .	O	1233..1505
In addition , corporations are now shouldering a bigger share of the tax burden , as the authors of the 1986 law hoped .	O	1508..1625
Corporate taxes paid for almost 12 % of federal spending in 1988 -- excluding Social Security -- compared with less than 8 % in the first half of the 1980s , the study found .	O	1626..1797
" Tax reform is working , " the study said .	O	1800..1840
" Under the new tax-reform law , the days of widespread , wholesale corporate tax avoidance have come to an end . "	O	1841..1951
Still , Kroger Co. , Pinnacle West Capital Corp. , CSX Corp. , Illinois Power Co. , Media General Inc. , Santa Fe Southern Pacific Corp. and Gulf States Utilities Co. , did n't pay any federal income tax last year although they garnered a total of $ 1.2 billion in profits , the group said .	O	1954..2234
In fact , six of those companies received refunds , which totaled $ 120 million .	O	2235..2312
The lobbying group used publicly available information to calculate each company 's domestic profits and its federal income tax payments .	B-entrel	2315..2451
This is the fifth year Citizens for Tax Justice has released a study on corporate tax bills .	B-entrel	2452..2544
Earlier reports , which revealed that as many as 73 companies were avoiding income tax legally , have been credited with helping galvanize efforts to overhaul the tax code .	I-entrel	2545..2715
But even though companies are paying more taxes , many are still paying less than the statutory rate , the report said .	O	2718..2835
And 45 companies paid effective tax rates of below 10 % of their income .	O	2836..2907
" While the overall picture is very encouraging , significant corporate tax avoidance continues , " the study said .	O	2908..3019
Glenn Hall contributed to this article .	O	3022..3061
