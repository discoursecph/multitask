Measuring cups may soon be replaced by tablespoons in the laundry room .	(NS-elaboration-additional
Procter & Gamble Co. plans to begin testing next month a superconcentrated detergent	(NS-elaboration-additional(NS-background(NS-elaboration-additional(NS-circumstance(NS-elaboration-object-attribute
that will require only a few spoonfuls per washload .	NS-elaboration-object-attribute)
The move stems from lessons	(NS-background(NN-List(NS-elaboration-object-attribute
learned in Japan	(NS-elaboration-object-attribute
where local competitors have had phenomenal success with concentrated soapsuds .	NS-elaboration-object-attribute))
It also marks P & G 's growing concern	(NS-elaboration-object-attribute
that its Japanese rivals , such as Kao Corp. , may bring their superconcentrates to the U.S .	NS-elaboration-object-attribute))
The Cincinnati consumer-products giant got clobbered two years ago in Japan	(NS-explanation-argumentative(NS-circumstance
when Kao introduced a powerful detergent ,	(NN-Same-Unit(NS-elaboration-additional
called Attack ,	NS-elaboration-additional)
which quickly won a 30 % stake in the Japanese markets .	NN-Same-Unit))
`` They do n't want to get caught again , ''	(NS-attribution
says one industry watcher .	NS-attribution))))
Retailers in Phoenix , Ariz. , say	(NS-elaboration-additional(SN-attribution
P & G 's new powdered detergent	(NN-Same-Unit(NS-elaboration-additional
-- to be called Cheer with Color Guard --	NS-elaboration-additional)
will be on shelves in that market by early November .	NN-Same-Unit))
A P & G spokeswoman confirmed	(NS-elaboration-additional(SN-attribution
that shipments to Phoenix started late last month .	SN-attribution)
She said	(SN-attribution
the company will study results from this market	(SN-circumstance
before expanding to others .	SN-circumstance)))))
Superconcentrates are n't entirely new for P & G .	(NS-elaboration-additional(NS-elaboration-additional(NS-explanation-argumentative
The company introduced a superconcentrated Lemon Cheer in Japan	(NS-reason
after watching the success of Attack .	NS-reason))
When Attack hit the shelves in 1987 ,	(NS-elaboration-additional(SN-result
P & G 's share of the Japanese market fell to about 8 % from more than 20 % .	SN-result)
With the help of Lemon Cheer , P & G 's share is now estimated to be 12 % .	NS-elaboration-additional))
While the Japanese have embraced the compact packaging and convenience of concentrated products ,	(NS-elaboration-additional(NS-elaboration-additional(SN-antithesis
the true test for P & G will be in the $ 4 billion U.S. detergent market ,	(NS-circumstance
where growth is slow	(NN-List
and liquids have gained prominence over powders .	NN-List)))
The company may have chosen to market the product under the Cheer name	(SN-concession(NS-reason
since it 's already expanded its best-selling Tide into 16 different varieties ,	(NS-example
including this year 's big hit , Tide with Bleach .	NS-example))
With superconcentrates , however , it is n't always easy to persuade consumers that less is more ;	(NS-explanation-argumentative
many people tend to dump too much detergent into the washing machine ,	(NS-reason
believing	(SN-attribution
that it takes a cup of powder to really clean the laundry .	SN-attribution)))))
In the early 1980s , P & G tried to launch here a concentrated detergent under the Ariel brand name	(NS-elaboration-additional(SN-concession(NS-elaboration-object-attribute
that it markets in Europe .	NS-elaboration-object-attribute)
But the product ,	(NN-List(NN-Same-Unit(NS-elaboration-additional
which was n't as concentrated as the new Cheer ,	NS-elaboration-additional)
bombed in a market test in Denver	NN-Same-Unit)
and was dropped .	NN-List))
P & G and others also have tried repeatedly to hook consumers on detergent and fabric softener combinations in pouches ,	(NS-explanation-argumentative(SN-concession
but they have n't sold well ,	(NS-concession
despite the convenience .	NS-concession))
But P & G contends	(NN-List(SN-attribution
the new Cheer is a unique formula	(NS-elaboration-object-attribute
that also offers an ingredient	(NS-elaboration-object-attribute
that prevents colors from fading .	NS-elaboration-object-attribute)))
And retailers are expected to embrace the product ,	(NS-explanation-argumentative(NS-reason
in part because it will take up less shelf space .	NS-reason)
`` When shelf space was cheap ,	(NS-elaboration-additional(SN-concession(NS-attribution(SN-circumstance
bigger was better , ''	SN-circumstance)
says Hugh Zurkuhlen , an analyst at Salomon Bros .	NS-attribution)
But with so many brands	(NN-Same-Unit(NS-elaboration-object-attribute
vying for space ,	NS-elaboration-object-attribute)
that 's no longer the case .	NN-Same-Unit))
If the new Cheer sells well ,	(NS-attribution(SN-circumstance(SN-condition
the trend toward smaller packaging is likely to accelerate	(NS-circumstance
as competitors follow with their own superconcentrates .	NS-circumstance))
Then retailers `` will probably push the { less-established } brands out altogether , ''	SN-circumstance)
he says .	NS-attribution)))))))))
Competition is bound to get tougher	(NS-explanation-argumentative(NS-explanation-argumentative(NS-elaboration-additional(NS-condition
if Kao introduces a product like Attack in the U.S .	NS-condition)
To be sure ,	(SN-comment
Kao would n't have an easy time	(NS-circumstance
taking U.S. market share away from the mighty P & G ,	(NS-elaboration-additional
which has about 23 % of the market .	NS-elaboration-additional))))
Kao officials previously have said	(SN-attribution
they are interested in selling detergents in the U.S. ,	(NS-elaboration-additional(SN-antithesis
but so far the company has focused on acquisitions , such as last year 's purchase of Andrew Jergens Co. , a Cincinnati hand-lotion maker .	SN-antithesis)
It also has a product-testing facility in California .	NS-elaboration-additional)))
Some believe	(NS-example(SN-attribution
P & G 's interest in a superconcentrated detergent goes beyond the concern for the Japanese .	SN-attribution)
`` This is something	(NS-attribution(NS-elaboration-object-attribute
P & G would do with or without Kao , ''	NS-elaboration-object-attribute)
says Mr. Zurkuhlen .	NS-attribution)))))
