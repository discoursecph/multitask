Wham !	(NN(NS(NS(SN(NS(SN(SN(NN
Bam !	NN)
Twice in two weeks the unraveling of the on-again , off-again UAL buy-out slammed the stock market .	SN)
Now , stock prices seem to be in a general retreat .	SN)
Since peaking at 2791.41 on Oct. 9 ,	NS)
the Dow Jones Industrial Average has lost 194.69 points , or 7 % ,	(NS
closing Friday at 2596.72 , down 17.01 .	NS))
The number of issues	(NN(NN(NS
falling on the New York Stock Exchange each day	NS)
is eclipsing the number of gainers .	NN)
And the number of stocks	(NN(NS
hitting new lows	NS)
far outstrips the number	(NS
setting new highs .	NS))))
But why should an iffy $ 6.79 billion leveraged buy-out deal shake the foundations of the entire stock market ?	(NN
Opinions vary	(NS(SN(NS
about how important the UAL deal was to the market 's health ,	NS)
but analysts generally agree	(SN
that the market gyrations	(NN(NS
created as the UAL plan crumbled	NS)
revealed a fundamental change in investor psychology .	NN)))
`` If this had happened a few months ago	(NN(NS(NN(NS
when the atmosphere was still very positive	NS)
it would n't have been greeted with anything like the impact it has had over the past two weeks , ''	NN)
says Dennis Jarrett , a market strategist at Kidder Peabody .	NS)
There are , of course , analysts	(NS(NS
who view the near-panic	(NN(NS
that briefly swept through investors on Oct. 13 and again on Oct. 24	NS)
as momentary lapses of good judgment	(NS
that have only temporarily undermined a healthy stock market .	NS)))
Sure , price action is volatile	(NS(NS(SN(NN
and that 's scary ,	NN)
but all-in-all stocks are still a good place to be ,	SN)
they suggest .	NS)
The reaction to the UAL debacle `` is mindless , ''	(NN(NS(NS
says John Connolly , chief market strategist at Dean Witter .	NS)
`` UAL is a small deal	(NS(NS
as far as the overall market is concerned .	NS)
The only way	(NS(NN(NS
you can make it a big deal	NS)
is to draw linkages	(NS
that just do n't make sense . ''	NS))
He suggests , for example ,	(NS(SN
that investors may have assumed	(SN
that	(NN
just because UAL could n't get financing ,	(SN
no leveraged buy-outs can get financing .	SN))))
Carried even further ,	(NS(SN
some investors assumed	(SN
that	(NN
since leveraged buy-outs are the only thing	(NS(SN(NS
propping up stock prices ,	NS)
the market would collapse	SN)
if no more LBOs could be done .	NS))))
`` There will still be deals , ''	(NS(NS(NS
argues Mr. Connolly .	NS)
`` There may not be as many	(NN(NN
and the buyers may not get away with some of the things	(NS
they 've done in the past ,	NS))
but deals wo n't disappear . ''	NN))
He forecasts	(NS(SN
that the emphasis in mergers and acquisitions may soon return to what he calls `` strategic deals ,	(NS
in which somebody is taking over a company	(NS(NS
not to milk the cash flow ,	NS)
but because it 's a good fit . ''	NS)))
And	(NS(NN
even without deals ,	(SN
Mr. Connolly figures	(SN
the market would remain healthy .	SN)))
He notes , for instance ,	(NS(SN
that there has n't been a merger or acquisition among the 30 stocks in the Dow Jones Industrial Average since 1986 ,	(SN
yet that average only three weeks ago hit a record high .	SN))
`` Those stocks are up	(NS(NS(NN
because their earnings are up	(NN
and their dividends are up , ''	NN))
he says .	NS)
Even the volatility	(NS(NN(NS
created by stock index arbitrage and other computer-driven trading strategies	NS)
is n't entirely bad , in Mr. Connolly 's view .	NN)
For the long-term investor	(NS(NN(NS
who picks stocks carefully ,	NS)
the price volatility can provide welcome buying opportunities	(NS
as short-term players scramble frantically	(NS
to sell stocks in a matter of minutes .	NS)))
`` Who can make the better decision , the guy	(NN(NS(NN(NS
who has 10 seconds	(SN
to decide what to do	SN))
or the guy with all the time in the world ? ''	NN)
he says .	NS)
`` What on earth does the UAL deal have to do with the price of Walmart ,	(NS
which I was able to buy on Oct. 16 at a very attractive price ? ''	NS))))))))))))))
Kidder Peabody 's Mr. Jarrett also sees some benefits to the stock market 's recent drop .	(NS
`` We 've run into a market	(NN(NS(NS(NS
that was beginning to run out of steam	(NN
and get frothy , ''	NN))
he says .	NS)
`` The balloon had been blown up so big	(NS(NS
that	(NN
when somebody came along with a pin	(SN(NS
-- in this case the UAL deal --	NS)
we got a little pop . ''	SN)))
The pop sobered up investors	(NS(NS(NS
who had been getting a little too ebullient ,	NS)
says Mr. Jarrett .	NS)
`` It provided an excuse	(NS
for people to get back to reality	(NN
and to look at the economic data , especially the third-quarter economic numbers ,	(NN
and to realize	(SN
that we ca n't continue to gloss over what is going on in the junk bond market . ''	SN)))))))
But he figures	(NS(SN
that at current levels the stock market is comfortably valued ,	(NS(NS
even with the economy obviously slowing .	NS)
`` Just because we 've got some realism back in the market does n't mean it 's going lower from here , ''	(NS(NS
he says .	NS)
`` The bottom line is that it 's healthy to have this kind of sideways activity , especially after a 30 % gain in stock values over the past 12 months . ''	NS)))
He 's now estimating	(NS(SN
that after a period of consolidation , the Dow Jones Industrial Average will once again forge new highs .	SN)
Maybe , maybe not .	(NS
Abby Joseph Cohen , a market strategist at Drexel Burnham Lambert , is n't nearly so sanguine about the market 's chances	(NS(NS
of surging to new highs anytime soon .	NS)
Her view is that stock prices have three major props :	(NS(NS(NS(NS
merger and buy-out proposals , earnings and the economic outlook .	NS)
At current levels of economic activity and earnings , stocks are fairly valued ,	(SN(NS
she says .	NS)
But any chance	(NN(NS
for prices to surge above fair value	NS)
lies in the speculation	(NS(NS
that accompanies a vigorous merger and buy-out business ,	NS)
and UAL has obviously put a damper on that .	NS))))
`` Stocks are n't cheap anymore ,	(NS(NS(NN
there have been some judicial and legislative changes in the merger area	(NN
and all of this changes the arithmetic of deals , ''	NN))
she says .	NS)
`` I 'm not saying	(SN(SN
they 've stopped altogether ,	SN)
but future deals are going to be structured differently	(SN
and bids probably wo n't be as high . ''	SN))))
But that 's not the only problem for stocks .	(NS(NS
The other two props	(NN(NS
-- earnings and the economic outlook --	NS)
are troubling , too .	NN))
`` M & A is getting all the headlines right now ,	(NN(NS(NN
but these other things have been building up more gradually , ''	NN)
she says .	NS)
Third-quarter earnings have been generally disappointing	(NN(SN
and with economic data	(NN(NS
showing a clear slowing ,	NS)
the outlook for earnings in the fourth quarter and all of 1990 is getting worse .	NN))
`` There are a lot more downward than upward revisions	(NS(NS(NN
and it looks like people are questioning corporate profits as a means of support for stock prices , ''	NN)
she says .	NS)
With all this , can stock prices hold their own ?	(NN
`` The question is unanswerable at this point , ''	(NS(SN
she says .	SN)
`` It depends on what happens .	(NS
If the economy slips into a recession ,	(SN
then this is n't a level that 's going to hold . ''	SN))))))))))))))))))))))
Friday 's Market Activity	(NN
Stock prices tumbled for a third consecutive day	(NS(NS
as earnings disappointments , a sluggish economy and a fragile junk bond market continued to weigh on investors .	NS)
The Dow Jones Industrial Average fell 17.01 points to 2596.72 in active trading .	(NN
Volume on the New York Stock Exchange totaled 170,330,000 shares .	(NN
Declining issues on the Big Board were far ahead of gainers , 1,108 to 416 .	(NN
For the week the Dow Jones Industrial Average sank 92.42 points , or 3.4 % .	(NN
Oil stocks escaped the brunt of Friday 's selling	(NN(NS(NS(NN
and several were able to post gains ,	(NS
including Chevron ,	(NS
which rose 5/8 to 66 3/8 in Big Board composite trading of 2.4 million shares .	NS)))
The stock 's advance reflected ongoing speculation	(NS(NS
that Pennzoil is accumulating a stake in the company ,	NS)
according to Dow Jones Professional Investor Report .	NS))
Both companies declined to comment on the rumors ,	(SN
but several industry analysts told the Professional Investor Report	(SN
they believed	(SN
it was plausible that Pennzoil may be buying Chevron shares as a prelude	(NS
to pushing for a restructuring of the company .	NS)))))
USX gained 1/2 to 33 3/8 on a report in Business Week magazine	(NN(NS(NS
that investor Carl Icahn is said to have raised his stake in the oil and steel company to just about 15 % .	NS)
Earlier this month , Mr. Icahn boosted his USX stake to 13.4 % .	NS)
Elsewhere in the oil sector , Exxon rallied 7/8 to 45 3/4 ;	(NN
Amoco rose 1/8 to 47 ;	(NN
Texaco was unchanged at 51 3/4 ,	(NN
and Atlantic Richfield fell 1 5/8 to 99 1/2 .	(NN
Mobil ,	(NN(NN(NS
which said	(SN
it plans to cut its exploration and production work force by about 8 % in a restructuring ,	SN))
dropped 5/8 to 56 1/8 .	NN)
The precious metals sector outgained other Dow Jones industry groups by a wide margin for the second consecutive session .	(NN(NS(NS
Hecla Mining rose 5/8 to 14 ;	(NN
Battle Mountain Gold climbed 3/4 to 16 3/4 ;	(NN
Homestake Mining rose 1 1/8 to 16 7/8 ;	(NN
Lac Minerals added 5/8 to 11 ;	(NN
Placer Dome went up 7/8 to 16 3/4 ,	(NN
and ASA Ltd. jumped 3 5/8 to 49 5/8 .	NN))))))
Gold mining stocks	(NS(NN(NS
traded on the American Stock Exchange	NS)
also showed strength .	NN)
Echo Bay Mines rose 5/8 to 15 7/8 ;	(NN
Pegasus Gold advanced 1 1/2 to 12 ,	(NN
and Corona Class A gained 1/2 to 7 1/2 .	NN))))
Unisys dropped 3/4 to 16 1/4	(NN(NS(SN(NS
after posting a third-quarter loss of $ 4.25 a share ,	(NS
including restructuring charges ,	NS))
but other important technology issues were mixed .	SN)
Compaq Computer ,	(NN(NN(NS
which had lost 8 5/8 Thursday	(NS
following a disappointing quarterly report ,	NS))
gained 5/8 to 100 5/8 .	NN)
International Business Machines dropped 7/8 to 99 7/8 .	(NN
Digital Equipment tacked on 1 1/8 to 89 1/8 ,	(NN
and Hewlett-Packard fell 3/8 to 49 3/8 .	NN))))
Dividend-related trading swelled volume in Merrill Lynch ,	(NN(NS(NS
which closed unchanged at 28 3/8	(NS
as 2.7 million shares changed hands .	NS))
The stock has a 3.5 % dividend yield	(NN
and goes ex-dividend today .	NN))
Erbamont advanced 1 1/8 to 36 1/2 on 1.9 million shares .	(NN(NS
Montedison ,	(NS(NN(NS
which owns about 72 % of the company 's common stock ,	NS)
agreed to buy the rest for $ 37 a share .	NN)
Himont , another majority-owned unit of Montedison , added 1 1/4 to 47 1/8 .	NS))
Milton Roy jumped 2 to 18 3/8 .	(NN(NS(NS
Crane said	(SN
it holds an 8.9 % stake in the company	(SN
and may seek control .	SN)))
Crane dropped 1 1/8 to 21 1/8 .	NS)
Comprehensive Care ,	(NS(NN(NS
which terminated its agreement	(NS
to merge with First Hospital ,	NS))
dropped 7/8 to 3 7/8 .	NN)
The company 's decision was made	(NS
after First Hospital failed to obtain financing for its offer .	NS)))))))))))))))))))))
