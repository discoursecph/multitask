Boeing Co. 's third-quarter profit leaped 68 %	root	9..54
but Wall Street 's attention was focused on the picket line , not the bottom line .	concession	55..136
In fact , the earnings report unfolded	restatement	139..176
as representatives of the world 's No. 1 jet maker and the striking Machinists union came back to the negotiating table for their first meeting in two weeks .	synchrony	177..333
Doug Hammond , the federal mediator in Seattle , where Boeing is based , said the parties will continue to sit down daily until a new settlement proposal emerges or the talks break off again .	entrel	334..522
Despite the progress , Boeing indicated that the work stoppage , now in its 27th day , will have " a serious adverse impact " on the current quarter .	norel	525..669
For the third quarter , net rose to $ 242 million , or $ 1.05 a share , from $ 144 million , or 63 cents a share .	norel	672..778
Sales climbed 71 % to $ 6.36 billion from $ 3.72 billion	conjunction	779..832
as the company capitalized on the ravenous global demand for commercial airliners .	cause	833..915
Because it 's impossible to gauge how long the walkout by 55,000 Machinists rank and file will last , the precise impact on Boeing 's sales , earnings , cash flow and short-term investment position could n't be determined .	norel	918..1134
The investment community , however , strongly believes that the strike will be settled before there is any lasting effect on either Boeing or its work force .	norel	1137..1292
The company 's total firm backlog of unfilled orders at Sept. 30 stood at a mighty $ 69.1 billion , compared with $ 53.6 billion at the end of	entrel	1293..1431
Although the company could see fourth-quarter revenue shrink by nearly $ 5 billion if it is n't able to deliver any more planes this year , those dollars actually would just be deferred until 1990 .	norel	1434..1628
And the company is certain to get out some aircraft with just supervisors and other non-striking employees on hand .	conjunction	1629..1744
Before the union rejected the company 's offer and the strike was launched with the graveyard shift of Oct. 4 , Boeing had been counting on turning 96 aircraft out the door in the present period .	norel	1747..1940
That included 21 of the company 's 747-400 jumbo jets , its most successful product .	restatement	1941..2023
" It 's not a pretty picture , " said David Smith , an analyst with Raymond James & Associates .	norel	2026..2116
" But it would just mean a great first and second quarter next year . "	contrast	2117..2185
Phillip Brannon of Merrill Lynch Capital Markets added : " You do n't want to minimize this and say nobody is looking at it .	norel	2188..2309
But the strike has n't gone on long enough for Boeing to lose business in any real sense . "	contrast	2310..2399
That 's the primary reason the company 's share price has held up so well when , in Mr. Smith 's words , " most companies would have unraveled " by now .	norel	2402..2547
In New York Stock Exchange composite trading , Boeing closed yesterday at $ 54.50 a share , off a scant 12.5 cents .	restatement	2548..2660
Still , Boeing went through its normal verbal gymnastics and played up the downside .	norel	2663..2746
In a statement , Chairman Frank Shrontz asserted that the company " faces significant challenges and risks , " on both its commercial and government contracts .	instantiation	2747..2902
For instance , he noted that spending on Pentagon programs is shrinking , and Boeing is either the prime contractor or a major supplier on many important military projects , including the B-2 Stealth bomber , the V-22 Osprey tilt-rotor aircraft and the Air Force 's next-generation tactical fighter .	instantiation	2905..3199
Because of cost overruns on fixed-price military work , Mr. Shrontz said , the company 's defense business will record " a significant loss " in 1989 .	conjunction	3200..3345
Moreover , Mr. Shrontz added , production-rate increases that have been implemented on the 737 , 747 , 757 and 767 programs have resulted in " serious work force skill-dilution problems . "	conjunction	3348..3530
Suppliers and subcontractors are experiencing heightened pressure to support delivery schedules .	cause	3531..3627
And , of course , there 's the unsteady labor situation .	conjunction	3630..3683
Besides the Machinists pact , accords representing 30,000 of the company 's engineering and technical employees in the Puget Sound and Wichita , Kan. , areas expire in early December .	instantiation	3684..3863
Also , a contract with the United Auto Workers at the company 's helicopter plant in Philadelphia expired Oct. 15 .	conjunction	3864..3976
This contract , covering about 3,000 hourly production and maintenance workers , is being extended on a day-to-day basis .	cause	3977..4096
The Machinists rejected a proposal featuring a 10 % base wage increase over the life of the three-year contract , plus bonuses of 8 % the first year and 3 % the second .	norel	4099..4263
On top of that , Boeing would make cost-of-living adjustments projected to be 5 % for each year of the contract .	concession	4264..4374
The union , though , has called the offer " insulting . "	contrast	4377..4429
The company reiterated yesterday that it 's willing to reconfigure the package , but not add to the substance of it .	contrast	4430..4544
For the nine months , Boeing 's net increased 36 % to $ 598 million , or $ 2.60 a share , from $ 440 million , or $ 1.92 a share .	norel	4547..4666
Sales soared 28 % to $ 15.43 billion from $ 12.09 billion .	conjunction	4667..4722
In a separate matter , the Justice Department yesterday said Boeing agreed to pay the government $ 11 million to settle claims that the company provided inaccurate cost information to the Air Force while negotiating contracts to replace the aluminum skins on the KC-135 tanker aircraft .	norel	4725..5009
The settlement relates to four contracts negotiated from 1982 to 1985 , prosecutors said .	norel	5012..5100
They added that the settlement is the culmination of a 2 1/2-year investigation into the company 's aluminum pricing practices in connection with KC-135s .	conjunction	5101..5254
A Boeing spokesman responded : " All along the company has said there was no grounds for criminal prosecution .	norel	5257..5365
That was borne out by the Justice Department 's decision " to settle the case .	conjunction	5366..5442
