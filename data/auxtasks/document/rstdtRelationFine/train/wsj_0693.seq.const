Although bullish dollar sentiment has fizzled ,	(Topic-Shift(Topic-Drift(interpretation(elaboration-additional(interpretation(explanation-argumentative(concession
many currency analysts say	(attribution
a massive sell-off probably wo n't occur in the near future .	attribution))
While Wall Street 's tough times and lower U.S. interest rates continue to undermine the dollar ,	(antithesis
weakness in the pound and the yen is expected to offset those factors .	antithesis))
`` By default , '' the dollar probably will be able to hold up pretty well in coming days ,	(elaboration-additional(attribution
says Francoise Soares-Kemp , a foreign-exchange adviser at Credit Suisse .	attribution)
`` We 're close to the bottom '' of the near-term ranges ,	(attribution
she contends .	attribution)))
In late Friday afternoon New York trading , the dollar was at 1.8300 marks and 141.65 yen , off from late Thursday 's 1.8400 marks and 142.10 yen .	(interpretation(background(Sequence(Contrast
The pound strengthened to $ 1.5795 from $ 1.5765 .	Contrast)
In Tokyo Monday , the U.S. currency opened for trading at 141.70 yen , down from Friday 's Tokyo close of 142.75 yen .	Sequence)
The dollar began Friday on a firm note ,	(Contrast(explanation-argumentative(manner
gaining against all major currencies in Tokyo dealings and early European trading	(concession
despite reports	(elaboration-object-attribute
that the Bank of Japan was seen unloading dollars around 142.70 yen .	elaboration-object-attribute)))
The rise came	(circumstance
as traders continued to dump the pound after the sudden resignation Thursday of British Chancellor of the Exchequer Nigel Lawson .	circumstance))
But	(Same-Unit(attribution(Same-Unit
once the pound steadied with help from purchases by the Bank of England and the Federal Reserve Bank of New York ,	(circumstance
the dollar was dragged down ,	circumstance))
traders say ,	attribution)
by the stock-market slump	(elaboration-object-attribute
that left the Dow Jones Industrial Average with a loss of 17.01 points .	elaboration-object-attribute))))
With the stock market wobbly and dollar buyers	(List(attribution(Same-Unit(elaboration-object-attribute
discouraged by signs of U.S. economic weakness and the recent decline in U.S. interest rates	(elaboration-object-attribute
that has diminished the attractiveness of dollar-denominated investments ,	elaboration-object-attribute))
traders say	Same-Unit)
the dollar is still in a precarious position .	attribution)
`` They 'll be looking at levels	(elaboration-additional(attribution(elaboration-object-attribute
to sell the dollar , ''	elaboration-object-attribute)
says James Scalfaro , a foreign-exchange marketing representative at Bank of Montreal .	attribution)
While some analysts say	(antithesis(attribution
the dollar eventually could test support at 1.75 marks and 135 yen ,	attribution)
Mr. Scalfaro and others do n't see the currency decisively sliding under support at 1.80 marks and 140 yen soon .	antithesis)))))
Predictions for limited dollar losses are based largely on the pound 's weak state after Mr. Lawson 's resignation and the yen 's inability	(elaboration-additional(circumstance(elaboration-object-attribute
to strengthen substantially	elaboration-object-attribute)
when there are dollar retreats .	circumstance)
With the pound and the yen lagging behind other major currencies ,	(attribution(circumstance
`` you do n't have a confirmation ''	(elaboration-object-attribute
that a sharp dollar downturn is in the works ,	elaboration-object-attribute))
says Mike Malpede , senior currency analyst at Refco Inc. in Chicago .	attribution)))
As far as the pound goes ,	(Topic-Drift(problem-solution(elaboration-additional(evidence(elaboration-additional(circumstance
some traders say	(attribution
a slide toward support at $ 1.5500 may be a favorable development for the dollar this week .	attribution))
While the pound has attempted to stabilize ,	(concession
currency analysts say	(attribution
it is in critical condition .	attribution)))
Sterling plunged about four cents Thursday	(circumstance(Temporal-Same-Time
and hit the week 's low of $ 1.5765	Temporal-Same-Time)
when Mr. Lawson resigned from his six-year post	(Sequence(reason
because of a policy squabble with other cabinet members .	reason)
He was succeeded by John Major ,	(interpretation(elaboration-additional
who Friday expressed a desire for a firm pound	(Problem-Solution
and supported the relatively high British interest rates	(elaboration-object-attribute
that	(Same-Unit(attribution
he said	attribution)
`` are working exactly as intended ''	(purpose
in reducing inflation .	purpose)))))
But the market remains uneasy about Mr. Major 's policy strategy and the prospects for the pound ,	(attribution
currency analysts contend .	attribution)))))
Although the Bank of England 's tight monetary policy has fueled worries	(interpretation(hypothetical(concession(elaboration-object-attribute
that Britain 's slowing economy is headed for a recession ,	elaboration-object-attribute)
it is widely believed that Mr. Lawson 's willingness	(Same-Unit(elaboration-object-attribute
to prop up the pound with interest-rate increases	elaboration-object-attribute)
helped stem pound selling in recent weeks .	Same-Unit))
If there are any signs	(condition(elaboration-object-attribute
that Mr. Major will be less inclined to use interest-rate boosts	(purpose
to rescue the pound from another plunge ,	purpose))
that currency is expected to fall sharply .	condition))
`` It 's fair to say there are more risks for the pound under Major	(Contrast(elaboration-additional(attribution(comparison
than there were under Lawson , ''	comparison)
says Malcolm Roberts , a director of international bond market research at Salomon Brothers in London .	attribution)
`` There 's very little upside to sterling , ''	(antithesis(attribution
Mr. Roberts says ,	attribution)
but he adds	(attribution
that near-term losses may be small	(consequence
because the selling wave	(Same-Unit(elaboration-object-attribute
that followed Mr. Major 's appointment	elaboration-object-attribute)
apparently has run its course .	Same-Unit)))))
But some other analysts have a stormier forecast for the pound ,	(example(reason
particularly because Britain 's inflation is hovering at a relatively lofty annual rate of about 7.6 %	(List
and the nation is burdened with a struggling government and large current account and trade deficits .	List))
The pound likely will fall in coming days	(elaboration-additional(attribution(elaboration-additional
and may trade as low as 2.60 marks within the next year ,	elaboration-additional)
says Nigel Rendell , an international economist at James Capel & Co. in London .	attribution)
The pound was at 2.8896 marks late Friday , off sharply from 2.9511 in New York trading a week earlier .	(interpretation
If the pound falls closer to 2.80 marks ,	(antithesis(attribution(condition
the Bank of England may raise Britain 's base lending rate by one percentage point to 16 % ,	condition)
says Mr. Rendell .	attribution)
But such an increase ,	(Same-Unit(attribution
he says ,	attribution)
could be viewed by the market as `` too little too late . ''	Same-Unit))))))))
The Bank of England indicated its desire	(attribution(manner(elaboration-object-attribute
to leave its monetary policy unchanged Friday	elaboration-object-attribute)
by declining to raise the official 15 % discount-borrowing rate	(elaboration-object-attribute
that it charges discount houses ,	elaboration-object-attribute))
analysts say .	attribution))
Pound concerns aside , the lack of strong buying interest in the yen is another boon for the dollar ,	(explanation-argumentative(attribution
many traders say .	attribution)
The dollar has a `` natural base of support '' around 140 yen	(interpretation(attribution(consequence
because the Japanese currency has n't been purchased heavily in recent weeks ,	consequence)
says Ms. Soares-Kemp of Credit Suisse .	attribution)
The yen 's softness ,	(Same-Unit(attribution
she says ,	attribution)
apparently stems from Japanese investors ' interest	(Same-Unit(elaboration-object-attribute
in buying dollars against the yen	(purpose
to purchase U.S. bond issues	purpose))
and persistent worries about this year 's upheaval in the Japanese government .	Same-Unit))))))
On New York 's Commodity Exchange Friday , gold for current delivery jumped $ 5.80 , to $ 378.30 an ounce , the highest settlement since July 12 .	(Sequence(evaluation
Estimated volume was a heavy seven million ounces .	evaluation)
In early trading in Hong Kong Monday , gold was quoted at $ 378.87 an ounce .	Sequence))
