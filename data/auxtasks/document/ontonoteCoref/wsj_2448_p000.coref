When China opened its doors to foreign investors in 1979 , toy makers from Hong Kong were among the first to march in .	ROOT	0
Today , with about 75 % of the companies ' products being made in China , the chairman of the , Hong Kong Toys CouncilDennis Ting , has suggested a new sourcing label : `` Made in China by Hong Kong Companies . ''	COREF_PREV	1
The toy makers were pushed across the border by rising labor and land costs in the British colony .	COREF_PREV	2
But in the wake of the shootings in Beijing on June 4 , the Hong Kong toy industry is worrying about its strong dependence on China .	COREF_PREV	3
Although the manufacturers stress that production has n't been affected by China 's political turmoil , they are looking for additional sites .	COREF_PREV	4
The toy makers , and their foreign buyers , cite uncertainty about China 's economic and political policies .	COREF_PREV	5
`` Nobody wants to have all his eggs in one basket , '' says David Yeh , chairman and chief executive officer of International Matchbox Group Ltd .	NONE	6
Indeed , Matchbox and other leading Hong Kong toy makers were setting up factories in Southeast Asia , especially in Thailand , long before the massacre .	COREF_PREV	7
Their steps were partly prompted by concern over a deterioration of business conditions in southern China .	COREF_PREV	8
By diversifying supply sources , the toy makers do n't intend to withdraw from China , manufacturers and foreign buyers say .	COREF_PREV	9
It would n't be easy to duplicate quickly the manufacturing capacity built up in southern China during the past decade .	COREF_PREV	10
A supply of cheap labor and the access to Hong Kong 's port , airport , banks and support industries , such as printing companies , have made China 's Guangdong province a premier manufacturing site .	COREF_PREV	11
`` South China is the most competitive source of toys in the world , '' says Henry Hu , executive director of Wah Shing Toys Consolidated Ltd .	COREF_PREV	12
Hong Kong trade figures illustrate the toy makers ' reliance on factories across the border .	COREF_OTHER	13
In 1988 , exports of domestically produced toys and games fell 19 % from 1987 , to HK$ 10.05 billion ( US$ 1.29 billion ) .	COREF_OTHER	14
But re-exports , mainly from China , jumped 75 % , to HK$ 15.92 billion .	COREF_OTHER	15
In 1989 's first seven months , domestic exports fell 29 % , to HK$ 3.87 billion , while re-exports rose 56 % , to HK$ 11.28 billion .	NONE	16
Manufacturers say there is no immediate substitute for southern China , where an estimated 120,000 people are employed by the toy industry .	COREF_OTHER	17
`` For the next few years , like it or not , China is going to be the main supplier , '' says Edmund Young , vice president of Perfecta Enterprises Ltd. , one of the first big Hong Kong toy makers to move across the border .	COREF_PREV	18
In the meantime , as manufacturers and buyers seek new sites , they are focusing mainly on Southeast Asia .	COREF_OTHER	19
Several big companies have established manufacturing joint ventures in Thailand , including Matchbox , Wah Shing and Kader Industrial Co. , the toy manufacturer headed by Mr. Ting .	COREF_OTHER	20
Malaysia , the Philippines and Indonesia also are being studied .	COREF_OTHER	21
With the European Community set to remove its internal trade barriers in 1992 , several Hong Kong companies are beginning to consider Spain , Portugal and Greece as possible manufacturing sites .	COREF_OTHER	22
Worries about China came just as Hong Kong 's toy industry was recovering from a 1987 sales slump and bankruptcy filings by two major U.S. companies , Worlds of Wonder Inc. and Coleco Industries Inc .	COREF_PREV	23
Hong Kong manufacturers say large debt writeoffs and other financial problems resulting from the 1987 difficulties chastened the local industry , causing it to tighten credit policies and financial management .	COREF_PREV	24
The industry regards last year and this year as a period of recovery that will lead to improved results .	COREF_PREV	25
Still , they long for a `` mega-hit '' toy to excite retail sales in the U.S. , Hong Kong 's biggest market for toys and games .	COREF_PREV	26
The closest thing the colony 's companies have to a U.S. mega-hit this year is the Teenage Mutant Ninja Turtles series of action figures manufactured by Playmates Holdings Ltd .	COREF_PREV	27
Introduced in mid-1988 , the 15 - centimeter - tall plastic turtles are based on an American comic book and television series .	COREF_PREV	28
Paul Kwan , managing director of ,Playmates says 10 million Ninja Turtles have been sold , placing the reptilian warriors among the 10 biggest - selling toys in the U.S. .	COREF_PREV	29
Should sales continue to be strong through the Christmas season , which accounts for about 60 % of U.S. retail toy sales , Mr. Kwan said the Ninja Turtles could make 1989 a record sales year for Playmates .	COREF_PREV	30
Other Hong Kong manufacturers expect their results to improve only slightly this year from 1988 .	COREF_OTHER	31
Besides the lack of a fast - selling product , they cite the continued dominance of the U.S. market by Nintendo Entertainment System , an expensive video game made by Nintendo Co. of Japan .	COREF_PREV	32
Nintendo buyers have little money left to spend on other products .	COREF_PREV	33
Many of the toy makers ' problems started well before June 4 as a result of overstrained infrastructure and Beijing 's austerity programs launched late last year .	COREF_OTHER	34
Toy makers complain that electricity in Guangdong has been provided only three days a week in recent months , down from five days a week , as the province 's rapid industrialization has outstripped its generating capacity .	COREF_PREV	35
Manufacturers are upgrading standby power plants .	COREF_PREV	36
Bank credit for China investments all but dried up following June 4 .	COREF_OTHER	37
Also , concern exists that the harder - line Beijing leadership will tighten its control of Guangdong , which has been the main laboratory for the open - door policy and economic reforms .	COREF_OTHER	38
But , toy manufacturers and other industrialists say Beijing will be restrained from tightening controls on export - oriented southern China .	COREF_PREV	39
They say China 's trade deficit is widening and the country is too short of foreign exchange for it to hamper production in Guangdong .	COREF_PREV	40
`` The Chinese leaders have to decide whether they want control or whether the want exports , '' says Mr. Kwan of Playmates .	COREF_PREV	41
