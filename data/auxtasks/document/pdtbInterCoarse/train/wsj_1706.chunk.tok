Dell Computer Corp. , squeezed by price pressure from its larger competitors and delays in its new product line , said its per-share earnings for fiscal 1990 will be half its previous forecasts .	O	9..201
Although the personal computer maker said it expects revenue to meet or exceed previous projections of $ 385 million for the year ending Jan. 28 , 1990 , earnings are expected to be 25 cents to 35 cents a share , down from previous estimates of 50 cents to 60 cents .	O	204..466
Earnings for fiscal 1989 were $ 14.4 million , or 80 cents a share , on sales of $ 257.8 million .	O	467..560
Results for the third quarter ending Oct. 31 , are expected to be released the third week of November , according to Michael Dell , chairman and chief executive officer .	B-expansion	563..729
Mr. Dell said he does n't expect a loss in either the third or fourth quarter , but said third-quarter earnings could be as low as four cents a share .	I-expansion	730..878
In the third quarter last year , Dell had net income of $ 5 million , or 26 cents a share , on sales of $ 75.2 million .	O	879..993
Mr. Dell attributed the earnings slide to new product delays , such as a laptop scheduled for September that wo n't be introduced until early November .	O	996..1145
Some delays have been caused by a shortage of micoprocessors -- notably Intel Corp. 's newest chip , the 486 -- but others apparently have been caused by Dell 's explosive growth and thinly stretched resources .	O	1146..1354
" They 've got a lot of different balls in the air at the same time , " observes Jim Poyner , a computer securities analyst with Dallas-based William K. Woodruff & Co .	B-temporal	1357..1519
Mr. Dell , meanwhile , concedes the company was " definitely too optimistic " in its expectations .	I-temporal	1520..1614
Product delays , however , have left Dell buffeted by harsher competition in its bread-and-butter line of desktop computers , as powerhouse competitors Compaq Computer Corp. and International Business Machines Corp. price their PCs more aggressively .	O	1617..1864
The result has been thinner margins , which have been further eroded by an ambitious research and development effort and rapid overseas expansion .	O	1865..2010
Analyst James Weil of the Soundview Financial Group believes Dell 's response has been to place increased emphasis on product quality , " in an effort to rise above some of that price pressure . "	O	2013..2204
But that has been the key to Compaq 's success , he adds , whereas Dell carved out its market niche as a direct seller of low-cost but reliable computers -- and it might be too late in the game for a shift in strategy .	O	2205..2420
In national over-the-counter trading , Dell closed yesterday at $ 6 a share , down 87.5 cents .	O	2423..2514
