Early this century , diamond mining in the magnificent dunes	(Summary-Evaluation-Topic(Temporal(Cause(Textual-organization(Elaboration
where the Namib Desert meets the Atlantic Ocean	Elaboration)
was a day at the beach .	Textual-organization)
Men would crawl in the sand	(Joint(Cause
looking for shiny stones .	Cause)
It was as easy	(Comparison
as collecting sea shells at Malibu .	Comparison)))
Men are still combing the beach with shovels and hand brushes ,	(Temporal(Comparison(Cause
searching for that unusual glint .	Cause)
But only after a fleet of 336 gargantuan earthmoving vehicles	(Textual-organization(Elaboration
belonging to De Beers Consolidated Mines Ltd. , the world 's diamond kingpins ,	Elaboration)
do their work .	Textual-organization))
Last year , 43 million tons of desert were moved from one dune to another	(Elaboration(Summary-Evaluation-Topic(Summary-Evaluation-Topic(Cause
to recover 934,242 carats ,	Cause)
which comes to 46 tons of sand per carat , or one-fifth gram .	Summary-Evaluation-Topic)
Oh yes , the Atlantic was also pushed back 300 yards .	Summary-Evaluation-Topic)
`` If there 's diamonds out there ,	(Attribution(Cause
we 'll get to them , ''	Cause)
says Les Johns , De Beers 's engineering manager .	Attribution))))
Here ,	(Summary-Evaluation-Topic(Cause(Textual-organization
wedged between shifting dunes and pounding waves at the world 's most inhospitable diamond dig ,	(Elaboration
lies the earth 's most precious jewel box .	Elaboration))
Thanks to centuries of polishing by Mother Nature	(Joint(Temporal(Elaboration
-- first in the gentle current of the Orange River	(Textual-organization(Elaboration
that carried the stones from South Africa 's interior ,	Elaboration)
then in the cold surf of the ocean , and finally in the coarse sands of the desert --	Textual-organization))
98 % of the diamonds uncovered are of gem quality .	Temporal)
While other mines might yield more carats ,	(Joint(Comparison
a higher percentage of them go to industrial use .	Comparison)
Since this treasure chest is too big	(Cause(Cause
to fit in a bank vault ,	Cause)
it has been turned into one .	Cause))))
Months after railway worker Zacharias Lewala first picked up a diamond from the sand in 1908 ,	(Summary-Evaluation-Topic(Comparison(Elaboration(Temporal(Temporal(Temporal
the German colonialists	(Textual-organization(Elaboration
who controlled Namibia	Elaboration)
proclaimed a wide swath of the desert	(Textual-organization(Elaboration
-- about 200 miles north from the Orange River and 60 miles inland from the Atlantic --	Elaboration)
a restricted area , a designation normally reserved for military operations .	Textual-organization)))
When the Germans lost World War I ,	(Temporal
they lost Namibia to South Africa and the diamonds to Ernest Oppenheimer , patriarch of Anglo American Corp. and De Beers .	Temporal))
Today , no one gets in or out of the restricted area	(Cause(Cause
without De Beers 's stingy approval .	Cause)
The mining zone has thus remained one of the most desolate places in Africa .	(Cause
Ghost towns dot the Namib dunes ,	(Cause
proving diamonds are n't forever .	Cause))))
Oranjemund , the mine headquarters , is a lonely corporate oasis of 9,000 residents .	(Joint
Jackals roam the streets at night ,	(Joint(Joint
and gemsbok , hardy antelope with long straight horns , wander in from the desert	(Cause
to drink from water sprinklers .	Cause))
On most days , the desert 's heat and the cool of the ocean combine	(Joint(Cause
to create a mist like a damp rag .	Cause)
The wind ,	(Textual-organization(Elaboration
stinging with sand ,	Elaboration)
never seems to stop .	Textual-organization)))))
Still , miners from all parts of Namibia as well as professional staff from De Beers 's head offices in South Africa and London keep coming .	Comparison)
And Oranjemund boasts attractions besides diamonds .	(Summary-Evaluation-Topic(Cause
There are six video rental shops , three restaurants , one cinema and 34 sports and recreation clubs for everything from cricket to lawn bowling .	(Elaboration
The pride of Oranjemund is the 18-hole golf course	(Cause(Elaboration
-- with the largest sand trap in the world .	Elaboration)
Last year ,	(Cause(Textual-organization(Temporal
when the rising Orange River threatened to swamp the course ,	Temporal)
the same engineers	(Textual-organization(Elaboration
who are pushing back the Atlantic	Elaboration)
rushed to build a wall	(Cause
to hold back the flood .	Cause)))
`` Nothing is too good for our golf course , ''	(Attribution
says Tony George , a mining engineer .	Attribution)))))
Despite fears	(Summary-Evaluation-Topic(Comparison(Elaboration
the mine may be partially nationalized by the new Namibian government	(Cause(Temporal
following next month 's elections	Temporal)
freeing the country from South African control ,	Cause))
De Beers engineers are working	(Elaboration(Cause
to extend the mine 's productive life for another 25 years , from the current estimate of 10 .	Cause)
Huge machines	(Joint(Textual-organization(Elaboration
that look as though they came from the Star Wars desert-battle scene	Elaboration)
lumber among the dunes .	Textual-organization)
Mechanized vacuum cleaners probe the sand like giant anteaters ;	(Joint
a whirring ferris wheellike excavator , with buckets instead of seats , chews through layers of compacted sand ;	(Joint
tracks and conveyor belts ,	(Joint(Textual-organization
shuttling sand to the screening plants ,	(Temporal
criss-cross the beach .	Temporal))
Then there is the artifical sea wall , 600 yards long and 60 yards thick ,	(Elaboration(Elaboration(Elaboration
jutting into the ocean .	Elaboration)
Made of sand ,	(Elaboration(Elaboration
it receives around-the-clock maintainence against the battering waves .	Elaboration)
When the mining in front of the wall is complete ,	(Temporal
it is moved northward .	Temporal)))
A companion jetty	(Elaboration(Textual-organization(Elaboration
that helps hold back the sea	Elaboration)
looks like a rusting junkyard .	Textual-organization)
Engineers first used concrete blocks	(Temporal(Comparison(Cause
to bolster the barrier ,	Cause)
but the ocean tossed them aside like driftwood .	Comparison)
Then someone decided to try broken-down earthmoving equipment	(Summary-Evaluation-Topic(Elaboration
that , inexplicably , held against the waves .	Elaboration)
`` The Caterpillar people are n't too happy	(Cause(Attribution(Temporal
when they see their equipment used like that , ''	Temporal)
shrugs Mr. George .	Attribution)
`` They figure	(Attribution
it 's not a very good advert . ''	Attribution))))))))))))
Despite all these innovations ,	(Comparison
most of the diamonds are still found in the sand	(Elaboration
swept away by the men wielding shovels and brushes	(Elaboration(Elaboration(Elaboration
-- the ignominiously named `` bedrock sweepers ''	(Elaboration
who toil in the wake of the excavators .	Elaboration))
Laboring in blue and gray overalls ,	(Comparison(Elaboration
they are supposed to concentrate on cleaning out crevices ,	(Comparison
and not strain their eyes	(Joint
looking for diamonds .	Joint)))
But	(Textual-organization
should they spy one ,	(Cause
the company will pay a bonus equal to one-third its value .	Cause))))
For these workers at the bottom of the mine 's pay scale , this is usually enough	(Comparison(Elaboration
to overcome the temptation	(Elaboration
to steal	(Elaboration
-- a crime	(Elaboration
that could earn them up to 15 years in jail .	Elaboration))))
Still , employees do occasionally try to smuggle out a gem or two .	(Elaboration
One man wrapped several diamonds in the knot of his tie .	(Elaboration(Joint
Another poked a hole in the heel of his shoe .	(Joint
A food caterer stashed stones in the false bottom of a milk pail .	Joint))
None made it past the body searches and X-rays of mine security .	Elaboration)))))))))))
