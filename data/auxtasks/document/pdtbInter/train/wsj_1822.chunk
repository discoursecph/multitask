Two rules in pending congressional legislation threaten to hinder leveraged buy-outs by raising the price tags of such deals by as much as 10%.	O	9..152
Wall Street is seething over the rules, which would curtail the tax deductibility of debt used in most LBOs.	O	155..263
The provisions, in deficit-reduction bills recently passed by the House and Senate, could further cool the takeover boom that has been the driving force behind the bull market in stocks for much of the 1980s, some tax experts and investment bankers argue.	O	264..519
Indeed, some investment bankers have already started restructuring deals to cope with the expected rules.	O	520..625
Wall Street has all but conceded on the issue and is now lobbying for the less onerous Senate version of one of the provisions.	O	628..755
At issue is the deductibility of certain junk bonds that are used in most LBOs.	B-entrel	758..837
Such high-yield debt is similar to a zero-coupon bond in that it is sold at a discount to face value, with interest accruing instead of being paid to the holder.	B-entrel	838..999
Under current rules, that accrued interest is deductible by the company issuing the debt.	I-entrel	1000..1089
The House version of the legislation would kill that deduction, and label any such debt as equity, which isn't deductible.	B-contrast	1092..1214
The less-rigorous Senate version would defer the deductibility for roughly five years.	I-contrast	1215..1301
"You see these in just about every LBO," said Robert Willens, senior vice president in charge of tax issues at Shearson Lehman Hutton Inc. in New York.	O	1304..1455
"It becomes a source of cash" for the company making the LBO because it gets a deduction and doesn't have to repay the debt for several years.	O	1456..1598
Typically, Mr. Willens estimates, this type of debt makes up 15% to 20% of the financing for LBOs.	B-instantiation	1601..1699
These types of bonds have been used in buy-outs of companies such as RJR Nabisco Inc., Storer Communications Inc. and Kroger Co.	I-instantiation	1700..1828
A second provision passed by the Senate and House would eliminate a rule allowing companies that post losses resulting from LBO debt to receive refunds of taxes paid over the previous three years.	B-instantiation	1831..2027
For example, if a company posted a loss of $100 million from buy-out interest payments, the existing rule would allow the concern to be able to receive a refund from the tax it paid from 1986 through 1989, when it may have been a profitable public company.	I-instantiation	2028..2284
But that rule is being virtually overlooked by Wall Street, which is concentrating on coping with the deduction issue.	O	2287..2405
"Prices for LBOs have to come down if you don't have that feature," argued Lawrence Schloss, managing director for merchant banking at Donaldson, Lufkin & Jenrette Securities Corp. in New York.	O	2406..2599
Several Wall Street officials say the proposed legislation already is having an impact.	O	2602..2689
An investment group led by Chicago's Pritzker family recently lowered a $3.35 billion bid for American Medical International, Beverly Hills, Calif., because of the threat of the legislation.	O	2690..2880
Moreover, one investment banker, who requested anonymity, said his firm didn't raise the ante for a target company earlier this month after a stronger bid emerged from a public company that wasn't concerned about the financing provision.	O	2883..3120
"We would have paid more if we thought that law wasn't going to pass," he said.	O	3121..3200
One possible solution for Wall Street is to increase the equity part of the transaction -- that is, give lenders a bigger stake in the surviving company rather than just interest payments.	B-cause	3203..3391
That would force the buy-out firm and the target company's management to reduce their level of ownership.	I-cause	3392..3497
"The pigs in the trough may have to give a little bit of the slop back and then the deal can go through," said Peter C. Canellos, tax partner at Wachtell, Lipton, Rosen & Katz.	O	3498..3674
Another solution, said a tax lawyer who requested anonymity, is for firms to use convertible bonds that sell at a discount.	O	3677..3800
Since they have a lower interest rate, they wouldn't fall under the junk-bond category that would lose its deductibility.	O	3801..3922
The House version of the bill would make debt non-deductible if it pays five percentage points above Treasury notes, has at least a five-year maturity and doesn't pay interest for at least one year out of the first five.	O	3923..4143
The House version of the bill would make debt non-deductible if it pays five percentage points above Treasury notes, has at least a five-year maturity and doesn't pay interest for at least one year out of the first five.The bill would then declare that the debt is equity and therefore isn't deductible.	B-contrast	4144..4227
The Senate bill would only deny the deduction until interest is actually paid.	I-contrast	4228..4306
Currently, even though the issuer doesn't pay tax, the debt holder is taxed on the accrued interest.	B-concession	4309..4409
But those holders are often foreign investors and tax-exempt pension funds that don't pay taxes on their holdings.	I-concession	4410..4524
The Senate estimates that its version of the provision would yield $17 million the first year and a total of $409 million over five years.	O	4527..4665
The House version would raise slightly more.	O	4666..4710
Even if Wall Street finds ways around the new rules, a Senate aide contends LBOs will become somewhat more difficult.	O	4713..4830
"There's no question it will make LBOs more expensive," he said.	O	4831..4895
"The interest deduction was the engine that made these things more productive.	O	4896..4974
