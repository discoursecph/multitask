The government moved aggressively to open the spigots of federal aid for victims of the California earthquake , but its reservoir of emergency funds must be replenished soon if the aid is to continue .	O	9..208
President Bush signed a disaster declaration covering seven Northern California counties .	B-contingency	211..300
The declaration immediately made the counties eligible for temporary housing , grants and low-cost loans to cover uninsured property losses .	I-contingency	301..440
In addition , an unusually wide array of federal agencies moved to provide specialized assistance .	B-expansion	443..540
The Department of Housing and Urban Development prepared to make as many as 100 vacant houses available for those left homeless , the Agriculture Department was set to divert food from the school-lunch program to earthquake victims , and the Pentagon was providing everything from radio communications to blood transfusions to military police for directing traffic .	B-comparison	541..904
But the pool of federal emergency-relief funds already is running low because of the heavy costs of cleaning up Hurricane Hugo , and Congress will be under pressure to allocate more money quickly .	I-comparison	907..1102
In Hugo 's wake , Congress allocated $ 1.1 billion in relief funds , and White House spokesman Marlin Fitzwater said $ 273 million of that money remains and could be diverted for quick expenditures related to the earthquake .	O	1103..1322
Now , though , enormous costs for earthquake relief will pile on top of outstanding costs for hurricane relief .	O	1325..1434
" That obviously means that we wo n't have enough for all of the emergencies that are now facing us , and we will have to consider appropriate requests for follow-on funding , " Mr. Fitzwater said .	O	1435..1627
The federal government is n't even attempting yet to estimate how much the earthquake will cost it .	O	1628..1726
But Mr. Fitzwater said , " There will be , I think quite obviously , a very large amount of money required from all levels of government . "	O	1727..1861
In Congress , lawmakers already are looking for ways to add relief funds .	O	1864..1936
Money could be added to a pending spending bill covering the Federal Emergency Management Agency , which coordinates federal disaster relief .	O	1937..2077
More likely , relief funds could be added to an omnibus spending bill that Congress is to begin considering next week .	O	2078..2195
But it is n't just Washington 's relief dollars that are spread thin ; its relief manpower also is stretched .	O	2198..2304
FEMA still has special disaster centers open to handle the aftermath of Hugo , and spokesman Russell Clanahan acknowledged that " we 're pretty thin . "	O	2305..2452
Mr. Clanahan says FEMA now possibly may have the heaviest caseload in its history .	O	2453..2535
To further complicate relief efforts , the privately funded American Red Cross also finds itself strapped for funds after its big Hugo operation .	O	2538..2682
" It 's been a bad month money-wise and every other way , " said Sally Stewart , a spokeswoman for the Red Cross .	O	2683..2791
" It just makes it a little rough when you have to worry about the budget . "	B-comparison	2792..2866
The Red Cross has opened 30 shelters in the Bay area , serving 5,000 people .	B-expansion	2867..2942
Twenty-five trucks capable of cooking food were dispatched from other states .	I-expansion	2943..3020
All the precise types of federal aid that will be sent to California wo n't be determined until state officials make specific requests to FEMA , agency officials said .	O	3023..3188
And in the confusion after the earthquake , " the information flow is a little slow coming in from the affected area , " said Carl Suchocki , a FEMA spokesman .	O	3189..3343
Still , some aid is moving westward from Washington almost immediately .	O	3346..3416
HUD officials said they will make available as many as 100 Bay area houses that are under HUD loans but now are vacant after the houses have been inspected to ensure they are sound .	B-expansion	3417..3598
Additional housing vouchers and certificates will be made available , officials said , and some housing and community-development funds may be shifted from other programs or made available for emergency use .	I-expansion	3599..3804
Another federal agency not normally associated with disaster relief -- the Internal Revenue Service -- moved quickly as well .	O	3807..3932
The IRS said it will waive certain tax penalties for earthquake victims unable to meet return deadlines or make payments because of the quake 's devastation .	B-entrel	3933..4089
The agency plans to announce specific relief procedures in the coming days .	I-entrel	4090..4165
And the Treasury said residents of the San Francisco area will be able to cash in savings bonds even if they have n't held them for the minimum six-month period .	O	4168..4328
One advantage that federal officials have in handling earthquake relief is the large number of military facilities in the San Francisco Bay area , facilities that provide a ready base of supplies and workers .	O	4331..4538
Even before the full extent of the devastation was known , Defense Secretary Dick Cheney ordered the military services to set up an emergency command center in the Pentagon and prepare to respond to various FEMA requests for assistance .	O	4539..4774
By yesterday afternoon , Air Force transport planes began moving additional rescue and medical supplies , physicians , communications equipment and FEMA personnel to California .	B-temporal	4777..4951
A military jet flew a congressional delegation and senior Bush administration officials to survey the damage .	I-temporal	4952..5061
And the Pentagon said dozens of additional crews and transport aircraft were on alert " awaiting orders to move emergency supplies . "	O	5062..5193
Two Air Force facilities near Sacramento , and Travis Air Force Base , 50 miles northeast of San Francisco , were designated to serve as medical-airlift centers .	B-expansion	5196..5354
Some victims also were treated at the Letterman Army Medical Center in San Francisco and at the Naval Hospital in Oakland .	B-expansion	5355..5477
In addition , 20 military police from the Presidio , a military base in San Francisco , are assisting with traffic control , and a Navy ship was moved from a naval station at Treasure Island near the Bay Bridge to San Francisco to help fight fires .	I-expansion	5478..5722
To help residents in Northern California rebuild , FEMA intends to set up 17 disaster assistance offices in the earthquake area in the next several days and to staff them with 400 to 500 workers from various agencies , said Robert Volland , chief of the agency 's individual assistance division .	B-entrel	5725..6016
At these offices , earthquake victims will be helped in filling out a one-page form that they will need to qualify for such federal assistance as home-improvement loans and to repair houses .	I-entrel	6017..6206
And federal officials are promising to move rapidly with federal highway aid to rebuild the area 's severely damaged road system .	B-entrel	6209..6337
The Federal Highway Administration has an emergency relief program to help states and local governments repair federally funded highways and bridges seriously damaged by natural disasters .	B-entrel	6338..6526
The account currently has $ 220 million .	B-expansion	6527..6566
And though federal law dictates that only $ 100 million can be disbursed from that fund in any one state per disaster , administration officials expect Congress to move in to authorize spending more now in California .	I-expansion	6567..6782
To get that money , states must go through an elaborate approval process , but officials expect red tape to be cut this time .	O	6785..6908
Keith Mulrooney , special assistant to Federal Highway Administrator Thomas Larson , also said that after the 1971 San Fernando earthquake in Southern California , the state set tougher standards for bridges , and with federal aid , began a program to retrofit highways and bridges for earthquake hazards .	O	6911..7211
The first phase of the program has been completed , but two other phases are continuing .	O	7212..7299
The two major structures that failed Tuesday night , he said , were both built well before the 1971 earthquake -- the San Francisco Bay Bridge , completed in the 1930s , and the section of I-880 , built in the 1950s .	B-entrel	7302..7513
The I-880 section had completed the first phase of the retrofitting .	I-entrel	7514..7582
Laurie McGinley contributed to this article .	O	7585..7629
