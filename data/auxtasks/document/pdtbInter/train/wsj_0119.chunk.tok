Some Democrats in Congress are warning that a complicated new funding device for the two federal antitrust agencies could result in further cutbacks in a regulatory area already reduced sharply in recent years .	O	9..219
The funding mechanism , which has received congressional approval and is expected to be signed by President Bush , would affect the antitrust operations of the Justice Department and the Federal Trade Commission .	O	222..432
As a part of overall efforts to reduce spending , Congress cut by $ 30 million the Bush administration 's request for antitrust enforcement for fiscal 1990 , which began Oct. 1 .	B-contrast	435..608
To offset the reduction , Congress approved a $ 20,000 fee that investors and companies will have to pay each time they make required filings to antitrust regulators about mergers , acquisitions and certain other transactions .	I-contrast	609..832
Some Democrats , led by Rep. Jack Brooks ( D. , Texas ) , unsuccessfully opposed the measure because they fear that the fees may not fully make up for the budget cuts .	O	835..997
But Justice Department and FTC officials said they expect the filing fees to make up for the budget reductions and possibly exceed them .	O	1000..1136
" It could operate to augment our budget , " James Rill , the Justice Department 's antitrust chief , said in an interview .	O	1137..1254
Under measures approved by both houses of Congress , the administration 's request for $ 47 million for the Antitrust Division would be cut $ 15 million .	B-conjunction	1257..1406
Under measures approved by both houses of Congress , the administration 's request for $ 47 million for the Antitrust Division would be cut $ 15 million .	B-conjunction	1257..1406
The FTC budget request of $ 70 million , about $ 34 million of which would go for antitrust enforcement , would also be cut by $ 15 million .	B-conjunction	1407..1542
The administration had requested roughly the same amount for antitrust enforcement for fiscal 1990 as was appropriated in fiscal 1989 .	I-conjunction	1543..1677
The offsetting fees would apply to filings made under the Hart-Scott-Rodino Act .	B-entrel	1680..1760
Under that law , parties proposing mergers or acquisitions valued at $ 15 million or more must notify FTC and Justice Department antitrust regulators before completing the transactions .	B-entrel	1761..1944
Currently , the government charges nothing for such filings .	I-entrel	1945..2004
Proponents of the funding arrangement predict that , based on recent filing levels of more than 2,000 a year , the fees will yield at least $ 40 million this fiscal year , or $ 10 million more than the budget cuts .	O	2007..2216
" When you do that , there is not a cut , but there is in fact a program increase of $ 5 million " each for the FTC and the Justice Department , Rep. Neal Smith ( D. , Iowa ) said during House debate .	O	2217..2408
But Rep. Don Edwards ( D. , Calif . ) responded that a recession could stifle merger activity , reducing the amount of fees collected .	B-asynchronous	2411..2540
The antitrust staffs of both the FTC and Justice Department were cut more than 40 % in the Reagan administration , and enforcement of major merger cases fell off drastically during that period .	B-entrel	2541..2732
" Today is not the time to signal that Congress in any way sanctions the dismal state into which antitrust enforcement has fallen , " Mr. Edwards argued .	I-entrel	2733..2883
Any money in excess of $ 40 million collected from the fees in fiscal 1990 would go to the Treasury at large .	O	2886..2994
Corporate lawyers said the new fees would n't inhibit many mergers or other transactions .	B-entrel	2997..3085
Though some lawyers reported that prospective acquirers were scrambling to make filings before the fees take effect , government officials said they had n't noticed any surge in filings .	I-entrel	3086..3270
