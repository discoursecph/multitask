The estimates of real gross national product prepared by the Bureau of Economic Analysis in the Department of Commerce significantly understate the rate of economic growth .	B-contingency	9..181
Since the bureau 's estimates for the business sector provide the numerator for the productivity ratios calculated by the Department of Labor , underestimated growth rates artificially depress official productivity statistics .	I-contingency	182..406
If this thesis is correct , it has important implications for macroeconomic policies : It may lower the sense of urgency behind efforts to enact tax incentives and other measures to increase the rate of growth in productivity and real GNP .	O	409..646
It would also affect the perceptions of the board of governors of the Federal Reserve System , and the informed public generally , as to what constitutes a reasonable degree of price stability .	O	647..838
In the early 1980s , I predicted a significant acceleration in productivity growth over the rest of the decade .	B-contingency	841..951
This forecast was based on the apparent reversal of most of the negative forces -- such as demographic changes , the oil shock and accelerating inflation -- that had reduced productivity gains in the 1970s .	I-contingency	952..1157
There has indeed been more than a one percentage point improvement in productivity growth since 1981 .	O	1158..1259
But I had expected more , which is one reason I began looking at evidence suggesting defects in the official output estimates .	O	1260..1385
The evidence does not clearly support the view that the downward bias in output growth has become greater during the 1948-89 period , but all I am claiming is that the growth trend is understated .	B-comparison	1386..1581
( It is , however , possible , that further study will reveal increasing bias . )	I-comparison	1582..1657
This bias is in no way deliberate .	B-expansion	1660..1694
The understatement of growth is due largely to the conservative expedients adopted to deal with deficiencies in basic economic data .	I-expansion	1695..1827
The first of three major sources of error is the use of labor input estimates ( mainly employment or hours ) instead of output estimates for those sectors , such as governments , paid household services and private non-profit institutions , where there are difficulties in assembling output data .	B-contingency	1830..2121
This means that no allowance is made for possible increases in output per unit of labor .	B-comparison	2122..2210
In an unrelated program in which the Labor Department does estimate output per employee for more than two-thirds of federal civilian employees , it found an average annual rate of productivity improvement of 1.7 % during the 1980s .	B-contingency	2211..2440
Even if it is assumed that productivity rose no more than half as quickly in the rest of the nonbusiness sector , this Labor Department estimate indicates a downward bias in the real GNP estimates of 0.2 percentage point a year , on average .	I-contingency	2441..2680
The federal productivity estimators use labor input , rather than output , data for their calculations of half of private financial and service industries as well .	B-comparison	2683..2844
Independent estimates of output in those industries , including one by the Department of Labor for banking , suggests that productivity in finance and services appears to have risen by an average of at least 1.5 % a year between 1948 and 1988 .	I-comparison	2845..3085
Because finance and services contribute 10 % to final business product , missing these productivity improvements depresses the overall growth rate by 0.15 % a year .	O	3086..3247
The second source of error in growth statistics is the use of inappropriate deflators to adjust for price changes .	O	3250..3364
I estimate that these mismeasurements as detailed by Martin N. Baily and Robert J. Gordon add a further 0.12 percentage point to the downward bias in the growth rate of real business product .	O	3365..3556
Finally , the official estimates understate growth because they make inadequate allowance for improvements in quality of goods and services .	B-expansion	3559..3698
In 1985 , a new price index for computers adjusted for changes in performance characteristics was introduced , and that resulted in a significantly larger increase in real outlays for durable goods than the earlier estimates had showed .	I-expansion	3699..3933
Since then , further research argues that failure to take account of quality improvements has contributed a total of at least 0.26 percentage point to the downward bias in the growth rate .	O	3934..4121
In sum , the biases ennumerated above indicate a 0.7 percentage point understatement in growth of total real GNP .	B-comparison	4124..4236
For the private domestic business economy , the bias was a bit over 0.5 percentage point .	B-expansion	4237..4325
In other words , the growth rates of both total GNP and real private business product per labor hour have been underestimated by about 20 % .	I-expansion	4326..4464
Mr. Kendrick is professor emeritus of economics at George Washington University .	B-entrel	4467..4547
He is co-author of " Personal Productivity : How to Increase Your Satisfaction in Living " ( M.E. Sharp , 1988 ) .	I-entrel	4548..4655
