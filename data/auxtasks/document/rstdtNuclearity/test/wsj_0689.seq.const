Wanted :	(NS(NS
An investment	(NS
that 's as simple and secure as a certificate of deposit	(NN
but offers a return	(NS
worth getting excited about .	NS))))
With $ 150 billion of CDs	(NS(NS(NS(SN(NS(NS(SN(SN(NS
maturing this month ,	NS)
a lot of people have been scouring the financial landscape for just such an investment .	SN)
In April ,	(SN(SN(SN(NN(NS
when many of them bought their CDs ,	NS)
six-month certificates were yielding more than 9 % ;	NN)
investors	(NN(NS
willing to look	NS)
could find double-digit yields at some banks and thrifts .	NN))
Now , the nationwide average yield on a six-month CD is just under 8 % ,	(NS
and 8.5 % is about the best around .	NS))
But investors	(NN(NS
looking for alternatives	NS)
are n't finding it easy .	NN)))
Yields on most fixed-income securities are lower than several months ago .	(NN
And the stock market 's recent gyrations are a painful reminder of the dangers there .	NN))
`` If you 're looking for a significantly higher yield with the same level of risk as a CD ,	(NS(SN
you 're not going to find it , ''	SN)
says Washington financial planner Dennis M. Gurtz .	NS))
There are , however , some alternatives	(NS(NS
that income-oriented investors should consider ,	NS)
investment advisers say .	NS))
Short-term municipal bonds , bond funds and tax-deferred annuities are some of the choices	(NS(NS
they mention	NS)
-- and not just as a way	(NS
to get a higher return .	NS)))
In particular ,	(SN(NN(NS
advisers say ,	NS)
investors may want to look at securities	(NS
that reduce the risk	(NS(NS
that CD holders are confronting right now ,	NS)
of having to reinvest the proceeds of maturing short-term certificates at lower rates .	NS)))
A mix of CDs and other holdings may make the most sense .	(NS
`` People should remember	(NS(SN
their money is n't all or nothing	(NS
-- they do n't need to be shopping for the one interest-rate-type investment and putting all their money in it , ''	NS))
says Bethesda , Md. , adviser Karen Schaeffer .	NS))))
Here 's a look at some of the alternatives :	(NS
SHORT-TERM MUNICIPALS :	(NN(NS
Investors with a heavy tax load should take out their calculators .	(SN(NS
Yields on municipal bonds can be higher than after-tax yields on CDs for maturities of perhaps one to five years .	(NS(NS
That 's because municipal-bond interest is exempt from federal income tax	(NS
-- and from state and local taxes too , for in-state investors .	NS))
For an investor	(NN(NS
paying tax at a 33 % rate ,	NS)
a seemingly puny 6 % yield on a one-year muni is equivalent to a taxable 9 % .	(NS
Rates approach 6.5 % on five-year municipals .	NS))))
Some of the more cautious CD holders might like `` pre-refunded '' municipals .	(NS(NS(NS
These securities get top credit ratings	(NS
because the issuers have put aside U.S. bonds	(NS
that will be sold	(NS
to pay off holders	(NS
when the municipals are retired .	NS)))))
`` It 's a no-brainer :	(NS(NS
You do n't have to worry about diversification ;	(NN
you do n't have to worry about quality , ''	NN))
says Steven J. Hueglin , executive vice president of the New York bond firm of Gabriele , Hueglin & Cashman Inc .	NS))
Consider a `` laddered '' bond portfolio , with issues	(NS(NS(NS
maturing in , say , 1992 , 1993 and 1994 ,	NS)
advises Malcolm A. Makin , a Westerly , R.I. , financial planner .	NS)
The idea is to have money rolling over each year at prevailing interest rates .	NS))))
BOND FUNDS :	(NN(NS
Bond mutual funds offer diversification	(NS(NS(NN
and are easy to buy and sell .	NN)
That makes them a reasonable option for investors	(NS(NS
who will accept some risk of price fluctuation	(NS
in order to make a bet	(NS
that interest rates will decline over the next year or so .	NS)))
Buyers can look forward to double-digit annual returns	(NN(NS
if they are right .	NS)
But they will have disappointing returns or even losses	(NS
if interest rates rise instead .	NS))))
Bond resale prices , and thus fund share prices , move in the opposite direction from rates .	(NS(NS(NS
The price movements get bigger	(NS
as the maturity of the securities lengthens .	NS))
Consider , for instance , two bond funds from Vanguard Group of Investment Cos .	(NS(NS
that were both yielding 8.6 % on a recent day .	NS)
The Short Term Bond Fund , with an average maturity of 2 1/2 years , would deliver a total return for one year of about 10.6 %	(NN(NN(NS
if rates drop one percentage point	NS)
and a one-year return of about 6.6 %	(NS
if rates rise by the same amount .	NS))
But , in the same circumstances , the returns would be a more extreme 14.6 % and 2.6 % for the Vanguard Bond Market Fund , with its 12 1/2-year average maturity .	NN)))
`` You get equity-like returns '' from bonds	(NS(NS(NS
if you guess right on rates ,	NS)
says James E. Wilson , a Columbia , S.C. , planner .	NS)
If interest rates do n't change ,	(SN
bond fund investors ' returns will be about equal to the funds ' current yields .	SN)))))
DEFERRED ANNUITIES :	(NN(NS
These insurance company contracts feature some of the same tax benefits and restrictions as non-deductible individual retirement accounts :	(NS(NS(NS
Investment gains are compounded	(NN(NS(NS
without tax consequences	NS)
until money is withdrawn ,	NS)
but a 10 % penalty tax is imposed on withdrawals	(NS
made before age 59 1/2 .	NS)))
Aimed specifically at CD holders are so-called CD-type annuities , or certificates of annuity .	(NS(NS
An interest rate is guaranteed for between one and seven years ,	(NS
after which holders get 30 days	(NS
to choose another guarantee period	(NS(NN
or to switch to another insurer 's contract	NN)
without the surrender charges	(NS
that are common to annuities .	NS)))))
Some current rates exceed those on CDs .	(NS
For instance , a CD-type annuity from North American Co. for Life & Health Insurance , Chicago , offers 8.8 % interest for one year or a 9 % rate for two years .	NS)))
Annuities are rarely a good idea at age 35	(SN(NS
because of the withdrawal restrictions .	NS)
But at age 55 , `` they may be a great deal , ''	(NS
says Mr. Wilson , the Columbia , S.C. , planner .	NS))))
MONEY MARKET FUNDS :	(NS
That 's right ,	(NS(SN
money market mutual funds .	SN)
The conventional wisdom is to go into money funds	(NS(NS(NS(NS(NN(NS
when rates are rising	NS)
and shift out at times such as the present ,	(NS
when rates seem headed down .	NS))
With average maturities of a month or so , money funds offer fixed share prices and floating returns	(NN(NS
that track market interest rates ,	NS)
with a slight lag .	NN))
Still , today 's highest-yielding money funds may beat CDs over the next year	(NS(NS(NS
even if rates fall ,	NS)
says Guy Witman , an editor of the Bond Market Advisor newsletter in Atlanta .	NS)
That 's because top-yielding funds currently offer yields almost 1 1/2 percentage points above the average CD yield .	NS))
Mr. Witman likes the Dreyfus Worldwide Dollar Money Market Fund , with a seven-day compound yield just under 9.5 % .	(NS
A new fund , its operating expenses are being temporarily subsidized by the sponsor .	NS))
Try combining a money fund and an intermediate-term bond fund as a low-risk bet on falling rates ,	(NS(NS
suggests Back Bay Advisors Inc. , a mutual fund unit of New England Insurance Co .	NS)
If rates unexpectedly rise ,	(SN
the increasing return on the money fund will partly offset the lower-than-expected return from the bond fund .	SN)))))))))))
