The chemical industry is expected to report	(elaboration-additional(Contrast(evaluation(Topic-Comment(elaboration-additional(circumstance(attribution
that profits eroded in the third quarter	(consequence
because of skidding prices in the commodity end of the business .	consequence))
Producers of commodity chemicals , the basic chemicals	(Same-Unit(elaboration-object-attribute
produced in huge volumes for other manufacturers ,	elaboration-object-attribute)
have seen sharp inventory cutting by buyers .	Same-Unit))
Once the chief beneficiaries of the industry 's now fading boom , these producers also will be reporting against exceptionally strong performances in the 1988 third quarter .	elaboration-additional)
`` For some of these companies , this will be the first quarter with year-to-year negative comparisons , ''	(example(hypothetical(attribution
says Leonard Bogner , a chemical industry analyst at Prudential Bache Research .	attribution)
`` This could be the first of five or six down quarters . ''	hypothetical)
Perhaps most prominent , Dow Chemical Co. ,	(evaluation(explanation-argumentative(attribution(Same-Unit(elaboration-additional
which as of midyear had racked up eight consecutive record quarters ,	elaboration-additional)
is expected to report	Same-Unit)
that profit decreased in the latest quarter from a year earlier , if only by a shade .	attribution)
Though Dow has aggressively diversified into specialty chemicals and pharmaceuticals ,	(concession
the company still has a big stake in polyethylene ,	(definition
which is used in packaging and housewares .	definition)))
Analysts ' third-quarter estimates for the Midland , Mich. , company are between $ 3.20 a share and $ 3.30 a share ,	(elaboration-additional(comparison
compared with $ 3.36 a year ago ,	(circumstance
when profit was $ 632 million on sales of $ 4.15 billion .	circumstance))
A Dow spokeswoman declined to comment on the estimates .	elaboration-additional))))
At the investment firm of Smith Barney , Harris Upham & Co. , the commodity-chemical segment is seen	(List(explanation-argumentative(manner
pulling down overall profit for 20 companies representative of the whole industry by 8 % to 10 % .	manner)
`` You will find the commodities off more than the others and the diversified companies about even or slightly better , ''	(attribution
says James Wilbur , a Smith Barney analyst .	attribution))
First Boston Corp. projects	(explanation-argumentative(attribution
that 10 of the 15 companies it follows will report lower profit .	attribution)
Most of the 10 have big commodity-chemical operations .	explanation-argumentative)))
Still , some industry giants are expected to report continuing gains ,	(explanation-argumentative
largely because so much of their business is outside commodity chemicals .	(example
Du Pont Co. is thought to have had steady profit growth in white pigments , fibers and polymers .	(List(elaboration-additional(evaluation(Contrast(elaboration-additional
Moreover , the Wilmington , Del. , company is helped	(contingency
when prices weaken on the commodity chemicals	(Same-Unit(elaboration-object-attribute
it buys for its own production needs ,	elaboration-object-attribute)
such as ethylene .	Same-Unit)))
Analysts are divided	(attribution
over whether Du Pont will report much of a gain in the latest quarter from its Conoco Inc. oil company .	attribution))
The estimates for Du Pont range from $ 2.25 to $ 2.45 a share .	(comparison
In the 1988 third quarter , the company earned $ 461 million , or $ 1.91 a share , on sales of $ 7.99 billion .	comparison))
Du Pont declined to comment .	elaboration-additional)
Monsanto Co. , too , is expected to continue reporting higher profit ,	(elaboration-additional(evaluation(elaboration-additional(concession
even though its sales of crop chemicals were hurt in the latest quarter by drought in northern Europe and the western U.S .	concession)
The St. Louis-based company is expected to report again	(interpretation(attribution
that losses in its G.D. Searle & Co. pharmaceutical business are narrowing .	attribution)
Searle continued to operate in the red through the first half of the year ,	(antithesis
but Monsanto has said	(attribution
it expects Searle to post a profit for all of 1989 .	attribution))))
Most estimates for Monsanto run between $ 1.70 and $ 2 a share .	(comparison
A year ago , the company posted third-quarter profit of $ 116 million , or $ 1.67 a share , on sales of $ 2.02 billion .	comparison))
Monsanto declined to comment .	elaboration-additional)))))
But the commodity-chemical producers are caught on the downside of a pricing cycle .	(example(evaluation(explanation-argumentative
By some accounts on Wall Street and in the industry , the inventory reductions are near an end ,	(Contrast(interpretation
which may presage firmer demand .	interpretation)
But doubters say	(attribution
growing production capacity could keep pressure on prices into the early 1990s .	attribution)))
In the latest quarter , at least , profit is expected to fall sharply .	evaluation)
For Himont Inc. , `` how far down it is ,	(elaboration-additional(List(elaboration-additional(evaluation(attribution(attribution
we do n't know , ''	attribution)
says Leslie Ravitz at Salomon Brothers .	attribution)
The projections are in the neighborhood of 50 cents a share to 75 cents ,	(comparison
compared with a restated $ 1.65 a share a year earlier ,	(circumstance
when profit was $ 107.8 million on sales of $ 435.5 million .	circumstance)))
Himont faces lower prices for its mainstay product , polypropylene ,	(background(elaboration-object-attribute(Temporal-Same-Time
while it goes forward with a heavy capital investment program	Temporal-Same-Time)
to bolster its raw material supply	(List
and develop new uses for polypropylene ,	(elaboration-additional
whose markets include the packaging and automobile industries .	elaboration-additional)))
The company ,	(Same-Unit(elaboration-additional
based in Wilmington , Del. ,	elaboration-additional)
is 81 % -owned by Montedison S.p. A. , Milan ,	(elaboration-additional
which has an offer outstanding for the Himont shares	(elaboration-object-attribute
it does n't already own .	elaboration-object-attribute)))))
At Quantum Chemical Corp. , New York , the trouble is lower prices for polyethylene , higher debt costs and the idling of an important plant due to an explosion .	(List(elaboration-additional(evaluation
Some analysts hedge their estimates for Quantum ,	(Contrast(explanation-argumentative
because it is n't known when the company will book certain one-time charges .	explanation-argumentative)
But the estimates range from break-even to 35 cents a share .	Contrast))
In the 1988 third quarter , Quantum earned $ 99.8 million , or $ 3.92 a share , on sales of $ 724.4 million .	elaboration-additional)
Another big polyethylene producer , Union Carbide Corp. , is expected to post profit of between $ 1 a share and $ 1.25 ,	(comparison
compared with $ 1.56 a share a year earlier ,	(circumstance
when the company earned $ 213 million on sales of $ 2.11 billion .	circumstance))))
Himont , Quantum and Union Carbide all declined to comment .	elaboration-additional)))
