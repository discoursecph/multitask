In a move that could pose a new competitive challenge to Time Warner Inc. 's powerful Home Box Office , cable giant Tele-Communications Inc. agreed to buy half of Showtime Networks Inc. from Viacom Inc. for $ 225 million .	root	9..228
The purchase comes after nearly three years of on-again off-again talks between TCI and Viacom , which has also discussed the sale of an interest in Showtime with other cable operators .	conjunction	231..415
Showtime is a distant No. 2 to Home Box Office , and in May filed a $ 2.5 billion antitrust suit against Time Warner , charging the company and its HBO and American Television cable units with conspiring to monopolize the pay TV business .	entrel	416..651
HBO has close to 24 million subscribers to its HBO and Cinemax networks	norel	654..725
while Showtime and its sister service , The Movie Channel , have only about 10 million , according to Paul Kagan Associates , a Carmel , Calif. , research firm .	contrast	726..881
For TCI , the investment in Showtime puts it in an unusual position ; as the largest cable operator , with control of close to 12 million of the nation 's 52 million cable subscribers , TCI is HBO 's largest customer .	norel	884..1095
But TCI President John Malone has long been concerned about HBO 's dominance of the pay TV business , and has been eager to keep Showtime as a healthy competitor .	contrast	1096..1256
" It is important to the cable industry that we have a vibrant and competitive pay-television marketplace , " Mr. Malone said in a statement .	cause	1257..1395
In a telephone interview , Robert Thomson , TCI senior vice president , said Showtime 's suit against HBO " does n't involve us , and nothing we 're doing here bears any relationship to that . "	norel	1398..1582
He added , " We do n't intend to be drawn into it , " noting that TCI wo n't play any active role in the management of Showtime .	conjunction	1583..1705
Linking up Showtime with the largest cable operator in the U.S. could sharply boost its subscribers .	norel	1708..1808
TCI said it may bring in other cable operators as investors , a practice it has employed in the past with investments in other cable networks , such as The Discovery Channel .	entrel	1809..1981
Additional cable partners could boost subscribers even further .	entrel	1982..2045
Time Warner declined comment .	norel	2048..2077
In addition to owning HBO , Time Warner owns American Television & Communications Inc. , the nation 's second largest cable operator after TCI .	entrel	2078..2218
Viacom also owns cable systems	conjunction	2219..2249
but it is the 14th largest operator of such systems , with less than one million subscribers .	contrast	2250..2343
The TCI investment is a big victory for Viacom 's chief executive officer , Frank Biondi , and Winston H. Cox , president of the Showtime unit .	norel	2346..2485
" This takes any question of Showtime 's viability and puts it away once and for all , " Mr. Biondi said in a telephone interview .	cause	2486..2612
The fight between HBO and Showtime is particularly acrimonious	norel	2615..2677
because Mr. Biondi is the former chief executive of HBO , and Mr. Cox served as chief of marketing for the service .	cause	2678..2792
They were both hired by Sumner Redstone , the Boston billionaire who took control of Viacom three years ago in a leveraged buy-out .	entrel	2793..2923
Time Warner has vigorously denied all of Viacom 's allegations .	norel	2926..2988
