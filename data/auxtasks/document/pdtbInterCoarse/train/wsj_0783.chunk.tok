If you 're still wondering about the causes of the slump in the junk-bond market , consider the case of Columbia Savings & Loan .	B-expansion	9..135
The California thrift has just announced a $ 226 million third-quarter loss .	I-expansion	136..211
How did this happen ?	O	212..232
Well , when Congress in its recent S&L bailout mandated that the thrifts sell off all their junk-bond holdings by 1994 , it not only artificially increased the supply of these bonds in the market but also eliminated one of the few profitable investments thrifts have made .	O	235..505
But there is a grimly ironic twist to the Columbia loss .	O	506..562
As followers of the debate over a capital-gains tax cut know , there is much talk in Congress and indeed all over Washington about the need to " encourage " long-term investment and discourage the financial sector 's presumed obsession with the short term .	B-entrel	565..817
Now , we regard this as a largely phony issue , but the " long term " is nonetheless a big salon topic all around the Beltway .	B-expansion	818..940
It turns out that Columbia had this huge loss in large part because the new congressionally mandated rules forced it to adjust the book value of its soon-to-be-sold junk bonds to the lower of either their cost or market value .	B-contingency	941..1167
They could no longer be classified as what Columbia regarded them , namely long-term investments .	I-contingency	1168..1264
Congress 's ham-handed treatment of the existing structure of junk-bond holdings reminds us of a story in the Journal earlier this year about the Baby Bell companies ' desire to have the court-ordered bans lifted on offering information services .	B-expansion	1267..1511
The issue of seeking relief from Congress was raised to Delbert Staley , the chairman of Nynex .	I-expansion	1512..1606
Mr. Staley replied : " Legislation tends to be compromised .	O	1607..1666
I believe we have to take a shot at getting as much done as we can through the court , through Justice and through state and federal regulatory agencies .	B-expansion	1667..1819
I see Congress as a last resort . "	B-entrel	1820..1853
Healthy thrifts such as Columbia or the junk-bond market itself should have been so lucky .	B-expansion	1854..1944
The reality of life in modern America is that if you want to wreck something that works , let it fall into the hands of Congress .	I-expansion	1945..2073
