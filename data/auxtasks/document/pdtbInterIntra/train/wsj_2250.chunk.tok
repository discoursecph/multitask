What does n't belong here ?	root	9..34
A. manual typewriters , B. black-and-white snapshots , C. radio adventure shows .	norel	37..119
If you guessed black-and-white snapshots , you 're right .	norel	122..177
After years of fading into the background , two-tone photography is coming back .	cause	178..257
Trendy magazine advertisements feature stark black-and-white photos of Hollywood celebrities pitching jeans , shoes and liquor .	norel	260..386
Portrait studios accustomed to shooting only in color report a rush to black-and-white portrait orders .	conjunction	387..490
And black-and-white photography classes are crowded with students .	conjunction	491..557
What 's happening in photography mirrors the popularity of black and white in fashion , home furnishings and cinematography .	norel	560..682
On Seventh Avenue , designers have been advancing the monochrome look with clothing collections done entirely in black and white .	instantiation	683..811
And classic black-and-white movies are enjoying a comeback on videocassette tapes , spurred , in part , by the backlash against colorization of old films .	conjunction	812..963
" The pendulum is swinging back to black and white , " says Richard DeMoulin , the general manager of Eastman Kodak Co. 's professional photography division .	norel	966..1119
Until two years ago , sales of black-and-white film had been declining steadily since the 1960s .	norel	1122..1217
But last year , buoyed by increased use in advertising and other commercial applications , sales increased 5 %	contrast	1218..1325
and they are expected to jump at least that much again this year .	conjunction	1326..1392
Photographic companies are scrambling to tap the resurging market , reviving some black-and-white product lines and developing new ones .	norel	1395..1530
At Kodak , which largely ignored the market for years , black-and-white film sales now account for nearly 15 % of the company 's $ 3 billion in film and paper sales annually , up from 10 % three years ago .	instantiation	1531..1729
The Rochester , N.Y. , photographic giant recently began marketing T-Max 3200 , one of the fastest and most sensitive monochrome films .	norel	1732..1864
Aimed at commercial photographers , the film can be used in very low light without sacrificing quality , says Donald Franz of Photofinishing Newsletter .	entrel	1865..2015
Also trying to snare a portion of the $ 2 billion-a-year industry is Agfa Corp. , a unit of Bayer AG .	norel	2018..2117
Agfa recently signed Olympic gold medalist Florence Griffith-Joyner to endorse a new line of black-and-white paper that 's geared to consumers and will compete directly with Kodak 's papers .	restatement	2118..2306
Slated for market by the end of the year , the paper " could have been introduced a long time ago but the market was n't there then , " says an Agfa spokesman .	restatement	2307..2461
The biggest beneficiary of the black-and-white revival is likely to be International Paper Co. 's Ilford division , known in the industry for its premium products .	norel	2464..2626
Sales of Ilford 's four varieties of black-and-white film this year are outpacing growth in the overall market	restatement	2627..2736
although the company wo n't say by exactly how much .	contrast	2737..2789
" We hope the trend lasts , " says Laurie DiCara , Ilford 's marketing communications director .	cause	2790..2880
Why all the interest ?	norel	2883..2904
For baby boomers who grew up being photographed in color , black and white seems eye-catching and exotic .	norel	2905..3009
" It has an archival , almost nostalgic quality to it , " says Owen B. Butler , the chairman of the applied photography department at Rochester Institute of Technology .	conjunction	3010..3173
" You can shift out of reality with black and white , " he adds .	conjunction	3174..3235
Such features have been especially attractive to professional photographers and marketing executives , who have been steadily increasing their use of black and white in advertising .	norel	3238..3418
Processing of black-and-white commercial film jumped 24 % last year to 18.7 million rolls .	restatement	3419..3508
Consider Gap Inc. , whose latest ad campaign features black-and-white shots of Hollywood stars , artists and other well-known personalities modeling the retailer 's jeans and T-shirts .	norel	3511..3692
Richard Crisman , the account manager for the campaign , says Gap did n't intentionally choose black and white to distinguish its ads from the color spreads of competitors .	contrast	3693..3862
" We wanted to highlight the individual , not the environment , " he says , " and black and white allows you to do that better than color . "	norel	3865..3998
The campaign won a Cleo award as this year 's best ad by a specialty retailer .	entrel	3999..4076
Even food products and automobiles , which have long depended on color , are making the switch .	norel	4079..4172
Companies " feel black and white will convey a stronger statement , " says Marc L. Hauser , a Chicago photographer who is working on a black-and-white print ad for Stouffer Food Corp. 's Lean Cuisine .	cause	4173..4369
Other companies that are currently using two-tone ads include American Express Co. and Epson America Inc .	entrel	4370..4475
Portrait studios have also latched onto the trend .	norel	4478..4528
Using black and white , " we can make housewives look like stars , " says John Perrin .	cause	4529..4611
His On-Broadway Photography studio in Portland , Ore. , doubled its business last year and , he says , is booked solid for the next five .	entrel	4612..4745
One customer , Dayna Brunsdon , says she spurned a color portrait for black and white because " it 's more dramatic .	norel	4748..4860
I show it to my friends , and they all say ' wow . '	pragmatic cause	4861..4910
It is n't ordinary like color . "	cause	4911..4941
Still , most consumers are n't plunking black-and-white film into their cameras to take family snapshots .	norel	4944..5047
One big obstacle is that few drugstores develop the film anymore .	cause	5048..5113
Typically , it must be mailed to a handful of processors and may take a week or more to be processed and returned .	cause	5114..5227
Black-and-white film costs consumers a little less than color film	norel	5230..5296
and processing costs the same .	conjunction	5297..5328
But for photofinishers , developing costs for black-and-white film are higher .	contrast	5329..5406
Some companies are starting to tackle that problem .	norel	5409..5460
Ilford , for example , recently introduced a black-and-white film that can be processed quickly by color labs .	instantiation	5461..5569
Intent on wooing customers , the company is also increasing its sponsorship of black-and-white photography classes .	conjunction	5570..5684
Similarly , Agfa is sponsoring scores of photography contests at high schools and colleges , offering free black-and-white film and paper as prizes .	conjunction	5687..5833
And Kodak is distributing an instructional video to processors on how to develop its monochrome film more efficiently .	conjunction	5834..5952
Other companies are introducing related products .	norel	5955..6004
Charles Beseler Co. , a leading maker of photographic enlargers , introduced last month a complete darkroom starter kit targeted at teen-agers who want to process their own black-and-white photographs .	instantiation	6005..6204
The kit , which has a suggested retail price of $ 250 and has already become a bestseller , was introduced	entrel	6205..6308
after retailers noticed numerous requests from parents for children 's photography equipment .	asynchronous	6309..6401
" It seems computers as hobbies have waned , " says Ian Brightman , Beseler 's chairman and chief executive officer .	restatement	6402..6513
But some industry observers believe the resurgence of black and white is only a fad .	norel	6516..6600
They cite the emergence of still electronic photography , more newspapers turning to color on their pages and measurable improvements in the quality of color prints .	cause	6601..6765
" Black and white has n't made the same quantum leaps in technological development as color , " says Mr. Butler of the Rochester Institute .	norel	6768..6903
" The color print today is far superior to prints of 10 years ago .	instantiation	6904..6969
You ca n't say the same with black and white . "	contrast	6970..7015
But when Popular Photography , a leading magazine for photographers , selected 15 of the greatest photos ever made for its latest issue celebrating photography 's 150th anniversary , all were black and white .	norel	7018..7222
" It 's got a classic spirit and carries over emotionally , " says Alfred DeBat of Professional Photographers of America .	cause	7223..7340
" That 's the appeal .	entrel	7341..7360
