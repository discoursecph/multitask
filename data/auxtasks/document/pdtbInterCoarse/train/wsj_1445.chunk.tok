Conner Peripherals Inc. , which has a near-monopoly on a key part used in many portable computers , is on target to surpass Compaq Computer Corp. as the fastest-growing start-up manufacturing firm in U.S. business history .	O	9..229
Conner dominates the market for hard-disk drives used to store data in laptop computers .	O	232..320
It said yesterday that net income for its third quarter soared 72 % to $ 11.8 million , or 28 cents a share , from $ 6.8 million , or 19 cents a share , in the year-ago period .	B-expansion	321..490
Its revenue totaled $ 184.4 million , an increase of 172 % from $ 67.8 million a year ago .	I-expansion	491..577
For the nine months , the San Jose , Calif.-based company said net income jumped 84 % to $ 26.9 million , or 69 cents a share , from $ 14.6 million , or 43 cents a share .	O	580..742
Revenue nearly tripled to $ 479 million , from $ 160 million .	O	743..801
Analysts expect Conner 's earnings to reach roughly $ 40 million , or $ 1 to $ 1.05 a share , on sales of $ 650 million , for 1989 , the company 's third full year in business .	O	804..970
That 's a faster growth rate than reported by Compaq , which did n't post similar results until its fourth year , in 1986 .	O	971..1089
But Compaq had achieved that level of sales faster than any previous manufacturing start-up .	O	1090..1182
Conner 's performance is closely tied to the burgeoning demand for battery-operated computers , the computer industry 's fastest-growing segment .	B-contingency	1185..1327
Since its inception , Conner has both benefited from and helped make possible the rapid spread of portable computers by selling storage devices that consume five to 10 times less electricity than drives used in desktop machines .	I-contingency	1328..1555
Today , Conner controls an estimated 90 % of the hard-disk drive market for laptop computers .	B-expansion	1558..1649
The company supplies drives to Compaq and Zenith Data Systems , the top two U.S. manufacturers of laptops , and to Toshiba Corp. , NEC Corp. and Sharp Corp. , the leading Japanese laptop makers .	I-expansion	1650..1840
" They 've had this field to themselves for over a year now , and they 've been greatly rewarded , " said Bob Katsive , an analyst at Disk/Trend Inc. , a market researcher in Los Altos , Calif .	O	1843..2027
In the coming months , however , this is likely to change .	O	2030..2086
Next month , Seagate Technology , which is the dominant supplier of hard-disk drives for personal computers , plans to introduce its first family of low-power drives for battery-operated computers .	B-expansion	2087..2281
And the Japanese are likely to keep close on Conner 's heels .	I-expansion	2284..2344
" They are going to catch up , " said David Claridge , an analyst with Hambrecht & Quist .	O	2345..2430
Both Toshiba and NEC already produce hard-disk drives , and Sony also is studying the field , Mr. Claridge said .	O	2431..2541
But Conner is n't standing still .	B-expansion	2544..2576
Yesterday , the company introduced four products , three of which are aimed at a hot new class of computers called notebooks .	B-entrel	2577..2700
Each of the three drives uses a mere 1.5 watts of power and one weighs just 5.5 ounces .	I-entrel	2701..2788
" Most of our competitors are announcing products based on our ( older ) products , " said Finis Conner , chief executive officer and founder of the firm that bears his name .	O	2791..2959
" We continue to develop products faster than anyone else can . "	O	2960..3022
These new products could account for as much as 35 % of the company 's business in 1990 , Mr. Conner estimated .	O	3023..3131
" We 're not afraid of obsoleting some of our old stuff to stay ahead of the competition , " he said .	O	3132..3229
Conner already is shipping its new drives .	B-expansion	3232..3274
Last week , for instance , Compaq introduced its first notebook computer to rave reviews .	B-entrel	3275..3362
Conner is supplying hard-disk drives for the machine , which weighs only six pounds and fits in a briefcase .	I-entrel	3363..3470
From its inception , Conner has targeted the market for battery-operated machines , building hard-disk drives that are smaller and use far less power than those offered by competitors such as Seagate .	O	3473..3671
The availability of these drives , in turn , boosted demand for laptop computers , whose usefulness had been limited because of lack of storage .	O	3672..3813
Conner also makes hard-disk drives for desktop computers and is a major supplier to Compaq , which as of July owned 40 % of Conner 's stock .	O	3816..3953
Sales to Compaq represented 26 % of Conner 's business in its third quarter , compared with 42 % in the year-ago period .	O	3954..4070
