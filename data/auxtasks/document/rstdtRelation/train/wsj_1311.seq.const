Exxon Corp. said	(Textual-organization(Summary(Joint(Elaboration(Attribution
its third-quarter earnings slipped 9 %	(Explanation
as profits from two of its three major businesses sagged .	Explanation))
All cleanup costs from last spring 's Alaskan oil spill were reflected in earlier results ,	(Attribution
it said .	Attribution))
Phillips Petroleum Co. and Atlantic Richfield Co. also reported declines in quarterly profit ,	(Joint
while Ashland Oil Inc. posted a loss for the latest quarter .	(Joint
Amerada Hess Corp. and Occidental Petroleum Corp. reported higher earnings .	Joint)))
Exxon	(Joint(Textual-organization
Although Exxon spent heavily during the latest quarter	(Elaboration(Elaboration(Contrast(Enablement
to clean up the Alaskan shoreline	(Elaboration
blackened by its huge oil spill ,	Elaboration))
those expenses as well as the cost of a continuing spill-related program are covered by $ 880 million in charges	(Elaboration
taken during the first half .	Elaboration))
An Exxon official said	(Contrast(Attribution
that at this time the oil company does n't anticipate any additional charges to future earnings	(Elaboration
relating to the cleanup of oil	(Elaboration
spilled	(Cause
when one of its tankers rammed into an underwater reef .	Cause))))
She added , however ,	(Elaboration(Attribution
that charges	(Same-unit(Elaboration
already taken	Elaboration)
do n't take into account the potential effect of litigation	(Elaboration
involving the oil spill .	Elaboration)))
She said	(Attribution
that impact ca n't be reasonably assessed yet .	Attribution))))
Exxon 's net income during the third quarter dropped to $ 1.11 billion , or 87 cents a share , from $ 1.22 billion , or 93 cents a share , a year earlier .	(Elaboration(Joint
Revenue rose 8.1 % , to $ 23.65 billion from $ 21.88 billion .	(Joint
During the third quarter , Exxon purchased 8.34 million shares of its stock at a cost of $ 373 million .	Joint))
Exxon 's profitability , like that of many other oil companies , was hurt during the third quarter by declining returns from the chemicals and refining and marketing businesses .	(Elaboration(Elaboration
Exxon 's earnings from chemicals operations fell $ 90 million , to $ 254 million ,	(Joint
while refining and marketing profits declined $ 180 million , to $ 357 million .	Joint))
Although crude oil prices were significantly higher this year ,	(Elaboration(Attribution(Contrast
they were n't strong enough	(Cause
to offset the declining profits in those business sectors at most oil companies ,	Cause))
said William Randol , oil analyst for First Boston Corp .	Attribution)
He estimates	(Attribution
that the price of West Texas Intermediate , the U.S. benchmark crude , was $ 4.04 a barrel higher during the third quarter of this year than in the same period last year .	Attribution))))))
Ashland Oil	(Joint(Textual-organization
A rash of one-time charges left Ashland Oil with a loss of $ 39 million for its fiscal fourth quarter .	(Elaboration(Background
A year earlier , the refiner earned $ 66 million , or $ 1.19 a share .	(Joint
Quarterly revenue rose 4.5 % , to $ 2.3 billion from $ 2.2 billion .	(Joint
For the year , net income tumbled 61 % to $ 86 million , or $ 1.55 a share .	Joint)))
The Ashland , Ky. , oil company reported a $ 38 million charge	(Joint(Elaboration(Elaboration
resulting from settlement of a 10-year dispute with the National Iranian Oil Co. over claims	(Elaboration
that Ashland did n't pay for Iranian crude	(Elaboration
it had received .	Elaboration)))
In September , Ashland settled the long-simmering dispute	(Manner-Means
by agreeing to pay Iran $ 325 million .	Manner-Means))
Ashland also took a $ 25 million after-tax charge	(Joint(Enablement
to cover anticipated costs	(Elaboration
to correct problems with boilers	(Elaboration
built by one of its subsidiaries .	Elaboration)))
The oil refiner also booked a $ 15 million charge	(Explanation
for selling Ashland Technology Corp. , one of its subsidiaries , at a loss .	Explanation)))))
Amerada Hess	(Joint(Textual-organization
Third-quarter earnings at Amerada Hess more than tripled to $ 51.81 million , or 64 cents a share , from $ 15.7 million , or 20 cents a share , a year earlier .	(Elaboration(Elaboration(Elaboration
Revenue climbed 28 % , to $ 1.18 billion from $ 925 million .	Elaboration)
Profits improved across Hess 's businesses .	(Explanation
Refining and marketing earnings climbed to $ 33.3 million from $ 12.9 million ,	(Joint
and exploration and production earnings rose to $ 37.1 million from $ 17.9 million .	Joint)))
Hess 's earnings were up despite a $ 30 million charge	(Elaboration(Elaboration
to cover the cost	(Elaboration
of maintaining operations	(Background
after Hurricane Hugo heavily damaged the company 's refinery at St. Croix .	Background)))
It is widely known within industry circles	(Elaboration(Attribution
that Hess had to buy oil products in the high-priced spot markets	(Enablement
to continue supplying its customers .	Enablement))
Hess declined to comment .	Elaboration))))
Phillips Petroleum	(Joint(Textual-organization
Phillips Petroleum 's third-quarter earnings slid 60 % , to $ 87 million , or 36 cents a share , from $ 215 million , or 89 cents a share .	(Comparison(Explanation(Elaboration
Revenue rose 6.9 % , to $ 3.1 billion from $ 2.9 billion .	Elaboration)
Shrinking profit margins in chemical and refining and marketing sectors accounted for most of the decline ,	(Elaboration(Attribution
said Chairman C.J. Silas in a statement .	Attribution)
Despite higher oil prices ,	(Contrast
exploration and production profits were off	(Explanation
because of foreign-currency losses and some construction costs	(Elaboration
incurred in one of Phillips ' North Sea oil fields .	Elaboration)))))
A year ago , results were buoyed by a $ 20 million after-tax gain from an asset sale .	Comparison))
Occidental Petroleum	(Joint(Textual-organization
Occidental Petroleum 's third-quarter net income rose 2.9 % to $ 108 million , or 39 cents a share , from $ 105 million , or 38 cents a share , a year earlier .	(Elaboration(Elaboration(Elaboration(Joint(Elaboration(Elaboration
The latest quarter included an after-tax gain of $ 71 million from non-recurring items .	Elaboration)
Sales dropped 2 % , to $ 4.8 billion from $ 4.9 billion .	Elaboration)
The latest period included a $ 54 million gain from the sale of various oil and gas properties , a $ 22 million charge from the restructuring of Occidental 's domestic oil and gas operations , and tax credits of $ 42 million .	Joint)
Both periods included non-recurring charges of $ 3 million for early retirement of debt .	Elaboration)
Occidental said	(Elaboration(Attribution
oil and gas earnings fell to $ 17 million from $ 20 million .	Attribution)
The latest period includes net gains of $ 32 million in non-recurring credits from the sale of properties ,	(Elaboration
indicating operating losses for the quarter in the oil and gas division .	Elaboration)))
Chemical earnings fell 10 % ,	(Elaboration
reflecting softening of demand .	Elaboration)))
Atlantic Richfield	(Textual-organization
Citing its reduced ownership in the Lyondell Petrochemical Co. ,	(Elaboration(Elaboration(Elaboration(Elaboration(Background
Atlantic Richfield reported	(Attribution
that net income slid 3.1 % in the third quarter to $ 379 million , or $ 2.19 a share , from $ 391 million , or $ 2.17 a share , for the comparable period last year .	Attribution))
Sales fell 20 % , to $ 3.7 billion from $ 4.6 billion .	Elaboration)
Arco 's earnings from its 49.9 % stake in Lyondell fell to $ 37 million from $ 156 million for the same period last year ,	(Contrast(Elaboration
when Lyondell was wholly owned .	Elaboration)
Offsetting the lower stake in Lyondell were higher crude oil prices , increased natural gas volumes and higher coke prices ,	(Attribution
the company said .	Attribution)))
Coal earnings rose to $ 26 million from $ 21 million .	Elaboration)
For the nine months , Arco reported net income of $ 1.6 billion , or $ 8.87 a share , up 33 % from $ 1.2 billion , or $ 6.56 a share a year earlier .	(Elaboration
Sales were $ 12 billion , off 13 % from $ 13.8 billion .	Elaboration)))))))))
Jeff Rowe contributed to this article .	Textual-organization)
