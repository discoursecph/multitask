Out of the mouths of revolutionaries are coming words of moderation .	O	9..77
Here , at a soccer stadium near the black township of Soweto yesterday , were eight leaders of the African National Congress , seven of whom had spent most of their adult lives in prison for sabotage and conspiracy to overthrow the government .	B-conjunction	80..320
Here were more than 70,000 ANC supporters , gathering for the first ANC rally inside South Africa since the black liberation movement was banned in 1960 .	B-conjunction	321..473
Here was the state security appartus poised to pounce on any words or acts of provocation , let alone revolution .	B-contrast	474..586
But the words that boomed over the loudspeakers bore messages of peace , unity , negotiation and discipline .	I-contrast	589..695
" We stand for peace today and we will stand for peace tomorrow , " said Walter Sisulu , the ANC 's former secretary general who , along with five of his colleagues , served 26 years in prison before being released two weeks ago .	O	696..918
Some members of the huge crowd shouted " Viva peace , viva . "	O	919..977
These are curious times in South African politics .	B-cause	980..1030
The government and the ANC , the bitterest of enemies , are engaged in an elaborate mating dance designed to entice each other to the negotiating table .	B-cause	1031..1181
Pretoria releases the ANC leaders , most of whom were serving life sentences , and allows them to speak freely , hoping that the ANC will abandon its use of violence .	B-conjunction	1182..1345
The ANC leaders speak in tones of moderation , emphasizing discipline , hoping the government will be encouraged to take further steps , such as freeing Nelson Mandela , the most prominent ANC figure , and unbanning the organization .	I-conjunction	1346..1574
The government of President F.W. de Klerk is using this situation to improve its international image and head off further economic sanctions .	B-synchrony	1577..1718
Meanwhile , the many organizations inside the country that back the ANC are taking the opportunity to regain their strength and mobilize their supporters even though the state of emergency , which has severely curtailed black opposition , remains in force .	I-synchrony	1719..1972
The result is that the unthinkable and illogical are happening .	B-instantiation	1975..2038
Six months ago , government approval for an ANC rally was inconceivable .	B-conjunction	2039..2110
Equally inconceivable is that the ANC , given the chance to hold a rally , would extend a hand , albeit warily , to the government .	B-restatement	2111..2238
In a message read out at the rally , exiled ANC President Oliver Tambo , who ca n't legally be quoted in South Africa , said the country was at a crossroads and that Mr. de Klerk " may yet earn a place among the peacemakers of our country " if he chooses a " path of genuine political settlement . "	I-restatement	2239..2529
Still , this does n't mean that either the government or the ANC is changing stripes -- or that either has moved significantly closer to the other .	O	2532..2677
The government may ease repression in some areas , but it still keeps a tight grip in others .	B-instantiation	2680..2772
For instance , it releases Mr. Sisulu without conditions , yet his son , Zwelakhe , a newspaper editor , is restricted to his home much of the day and is n't allowed to work as a journalist .	I-instantiation	2773..2957
The ANC vows to keep up pressure on the government .	B-instantiation	2960..3011
Speakers yesterday called on foreign governments to increase sanctions against Pretoria and urged supporters inside the country to continue defying emergency restrictions and racial segregation , known as apartheid .	I-instantiation	3012..3226
" We can not wait on the government to make changes at its own pace , " Mr. Sisulu said .	O	3227..3311
Because the ANC remains banned , both the government , which approved the rally , and the organizers , who orchestrated it , denied it was an ANC rally .	O	3314..3461
They both called it a " welcome home " gathering .	B-contrast	3462..3509
Nevertheless , an ANC rally by any other name is still an ANC rally .	I-contrast	3510..3577
The recently released leaders sat high atop a podium in one section of the stadium stands .	B-conjunction	3580..3670
Behind them was a huge ANC flag and an even bigger sign that said " ANC Lives , ANC Leads . "	B-conjunction	3671..3760
Next to them was the red flag of the outlawed South African Communist Party , which has long been an ANC ally .	B-conjunction	3761..3870
In the stands , people waved ANC flags , wore ANC T-shirts , sang ANC songs and chanted ANC slogans .	I-conjunction	3871..3968
" Today , " said Mr. Sisulu , " the ANC has captured the center stage of political life in South Africa . "	O	3971..4071
As a police helicopter circled overhead , Mr. Sisulu repeated the ANC 's demands on the government to create a climate for negotiations : Release all political prisoners unconditionally ; lift all bans and restrictions on individuals and organizations ; remove all troops from the black townships ; end the state of emergency , and cease all political trials and political executions .	B-entrel	4074..4451
If these conditions are met , he said , the ANC would be prepared to discuss suspending its guerrilla activities .	I-entrel	4452..4563
" There can be no question of us unilaterally abandoning the armed struggle , " he said . "	O	4566..4653
To date , we see no clear indication that the government is serious about negotiations .	B-cause	4653..4739
All their utterances are vague . "	I-cause	4740..4772
Echoing a phrase from Mr. de Klerk , Mr. Sisulu said , " Let all of us who love this country engage in the task of building a new South Africa .	O	4775..4915
