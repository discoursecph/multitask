#!/bin/bash
sigma=0.1
h_layers=1
mkdir -p outs
python disco.py --cnn-seed 3068836234 --iters 20 --sigma $sigma --train ../data/rstdt_constituent/train/ --test ../data/rstdt_constituent/dev/ --h_layers $h_layers --pred_layer $h_layers --output outs/baseline.h_layers$h_layers.sigma$sigma


#python disco.py --cnn-seed 3068836234 --iters 10 --train ../data/rstdt_constituent/train/ ../data/factbank_seq/factbank_tense/ --pred_layer 1 1 --test ../data/rstdt_constituent/dev/ --h_layers $1 