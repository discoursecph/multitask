When Maj. Moises Giroldi , the leader of ,the abortive coup in was buried , his body bore several gunshot wounds , a cracked skull and broken legs and ribs .	ROOT	0
They were the signature of , his adversaryPanamanian leader Manuel Antonio Noriega .	COREF_PREV	1
The rebel officer 's slow and painful death , at the headquarters of Panama 's Battalion - 2000 squad , was personally supervised by Gen. Noriega , says a U.S. official with access to intelligence reports .	COREF_PREV	2
Leaping into rages , sinking into bouts of drunkenness and mistrust , Mr. Noriega has put to death some 70 of his troops involved in the coup , according to U.S. officials monitoring crematoriums and funeral parlors in Panama City .	COREF_PREV	3
He is now changing the place he sleeps every night , sometimes more than once a night .	COREF_PREV	4
His meals are most often prepared by women he trusts -- , his full - time mistressVicky Amado , and her mother , Norma .	COREF_PREV	5
And he is collecting the names of those who telephoned the coup - makers to congratulate them during their brief time in control of his headquarters .	COREF_PREV	6
More enemies to be dealt with .	NONE	7
In the two weeks since the rebellion , which the U.S. hesitantly backed , Mr. Noriega has been at his most brutal - and efficient - in maintaining power .	COREF_OTHER	8
Yet , while the failed coup is a major U.S. foreign policy embarrassment , it is merely the latest chapter in a byzantine relationship between Mr. Noriega and Washington that stretches back three decades .	COREF_PREV	9
America 's war on the dictator over the past two years , following his indictment on drug charges in February 1988 , is the legacy of that relationship .	COREF_PREV	10
Before American foreign policy set out to destroy Noriega , it helped create him out of the crucible of Panama 's long history of conspirators and pirates .	COREF_PREV	11
For most of the past 30 years , the marriage was one of convenience .	NONE	12
In 1960 , for example , when Mr. Noriega was both a cadet at an elite military academy in Peru and a spy - in - training for the U.S. Defense Intelligence Agency , he was detained by Lima authorities for allegedly raping and savagely beating a prostitute , according to a U.S. Embassy cable from that period .	COREF_OTHER	13
The woman had nearly died .	COREF_PREV	14
But U.S. intelligence , rather than rein in or cut loose its new spy , merely filed the report away .	COREF_OTHER	15
Mr. Noriega 's tips on emerging leftists at his school were deemed more important to U.S. interests .	COREF_PREV	16
From that point on , the U.S. would make a practice of overlooking the Panamanian 's misadventures .	COREF_PREV	17
The U.S. has befriended and later turned against many dictators , but none quite so resourceful .	COREF_PREV	18
The 55 - year - old Mr. Noriega is n't as smooth as the shah of Iran , as well - born as Nicaragua 's Anastasio Somoza , as imperial as Ferdinand Marcos of the Philippines or as bloody as Haiti 's Baby Doc Duvalier .	COREF_OTHER	19
Yet he has proved more resilient than any of them .	COREF_PREV	20
And out of necessity : The U.S. can make mistakes and still hope to remove him from power , but a single error on his part could cost him his life .	COREF_PREV	21
`` The U.S. underestimated Noriega all along , '' says Ambler Moss , a former Ambassador to Panama .	COREF_PREV	22
`` He has mastered the art of survival . ''	COREF_PREV	23
In keeping with America 's long history of propping up Mr. Noriega , recent U.S. actions have extended rather than shortened his survival .	COREF_PREV	24
Mr. Noriega might have fallen of his own weight in 1988 because of Panama 's dire economic situation , says Mr. Moss , but increasing external pressure has only given him additional excuses for repression , and a scapegoat for his own mismanagement .	COREF_PREV	25
`` If the U.S. had sat back and done nothing , he might not have made it through 1988 , '' Mr. Moss contends .	COREF_PREV	26
Perhaps most important , Mr. Noriega 's allies have intervened to encourage -- in some cases , to demand -- that the dictator maintain his grip of the throne .	COREF_PREV	27
One Colombian drug boss , upon hearing in 1987 that Gen. Noriega was negotiating with the U.S. to abandon his command for a comfortable exile , sent him a hand - sized mahogany coffin engraved with his name .	COREF_PREV	28
`` He is cornered , '' says the Rev. Fernando Guardia , who has led Catholic Church opposition against Noriega .	COREF_PREV	29
`` The Americans have left him without a way out .	COREF_PREV	30
It is easy to fight when you do n't have any other option . ''	COREF_PREV	31
His chief advantage in the fight : his intimate knowledge of American ways and weaknesses .	COREF_PREV	32
Mr. Noriega often tells friends that patience is the best weapon against the gringos , who have a short attention span and little stomach for lasting confrontation .	COREF_PREV	33
The U.S. discovered the young Tony Noriega in late 1959 , when he was in his second year at the Chorrillos Military Academy in Lima , according to former U.S. intelligence officials .	COREF_PREV	34
The contact occurred through , Mr. Noriega 's half - brothera Panamanian diplomat based in Peru named Luis Carlos Noriega Hurtado .	COREF_PREV	35
Luis Carlos , knowing that helping the Americans could advance the career of any Panamanian officer , relayed Tony 's reports on the leftist tendencies he observed among his fellow students and , more important , among his officers and instructors .	COREF_PREV	36
A spy was born .	COREF_PREV	37
It was a heady experience for the pockmarked and slightly built Mr. Noriega , who was known to his friends as Cara la Pina -- pineapple face .	COREF_PREV	38
Born the illegitimate son of his father 's maid , he was raised on the mean streets of the central market district of Panama City .	COREF_PREV	39
Tony was four years older than most of his fellow cadets , and gained admission to the academy because his brother had falsified his birth certificate .	COREF_PREV	40
He considered himself intellectually superior to his Peruvian peers , many of whom were wayward sons sent by their well - off families to the highly disciplined , French - modeled academy as a sort of reform school .	COREF_PREV	41
In his peaked military cap and neatly pressed , French - made uniform , Noriega felt more respected and powerful than ever in his underprivileged life , friends from the period say .	COREF_PREV	42
`` He had an elegant uniform with gold buttons in a country where there was a cult of militarism , where officers were the elite with special privileges , '' recalls Darien Ayala , a fellow student in Peru and a lifelong friend .	COREF_PREV	43
Mr. Noriega 's relationship to American intelligence agencies became contractual in either 1966 or 1967 , intelligence officials say .	COREF_PREV	44
His commanding officer at the Chiriqui Province garrison , Major Omar Torrijos , gave him an intriguing assignment : Mr. Noriega would organize the province 's first intelligence service .	COREF_PREV	45
The spy network would serve two clients : the Panamanian government , by monitoring political opponents in the region , and the U.S. , by tracking the growing Communist influence in the unions organized at United Fruit Co. 's banana plantations in Bocas del Toros and Puerto Armuelles .	COREF_PREV	46
United Fruit was one of the two largest contributors to Panama 's national income .	COREF_PREV	47
Satisfying its interests was a priority for any Panamanian leader .	COREF_PREV	48
Mr. Noriega 's initial retainer was only $ 50 to $ 100 a month , plus occasional gifts of liquor or groceries from the American PX , a former intelligence official says .	COREF_OTHER	49
It was modest pay by American standards , but a healthy boost to his small military salary , which fellow officers remember as having been $ 300 to $ 400 monthly .	COREF_PREV	50
`` He did it very well , '' recalls Boris Martinez , a former Panamanian colonel who managed Mr. Noriega and .	COREF_PREV	51
`` He started building the files that helped him gain power . ''	COREF_PREV	52
A National Guard job assumed by Capt. Noriega in 1964 -- as chief of the transit police in David City , capital of the Chiriqui Province -- was tailor - made for an aspiring super-spy .	COREF_PREV	53
By pressuring taxi and bus drivers who needed licenses , he gained a ready cache of information .	NONE	54
He knew which local luminaries had been caught driving drunk , which had been found with their mistresses .	COREF_OTHER	55
This proved particularly valuable to the Panamanian government in 1967 , when union leaders were planning a May Day march that the government feared could turn violent .	COREF_OTHER	56
Mr. Noriega had learned that a local union leader was sleeping with the wife of his deputy .	COREF_PREV	57
So he splashed the information on handbills that he distributed throughout the banana - exporting city of Puerto Armuelles , which was ruled by United Fruit Co .	COREF_PREV	58
The campaign so divided union leaders that the government found them far easier to control .	COREF_OTHER	59
`` It was like a play on Broadway , '' recalls Mr. Martinez .	COREF_PREV	60
`` Noriega managed the whole thing .	NONE	61
He was superb .	COREF_OTHER	62
Noriega was an expert at bribing and blackmailing people . ''	COREF_OTHER	63
During his years in Chiriqui , however , Mr. Noriega also revealed himself as an officer as perverse as he was ingenious .	COREF_PREV	64
Rodrigo Miranda , a local lawyer and human - rights monitor , recalls an intoxicated Noriega visiting prisoners in their cells at the 5th Zone Garrison headquarters in David , where he had his offices .	COREF_PREV	65
Mr. Noriega would order them all to take off their clothes and run around the courtyard naked , laughing at them and then retreating to his office .	COREF_PREV	66
`` People started wondering if something was wrong with him , '' Mr. Miranda recalls .	COREF_PREV	67
But through this period , so far as the U.S. military was concerned , Mr. Noriega was a model recruit .	COREF_PREV	68
He signed up for intelligence and counter-intelligence training under American officers at Fort Gulick in Panama in July 1967 , according to a copy of a 1983 resume with details Mr. Noriega has since classified as secret .	COREF_PREV	69
He flew to Fort Bragg , N.C. , in September of that year for a course in psychological operations , returning to the School of the Americas in Panama for a two - month course called `` military intelligence for officers . ''	COREF_PREV	70
Some American officers interpreted his eagerness and studiousness as a sign of loyalty , but they did so falsely .	COREF_PREV	71
He rose to chief of intelligence in Panama 's socalled G - 2 in 1970 after providing populist dictator Torrijos the critical support to defeat a coup attempt against him a year earlier .	COREF_PREV	72
He became Gen. Torrijos 's inseparable shadow , and the holder of all Panama 's secrets .	COREF_PREV	73
Mr. Noriega , by now a lieutenant colonel , expanded his contacts to include the Cubans -- not to mention the Israelis , the Taiwanese and any other intelligence service that came knocking .	COREF_PREV	74
When U.S. diplomats complained to the CIA of Col. Noriega 's moonlighting , intelligence experts always insisted that his allegiance was first to the Americans .	COREF_PREV	75
`` Early on in the State Department , we took to calling him the rent - a - colonel , in tribute to his ability to simultaneously milk the antagonistic intelligence services of Cuba and the United States , '' recalls Francis J. McNeil , who , as deputy assistant secretary of state for inter-American affairs , first ran across reports about Mr. Noriega in 1977 .	COREF_PREV	76
`` Some of us wondered how our intelligence people could put so much stock in his information when he was just as close to the Cubans . ''	COREF_PREV	77
Even at this early stage , drugs caused additional concerns .	COREF_PREV	78
During the Nixon administration , the Drug Enforcement Administration became dismayed at the extent of the G - 2 's connections to arrested drug traffickers .	NONE	79
One DEA agent drew up a list of five options for dealing with Col. Noriega , one of which was assassination .	COREF_PREV	80
The head of , the DEA at the timeJohn Ingersoll , scotched the assassination plan .	COREF_OTHER	81
But he did fly to Panama to scold dictator Torrijos on the drug ties of Panamanian officials , including Mr. Noriega .	COREF_PREV	82
Mr. Ingersoll later recalled that Gen. Torrijos seemed afraid to act on the concerns of the U.S. .	COREF_PREV	83
`` Everybody was afraid of him , '' Mr. Ingersoll says .	COREF_OTHER	84
Mr. Noriega became an even greater threat in 1976 , when U.S. intelligence services discovered that he had been buying recordings of electronically monitored conversations from three sergeants working for the U.S. Army 's 470th Military Intelligence Group .	COREF_OTHER	85
The tapes included wiretaps of Gen. Torrijos 's own phone , according to American intelligence officials .	COREF_PREV	86
`` We caught him with his hands on our cookie jar , '' says former CIA Director Stansfield Turner .	COREF_OTHER	87
For the first time , the U.S. considered cutting Mr. Noriega from its intelligence payroll -- and the deliberations were intense , Mr. Turner says .	COREF_PREV	88
`` In the world of intelligence , if you want to get information , you get it from seedy characters .	COREF_PREV	89
The question is how much you get tied in with seedy characters so they can extort you . ''	NONE	90
Intelligence officials to this day worry whether Mr. Noriega sold sensitive information on the recordings to the Cubans or others .	NONE	91
Mr. Turner was troubled enough to cancel the U.S. contract with the rent - a - colonel at the beginning of the Carter administration .	COREF_OTHER	92
The U.S. soon found new cause for concern : gun - running .	COREF_PREV	93
Prosecutors in Southern Florida indicted five Panamanians on charges of illegally running arms to Sandinista rebels trying to overthrow the Nicaraguan government of Mr. Somoza .	COREF_OTHER	94
They included one of Mr. Noriega 's closest friends and business partners , Carlos Wittgreen .	COREF_OTHER	95
And the investigators were quickly closing in on Mr. Noriega himself .	COREF_OTHER	96
At the time , though , in 1979 , the U.S. was once again flirting with its longtime Latin American spy .	COREF_PREV	97
Mr. Noriega made plans to fly to Washington for a meeting with his counterpart at the Pentagon .	COREF_PREV	98
Dade County and federal authorities , learning that he intended to fly through Miami , made plans to arrest him on the gun - running charges as soon as he hit U.S. soil .	COREF_PREV	99
It was a Friday in June .	NONE	100
The Pentagon foiled the plan .	COREF_OTHER	101
According to military officers at the time , word was passed to Mr. Noriega by his American hosts that the police would be waiting .	COREF_PREV	102
On Monday , U.S. officials received a routine , unclassified message from the military group commander in Panama .	COREF_PREV	103
`` Due to health reasons , Lt. Col. Noriega has elected to postpone his visit to Washington , '' it read .	COREF_OTHER	104
Prosecutors in Miami received yet another setback .	COREF_OTHER	105
Their original indictment against , and the other four was dismissed on a technicality .	COREF_PREV	106
But now , along with reindicting Mr. Noriega 's pal , they intended to charge Mr. Noriega himself , on allegations that he was involved in the illegal trading of some $ 2 million in arms .	COREF_PREV	107
In January 1980 , Jerome Sanford , as assistant U.S. attorney , was summoned to a meeting with a Federal Bureau of Investigation agent assigned to the Bureau of Alcohol , Tobacco and Firearms in Miami .	COREF_PREV	108
Panamanian dictator Torrijos , he was told , had granted the shah of Iran asylum in Panama as a favor to Washington .	COREF_OTHER	109
Mr. Sanford was told , Mr. Noriega 's friendMr. Wittgreen , would be handling the shah 's security .	COREF_OTHER	110
It would n't be a good idea to indict him -- much less Mr. Noriega , the prosecutor was told .	COREF_PREV	111
After prodding from Mr. Sanford , U.S. Attorney Jack Eskenazi pleaded with Justice Department officials in Washington to let the indictment proceed .	COREF_PREV	112
`` Unfortunately , '' Mr. Eskenazi wrote in a letter , `` those of us in law enforcement in Miami find ourselves frequently attempting to enforce the laws of the United States but simultaneously being caught between foreign policy considerations over which we have no control . ''	COREF_PREV	113
The letter , along with a detailed prosecution memo , sat on the desks of Justice officials for months before the case died a quiet death .	COREF_PREV	114
`` I think if we had been allowed to go ahead then we would n't have the problems we have now , '' Mr. Sanford says .	COREF_OTHER	115
`` If he had been found guilty , we could have stopped him . ''	NONE	116
In August 1983 , Mr. Noriega took over as General and de-facto dictator of Panama , having maneuvered his way to the top only two years after the mysterious death in a plane crash of his old boss Omar Torrijos .	COREF_OTHER	117
Soon , the military became a veritable mafia controlling legal and illegal businesses .	COREF_PREV	118
The Reagan administration also put Mr. Noriega 's G - 2 back on the U.S. payroll .	COREF_PREV	119
Payments averaged nearly $ 200,000 a year from the U.S. Defense Intelligence Agency and the CIA .	COREF_PREV	120
Although working for U.S. intelligence , Mr. Noriega was hardly helping the U.S. exclusively .	COREF_OTHER	121
During the Reagan years he expanded his business and intelligence contacts with the Cubans and the Sandinistas .	COREF_OTHER	122
He allegedly entered into Panama 's first formal business arrangement with Colombian drug bosses , according to Floyd Carlton , a pilot who once worked for Mr. Noriega and who testified before the U.S. grand jury in Miami that would ultimately indict the Panamanian on drug charges .	COREF_PREV	123
But Mr. Noriega was convinced the Reagan White House would n't act against him , recalls his close ally Jose Blandon , because he had an insurance policy : his involvement with the Contra rebels in .	COREF_PREV	124
Mr. Blandon says the general allowed the Contras to set up a secret training center in Panama .	COREF_PREV	125
Mr. Noriega also conveyed intelligence from his spy operation inside the Nicaraguan capital of Managua .	COREF_PREV	126
And on at least one occasion , in the spring of 1985 , he helped arrange a sabotage attack on a Sandinista arsenal in Nicaragua .	COREF_PREV	127
Although , his help for the Contra cause was limited , it was enough to win him important protectors in the Reagan administration , says Sen. Patrick Leahy , a Vermont Democrat who then served on the Senate Intelligence Committee .	COREF_PREV	128
`` Noriega played U.S. intelligence agencies and the U.S. government like a violin , '' he says .	NONE	129
An incident in 1984 suggested one additional means by which Mr. Noriega might have maintained such influence with Washington -- by compromising U.S. officials .	COREF_PREV	130
Curtin Windsor , then the ambassador to Costa Rica , recalls being invited to Panama by Mr. Noriega 's brother Luis Carlos for a weekend of deep sea fishing and `` quiet , serious conversation '' on the Aswara Peninsula .	COREF_PREV	131
Mr. Windsor notified Everett E. Briggs , the U.S. ambassador to ,Panama of the invitation .	NONE	132
`` Briggs screamed , '' Mr. Windsor recalls .	COREF_OTHER	133
He says Mr. Briggs told him he was being set up for a `` honey trap , '' in which Mr. Noriega would try to involve him in an orgy and then record the event `` with sound and video . ''	COREF_PREV	134
Mr. Briggs , on vacation after resigning his position at the National Security Council , could n't be reached for comment .	COREF_PREV	135
As Mr. Noriega 's political troubles grew , so did his offers of assistance to the Contras , an apparent attempt to curry more favor in Washington .	COREF_PREV	136
For instance , he helped steal the May 1984 Panamanian elections for the ruling party .	COREF_PREV	137
But just one month later , he also contributed $ 100,000 to a Contra leader , according to documents released for Oliver North 's criminal trial in Washington , D.C .	COREF_PREV	138
Yet , his political setbacks mounted .	COREF_OTHER	139
Mr. Noriega was accused of ordering in 1985 the beheading of Hugo Spadafora , his most outspoken political opponent and the first man to publicly finger Mr. Noriega on drug trafficking charges .	COREF_OTHER	140
He then ousted President Nicholas Ardito Barletta , a former World Bank official with close ties to the U.S. , after Mr. Barletta tried to create a commission to investigate the murder .	COREF_PREV	141
And , all the while , Panama 's debt problems continued to grow .	COREF_PREV	142
Mr. Noriega was growing desperate .	NONE	143
In late 1986 , he made an offer he thought the U.S. could n't refuse .	COREF_OTHER	144
As recounted in a stipulation that summarized government documents released for the North trial , Mr. Noriega offered to assassinate the Sandinista leadership in exchange `` for a promise to help clean up Noriega 's image and a commitment to lift the -LCB- U.S. . -RCB- ban on military sales to the Panamanian Defense Forces . ''	COREF_OTHER	145
`` North , '' the document went on , referring to Oliver North , `` has told Noriega 's representative that U.S. law forbade such actions .	COREF_PREV	146
The representative responded that Noriega had numerous assets in place in Nicaragua and could accomplish many essential things , just as Noriega had helped -LCB- the U.S. . -RCB- the previous year in blowing up a Sandinista arsenal . ''	COREF_PREV	147
Col. North conveyed the request to his superiors and to Assistant Secretary of State Elliot Abrams , who relayed it to Secretary of State George Shultz .	COREF_PREV	148
Mr. Noriega 's proposal was turned down .	COREF_PREV	149
And Mr. Shultz curtly told Mr. Abrams that the general should be told that only he could repair his tarnished image .	COREF_PREV	150
The end of the marriage was at hand .	COREF_PREV	151
Within weeks the unfolding Iran - Contra scandal took away Mr. Noriega 's insurance policy .	COREF_OTHER	152
The death of CIA Director William Casey and resignation of Oliver North allowed anti-Noriega political forces to gain influence .	COREF_PREV	153
Public protests against him were triggered in June 1987 due to charges by Diaz Herrera , his former chief of staff , that Mr. Noriega had stolen the 1984 election and had ordered the killing of Messrs. Spadafora and Torrijos .	COREF_PREV	154
Few American officials were willing any longer to defend him .	COREF_PREV	155
Lawyers in Miami -- this time working virtually without impediment -- prepared to have him indicted on drug charges in February 1988 .	COREF_PREV	156
During negotiations with American officials in May 1988 over proposals to drop the U.S. indictments in exchange for his resignation , Mr. Noriega often asked almost plaintively how the Americans , whom he had helped for so many years , could turn against him .	COREF_PREV	157
Now , neither side -- the U.S. nor Mr. Noriega -- has an easy out .	COREF_PREV	158
President Bush has sworn to bring him to justice .	COREF_PREV	159
Mr. Noriega believes he has n't any alternative but to continue clutching to power .	COREF_PREV	160
It is a knock - out battle -- perhaps to the death .	NONE	161
In the end , is Mr. Noriega the political equivalent of Frankenstein 's monster , created by a well - intentioned but misguided foreign power ?	COREF_OTHER	162
Not quite , Sen. Leahy contends .	COREF_PREV	163
`` For short - term gains , people were willing to put up with him .	NONE	164
That allowed him to get stronger and stronger , '' he says .	COREF_OTHER	165
`` I do n't think we created him as much as we fed him , nurtured him and let him grow up to be big and strong .	COREF_OTHER	166
