After two years of drought , it rained money in the stock-index futures markets yesterday .	O	9..98
As financial markets rebounded , trading volume in the Chicago Mercantile Exchange 's huge Standard & Poor 's 500 stock-index futures pit soared , reaching near-record levels for the first time since October 1987 .	O	101..310
The sudden influx of liquidity enabled several traders to reap six-figure windfalls in a matter of minutes as prices soared , traders said .	O	313..451
" Guys were minting money in there today , " said John Legittino , a futures broker for Elders Futures Inc. in Chicago .	O	454..569
The S&P 500 futures contract , which moves in fractions of an index point under normal conditions , jumped two to three points in seconds early yesterday after an initial downturn , then moved strongly higher the rest of the day .	B-entrel	572..798
Each index point represents a $ 500 profit for each S&P 500 contract held .	B-entrel	799..872
For the first time since the 1987 crash , traders said that they were able to trade several hundred S&P 500 contracts at a time in a highly liquid market .	I-entrel	873..1026
Many institutions and individual investors have shied away from stock-index futures , blaming them for speeding the stock market crash on Black Monday two years ago .	B-cause	1029..1193
Since the crash , many futures traders have n't assumed large positions for fear that the S&P 500 market , with much of its customer order flow missing , would dry up if prices turned against them .	I-cause	1194..1387
More than 400 traders jammed the S&P 500 futures pit to await the opening bell .	B-restatement	1390..1469
Traders were shouting bids and offers a full five minutes before the start of trading at 8:30 am	I-restatement	1470..1566
The contract fell five points at the open to 323.85 , the maximum opening move allowed under safeguards adopted by the Merc to stem a market slide .	B-contrast	1569..1715
But several traders quickly stepped up and bid for contracts , driving prices sharply higher .	B-asynchronous	1716..1808
The market hovered near Friday 's closing price of 328.85 for about a half hour , moving several index points higher or lower in seconds , then broke higher and did n't look back .	I-asynchronous	1809..1984
The S&P 500 contract that expires in December closed up a record 15.65 points on volume of nearly 80,000 contracts .	O	1987..2102
" Traders five feet from each other were making bids and offers that were a full point apart , " said one S&P 500 broker .	O	2105..2223
" You could buy at the bid and sell at the offer and make a fortune , " he marveled .	O	2224..2305
Several of Wall Street 's largest securities firms , including Salomon Brothers Inc. and PaineWebber Inc. , were also large buyers , traders said .	O	2308..2450
Salomon Brothers was among the largest sellers of stock-index futures last week , traders said .	O	2451..2545
Brokerage firms as a rule do n't comment on their market activity .	B-conjunction	2546..2611
Unlike the week following Black Monday two years ago , individual traders in the S&P 500 pit were also being uncharacteristically circumspect about their one-day profits .	I-conjunction	2614..2783
" With the FBI around here , bragging rights are a thing of the past , " said one trader , referring to the federal investigation of futures trading that so far has resulted in 46 indictments lodged against individuals on the Merc and the Chicago Board of Trade .	O	2786..3043
