Digital Equipment Corp. reported a 32% decline in net income on a modest revenue gain in its fiscal first quarter, causing some analysts to predict weaker results ahead than they had expected.	O	9..201
Although the second-largest computer maker had prepared Wall Street for a poor quarter, analysts said they were troubled by signs of flat U.S. orders and a slowdown in the rate of gain in foreign orders.	B-entrel	204..407
The Maynard, Mass., company is in a transition in which it is trying to reduce its reliance on mid-range machines and establish a presence in workstations and mainframes.	I-entrel	408..578
Net for the quarter ended Sept. 30 fell to $150.8 million, or $1.20 a share, from $223 million, or $1.71 a share, a year ago.	B-contrast	581..706
Revenue rose 6.4% to $3.13 billion from $2.94 billion.	I-contrast	707..761
Digital said a shift in its product mix toward low-end products and strong growth in workstation sales yielded lower gross margins.	B-conjunction	764..895
A spokesman also said margins for the company's service business narrowed somewhat because of heavy investments made in that sector.	I-conjunction	896..1028
The lack of a strong product at the high end of Digital's line was a significant drag on sales.	B-contrast	1031..1126
Digital hopes to address that with the debut of its first mainframe-class computers next Tuesday.	B-restatement	1127..1224
The new line is aimed directly at International Business Machines Corp.	I-restatement	1225..1296
"Until the new mainframe products kick in, there won't be a lot of revenue contribution at the high end, and that's hurt us," said Mark Steinkrauss, Digital's director of investor relations.	O	1299..1489
He said unfavorable currency translations were also a factor in the quarter.	O	1490..1566
DEC shares rose $1.375 to $89.75 apiece in consolidated New York Stock Exchange trading yesterday.	O	1569..1667
But analysts said that against the backdrop of a nearly 40-point rise in the Dow Jones Industrial Average, that shouldn't necessarily be taken as a sign of great strength.	O	1668..1839
Some cut their earnings estimates for the stock this year and predicted more efforts to control costs ahead.	O	1840..1948
"I think the next few quarters will be difficult," said Steven Milunovich of First Boston.	O	1951..2041
"Margins will remain under pressure, and when the new mainframe does ship, I'm not sure it will be a big winner.	O	2042..2154
" Mr. Milunovich said he was revising his estimate for DEC's current year from $8.20 a share to "well below $8," although he hasn't settled on a final number.	O	2154..2312
One troubling aspect of DEC's results, analysts said, was its performance in Europe.	O	2315..2399
DEC said its overseas business, which now accounts for more than half of sales, improved in the quarter.	B-conjunction	2400..2504
It even took the unusually frank step of telling analysts in a morning conference call that orders in Europe were up in "double digits" in foreign-currency terms.	I-conjunction	2505..2667
That gain probably translated into about 5% to 7% in dollar terms, well below recent quarters' gains of above 20%, reckons Jay Stevens of Dean Witter Reynolds.	O	2670..2829
"That was a disappointment" and a sign of overall computer-market softness in Europe, Mr. Stevens said.	O	2830..2933
Marc Schulman, with UBS Securities in New York, dropped his estimate of DEC's full-year net to $6.80 a share from $8.	O	2936..3053
Although overall revenues were stronger, Mr. Schulman said, DEC "drew down its European backlog" and had flat world-wide orders overall.	O	3054..3190
"The bottom line is that it's more hand to mouth than it has been before," he said.	O	3191..3274
Mr. Schulman said he believes that the roll-out of DEC's new mainframe will "occur somewhat more leisurely" than many of his investment colleagues expect.	B-entrel	3277..3431
He said current expectations are for an entry level machine to be shipped in December, with all of the more sophisticated versions out by June.	I-entrel	3432..3575
For reasons he wouldn't elaborate on, he said he's sure that schedule won't be met, meaning less profit impact from the product for DEC in the next few quarters.	O	3576..3737
John R. Wilke contributed to this article.	O	3740..3782
