The latest crewcut in the equities markets reminds me of the joke T. Boone Pickens tells about the guy who was run over by the parade.	B-restatement	9..143
When asked "What went wrong?" the unfortunate victim replied, "It was a combination of things."	I-restatement	144..239
And so it was on Gray Friday.	O	240..269
The grand marshal of this parade would appear to have been excess leverage.	B-contrast	272..347
Even if that is so, however, it's probably the case that no barriers should have been erected to stop the procession before the end of the rout(e).	I-contrast	348..495
The ceremonies began Friday afternoon when word spread that the UAL buy-out was collapsing.	B-cause	498..589
Although the union-bidder expects to patch together a substitute offer, consisting of less cash, the failure to get cash from Japanese and American banks confirmed a growing fear among arbitragers that the pageant of high-leverage takeover deals is ending.	I-cause	590..846
Lots of other entries made up the parade, of course -- notably a surprisingly large increase in producer prices, signalling Federal Reserve tightness; and the Bush administration's (temporary?) defeat in trying to lower the capital-gains tax.	B-entrel	849..1091
As usual, few favorable reviews were heard for that ever-present marching band of program traders, although most serious studies suggest they only play the music that others write.	I-entrel	1092..1272
What really spooked the crowds along Wall Street, however, was the sudden concern that, whatever the reason, the pool of debt capital is drying up.	B-entrel	1275..1422
Gray Friday reflects a panic mainly by the takeover arbitragers, rather than the small investor, as their highly margined investments in the "deal" stocks are jeopardized by the unexpected drying up of the lubricant for deal financing.	I-entrel	1423..1658
Deal stocks led the market down as they absorbed the heaviest losses.	O	1661..1730
UAL, which triggered the slide, opened Monday at $224, down about 20% from Thursday's close.	O	1731..1823
UAL, which triggered the slide, opened Monday at $224, down about 20% from Thursday's close.AMR opened Monday at $80, down nearly 20% from Thursday's close.	B-conjunction	1824..1888
(Both took further hits yesterday.)	I-conjunction	1889..1924
Hilton lost 20% on Friday; Paramount lost almost 11%.	O	1925..1978
A careful look reveals that where deal financing has been secured, the target's stock price was not affected on Friday.	O	1979..2098
The multibillion-dollar prospects, where the bidder must line up a consortium of banks and/or issue billions in high-yield debt, were where the damage was concentrated.	O	2099..2267
The market for so-called junk bonds has been setting the stage for Friday's dramatic march for several weeks.	B-instantiation	2270..2379
The growing financial difficulties of recent high-leverage restructurings or takeovers, such as Resorts International, Integrated Resources, and Campeau's retailing empire, have cast a pall over the entire market for high-yield securities.	B-asynchronous	2380..2619
Investors have reacted by ignoring recent efforts to float junk bonds by Ohio Mattress and by forcing Ramada to postpone indefinitely its planned junk-bond sale and restructuring.	B-cause	2620..2799
As a result, high-yield mutual funds have declined across the board and the many firms planning to sell $11 billion in junk bonds before year-end are experiencing anxious times.	I-cause	2800..2977
These are all market excesses (putting aside the artificial boosts that the tax code gives to debt over equity), and what we've seen is the market reining them in.	O	2980..3143
Of course, Washington hadn't been silent in the days leading up to the debacle, and its tendency to meddle in the leverage equation remains a troublesome prospect, but those preliminary steps shouldn't distract us from the basic market fundamentalism that was at work on Friday.	O	3144..3422
If it is correct to find that concerns over corporate debt and LBOs caused Gray Friday, what are the implications for policy makers?	B-cause	3425..3557
After all, the stock market's response to the collapse of the UAL deal might be taken to confirm the anti-debt direction of regulators.	B-cause	3558..3693
Is this a case where private markets are approving of Washington's bashing of Wall Street?	I-cause	3694..3784
Absolutely not.	O	3785..3800
To the extent that Friday's sell-off reflected a sudden reappraisal of the excesses of leverage, the message is that Wall Street and the private markets are fully capable of imposing the appropriate incentives and sanctions on corporate behavior.	O	3803..4049
The national economic interests are much better served allowing the private interests of bankers and investors be the ultimate judges of the investment quality of various LBO deals and leveraged restructurings.	B-restatement	4050..4260
The recent difficulties in the junk-bond markets and the scarcity of bank capital for recent deals underscores the wisdom of letting the free markets operate.	I-restatement	4261..4419
If takeover premiums become excessive, if LBO dealmakers become too aggressive, then the private market will recognize these problems more quickly and accurately than will policy makers, and the markets will move with lightning speed to impose appropriate sanctions.	B-entrel	4422..4688
Yes, the broader exchanges got caught up in the spiral, but they rode the tiger up all year.	I-entrel	4689..4781
Not surprisingly, he sometimes bites.	O	4782..4819
The arbitragers and takeover initiatiors got killed on Gray Friday, while the besieged managers of prospective targets cheered lustily.	O	4822..4957
If you identify with the besieged managers, you must concede that speedy and effective relief from the excesses of the takeover market is more likely to come from the marketplace than from Washington.	O	4958..5158
If you side with the arbitragers and raiders, you clearly have more to fear from private investors than from regulators, although the Delaware courts should never be underestimated.	O	5159..5340
The truth is, Washington understands politics better than economics.	B-entrel	5343..5411
Although the average citizen is probably not harmed too much from Washington's rhetorical war against Wall Street regarding excessive financial leveraging, actual legislation would probably impose considerable harm.	I-entrel	5412..5627
Any such attempt to distinguish "good debt" from "bad debt," or to draw the line at a particular industry, such as the airlines, is likely to blunt the spur that the proper amount of leverage provides both to equity markets and economic efficiency in general.	O	5628..5887
Far better for policy makers to concentrate on the war against drugs, Panama and the deficit, all of them parades that seem never to end.	O	5890..6027
Mr. Jarrell, former top economist at the Securities and Exchange Commission, teaches at the University of Rochester's Simon Business School.	O	6030..6170
