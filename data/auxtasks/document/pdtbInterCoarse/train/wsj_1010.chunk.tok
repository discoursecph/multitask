Wall Street would like UAL Corp. 's board to change its mind about keeping the company independent .	B-comparison	9..108
But what happens next in the continuing takeover drama may depend more on the company 's two most powerful and fractious unions : the pilots and machinists .	I-comparison	109..263
Some people familiar with the situation believe that the collapse of the previous $ 6.79 billion buy-out , if anything , may have strengthened the hands of these two labor groups .	O	266..442
As a result , both may now have virtual veto power over any UAL transaction .	O	443..518
One reason : banks -- likely to react to any new UAL deal with even more caution than the first time around -- probably will insist on labor harmony before they agree to put up cash for any new bid or even a less-ambitious recapitalization plan .	O	521..765
" United pilots have shown on a number of occasions they are willing and able to strike , " said an executive at Fuji Bank , one of UAL 's large lenders .	O	768..916
" If you have both { labor } groups on strike , you 've got no revenue and that 's a very scary thing for a bank to be looking at . "	O	917..1042
Just this past week , a leading Japanese bank asked for a meeting with the machinists ' union leaders to determine where the union would stand if another bid or recapitalization became possible .	O	1045..1237
Another reason : Emboldened by their success in helping to scuttle the previous transaction , the machinists are likely to be more aggressive if a second buy-out attempt occurs .	O	1240..1415
The two unions already had significant leverage simply because their employer has yet to settle with either on new contracts .	O	1418..1543
That gives them both the threat of a strike and the ability to resist any wage concessions that may be necessary to make a transaction work .	B-contingency	1544..1684
Thus , even investors who are pushing for the board to do a recapitalization that would pay shareholders a special dividend and possibly grant employees an ownership stake acknowledge that the unions are key .	I-contingency	1687..1894
" There 's less likelihood of creating and completing a transaction without the unions ' cooperation and wage concessions , " said Richard Nye , of Baker , Nye Investments , a New York takeover stock-trader .	B-entrel	1895..2094
Mr. Nye thinks the UAL board should be ousted if it does n't move soon to increase shareholder value .	I-entrel	2095..2195
Both the pilots and machinists have made it clear that they intend to block any transaction they do n't like .	B-expansion	2198..2306
" The pilots will be involved in any transaction that takes place around here , " pilot union chairman Frederick C. Dubinsky declared yesterday .	I-expansion	2307..2448
But whether the pilots can team up with their longtime adversaries , the machinists , is another question .	O	2449..2553
The pilots ' Mr. Dubinsky says his union would like majority ownership for employees .	O	2556..2640
At the very least , the pilots want some form of control over the airline , perhaps through super-majority voting rights .	O	2641..2760
On the other hand , the machinists have always opposed majority ownership in principle , saying they do n't think employees should be owners .	O	2763..2901
Still , in recent days , machinists ' union leaders have shown some new flexibility .	O	2904..2985
" We may be able to reach a tradeoff where we can accommodate { the pilot union 's } concerns and ours , " said Brian M. Freeman , the machinists ' financial adviser .	O	2986..3144
Mr. Freeman said machinists ' union advisers plan to meet this week to try to draw up a blueprint for some form of recapitalization that could include a special dividend for shareholders , an employee stake and , perhaps , an equity investment by a friendly investor .	O	3147..3410
If a compromise ca n't be reached , the pilots maintain they can do a transaction without the support of the machinists .	B-comparison	3413..3531
But at this point , that may just be wishful thinking .	I-comparison	3532..3585
The machinists lobbied heavily against the original bid from UAL pilots and management for the company .	B-entrel	3588..3691
Their opposition helped scare off some Japanese banks .	B-expansion	3692..3746
The pilots ' insistence on majority ownership also may make the idea of a recapitalization difficult to achieve .	I-expansion	3749..3860
" Who wants to be a public shareholder investing in a company controlled by the pilots ' union ? " asks Candace Browning , an analyst at Wertheim Schroeder & Co .	O	3861..4017
" Who would the board be working for -- the public shareholders or the pilots ? " she adds .	O	4018..4106
Ms. Browning says she believes a recapitalization involving employee ownership would succeed only if the pilots relent on their demand for control .	B-expansion	4109..4256
She also notes that even if the pilots accept a minority stake now , they still could come back at a later time and try to take control .	I-expansion	4257..4392
Another possibility is for the pilots ' to team up with an outside investor who might try to force the ouster of the board through the solicitation of consents .	O	4395..4554
In that way , the pilots may be able to force the board to approve a recapitalization that gives employees a majority stake , or to consider the labor-management group 's latest proposal .	O	4555..4739
The group did n't make a formal offer , but instead told UAL 's advisers before the most-recent board meeting that it was working on a bid valued at between $ 225 and $ 240 a share .	B-comparison	4742..4918
But again , they may need the help of the machinists .	I-comparison	4919..4971
" I think the dynamics of this situation are that something 's got to happen , " said one official familiar with the situation .	O	4974..5097
The board and UAL 's management , he says , " ca n't go back " to business as usual .	O	5098..5176
" The pilots wo n't let them .	O	5177..5204
