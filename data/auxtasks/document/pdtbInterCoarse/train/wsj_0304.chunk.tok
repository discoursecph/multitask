The head trader of Chemical Banking Corp. 's interest-rate options group has left the company , following valuation errors that resulted in a $ 33 million charge against its third-quarter results .	O	9..203
Chemical said Steven Edelson resigned recently , but one individual close to the situation said the resignation was forced .	B-entrel	206..328
Mr. Edelson could n't be reached for comment .	I-entrel	329..373
A separate inquiry by Chemical cleared Mr. Edelson of allegations that he had been lavishly entertained by a New York money broker .	B-comparison	376..507
That inquiry has n't resolved similar allegations involving another Chemical options trader .	I-comparison	508..599
In other personnel changes stemming from problems in its options unit :	O	602..672
-- Chemical named James Kennedy , a trader in swaps contracts for the bank , to assume Mr. Edelson 's duties and to be trading manager for derivative products , including swaps and interest-rate options .	O	675..874
-- Lee Wakeman , vice president in charge of options research who discovered the valuation errors and was asked by senior management to straighten out the mess , resigned to take a position in asset and liability management at Continental Bank in Chicago .	B-entrel	877..1130
Mr. Wakeman , whom Chemical tried to keep , did n't return calls for comment .	I-entrel	1131..1205
Separately , Chemical confirmed that it took an undisclosed charge in the second quarter for losses on forward-rate agreements involving foreign currency written by its branch in Frankfurt , West Germany .	O	1208..1410
A Chemical spokeswoman said the second-quarter charge was " not material " and that no personnel changes were made as a result .	B-expansion	1413..1538
The spokeswoman said the Frankfurt situation was " totally different " from problems in the interest-rate options unit .	I-expansion	1539..1656
According to individuals familiar with the situation , the Frankfurt loss stemmed from a computer program for calculating prices on forward-rate agreements that failed to envision an interest-rate environment where short-term rates were equal to or higher than long-term rates .	O	1659..1935
While the incidents involving interest-rate options and forward-rate agreements are unrelated , some observers say they echo a 1987 incident in which Bankers Trust New York Corp. restated the value of its foreign exchange options contracts downward by about $ 80 million .	O	1938..2207
These complex products require close monitoring because each must be valued separately in light of current market conditions .	B-expansion	2210..2335
In an interest-rate options contract , a client pays a fee to a bank for custom-tailored protection against adverse interest-rate swings for a specified period .	B-expansion	2336..2495
In a forward-rate agreement , a client agrees to an exchange rate on a future currency transaction .	I-expansion	2496..2594
Some competitors maintain the interestrate option loss , in particular , may have resulted more from Chemical 's taking large and often contrarian positions than a valuation problem .	O	2597..2776
Started three years ago , Chemical 's interest-rate options group was a leading force in the field .	B-expansion	2779..2876
From 1987 to 1988 , the value of Chemical 's option contracts outstanding mushroomed to $ 37 billion from $ 17 billion .	B-expansion	2877..2992
More importantly , the volume of options written exceeded those purchased by almost 2-to-1 .	I-expansion	2993..3083
With such a lopsided book of options , traders say , Chemical was more vulnerable to erroneous valuation assumptions .	O	3084..3199
The Chemical spokeswoman said the bank has examined its methodologies and internal controls .	B-entrel	3202..3294
" We consider our internal controls to have worked well , " she said , adding that some procedures have been strengthened .	I-entrel	3295..3413
Its valuation methodologies , she said , " are recognized as some of the best on the Street .	O	3414..3503
Not a lot was needed to be done .	O	3504..3536
