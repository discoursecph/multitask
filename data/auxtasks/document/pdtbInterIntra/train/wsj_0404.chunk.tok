Gen-Probe Inc. , a biotechnology concern , said it signed a definitive agreement to be acquired by Chugai Pharmaceutical Co. of Tokyo for about $ 110 million , or almost double the market price of Gen-Probe 's stock .	root	9..220
The move is sure to heighten concerns about increased Japanese investment in U.S. biotechnology firms .	norel	223..325
It is also likely to bolster fears that the Japanese will use their foothold in U.S. biotechnology concerns to gain certain trade and competitive advantages .	conjunction	326..483
Gen-Probe , an industry leader in the field of genetic probes , which is a new technology used in diagnostic tests , last year signed an agreement for Chugai to exclusively market its diagnostic products in Japan for infectious diseases and cancer .	norel	486..731
Chugai agreed then to fund certain associated research and development costs .	asynchronous	732..809
That arrangement apparently has worked well , and Thomas A. Bologna , president and chief executive officer of Gen-Probe , founded in 1983 , said the sale of the company means " we will be able to concentrate on running the business rather than always looking for sources of financing . "	norel	812..1093
Chugai agreed to pay $ 6.25 a share for Gen-Probe 's 17.6 million common shares outstanding on a fully diluted basis .	entrel	1094..1209
Yesterday , in national trading in the over-the-counter market , Gen-Probe common closed at $ 3.25 a share .	norel	1212..1316
Because the U.S. leads in most areas of biotechnology -- largely because of research investment by the U.S. government -- the sale is sure to increase concerns that Japanese companies will buy American know-how and use it to obtain the upper hand in biotechnology trade and competition .	norel	1319..1605
" The biotechnology firms may be setting up their own competitors , " said Richard Godown , president of the Industrial Biotechnology Association .	norel	1608..1750
He added that until now the Japanese have only acquired equity positions in U.S. biotechnology companies .	cause	1751..1856
" They are piggybacking onto developed technology , " he said .	restatement	1857..1916
During the past five years , Japanese concerns have invested in several of the U.S. 's 431 independent biotechnology companies .	restatement	1917..2042
Chugai has been one of the most active Japanese players in U.S. biotechnology companies	instantiation	2043..2130
it has an equity investment in Genetics Institute Inc. , Cambridge , Mass. , and a joint-venture agreement with Upjohn Co. , Kalamazoo , Mich .	instantiation	2131..2269
The Japanese government , Mr. Godown said , has stated that it wants 10 % to 11 % of its gross national product to come from biotechnology products .	norel	2272..2416
" It is becoming more of a horse race every day between the U.S. and Japan , " he said , adding that some fear that as with the semiconductor , electronics , and automobile industries , Japanese companies will use U.S.-developed technology to gain trade advantages .	restatement	2417..2675
Mr. Bologna said the sale would allow Gen-Probe to speed up the development of new technology , and to more quickly apply existing technology to an array of diagnostic products the company wants to offer .	norel	2678..2881
By 1988 , when only 10 genetic probe-based tests of diagnostic infectious diseases of humans had been approved for marketing by the Food and Drug Administration , eight of them had been developed and sold by Gen-Probe .	norel	2884..3100
Osamu Nagayama , deputy president of Chugai , which spends about 15 % of its sales on research and development , was unable to pinpoint how much money Chugai would pump into Gen-Probe .	norel	3103..3283
" We think Gen-Probe has technology important to people 's health , " he said , adding : " We think it is important for us to have such technology . "	entrel	3284..3425
He and Mr. Bologna emphasized that both companies would gain technological knowledge through the sale of Gen-Probe , which will expand " significantly " as a result of the acquisition .	conjunction	3426..3607
In 1988 , Chugai had net income of $ 60 million on revenue of $ 991 million .	norel	3610..3683
GenProbe had a net loss of $ 9.5 million on revenue of $ 5.8 million .	contrast	3684..3751
Recently , Gen-Probe received a broad U.S. patent for a technology that helps detect , identify and quantify non-viral organisms through the targeting of a form of genetic material called ribosomal RNA .	concession	3752..3952
Among other things , Mr. Bologna said that the sale will facilitate Gen-Probe 's marketing of a diagnostic test for acquired immune deficiency syndrome , or AIDS .	norel	3955..4114
Chugai also will help Gen-Probe with its regulatory and marketing expertise in Asia , Mr. Bologna said .	conjunction	4117..4219
The tender offer for Gen-Probe 's shares is expected to begin next Monday , the company said .	entrel	4220..4311
