Tandem Computers Inc. ,	(NN-Topic-Change(NS-Elaboration(NS-Background(NS-Elaboration(SN-Attribution(NN-Same-unit
preparing to fight with International Business Machines Corp. for a piece of the mainframe business ,	(SN-Background
said	SN-Background))
it expects to post higher revenue and earnings for its fiscal fourth quarter	(NS-Elaboration
ended Sept. 30 .	NS-Elaboration))
Tandem said	(NS-Elaboration(SN-Attribution
it expects to report revenue of about $ 450 million and earnings of 35 cents to 40 cents a share .	SN-Attribution)
The results ,	(NS-Attribution(NN-Same-unit(NS-Elaboration
which are in line with analysts ' estimates ,	NS-Elaboration)
reflect `` a continued improvement in our U.S. business , ''	NN-Same-unit)
said James Treybig , Tandem 's chief executive officer .	NS-Attribution)))
In the year-earlier period , Tandem reported net income of $ 30.1 million , or 31 cents a share , on revenue of $ 383.9 million .	NS-Background)
Tandem expects to report the full results for the quarter next week .	(NS-Evaluation(NS-Elaboration
Analysts have predicted	(SN-Attribution
that the Cupertino , Calif. , company will report revenue of $ 430 million to $ 460 million and earnings of 35 cents to 40 cents a share .	SN-Attribution))
Commenting on the results for the quarter ,	(NS-Explanation(NS-Elaboration(SN-Background
Mr. Treybig said	(SN-Attribution
the strength of the company 's domestic business came as `` a surprise '' to him ,	SN-Attribution))
noting	(SN-Attribution
that sales `` in every region of the U.S. exceeded our plan . ''	SN-Attribution))
The company 's U.S. performance was helped by `` a record quarter for new customers , ''	(NS-Attribution
he said .	NS-Attribution))))
Tandem makes `` fault-tolerant '' computers	(NS-Elaboration(NS-Elaboration(NS-Elaboration
-- machines with built-in backup systems --	NS-Elaboration)
that run stock exchanges , networks of automatic tellers and other complex computer systems .	NS-Elaboration)
Tomorrow the company is scheduled to announce its most powerful computer ever ,	(NN-Topic-Comment(NS-Evaluation(NS-Elaboration(NS-Elaboration
which for the first time will bring it into direct competition with makers of mainframe computers .	NS-Elaboration)
Tandem 's new high-end computer is called Cyclone .	(NS-Elaboration
Prices for the machine ,	(NN-Same-unit(NS-Elaboration
which can come in various configurations ,	NS-Elaboration)
are $ 2 million to $ 10 million .	NN-Same-unit)))
Analysts expect the new computer to wrest a hefty slice of business away from IBM , the longtime leader in mainframes .	(NS-Explanation
`` We believe	(NS-Elaboration(NS-Elaboration(NS-Attribution(SN-Attribution
they could siphon perhaps two to three billion dollars from IBM '' over the next few years ,	SN-Attribution)
said George Weiss , an analyst at the Gartner group .	NS-Attribution)
That will spur Tandem 's growth .	NS-Elaboration)
`` I 'd be disappointed	(NS-Attribution(NS-Condition
if the company grew by less than 20 % next year , ''	NS-Condition)
said John Levinson , an analyst at Goldman , Sachs & Co .	NS-Attribution))))
IBM is expected to respond to Tandem 's Cyclone	(NS-Elaboration(NS-Manner-Means
by discounting its own mainframes ,	(NS-Elaboration
which	(NN-Same-unit(NS-Attribution
analysts say	NS-Attribution)
are roughly three times the price of a comparable system from Tandem .	NN-Same-unit)))
`` Obviously IBM can give bigger discounts to users immediately , ''	(SN-Contrast(NS-Attribution
said Mr. Weiss .	NS-Attribution)
But Mr. Treybig questions	(NS-Explanation(SN-Attribution
whether that will be enough	(NS-Elaboration
to stop Tandem 's first mainframe from taking on some of the functions	(NS-Elaboration
that large organizations previously sought from Big Blue 's machines .	NS-Elaboration)))
`` The answer is n't price reductions , but new systems , ''	(NS-Elaboration(SN-Contrast(NS-Attribution
he said .	NS-Attribution)
Nevertheless , Tandem faces a variety of challenges , the biggest	(NS-Elaboration
being that customers generally view the company 's computers as complementary to IBM 's mainframes .	NS-Elaboration))
Even Mr. Treybig is reluctant to abandon this notion ,	(NS-Explanation(NS-Elaboration
insisting	(SN-Attribution
that Tandem 's new machines are n't replacements for IBM 's mainframes .	SN-Attribution))
`` We 're after a little bigger niche , ''	(NS-Attribution
he said .	NS-Attribution)))))))))
