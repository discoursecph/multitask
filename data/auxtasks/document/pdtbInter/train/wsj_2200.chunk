Influential members of the House Ways and Means Committee introduced legislation that would restrict how the new savings-and-loan bailout agency can raise capital, creating another potential obstacle to the government's sale of sick thrifts.	O	9..250
The bill, whose backers include Chairman Dan Rostenkowski (D., Ill.), would prevent the Resolution Trust Corp. from raising temporary working capital by having an RTC-owned bank or thrift issue debt that wouldn't be counted on the federal budget.	O	253..499
The bill intends to restrict the RTC to Treasury borrowings only, unless the agency receives specific congressional authorization.	O	500..630
"Such agency `self-help' borrowing is unauthorized and expensive, far more expensive than direct Treasury borrowing," said Rep. Fortney Stark (D., Calif.), the bill's chief sponsor.	O	633..814
The complex financing plan in the S&L bailout law includes raising $30 billion from debt issued by the newly created RTC.	B-entrel	817..938
This financing system was created in the new law in order to keep the bailout spending from swelling the budget deficit.	B-entrel	939..1059
Another $20 billion would be raised through Treasury bonds, which pay lower interest rates.	I-entrel	1060..1151
But the RTC also requires "working" capital to maintain the bad assets of thrifts that are sold, until the assets can be sold separately.	O	1154..1291
That debt would be paid off as the assets are sold, leaving the total spending for the bailout at $50 billion, or $166 billion including interest over 10 years.	O	1292..1452
"It's a problem that clearly has to be resolved," said David Cooke, executive director of the RTC.	O	1455..1553
The agency has already spent roughly $19 billion selling 34 insolvent S&Ls, and it is likely to sell or merge 600 by the time the bailout concludes.	O	1554..1702
Absent other working capital, he said, the RTC would be forced to delay other thrift resolutions until cash could be raised by selling the bad assets.	O	1705..1855
"We would have to wait until we have collected on those assets before we can move forward," he said.	O	1856..1956
The complicated language in the huge new law has muddied the fight.	B-entrel	1959..2026
The law does allow the RTC to borrow from the Treasury up to $5 billion at any time.	B-conjunction	2027..2111
Moreover, it says the RTC's total obligations may not exceed $50 billion, but that figure is derived after including notes and other debt, and subtracting from it the market value of the assets the RTC holds.	I-conjunction	2112..2320
But Congress didn't anticipate or intend more public debt, say opponents of the RTC's working-capital plan, and Rep. Charles Schumer (D., N.Y.) said the RTC Oversight Board has been remiss in not keeping Congress informed.	O	2323..2545
"That secrecy leads to a proposal like the one from Ways and Means, which seems to me sort of draconian," he said.	O	2548..2662
"The RTC is going to have to pay a price of prior consultation on the Hill if they want that kind of flexibility."	O	2663..2777
The Ways and Means Committee will hold a hearing on the bill next Tuesday.	O	2780..2854
