As competition heats up in Spain 's crowded bank market , Banco Exterior de Espana is seeking to shed its image of a state - owned bank and move into new activities .	ROOT	0
Under the direction of , its new chairmanFrancisco Luzon , Spain 's seventh largest bank is undergoing a tough restructuring that analysts say may be the first step toward the bank 's privatization .	COREF_PREV	1
The state - owned industrial holding company Instituto Nacional de Industria and the Bank of Spain jointly hold a 13.94 % stake in Banco Exterior .	COREF_PREV	2
The government directly owns 51.4 % and Factorex , a financial services company , holds 8.42 % .	NONE	3
The rest is listed on Spanish stock exchanges .	NONE	4
Some analysts are concerned , however , that Banco Exterior may have waited too long to diversify from its traditional export - related activities .	COREF_OTHER	5
Catching up with commercial competitors in retail banking and financial services , they argue , will be difficult , particularly if market conditions turn sour .	COREF_PREV	6
If that proves true , analysts say Banco Exterior could be a prime partner -- or even a takeover target -- for either a Spanish or foreign bank seeking to increase its market share after 1992 , when the European Community plans to dismantle financial barriers .	COREF_OTHER	7
With 700 branches in Spain and 12 banking subsidiaries , five branches and 12 representative offices abroad , the Banco Exterior group has a lot to offer a potential suitor .	COREF_PREV	8
Mr. Luzon and his team , however , say they are n't interested in a merger .	COREF_OTHER	9
Instead , they are working to transform Banco Exterior into an efficient bank by the end of 1992 .	COREF_PREV	10
`` I want this to be a model of the way a public - owned company should be run , '' Mr. Luzon says .	COREF_PREV	11
Banco Exterior was created in 1929 to provide subsidized credits for Spanish exports .	COREF_PREV	12
The market for export financing was liberalized in the mid-1980s , however , forcing the bank to face competition .	COREF_PREV	13
At the same time , many of Spain 's traditional export markets in Latin America and other developing areas faced a sharp decline in economic growth .	COREF_OTHER	14
As a result , the volume of Banco Exterior 's export credit portfolio plunged from 824 billion pesatas ( $ 7.04 billion ) as of Dec. 31 , 1986 , to its current 522 billion pesetas .	COREF_OTHER	15
The other two main pillars of Banco Exterior 's traditional business -- wholesale banking and foreign currency trading -- also began to crumble under the weight of heavy competition and changing client needs .	COREF_PREV	16
The bank was hamstrung in its efforts to face the challenges of a changing market by its links to the government , analysts say .	COREF_PREV	17
Until Mr. Luzon took the helm last November , Banco Exterior was run by politicians who lacked either the skills or the will to introduce innovative changes .	COREF_PREV	18
But Mr. Luzon has moved swiftly to streamline bureaucracy , cut costs , increase capital and build up new areas of business .	COREF_PREV	19
`` We 've got a lot to do , '' he acknowledged .	COREF_PREV	20
`` We 've got to move quickly . ''	COREF_PREV	21
In Mr. Luzon 's first year , the bank eliminated 800 jobs .	COREF_PREV	22
Now it says it 'll trim another 1,200 jobs over the next three to four years .	COREF_PREV	23
The bank employs 8,000 people in Spain and 2,000 abroad .	COREF_PREV	24
To strengthen its capital base , Banco Exterior this year issued $ 105 million in subordinated debt , launched two rights issues and sold stock held in its treasury to small investors .	COREF_PREV	25
The bank is now aggressively marketing retail services at its domestic branches .	COREF_PREV	26
Last year 's drop in export credit was partially offset by a 15 % surge in lending to individuals and small and medium - sized companies .	COREF_PREV	27
Though Spain has an excess of banks , analysts say the country still has one of the most profitable markets in Europe , which will aid Banco Exterior with the tough tasks it faces ahead .	COREF_PREV	28
Expansion plans also include acquisitions in growing foreign markets .	COREF_PREV	29
The bank says it 's interested in purchasing banks in Morocco , Portugal and Puerto Rico .	COREF_PREV	30
But the bank 's retail activities in Latin America are likely to be cut back .	COREF_PREV	31
Banco Exterior was one of the last banks to create a brokerage house before the four Spanish stock exchanges underwent sweeping changes in July .	COREF_PREV	32
The late start may be a handicap for the bank as Spain continues to open up its market to foreign competition .	COREF_PREV	33
But Mr. Luzon contends that the experienced team he brought with him from Banco Bilbao Vizcaya , where he was formerly director general , will whip the bank 's capital market division into shape by the end of 1992 .	COREF_PREV	34
The bank also says it 'll use its international network to channel investment from London , Frankfurt , Zurich and Paris into the Spanish stock exchanges .	COREF_PREV	35
