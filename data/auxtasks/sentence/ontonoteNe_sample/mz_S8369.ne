After	O	0
making	O	1
Autumn	B-WORK_OF_ART	2
Moon	I-WORK_OF_ART	3
and	O	4
Temptation	B-WORK_OF_ART	5
of	I-WORK_OF_ART	6
a	I-WORK_OF_ART	7
Monk	I-WORK_OF_ART	8
,	O	9
two	B-CARDINAL	10
highly	O	11
stylized	O	12
works	O	13
,	O	14
Law	B-PERSON	15
went	O	16
back	O	17
to	O	18
the	O	19
theme	O	20
of	O	21
emigration	O	22
at	O	23
which	O	24
she	O	25
excels	O	26
,	O	27
getting	O	28
a	O	29
grant	O	30
from	O	31
the	O	32
Australian	B-NORP	33
government	O	34
to	O	35
film	O	36
Floating	B-WORK_OF_ART	37
Life	I-WORK_OF_ART	38
.	O	39
