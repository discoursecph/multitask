Crude oil futures prices fell further as analysts and traders said OPEC oil producers are n't putting the brakes on output ahead of the traditionally weak first quarter .	O	9..177
In trading on the New York Mercantile Exchange , the U.S. benchmark West Texas Intermediate crude fell 39 cents a barrel to $ 19.76 for December delivery .	B-conjunction	180..332
Petroleum products prices also declined .	I-conjunction	333..373
Analysts pointed to reports that the Organization of Petroleum Exporting Countries is producing substantially more than its official limit of 20.5 million barrels a day , with some accounts putting the 13-nation group 's output as high as 23 million barrels a day .	O	376..638
That level of production did n't take its toll on futures prices for the fourth quarter , when demand is traditionally strong .	O	641..765
But because first-quarter demand is normally the weakest of the year , several market participants say , OPEC production will have to decline to keep prices from eroding further .	B-entrel	766..942
The group plans to meet in a month to discuss production strategy for early 1990 .	I-entrel	943..1024
With prices already headed lower , news of a series of explosions at a major Phillips Petroleum Co. chemical facility on the Houston Ship Channel also was bearish for prices .	O	1027..1200
Even though such facilities use a relatively small amount of crude , analysts say , now the facility wo n't need any , at a time of already high availability .	O	1201..1355
The Phillips plant makes polyethylene , polypropylene and other plastic products .	B-entrel	1358..1438
A company official said the explosions began when a seal blew out .	I-entrel	1439..1505
Dozens of workers were injured , authorities said .	B-entrel	1506..1555
There was no immediate estimate of damage from the company .	I-entrel	1556..1615
Some petroleum futures traders say technical considerations now will help to put downward pressure on futures prices .	B-instantiation	1618..1735
For instance , one trader said that prices inevitably will go lower now that they 've fallen below $ 20 a barrel .	I-instantiation	1736..1846
" Our technician is a little bearish now that we 've taken out $ 20 , " he said .	O	1847..1922
In other commodity markets yesterday :	O	1925..1962
COPPER :	O	1965..1972
The selling that started on Friday continued yesterday .	B-restatement	1973..2028
The December contract fell 3.85 cents a pound to $ 1.1960 .	I-restatement	2029..2086
London Metal Exchange warehouse stocks were down only 4,800 metric tons for the week to 84,500 tons ; expectations late last week were a drop of 10,000 to 15,000 tons .	O	2087..2253
The New York market made its high for the day on the opening and when it dropped below the $ 1.23-a-pound level , selling picked up as previous buyers bailed out of their positions and aggressive short sellers -- anticipating further declines -- moved in .	O	2254..2507
Fund selling also picked up at that point .	B-conjunction	2508..2550
According to Bernard Savaiko , senior commodity analyst at PaineWebber , the only stability to the market came when short sellers periodically moved in to cover their positions by buying contracts .	I-conjunction	2551..2746
This activity produced small rallies , which in turn attracted new short selling .	O	2747..2827
Mr. Savaiko noted that copper had a steep fall in spite of a weak dollar , which would normally support the U.S. copper market .	B-entrel	2828..2954
Such support usually comes from arbitragers who use a strong British pound to buy copper in New York .	B-entrel	2955..3056
" The sell-off would probably have been worse if the dollar had been strong , " he said .	B-entrel	3057..3142
Copper has been stuck in a trading range of $ 1.19 to $ 1.34 .	I-entrel	3143..3202
Mr. Savaiko believes that if copper falls below the bottom of this range the next significant support level will be about $ 1.04 .	O	3203..3331
PRECIOUS METALS :	O	3334..3350
Platinum and palladium struggled to maintain their prices all day despite news stories over the weekend that recent cold fusion experiments , which use both metals , showed signs of producing extra heat .	B-restatement	3351..3552
January platinum closed down $ 2.80 an ounce at $ 486.30 , nearly $ 4 above its low for the day .	B-conjunction	3553..3645
December palladium was off $ 1.55 an ounce at $ 137.20 .	B-entrel	3646..3699
Platinum is believed to have good support around $ 480 and palladium at around $ 130 .	B-entrel	3700..3783
Some traders were thought to be waiting for the auto sales report , which will be released today .	B-cause	3784..3880
Such sales are watched closely by platinum and palladium traders because both metals are used in automobile catalytic converters .	I-cause	3881..4010
Mr. Savaiko theorized that the news on cold fusion did n't affect the market yesterday because many traders have already been badly burnt by such stories .	O	4011..4164
He said the traders are demanding a higher level of proof before they will buy palladium again .	O	4165..4260
Also weighing on both metals ' prices is the role of the chief supplier , the Soviet Union .	O	4261..4350
Many analysts believe that the Soviets ' thirst for dollars this year to buy grain and other Western commodities and goods will bring them to the market whenever prices rally very much .	O	4351..4535
GRAINS AND SOYBEANS :	O	4538..4558
Prices closed mixed as contracts reacted to largely offsetting bullish and bearish news .	B-restatement	4559..4647
On the Chicago Board of Trade , soybeans for November delivery closed at $ 5.63 a bushel , down half a cent , while the December wheat contract rose three-quarters of a cent to $ 4.0775 a bushel .	B-conjunction	4648..4838
Supporting prices was the announcement late Friday of additional grain sales to the Soviet Union .	B-contrast	4839..4936
But acting as a drag on prices was the improved harvest weather over the weekend and the prospect for continued fair weather this week over much of the Farm Belt .	B-conjunction	4937..5099
Strong farmer selling over the weekend also weighed on prices .	I-conjunction	5100..5162
SUGAR :	O	5165..5171
World prices tumbled , mostly from their own weight , according to analysts .	B-restatement	5172..5246
The March contract ended at 13.79 cents a pound , down 0.37 cent .	B-entrel	5247..5311
For the past week or so , traders have been expecting India to buy between 150,000 and 200,000 tons of refined sugar , and there have been expectations of a major purchase by Japan .	B-contrast	5312..5491
But with no reports of either country actually entering the market , analysts said , futures prices became vulnerable .	I-contrast	5492..5608
Developing countries such as India , some analysts said , seem to have made it a point to stay away whenever sugar reached the top of its trading range , around 14.75 cents , and wait for prices to return to the bottom of the range , around 13.50 cents .	B-contrast	5609..5857
But Erik Dunlaevy , a sugar analyst with Balfour Maclaine International Ltd. , said the explanation for the latest drop in sugar prices is much simpler : Speculators , he said , " got too long too soon and ran into resistance around the old contract highs . "	I-contrast	5858..6109
A PaineWebber analyst said that in light of a new estimate of a production increase of four million metric tons and only a modest increase in consumption , sugar is n't likely to rise above the top of its trading range without a crop problem in a major producing country .	O	6110..6379
COCOA :	O	6382..6388
Futures rallied modestly .	B-restatement	6389..6414
The December contract rose $ 33 a metric ton to $ 1,027 , near its high for the day .	I-restatement	6415..6496
Gill & Duffus Ltd. , a British cocoa-trading house , estimated that the 1989-90 world cocoa surplus would be 231,000 tons , down from 314,000 tons for the previous year .	B-entrel	6497..6663
Market technicians were encouraged by the price patterns , which in the past have preceded sharp rallies .	B-conjunction	6664..6768
Recent prices for cocoa have been near levels last seen in the mid-1970s .	B-entrel	6769..6842
At such prices , according to Mr. Savaiko , bargain hunting and short-covering -- buying back of contracts previously sold -- by speculators is n't uncommon .	B-contrast	6843..6997
But Mr. Savaiko expects stepped-up producer selling at around the $ 1,040 to $ 1,050 level .	B-conjunction	6998..7087
He also noted that a strong sterling market yesterday might have helped cocoa in New York as arbitragers took advantage of the currency move .	B-contrast	7088..7229
Sandra Kaul , research analyst at Shearson Lehman Hutton , said the market pushed higher mainly in anticipation of a late harvest in the Ivory Coast , a major cocoa producer .	I-contrast	7230..7401
