Senate Democrats favoring a cut in the capital-gains tax have decided , under pressure from their leaders , not to offer their own proposal , placing another obstacle in the path of President Bush 's legislative priority .	O	9..226
A core group of six or so Democratic senators has been working behind the scenes to develop a proposal to reduce the tax on the gain from the sale of assets .	B-conjunction	229..386
The plan was complete except for finishing touches , and there was talk that it would be unveiled as early as yesterday .	B-contrast	387..506
But Senate Majority Leader George Mitchell ( D. , Maine ) , a vigorous opponent of the capital-gains tax cut , called the group to meet with him Wednesday night and again yesterday .	B-conjunction	509..685
Sen. Mitchell urged them to desist .	I-conjunction	686..721
Afterward , leaders of the dissident Democrats relented , and said they would n't offer their own proposal as they had planned .	O	722..846
The decision is a setback for President Bush , who needs the support of Democrats to pass the tax cut through the Democratic-controlled Senate .	O	849..991
Having a proposal sponsored by Democrats would have given the president an advantage .	B-contrast	992..1077
Having only a Republican measure makes the task harder .	B-contrast	1078..1133
Still , Sen. Bob Packwood ( R. , Ore. ) , the lead sponsor of the Republican capital-gains amendment , predicted that the tax cut would be enacted this year .	I-contrast	1136..1287
He said a clear majority of senators back the tax reduction and that ultimately there would be enough senators to overcome any procedural hurdle the Democratic leadership might erect .	B-contrast	1288..1471
But Sen. Mitchell , buoyed by his victory among fellow Democrats , strongly disagreed .	I-contrast	1474..1558
Mr. Mitchell has been predicting that the president 's initiative would fail this year .	O	1559..1645
Yesterday , in an interview , he added that the Democrats ' decision " increases the likelihood that a capital-gains tax cut will not pass this year . "	O	1646..1792
Mr. Mitchell 's first victory came last week , when the Senate passed a deficit-reduction bill that did n't contain a capital-gains provision .	O	1793..1932
That vote made it unlikely that a capital-gains tax cut would be included in the final bill , now being drafted by House and Senate negotiators .	B-contrast	1933..2076
The House version of the bill does include the tax cut .	I-contrast	2077..2132
Now Republican leaders are concentrating on attaching a capital-gains amendment to some other bill , perhaps a measure raising the federal borrowing limit or a second tax bill that would follow on the heels of the deficit-reduction legislation .	O	2135..2378
To help lay the groundwork for that fight , President Bush plans early next week to meet at the White House with some 20 Democratic senators who favor cutting the capital-gains tax or are undecided on the issue .	O	2379..2589
The president apparently will have only one bill to push , Sen. Packwood 's , and at least some of the dissident Democrats plan to support it .	O	2592..2731
" I may want to offer additional amendments to improve it when the bill comes to the floor , " said Sen. David Boren ( D. , Okla. ) , a leader of those Democrats .	O	2732..2887
The Packwood plan , as expected , would allow individuals to exclude from income 5 % of the gain from the sale of a capital asset held for more than one year .	B-conjunction	2890..3045
The exclusion would rise five percentage points for each year the asset was held , until it reached a maximum of 35 % after seven years .	B-conjunction	3046..3180
The exclusion would apply to assets sold after Oct. 1 .	I-conjunction	3181..3235
As an alternative , taxpayers could chose to reduce their gains by an inflation index .	O	3236..3321
For corporations , the top tax rate on the sale of assets held for more than three years would be cut to 33 % from the current top rate of 34 % .	B-conjunction	3324..3465
That rate would gradually decline to as little as 29 % for corporate assets held for 15 years .	I-conjunction	3466..3559
The Packwood plan also would include a proposal , designed by Sen. William Roth ( R. , Del. ) , that would create new tax benefits for individual retirement accounts .	B-restatement	3562..3723
The Roth plan would create a new , non-deductible IRA from which money could be withdrawn tax-free not only for retirement , but also for the purchase of a first home , education expenses and medical expenses .	B-conjunction	3724..3930
Current IRAs could be rolled over into the new IRAs , but would be subject to tax though no penalty .	I-conjunction	3931..4030
