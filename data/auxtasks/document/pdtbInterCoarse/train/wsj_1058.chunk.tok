Place a phone order through most any catalog and chances are the clerk who answers wo n't be the only one on the line .	B-contingency	9..126
Bosses have big ears these days .	I-contingency	127..159
Or open up an electronics magazine and peruse the ads for sneaky tape recorders and other snooping gadgets .	B-expansion	162..269
Some would make even James Bond green with envy .	I-expansion	270..318
Eavesdropping -- both corporate and private -- is on the rise , thanks to the proliferation of surveillance technologies .	B-expansion	321..441
And while sellers of the equipment and companies " monitoring " employees have few qualms , right-to-privacy advocates and some lawmakers are alarmed .	I-expansion	442..589
" New technologies are changing the way we deal with each other and the way we work , " says Janlori Goldman , a staff attorney at the American Civil Liberties Union .	O	592..754
" Our expectation of confidentiality is being eroded . "	O	755..808
On the corporate side , companies claim that monitoring employee phone conversations is both legal and necessary to gauge productivity and ensure good service .	O	811..969
The practice is common at catalog , insurance and phone companies , banks and telemarketers , according to trade groups and worker organizations .	B-expansion	970..1112
It 's also widespread for reservations clerks in the airline , car-rental , hotel and railroad industries .	I-expansion	1113..1216
The Communications Workers of America , which opposes such monitoring , says supervisors listen in on an estimated 400 million calls each year .	O	1219..1360
Among companies saying they monitor employees are United Airlines , American Airlines , United Parcel Service , Nynex Corp. , Spiegel Inc. , and the circulation department of this newspaper .	B-expansion	1361..1546
Some Wall Street firms monitor for recordkeeping purposes .	I-expansion	1547..1605
Dictaphone Corp. says there 's a big business demand for its voice-activated taping systems , whether the sophisticated Veritrac 9000 system , which costs from $ 10,000 to $ 120,000 and can record 240 conversations simultaneously , or simple handheld units selling for $ 395 .	O	1608..1876
Businesses " want to verify information and ensure accuracy , " says John Hiltunen , Dictaphone 's manager of media relations .	O	1879..2000
The state of Alaska recently bought the Veritrac system , he says , " to monitor the Exxon cleanup effort . "	O	2001..2105
Merrill Lynch & Co. and Shearson Lehman Hutton Inc. say they use voice-activated systems to record and verify orders between salesmen and traders .	O	2108..2254
Shearson says it has taped some of its institutional trading desks , such as commodities and futures , for about four years .	O	2255..2377
Both companies stress that employees know they are being recorded and that customer conversations are n't taped .	O	2378..2489
Kidder Peabody & Co. says it monitors bond-trading conversations between brokers and customers to safeguard order accuracy .	O	2492..2615
Eavesdropping by individuals is harder to measure .	B-comparison	2618..2668
But devices are there for the asking , whether in stores or through the mail .	B-expansion	2669..2745
The Counter Spy Shop in Washington , D.C. , for instance , offers the " Secret Connection " attache case , which can surreptitiously record conversations for nine hours at a stretch .	I-expansion	2746..2922
That and other fancy gizmos may cost thousands , but simple voice-activated tape recorders sell for as little as $ 70 at electronics stores like Radio Shack .	O	2923..3078
The most common use of spying devices is in divorce cases , say private investigators .	O	3081..3166
While tape recordings to uncover , say , infidelity are n't admissible in court , they can mean leverage in a settlement .	O	3167..3284
Concerned with the increased availability of surveillance technology and heavier use of it , lawmakers have proposed laws addressing the issue .	O	3287..3429
Nine states have introduced bills requiring that workers and customers be made aware of monitoring .	B-expansion	3430..3529
And four states -- California , Florida , Michigan and Pennsylvania -- have adopted rules that all parties involved must consent when phone calls are recorded .	I-expansion	3530..3687
Two bills in Congress hope to make such restrictions national .	B-expansion	3690..3752
In May , Rep. Don Edwards ( D. Calif . ) introduced congressional legislation that would require an audible beeping during any employee monitoring , warning people that they are being heard .	I-expansion	3753..3938
( The legislation is similar to a 1987 " beeper bill " that was defeated after heavy lobbying by the telemarketing industry . )	O	3939..4061
Also last spring , Rep. Ron Dellums ( D. , Calif. ) , introduced a bill requiring universal two-party consent to any tapings in cases that do n't involve law enforcement .	O	4062..4226
In addition , products such as voice-activated tape recorders would have to include beep tones and labels explaining federal laws on eavesdropping .	O	4227..4373
The outlook on both federal bills is uncertain , especially remembering the 1987 defeat .	O	4376..4463
The ACLU and worker organizations back tighter laws , but employers and device manufacturers object .	O	4464..4563
" I 'm sympathetic with workers who feel under the gun , " says Richard Barton of the Direct Marketing Association of America , which is lobbying strenuously against the Edwards beeper bill .	O	4566..4751
" But the only way you can find out how your people are doing is by listening . "	O	4752..4830
The powerful group , which represents many of the nation 's telemarketers , was instrumental in derailing the 1987 bill .	O	4831..4948
Spiegel also opposes the beeper bill , saying the noise it requires would interfere with customer orders , causing irritation and even errors .	O	4951..5091
Laura Dale , center manager at the catalog company 's customer order center in Reno , Nev. , defends monitoring .	O	5092..5200
" We like to follow up and make sure operators are achieving our standards of company service , " says Ms. Dale , who supervises 350 operators .	O	5201..5340
John Bonomo , a Nynex spokesman , says the telephone company needs to monitor operators to evaluate performance during the first six months on the job .	O	5343..5492
" Sometimes , " he says , " we 'll pull someone off the phones for more training . "	O	5493..5569
Federal wiretap statutes recognize the right of employers to monitor employees ' for evaluation purposes .	B-expansion	5572..5676
And in the past , Congress has viewed monitoring as an issue best handled in union negotiations .	B-comparison	5677..5772
But opponents , led by the CWA , say new laws are needed because monitoring is heavily concentrated in service industries and 81 % of monitored workers are n't represented by unions .	I-comparison	5773..5951
The CWA claims that monitoring not only infringes on employee privacy , but increases stress .	O	5952..6044
" Nine to Five , " a Cleveland-based office workers organization that supports the beeper bill , six months ago started a privacy hot line to receive reports of alleged monitoring abuses .	B-comparison	6047..6230
Meanwhile , supporters of the Dellums two-party consent bill say it is needed because of a giant loophole in the one-party consent law .	I-comparison	6233..6367
Currently , if the person taping is a party to the conversation , it 's all right to record without the knowledge of the other person on the line .	B-comparison	6368..6511
( Intercepting other people 's private conversations is illegal and punishable by five years in prison and fines of $ 10,000 . )	I-comparison	6512..6635
The electronics industry is closely following the Dellums bill .	B-expansion	6638..6701
Some marketers of surveillance gear -- including Communication Control System Ltd. , which owns the Counter Spy Shop and others like it -- already put warning labels in their catalogs informing customers of the one-party law .	I-expansion	6702..6926
But vendors contend that they ca n't control how their products are used .	O	6927..6999
Radio Shack says it has a policy against selling products if a salesperson suspects they will be used illegally .	O	7002..7114
" Everything sold at Radio Shack has a legal purpose , " says Bernard Appel , president of the Tandy Corp. subsidiary .	O	7115..7229
He says he has n't yet studied the Dellums bill , but that requiring a beeping tone on recorders " would be ludicrous . "	O	7230..7346
Still , Radio Shack is aware that some of its products are controversial .	O	7349..7421
A few years ago , the company voluntarily stopped selling " The Big Ear , " a powerful microphone .	O	7422..7516
With its ability to pick up rustlings and flapping wings , " it was meant to be a toy for children for bird watching , " says Mr. Appel .	O	7517..7649
" But we were getting too many complaints that people were using them to eavesdrop on their neighbors . "	O	7650..7752
