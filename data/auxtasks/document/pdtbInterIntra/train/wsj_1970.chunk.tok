A surprising surge in the U.S. trade deficit raised fears that the nation 's export drive has stalled , and caused new turmoil in financial markets .	root	9..155
The merchandise trade deficit widened in August to $ 10.77 billion , the Commerce Department reported , a sharp deterioration from July 's $ 8.24 billion and the largest deficit of any month this year .	norel	158..354
Exports fell for the second month in a row	synchrony	355..397
while imports rose to a record .	contrast	398..430
" This is one of the worst trade releases we 've had since the dollar troughed out in 1987 , " said Geoffrey Dennis , chief international economist at James Capel Inc .	norel	433..595
Like most analysts , Mr. Dennis was hesitant to read too much into one month 's numbers ; but he said , " It indicates perhaps that the balance in the U.S. economy is not as good as we 've been led to believe . "	contrast	596..800
The number had a troubling effect on Wall Street , suggesting that more fundamental economic problems may underlie last Friday 's stock market slide .	norel	803..950
The Dow Jones Industrial Average tumbled more than 60 points after the report 's release	instantiation	951..1038
before recovering to close 18.65 points lower at 2638.73 .	asynchronous	1039..1097
" This bad trade number raises some deeper issues about the market decline , " said Norman Robertson , chief economist for Mellon Bank .	norel	1100..1231
" It raises questions about more deep-seated problems , the budget deficit and the trade deficit and the seeming lack of ability to come to grips with them . "	restatement	1232..1387
The trade report drew yet another unsettling parallel to October 1987 .	norel	1390..1460
On Oct. 14 of that year , the announcement of an unusually large August trade deficit helped trigger a steep market decline .	restatement	1461..1584
The slide continued until the record 508-point market drop on Oct. 19 .	asynchronous	1585..1655
In 1987 , however , the news was the latest in a string of disappointments on trade	norel	1656..1737
while the current report comes after a period of improvement .	contrast	1738..1800
The bleak trade report was played down by the Bush administration .	norel	1803..1869
Commerce Secretary Robert Mosbacher called the worsening trade figures " disappointing after two very good months . "	instantiation	1870..1984
And White House spokesman Marlin Fitzwater said the deficit was " an unwelcome increase , " adding that " we 're hopeful that it simply is a one-month situation and will turn around . "	conjunction	1985..2163
But the figures reinforced the view of many private analysts that the improvement in the U.S. trade deficit has run out of steam .	norel	2166..2295
" The figures today add further evidence to support the view that the improvement in the U.S. trade deficit has essentially stalled out at a level of about a $ 110 billion annual rate , " said Jeffrey Scott , a research fellow at the Institute for International Economics here .	norel	2298..2570
" That 's still an improvement over last year , but it leads one to conclude that basically we 've gotten all the mileage we can out of past dollar depreciation and past marginal cuts in the federal budget deficit . "	contrast	2571..2782
Exports declined for the second consecutive month in August , slipping 0.2 % to $ 30.41 billion , the Commerce Department reported .	norel	2785..2912
Imports , on the other hand , leaped 6.4 % to a record $ 41.18 billion .	contrast	2913..2980
Not only was August 's deficit far worse than July 's	norel	2983..3034
but the government revised the July figure substantially from the $ 7.58 billion deficit it had initially reported last month .	contrast	3035..3161
Many economists contend that deep cuts in the U.S. budget deficit are needed before further trade improvement can occur .	norel	3164..3284
That 's	cause	3285..3291
because the budget deficit feeds an enormous appetite in this country for both foreign goods and foreign capital , overwhelming the nation 's capacity to export .	cause	3292..3451
" People are sick and tired of hearing about these deficits , but the imbalances are still there and they are still a problem , " said Mr. Robertson .	entrel	3452..3597
In addition , the rise in the value of the dollar against foreign currencies over the past several months has increased the price of U.S. products in overseas markets and hurt the country 's competitiveness .	norel	3600..3805
Since March , exports have been virtually flat .	cause	3806..3852
At the same time , William T. Archey , international vice president at the U.S. Chamber of Commerce , notes : " Clearly the stronger dollar has made imports more attractive " by causing their prices to decline .	norel	3853..4057
Most economists expect the slowing U.S. economy to curb demand for imports .	norel	4060..4135
But they foresee little substantial progress in exports	contrast	4136..4191
unless the dollar and the federal budget deficit come down .	alternative	4192..4251
" The best result we could get from these numbers would be to see the administration and Congress get serious about putting the U.S. on an internationally competitive economic footing , " said Howard Lewis , vice president of international economic affairs at the National Association of Manufacturers .	norel	4254..4552
" That must start with cutting the federal budget deficit . "	entrel	4553..4611
August 's decline in exports reflected decreases in sales of industrial supplies , capital goods and food abroad and increases in sales of motor vehicles , parts and engines .	norel	4614..4785
The jump in imports stemmed from across-the-board increases in purchases of foreign goods .	contrast	4786..4876
The numbers were adjusted for usual seasonal fluctuations .	norel	4879..4937
Alan Murray contributed to this article .	norel	4940..4980
( In billions of U.S. dollars , not seasonally adjusted )	norel	4983..5037
* Newly industrialized countries : Singapore , Hong Kong , Taiwan , South Korea	norel	5040..5114
