Former Democratic fund-raiser Thomas M. Gaubert , whose savings and loan was wrested from his control by federal thrift regulators , has been granted court permission to sue the regulators .	O	9..196
In a ruling by the Fifth U.S. Circuit Court of Appeals in New Orleans , Mr. Gaubert received the go-ahead to pursue a claim against the Federal Home Loan Bank Board and the Federal Home Loan Bank of Dallas for losses he suffered when the Bank Board closed the Independent American Savings Association of Irving , Texas .	O	199..516
Mr. Gaubert , who was chairman and the majority stockholder of Independent American , had relinquished his control in exchange for federal regulators ' agreement to drop their inquiry into his activities at another savings and loan .	O	519..748
As part of the agreement , Mr. Gaubert contributed real estate valued at $ 25 million to the assets of Independent American .	O	749..871
While under the control of federal regulators , Independent American 's net worth dropped from $ 75 million to a negative $ 400 million , wiping out the value of Mr. Gaubert 's real estate contribution and his stock in the institution .	O	874..1103
Mr. Gaubert 's suit to recover his damages was dismissed last year by U.S. District Judge Robert Maloney of Dallas under the Federal Tort Claims Act , which offers broad protection for actions by federal agencies and employees .	O	1106..1331
Earlier this week , a Fifth Circuit appellate panel upheld Judge Maloney 's dismissal of Mr. Gaubert 's claim as a shareholder but said the judge should reconsider Mr. Gaubert 's claim for the loss of his property .	O	1334..1544
" It may depend on whether there was an express or implied promise ... that the federal officials would not negligently cause the deterioration " of Independent American , the court wrote .	B-entrel	1545..1730
Mr. Gaubert 's lawyer , Abbe David Lowell of Washington , D.C. , says the impact of the ruling on other cases involving thrift takeovers will depend on the degree of similarity in the facts .	I-entrel	1731..1917
" I do n't know if this will affect one institution or a hundred , " Mr. Lowell says .	O	1918..1999
" It does establish a very clear precedent for suing the FHLBB where there was none before . "	O	2000..2091
MAITRE'D CLAIMS in suit that restaurant fired her because she was pregnant .	O	2094..2169
In a suit filed in state court in Manhattan , the American Civil Liberties Union is representing the former maitre 'd of the chic Odeon restaurant .	O	2172..2317
The suit , which seeks compensatory and punitive damages of $ 1 million , alleges that the firing of Marcia Trees Levine violated New York state 's human-rights law .	O	2318..2479
Among other things , the law prohibits discrimination on the basis of sex and pregnancy .	O	2480..2567
The suit alleges that Ms. Levine was fired after she refused to accept a lower paying , less visible job upon reaching her sixth month of pregnancy .	O	2570..2717
Ms. Levine told her employer that she was pregnant in February ; a month later , the suit says , the restaurant manager told Ms. Levine that she would be demoted to his assistant because he felt customers would be uncomfortable with a pregnant maitre 'd .	O	2718..2968
Kary Moss , an attorney with the ACLU 's Women 's Rights Project , said , " They wanted a svelte-looking woman , and a pregnant woman is not svelte .	O	2971..3112
They told her , ' We do n't hire fat people and we do n't hire cripples .	O	3113..3181
And pregnant women are fat . ' "	O	3182..3212
Ms. Moss said Ms. Levine secretly taped many conversations with her bosses at the Odeon in which they told her she was being fired as maitre 'd because she was pregnant .	O	3213..3381
Paul H. Aloe , an attorney for Odeon owner Keith McNally , denied the allegations .	O	3384..3464
He said Ms. Levine had never been fired , although she had stopped working at the restaurant .	O	3465..3557
" The Odeon made a written offer to Marcia Levine on July 10 to return to work as the maitre 'd , at the same pay , same hours and with back pay accrued , " he said .	O	3558..3717
Mr. Aloe said the Odeon " has no policy against hiring pregnant people . "	O	3720..3791
LAWYERS IN Texas 's biggest bank-fraud case want out in face of retrial .	O	3794..3865
Lawyers representing five of the seven defendants in the case say their clients can no longer afford their services .	O	3868..3984
The trial of the case lasted seven months and ended in September with a hung jury .	O	3985..4067
The defendants were indicted two years ago on charges that they conspired to defraud five thrifts of more than $ 130 million through a complicated scheme to inflate the price of land and condominium construction along Interstate 30 , east of Dallas .	O	4070..4317
The defense lawyers , three of whom are solo practitioners , say they ca n't afford to put their law practices on hold for another seven-month trial .	O	4320..4466
Some of the lawyers say they would continue to represent their clients if the government pays their tab as court-appointed lawyers .	O	4467..4598
Assistant U.S. Attorney Terry Hart of Dallas says the government will oppose any efforts to bring in a new defense team because it would delay a retrial .	O	4601..4754
FEDERAL JUDGE ALCEE HASTINGS of Florida , facing impeachment , received an unanticipated boost yesterday .	B-restatement	4757..4860
Sen. Arlen Specter ( R. , Pa . ) urged acquittal of the judge in a brief circulated to his Senate colleagues during closed-door deliberations .	B-restatement	4861..4999
Among other things , the brief cited insufficient evidence .	B-entrel	5000..5058
Sen. Specter was vice chairman of the impeachment trial committee that heard evidence in the Hastings case last summer .	I-entrel	5059..5178
A former prosecutor and member of the Senate Judiciary Committee , Sen. Specter is expected to exercise influence when the Senate votes on the impeachment today .	O	5179..5339
RICHMOND RESIGNATIONS :	O	5342..5364
Six partners in the Richmond , Va. , firm of Browder , Russell , Morris & Butcher announced they are resigning .	O	5365..5472
Five of the partners -- James W. Morris , Philip B. Morris , Robert M. White , Ann Adams Webster and Jacqueline G. Epps -- are opening a boutique in Richmond to concentrate on corporate defense litigation , particularly in product liability cases .	O	5473..5716
The sixth partner , John H. OBrion , Jr. , is joining Cowan & Owen , a smaller firm outside Richmond .	O	5717..5814
LAW FIRM NOTES :	O	5817..5832
Nixon , Hargrave , Devans & Doyle , based in Rochester , N.Y. , has opened an office in Buffalo , N.Y ... .	O	5833..5933
Mayer , Brown & Platt , Chicago , added two partners to its Houston office , Eddy J. Roger Jr. , and Jeff C. Dodd ... .	O	5934..6047
Copyright specialist Neil Boorstyn , who writes the monthly Copyright Law Journal newsletter , is joining McCutchen , Doyle , Brown & Enersen .	O	6048..6186
