The Polish rat will eat well this winter .	ROOT	0
Tons of delectably rotting potatoes , barley and wheat will fill damp barns across the land as thousands of farmers turn the state 's buyers away .	NONE	1
Many a piglet wo n't be born as a result , and many a ham will never hang in a butcher shop .	NONE	2
But with inflation raging , grain in the barn will still be a safer bet for the private farmer than money in the bank .	NONE	3
Once again , the indomitable peasant holds Poland 's future in his hands .	NONE	4
Until his labor can produce a profit in this dying and distorted system , even Solidarity 's sympathetic new government wo n't win him over .	COREF_PREV	5
In coming months , emergency food aid moving in from the West will be the one buffer between a meat - hungry public and a new political calamity .	NONE	6
Factory workers on strike knocked Poland 's Communist bosses off balance last year ; this year , it was the farmers who brought them down .	COREF_OTHER	7
In June , farmers held onto meat , milk and grain , waiting for July 's usual state - directed price rises .	NONE	8
The Communists froze prices instead .	NONE	9
The farmers ran a boycott , and meat disappeared from the shops .	NONE	10
On Aug. 1 , the state tore up its controls , and food prices leaped .	COREF_OTHER	11
Without buffer stocks , inflation exploded .	NONE	12
That was when the tame old Peasants ' Party , desperate to live through the crisis , broke ranks with the Communists and joined with Solidarity in the East Bloc 's first liberated government .	COREF_OTHER	13
But by the time Solidarity took office in September , the damage was done .	COREF_PREV	14
`` Shortageflation , '' as economists have come to call it , had gone hyper .	NONE	15
The cost of raising a pig kept bounding ahead of the return for selling one .	NONE	16
The farmers stayed angry .	NONE	17
They still are .	COREF_PREV	18
At dawn on a cool day , hundreds travel to the private market in Radzymin , a town not far from , hauling pigs , cattle and sacks of feed that the state 's official buyers ca n't induce them to sell .	COREF_OTHER	19
Here , they are searching for a higher price .	COREF_PREV	20
In a crush of trucks and horse carts on the trodden field , Andrzej Latowski wrestles a screeching , overweight hog into the trunk of a private butcher 's Polish Fiat .	NONE	21
`` Of course it 's better to sell private , '' he says , as the butcher trundles away .	COREF_PREV	22
`` Why should anybody want to sell to them ? ''	NONE	23
The young farmer makes money on the few hogs he sells here .	COREF_OTHER	24
He wo n't for long , because his old state sources of rye and potatoes are drying up .	COREF_PREV	25
`` There 's no feed , '' he says .	NONE	26
`` You ca n't buy anything nowadays .	COREF_OTHER	27
I do n't know why . ''	COREF_PREV	28
Edward Chojnowski does .	NONE	29
His truck is parked across the field , in a row of grain sellers .	NONE	30
Like the others , it is loaded with rye , wheat and oats in sacks labeled `` Asbestos . Made in U.S.S.R . ''	COREF_PREV	31
The farmer at the next truck shouts , `` Wheat !	NONE	32
It 's nice !	NONE	33
It wo n't be cheaper !	NONE	34
We sell direct ! ''	COREF_PREV	35
A heavy , kerchiefed woman runs a handful through her fingers , and counts him out a pile of zlotys .	NONE	36
`` Country people breed pigs , '' says Mr. Chojnowski , leaning against the back of his truck .	COREF_OTHER	37
`` They ca n't buy feed from the state .	COREF_PREV	38
There is n't enough .	COREF_OTHER	39
Some state middlemen come to buy from me .	COREF_OTHER	40
I sell -- a little .	NONE	41
I am waiting .	NONE	42
I have plenty more at home . ''	COREF_OTHER	43
On this morning , he does n't sell much in Radzymin , either .	COREF_PREV	44
At closing time , farmers cart out most of what they carted in .	COREF_PREV	45
A private market like this just is n't big enough to absorb all that business .	COREF_PREV	46
The hulk of Stalinism , it seems , will not quickly crumble away .	NONE	47
State monopolies will keep on stifling trade , `` free '' prices or not , until something else replaces them .	NONE	48
Polish agriculture will need a whole private network of procurement , processing and distribution -- plus a new manufacturing industry to supply it with tractors , pesticides , fertilizers and feed .	COREF_PREV	49
The Communists spent 40 years working to ensure that no such capitalistic structures ever arose here .	COREF_OTHER	50
Building them now will require undergirding from the West , and removal of political deadwood , a job that Solidarity has barely started .	COREF_OTHER	51
But Polish agriculture does possess one great asset already : the private farmer .	COREF_PREV	52
`` We are dealing with real entrepreneurs , '' says Antoni Leopold , an economist who advises Rural Solidarity , .	COREF_PREV	53
`` There are a lot of them , and they have property . ''	COREF_PREV	54
Polish peasants , spurning the collectivizers , were once a source of shame to orthodox Communists .	COREF_OTHER	55
Now , among Communist reformers , they are objects of envy .	NONE	56
Food is the reformer 's top priority , the key to popular support .	COREF_OTHER	57
As the Chinese have shown and the Soviets are learning , family farms thrive where collectives fail .	COREF_PREV	58
Ownership , it seems , is the best fertilizer .	NONE	59
The Poles have had it all along .	NONE	60
Poland 's 2.7 million small private farms cover 76 % of its arable land .	COREF_PREV	61
On it , a quarter of the country 's 39 million people produce three - quarters of its grain , beef , eggs and milk , and nine - tenths of its fruit , vegetables and potatoes .	COREF_PREV	62
Like the Roman Catholic Church , the Polish peasant is a pillar of the nation .	COREF_PREV	63
Try as they might , the Communists could neither replace nor break him .	COREF_PREV	64
And they did try .	COREF_PREV	65
A few miles past Radzymin , a dirt road narrows to a track of sand and leads into Zalubice , a village of tumbledown farms .	COREF_PREV	66
Czeslaw Pyszkiewicz owns 30 acres in 14 scattered scraps .	NONE	67
He grows rye and potatoes for a few hens , five cows and 25 piglets .	NONE	68
In patched pants and torn shoes , he stands in his barnyard eyeing the ground with a look both helpless and sardonic .	COREF_PREV	69
`` It 's bad soil , '' he says .	NONE	70
Until 1963 , it was good soil .	NONE	71
Then the state put in a reservoir to supply the area with drinking water .	COREF_OTHER	72
Farmers lay down before the bulldozers .	NONE	73
Their protest was ignored .	NONE	74
The dam caused the water level to drop in Zalubice .	NONE	75
Mr Pyszkiewicz smiles and his brow furrows .	NONE	76
He expected as much .	NONE	77
In his lifetime , 47 years , the Communists brought electricity to his village and piped in drinking water from the reservoir .	COREF_OTHER	78
No phones .	NONE	79
No gas .	NONE	80
`` We wanted them to build a road here , '' he says .	COREF_OTHER	81
`` They started , and then abandoned it . ''	COREF_OTHER	82
A tractor , ,his only mechanized equipment stands in front of the pigsty .	COREF_PREV	83
`` It 's Russian .	COREF_PREV	84
Good for nothing .	COREF_PREV	85
Parts are a tragedy .	NONE	86
Even if I had a lot of money , I could n't buy what I need . ''	COREF_OTHER	87
The farmer can say the same for coal , cement , saw blades .	COREF_OTHER	88
In Poland , only 4 % of all investment goes toward making things farmers want ; in the West , it is closer to 20 % .	COREF_PREV	89
The few big state farms take first crack at what does get made .	COREF_OTHER	90
They use 60 % more fertilizer per acre , twice the high - grade feed .	NONE	91
Yet their best boast is that they produce 32 % of Polish pork .	COREF_PREV	92
`` I 've heard from friends that state farms are subsidized , '' Mr. Pyszkiewicz says as , his wifeWieslawa , sets some chairs out in the sun .	COREF_PREV	93
`` We have one near here .	COREF_PREV	94
There is a lot of waste .	COREF_PREV	95
A private farmer never wastes anything . ''	NONE	96
The state quit shoving peasants onto its subsidized farms over 30 years ago .	COREF_OTHER	97
But it never did let up on the pressure .	COREF_OTHER	98
Until recently , a farmer with no heir had to will the state his land to collect his pension .	COREF_PREV	99
The pension 's size still depends on how much produce he sells the state .	COREF_PREV	100
His allotment of materials also did , until the state could n't hold up its end of that bargain .	COREF_PREV	101
Yet the state alone sells seeds and machines .	COREF_PREV	102
When supplies are short , it often hands them over only in exchange for milk or grain .	COREF_OTHER	103
A private farmer in Poland is free to buy and sell land , hire help , decide what to grow and how to grow it .	COREF_PREV	104
He is free to invest in chickens , and to fail for lack of chicken wire .	NONE	105
He has plenty of freedom -- but no choices .	COREF_OTHER	106
`` I 'm on my own land , '' Mr. Pyszkiewicz says .	NONE	107
`` I do n't have to listen to what anybody tells me to do . ''	COREF_OTHER	108
`` Sometimes , '' says his wife , `` we 're happy about that . ''	COREF_PREV	109
By starving the peasant , the Communists have starved Poland .	COREF_PREV	110
Villages like Zalubice exist in a desert of poor schools and few doctors .	COREF_OTHER	111
Farm income is 15 % below the average .	COREF_OTHER	112
The young leave , especially girls who wo n't milk cows by hand .	COREF_OTHER	113
Some men stay , their best friend a bottle of vodka , but two million acres have gone fallow .	NONE	114
Without machines , good farms ca n't get bigger .	NONE	115
So the potato crop , once 47 million tons , is down to 35 million .	NONE	116
Meat consumption is at 1979 's level , pork production at 1973 's , milk output at 1960 's .	NONE	117
If a food crisis undid the Communists , a food revolution will make Solidarity .	NONE	118
The potential is displayed along every road into Warsaw : row upon row of greenhouses , stretching out behind modern mansions that trumpet their owners ' wealth .	COREF_OTHER	119
Vegetables are abundant and full of flavor in Poland , the pickles and sauerkraut sublime , the state monopolies long broken .	NONE	120
Grain , milk and meat come next .	NONE	121
A private challenge to the monolithic tractor industry will take more time and money than Poland can spare , although a smokehouse or a local dairy can spring up fast .	COREF_OTHER	122
Poland makes no machinery for a plant on that scale .	COREF_PREV	123
Solidarity wants it from the West .	NONE	124
Maria Stolzman , one of its farm experts , lays it on the line : `` The World Bank will be brought in to help us destroy the old system . ''	COREF_OTHER	125
Felix Siemienas is destroying it now .	NONE	126
He packs pork .	NONE	127
A law went on the books in January that let him smoke bacon without breeding pigs .	COREF_OTHER	128
He cashed in .	COREF_PREV	129
Poland is short on enterprises , not enterprise .	NONE	130
`` I pay a lot to the farmer and five times the state salary to my employees , '' he says .	COREF_OTHER	131
He is in Warsaw to open a shop .	COREF_PREV	132
`` I hire transportation , and my customers have fresh cold cuts every day .	COREF_PREV	133
I do n't subsidize anyone .	COREF_PREV	134
Everyone around me lives well .	NONE	135
Yes , my prices are high .	COREF_OTHER	136
If nobody buys , I bring my prices down .	COREF_PREV	137
That 's the rule .	COREF_PREV	138
That 's the market . ''	NONE	139
Mr. Siemienas is making a fortune -- $ 10,000 a month , he says .	COREF_OTHER	140
He has bought some trendy Western clothes , and a green Mercedes with an American flag in the window .	COREF_PREV	141
But the meat - processing machines he picked up are 50 years old .	COREF_PREV	142
`` I do n't want expensive machines .	NONE	143
If the situation changes , I 'll get stuck with them . ''	NONE	144
That 's politics .	COREF_OTHER	145
By taking power in a deal with the Peasant Party 's onetime Communist stooges , Solidarity has spooked the rural entrepreneur .	COREF_PREV	146
Rural Solidarity objected , to no avail , when Solidarity leader Lech Walesa accepted the Peasants ' support .	COREF_OTHER	147
It objected again in September when Prime Minister Tadeusz Mazowiecki reluctantly named a Peasant Party man as his agriculture minister .	COREF_PREV	148
Both the Peasants and Rural Solidarity are forming new political parties for farmers .	COREF_PREV	149
The Peasants can make a credible case , against Solidarity , that hell - bent reform will drive millions from the land .	COREF_PREV	150
Next Spring , the two will battle in local elections .	COREF_PREV	151
But until then , and probably long afterward , the Communists ' apparat of obstruction -- from the head of the dairy co-op to the village bank manager -- will stay planted in the Polish countryside .	COREF_OTHER	152
`` We know how to get from capitalism to socialism , '' Sergiusz Niciporuk is saying one afternoon .	NONE	153
`` We do n't know how to get from socialism to capitalism . ''	NONE	154
He farms 12 acres in Grabowiec , two miles from the Soviet border in one of Poland 's poorest places .	NONE	155
Now he is mounting the steps of a stucco building in a nearby village , on a visit to the Communist administrator , the `` naczelnik . ''	COREF_PREV	156
`` Many people in Poland hope this government will break down , '' says Mr. Niciporuk , who belongs to the local council and to Rural Solidarity .	COREF_PREV	157
`` That 's what the naczelnik counts on .	COREF_OTHER	158
He is our most dangerous enemy .	COREF_OTHER	159
Every time he sees me , he gets very nervous . ''	NONE	160
The farmer barges into the naczelnik 's office .	COREF_OTHER	161
A thin man in a gray suit looks up from a newspaper .	COREF_PREV	162
Mr. Niciporuk sits .	COREF_PREV	163
Anatol Pawlowski 's leg begins jiggling beneath his desk .	COREF_PREV	164
`` Solidarity does n't care for the good of this region , '' he says after a few pleasantries .	COREF_PREV	165
`` They want to turn everything upside down in a week .	COREF_PREV	166
Mr. Niciporuk here wants 60 acres used at the moment by a state farm .	COREF_PREV	167
He ca n't guarantee that he can use it any better . ''	COREF_PREV	168
`` I am ready at any moment to compete with a state farm . ''	COREF_OTHER	169
The naczelnik averts his eyes .	NONE	170
`` What have you got ?	COREF_OTHER	171
Not even a tractor .	COREF_PREV	172
And you want to make wicker baskets , too . ''	COREF_PREV	173
`` I can do five things at once -- to be a businessman . ''	COREF_OTHER	174
`` Big business , '' Mr. Pawlowski snorts in English .	COREF_OTHER	175
The farmer stands to go .	NONE	176
The naczelnik stands , too .	COREF_OTHER	177
`` I care very much for this post , '' he says .	NONE	178
`` Eight years I 've had it .	COREF_OTHER	179
A cultural center has been built , shops .	COREF_OTHER	180
Suddenly , I am not a comfortable man for Solidarity .	COREF_OTHER	181
I have accomplished too much .	COREF_PREV	182
They want to do more .	COREF_PREV	183
I wish them all the best ! ''	COREF_OTHER	184
The farmer leaves .	NONE	185
And the naczelnik shuts his door .	COREF_OTHER	186
