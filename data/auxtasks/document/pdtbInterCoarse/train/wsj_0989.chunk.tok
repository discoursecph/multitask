Motorola is fighting back against junk mail .	B-expansion	9..53
So much of the stuff poured into its Austin , Texas , offices that its mail rooms there simply stopped delivering it .	I-expansion	54..169
Now , thousands of mailers , catalogs and sales pitches go straight into the trash .	O	170..251
" We just do n't have the staff to { deliver } it , nor do we have the space or the time , " says a spokesman for the Schaumburg , Ill. , electronics company , which has 5,000 employees in the Austin area .	O	254..449
" It 's the overload problem and the weight problem we have . "	O	450..509
Motorola is in good company .	O	512..540
Businesses across the country are getting fed up with junk mail , and some are saying they just are n't going to take it anymore -- literally .	O	541..681
While no one has tracked how many company mail rooms throw out junk mail , direct-mail advertising firms say the number is growing .	O	682..812
General Motors earlier this year said it would n't deliver bulk mail or free magazines in its Flint , Mich. , office , while Air Products & Chemicals , Allentown , Pa. , says it screens junk mail and often throws out most of a given mass mailing .	O	815..1054
Why the revolt ?	B-entrel	1057..1072
Anybody with a mailbox can answer that : sheer , overwhelming , mind-numbing volume .	B-expansion	1073..1154
According to the Direct Marketing Association , total direct mail -- to both businesses and consumers -- jumped 50 % to 65.4 billion pieces in 1988 from five years earlier .	I-expansion	1155..1325
Though direct mail to businesses is n't broken out separately , the association says it 's growing even faster .	O	1326..1434
The deluge has spurred cost-conscious companies to action , with mail rooms throwing the stuff out rather than taking the time or money to deliver it .	O	1437..1586
The direct-mail industry , not surprisingly , is fuming at the injustice of it all .	B-contingency	1589..1670
After all , this is the industry that has a hard enough time getting any respect , that is the butt of so many jokes that television 's " L.A. Law " portrays direct-mail-mogul David as short , bald , intensely nerdy , and unremittingly boring .	I-contingency	1671..1906
The practice of businesses throwing out junk mail " is a commonly known problem , and it 's increasing as companies attempt to put through budget cuts across the board , " right down to the mail-room level , says Stephen Belth , a list consultant and chairman of the Direct Marketing Association 's business-to-business council .	O	1909..2229
" But it 's like biting the hand that feeds them , because every one of these companies uses direct marketing . "	O	2230..2338
It 's almost impossible to track the number of companies trashing junk mail , since the decision is usually made in the mail room -- not the board room .	O	2341..2491
And the practice often varies from location to location even within a company .	O	2492..2570
But industry executives say businesses seem especially inclined to dump mailers sent to titles rather than to individual names .	O	2571..2698
Motorola 's Austin operation was one of the first to lose patience , deciding a few years ago to junk any bulk mail that was n't addressed to an individual .	O	2701..2854
Magazines are n't delivered at all , even if an individual 's name is listed ; employees who want their magazines have to pick them up from the mail room or the company library -- and are told to change the subscriptions to their home addresses .	O	2855..3096
At Air Products , meanwhile , the mailroom staff opens junk mail and often throws it away -- even if addressed to an individual .	O	3099..3225
" If they get 50 packets of something , they open one , see what it says , throw 48 away and send two to people or departments they think are appropriate , " a spokesman says .	O	3226..3395
Direct marketers were especially alarmed when General Motors -- one of the country 's largest companies and a big direct-mail user itself -- entered the junk-mail battle .	O	3398..3567
As of March 1 , its Flint office , with about 2,500 employees , stopped delivering bulk mail and non-subscription magazines .	B-expansion	3568..3689
Employees were told that if they really wanted the publications , they would have to have them sent home instead .	B-contingency	3690..3802
The reason : overload , especially of non-subscription magazines .	I-contingency	3803..3866
Direct-mail executives see GM 's stand as an ominous sign -- even if the junk-mail kings did bring it on themselves .	O	3869..3984
" Why anyone would want to close themselves off { from direct mail } , a priori , does n't make any sense , " says Michael Bronner of Bronner Slosberg Associates , a Boston directmail firm .	O	3985..4165
" It smacks of big brotherism .	B-contingency	4166..4195
They 're going to decide what their employees can or can not read . "	I-contingency	4196..4261
The practice is , however , legal in most cases .	O	4264..4310
Jack Ellis , a U.S. postal inspector in New York , says the Postal Service 's only job is to deliver the mail to the mail room ; once it gets there , a company can do with it what it wishes .	O	4311..4496
The junk-mail titans , ever optimistic , are looking for ways around the problem .	O	4499..4578
So far , they say , it has n't had any noticeable effect on response rates .	O	4579..4651
And before it does , they 're trying to cut back on the clutter that created the situation in the first place .	O	4652..4760
Among other things , the industry is trying to come up with standardized business lists that cut down on duplications .	O	4761..4878
" We 're going to have to mail a lot less and a lot smarter , " says Jack Miller , president of Quill Corp. , a Lincolnshire , Ill. , business-to-business mail-order company .	O	4879..5045
But then again , mailing less and smarter wo n't be much help if the mail ends up in the garbage anyway .	O	5046..5148
New Hyundai Campaign	O	5151..5171
Hyundai Motor America , fighting quality complaints , declining sales and management turmoil , yesterday unveiled its 1990 ad strategy , tagged " We 're Making More Sense Than Ever . "	O	5174..5350
The ad campaign , created by Saatchi & Saatchi 's Backer Spielvogel Bates agency , is an extension of the auto company 's " Cars That Make Sense " campaign , which emphasized affordability .	B-expansion	5353..5535
TV ads plugging the company 's new V-6 Sonata and its souped-up Excel subcompact will begin appearing Monday .	I-expansion	5536..5644
One spot shows a Sonata next to a rival midsized car , and an announcer says , " Listen to what they 're saying about the Hyundai Sonata . "	B-temporal	5647..5781
As the announcer reads favorable quotes about the model from Motor Trend and Road & Track magazines , the other car , which is white , slowly turns green .	B-expansion	5782..5933
" No wonder the competition 's green with envy , " the announcer says .	I-expansion	5934..6000
Ad Notes ... .	O	6003..6016
ACQUISITION :	O	6019..6031
EWDB , formed by the merger of Eurocom and Della Femina McNamee WCRS , said it agreed to buy Vizeversa , an agency in Barcelona .	O	6032..6157
Terms were n't disclosed .	O	6158..6182
HOLIDAY ADS :	O	6185..6197
Seagram will run two interactive ads in December magazines promoting its Chivas Regal and Crown Royal brands .	O	6198..6307
The Chivas ad illustrates -- via a series of pullouts -- the wild reactions from the pool man , gardener and others if not given Chivas for Christmas .	B-expansion	6308..6457
The three-page Crown Royal ad features a black-and-white shot of a boring holiday party -- and a set of colorful stickers with which readers can dress it up .	B-entrel	6458..6615
Both ads were designed by Omnicom 's DDB Needham agency .	I-entrel	6616..6671
