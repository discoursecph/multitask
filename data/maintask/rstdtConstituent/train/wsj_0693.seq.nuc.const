Although bullish dollar sentiment has fizzled ,	(NN-Topic-Change(NN-Topic-Change(NS-Evaluation(NS-Elaboration(NS-Evaluation(NS-Explanation(SN-Contrast
many currency analysts say	(SN-Attribution
a massive sell-off probably wo n't occur in the near future .	SN-Attribution))
While Wall Street 's tough times and lower U.S. interest rates continue to undermine the dollar ,	(SN-Contrast
weakness in the pound and the yen is expected to offset those factors .	SN-Contrast))
`` By default , '' the dollar probably will be able to hold up pretty well in coming days ,	(NS-Elaboration(NS-Attribution
says Francoise Soares-Kemp , a foreign-exchange adviser at Credit Suisse .	NS-Attribution)
`` We 're close to the bottom '' of the near-term ranges ,	(NS-Attribution
she contends .	NS-Attribution)))
In late Friday afternoon New York trading , the dollar was at 1.8300 marks and 141.65 yen , off from late Thursday 's 1.8400 marks and 142.10 yen .	(NS-Evaluation(NS-Background(NN-Temporal(NN-Contrast
The pound strengthened to $ 1.5795 from $ 1.5765 .	NN-Contrast)
In Tokyo Monday , the U.S. currency opened for trading at 141.70 yen , down from Friday 's Tokyo close of 142.75 yen .	NN-Temporal)
The dollar began Friday on a firm note ,	(NN-Contrast(NS-Explanation(NS-Manner-Means
gaining against all major currencies in Tokyo dealings and early European trading	(NS-Contrast
despite reports	(NS-Elaboration
that the Bank of Japan was seen unloading dollars around 142.70 yen .	NS-Elaboration)))
The rise came	(NS-Background
as traders continued to dump the pound after the sudden resignation Thursday of British Chancellor of the Exchequer Nigel Lawson .	NS-Background))
But	(NN-Same-unit(NS-Attribution(NN-Same-unit
once the pound steadied with help from purchases by the Bank of England and the Federal Reserve Bank of New York ,	(SN-Background
the dollar was dragged down ,	SN-Background))
traders say ,	NS-Attribution)
by the stock-market slump	(NS-Elaboration
that left the Dow Jones Industrial Average with a loss of 17.01 points .	NS-Elaboration))))
With the stock market wobbly and dollar buyers	(NN-Joint(SN-Attribution(NN-Same-unit(NS-Elaboration
discouraged by signs of U.S. economic weakness and the recent decline in U.S. interest rates	(NS-Elaboration
that has diminished the attractiveness of dollar-denominated investments ,	NS-Elaboration))
traders say	NN-Same-unit)
the dollar is still in a precarious position .	SN-Attribution)
`` They 'll be looking at levels	(NS-Elaboration(NS-Attribution(NS-Elaboration
to sell the dollar , ''	NS-Elaboration)
says James Scalfaro , a foreign-exchange marketing representative at Bank of Montreal .	NS-Attribution)
While some analysts say	(SN-Contrast(SN-Attribution
the dollar eventually could test support at 1.75 marks and 135 yen ,	SN-Attribution)
Mr. Scalfaro and others do n't see the currency decisively sliding under support at 1.80 marks and 140 yen soon .	SN-Contrast)))))
Predictions for limited dollar losses are based largely on the pound 's weak state after Mr. Lawson 's resignation and the yen 's inability	(NS-Elaboration(NS-Background(NS-Elaboration
to strengthen substantially	NS-Elaboration)
when there are dollar retreats .	NS-Background)
With the pound and the yen lagging behind other major currencies ,	(NS-Attribution(SN-Background
`` you do n't have a confirmation ''	(NS-Elaboration
that a sharp dollar downturn is in the works ,	NS-Elaboration))
says Mike Malpede , senior currency analyst at Refco Inc. in Chicago .	NS-Attribution)))
As far as the pound goes ,	(NN-Topic-Change(NS-Topic-Comment(NS-Elaboration(NS-Explanation(NS-Elaboration(SN-Background
some traders say	(SN-Attribution
a slide toward support at $ 1.5500 may be a favorable development for the dollar this week .	SN-Attribution))
While the pound has attempted to stabilize ,	(SN-Contrast
currency analysts say	(SN-Attribution
it is in critical condition .	SN-Attribution)))
Sterling plunged about four cents Thursday	(NS-Background(NN-Temporal
and hit the week 's low of $ 1.5765	NN-Temporal)
when Mr. Lawson resigned from his six-year post	(NN-Temporal(NS-Explanation
because of a policy squabble with other cabinet members .	NS-Explanation)
He was succeeded by John Major ,	(NS-Evaluation(NS-Elaboration
who Friday expressed a desire for a firm pound	(NN-Topic-Comment
and supported the relatively high British interest rates	(NS-Elaboration
that	(NN-Same-unit(NS-Attribution
he said	NS-Attribution)
`` are working exactly as intended ''	(NS-Enablement
in reducing inflation .	NS-Enablement)))))
But the market remains uneasy about Mr. Major 's policy strategy and the prospects for the pound ,	(NS-Attribution
currency analysts contend .	NS-Attribution)))))
Although the Bank of England 's tight monetary policy has fueled worries	(NS-Evaluation(NS-Condition(SN-Contrast(NS-Elaboration
that Britain 's slowing economy is headed for a recession ,	NS-Elaboration)
it is widely believed that Mr. Lawson 's willingness	(NN-Same-unit(NS-Elaboration
to prop up the pound with interest-rate increases	NS-Elaboration)
helped stem pound selling in recent weeks .	NN-Same-unit))
If there are any signs	(SN-Condition(NS-Elaboration
that Mr. Major will be less inclined to use interest-rate boosts	(NS-Enablement
to rescue the pound from another plunge ,	NS-Enablement))
that currency is expected to fall sharply .	SN-Condition))
`` It 's fair to say there are more risks for the pound under Major	(NN-Contrast(NS-Elaboration(NS-Attribution(NS-Comparison
than there were under Lawson , ''	NS-Comparison)
says Malcolm Roberts , a director of international bond market research at Salomon Brothers in London .	NS-Attribution)
`` There 's very little upside to sterling , ''	(SN-Contrast(NS-Attribution
Mr. Roberts says ,	NS-Attribution)
but he adds	(SN-Attribution
that near-term losses may be small	(NS-Cause
because the selling wave	(NN-Same-unit(NS-Elaboration
that followed Mr. Major 's appointment	NS-Elaboration)
apparently has run its course .	NN-Same-unit)))))
But some other analysts have a stormier forecast for the pound ,	(NS-Elaboration(NS-Explanation
particularly because Britain 's inflation is hovering at a relatively lofty annual rate of about 7.6 %	(NN-Joint
and the nation is burdened with a struggling government and large current account and trade deficits .	NN-Joint))
The pound likely will fall in coming days	(NS-Elaboration(NS-Attribution(NS-Elaboration
and may trade as low as 2.60 marks within the next year ,	NS-Elaboration)
says Nigel Rendell , an international economist at James Capel & Co. in London .	NS-Attribution)
The pound was at 2.8896 marks late Friday , off sharply from 2.9511 in New York trading a week earlier .	(NS-Evaluation
If the pound falls closer to 2.80 marks ,	(NS-Contrast(NS-Attribution(SN-Condition
the Bank of England may raise Britain 's base lending rate by one percentage point to 16 % ,	SN-Condition)
says Mr. Rendell .	NS-Attribution)
But such an increase ,	(NN-Same-unit(NS-Attribution
he says ,	NS-Attribution)
could be viewed by the market as `` too little too late . ''	NN-Same-unit))))))))
The Bank of England indicated its desire	(NS-Attribution(NS-Manner-Means(NS-Elaboration
to leave its monetary policy unchanged Friday	NS-Elaboration)
by declining to raise the official 15 % discount-borrowing rate	(NS-Elaboration
that it charges discount houses ,	NS-Elaboration))
analysts say .	NS-Attribution))
Pound concerns aside , the lack of strong buying interest in the yen is another boon for the dollar ,	(NS-Explanation(NS-Attribution
many traders say .	NS-Attribution)
The dollar has a `` natural base of support '' around 140 yen	(NS-Evaluation(NS-Attribution(NS-Cause
because the Japanese currency has n't been purchased heavily in recent weeks ,	NS-Cause)
says Ms. Soares-Kemp of Credit Suisse .	NS-Attribution)
The yen 's softness ,	(NN-Same-unit(NS-Attribution
she says ,	NS-Attribution)
apparently stems from Japanese investors ' interest	(NN-Same-unit(NS-Elaboration
in buying dollars against the yen	(NS-Enablement
to purchase U.S. bond issues	NS-Enablement))
and persistent worries about this year 's upheaval in the Japanese government .	NN-Same-unit))))))
On New York 's Commodity Exchange Friday , gold for current delivery jumped $ 5.80 , to $ 378.30 an ounce , the highest settlement since July 12 .	(NN-Temporal(NS-Evaluation
Estimated volume was a heavy seven million ounces .	NS-Evaluation)
In early trading in Hong Kong Monday , gold was quoted at $ 378.87 an ounce .	NN-Temporal))
