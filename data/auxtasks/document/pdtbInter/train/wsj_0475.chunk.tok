Eastern Airlines ' creditors committee backed off a move to come up with its own alternative proposals to the carrier 's bankruptcy reorganization plans , according to sources familiar with the committee .	O	9..210
In a meeting in New York yesterday , the committee put on hold instructions it gave two weeks ago to its experts to explore other options for Eastern 's future , the sources said .	B-entrel	213..389
The consultants had been working to finish a report this week .	I-entrel	390..452
That means Eastern , a unit of Texas Air Corp. of Houston , can go forward with its pitch for creditor approval as early as today , when it is expected to deliver a revised reorganization plan to the committee .	B-entrel	455..662
The committee intends to meet next week to make a recommendation on the new plan .	I-entrel	663..744
In another development yesterday , creditors were told that $ 40 million they had expected to become available for implementing a reorganization may not materialize , according to one source .	O	747..935
Texas Air has run into difficulty reselling about $ 20 million of debt securities because of problems in the junk bond market , the person said .	O	938..1080
And plans to raise another $ 20 million through changes to an insurance policy have hit a snag , the source said .	O	1081..1192
An Eastern spokesman said " the $ 40 million will have no effect whatsoever on the asset structure of Eastern 's plan .	O	1193..1308
Forty million in the total scheme of things is not that significant . "	O	1309..1378
It is unclear what caused the creditors to do an about-face on exploring alternatives to Eastern 's new reorganization plan .	B-contrast	1381..1504
However , since Eastern first filed for Chapter 11 protection March 9 , it has consistently promised to pay creditors 100 cents on the dollar .	B-cause	1505..1645
Because the carrier is still pledging to do that , some committee members successfully argued that there 's little reason yet to explore a different plan , according to one person familiar with the creditors ' position .	B-contrast	1646..1861
Earlier this month the accounting firm of Ernst & Young and the securities firm of Goldman , Sachs & Co. , the experts hired by the creditors , contended that Eastern would have difficulty meeting earnings targets the airline was projecting .	B-restatement	1862..2100
Ernst & Young said Eastern 's plan would miss projections by $ 100 million .	B-conjunction	2101..2174
Goldman said Eastern would miss the same mark by at least $ 120 million .	I-conjunction	2175..2246
The consultants maintained Eastern would n't generate the cash it needs and would have to issue new debt to meet its targets under the plan .	O	2249..2388
Eastern at the time disputed those assessments and called the experts ' report " completely off base . "	O	2389..2489
Yesterday , Joel Zweibel , an attorney for Eastern 's creditors committee , declined to comment on whether the experts had ever been instructed to look at other choices and whether they now were asked not to .	B-alternative	2492..2696
He said only that the committee has not yet taken any position on Eastern 's reorganization plan and that the two sides were still negotiating .	I-alternative	2697..2839
" In every case , people would like to see a consentual plan , " he said .	O	2840..2909
Eastern and its creditors agreed in July on a reorganization plan that called for the carrier to sell off $ 1.8 billion in assets and to emerge from Chapter 11 status in late 1989 at two-thirds its former size .	B-concession	2912..3121
Eastern eventually decided not to sell off a major chunk , its South American routes , which were valued at $ 400 million .	B-contrast	3122..3241
Such a change meant the reorganization plan the creditors had agreed on was no longer valid , and the two sides had to begin negotiating again .	I-contrast	3242..3384
Eastern has publicly stated it is exceeding its goals for getting back into operation and has predicted it would emerge from Chapter 11 proceedings early next year , operating more flights than it originally had scheduled .	O	3387..3608
