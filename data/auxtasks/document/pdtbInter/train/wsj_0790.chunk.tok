On the hungry streets of Naguib Mahfouz 's Cairo , life is nasty , brutish and wickedly entertaining .	O	9..107
Zaita the " cripple-maker " rearranges the limbs of aspiring beggars -- and takes a cut of every cent they cadge .	B-entrel	110..221
Hassan Kamel Ali is a card shark and dope dealer who has a simple creed : " I live in this world , assuming that there is no morality , God or police . "	B-conjunction	222..369
For the killer and thief , Said Mahran , fame flows from the barrel of a gun .	I-conjunction	370..445
" One man said you act as a stimulant , " a prostitute tells him , " a diversion to relieve people 's boredom . "	O	446..551
Mr. Mahfouz 's Cairo also has Sufi sheiks and saintly wives who look to God , not crime , for their salvation .	O	554..661
But it is his portrait of Cairo low-life -- of charlatans and opium addicts , of streets filled with " dust , vegetable litter , and animal dung " -- that made his reputation , and won him the Nobel Prize in 1988 .	O	662..869
Three novels , " The Beginning and the End " ( 412 pages , $ 19.95 ) , " The Thief and the Dogs " ( 158 pages , $ 16.95 ) , and " Wedding Song " ( 174 pages , $ 16.95 ) , recently published by Doubleday offer an uneven sample of the 77-year-old Mr. Mahfouz 's talent .	O	872..1116
But they do show the range of a restless intellect whose 30-odd novels span five decades and include work of social realism , protest and allegory .	B-conjunction	1117..1263
They also chart the evolution of a city that has grown tenfold in the author 's lifetime , from a colonial outpost of fez-wearing pashas to a Third World slum choking on its own refuse .	B-restatement	1264..1447
" Soon it 'll be so crowded , " a narrator complains , " that people will start eating each other . "	I-restatement	1448..1541
" The Beginning and the End , " easily the best of the three , belongs to Mr. Mahfouz 's " realistic " period and it is the one for which he is most renowned .	B-entrel	1544..1695
Published in 1949 , it follows the decline of a Cairo family with the saga-like sweep and rich detail that critics often compare to Dickens , Balzac and Galsworthy .	I-entrel	1696..1858
A minor bureaucrat dies suddenly , dooming his family to poverty and eventual disgrace .	O	1861..1947
His daughter turns to dressmaking , then to peddling herself for a few piasters .	O	1948..2027
One son sacrifices his own career so that his avaricious brother can succeed , while another helps support the family with money siphoned from crime .	O	2028..2176
The real tragedy , though , lies not in the family 's circumstances but in its concern for appearances .	B-instantiation	2179..2279
Mourning for the father is overshadowed by the shame of burying him in a pauper 's grave .	B-list	2280..2368
The family moves to another house at night to conceal shabby belongings from neighbors .	B-list	2369..2456
And the successful son wishes his embarrassing siblings dead .	I-list	2457..2518
As a critique of middle-class mores , the story is heavy-handed .	B-contrast	2521..2584
But its unsentimental sketches of Cairo life are vintage Mahfouz .	B-cause	2585..2650
We see , smell and hear slums filled with " the echoes of hawkers advertising their wares interspersed with abusive language , rattling coughs and the sound of people gathering spittle in their throats and spewing into the street . "	B-conjunction	2651..2879
And we meet engaging crooks , such as Hassan " the Head , " famed for his head-butting fights , his whoring and his hashish .	I-conjunction	2880..2999
" ' God has not yet ordained that I should have earnings , ' he tells his worried mother . "	B-entrel	3000..3087
Hassan comes to a bad end , but so does almost everyone else in the book .	I-entrel	3088..3160
If the setting is exotic , the prose is closer to Balzac 's " Pere Goriot " than it is to " Arabian Nights . "	O	3163..3266
Mr. Mahfouz began writing when there was no novelistic tradition in Arabic , and he modeled his work on Western classics .	O	3267..3387
In one sense , this limits him ; unlike a writer such as Gabriel Garcia Marquez , who has a distinctive Latin voice , Mr. Mahfouz 's style offers little that can be labeled " Egyptian . "	O	3388..3567
But the familiarity of his style also makes his work accessible , as the streets of Cairo come alive for the Western reader as vividly as Dickens 's London or Dostoevski 's St. Petersburg .	O	3568..3753
" The Thief and the Dogs , " written in 1961 , is a taut , psychological drama , reminiscent of " Crime and Punishment . "	O	3756..3869
Its antihero , Said Mahran , is an Egyptian Raskolnikov who seeks nobility in robbing and killing .	B-restatement	3870..3966
" I am the hope and the dream , the redemption of cowards , " he says in one of many interior monologues .	B-asynchronous	3967..4068
Later , he recalls the words of his Marxist mentor : " The people ! Theft ! The holy fire ! "	I-asynchronous	4069..4155
Said 's story reflects the souring of socialism under Nasser , whose dictatorial rule replaced the monarchy overthrown in 1952 .	B-entrel	4158..4283
By 1961 , Mr. Mahfouz 's idealism had vanished or become twisted , as it has in Said .	I-entrel	4284..4366
His giddy dream of redeeming a life of " badly aimed bullets " by punishing the " real robbers " -- the rich " dogs " who prey on the poor -- leads only to the death of innocents , and eventually to his own .	O	4367..4567
Cairo 's spirited squalor also has gone gray .	B-restatement	4570..4614
Here , the city is dark and laden with symbolism : Said has left his jail cell only to enter the larger prison of Cairo society .	B-entrel	4615..4741
While the theme is compelling , the plot and characters are not .	B-cause	4742..4805
We never care about Said or the " hypocrites " he hunts .	B-cause	4806..4860
" The Thief and the Dogs " is a pioneering work , the first stream-of-consciousness novel in Arabic , but it is likely to disappoint Western readers .	I-cause	4861..5006
The 1981 novel " Wedding Song " also is experimental , and another badly aimed bullet .	B-entrel	5009..5092
The story of a playwright 's stage debut unfolds in first-person monologues , in the manner of Faulkner 's " The Sound and the Fury . "	B-conjunction	5093..5222
But the device obscures more than it illuminates .	B-entrel	5223..5272
Buried in the work is a meditation on the morality of art , and on the struggle for integrity in an unfair world .	B-conjunction	5273..5385
But again , the themes get tangled in Mr. Mahfouz 's elliptical storytelling .	B-entrel	5386..5461
The indirectness of his later work reflects both an appetite for new genres and the hazards of art in the Arab world .	B-restatement	5462..5579
Mr. Mahfouz has been pilloried and censored for questioning Islam and advocating peace with Israel .	B-cause	5580..5679
Veiling his message has helped him endure .	I-cause	5680..5722
Art , says the playwright in " Wedding Song , " is " the surrogate for the action that an idealist like me is unable to take . "	O	5723..5844
" Wedding Song " gives glimpses of a Cairo that has become so much harsher since his youth , when , as he once said , " the poorest person was able to find his daily bread and without great difficulty . "	O	5847..6043
The clutter of the 1940s remains , but its color has drained away , and the will to overcome has been defeated .	B-list	6044..6153
Cars ca n't move because of overflowing sewers .	B-list	6154..6200
Characters complain ceaselessly about food queues , prices and corruption .	B-list	6201..6274
And the ubiquitous opium addict is now a cynical and selfish man who gripes : " Only government ministers can afford it these days ! "	B-restatement	6275..6405
Having lost their faith in God , in social reform and in opium , Cairenes are left with nothing but their sense of humor .	I-restatement	6406..6525
Mr. Horwitz is a Journal staff reporter covering the Middle East .	O	6528..6593
