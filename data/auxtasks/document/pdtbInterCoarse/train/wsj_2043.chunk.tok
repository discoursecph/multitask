The inverse trading relationship between bonds and stocks was interrupted yesterday as bonds fell despite a modest decline in stock prices .	B-comparison	9..148
But bond investors continue to keep a close watch on the jittery stock market .	B-entrel	151..229
In early trading , investors were bidding bond prices higher as stocks tumbled and fears mounted that Friday 's stock market debacle would be repeated .	I-entrel	230..379
But a partial recovery in the Dow Jones Industrial Average , which had been down more than 60 points in midmorning , dashed those expectations .	O	382..523
Treasury bonds also were hurt late in the day by a $ 4 billion offering by the Tennessee Valley Authority and the prospect of a huge amount of new agency debt .	O	524..682
" Bond investors were hoping that stock prices would continue to fall , " said Roger Early , a vice president at Federated Investors Inc. , Pittsburgh .	O	685..831
" When stocks stabilized , that was a disappointment . "	O	832..884
Meanwhile , for the second straight day , the bond market paid little attention to the Federal Reserve 's open market operations .	O	887..1013
Fed officials injected more cash into the banking system by arranging $ 1.5 billion of repurchase agreements during the usual pre-noon intervention period .	B-contingency	1014..1168
The move was meant to keep a lid on interest rates and to boost investor confidence .	I-contingency	1169..1253
" The intervention has been friendly , meaning that they really did n't have to do it , " said Maria Fiorini Ramirez , money-market economist at Drexel Burnham Lambert Inc .	O	1256..1422
She said a more aggressive move was n't needed .	B-expansion	1423..1469
The Fed also appears reluctant to ease credit conditions further .	B-entrel	1472..1537
It already has allowed the closely watched federal funds rate to decline 1/4 percentage point to about 8 3/4 % from its previous target level of about 9 % .	B-entrel	1538..1691
The rate , which banks charge each other on overnight loans , is considered an early signal of changes in Fed policy .	B-entrel	1692..1807
It ended at about 8 11/16 % yesterday , but was as low as 8 1/2 % Monday .	I-entrel	1808..1878
The Treasury 's benchmark 30-year bond fell more than 1/2 point , or over $ 5 for each $ 1,000 face amount , while the yield moved above 8 % for the first time since Thursday .	O	1881..2050
Investment-grade corporate , municipal and mortgage-backed securities also fell .	B-comparison	2051..2130
But most junk bonds closed unchanged after opening slightly higher on bargain-hunting by institutional investors .	B-comparison	2133..2246
Some so-called high-quality junk issues , such as R.H. Macy & Co. 's 14 1/2 % subordinated debentures , rose .	B-expansion	2247..2353
The Macy 's issue closed up about one point at a bid price of 97 .	I-expansion	2354..2418
The TVA 's public debt offering was its first in 15 years .	B-entrel	2421..2478
Strong investor demand prompted it to boost the size of the issue from $ 3 billion .	I-entrel	2479..2561
Traders said hedging related to the TVA pricing also pressured Treasury bonds .	O	2564..2642
" Underwriters of the TVA bonds reduced their market risk by selling Treasurys to cover at least part of their { TVA } holdings , " said James R. Capra , a senior vice president at Shearson Lehman Government Securities Inc .	O	2643..2860
The TVA bonds also " served to remind the market that there will be even more new supply , " said Lawrence N. Leuzzi , a managing director at S.G. Warburg Securities & Co .	O	2863..3030
Today the Treasury will announce the size of its next two-year note sale and Resolution Funding Corp. will announce details of its first bond offering .	B-entrel	3033..3184
Some traders estimate $ 9.75 billion of new two-year Treasurys will be sold next week , and they expect Refcorp to offer $ 4 billion to $ 6 billion of long-term " bailout " bonds .	B-entrel	3185..3358
Refcorp was created to help fund the thrift bailout .	I-entrel	3359..3411
Another agency issue came to market yesterday .	O	3414..3460
The Office of Finance of the Federal Home Loan Banks said it priced a four-part $ 2.27 billion bond offering for the banks to yield from 8.125 % to 8.375 % .	O	3461..3614
The release of several economic reports had little impact on the market , including a report that the U.S. trade deficit expanded to a surprisingly wide $ 10.77 billion in August , up from a revised $ 8.24 billion in July .	B-expansion	3617..3835
The August gap was expected to have expanded to $ 9.1 billion .	I-expansion	3836..3897
Treasury Securities	O	3900..3919
Treasury securities were essentially flat to about 1/2 point lower .	O	3922..3989
The benchmark 30-year bond was quoted late at 100 28/32 to yield 8.04 % , compared with 101 19/32 to yield 7.97 % Monday .	B-expansion	3992..4110
The latest 10-year notes were quoted late at 99 25/32 to yield 8.01 % , compared with 100 1/32 to yield 7.97 % .	I-expansion	4111..4219
Short-term rates increased .	B-expansion	4222..4249
The discount rate on three-month bills rose to 7.52 % for a bond-equivalent yield of 7.75 % .	B-expansion	4250..4340
The rate on six-month bills rose to 7.53 % for a bond-equivalent yield of 7.92 % .	I-expansion	4341..4420
Corporate , Other Issues	O	4423..4446
Investment-grade corporate bonds ended 1/4 to 1/2 point lower , while most junk bonds ended unchanged .	O	4449..4550
The TVA 's huge $ 4 billion offering dominated attention in the new-issue market .	B-expansion	4553..4632
TVA offered $ 2 billion of 30-year bonds priced to yield 9.06 % ; $ 1 billion in 10-year notes priced to yield 8.42 % ; and $ 1 billion in five-year notes priced to yield 8.33 % .	I-expansion	4633..4803
The TVA , which operates one of the nation 's largest electric power systems , is a corporation wholly owned by the U.S. government .	B-entrel	4806..4935
Yesterday 's bond sale was part of a $ 6.7 billion refinancing plan to pay off high-interest debt the TVA owes the Federal Financing Bank , an arm of the Treasury .	I-entrel	4936..5096
Meanwhile , Lockheed Corp. priced a $ 300 million note offering to yield 9.39 % .	O	5099..5176
Mortgage-Backed Securities	O	5179..5205
The derivative mortgage-backed market revived after a brief hiatus as two new Remics totaling $ 850 million were offered and talk circulated about two more issues that could be priced today .	O	5208..5397
The revival of the real estate mortgage investment conduit market reflected the relative calm in the mortgage market after two days of volatile trading .	O	5400..5552
Dealers noted that it 's difficult to structure new Remics when prices are moving widely .	O	5553..5641
The two Remics priced were a $ 500 million Federal Home Loan Mortgage Corp. issue underwritten by Salomon Brothers Inc. and a $ 350 million Federal National Mortgage Association deal underwritten by Greenwich Capital Markets .	O	5644..5867
The Remic issuance supported prices of Freddie Mac and Fannie Mae securities , which held up better than Government National Mortgage Association securities during an afternoon sell-off .	O	5870..6055
Ginnie Mae 9 % securities for November delivery ended at 97 29/32 , down 7/32 ; 9 1/2 % securities at 99 31/32 , down 6/32 ; and 10 % securities at 101 29/32 , down 5/32 .	B-temporal	6058..6220
Freddie Mac 9 % securities were at 97 5/32 , down 3/32 .	I-temporal	6221..6274
The Ginnie Mae 9 % issue was yielding 9.43 % to a 12-year average life assumption , as the spread above the Treasury 10-year note held at 1.42 percentage points .	O	6277..6435
Municipals	O	6438..6448
Confusion over the near-term trend for rates dominated the municipal arena , as gyrations in the stock market continued to buffet bonds .	O	6451..6586
Long tax-exempt dollar bonds were mostly flat to 3/8 point lower after a whipsaw session of moving inversely to stocks in modest dealer-led trading .	O	6589..6737
Prices of pre-refunded municipal bonds were capped by news that Chemical Securities Inc. , as agent for a customer , will accept bids today for two large lists of bonds that include many such issues .	O	6740..6937
The lists total $ 654.5 million .	B-entrel	6938..6969
Pre-refunded bonds are called at their earliest call date with the escrowed proceeds of another bond issue .	I-entrel	6970..7077
Meanwhile , several new issues were priced .	O	7080..7122
Underwriters led by PaineWebber Inc. set preliminary pricing for $ 144.4 million of California Health Facilities Financing Authority revenue bonds for Kaiser Permanente .	B-entrel	7125..7293
Tentative reoffering yields were set from 6.25 % in 1993 to 7.227 % in 2018 .	I-entrel	7294..7368
As part of its College Savings Plan , Connecticut offered $ 100.4 million of general obligation capital appreciation bonds priced to yield to maturity from 6.25 % in 1994 to 6.90 % in 2006 , 2007 and 2009 .	O	7371..7571
A Chemical Securities group won a $ 100 million Oregon general obligation veterans ' tax note issue due Nov. 1 , 1990 .	B-entrel	7574..7689
The 6 3/4 % notes yield 6.25 % .	I-entrel	7690..7719
Foreign Bonds	O	7722..7735
West German government bond prices took a wild roller-coaster ride , pulled down by Monday 's U.S. stock market gains then up by a wider-than-expected U.S. trade deficit and falling U.S. stock prices .	O	7738..7936
West Germany 's 7 % bond due October 1999 was at 99.95 late yesterday , off 0.10 point from Monday , to yield 7.01 % .	B-temporal	7939..8051
The 6 3/4 % notes due April 1994 were up 0.10 point to 97.85 to yield 7.31 % .	I-temporal	8052..8127
British government bonds surged on renewed volatility in the stock market .	B-expansion	8130..8204
The Treasury 11 3/4 % bond due 2003/2007 rose 23/32 to 112 10/32 to yield 10.03 % .	I-expansion	8205..8285
But Japanese bonds ended weaker .	B-expansion	8288..8320
The benchmark No. 111 4.6 % bond due 1998 ended on brokers ' screens at a price of 96 , off 0.15 point to yield 5.27 % .	I-expansion	8321..8436
