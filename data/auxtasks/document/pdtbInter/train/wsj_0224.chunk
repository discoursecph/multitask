In a surprise move, the British government cleared the way for a bidding war for Jaguar PLC by agreeing to remove an obstacle to a takeover of the auto maker.	O	9..167
Trade and Industry Secretary Nicholas Ridley told the House of Commons yesterday that he will relinquish the government's so-called golden share in the company as long as Jaguar shareholders agree.	B-entrel	170..367
The golden share restricts any individual holding to 15% and expires at the end of 1990.	B-entrel	368..456
It was in Jaguar's best interests "for the company's future to be assured and the present climate of uncertainty resolved as quickly as possible," Mr. Ridley said.	I-entrel	457..620
Mr. Ridley's decision fires the starting pistol for perhaps a costly contest between the world's auto giants for Britain's leading luxury-car maker.	B-cause	623..771
Both General Motors Corp. and Ford Motor Co. have been trying to amass 15% stakes in Jaguar.	B-instantiation	772..864
Ford, which already has an unwelcome 13.2% holding, is prepared to bid for the entire company and had lobbied the government to lift the takeover restrictions early.	I-instantiation	865..1030
GM has been negotiating a friendly transaction with Jaguar that likely would involve joint ventures and an eventual stake of just under 30%.	B-contrast	1031..1171
But the government's action, which caught Jaguar management flat-footed, may scuttle the GM minority deal by forcing it to fight for all of Jaguar.	I-contrast	1174..1321
"I can't believe they (GM) will let Ford have a free run," said Stephen Reitman, a European auto industry analyst at UBS-Phillips & Drew.	O	1322..1459
"I am sure they will be going for a full bid."	O	1460..1506
Many investors certainly believe a bidding war is imminent.	B-cause	1509..1568
Jaguar shares skyrocketed yesterday after Mr. Ridley's announcement, following their temporary suspension on London's Stock Exchange.	I-cause	1569..1702
In late trading, the shares were up a whopping 122 pence ($1.93) -- a 16.3% gain -- to a record 869 pence on very heavy volume of 9.7 million shares.	B-contrast	1703..1852
In the U.S. over-the-counter market, Jaguar shares trading as American Depositary Receipts closed at $13.625, up $1.75.	I-contrast	1853..1972
Analysts expect Ford will make the first move, perhaps today, with an initial offer of about 900 pence ($14.25) a share.	B-entrel	1975..2095
Such a proposal values Jaguar at more than #1.6 billion ($2.53 billion).	B-entrel	2096..2168
Speculation about a takeover fight has sent Jaguar shares soaring in the past six weeks.	B-restatement	2169..2257
The share price was languishing at about 400 pence before Ford's Sept. 19 announcement of its interest in a minority stake.	I-restatement	2258..2381
Ford is "in the driving seat at the moment," observed Bob Barber, an auto analyst at brokers James Capel & Co.	O	2384..2494
An aggressive Ford bid for Jaguar would put pressure on GM to make a better offer as the British company's "white knight.	B-contrast	2497..2619
Such a countermove could end Jaguar's hopes for remaining independent and British-owned.	B-contrast	2620..2708
But it isn't clear how long GM would be willing to fight Ford for Jaguar.	I-contrast	2709..2782
Because of their longstanding rivalry, GM just "wants to make sure Ford pays a huge packet for (Jaguar)," said John Lawson, an auto analyst at London's Nomura Research Institute.	O	2783..2961
People close to the GM-Jaguar talks agreed that Ford now may be able to shut out General Motors.	O	2964..3060
"It's either going to be a shootout, or there only may be one player in town," one person said.	O	3061..3156
Another person close to the talks said, "It is very hard to justify paying a silly price for Jaguar if an out-and-out bidding war were to start now."	O	3157..3306
In a statement, Jaguar's board said they "were not consulted about the (Ridley decision) in advance and were surprised at the action taken.	B-conjunction	3309..3449
The statement emphasized that holders representing 75% of the shares voting at a special shareholders' meeting must agree to lift the takeover restrictions.	I-conjunction	3450..3606
Jaguar officials in the U.S. noted that Ford, as Jaguar's largest shareholder, now has the power to call for such a meeting.	B-conjunction	3607..3731
U.S. auto analysts also noted that Ford is in the best position to benefit from the large number of Jaguar shares that have moved over the past month into the hands of arbitragers waiting for the highest takeover bid.	I-conjunction	3734..3951
Jaguar's own defenses against a hostile bid are weakened, analysts add, because fewer than 3% of its shares are owned by employees and management.	O	3952..4098
Ford officials in the U.S. declined to comment on the British government's action or on any plans to call a special Jaguar shareholders meeting.	O	4101..4245
But GM officials said they, too, were surprised by the move, which left them to "consider all our options and explore matters further.	B-entrel	4246..4381
Although GM has U.S. approval to buy up to 15% of Jaguar's stock, it hasn't yet disclosed how many shares it now owns.	I-entrel	4382..4500
In a prepared statement, GM suggested its plans for Jaguar would be more valuable in the long run than the initial windfalls investors might reap from a hostile Ford bid.	O	4503..4673
"Our intensive discussions with Jaguar, at their invitation," GM said, "have as their objectives to create a cooperative business relationship with Jaguar that would provide for the continued independence of this great British car company, to ensure a secure future for its employees and to provide an attractive long-term return for its shareholders."	O	4674..5026
Jaguar was shocked by Mr. Ridley's decision, because management had believed the government wouldn't lift the golden share without consulting the company first.	O	5029..5189
Indeed, the government is taking a calculated risk.	B-cause	5192..5243
Mr. Ridley's announcement set off a howl of protests from members of the opposition Labor Party, who accused the Thatcher administration of backing down on promised protection for a privatized company.	B-entrel	5244..5445
The British government retained the single golden share after selling its stake in Jaguar in	I-entrel	5446..5538
The Conservative government's decision may reflect its desire to shed a politically sensitive issue well before the next election, expected in late 1991.	O	5541..5694
"It's now a very good time politically to get this over and done with," observed Daniel Jones, professor of motor industry management at the University of Cardiff in Wales.	O	5695..5867
The government, already buffeted by high interest rates and a slowing economy, has been badly hurt by last week's shake-up in Mrs. Thatcher's cabinet.	O	5868..6018
At the same time, the government didn't want to appear to favor GM by allowing a minority stake that might preclude a full bid by Ford.	B-restatement	6021..6156
Mr. Ridley hinted at this motive in answering questions from members of Parliament after his announcement.	B-restatement	6157..6263
He said he was giving up the golden share "to clear the way so the playing field is level between all contestants."	I-restatement	6264..6379
Bradley A. Stertz in Detroit contributed to this article.	O	6382..6439
