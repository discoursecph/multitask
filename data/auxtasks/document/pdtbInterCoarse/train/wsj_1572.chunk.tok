FOR THOSE WHO DELIGHT in the misfortune of others , read on .	B-contingency	9..68
This is a story about suckers .	I-contingency	69..99
Most of us know a sucker .	B-expansion	102..127
Many of us are suckers .	B-comparison	128..151
But what we may not know is just what makes somebody a sucker .	B-expansion	152..214
What makes people blurt out their credit-card numbers to a caller they 've never heard of ?	I-expansion	215..304
Do they really believe that the number is just for verification and is simply a formality on the road to being a grand-prize winner ?	O	305..437
What makes a person buy an oil well from some stranger knocking on the screen door ?	O	438..521
Or an interest in a retirement community in Nevada that will knock your socks off , once it is built ?	B-expansion	522..622
Because in the end , these people always wind up asking themselves the same question : " How could I be so stupid ? "	I-expansion	625..737
There are , unfortunately , plenty of answers to that question -- and scam artists know all of them .	O	740..838
" These people are very skilled at finding out what makes a person tick , " says Kent Neal , chief of the economic-crime unit of the Broward County State Attorney 's Office in Fort Lauderdale , Fla. , a major haven for boiler rooms .	O	841..1066
" Once they size them up , then they know what buttons to push . "	O	1067..1129
John Blodgett agrees -- and he ought to know .	B-contingency	1132..1177
He used to be a boiler-room salesman , peddling investments in oil and gas wells and rare coins .	B-entrel	1178..1273
" There 's a definite psychology of the sale and different personalities you pitch different ways , " he says .	I-entrel	1274..1380
The most obvious pitch , of course , is the lure of big returns .	B-contingency	1383..1445
" We 're all a little greedy .	I-contingency	1446..1473
Everyone is vulnerable , " says Charles Harper , associate regional administrator for the Securities and Exchange Commission in Miami .	B-entrel	1474..1605
" These guys prey on human frailties . "	I-entrel	1606..1643
While the promises of big profits ought to set off warning bells , they often do n't , in part because get-rich-quick tales have become embedded in American folklore .	O	1646..1809
" The overnight success story is part of our culture , and our society puts an emphasis on it with lotteries and Ed McMahon making millionaires out of people , " says Michael Cunningham , an associate professor of psychology at the University of Kentucky in Louisville .	O	1812..2076
" Other people are making it overnight , and the rest who toil daily do n't want to miss that opportunity when it seems to come along . "	O	2077..2209
Adds Spencer Barasch , branch chief for enforcement at the SEC in Fort Worth , Texas : " Why do people play the lottery when the odds are great against them ?	O	2212..2365
People are shooting for a dream . "	O	2366..2399
Clearly , though , scam artists have to be a bit more subtle than simply promising millions ; the psychology of suckers is n't simply the psychology of the greedy .	O	2402..2561
There 's also , for instance , the need to be part of the in-crowd .	B-contingency	2562..2626
So one popular ploy is to make a prospective investor feel like an insider , joining an exclusive group that is about to make a killing .	B-expansion	2627..2762
Between 1978 and 1987 , for instance , SH Oil in Winter Haven , Fla. , sold interests in oil wells to a very select group of local residents , while turning away numerous other eager investors .	I-expansion	2765..2953
The owner of the company , Stephen Smith , who has since pleaded guilty to state and federal fraud charges , confided to investors that he had a secret agreement with Amoco Oil Co. and said the location of his wells was confidential , according to a civil suit filed in a Florida state court by the Florida comptroller 's office .	O	2954..3278
Neither the Amoco agreement nor the wells existed , the suit alleged .	O	3279..3347
Such schemes , says Tony Adamski , chief of the financial-crimes unit of the Federal Bureau of Investigation in Washington , D.C. , appeal to investors ' " desire to believe this is really true and that they are part of a chosen group being given this opportunity . "	O	3350..3609
At times , salesmen may embellish the inside information with " the notion that this is some slightly shady , slightly illegal investment the person is being included in , " says Mr. Cunningham .	O	3612..3801
In appealing to those with a bit of larceny in their hearts , the fraud artist can insist that a person keep an investment secret -- insulating himself from being discovered and keeping his victim from consulting with others .	O	3802..4026
It also adds to the mystery of the venture .	O	4029..4072
Mr. Blodgett , the boiler-room veteran , believes that for many investors , the get-rich-quick scams carry a longed-for element of excitement .	O	4073..4212
" Once people got into it , I was allowing them to live a dream , " he says .	O	4213..4285
He phoned them with updates on the investment , such as " funny things that happened at the well that week , " he says .	O	4286..4401
" You gave them some excitement that they did n't have in their lives . "	B-entrel	4402..4471
( Mr. Blodgett , who was convicted in Florida state court of selling unregistered securities and in California state court of unlawful use of the telephone to defraud and deceive , is now on probation .	B-entrel	4472..4670
He says he has quit the business and is back in school , majoring in psychology with aspirations to go into industrial psychology . )	I-entrel	4671..4801
For some investors , it 's the appearances that leave them deceived .	O	4804..4870
" The trappings of success go a long way -- wearing the right clothes , doing the right things , " says Paul Andreassen , an associate professor of psychology at Harvard .	O	4871..5036
Conservative appearances make people think it 's a conservative investment .	O	5037..5111
" People honestly lose money on risky investments that they did n't realize were a crapshoot , " he says .	O	5112..5213
Paul Wenz , a Phoenix , Ariz. , attorney , says a promise of unrealistic returns would have made him leery .	O	5216..5319
But Mr. Wenz , who says he lost $ 43,000 in one precious-metals deal and $ 39,000 in another , says a salesman " used a business-venture approach " with him , sending investment literature , a contract limiting the firm 's liability , and an insurance policy .	O	5320..5569
When he visited the company 's office , he says , it had " all the trappings of legitimacy . "	O	5570..5658
Still others are stung by a desire to do both well and good , says Douglas Watson , commanding officer of the Los Angeles Police Department 's bunko-forgery division .	O	5661..5824
Born-again Christians are the most visible targets of unscrupulous do-gooder investment pitches .	O	5825..5921
But hardly the only ones : The scams promise -- among other things -- to help save the environment , feed starving families and prevent the disappearance of children .	O	5922..6086
Psychologists say isolated people who do n't discuss their investments with others are particularly at risk for fraud .	B-contingency	6089..6206
Scam artists seek out such people -- or try to make sure that their victims isolate themselves .	I-contingency	6207..6302
For instance , salesmen may counter a man 's objection that he wants to discuss an investment with his wife by asking , " Who wears the pants in your family ? "	B-expansion	6303..6457
Or an investor who wants his accountant 's advice may be told , " You seem like a guy who can make up his own mind . "	I-expansion	6458..6571
Often con artists will try to disarm their victims by emphasizing similarities between them .	O	6574..6666
William Lynes , a retired engineer from Lockheed Corp. , says he and his wife , Lily , warmed to the investment pitches of a penny-stock peddler from Stuart-James Co. in Atlanta after the broker told them he , too , had once worked with Lockheed .	O	6667..6907
The Lyneses , of Powder Springs , Ga. , have filed suit in Georgia state court against Stuart James , alleging fraud .	B-entrel	6910..7023
They are awaiting an arbitration proceeding .	B-entrel	7024..7068
They say the broker took them out for lunch frequently .	I-entrel	7069..7124
He urged them to refer their friends , who also lost money .	B-comparison	7125..7183
( Donald Trinen , an attorney for the penny-brokerage firm , denies the fraud allegations and says the Lyneses were fully apprised that they were pursuing a high-risk investment . )	I-comparison	7184..7360
" It 's not uncommon for these guys to send pictures of themselves or their families to ingratiate themselves to their clients , " says Terree Bowers , chief of the major-frauds section of the U.S. attorney 's office in Los Angeles .	O	7363..7589
" We 've seen cases where salesmen will affect the accent of the region of the country they are calling .	B-expansion	7590..7692
Anything to make a sale . "	I-expansion	7693..7718
Experts say that whatever a person 's particular weak point , timing is crucial .	O	7721..7799
People may be particularly vulnerable to flim-flam pitches when they are in the midst of a major upheaval in their lives .	O	7800..7921
" Sometimes when people are making big changes , retiring from their jobs , moving to a new area , they lose their bearings , " says Maury Elvekrog , a licensed psychologist who is now an investment adviser and principal in Seger-Elvekrog Inc. , a Birmingham , Mich. , investment-counseling firm .	O	7922..8208
" They may be susceptible to some song and dance if it hits them at the right time . "	O	8209..8292
They are obviously also more susceptible when they need money - retirees , for instance , trying to bolster their fixed income or parents fretting over how to pay for a child 's college expenses .	O	8295..8485
" These people are n't necessarily stupid or naive .	O	8488..8537
Almost all of us in comparable circumstances might be victimized in some way , " says Jerald Jellison , a psychology professor at the University of Southern California in Los Angeles .	O	8538..8718
Nick Cortese thinks that 's what happened to him .	O	8721..8769
Mr. Cortese , a 33-year-old Delta Air Lines engineer , invested some $ 2,000 in penny stocks through a broker who promised quick returns .	O	8770..8904
" We were saving up to buy a house , and my wife was pregnant , " says Mr. Cortese .	O	8905..8984
" It was just before the Christmas holidays , and I figured we could use some extra cash . "	B-expansion	8985..9073
The investment is worth about $ 130 today .	I-expansion	9074..9115
" Maybe it was just a vulnerable time , " says Mr. Cortese .	O	9118..9174
" Maybe the next day or even an hour later , I would n't have done it . "	O	9175..9243
Ms. Brannigan is a staff reporter in The Wall Street Journal 's Atlanta bureau .	O	9246..9324
