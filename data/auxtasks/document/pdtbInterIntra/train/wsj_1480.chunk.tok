Growth is good .	root	9..24
At least , that 's a theme emerging among many money managers who are anxious both to preserve the handsome stock-market gains they have already achieved this year and to catch the next wave of above-average performers .	norel	27..244
They are starting to buy growth stocks .	cause	245..284
Remember them ?	norel	287..301
The upper echelon of this group were shares of the " nifty 50 " companies whose profits of the 1960s and early 1970s grew steadily , if not spectacularly , through thick and thin .	norel	302..477
That sort of workhorse performance sounds made to order for a time when corporate profits overall have been weakening from the brisk increases of recent years .	entrel	478..637
The current flood of third-quarter reports are producing many more negative surprises than positive ones .	instantiation	638..743
Those are unwelcome trends in a year that the Dow Jones Industrial Average has risen 23 % so far , even with the 190.58-point plunge on Oct. 13	norel	746..887
broader market measures are in the same neighborhood .	entrel	888..942
The question for investors is , how to protect these returns and yet reach a little for additional gains .	entrel	943..1047
That 's the path of reasoning leading to growth stocks .	cause	1048..1102
" I think it is a good theme for what looks to be an uncertain market , " says Steven Einhorn , partner at Goldman Sachs .	norel	1105..1222
Growth stocks may be as big as Philip Morris or medium-sized such as Circuit City Stores , but their common characteristic is a history of increasing profits on the order of at least 15 % to 20 % a year , money managers say .	norel	1225..1445
" The period when growth stocks should be performing well is when their earnings are growing at a superior rate to the general level of corporate profits , " says Stephen Boesel , president of T. Rowe Price 's Growth and Income Fund .	norel	1448..1676
Growth stocks also are attractive in periods of market volatility , which many investors and analysts expect in the weeks ahead as everybody tries to discern where the economy is heading .	conjunction	1679..1865
This kind of jumpy uncertainty reminds John Calverley , senior economist for American Express Bank , of the 1969-72 period , when the industrial average rolled through huge ranges and investors flocked to the shares of companies with proven earnings records , which became known as the " nifty 50 . "	conjunction	1866..2159
And they will again , say money-manager proponents of the growth-stock theme .	conjunction	2162..2238
Cabanne Smith , president of a money management company bearing his name , predicts that investment companies using computers to identify companies with earnings " momentum " will climb on the growth-stock bandwagon as the overall corporate earnings outlook deteriorates further .	instantiation	2239..2514
He also thinks foreign investors , who are showing signs of more discriminate investing , will join the pursuit and pump up prices .	conjunction	2515..2644
" We 're just seeing the beginning of a shift , " Mr. Smith says .	norel	2647..2708
Mr. Smith recommends Cypress Semiconductor that is currently showing a robust 63 % earnings growth rate .	entrel	2709..2812
Ronald Sloan , executive vice president of Siebel Capital Management , likes Wellman Inc. , a company that recycles plastic into synthetic fibers for carpeting .	norel	2815..2972
Mr. Sloan praises the company as recession resistant and notes that it has an annual earnings growth rate of 32 % a year over the past five years .	restatement	2973..3118
Wellman stock closed Friday at 39 3/8 , up 1/8	entrel	3119..3164
Mr. Sloan thinks that in a year it could hit 60 .	conjunction	3165..3214
Others preach the gospel of buying only blue-chip growth stocks .	norel	3217..3281
Carmine Grigoli , chief market strategist for First Boston , who still says , " We expect the Dow average { to be at } 3000 by mid-1990 , " nonetheless foresees a sluggish economy in the meantime .	entrel	3282..3470
He recommends such blue-chip growth stalwarts as Philip Morris , PepsiCo , CPC International , Reebok International , and Limited Inc .	cause	3471..3601
All have a fiveyear earnings growth rate of more than 20 % a year .	cause	3602..3667
Some money managers are pursuing growth stocks at the expense of those that rise and fall along with the economic cycle .	norel	3670..3790
" One of the stories of the fourth quarter is that we will get an unusual number of earnings disappointments from companies sensitive to the economy , " says Mr. Boesel of T. Rowe Price .	cause	3791..3974
James Wright , chief investment officer for Banc One Asset Management , says , " We 've been selling a disproportionate share of cyclical companies and buying a disproportionate share of high earnings stocks . "	norel	3977..4181
He recently trimmed his portfolio of International Paper , Dow Chemical , Quantum Chemical , International Business Machines and Digital Equipment .	restatement	4182..4326
He is putting money in Dress Barn , Circuit City Stores , Bruno 's , and Rubbermaid .	alternative	4327..4407
Big cyclical companies are using " all the tricks they can to stabilize earnings , " says Mr. Sloan .	norel	4410..4507
He cites IBM , which reported a 30 % earnings decline in the third quarter , and which last week announced a $ 1 billion buy-back of its shares .	instantiation	4508..4648
" What they are telling you is that they do n't have the ability to generate higher returns internally , " says Mr. Sloan .	norel	4651..4769
" When they are buying back stock at 10 times earnings , they are suggesting that the rate of return on competing internal projects is below " returns on the stock .	restatement	4770..4931
IBM says it considers its shares a good investment .	comparison	4932..4983
But not all strategists or money managers are ready to throw in the towel completely on cyclicals .	norel	4986..5084
Growth stocks may underperform cyclical stocks next year	cause	5085..5141
if the Federal Reserve begins to let interest rates drift sufficiently lower to boost the economy .	condition	5142..5240
Goldman Sachs 's Mr. Einhorn , for one , subscribes to that scenario .	norel	5241..5307
He suggests investors think about buying cyclical shares in the weeks ahead , as well as growth issues .	cause	5308..5410
Friday 's Market Activity	norel	5413..5437
Stock prices finished about unchanged Friday in quiet expiration trading .	norel	5440..5513
Traders anticipated a volatile session due to the October expiration of stock-index futures and options , and options on individual stocks .	norel	5516..5654
But there were fewer price swings than expected .	concession	5655..5703
Buy order imbalances on several big stocks were posted by the New York Stock Exchange .	comparison	5704..5790
But block trading desks and money managers made a concerted effort to meet the imbalances with stock to sell , one trader said .	comparison	5791..5917
As a result , the Dow Jones Industrial Average drifted in narrow ranges in the final hour of trading , and closed 5.94 higher to 2689.14 .	cause	5920..6055
New York Stock Exchange volume was 164,830,000 .	list	6056..6103
Advancers on the Big Board lagged decliners 662 to 829 .	list	6104..6159
For the week , the industrial average gained 119.88 points , or 4.7 % , the biggest weekly point advance ever and a better than 50 % rebound from the 190.58 point loss the industrial average logged Oct. 13 .	norel	6162..6363
Broader market averages were little changed in the latest session .	norel	6366..6432
Standard & Poor 's 500-Stock Index gained 0.03 to 347.16 , the Dow Jones Equity Market Index fell 0.02 to 325.50	restatement	6433..6543
and the New York Stock Exchange Composite Index fell 0.05 to 192.12 .	conjunction	6544..6613
Most of last week 's surge in the industrial average came on Monday , when the average rose 88.12 points as market players snapped up blue-chip issues and shunned the broad market .	norel	6616..6794
That contrast was reflected in the smaller weekly percentage gains recorded by the broader averages .	norel	6797..6897
The S&P 500 rose 4 % , the Dow Jones Equity Market index gained 3.7 %	restatement	6898..6964
and the New York Stock Exchange composite index added 3.5 % .	list	6965..7024
The Dow Jones Transportation Average fell 32.71 to 1230.80 amid renewed weakness in the airline sector .	norel	7027..7130
UAL skidded 21 5/8 to 168 1/2 on 2.2 million shares .	norel	7133..7185
On the week , UAL was down nearly 40 % .	restatement	7186..7223
The latest drop followed a decision by British Airways , which had supported the $ 300-a-share buy-out offer for UAL from a labor-management group , not to participate in any revised bid .	cause	7224..7408
British Airways fell 1 to 31 7/8 .	entrel	7409..7442
While most other airline issues took their cue from UAL , USAir Group rose 1 3/4 to 43 1/4 on 1.5 million shares amid speculation about a possible takeover proposal from investor Marvin Davis .	norel	7445..7636
USA Today reported that Mr. Davis , who had pursued UAL before dropping his bid Wednesday , has acquired a stake of about 3 % in USAir .	entrel	7637..7769
Unocal fell 1 1/2 to 52 1/4	norel	7770..7797
and Burlington Resources declined 7/8 to 45 5/8 .	conjunction	7798..7846
At a meeting with analysts , British Petroleum officials dispelled speculation that the company may take over a U.S. oil company , according to Dow Jones Professional Investor Report .	norel	7847..8028
Both Unocal and Burlington had been seen as potential targets for a British Petroleum bid .	entrel	8029..8119
Paper and forest-products stocks declined	norel	8122..8163
after Smith Barney , Harris Upham & Co. lowered investment ratings on a number of issues in the two sectors , based on a forecast that pulp prices will fall sharply .	asynchronous	8164..8327
International Paper dropped 5/8 to 51 , Georgia-Pacific fell 1 3/4 to 56 1/4 , Stone Container tumbled 1 1/2 to 26 5/8 , Great Northern Nekoosa went down 5/8 to 38 3/8	restatement	8328..8492
and Weyerhaeuser lost 7/8 to 28 1/8 .	contrast	8493..8529
Dun & Bradstreet dropped 3/4 to 51 1/8 on 1.9 million shares on uncertainty about the company 's earnings prospects .	norel	8532..8647
Merrill Lynch cut its rating and 1990 earnings estimate Thursday , citing weakness in its credit-rating business .	conjunction	8648..8760
Lamson & Sessions , which posted sharply lower third-quarter earnings and forecast that results for the fourth quarter might be " near break-even , " fell 1/2 to 9 1/4 .	norel	8763..8927
Winnebago Industries slid 5/8 to 5 1/4 .	norel	8930..8969
The company , which reported that its loss for the fiscal quarter ended Aug. 26 widened from a year earlier , cut its semiannual dividend in half in response to the earnings weakness .	asynchronous	8970..9151
MassMutual Corporate Investors fell 3 to 29	norel	9154..9197
after declaring a quarterly dividend of 70 cents a share , down from 95 cents a share .	asynchronous	9198..9283
