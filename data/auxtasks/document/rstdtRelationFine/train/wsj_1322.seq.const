The bond market ,	(summary(Same-Unit(elaboration-additional
which sometimes thrives on bad news ,	elaboration-additional)
cheered yesterday 's stock market sell-off and perceptions	(elaboration-object-attribute
that the economy is growing weaker .	elaboration-object-attribute))
Early in the day , bonds rose modestly on economists ' forecasts	(Topic-Drift(Sequence(example(evaluation(elaboration-object-attribute
that this week 's slate of economic data will portray an economy headed for trouble .	elaboration-object-attribute)
Such news is good for bonds	(interpretation
because economic weakness sometimes causes the Federal Reserve to lower interest rates in an effort	(elaboration-object-attribute
to stimulate the economy	(List
and stave off a recession .	List))))
For example , today the Department of Commerce is scheduled to release the September durable goods report .	(elaboration-additional
The consensus forecast of 14 economists	(circumstance(Same-Unit(elaboration-additional
surveyed by Dow Jones Capital Markets Report	elaboration-additional)
is for a 1.2 % drop in September orders .	Same-Unit)
That would follow a 3.9 % advance in August .	circumstance)))
Bonds received a bigger boost later in the day	(elaboration-additional(elaboration-general-specific(circumstance
when stock prices moved broadly lower .	circumstance)
The Dow Jones Industrial Average fell 26.23 points to 2662.91 .	elaboration-general-specific)
`` Bond investors have been watching stocks closely , ''	(List(attribution
said Joel Marver , chief fixed-income analyst at Technical Data Global Markets Group .	attribution)
`` When you get a big sell-off in equities ,	(attribution(circumstance
money starts to shift into bonds , ''	(elaboration-additional
which are considered safer ,	elaboration-additional))
he said .	attribution))))
The Treasury 's benchmark 30-year bond ended about 1/2 point higher , or up about $ 5 for each $ 1,000 face amount ,	(Topic-Drift(List(Contrast
while the yield slid to 7.93 % from 7.98 % Friday .	Contrast)
Municipals ended mixed ,	(List(List
while mortgage-backed and investment-grade corporate bonds rose .	List)
Prices of high-yield , high-risk corporate securities ended unchanged .	(List
In more evidence of the growing division between `` good '' and `` bad '' junk bonds , a $ 150 million issue by Imo Industries Inc. was snapped up by investors	(List(Contrast
while underwriters for Beatrice Co. 's $ 350 million issue are considering restructuring the deal	(purpose
to attract buyers .	purpose))
In the Treasury market , analysts expect bond prices to trade in narrow ranges this week	(List(evaluation(circumstance
as the market takes in positive and negative news .	circumstance)
`` On the negative side , the market will be affected by constant supply in all sectors of the market , ''	(Contrast(attribution
said William M. Brachfeld , economist at Daiwa Securities America Inc .	attribution)
`` On the other hand , we have economic news	(List(elaboration-object-attribute
that is { expected to be } relatively positive for the bond market .	elaboration-object-attribute)
We will go back and forth with a tilt toward slightly lower yields , ''	(attribution
he said .	attribution))))
Today , the Treasury will sell $ 10 billion of new two-year notes .	(List
Tomorrow , Resolution Funding Corp. , a division of a new government agency	(List(evaluation(elaboration-additional(Same-Unit(elaboration-object-attribute
created	(purpose
to bail out the nation 's troubled thrifts ,	purpose))
will hold its first bond auction	(elaboration-object-attribute
at which it will sell $ 4.5 billion of 30-year bonds .	elaboration-object-attribute))
So far , money managers and other bond buyers have n't shown much interest in the Refcorp bonds .	elaboration-additional)
Analysts have mixed views about the two-year note auction .	(elaboration-additional
While some say	(Contrast(attribution
the auction should proceed smoothly ,	attribution)
others contend	(attribution
that yesterday 's sale of $ 2.58 billion of asset-backed securities by Ford Motor Credit Corp. may have siphoned some potential institutional buyers from the government 's note sale .	attribution))))
The division of auto maker Ford Motor Co. made its debut in the asset-backed securities market with the second-largest issue in the market 's four-year history .	(comparison(elaboration-additional(elaboration-additional
The company offered securities	(elaboration-additional(elaboration-object-attribute
backed by automobile loans through an underwriting group	(elaboration-additional
headed by First Boston Corp .	elaboration-additional))
The issue yields 8.90 %	(elaboration-additional(List
and carries a guarantee	(elaboration-additional
covering 9 % of the deal from the company .	elaboration-additional))
First Boston sweetened the terms from the original yield estimate in an apparent effort	(elaboration-object-attribute
to place the huge offering .	elaboration-object-attribute))))
The issue was offered at a yield nearly one percentage point above the yield on two-year Treasurys .	elaboration-additional)
The only asset-backed deal larger than Ford 's was a $ 4 billion offering by General Motors Acceptance Corp. in 1986 .	comparison))))))))
Treasury Securities	(TextualOrganization(TextualOrganization
Treasury bonds were 1/8 to 1/2 point higher yesterday in light trading .	(elaboration-additional
The benchmark 30-year bond ended at a price of 102 3/32 ,	(elaboration-additional(elaboration-additional(List(Comparison
compared with 101 17/32 Friday .	Comparison)
The latest 10-year notes were quoted late at 100 17/32	(List(List(Comparison(consequence
to yield 7.90 % ,	consequence)
compared with 100 3/32	(consequence
to yield 7.97 % .	consequence))
The latest two-year notes were quoted late at 100 28/32	(consequence
to yield 7.84 % .	consequence))
Short-term rates rose yesterday at the government 's weekly Treasury bill auction ,	(List(Comparison
compared with the previous bill sale .	Comparison)
The Treasury sold $ 7.81 billion of three-month bills with an average discount rate of 7.52 % , the highest since the average of 7.63 % at the auction on Oct. 10 .	(List
The $ 7.81 billion of six-month Treasury bills were sold with an average discount rate of 7.50 % , the highest since the average of 7.60 % at the Oct. 10 auction .	List))))
The rates were up from last week 's auction ,	(Comparison
when they were 7.37 % and 7.42 % , respectively .	Comparison))
Here are auction details :	(example
Rates are determined by the difference between the purchase price and face value .	(List(interpretation
Thus , higher bidding narrows the investor 's return	(Contrast
while lower bidding widens it .	Contrast))
The percentage rates are calculated on a 360-day year ,	(List(Contrast
while the coupon-equivalent yield is based on a 365-day year .	Contrast)
Both issues are dated Oct. 26 .	(elaboration-additional
The 13-week bills mature Jan. 25 , 1990 ,	(Comparison
and the 26-week bills mature April 26 , 1990 .	Comparison))))))))
Corporate Issues	(TextualOrganization(TextualOrganization
Investment-grade corporates closed about 1/4 point higher in quiet trading .	(List
In the junk bond market , Imo Industries ' issue of 12-year debentures ,	(elaboration-additional(elaboration-additional(elaboration-additional(Same-Unit(elaboration-additional
considered to be one of the market 's high-quality credits ,	elaboration-additional)
was priced at par to yield 12 % .	Same-Unit)
Peter Karches , managing director at underwriter Morgan Stanley & Co. , said	(explanation-argumentative(attribution
the issue was oversubscribed .	attribution)
`` It 's a segmented market ,	(attribution(circumstance
and	(Same-Unit
if you have a good , strong credit ,	(condition
people have an appetite for it , ''	condition)))
he said .	attribution)))
Morgan Stanley is expected to price another junk bond deal , $ 350 million of senior subordinated debentures by Continental Cablevision Inc. , next Tuesday .	elaboration-additional)
In light of the recent skittishness in the high-yield market , junk bond analysts and traders expect other high-yield deals to be sweetened or restructured	(circumstance(example(condition
before they are offered to investors .	condition)
In the case of Beatrice , Salomon Brothers Inc. is considering restructuring the reset mechanism on the $ 200 million portion of the offering .	example)
Under the originally contemplated terms of the offering , the notes would have been reset annually at a fixed spread above Treasurys .	(Comparison
Under the new plan	(elaboration-additional(Consequence(Same-Unit(elaboration-object-attribute
being considered ,	elaboration-object-attribute)
the notes would reset annually at a rate	Same-Unit)
to maintain a market value of 101 .	Consequence)
Price talk calls for the reset notes to be priced at a yield of between 13 1/4 % and 13 1/2 % .	elaboration-additional))))))
Mortgage-Backed Securities	(TextualOrganization(TextualOrganization
Activity in derivative markets was strong with four new real estate mortgage investment conduits announced and talk of several more deals in today 's session .	(elaboration-additional(example
The Federal National Mortgage Association offered $ 1.2 billion of Remic securities in three issues ,	(explanation-argumentative(List
and the Federal Home Loan Mortgage Corp. offered a $ 250 million Remic	(elaboration-object-attribute
backed by 9 % 15-year securities .	elaboration-object-attribute))
Part of the reason for the heavy activity in derivative markets is that underwriters are repackaging mortgage securities	(List(elaboration-object-attribute
being sold by thrifts .	elaboration-object-attribute)
Traders said	(circumstance(attribution
thrifts have stepped up their mortgage securities sales	attribution)
as the bond market has risen in the past two weeks .	circumstance))))
In the mortgage pass-through sector , active issues rose	(List(Comparison
but trailed gains in the Treasury market .	Comparison)
Government National Mortgage Association 9 % securities for November delivery were quoted late yesterday at 98 10/32 , up 10/32 ;	(elaboration-additional(List
and Freddie Mac 9 % securities were at 97 1/2 , up 1/4 .	List)
The Ginnie Mae 9 % issue was yielding 8.36 % to a 12-year average life assumption ,	(List
as the spread above the Treasury 10-year note widened slightly to 1.46 percentage points .	List)))))
Municipals	(TextualOrganization(Topic-Drift
A $ 575 million San Antonio , Texas , electric and gas system revenue bond issue dominated the new issue sector .	(Topic-Drift(elaboration-additional
The refunding issue ,	(concession(Same-Unit(elaboration-additional
which had been in the wings for two months ,	elaboration-additional)
was one of the chief offerings	(elaboration-object-attribute
overhanging the market	(List
and limiting price appreciation .	List)))
But alleviating that overhang failed to stimulate much activity in the secondary market ,	(explanation-argumentative(elaboration-additional
where prices were off 1/8 to up 3/8 point .	elaboration-additional)
An official with lead underwriter First Boston said	(elaboration-additional(attribution
orders for the San Antonio bonds were `` on the slow side . ''	attribution)
He attributed that to the issue 's aggressive pricing and large size , as well as the general lethargy in the municipal marketplace .	(List
In addition ,	(Same-Unit(attribution
he noted ,	attribution)
the issue would normally be the type	(concession(elaboration-object-attribute
purchased by property and casualty insurers ,	elaboration-object-attribute)
but recent disasters , such as Hurricane Hugo and the Northern California earthquake , have stretched insurers ' resources	(List
and damped their demand for bonds .	List))))))))
A $ 137.6 million Maryland Stadium Authority sports facilities lease revenue bond issue appeared to be off to a good start .	(Topic-Drift(elaboration-additional
The issue was oversubscribed	(attribution(List
and `` doing very well , ''	List)
according to an official with lead underwriter Morgan Stanley .	attribution))
Activity quieted in the New York City bond market ,	(circumstance
where heavy investor selling last week drove yields on the issuer 's full faith and credit backed bonds up as much as 0.50 percentage point .	circumstance))))
Foreign Bonds	(TextualOrganization
Japanese government bonds ended lower	(Topic-Drift(elaboration-additional(circumstance
after the dollar rose modestly against the yen .	(explanation-argumentative
The turnaround in the dollar fueled bearish sentiment about Japan 's bond market .	explanation-argumentative))
The benchmark No. 111 4.6 % bond due 1998 ended on brokers ' screens at a price of 95.39 , off 0.28 .	(elaboration-additional
The yield rose to 5.38 % .	elaboration-additional))
West German bond prices ended lower after a day of aimless trading .	(Topic-Drift(elaboration-additional
The benchmark 7 % bond due October 1999 fell 0.20 point to 99.80	(List(consequence
to yield 7.03 % ,	consequence)
while the 6 3/4 % notes due July 1994 fell 0.10 to 97.65	(consequence
to yield 7.34 % .	consequence)))
British government bonds ended slightly higher in quiet trading	(elaboration-additional(Temporal-Same-Time
as investors looked ahead to today 's British trade report .	Temporal-Same-Time)
The benchmark 11 3/4 % Treasury bond due 2003/2007 rose 1/8 to 111 21/32	(List(consequence
to yield 10.11 % ,	consequence)
while the 12 % issue of 1995 rose 3/32 to 103 23/32	(consequence
to yield 11.01 % .	consequence)))))))))))))
