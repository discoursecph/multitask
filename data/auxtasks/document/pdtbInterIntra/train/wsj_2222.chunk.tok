What happened Friday shows that financial markets are not yet sufficiently coordinated to handle another meltdown in prices .	root	9..133
No fiddling with systems and procedures will ever prevent markets from suffering a panic wave of selling .	entrel	134..239
But markets can operate with greater or lesser efficiency .	contrast	240..298
After the 1987 plunge , markets agreed that it would be wise to halt trading whenever panic conditions arose .	norel	301..409
The New York Stock Exchange adopted two specific circuit breakers : If the Dow Jones index falls 250 points in a day , the exchange will halt trading for one hour ; if the decline hits 400 points , the exchange will close for an additional two hours .	instantiation	410..656
The rationale is that an interruption of trading will allow investors to reconsider their strategies , calm sellers and lead buyers to enter the market at indicated new price levels .	cause	657..838
It is impossible to know whether that theory is realistic .	norel	841..899
A temporary cessation of trading may indeed discourage a selling panic from feeding on itself .	conjunction	900..994
But there is also the possibility that shutting down markets will intensify fears and cause an even more abrupt slide in prices .	contrast	995..1123
What happened Friday was the worst of all worlds .	norel	1126..1175
The futures exchanges followed their own pre-set circuit breakers and shut down at about 3 p.m. for 30 minutes , after the Standard & Poor 's 500 stock index had fallen 12 points , or about 100 points on the Dow Jones index .	restatement	1176..1397
Options markets stopped trading in many securities .	conjunction	1398..1449
The New York Stock Exchange , under its own rules , remained open .	contrast	1450..1514
With nowhere else to go , sellers , and particularly program traders , focused all their selling on the New York Stock Exchange .	norel	1517..1642
As liquidity on that market weakened , prices fell sharply .	asynchronous	1643..1701
Had the futures and options markets been open , additional liquidity would have been provided and the decline , most probably , would have been less intense .	norel	1704..1858
At 3:30 , after intense telephone negotiations between the trading markets and Washington , the futures exchanges reopened .	entrel	1859..1980
Futures trading , however , was halted altogether at 3:45 , after the futures markets had dropped an additional 30 points , which is the daily limit for price declines .	contrast	1981..2145
At this point , the options markets also shut down and once more left all sales to be handled by the New York Stock Exchange .	synchrony	2146..2270
It is time to recognize that the New York Stock Exchange , the futures markets and the options markets , though physically separate , have actually become so closely intertwined as to constitute one market effectively .	norel	2273..2488
Traders can vary their strategies and execute their orders in any one of them .	cause	2489..2567
It therefore makes no sense for each market to adopt different circuit breakers .	cause	2568..2648
To achieve maximum liquidity and minimize price volatility , either all markets should be open to trading or none .	contrast	2649..2762
Synchronized circuit breakers would not have halted the slide in prices on Friday	norel	2765..2846
but they probably would have made for smoother , less volatile executions .	contrast	2847..2921
It 's time for the exchanges and the Securities and Exchange Commission to agree on joint conditions for halting trading or staying open .	cause	2922..3058
Let 's not have one market shut down for 30 minutes when the Dow declines 100 points and another shut down for an hour after a 250-point decline .	cause	3059..3203
The need for hurried last-minute telephone negotiations among market officials will disappear	norel	3206..3299
once rules are in place that synchronize circuit breakers in all markets .	asynchronous	3300..3373
The new circuit breakers , if they are to be applied at all , will require that futures and options trading continue as long as the New York Stock Exchange remains open .	entrel	3374..3541
The rules should be established by agreement of the officials of all affected exchanges acting under the oversight and with the approval of the government regulatory agencies .	entrel	3542..3717
Should the SEC and the Commodities Futures Trading Commission ( which , with the SEC , regulates the Chicago stock-index markets ) be unable to agree , the issue may have to be resolved by decision of the Treasury secretary .	contrast	3718..3937
In many ways , our financial markets are better prepared today to handle a decline than they were two years ago .	norel	3940..4051
The New York Stock Exchange now has the capacity to handle a volume of nearly a billion shares a day .	instantiation	4052..4153
Telephone service has been improved for customers trying to reach their brokers	conjunction	4154..4233
and specialists -- who I believe should stay , despite the urgings of some post-crash critics -- have larger capital positions .	conjunction	4234..4361
( Of course , specialists ' actions alone can never prevent a major crack in stock prices .	contrast	4362..4449
Witness the fact that trading in some stocks closed early Friday and opened late Monday because of an excess of sell orders . )	instantiation	4450..4575
But the task of improving market performance remains unfinished .	norel	4576..4640
Mr. Freund , former chief economist of the New York Stock Exchange , is a professor of economics at Pace University 's business school in New York .	norel	4643..4787
