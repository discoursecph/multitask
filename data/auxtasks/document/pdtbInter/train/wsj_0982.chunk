Investors poured $2.8 billion more into money-market mutual funds in the latest week despite further declines in yields.	O	9..129
Assets of the 400 taxable funds tracked by IBC/Donoghue's Money Fund Report jumped to $351.2 billion in the week ended Tuesday, the Holliston, Mass.-based newsletter said.	O	132..303
Assets soared $4.5 billion in the previous week.	O	304..352
Meanwhile, the average yield on taxable funds dropped nearly a tenth of a percentage point, the largest drop since midsummer.	O	355..480
The average seven-day compound yield, which assumes that dividends are reinvested and that current rates continue for a year, fell to 8.47%, its lowest since late last year, from 8.55% the week before, according to Donoghue's.	O	481..707
"Lower yields are just reflecting lower short-term interest rates," said Brenda Malizia Negus, editor of Money Fund Report.	O	710..833
Money funds invest in such things as short-term Treasury securities, commercial paper and certificates of deposit, all of which have been posting lower interest rates since last spring.	B-concession	834..1019
Individual investors can still get better yields on money funds than on many other short-term instruments.	B-restatement	1022..1128
The yield on six-month Treasury bills sold at Monday's auction, for example, was just 7.77%.	B-contrast	1129..1221
The average yield on six-month CDs of $50,000 or less at major banks was 7.96% in the week ended Tuesday, according to Banxquote Money Markets, a New York information service.	I-contrast	1222..1397
One way that money fund managers boost yields in a declining rate environment is by extending the maturities of their investments, so they can earn the current higher rates for a longer period.	O	1400..1593
The average maturity of the taxable funds that Donoghue's follows increased by two days in the latest week to 40 days, its longest since August.	O	1594..1738
"They're anticipating further declines in rates and they're going to get them, slowly," said Walter Frank, chief economist for the Donoghue Organization, publisher of Money Fund Report.	O	1741..1926
Average maturity was as short as 29 days at the start of this year, when short-term interest rates were moving steadily upward.	O	1929..2056
The average seven-day compound yield of the funds reached 9.62% in late April.	O	2057..2135
The highest-yielding funds are still above 9%.	B-restatement	2138..2184
The top-performing fund in the latest week was Dreyfus Worldwide Dollar, with a seven-day compound yield of 9.45%.	B-cause	2185..2299
The fund invests heavily in dollar-denominated money-market securities overseas.	I-cause	2300..2380
It is currently waiving management fees, which contributes to the higher yield.	O	2381..2460
The average seven-day simple yield of the 400 funds fell to 8.14% from 8.21%, Donoghue's reported.	O	2463..2561
The average 30-day simple yield slid to 8.22% from 8.26%, and the average 30-day compound yield fell to 8.56% from 8.60%.	O	2562..2683
