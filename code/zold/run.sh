#python disco.py --cnn-seed 3068836234 --iters 10 --train ../data/rstdt_constituent/train/ --test ../data/rstdt_constituent/dev/ --h_layers $1 --pred_layer 2


python disco.py --cnn-seed 3068836234 --iters 20 --train ../data/rstdt_constituent/train/ ../data/factbank_seq/factbank_tense/ --pred_layer 2 2 --test ../data/rstdt_constituent/dev/ --h_layers 2 --sigma 0.2 --output outs/fact_tense.2.2.sigma0.2