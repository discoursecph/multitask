A frozen mountaintop in Tibet may offer an important clue	(Elaboration(Elaboration
about whether the Earth is warming perilously .	Elaboration)
Researchers at Ohio State University and Lanzhou Institute of Glaciology and Geocryology in China have analyzed samples of glacial ice in Tibet	(Elaboration(Evaluation
and say	(Attribution
temperatures there have been significantly higher on average over the past half-century than in any similar period in the past 10,000 years .	Attribution))
The ice samples are an important piece of evidence	(Elaboration(Evaluation(Elaboration
supporting theories	(Elaboration
that the Earth has warmed considerably in recent times ,	(Same-unit(Cause
largely because of pollutants in the air ,	Cause)
and will warm far more in the century ahead .	(Elaboration
A substantial warming would melt some of the Earth 's polar ice caps ,	(Temporal
raising the level of the oceans	(Temporal
and causing widespread flooding of heavily populated coastal areas .	Temporal))))))
`` If you can use data	(Attribution(Condition(Enablement
to reconstruct what happened in the past ,	Enablement)
you have much more confidence in predictions for the future , ''	Condition)
said Lonnie Thompson , a research scientist at Ohio State	(Elaboration
who dug for and analyzed the ice samples .	Elaboration)))
To compare temperatures over the past 10,000 years ,	(Elaboration(Enablement
researchers analyzed the changes in concentrations of two forms of oxygen .	Enablement)
These measurements can indicate temperature changes ,	(Elaboration(Explanation(Attribution
researchers said ,	Attribution)
because the rates of evaporation of these oxygen atoms differ	(Temporal
as temperatures change .	Temporal))
Analysis of ice from the Dunde ice cap , a glacial plateau in Tibet 17,000 feet above sea level , show	(Elaboration(Attribution(Attribution
that average temperatures were higher in 1937-87 than in any other 50-year period since before the last Ice Age ,	Attribution)
Mr. Thompson said .	Attribution)
Some climate models project	(Elaboration(Attribution
that interior regions of Asia would be among the first	(Elaboration
to heat up in a global warming	(Cause
because they are far from oceans ,	(Elaboration
which moderate temperature changes .	Elaboration))))
But the ice-core samples are n't definitive proof	(Elaboration(Attribution(Elaboration
that the so-called greenhouse effect will lead to further substantial global heating ,	Elaboration)
Mr. Thompson acknowledged .	Attribution)
According to greenhouse theories ,	(Elaboration(Attribution
increased carbon dioxide emissions ,	(Cause(Same-unit(Elaboration
largely caused by burning of fossil fuels ,	Elaboration)
will cause the Earth to warm up	Same-unit)
because carbon dioxide prevents heat from escaping into space .	Cause))
Skeptics say	(Elaboration(Contrast(Same-unit(Attribution
that	Attribution)
if that 's the case ,	(Condition
temperatures should have risen fairly uniformly over the past century ,	(Elaboration
reflecting the increase in carbon dioxide .	Elaboration)))
Instead , the Dunde ice-core record shows increasing temperatures from 1900 through the early 1950s , decreasing temperatures from the late 1950s through the mid-1970s , then higher temperatures again through last year .	Contrast)
Other temperature data show similar unexplained swings .	(Elaboration
`` Climate varies drastically due to natural causes , ''	(Contrast(Attribution
said Mr. Thompson .	Attribution)
But he said	(Attribution
ice samples from Peru , Greenland and Antarctica all show substantial signs of warming .	Attribution)))))))))))))
