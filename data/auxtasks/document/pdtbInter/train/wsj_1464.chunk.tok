Bullish bond market sentiment is on the rise again .	O	9..60
As the government prepares to release the next batch of economic reports , the consensus among economists and money managers is that the news will be negative .	B-conjunction	63..221
And that , they say , will be good for bonds .	I-conjunction	222..265
" Recent data have indicated somewhat weaker economic activity , " said Elliott Platt , director of economic research at Donaldson , Lufkin & Jenrette Securities .	O	268..425
Mr. Platt is advising clients that " the near-term direction of bond prices is likely to remain upward . "	O	426..529
Analysts insist that even without help from a shaky stock market , which provided a temporary boost for bonds during the Oct. 13 stock market plunge , bond prices will start to climb on the prospects that the Federal Reserve will allow interest rates to move lower in the coming weeks .	O	532..815
That would be comforting to fixed-income investors , many of whom were badly burned in the third quarter by incorrectly assuming that the Fed would ease .	O	818..970
Investors rushed to buy bonds during the summer as prices soared on speculation that interest rates would continue to fall .	B-contrast	971..1094
But when it became clear that rates had stabilized and that the Fed 's credit-easing policy was on hold , bond yields jumped and prices tumbled .	B-restatement	1095..1237
Long-term bonds have performed erratically this year .	B-instantiation	1238..1291
For example , a group of long-term Treasury bonds tracked by Merrill Lynch & Co. produced a total return of 1 % in the first quarter , 12.45 % in the second quarter and -0.06 % in the third quarter .	B-entrel	1292..1485
Total return is price changes plus interest income .	I-entrel	1486..1537
Now some investment analysts insist that the economic climate has turned cold and gloomy , and they are urging clients to buy bonds before the rally begins .	O	1540..1695
Among other things , economists note that consumer spending is slowing , corporate profit margins are being squeezed , business confidence is slipping and construction and manufacturing industries are depressed .	B-synchrony	1698..1906
At the same time , last week 's consumer price index showed that inflation is moderating .	I-synchrony	1907..1994
Add it all up and it means " that the Fed has a little leeway to ease its credit policy stance without the risk of rekindling inflation , " said Norman Robertson , chief economist at Mellon Bank Corp. , Pittsburgh .	O	1997..2206
" I think we will see a federal funds rate of close to 8 1/2 % in the next two weeks and 8 % by year end . "	O	2207..2310
The federal funds rate , which banks charge each other on overnight loans , is considered an early signal of changes in the Fed 's credit policy .	B-entrel	2313..2455
Economists generally agree that the rate was lowered by the Fed from around 9 % , where it had been since July , to about 8 3/4 % in early October on the heels of a weak employment report .	I-entrel	2456..2640
Although the rate briefly drifted even lower following the stock market sell-off that occurred Oct. 13 , it ended Friday at about 8 11/16 % .	O	2641..2779
James Kochan , chief fixed-income strategist at Merrill Lynch , is touting shorter-term securities , which he says should benefit more quickly than longer-term bonds as interest rates fall .	O	2782..2968
" Given our forecast for lower rates , purchases made now should prove quite rewarding before year end , " he said .	O	2969..3080
Mr. Kochan also likes long-term , investment-grade corporate bonds and long-term Treasurys .	O	3083..3173
He says these bonds should appreciate in value as some investors , reacting to the recent turmoil in the stock and high-yield junk bond markets , seek safer securities .	O	3174..3340
" If the { Tennessee Valley Authority } sale is any guide , there appears to be good demand for top-quality , long-term paper from both domestic and overseas accounts , " he said .	O	3341..3513
TVA , in its first public debt offering in 15 years , sold $ 4 billion of long-term and intermediate-term securities last week .	O	3516..3640
Strong investor demand prompted the utility to boost the size of the issue from $ 3 billion .	B-entrel	3641..3732
TVA , which operates one of the nation 's largest electric power systems , is a corporation owned by the federal government .	I-entrel	3733..3854
But persuading investors to buy bonds may be especially tough this week , when the U.S. government will auction more than $ 30 billion of new securities .	O	3857..4008
Today , the Treasury Department will sell $ 15.6 billion of three-month and six-month bills at the regular weekly auction .	B-conjunction	4009..4129
Tomorrow , the Treasury will sell $ 10 billion of two-year notes .	I-conjunction	4130..4193
Resolution Funding Corp. , known as Refcorp , a division of a new government agency created to bail out the nation 's troubled savings and loan associations , will hold its first bond auction Wednesday , when it will sell $ 4.5 billion of 30-year bonds .	B-conjunction	4196..4443
All of this comes ahead of the government 's big quarterly refunding of the federal debt , which takes place sometime in November .	I-conjunction	4444..4572
So far , investors have n't shown much appetite for Refcorp 's initial bond offering .	O	4575..4657
Roger Early , a portfolio manager at Federated Investors Corp. , said that yields on the so-called bailout bonds are n't high enough to attract his attention .	O	4660..4815
" Why should I bother with something that 's an unknown for a very small pickup in yield ? " he said .	O	4818..4915
" I 'm not going to jump on them the first day they come out . "	O	4916..4976
He seems to be typical of many professional money managers .	O	4979..5038
When the size of the Refcorp offering was announced last week and when-issued trading activity began , the bailout bonds were yielding about 1/20 percentage point more than the Treasury 's benchmark 30-year bond .	B-conjunction	5039..5249
On Friday , the yield was quoted at about 1/4 percentage point higher than the benchmark bond , an indication of weak demand .	I-conjunction	5250..5373
Some economists believe that yields on all Treasury securities may rise this week as the market struggles to absorb the new supply .	B-contrast	5376..5507
But once the new securities are digested , they expect investors to focus on the weak economic data .	I-contrast	5508..5607
" The supply is not a constraint to the market , " said Samuel Kahan , chief financial economist at Kleinwort Benson Government Securities Inc .	O	5610..5749
" If one thinks that rates are going down , you do n't care how much supply is coming . "	O	5750..5834
Friday 's Market Activity	O	5837..5861
Most bond prices fell on concerns about this week 's new supply and disappointment that stock prices did n't stage a sharp decline .	B-contrast	5864..5993
Junk bond prices moved higher , however .	I-contrast	5994..6033
In early trading , Treasury bonds were higher on expectations that a surge in buying among Japanese investors would continue .	B-conjunction	6036..6160
Also providing support to Treasurys was hope that the stock market might see declines because of the expiration of some stock-index futures and options on indexes and individual stocks .	B-concession	6161..6346
Those hopes were dashed when the stock market put in a relatively quiet performance .	I-concession	6347..6431
Treasury bonds ended with losses of as much as 1/4 point , or about $ 2.50 for each $ 1,000 face amount .	O	6434..6535
The benchmark 30-year bond , which traded as high as 102 1/4 during the day , ended at 101 17/32 .	O	6536..6631
The yield on the benchmark bond rose slightly to 7.98 % from 7.96 % .	O	6632..6698
In the corporate bond market , traders said the new-issue market for junk bonds is likely to pick up following Chicago & North Western Acquisition Corp. 's $ 475 million junk bond offering Friday .	O	6701..6895
Today , for example , underwriters at Morgan Stanley & Co. said they expect to price a $ 150 million , 12-year senior subordinated debenture offering by Imo Industries Inc .	O	6898..7066
Traders expect the issue to be priced to yield 12 % .	O	7067..7118
Shearson Lehman Hutton Inc. said that a $ 150 million senior subordinated discount debenture issue by R.P. Scherer Drugs is expected by the end of the month .	O	7119..7275
However , despite the big new-issue calendar , many junk bond investors and analysts are skeptical the deals will get done .	O	7278..7399
" There are about a dozen more deals coming , " said Michael McNamara , director of fixed-income research at Kemper Financial Services Inc .	O	7402..7537
" If they had this much trouble with Chicago & North Western , they are going to have an awful time with the rest . "	O	7538..7651
Last week , underwriters were forced to postpone three junk bond deals because of recent weakness in the market .	B-conjunction	7654..7765
And pressure by big investors forced Donaldson Lufkin & Jenrette Securities Corp. to sweeten Chicago & North Western 's $ 475 million junk bond offering .	B-restatement	7768..7919
After hours of negotiating that stretched late into Thursday night , underwriters priced the 12-year issue of resettable senior subordinated debentures at par to yield 14.75 % , higher than the 14.5 % that had been expected .	I-restatement	7920..8140
The coupon on the issue will be reset in one year at a rate that will give the issue a market value of 101 .	O	8141..8248
However , the maximum coupon rate on the issue when it is reset is 15.50 % .	O	8249..8322
Debenture holders also will receive the equivalent of 10 % of the common stock in Chicago & North Western 's parent company .	O	8323..8445
" The coupon was raised to induce some of the big players on the fence to come in , " said a spokesman for Donaldson .	O	8448..8562
" We put a price on the deal that the market required to get it done . "	O	8563..8632
The spokesman said the issue was sold out and met with strong interest abroad , particularly from Japanese investors .	O	8635..8751
In the secondary , or resale , market , junk bonds closed 1/2 point higher , while investment-grade corporate bonds fell 1/8 to 1/4 point .	O	8754..8888
