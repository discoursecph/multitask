It	O	0
was	O	1
only	O	2
in	O	3
the	O	4
mid-1980s	B-DATE	5
,	O	6
once	O	7
the	O	8
timetable	O	9
for	O	10
the	O	11
handover	O	12
of	O	13
Hong	B-GPE	14
Kong	I-GPE	15
and	O	16
Macau	B-GPE	17
had	O	18
been	O	19
agreed	O	20
,	O	21
that	O	22
the	O	23
government	O	24
began	O	25
to	O	26
localize	O	27
the	O	28
civil	O	29
service	O	30
in	O	31
preparation	O	32
for	O	33
the	O	34
transition	O	35
to	O	36
"	O	37
Macau	B-GPE	38
people	O	39
running	O	40
Macau	O	41
.	O	42
"	O	43
