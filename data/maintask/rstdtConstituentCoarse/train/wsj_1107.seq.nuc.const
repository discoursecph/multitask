This is in response to George Melloan 's Business World column `` The Housing Market Is a Bigger Mess	(NS-Elaboration(NS-Elaboration(NS-Comparison
Than You Think ''	NS-Comparison)
( op-ed page , Sept. 26 ) .	NS-Elaboration)
In Houston , we have seen	(NN-Joint(NN-Textual-organization(NN-Summary-Evaluation-Topic(NS-Cause(SN-Attribution
how bad the housing problem can become .	SN-Attribution)
Unused houses deteriorate rapidly ,	(NS-Elaboration(NS-Cause(NS-Cause
affecting the value of nearby homes ;	NS-Cause)
in a domino effect , the entire neighborhood can fall victim .	NS-Cause)
At this stage some people just `` walk away '' from homes	(NS-Elaboration(NS-Elaboration
where the mortgage exceeds current market value .	NS-Elaboration)
But most of them could have afforded to keep up their payments	(NN-Comparison
-- they chose not to do so .	NN-Comparison))))
The problem is so vast	(NS-Elaboration(SN-Cause
that we need to try innovative solutions	(NS-Elaboration
-- in small-scale experiments .	NS-Elaboration))
Here are some ideas :	(NS-Elaboration
1 ) Foreclosed homes could be sold by the FHA for no down payment	(NN-Joint(NN-Textual-organization(NS-Elaboration
( the biggest obstacle to young buyers ) ,	NS-Elaboration)
but with personal liability for the mortgage	(NS-Summary-Evaluation-Topic
( no walking away by choice ) .	NS-Summary-Evaluation-Topic))
2 ) Encourage long-term occupancy	(NN-Joint(NS-Joint
by forgiving one month 's payment	(NS-Cause(NN-Joint(NN-Textual-organization(NS-Elaboration
( off the tail end of the mortgage )	NS-Elaboration)
for every six months paid ;	NN-Textual-organization)
or perhaps have the down payment deferred to the end of the mortgage	(NN-Joint(NS-Elaboration
( balloon ) ,	NS-Elaboration)
but `` forgiven '' on a monthly pro-rata basis	NN-Joint))
as long as the owner remains the occupant .	NS-Cause))
3 ) Develop rental agreements with exclusive purchase options for the renter .	(NS-Cause(NS-Summary-Evaluation-Topic
An occupant will , in most every case , be better for the home and neighborhood than a vacant house .	NS-Summary-Evaluation-Topic)
In this way , the house is not dumped on to a glutted market .	NS-Cause))))))
John F. Merrill	(NN-Joint
Houston	NN-Joint))
The Federal Housing Administration , Veterans Administration and the Department of Housing and Urban Development further aggravate the problem of affordable housing stock	(NN-Textual-organization(NS-Elaboration(NS-Cause
by `` buying in '' to their foreclosed properties	(NS-Comparison(NN-Textual-organization(NS-Elaboration
( of which there are , alas , many )	NS-Elaboration)
at an inflated `` balance due ''	(NS-Elaboration
-- say $ 80,000 on a house worth $ 55,000 --	NS-Elaboration))
instead of allowing a free market to price the house	(NS-Elaboration
for what it 's really worth .	NS-Elaboration)))
Worse , the properties then sit around	(NN-Temporal(NS-Elaboration
deteriorating for maybe a year or so ,	NS-Elaboration)
but are resold eventually	(NN-Temporal(NN-Textual-organization(NS-Cause
( because of the attractiveness of the low down payment , etc . )	NS-Cause)
to a marginal buyer	(NS-Elaboration
who ca n't afford both the mortgage and needed repairs ;	NS-Elaboration))
and having little vested interest	(SN-Cause(SN-Temporal
that buyer will walk away	SN-Temporal)
and the vicious cycle repeats itself all over again .	SN-Cause))))
Paul Padget	(NN-Joint
Cincinnati	NN-Joint))))
