Part of a Series	(NN-TextualOrganization
SMYRNA , Ga. --	(NN-TextualOrganization
The auto-dealer strip in this booming suburb runs nearly five miles along Cobb Parkway ,	(NS-topic-shift(NS-background(NS-evaluation(NS-explanation-argumentative(NS-background(NS-elaboration-additional
stretching from the Perimeter highway	(NN-Same-Unit(NS-elaboration-additional
that circles Atlanta	NS-elaboration-additional)
to the `` Big Chicken , '' a pullet-roofed fast-food restaurant and local landmark .	NN-Same-Unit))
Twenty years ago , in the infancy of suburban sprawl , just a handful of dealerships were here .	(NS-elaboration-additional(NN-Contrast
Now there are 23 .	(NS-elaboration-set-member
Alongside such long-familiar names as Chevrolet , Ford and Dodge are nameplates	(NS-elaboration-object-attribute
that did n't exist until three years ago :	(NS-example
Acura , Sterling , Hyundai .	NS-example))))
Under construction is the strip 's 24th showroom , the future home of Lexus , a luxury marque	(NS-elaboration-additional
launched by Toyota Motor Corp. just two months ago .	NS-elaboration-additional)))
The 1980s have spawned an explosion of consumer choice in America , in everything from phone companies to colas .	(NS-example
And especially ,	(NS-elaboration-set-member(NN-Same-Unit(NS-evidence
as the Cobb Parkway strip attests ,	NS-evidence)
in cars .	NN-Same-Unit)
Americans now can choose among 572 different models of cars , vans and trucks , up from just 408	(NS-attribution(NS-circumstance
when the decade began ,	NS-circumstance)
according to Automotive News , a trade publication .	NS-attribution))))
For car marketers , it has become a much tougher battle	(NS-interpretation(NS-purpose
to keep loyal customers from defecting to one of the new makes on the block .	NS-purpose)
For American car buyers , the proliferation of choice is both liberating and confusing .	(NS-comment
Malcolm MacDougall , vice chairman of the Jordan , McGrath , Case & Taylor advertising agency in New York , calls the proliferation `` nameplate mania . ''	(NN-List
He says	(NN-List(NS-elaboration-set-member(SN-attribution
the number of automobile choices is causing stress among consumers today ,	(NN-List
and that people will simply ignore new models	(NS-elaboration-object-attribute
that lack a well-defined image .	NS-elaboration-object-attribute)))
`` The winners , ''	(NS-elaboration-additional(NN-Same-Unit(NS-attribution
he predicts ,	NS-attribution)
`` will be brands from car makers	(NS-elaboration-object-attribute
that have traditionally been associated with quality and value . ''	NS-elaboration-object-attribute))
He says	(SN-attribution
it 's important for a new make to be as distinctive as possible	(NS-result(NS-temporal-same-time
while still retaining links to the parent company 's quality image .	NS-temporal-same-time)
He applauds Toyota and Nissan Motor Co .	(NS-reason
for creating separate divisions for their new luxury models ,	(NN-Contrast
rather than simply adding more nameplates to their standard car lines .	NN-Contrast))))))
Some auto executives believe	(NS-interpretation(SN-attribution
the benefits of more choice outweigh the drawbacks .	SN-attribution)
`` There 's more noise out there ,	(NS-example(NS-antithesis(NS-attribution(NN-List
and the consumer may have to work harder	(NS-purpose
to cut through it , ''	NS-purpose))
says Vincent P. Barabba , executive director of market research and planning at General Motors Corp .	NS-attribution)
`` But the reward is that there 's less need to make tradeoffs ''	(NS-elaboration-additional
in choosing one 's wheels .	NS-elaboration-additional))
Jeanene Page , of North Salt Lake City , Utah , likes the broader selection .	(NS-elaboration-additional(NS-background
She wants something big ,	(NN-List
and already has looked at the Chrysler New Yorker and Lincoln Town Car .	NN-List))
Now , the 55-year-old car shopper is zeroing in on a full-sized van ,	(NS-reason
figuring	(NS-elaboration-object-attribute(NS-elaboration-additional
that it 's just the thing	(NS-elaboration-object-attribute
to haul nine grandchildren	(NN-List
and pull a boat at the same time .	NN-List)))
`` That seems to be what all my friends are using	(NS-attribution(NS-purpose
to take the grandkids to the lake , ''	NS-purpose)
she says .	NS-attribution)))))))))))
Market segmentation in cars is n't new ,	(NS-evaluation(NS-evidence(NS-antithesis
but it 's far more extensive	(NS-comparison
than when Alfred P. Sloan Jr. conceived the idea 50 years ago .	(NS-elaboration-additional
The legendary GM chairman declared	(NN-Contrast(SN-attribution
that his company would make `` a car for every purse and purpose . ''	SN-attribution)
Now there are many cars for every purse and purpose .	(NS-example
Just four years ago , GM planners divided the combined car and truck market into seven segments .	(NN-Contrast
Today , they identify 19 distinct segments for cars , and another 11 for trucks and vans .	NN-Contrast))))))
The number of makes has mushroomed	(NS-evidence(NS-result(NS-reason
because the U.S. is the world 's biggest and richest market for automobiles ;	NS-reason)
virtually every auto maker wants to sell here .	NS-result)
For every brand like Renault or Fiat	(NS-result(NN-Same-Unit(NS-elaboration-object-attribute
that has been squeezed out ,	NS-elaboration-object-attribute)
others such as Isuzu , Daihatsu and Mitsubishi have come in .	NN-Same-Unit)
Detroit tries to counter the foreign invasion with new brands of its own .	(NS-example
GM launched the Geo marque this year	(NN-List(NS-purpose
to sell cars	(NS-elaboration-object-attribute
made in partnership with foreign auto makers ,	NS-elaboration-object-attribute))
and next year GM 's long-awaited Saturn cars will make their debut .	(NN-List
Ford Motor Co. created the Merkur nameplate in 1985	(NS-antithesis(NS-purpose
to sell its German-made touring sedans in the U.S .	NS-purpose)
But slow sales forced Ford to kill the brand just last week .	NS-antithesis)))))))
When consumers have so many choices ,	(NS-comment(NS-example(NS-evidence(SN-cause
brand loyalty is much harder to maintain .	SN-cause)
The Wall Street Journal 's `` American Way of Buying '' survey found	(NS-elaboration-additional(SN-attribution
that 53 % of today 's car buyers tend to switch brands .	SN-attribution)
For the survey , Peter D. Hart Research Associates and the Roper Organization each asked about 2,000 U.S. consumers about their buying habits .	NS-elaboration-additional))
Which cars do Americans favor most these days ?	(NS-example(NS-concession
It 's hard to generalize ,	(NS-antithesis
but age seems to be the best predictor .	NS-antithesis))
Adults under age 30 like sports cars , luxury cars , convertibles and imports far more	(NS-evidence(NS-comparison
than their elders do .	NS-comparison)
Three of every 10 buyers under 30 would prefer to buy a sports car ,	(NN-List(NS-attribution(NS-comparison
compared with just 16 % of adults 30 and over ,	NS-comparison)
according to the Journal survey .	NS-attribution)
Young consumers prefer luxury cars by a 37 % to 28 % margin	(NN-List(NS-concession
-- even though older buyers ,	(NN-Same-Unit(NS-reason
because of their incomes ,	NS-reason)
are more likely to actually purchase a luxury car .	NN-Same-Unit))
Perhaps most striking , 35 % of households	(NS-explanation-argumentative(NS-comment(NS-elaboration-additional(NS-antithesis(NN-Same-Unit(NS-elaboration-object-attribute
headed by people aged 18 to 44	NS-elaboration-object-attribute)
have at least one foreign car .	NN-Same-Unit)
That 's true of only 14 % of households	(NS-elaboration-object-attribute
headed by someone 60 or older .	NS-elaboration-object-attribute))
Generally , imports appeal most to Americans	(NS-elaboration-object-attribute
who live in the West	(NN-List
and are well-educated , affluent and , especially , young .	NN-List)))
`` For many baby boomers , buying a domestic car is a totally foreign experience , ''	(NS-attribution
says Christopher Cedergren , auto-market analyst with J.D. Power & Co. of Agoura Hills , Calif .	NS-attribution))
Such preferences persist	(NS-concession
even though many Americans believe	(NS-evidence(SN-attribution
differences between imported and domestic cars are diminishing .	SN-attribution)
Only 58 % of Americans now believe	(NS-antithesis(NN-List(NS-attribution(SN-attribution
that foreign cars get better gas mileage than domestic models ,	SN-attribution)
the Journal survey found , down from 68 % in 1987 .	NS-attribution)
Some 46 % give foreign cars higher quality ratings , down from 50 % two years ago .	NN-List)
On the other hand , only 42 % say	(SN-attribution
foreign cars are less comfortable than U.S. models , down from 55 % in 1987 .	SN-attribution))))))))))
People in the automotive business disagree	(SN-attribution
over how susceptible younger Americans are to brand switching .	(NS-example
`` Once buying habits are formed ,	(NN-Contrast(NS-attribution(SN-concession
they 're very hard to break , ''	SN-concession)
declares Thomas Mignanelli , executive vice president for Nissan 's U.S. sales operations .	NS-attribution)
But out on Cobb Parkway , Ted Negas sees it differently .	(NS-evaluation(SN-attribution
`` The competition is so intense	(NS-attribution(NS-result
that an owner 's loyalty to a dealership or a car is virtually nonexistent , ''	NS-result)
says Mr. Negas , vice president of Ed Voyles Oldsmobile , one of the first dealerships	(NS-elaboration-object-attribute
to locate on the strip .	NS-elaboration-object-attribute)))
Thus the very fickleness of baby boomers may make it possible to win them back ,	(NS-elaboration-additional(NS-comparison
just as it was possible to lose them .	NS-comparison)
The battle for customer loyalty is evident along the Cobb Parkway strip .	(NN-List(NS-example
Ed Voyles Olds recently established a special section in the service department for owners	(NS-purpose(NS-elaboration-object-attribute
whose cars are less than a year old ,	NS-elaboration-object-attribute)
so they get quicker service .	NS-purpose))
Just down the street , Chris Volvo invites serious shoppers to test-drive a new Volvo to any other dealership along the strip , and compare the cars side-by-side .	(NN-List
Manufacturers , too , are stretching further	(NS-elaboration-additional(NS-purpose
to lure buyers .	NS-purpose)
GM 's Cadillac division ,	(NS-elaboration-additional(NN-Same-Unit(NS-elaboration-object-attribute
ignoring Detroit 's long-held maxim	(NS-elaboration-object-attribute
that safety does n't sell ,	NS-elaboration-object-attribute))
is airing television commercials	(NS-elaboration-additional
touting its cars ' safety features .	NS-elaboration-additional))
Cadillac may be on to something .	(NS-evidence
Some 60 % of the survey respondents said	(NN-Contrast(NS-elaboration-additional(NN-List(SN-attribution
they would buy anti-lock brakes	(NS-concession
even if they carry a medium or high price tag .	NS-concession))
More than 50 % felt the same way about air bags .	NN-List)
Both features appealed most to buyers under 45 .	NS-elaboration-additional)
In contrast , dashboard computers , power seats and turbo-charged engines had little appeal .	(NS-antithesis
But even a little appeal has a lot of attraction these days .	(NS-example
GM 's Pontiac division is offering a turbo-charged V-6 engine on its Grand Prix model ,	(NS-reason(NS-antithesis
even though it expects to sell only about 4,000 cars	(NS-elaboration-object-attribute
equipped with that option .	NS-elaboration-object-attribute))
The reason :	(NS-elaboration-object-attribute
Items with narrow appeal can be important in a market as fragmented as today 's .	(NS-evaluation
Americans spent more than $ 190 billion on new cars and trucks last year ,	(NN-List
and just 1 % of that market exceeded Polaroid Co. 's sales of $ 1.86 billion .	(NS-comment
`` Even if it 's only 1 % , ''	(SN-concession(NS-attribution
says GM 's Mr. Barabba ,	NS-attribution)
`` would you throw away sales the size of Polaroid ? ''	SN-concession))))))))))))))))))))))
( All buyers 47 % )	NS-topic-shift)))
