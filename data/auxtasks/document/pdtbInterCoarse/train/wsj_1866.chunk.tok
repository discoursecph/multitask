Two years of coddling , down the drain .	O	9..47
That 's the way a lot of brokers feel today on the second anniversary of the 1987 stock-market crash .	O	50..150
Ever since that fearful Black Monday , they 've been tirelessly wooing wary individual investors -- trying to convince them that Oct. 19 , 1987 , was a fluke and that the stock market really is a safe place for average Americans to put their hard-earned dollars .	B-expansion	153..411
And until last Friday , it seemed those efforts were starting to pay off .	I-expansion	414..486
" Some of those folks were coming back , " says Leslie Quick Jr. , chairman , of discount brokers Quick & Reilly Group Inc .	O	487..605
" We had heard from people who had n't been active " for a long time .	O	606..672
Then came the frightening 190-point plunge in the Dow Jones Industrial Average and a new wave of stock-market volatility .	B-contingency	675..796
All of a sudden , it was back to square one .	I-contingency	797..840
" It 's going to set things back for a period , because it reinforces the concern of volatility , " says Jeffrey B. Lane , president of Shearson Lehman Hutton Inc .	O	843..1000
" I think it will shake confidence one more time , and a lot of this business is based on client confidence . "	O	1001..1108
Brokers around the country say the reaction from individual investors this week has been almost eerie .	O	1111..1213
Customers and potential customers are suddenly complaining about the stock market in the exact way they did in post-crash 1987 .	O	1214..1341
" The kinds of questions you had before have resurfaced , " says Raymond A. " Chip " Mason , chairman of regional brokerage firm Legg Mason Inc. , Baltimore .	O	1344..1494
" I can just tell the questions are right back where they were : ' What 's going on ? , ' ' Ca n't anything be done about program trading ? , ' ' Does n't the exchange understand ? , ' ' Where is the SEC on this ? ' "	O	1495..1692
Mr. Mason says he 's convinced the public still wants to invest in common stocks , even though they believe the deck is stacked against them .	O	1695..1834
But " these wide swings scare them to death . "	B-entrel	1835..1879
All of this is bad news for the big brokerage firms such as Shearson and Merrill Lynch & Co. that have big " retail , " or individual-investor , businesses .	B-contingency	1880..2032
After expanding rapidly during the bull-market years up to the 1987 crash , retail brokerage operations these days are getting barely enough business to pay the overhead .	I-contingency	2033..2202
True , the amount of money investors are willing to entrust to their brokers has been growing lately .	B-comparison	2205..2305
But those dollars have been going into such " safe " products as money market funds , which do n't generate much in the way of commissions for the brokerage firms .	I-comparison	2306..2465
At discount brokerage Charles Schwab & Co. , such " cash-equivalent " investments recently accounted for a record $ 8 billion of the firm 's $ 25 billion of client 's assets .	O	2466..2633
The brokers ' hope has been that they could soon coax investors into shifting some of their hoard into the stock market .	B-expansion	2636..2755
And before last Friday , they were actually making modest progress .	I-expansion	2756..2822
A slightly higher percentage of New York Stock Exchange volume has been attributed to retail investors in recent months compared with post-crash 1988 , according to Securities Industry Association data .	B-entrel	2825..3026
In 1987 , an average 19.7 % of Big Board volume was retail business , with the monthly level never more than 21.4 % .	B-temporal	3027..3139
The retail participation dropped to an average 18.2 % in 1988 , and shriveled to barely 14 % some months during the year .	B-comparison	3140..3258
Yet in 1989 , retail participation has been more than 20 % in every month , and was 23.5 % in August , the latest month for which figures are available .	I-comparison	3261..3408
Jeffrey Schaefer , the SIA 's research director , says that all of his group 's retail-volume statistics could be overstated by as much as five percentage points because corporate buy-backs are sometimes inadvertently included in Big Board data .	O	3411..3652
But there did seem to be a retail activity pickup .	O	3653..3703
But " Friday did n't help things , " says Mr. Schaefer .	O	3706..3757
With the gyrations of recent days , says Hugo Quackenbush , senior vice president at Charles Schwab , many small investors are absolutely convinced that " they should n't play in the stock market . "	O	3760..3952
Joseph Grano , president of retail sales and marketing at PaineWebber Group Inc. , still thinks that individual investors will eventually go back into the stock market .	O	3955..4121
Investors will develop " thicker skins , " and their confidence will return , he says .	O	4122..4204
Friday 's plunge , he is telling PaineWebber brokers , was nothing more than a " tremendous reaction to leveraged buy-out stocks . "	O	4205..4331
Meanwhile , PaineWebber remains among the leaders in efforts to simply persuade investors to keep giving Wall Street their money .	O	4334..4462
" It 's more of an important issue to keep control of those assets , rather than push the investor to move into ( specific ) products such as equities , " Mr. Grano says .	O	4463..4626
" The equity decision will come when the client is ready and when there 's a semblance of confidence . "	O	4627..4727
It could be a long wait , say some industry observers .	O	4730..4783
" Some investors will tiptoe back in , " says Richard Ross , a market research director for Elrick & Lavidge in Chicago .	O	4784..4900
" Then there 'll be another swing .	B-entrel	4901..4933
Given enough of these , this will drive everyone out except the most hardy , " he adds .	I-entrel	4934..5018
Mr. Ross , who has been studying retail investors ' perception of risks in the brokerage industry , said a market plunge like Friday 's " shatters investors ' confidence in their ability to make any judgments on the market . "	O	5021..5239
The long-term outlook for the retail brokerage business is " miserable , " Mr. Ross declares .	O	5240..5330
