With the dust settling from the failed coup attempt in Panama , one of the many lingering questions the Bush administration will ponder is this : Is the National Security Council staff big enough , and does it have enough clout , to do its job of coordinating foreign policy ?	O	9..280
President Bush 's national security adviser , Lt. Gen. Brent Scowcroft , came into office in January intent on making the NSC staff leaner and more disciplined than it had been during the Reagan administration .	B-temporal	283..490
Gen. Scowcroft was a member of the Tower Commission , which investigated the Iran-Contra affair .	B-contingency	491..586
He was all too aware of how a large , inadequately supervised NSC staff had spun out of control and nearly wrecked President Reagan 's second term .	I-contingency	587..732
So , following both the style he pursued as President Ford 's national security adviser and the recommendations of the Tower Commission , Gen. Scowcroft has pruned the NSC staff and tried to ensure that it sticks to its assigned tasks -- namely , gathering the views of the State Department , Pentagon and intelligence community ; serving as an honest broker in distilling that information for the president and then making sure presidential decisions are carried out .	O	735..1197
The Tower Commission specifically said that the NSC staff should be " small " and warned against letting " energetic self-starters " like Lt. Col. Oliver North strike out on their own rather than leaving the day-to-day execution of policies to the State Department , Pentagon or Central Intelligence Agency .	O	1200..1502
However , the Panama episode has raised questions about whether the NSC staff is sufficiently big , diverse and powerful to coordinate U.S. policy on tough issues .	O	1505..1666
During the coup attempt and its aftermath , NSC staffers were " stretched very thin , " says one senior administration official .	O	1667..1791
" It 's a very small shop . "	O	1792..1817
Gen. Scowcroft does n't plan to increase the staff right now , but is weighing that possibility , the official adds .	O	1820..1933
The NSC staff " does n't have the horsepower that I believe is required to have an effective interagency process , " says Frank Gaffney , a former Pentagon aide who now runs the Center for Security Policy , a conservative Washington think-tank .	O	1934..2172
" The problem with this administration , I think , is that by design it has greatly diminished , both in a physical sense and in a procedural sense , the role of the NSC . "	O	2173..2339
The National Security Council itself was established in 1947 because policy makers sensed a need , in an increasingly complex world , for a formal system within the White House to make sure that communications flowed smoothly between the president and the State Department , Pentagon and intelligence agencies .	B-entrel	2342..2649
By law , the council includes the president , vice president and secretaries of state and defense .	B-expansion	2650..2746
In practice , the director of central intelligence and chairman of the Joint Chiefs of Staff also serve as unofficial members .	B-comparison	2747..2872
But the size , shape and role of the NSC staff have been left for each president and his national security adviser to decide .	B-entrel	2875..2999
That task is one of Washington 's perennial problems .	B-entrel	3000..3052
In the Bush White House , the size of the NSC 's staff of professional officers is down to about 50 from about 70 in 1987 , administration officials say .	I-entrel	3053..3203
Administration officials insist that the size of the staff was n't a problem during the Panama crisis .	O	3206..3307
But one clear problem during the coup attempt was that the NSC staffer most experienced in Latin America , Everett Briggs , was gone .	B-expansion	3308..3439
He had just resigned , at least in part because of a feud with Assistant Secretary of State Bernard Aronson over the administration 's policy on Panama and support for Nicaragua 's Contra rebels .	I-expansion	3440..3632
The absence of Mr. Briggs underscored the possible inadequacy of the current NSC staff .	B-entrel	3635..3722
Both Gen. Scowcroft and his deputy , Robert Gates , are experts in U.S.-Soviet affairs .	I-entrel	3723..3808
Gen. Scowcroft is particularly well-versed in arms control , and Mr. Gates has spent years studying Soviet politics and society .	B-entrel	3809..3936
Both have become confidants of President Bush .	I-entrel	3937..3983
But neither has an extensive background in Latin America , the Middle East or Asia .	B-contingency	3986..4068
In those areas , the role of NSC staffers under them therefore have become more important .	I-contingency	4069..4158
Gen. Scowcroft knows as well as anyone that one of the biggest dangers he faces is that NSC staffers working in relative anonymity will take over policy-making and operational tasks that are best left to bigger and more experienced State Department and Pentagon bureaus .	O	4161..4431
But just as every previous NSC adviser has , Gen. Scowcroft now will have to mull at what point the NSC staff becomes too lean and too restrained .	O	4432..4577
