Wall Street is just about ready to line the bird cage with paper stocks .	(NN-Contrast(NS-explanation-argumentative
For three years , a healthy economy and the export-boosting effects of a weak dollar propelled sales and earnings of the big paper companies to record levels .	(NS-elaboration-additional
As the good times rolled	(NS-evaluation(NS-comment(SN-concession(SN-circumstance
they more than doubled their prices for pulp , a raw material	(NN-Same-Unit(NS-elaboration-object-attribute
used in all sorts of paper ,	NS-elaboration-object-attribute)
to $ 830 a metric ton this past spring from $ 380 a ton at the start of 1986 .	NN-Same-Unit))
But now the companies are getting into trouble	(NN-Cause-Result(NN-Cause-Result
because they undertook a record expansion program	(NN-Temporal-Same-Time
while they were raising prices sharply .	NN-Temporal-Same-Time))
Third-quarter profits fell at several companies .	NN-Cause-Result))
`` Put your money in a good utility or bank stock , not a paper company , ''	(NS-attribution
advises George Adler of Smith Barney .	NS-attribution))
Other analysts are nearly as pessimistic .	(NS-elaboration-additional(NN-List(NN-Statement-Response(NS-example
Gary Palmero of Oppenheimer & Co. expects a 30 % decline in earnings between now and 1991 for `` commodity-oriented '' paper companies ,	(NN-List(NS-elaboration-additional
which account for the majority of the industry .	NS-elaboration-additional)
Robert Schneider of Duff & Phelps sees paper-company stock prices falling 10 % to 15 % in 1990 , perhaps 25 %	(NS-condition
if there 's a recession .	NS-condition)))
Paper companies concede that business has been off recently .	(NS-explanation-argumentative
But they attribute much of the weakness to customer inventory reductions .	(NN-List
Generally they maintain	(NS-elaboration-additional(SN-attribution
that ,	(NN-Same-Unit
barring a recession and a further strengthening of the dollar against foreign currencies ,	(SN-circumstance
the industry is n't headed for a prolonged slump .	SN-circumstance)))
`` It wo n't be an earthshaking drop , ''	(NS-attribution
a Weyerhaeuser spokesman says .	NS-attribution)))))
Last week Mr. Adler lowered his rating from hold to `` avoid '' on Boise Cascade , Champion International , Great Northern Nekoosa , International Paper , Louisiana Pacific and Weyerhaeuser .	(NS-evaluation(NN-List
Oppenheimer 's Mr. Palmero , meanwhile , is steering clear of Gaylord Container , Stone Container and Federal Paper Board .	(NN-List
Mr. Schneider is cool to Georgia Pacific and Abitibi-Price .	(NN-List
Lawrence Ross of PaineWebber would avoid Union Camp .	NN-List)))
The companies in question believe	(NS-example(SN-attribution
the analysts are too pessimistic .	SN-attribution)
Great Northern Nekoosa said ,	(NN-List(SN-attribution
`` The odds of the dire predictions	(NN-Same-Unit(NS-elaboration-object-attribute
about us being right	NS-elaboration-object-attribute)
are small . ''	NN-Same-Unit))
International Paper emphasizes	(NN-List(SN-attribution
that it is better positioned than most companies for the coming overcapacity	(NS-result
because its individual mills can make more than one grade of paper .	NS-result))
A Boise-Cascade spokesman referred to a speech by Chairman John Fery ,	(NN-List(NS-elaboration-object-attribute
in which he said	(SN-attribution
that markets generally are stable ,	(NS-antithesis
although some risk of further price deterioration exists .	NS-antithesis)))
Stone Container Chairman Roger Stone said	(NN-List(NS-elaboration-additional(SN-attribution
that , unlike for some other paper products , demand for Stone 's principal commodity , unbleached containerboard , remains strong .	SN-attribution)
He expects the price for that product to rise even more next year .	NS-elaboration-additional)
Gaylord Container said	(NN-List(SN-attribution
analysts are skeptical of it	(NS-reason
because it 's carrying a lot of debt .	NS-reason))
Champion International said ,	(NN-List(SN-attribution
`` We 've gotten our costs down	(SN-circumstance
and we 're better positioned for any cyclical downturn	(NN-Comparison
than we 've ever been . ''	NN-Comparison)))
Louisiana Pacific and Georgia Pacific said	(NN-List(SN-attribution
a number of other analysts are recommending them	(NS-reason
because of their strong wood-products business .	NS-reason))
Federal Paper Board said ,	(NS-explanation-argumentative(SN-attribution
`` We 're not as exposed as the popular perception of us . ''	SN-attribution)
The company explained	(SN-attribution
that its main product , bleached paperboard ,	(NN-Same-Unit(NS-elaboration-additional
which goes into some advertising materials and white boxes ,	NS-elaboration-additional)
historically does n't have sharp price swings .	NN-Same-Unit)))))))))))))
Because the stock prices of some paper companies already reflect an expected profit slump ,	(NS-elaboration-additional(NN-Cause-Result
PaineWebber 's Mr. Ross says	(SN-attribution
he thinks	(SN-attribution
that next year the share prices of some companies may fall at most only 5 % to 10 % .	SN-attribution)))
A company such as Federal Paper Board may be overly discounted	(NS-antithesis(NS-attribution(NN-List
and looks `` tempting '' to him ,	NN-List)
he says ,	NS-attribution)
though he is n't yet recommending the shares .	NS-antithesis))))))
Wall Street is n't avoiding everything	(NS-explanation-argumentative(NS-elaboration-object-attribute
connected with paper .	NS-elaboration-object-attribute)
Mr. Palmero recommends Temple-Inland ,	(NN-List(NS-elaboration-additional(NS-explanation-argumentative
explaining	(SN-attribution
that it is `` virtually the sole major paper company	(NN-Cause-Result(NS-elaboration-object-attribute
not undergoing a major capacity expansion , ''	NS-elaboration-object-attribute)
and thus should be able to lower long-term debt substantially next year .	NN-Cause-Result)))
A Temple-Inland spokesman said	(SN-attribution
the company expects record earnings in 1989 ,	(NN-List
and `` we 're still pretty bullish '' on 1990 .	NN-List)))
The analysts say	(NS-example(SN-attribution
their gloomy forecasts have a flip side .	SN-attribution)
Some take a warm view of consumer-oriented paper companies ,	(NS-evaluation(NS-elaboration-additional(NS-elaboration-additional
which buy pulp from the commodity producers	(NN-List
and should benefit from the expected declines in pulp prices .	NN-List))
Estimates on how much pulp prices will fall next year currently run between $ 150 and $ 250 a metric ton .	NS-elaboration-additional)
Analysts agree	(NS-elaboration-additional(SN-attribution
that the price drop should especially benefit the two big tissue makers , Scott Paper and Kimberly-Clark .	SN-attribution)
A spokesman for Scott says	(NN-Same-Unit(SN-attribution
that	SN-attribution)
assuming the price of pulp continues to soften ,	(SN-circumstance
`` We should do well . ''	SN-circumstance))))))))
