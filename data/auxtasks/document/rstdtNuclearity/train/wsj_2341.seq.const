Earnings for most of the nation 's major pharmaceutical makers are believed to have moved ahead briskly in the third quarter ,	(NS(NS(NS(NS(NS
as companies with newer , big-selling prescription drugs fared especially well .	NS)
For the third consecutive quarter , however , most of the companies ' revenues were battered by adverse foreign-currency translations	(NS
as a result of the strong dollar abroad .	NS))
Analysts said	(NS(SN
that Merck & Co. , Eli Lilly & Co. , Warner-Lambert Co. and the Squibb Corp. unit of Bristol-Myers Squibb Co. all benefited from strong sales of relatively new , higher-priced medicines	(NS
that provide wide profit margins .	NS))
Less robust earnings at Pfizer Inc. and Upjohn Co. were attributed to those companies ' older products ,	(NS
many of which face stiffening competition from generic drugs and other medicines .	NS)))
Joseph Riccardo , an analyst with Bear , Stearns & Co. , said	(NS(SN
that over the past few years most drug makers have shed their slow-growing businesses	(NN
and instituted other cost savings ,	(NS
such as consolidating manufacturing plants and administrative staffs .	NS)))
As a result , `` major new products are having significant impact , even on a company with very large revenues , ''	(NS
Mr. Riccardo said .	NS)))
Analysts said	(NS(NS(SN
profit for the dozen or so big drug makers , as a group , is estimated to have climbed between 11 % and 14 % .	SN)
While that 's not spectacular ,	(SN
Neil Sweig , an analyst with Prudential Bache , said	(SN
that the rate of growth will `` look especially good	(NS(NS
as compared to other companies	NS)
if the economy turns downward . ''	NS))))
Mr. Sweig estimated	(NN(NS(NS(NS(SN
that Merck 's profit for the quarter rose by about 22 % ,	(NS
propelled by sales of its line-up of fast-growing prescription drugs ,	(NS
including its anti-cholesterol drug , Mevacor ; a high blood pressure medicine , Vasotec ; Primaxin , an antibiotic , and Pepcid , an anti-ulcer medication .	NS)))
Profit climbed	(NS(NS(NS
even though Merck 's sales were reduced by `` one to three percentage points ''	NS)
as a result of the strong dollar ,	NS)
Mr. Sweig said .	NS))
In the third quarter of 1988 , Merck earned $ 311.8 million , or 79 cents a share .	NS)
In Rahway , N.J. , a Merck spokesman said	(SN
the company does n't make earnings projections .	SN))
Mr. Sweig said	(NN(NS(NS(NS(NS(SN
he estimated	(SN
that Lilly 's earnings for the quarter jumped about 20 % ,	(NS
largely because of the performance of its new anti-depressant Prozac .	NS)))
The drug ,	(NN(NS
introduced last year ,	NS)
is expected to generate sales of about $ 300 million this year .	NN))
`` It 's turning out to be a real blockbuster , ''	(NS
Mr. Sweig said .	NS))
In last year 's third quarter , Lilly earned $ 171.4 million , or $ 1.20 a share .	NS)
In Indianapolis , Lilly declined comment .	NS)
Several analysts said	(NN(NS(SN
they expected Warner-Lambert 's profit also to increase by more than 20 % from $ 87.7 million , or $ 1.25 a share ,	(NS
it reported in the like period last year .	NS))
The company is praised by analysts	(NS(NS
for sharply lowering its costs in recent years	(NN
and shedding numerous companies with low profit margins .	NN))
The company 's lean operation ,	(NS(NN(NS
analysts said ,	NS)
allowed sharp-rising sales from its cholesterol drug , Lopid ,	(NS
to power earnings growth .	NS))
Lopid sales are expected to be about $ 300 million this year , up from $ 190 million in 1988 .	(NS
In Morris Plains , N.J. , a spokesman for the company said	(SN
the analysts ' projections are `` in the ballpark . ''	SN)))))
Squibb 's profit ,	(NN(NS(NS(NN(NS
estimated by analysts to be about 18 % above the $ 123 million , or $ 1.25 a share ,	(NS
it earned in the third quarter of 1988 ,	NS))
was the result of especially strong sales of its Capoten drug	(NS
for treating high blood pressure and other heart disease .	NS))
The company was officially merged with Bristol-Myers Co. earlier this month .	NS)
Bristol-Myers declined to comment .	NS)
Mr. Riccardo of Bear Stearns said	(NN(NS(NS(SN
that Schering-Plough Corp. 's expected profit rise of about 18 % to 20 % , and Bristol-Meyers 's expected profit increase of about 13 % are largely because `` those companies are really managed well . ''	SN)
ScheringPlough earned $ 94.4 million , or 84 cents a share ,	(NN
while Bristol-Myers earned $ 232.3 million , or 81 cents a share , in the like period a year earlier .	NN))
In Madison , N.J. , a spokesman for Schering-Plough said	(NS(SN
the company has `` no problems '' with the average estimate by a analysts	(NS
that third-quarter earnings per share rose by about 19 % , to $ 1 .	NS))
The company expects to achieve the 20 % increase in full-year earnings per share ,	(NS(NS
as it projected in the spring ,	NS)
the spokesman said .	NS)))
Meanwhile , analysts said	(NN(NS(NS(NS(NS(SN
Pfizer 's recent string of lackluster quarterly performances continued ,	(NS
as earnings in the quarter were expected to decline by about 5 % .	NS))
Sales of Pfizer 's important drugs , Feldene	(NS(NN(NS
for treating arthritis ,	NS)
and Procardia , a heart medicine , have shrunk	NN)
because of increased competition .	NS))
`` The ( strong ) dollar hurt Pfizer a lot , too , ''	(NS
Mr. Sweig said .	NS))
In the third quarter last year , Pfizer earned $ 216.8 million , or $ 1.29 a share .	NS)
In New York , the company declined comment .	NS)
Analysts said	(NS(NS(SN
they expected Upjohn 's profit to be flat or rise by only about 2 % to 4 %	(NS
as compared with $ 89.6 million , or 49 cents a share ,	(NS
it earned a year ago .	NS)))
Upjohn 's biggest-selling drugs are Xanax , a tranquilizer , and Halcion , a sedative .	(NN(NS
Sales of both drugs have been hurt by new state laws	(NN(NS
restricting the prescriptions of certain tranquilizing medicines	NS)
and adverse publicity about the excessive use of the drugs .	NN))
Also , the company 's hair-growing drug , Rogaine , is selling well	(NS(SN(NS
-- at about $ 125 million for the year ,	NS)
but the company 's profit from the drug has been reduced by Upjohn 's expensive print and television campaigns for advertising ,	SN)
analysts said .	NS)))
In Kalamazoo , Mich. , Upjohn declined comment .	NS)))))))))
