Short interest in Nasdaq over-the-counter stocks rose 6 % as of mid-October , its biggest jump since 6.3 % last April .	(NN-Textual-organization(NS-Summary-Evaluation-Topic
The most recent OTC short interest statistics were compiled Oct. 13 , the day	(NS-Elaboration(NS-Elaboration(NS-Elaboration(NS-Elaboration(NS-Elaboration
the Nasdaq composite index slid 3 %	(NN-Joint
and the New York Stock Exchange tumbled 7 % .	NN-Joint))
The coincidence might lead to the conclusion	(SN-Comparison(NS-Summary-Evaluation-Topic(NS-Elaboration
that short-sellers bet heavily on that day	(NS-Attribution
that OTC stocks would decline further .	NS-Attribution))
As it happens ,	(SN-Temporal
the Nasdaq composite did continue to fall for two days after the initial plunge .	SN-Temporal))
However , the short interest figures	(NS-Elaboration(NS-Attribution(NN-Textual-organization(NS-Elaboration
reported by brokerage and securities clearing firms to the National Association of Securities Dealers	NS-Elaboration)
include only those trades	(NS-Elaboration
completed , or settled , by Oct. 13 , rather than trades	(NS-Elaboration
that occurred on that day ,	NS-Elaboration)))
according to Gene Finn , chief economist for the NASD .	NS-Attribution)
Generally , it takes five business days to transfer stock and to take the other steps	(NS-Elaboration
necessary to settle a trade .	NS-Elaboration))))
The total short interest in Nasdaq stocks as of mid-October was 237.1 million shares , up from 223.7 million in September but well below the record level of 279 million shares	(NS-Elaboration(NS-Elaboration
established in July 1987 .	NS-Elaboration)
The sharp rise in OTC short interest compares with the 4.2 % decline in short interest on the New York Stock Exchange and the 3 % rise on the American Stock Exchange during the September-October period .	NS-Elaboration))
Generally , a short seller expects a fall in a stock 's price	(NS-Comparison(NS-Elaboration(SN-Temporal(NN-Joint(NN-Joint
and aims to profit	(NS-Joint
by selling borrowed shares	(NS-Elaboration
that are to be replaced later ;	NS-Elaboration)))
the short seller hopes	(SN-Attribution
the replacement shares	(NN-Textual-organization(NS-Elaboration
bought later	NS-Elaboration)
will cost less than those	(NS-Elaboration
that were sold .	NS-Elaboration))))
Short interest ,	(NN-Textual-organization(NS-Elaboration
which represents the number of shares	(NS-Elaboration
borrowed and sold ,	(NN-Joint
but not yet replaced ,	NN-Joint)))
can be a bad-expectations barometer for many stocks .	NN-Textual-organization))
Among 2,412 of the largest OTC issues , short interest rose to 196.8 million shares , from 185.7 million in 2,379 stocks in September .	(NS-Elaboration
Big stocks with large short interest gains as of Oct. 13 included First Executive , Intel , Campeau and LIN Broadcasting .	(NS-Elaboration(NS-Elaboration
Short interest in First Executive , an insurance issue , rose 55 % to 3.8 million .	(NN-Comparison
Intel 's short interest jumped 42 % ,	(NS-Elaboration(NN-Comparison
while Campeau 's increased 62 % .	NN-Comparison)
Intel makes semiconductors	(NN-Joint
and Campeau operates department-store chains	(NN-Joint
and is strained for cash .	NN-Joint)))))
Meritor Savings again had the dubious honor	(NS-Comparison(NS-Elaboration(NS-Elaboration
of being the OTC stock with the biggest short interest position on Nasdaq .	NS-Elaboration)
Meritor has headed the list since May .	NS-Elaboration)
First Executive and troubled Valley National Corp. of Arizona were next in line .	NS-Comparison))))
Short selling is n't necessarily bad for the overall market .	(NS-Cause
Shorted shares must eventually be replaced	(NS-Elaboration(NN-Joint(NS-Joint
through buying .	NS-Joint)
In addition , changes in short interest in some stocks may be caused by arbitrage .	NN-Joint)
For example , an investor may seek to profit during some takeover situations	(NS-Elaboration(NS-Joint
by buying stock in one company involved	(NN-Joint
and shorting the stock of the other .	NN-Joint))
Two big stocks	(NS-Comparison(NS-Elaboration(NN-Textual-organization(NS-Elaboration
involved in takeover activity	NS-Elaboration)
saw their short interest surge .	NN-Textual-organization)
Short interest in the American depositary receipts of Jaguar , the target of both Ford Motor and General Motors , more than doubled .	NS-Elaboration)
Nasdaq stocks	(NN-Textual-organization(NS-Elaboration
that showed a drop in short interest	NS-Elaboration)
included Adobe Systems , Class A shares of Tele-Communications and takeover targets Lyphomed and Jerrico .	NN-Textual-organization)))))))
The NASD ,	(SN-Temporal(NS-Elaboration(NN-Textual-organization(NS-Elaboration
which operates the Nasdaq computer system	(NS-Elaboration
on which 5,200 OTC issues trade ,	NS-Elaboration))
compiles short interest data in two categories :	NN-Textual-organization)
the approximately two-thirds , and generally biggest , Nasdaq stocks	(NN-Joint(NS-Elaboration
that trade on the National Market System ;	NS-Elaboration)
and the one-third , and generally smaller , Nasdaq stocks	(NS-Elaboration
that are n't a part of the system .	NS-Elaboration)))
Short interest in 1,327 non-NMS securities totaled 40.3 million shares ,	(NN-Joint(NS-Comparison
compared with almost 38 million shares in 1,310 issues in September .	NS-Comparison)
The October short interest represents 1.04 days of average daily trading volume in the smaller stocks in the system for the reporting period ,	(NN-Joint(NS-Comparison
compared with 0.94 day a month ago .	NS-Comparison)
Among bigger OTC stocks , the figures represent 2.05 days of average daily volume ,	(NS-Comparison
compared with 2.14 days in September .	NS-Comparison))))))
The adjacent tables show the issues	(NS-Elaboration(NS-Elaboration
in which a short interest position of at least 50,000 shares existed as of Oct. 13	(NN-Joint
or in which there was a short position change of at least 25,000 shares since Sept. 15	NN-Joint))
( see accompanying tables	(NS-Attribution
-- WSJ Oct. 25 , 1989 ) .	NS-Attribution)))
