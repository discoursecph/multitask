Albert Engelken and Robert Thomson had never met, though for 38 years their lives had been intertwined in a way peculiar to the sports world.	O	9..150
Mr. Engelken, now a transit-association executive in Washington, D.C., and Mr. Thomson, a paper-products salesman in Montvale, N.J., hadn't even talked to each other.	B-concession	151..317
But one recent day, they became much closer.	I-concession	318..362
Mr. Engelken, a rabid baseball fan, pores over the sports pages to chart the exploits of "my favorite and not-so-favorite teams and players.	B-entrel	365..506
He often groans, he says, at the "clutter" of sports stories about drugs, alcohol, gambling and some player's lament "about the miserly millions he is offered to play the game."	I-entrel	507..684
He often groans, he says, at the "clutter" of sports stories about drugs, alcohol, gambling and some player's lament "about the miserly millions he is offered to play the game." His morning paper, the Washington Post, even carries a sports column called "Jurisprudence" that recounts the latest arrests and convictions of players and team managers.	B-cause	685..855
Like many sports buffs, Mr. Engelken has turned cynic.	B-concession	856..910
But his is a story about a hero in an era of sports anti-heroes, and about what Babe Ruth, Mr. Engelken reminds us, once called "the only real game in the world."	I-concession	913..1075
To Mr. Engelken, it is also a story "about love, because I'm blessed to have a wife who still thinks her slightly eccentric husband's 50th birthday deserves the ultimate present."	O	1076..1255
To understand what Mr. Engelken means, one must go back to a sunny October afternoon in 1951 at New York's Polo Grounds stadium, where, it can be argued, the most dramatic moment in baseball history was played out.	O	1258..1472
It was the ninth inning of the third game of a three-game playoff between the Brooklyn Dodgers and the New York Giants (the predecessor to the San Francisco Giants scheduled to play in tonight's world series).	O	1475..1684
Baseball fans throughout New York had sweated out a long summer with their teams, and now it had come to this: a battle between the two for the National League pennant -- down to the last inning of the last game, no less.	O	1685..1906
Some 34,320 fans jammed the stands, and shouted at the top of their lungs.	B-entrel	1909..1983
Mr. Engelken was doing the same across the Hudson River in New Jersey, where, with his nose pressed against the front window of the Passaic-Clifton National Bank, he watched the duel on a television set the bank set up for the event.	I-entrel	1984..2217
The playoff series had riveted the 12-year-old Giants fan.	B-entrel	2220..2278
"The Giants struck first, winning the opener, 3-1, on a two-run homer off Dodger right-hander Ralph Branca," Mr. Engelken recalls with precision today.	I-entrel	2279..2430
The Giants got swamped in the second game, 100, and trailed 4-1 going into the bottom of the ninth of the third and deciding game.	B-asynchronous	2431..2562
The Giants scored once and had runners on second {Whitey Lockman} and third {Clint Hartung} as Bobby Thomson advanced to the plate."	I-asynchronous	2563..2695
The rest, as they say, is history.	B-restatement	2698..2732
Mr. Thomson, a tall, Scottish-born, right-hand hitter, stepped into the batter's box.	I-restatement	2733..2818
"Thomson took a called strike," Mr. Engelken recounts.	O	2819..2873
The tension mounted as Ralph Branca, again on the mound, stared down the batter.	B-asynchronous	2874..2954
He wound up and let loose a fastball.	B-entrel	2955..2992
The pitch sailed toward Bobby Thomson high and inside and then, with a crack of the bat, was sent rocketing back into the lower leftfield stands.	I-entrel	2993..3138
"Giants fans went into euphoria," says Mr. Engelken.	O	3139..3191
And Bobby Thomson was made a legend.	B-entrel	3194..3230
The same Bobby Thomson, it turns out, who sells those paper-goods today.	I-entrel	3231..3303
There can't be an older baseball fan alive who doesn't clearly remember that Bobby Thomson homer, who can't tell you where he was when he heard the famous Russ Hodges radio broadcast -- the one that concluded with Mr. Hodges shouting, over and over, "The Giants win the pennant, the Giants win the pennant!"	O	3306..3613
Mr. Engelken and Mr. Thomson drifted in different directions in the subsequent years, and the Polo Grounds, located under Coogan's Bluff in upper Manhattan, was replaced by a public-housing project.	O	3616..3814
Mr. Thomson played outfield and third base until 1960, posting a lifetime .270 batting average and chalking up 264 home runs before retiring and going into paper-goods sales.	O	3815..3989
Mr. Engelken moved south to Washington, but he took with him enduring memories of the homer of 1951.	B-instantiation	3992..4092
When his wife, Betsy, came down the aisle on their wedding day in 1966, Mr. Engelken -- no slouch on the romantic front -- gave her the ultimate compliment: "You look prettier than Bobby Thomson's home run."	I-instantiation	4093..4300
The couple's first dog, Homer, was named after the Great Event, though unwitting friends assumed he was the namesake of the poet.	O	4301..4430
And when Mr. Engelken's sister, Martha, who was born two days before the home run, reached her 25th birthday, Mr. Engelken wrote his sports hero to tell him of the coincidence of events.	O	4433..4619
Mr. Thomson sent off a card to Martha: "It doesn't seem like 25 years since I hit that home run to celebrate your birth," it read.	B-entrel	4620..4750
Martha was pleased, but nowhere near as much as Mr. Engelken.	I-entrel	4751..4812
The family license plate reads "ENG 23," the first three letters of the family name and -- no surprise here -- Bobby Thomson's uniform number.	B-conjunction	4815..4957
And on Mr. Engelken's 40th birthday, his wife bought a book detailing the big homer and sent it off to Mr. Thomson to be autographed.	B-entrel	4958..5091
"What could have been better?" asks Mr. Engelken.	I-entrel	5092..5141
Betsy Engelken asked the same question earlier this year, when her husband was about to turn 50.	B-asynchronous	5144..5240
She had an idea.	I-asynchronous	5241..5257
On her husband's 50th birthday (after an auspicious 23 years of marriage, it should be noted), Betsy, Al and their college-bound son set out for New York to visit Fordham University.	B-entrel	5260..5442
Mrs. Engelken had scheduled a stop on the New Jersey Turnpike to, she told her husband, pick up some papers for a neighbor.	B-restatement	5443..5566
The papers would be handed over at a bank of telephone booths just off Exit 10.	I-restatement	5567..5646
"It sounded like something out of Ian Fleming," Mr. Engelken recalls.	O	5649..5718
At the appointed exit, the family pulled over, and Mrs. Engelken went to get her papers.	B-entrel	5721..5809
Mr. Engelken turned off the motor and rolled down the window.	I-entrel	5810..5871
In a matter of minutes, she was back, with a tall, silver-haired man in tow.	B-asynchronous	5874..5950
She crouched down by the car window and addressed her husband with her favorite nickname:	I-asynchronous	5951..6040
She crouched down by the car window and addressed her husband with her favorite nickname: "Bertie," she said, "Happy 50th Birthday.	B-entrel	6041..6082
This is Bobby Thomson."	I-entrel	6083..6106
"And there he was," recalls Mr. Engelken.	O	6109..6150
"The hero of my youth, the one person in history I'd most like to meet.	O	6151..6222
Keep your Thomas Jeffersons, or St. Augustines or Michelangelos; I'd take baseball's Flying Scot without hesitation."	O	6223..6340
They talked of the home run.	B-instantiation	6343..6371
I thought it was in the upper deck," said Bobby Thomson, now 66 years old.	B-entrel	6372..6447
They talked of the aftermath.	B-instantiation	6448..6477
I never thought it would become so momentous," Bobby remarked.	B-entrel	6478..6541
Mr. Engelken, says his wife, "was overwhelmed by the whole thing.	I-entrel	6542..6607
It was worth it, just for the look on Albert's face."	O	6608..6661
The two men spent an hour at Exit 10, rehashing the event, "fulfilling the lifelong dream of a young boy now turned 50," Mr. Engelken says.	O	6664..6803
His hero signed photographs of the homer and diplomatically called Ralph Branca "a very fine pitcher.	B-entrel	6804..6906
His hero signed photographs of the homer and diplomatically called Ralph Branca "a very fine pitcher.	B-conjunction	6804..6906
And when Mr. Engelken asked him why he took time off from work for somebody he didn't even know, Bobby Thomson replied: "You know, Albert, if you have the chance in life to make somebody this happy, you have an obligation to do it."	I-conjunction	6907..7139
In an interview, Mr. Thomson, who is married and has three grown children, says he has few ties to baseball these days, other than playing old-timers games now and again.	B-concession	7142..7312
But his fans, to his constant amazement, never let him forget the famous four-bagger.	B-restatement	7313..7398
His mail regularly recalls "my one event," and has been growing in recent years.	I-restatement	7399..7479
In response to the letters, Mr. Thomson usually sends an autographed photo with a polite note, and rarely arranges a rendezvous.	B-concession	7482..7610
But when Betsy Engelken wrote him, saying she could stop near his New Jersey home, it seemed different.	B-entrel	7611..7714
"What a good feeling it would be for me to do that," he says he thought.	I-entrel	7715..7787
When the Engelken family got back from its trip up north, Mr. Engelken wrote it all down, just to make sure no detail was missed.	B-entrel	7790..7919
"On the way home," his notes recall, "it took concentrated effort to keep that car pointed south.	I-entrel	7920..8017
My mind was miles north at a place called Coogan's Bluff, where a real sports hero had captured the imagination of a kid who never fully grew up and is all the richer for it.	O	8018..8192
"Take heart, sports fans," he wrote.	O	8195..8231
Real heroes exist.	B-contrast	8232..8251
You might not find one in the `Jurisprudence' column.	B-contrast	8252..8305
But who knows?	I-contrast	8306..8320
You might meet up with him at that bank of telephone booths just off Exit 10 of the New Jersey Turnpike.	O	8321..8425
