Wham ! Bam !	O	9..19
Twice in two weeks the unraveling of the on-again , off-again UAL buy-out slammed the stock market .	O	20..118
Now , stock prices seem to be in a general retreat .	B-expansion	121..171
Since peaking at 2791.41 on Oct. 9 , the Dow Jones Industrial Average has lost 194.69 points , or 7 % , closing Friday at 2596.72 , down 17.01 .	B-expansion	172..310
The number of issues falling on the New York Stock Exchange each day is eclipsing the number of gainers .	B-expansion	311..415
And the number of stocks hitting new lows far outstrips the number setting new highs .	I-expansion	416..501
But why should an iffy $ 6.79 billion leveraged buy-out deal shake the foundations of the entire stock market ?	O	504..613
Opinions vary about how important the UAL deal was to the market 's health , but analysts generally agree that the market gyrations created as the UAL plan crumbled revealed a fundamental change in investor psychology .	O	616..832
" If this had happened a few months ago when the atmosphere was still very positive it would n't have been greeted with anything like the impact it has had over the past two weeks , " says Dennis Jarrett , a market strategist at Kidder Peabody .	O	835..1074
There are , of course , analysts who view the near-panic that briefly swept through investors on Oct. 13 and again on Oct. 24 as momentary lapses of good judgment that have only temporarily undermined a healthy stock market .	O	1077..1299
Sure , price action is volatile and that 's scary , but all-in-all stocks are still a good place to be , they suggest .	O	1300..1414
The reaction to the UAL debacle " is mindless , " says John Connolly , chief market strategist at Dean Witter .	O	1417..1523
" UAL is a small deal as far as the overall market is concerned .	B-expansion	1524..1587
The only way you can make it a big deal is to draw linkages that just do n't make sense . "	I-expansion	1588..1676
He suggests , for example , that investors may have assumed that just because UAL could n't get financing , no leveraged buy-outs can get financing .	O	1677..1821
Carried even further , some investors assumed that since leveraged buy-outs are the only thing propping up stock prices , the market would collapse if no more LBOs could be done .	O	1822..1998
" There will still be deals , " argues Mr. Connolly .	O	2001..2050
" There may not be as many and the buyers may not get away with some of the things they 've done in the past , but deals wo n't disappear . "	O	2051..2186
He forecasts that the emphasis in mergers and acquisitions may soon return to what he calls " strategic deals , in which somebody is taking over a company not to milk the cash flow , but because it 's a good fit . "	O	2187..2396
And even without deals , Mr. Connolly figures the market would remain healthy .	O	2399..2476
He notes , for instance , that there has n't been a merger or acquisition among the 30 stocks in the Dow Jones Industrial Average since 1986 , yet that average only three weeks ago hit a record high .	O	2477..2672
" Those stocks are up because their earnings are up and their dividends are up , " he says .	O	2675..2763
Even the volatility created by stock index arbitrage and other computer-driven trading strategies is n't entirely bad , in Mr. Connolly 's view .	B-contingency	2766..2907
For the long-term investor who picks stocks carefully , the price volatility can provide welcome buying opportunities as short-term players scramble frantically to sell stocks in a matter of minutes .	I-contingency	2908..3106
" Who can make the better decision , the guy who has 10 seconds to decide what to do or the guy with all the time in the world ? " he says .	O	3107..3242
" What on earth does the UAL deal have to do with the price of Walmart , which I was able to buy on Oct. 16 at a very attractive price ? "	O	3243..3377
Kidder Peabody 's Mr. Jarrett also sees some benefits to the stock market 's recent drop .	O	3380..3467
" We 've run into a market that was beginning to run out of steam and get frothy , " he says .	O	3468..3557
" The balloon had been blown up so big that when somebody came along with a pin -- in this case the UAL deal -- we got a little pop . "	O	3558..3690
The pop sobered up investors who had been getting a little too ebullient , says Mr. Jarrett .	O	3693..3784
" It provided an excuse for people to get back to reality and to look at the economic data , especially the third-quarter economic numbers , and to realize that we ca n't continue to gloss over what is going on in the junk bond market . "	O	3785..4017
But he figures that at current levels the stock market is comfortably valued , even with the economy obviously slowing .	O	4020..4138
" Just because we 've got some realism back in the market does n't mean it 's going lower from here , " he says .	O	4139..4245
" The bottom line is that it 's healthy to have this kind of sideways activity , especially after a 30 % gain in stock values over the past 12 months . "	O	4246..4393
He 's now estimating that after a period of consolidation , the Dow Jones Industrial Average will once again forge new highs .	O	4394..4517
Maybe , maybe not .	O	4520..4537
Abby Joseph Cohen , a market strategist at Drexel Burnham Lambert , is n't nearly so sanguine about the market 's chances of surging to new highs anytime soon .	B-expansion	4538..4693
Her view is that stock prices have three major props : merger and buy-out proposals , earnings and the economic outlook .	I-expansion	4694..4812
At current levels of economic activity and earnings , stocks are fairly valued , she says .	O	4813..4901
But any chance for prices to surge above fair value lies in the speculation that accompanies a vigorous merger and buy-out business , and UAL has obviously put a damper on that .	B-entrel	4902..5078
" Stocks are n't cheap anymore , there have been some judicial and legislative changes in the merger area and all of this changes the arithmetic of deals , " she says .	I-entrel	5079..5241
" I 'm not saying they 've stopped altogether , but future deals are going to be structured differently and bids probably wo n't be as high . "	O	5242..5378
But that 's not the only problem for stocks .	O	5381..5424
The other two props -- earnings and the economic outlook -- are troubling , too .	B-entrel	5425..5504
" M&A is getting all the headlines right now , but these other things have been building up more gradually , " she says .	I-entrel	5505..5621
Third-quarter earnings have been generally disappointing and with economic data showing a clear slowing , the outlook for earnings in the fourth quarter and all of 1990 is getting worse .	O	5622..5807
" There are a lot more downward than upward revisions and it looks like people are questioning corporate profits as a means of support for stock prices , " she says .	O	5810..5972
With all this , can stock prices hold their own ?	O	5975..6022
" The question is unanswerable at this point , " she says .	O	6023..6078
" It depends on what happens .	B-expansion	6079..6107
If the economy slips into a recession , then this is n't a level that 's going to hold . "	I-expansion	6108..6193
Friday 's Market Activity	O	6196..6220
Stock prices tumbled for a third consecutive day as earnings disappointments , a sluggish economy and a fragile junk bond market continued to weigh on investors .	O	6223..6383
The Dow Jones Industrial Average fell 17.01 points to 2596.72 in active trading .	B-expansion	6386..6466
Volume on the New York Stock Exchange totaled 170,330,000 shares .	B-expansion	6467..6532
Declining issues on the Big Board were far ahead of gainers , 1,108 to 416 .	B-expansion	6533..6607
For the week the Dow Jones Industrial Average sank 92.42 points , or 3.4 % .	I-expansion	6608..6681
Oil stocks escaped the brunt of Friday 's selling and several were able to post gains , including Chevron , which rose 5/8 to 66 3/8 in Big Board composite trading of 2.4 million shares .	B-expansion	6684..6867
The stock 's advance reflected ongoing speculation that Pennzoil is accumulating a stake in the company , according to Dow Jones Professional Investor Report .	I-expansion	6868..7024
Both companies declined to comment on the rumors , but several industry analysts told the Professional Investor Report they believed it was plausible that Pennzoil may be buying Chevron shares as a prelude to pushing for a restructuring of the company .	O	7027..7278
USX gained 1/2 to 33 3/8 on a report in Business Week magazine that investor Carl Icahn is said to have raised his stake in the oil and steel company to just about 15 % .	B-expansion	7281..7449
Earlier this month , Mr. Icahn boosted his USX stake to 13.4 % .	I-expansion	7450..7511
Elsewhere in the oil sector , Exxon rallied 7/8 to 45 3/4 ; Amoco rose 1/8 to 47 ; Texaco was unchanged at 51 3/4 , and Atlantic Richfield fell 1 5/8 to 99 1/2 .	O	7514..7670
Mobil , which said it plans to cut its exploration and production work force by about 8 % in a restructuring , dropped 5/8 to 56 1/8 .	O	7671..7801
The precious metals sector outgained other Dow Jones industry groups by a wide margin for the second consecutive session .	O	7804..7925
Hecla Mining rose 5/8 to 14 ; Battle Mountain Gold climbed 3/4 to 16 3/4 ; Homestake Mining rose 1 1/8 to 16 7/8 ; Lac Minerals added 5/8 to 11 ; Placer Dome went up 7/8 to 16 3/4 , and ASA Ltd. jumped 3 5/8 to 49 5/8 .	O	7926..8139
Gold mining stocks traded on the American Stock Exchange also showed strength .	O	8142..8220
Echo Bay Mines rose 5/8 to 15 7/8 ; Pegasus Gold advanced 1 1/2 to 12 , and Corona Class A gained 1/2 to 7 1/2 .	O	8221..8330
Unisys dropped 3/4 to 16 1/4 after posting a third-quarter loss of $ 4.25 a share , including restructuring charges , but other important technology issues were mixed .	O	8333..8497
Compaq Computer , which had lost 8 5/8 Thursday following a disappointing quarterly report , gained 5/8 to 100 5/8 .	B-expansion	8500..8613
International Business Machines dropped 7/8 to 99 7/8 .	B-expansion	8614..8668
Digital Equipment tacked on 1 1/8 to 89 1/8 , and Hewlett-Packard fell 3/8 to 49 3/8 .	I-expansion	8669..8753
Dividend-related trading swelled volume in Merrill Lynch , which closed unchanged at 28 3/8 as 2.7 million shares changed hands .	B-expansion	8756..8883
The stock has a 3.5 % dividend yield and goes ex-dividend today .	I-expansion	8884..8947
Erbamont advanced 1 1/8 to 36 1/2 on 1.9 million shares .	B-expansion	8950..9006
Montedison , which owns about 72 % of the company 's common stock , agreed to buy the rest for $ 37 a share .	B-expansion	9007..9110
Himont , another majority-owned unit of Montedison , added 1 1/4 to 47 1/8 .	I-expansion	9111..9184
Milton Roy jumped 2 to 18 3/8 .	B-expansion	9187..9217
Crane said it holds an 8.9 % stake in the company and may seek control .	B-entrel	9218..9288
Crane dropped 1 1/8 to 21 1/8 .	I-entrel	9289..9319
Comprehensive Care , which terminated its agreement to merge with First Hospital , dropped 7/8 to 3 7/8 .	B-entrel	9322..9424
The company 's decision was made after First Hospital failed to obtain financing for its offer .	I-entrel	9425..9519
