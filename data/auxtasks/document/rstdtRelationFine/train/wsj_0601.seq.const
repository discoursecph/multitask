In an age of specialization , the federal judiciary is one of the last bastions of the generalist .	(elaboration-additional(Contrast(explanation-argumentative
A judge must jump from murder to antitrust cases , from arson to securities fraud ,	(manner
without missing a beat .	manner))
But even on the federal bench , specialization is creeping in ,	(elaboration-additional
and it has become a subject of sharp controversy on the newest federal appeals court .	elaboration-additional))
The Court of Appeals for the Federal Circuit was created in 1982	(background(purpose
to serve , among other things , as the court of last resort for most patent disputes .	purpose)
Previously , patent cases moved through the court system to one of the 12 circuit appeals courts .	(elaboration-additional
There , judges	(Problem-Solution(Same-Unit(elaboration-object-attribute
who saw few such cases	(List
and had no experience in the field	List))
grappled with some of the most technical and complex disputes imaginable .	Same-Unit)
A new specialty court was sought by patent experts ,	(elaboration-additional(elaboration-additional
who believed	(attribution
that the generalists had botched too many important , multimillion-dollar cases .	attribution))
Some patent lawyers had hoped	(background(Contrast(attribution
that such a specialty court would be filled with experts in the field .	attribution)
But the Reagan administration thought otherwise ,	(List
and so may the Bush administration .	List))
Since 1984 , the president has filled four vacancies in the Federal Circuit court with non-patent lawyers .	(Problem-Solution(elaboration-additional(elaboration-additional
Now only three of the 12 judges	(elaboration-additional(Same-Unit(elaboration-set-member
-- Pauline Newman , Chief Judge Howard T. Markey , 68 , and Giles Rich , 85 --	elaboration-set-member)
have patent-law backgrounds .	Same-Unit)
The latter two and Judge Daniel M. Friedman , 73 , are approaching senior status or retirement .	elaboration-additional))
Three seats currently are vacant	(List
and three others are likely to be filled within a few years ,	List))
so patent lawyers and research-based industries are making a new push	(elaboration-additional(elaboration-object-attribute
for specialists to be added to the court .	elaboration-object-attribute)
Several organizations ,	(elaboration-additional(elaboration-additional(Same-Unit(elaboration-set-member
including the Industrial Biotechnical Association and the Pharmaceutical Manufacturers Association ,	elaboration-set-member)
have asked the White House and Justice Department to name candidates with both patent and scientific backgrounds .	Same-Unit)
The associations would like the court to include between three and six judges with specialized training .	elaboration-additional)
Some of the associations have recommended Dr. Alan D. Lourie , 54 , a former patent agent with a doctorate in organic chemistry	(elaboration-additional(elaboration-additional(elaboration-additional
who now is associate general counsel with SmithKline Beckman Corp. in Philadelphia .	elaboration-additional)
Dr. Lourie says	(attribution
the Justice Department interviewed him last July .	attribution))
Their effort has received a lukewarm response from the Justice Department .	(Contrast(example(explanation-argumentative
`` We do not feel	(attribution(attribution
that seats are reserved	(elaboration-additional
( for patent lawyers ) , ''	elaboration-additional))
says Justice spokesman David Runkel ,	(elaboration-additional
who declines to say	(attribution
how soon a candidate will be named .	(elaboration-additional
`` But we will take it into consideration . ''	elaboration-additional)))))
The Justice Department 's view is shared by other lawyers and at least one member of the court , Judge H. Robert Mayer , a former civil litigator	(explanation-argumentative(temporal-before(elaboration-object-attribute
who served at the claims court trial level	elaboration-object-attribute)
before he was appointed to the Federal Circuit two years ago .	temporal-before)
`` I believe	(elaboration-additional(attribution(attribution
that any good lawyer should be able to figure out and understand patent law , ''	attribution)
Judge Mayer says ,	attribution)
adding	(elaboration-additional
that `` it 's the responsibility of highly paid lawyers	(Same-Unit(elaboration-object-attribute
( who argue before the court )	elaboration-object-attribute)
to make us understand	(elaboration-additional
( complex patent litigation ) . ''	elaboration-additional))))))
Yet some lawyers point to Eli Lilly & Co. vs. Medtronic , Inc. , the patent infringement case the	(elaboration-additional(elaboration-additional(elaboration-additional(explanation-argumentative
Supreme Court this month agreed to review , as an example of poor legal reasoning by judges	(elaboration-object-attribute
who lack patent litigation experience .	elaboration-object-attribute))
( Judge Mayer was not on the three-member panel . )	elaboration-additional)
In the Lilly case , the appeals court broadly construed a federal statute	(elaboration-additional(elaboration-object-attribute
to grant Medtronic , a medical device manufacturer , an exemption	(purpose
to infringe a patent under certain circumstances .	purpose))
If the Supreme Court holds in Medtronic 's favor ,	(condition
the decision will have billion-dollar consequences for the manufacturers of medical devices , color and food additives and all other non-drug products	(elaboration-object-attribute
that required Food & Drug Administration approval .	elaboration-object-attribute))))
Lisa Raines , a lawyer and director of government relations for the Industrial Biotechnical Association , contends	(elaboration-additional(elaboration-additional(attribution
that a judge well-versed in patent law and the concerns of research-based industries would have ruled otherwise .	attribution)
And Judge Newman , a former patent lawyer , wrote in her dissent	(attribution
when the court denied a motion for a rehearing of the case by the full court ,	(Cause-Result
`` The panel 's judicial legislation has affected an important high-technological industry ,	(manner
without regard to the consequences for research and innovation or the public interest . ''	manner))))
Says Ms. Raines ,	(attribution
`` ( The judgment )	(Same-Unit
confirms our concern	(elaboration-object-attribute
that the absence of patent lawyers on the court could prove troublesome . ''	elaboration-object-attribute))))))))))))))))
