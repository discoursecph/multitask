Digital Equipment Corp. is planning a big coming-out party on Tuesday for its first line of mainframe computers .	root	9..121
But an uninvited guest is expected to try to crash the party .	contrast	122..183
On the morning of the long-planned announcement , International Business Machines Corp. is to introduce its own new mainframe .	norel	186..311
" Their attitude is , ' You want to talk mainframes , we 'll talk mainframes , ' " says one computer industry executive .	restatement	312..425
" They 're deliberately trying to steal our thunder , " a Digital executive complains .	norel	428..510
" Maybe we should take it as a compliment . "	conjunction	511..553
Digital 's target is the $ 40 billion market for mainframe computers , the closet-sized number-crunchers that nearly every big company needs to run its business .	norel	556..714
IBM , based in Armonk , N.Y. , has dominated the market for decades .	entrel	715..780
That does n't scare Digital , which has grown to be the world 's second-largest computer maker by poaching customers of IBM 's mid-range machines .	norel	783..925
Digital , based in Maynard , Mass. , hopes to stage a repeat performance in mainframes	restatement	926..1009
and it has spent almost $ 1 billion developing the new technology .	conjunction	1010..1076
A spoiler , nimble Tandem Computers Inc. in Cupertino , Calif. , jumped into the fray earlier this week with an aggressively priced entry .	conjunction	1077..1212
IBM appears more worried about Digital , which has a broad base of customers waiting for the new line , dubbed the VAX 9000 .	norel	1215..1337
" It 's going to be nuclear war , " says Thomas Willmott , a consultant with Aberdeen Group Inc .	cause	1338..1429
The surge in competition is expected to stir new life into the huge mainframe market , where growth has slowed to single digits in recent years .	norel	1432..1575
IBM 's traditional mainframe rivals , including Unisys Corp. , Control Data Corp. and NCR Corp. , have struggled recently .	restatement	1576..1694
Digital is promising a new approach .	norel	1697..1733
Robert M. Glorioso , Digital 's vice president for high performance systems , says Digital 's mainframe is designed not as a central computer around which everything revolves , but as part of a decentralized network weaving together hundreds of workstations , personal computers , printers and other devices .	restatement	1734..2035
And unlike IBM 's water-cooled mainframes , it does n't need any plumbing .	conjunction	2036..2107
The challengers will have a big price advantage .	norel	2110..2158
Digital is expected to tag its new line from about $ 1.24 million to $ 4.4 million and up , depending on configuration .	instantiation	2159..2275
That 's about half the price of comparably equipped IBM mainframes .	contrast	2276..2342
Tandem 's pricing is just as aggressive .	conjunction	2343..2382
The heightened competition will hit IBM at a difficult time .	norel	2385..2445
The computer giant 's current mainframe line , which has sold well and has huge profit margins , is starting to show its age .	cause	2446..2568
The new 3090s due next week will boost performance by only about 8 % to 10 % .	contrast	2569..2644
And IBM is n't expected to deliver a new generation of mainframes until 1991 .	conjunction	2645..2721
Still , no one expects IBM 's rivals to deliver a knockout .	norel	2724..2781
IBM has a near-monopoly on mainframes , with an estimated 70 % share of the market .	cause	2782..2863
IBM is five times the size of Digital -- and 40 times the size of Tandem -- and wields enormous market power .	conjunction	2864..2973
It counts among its customers a majority of the world 's largest corporations , which entrust their most critical business information to IBM computers .	conjunction	2974..3124
" We 're not going to walk in and replace a company 's corporate accounting system if it 's already running on an IBM mainframe , " concedes Kenneth H. Olsen , Digital 's president .	norel	3127..3300
He says Digital will target faster-growing market segments such as on-line transaction processing , which includes retail-sales tracking , airline reservations and bank-teller networks .	norel	3303..3486
Tandem , which already specializes in on-line transaction processing , is a potent competitor in that market .	entrel	3487..3594
A key marketing target for Digital will be the large number of big customers who already own both Digital and IBM systems .	norel	3597..3719
One such company is Bankers Trust Co .	instantiation	3720..3757
Stanley Rose , a vice president , technological and strategic planning at Bankers Trust , says that despite Digital 's low prices , " we are n't about to unplug our IBM mainframes for a DEC machine .	contrast	3758..3949
The software conversion costs would dwarf any savings . "	cause	3950..4005
But Mr. Rose is still looking seriously at the 9000 .	concession	4008..4060
Bankers Trust uses Digital 's VAX to run its huge money-transfer and capital markets accounts , juggling hundreds of billions of dollars each day , he says .	entrel	4061..4214
As that system grows , larger computers may be needed .	contrast	4215..4268
" In the past , customers had to go to IBM when they outgrew the VAX .	asynchronous	4269..4336
Now they do n't have to , " he says .	contrast	4337..4370
" That 's going to cost IBM revenue . "	cause	4371..4406
Analysts say Digital can expect this pent-up demand for the new VAX to fuel strong sales next year .	norel	4409..4508
Barry F. Willman , an analyst at Sanford C. Bernstein & Co. , estimates the 9000 could boost sales by more than $ 1 billion in the fiscal year beginning in July .	restatement	4509..4667
He bases the estimate on a survey of hundreds of Digital 's largest customers .	entrel	4668..4745
Although Digital will announce a full family of mainframes next week , it is n't expected to begin shipping in volume until next year .	norel	4748..4880
The first model available will be the 210 , which is likely to appeal to many technical and scientific buyers interested in the optional super-charger , or vector processor , says Terry Shannon of International Data Corp. , a market research concern .	restatement	4881..5127
Four more models , aimed squarely at IBM 's commercial customers , are expected to begin shipping in late June .	norel	5130..5238
Most analysts do n't expect the new mainframes to begin contributing significantly to revenue before the fiscal first quarter , which begins next July 1 .	contrast	5239..5390
Digital 's new line has been a long time coming .	norel	5393..5440
The company has long struggled to deliver a strong mainframe-class product , and made a costly decision in 1988 to halt development of an interim product meant to stem the revenue losses at the high end .	cause	5441..5643
Digital 's failure to deliver a true mainframe-class machine before now may have cost the company as much as $ 1 billion in revenue in fiscal 1989 , Mr. Willman says .	restatement	5644..5807
IBM will face still more competition in coming months .	norel	5810..5864
Amdahl Corp. , backed by Japan 's Fujitsu Ltd. , has a growing share of the market with its low-priced , IBM-compatible machines .	instantiation	5865..5990
And National Advanced Systems , a joint venture of Japan 's Hitachi Ltd. and General Motors Corp. 's Electronic Data Systems , is expected to unveil a line of powerful IBM-compatible mainframes later this year .	conjunction	5991..6198
NOTE :	norel	6201..6206
NAS is National Advanced Systems , CDC -- Control Data Corp. , Bull NH Information Systems Inc .	norel	6207..6302
Source : International Data Corp .	norel	6305..6337
Compiled by Publishers Weekly from data from large-city bookstores , bookstore chains and local bestseller lists across the U.S.	norel	6340..6467
Copyright 1989 by Reed Publishing USA .	norel	6468..6506
