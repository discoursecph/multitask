Just when it seemed safe to go back into stocks , Wall Street suffered another severe attack of nerves .	ROOT	0
Does this signal another Black Monday is coming ?	COREF_PREV	1
Or is this an extraordinary buying opportunity , just like Oct. 19 , 1987 , eventually turned out to be ?	COREF_PREV	2
Here 's what several leading market experts and money managers say about Friday 's action , what happens next and what investors should do .	NONE	3
Joseph Granville .	NONE	4
`` I 'm the only one who said there would be an October massacre , all through late August and September , '' says Mr. Granville , once a widely followed market guru and still a well - known newsletter writer .	COREF_PREV	5
`` Everyone will tell you that this time is different from 1987 , '' he says .	COREF_PREV	6
`` Well , in some ways it is different , but technically it is just the same .	COREF_PREV	7
If you 're a technician , you obey the signals .	NONE	8
Right now they 're telling me to get the hell out and stay out .	COREF_PREV	9
I see no major support until 2200 .	COREF_PREV	10
I see a possibility of going to 2200 this month . ''	COREF_PREV	11
Mr. Granville says he would n't even think of buying until at least 600 to 700 stocks have hit 52 - week lows ; about 100 stocks hit new lows Friday .	COREF_PREV	12
`` Most people , '' he says , `` have no idea what a massacre pattern looks like . ''	COREF_PREV	13
Elaine Garzarelli .	NONE	14
A quantitative analyst with Shearson Lehman Hutton Inc. , Ms. Garzarelli had warned clients to take their money out of the market before the 1987 crash .	NONE	15
Friday 's big drop , she says , `` was not a crash .	COREF_OTHER	16
This was an October massacre '' like those that occurred in 1978 and 1979 .	COREF_PREV	17
Now , as in those two years , her stock market indicators are positive .	COREF_OTHER	18
So she thinks the damage will be short - lived and contained .	COREF_PREV	19
`` Those corrections lasted one to four weeks and took the market 10%-12 % down , '' she says .	COREF_OTHER	20
`` This is exactly the same thing , as far as I 'm concerned . ''	COREF_OTHER	21
Thus , she says , if the Dow Jones Industrial Average dropped below 2450 , `` It would just be a fluke .	COREF_PREV	22
My advice is to buy . ''	COREF_PREV	23
As she calculates it , the average stock now sells for about 12.5 times companies ' earnings .	COREF_PREV	24
She says that ratio could climb to 14.5 , given current interest rates , and still be within the range of `` fair value . ''	COREF_PREV	25
Ned Davis .	NONE	26
Friday 's fall marks the start of a bear market , says Mr. Davis , president of Ned Davis Research Inc .	COREF_OTHER	27
But Mr. Davis , whose views are widely respected by money managers , says he expects no 1987 - style crash .	COREF_PREV	28
`` There was a unique combination in 1987 , '' he says .	NONE	29
`` Margin debt was at a record high .	COREF_OTHER	30
There was tremendous public enthusiasm for stock mutual funds .	NONE	31
The main thing was portfolio insurance , '' a mechanical trading system intended to protect an investor against losses . ``	NONE	32
A hundred billion dollars in stock was subject '' to it .	NONE	33
In 1987 , such selling contributed to a snowball effect .	COREF_OTHER	34
Today could even be an up day , Mr. Davis says , if major brokerage firms agree to refrain from program trading .	COREF_OTHER	35
Over the next several months , though , he says things look bad .	NONE	36
`` I think the market will be heading down into November , '' he says .	COREF_OTHER	37
`` We will probably have a year - end rally , and then go down again .	COREF_PREV	38
Sort of a two - step bear market . ''	NONE	39
He expects the downturn to carry the Dow Jones Industrial Average down to around 2000 sometime next year .	COREF_PREV	40
`` That would be a normal bear market , '' he says .	NONE	41
`` I guess that 's my forecast . ''	COREF_OTHER	42
Leon G. Cooperman .	COREF_PREV	43
`` I do n't think the market is going through another October '87 .	COREF_PREV	44
I do n't think that 's the case at all , '' says Mr. Cooperman , a partner at Goldman , Sachs -AMP- Co. and chairman of Goldman Sachs Asset Management .	COREF_PREV	45
Mr. Cooperman sees this as a good time to pick up bargains , but he does n't think there 's any need to rush .	COREF_PREV	46
`` I expect the market to open weaker Monday , but then it should find some stability . ''	COREF_PREV	47
He ticks off several major differences between now and two years ago .	COREF_PREV	48
Unlike 1987 , interest rates have been falling this year .	COREF_OTHER	49
Unlike 1987 , the dollar has been strong .	NONE	50
And unlike 1987 , the economy does n't appear to be in any danger of overheating .	COREF_OTHER	51
But the economy 's slower growth this year also means the outlook for corporate profits `` is n't good , '' he says .	COREF_PREV	52
`` So it 's a very mixed bag . ''	NONE	53
Thus , he concludes , `` This is not a good environment to be fully invested '' in stocks .	COREF_OTHER	54
`` If I had come into Friday on margin or with very little cash in the portfolios , I would not do any buying .	COREF_PREV	55
But we came into Friday with a conservative portfolio , so I would look to do some modest buying '' on behalf of clients . ``	COREF_PREV	56
We 're going to look for some of the better - known companies that got clocked '' Friday .	COREF_PREV	57
John Kenneth Galbraith .	NONE	58
`` This is the latest manifestation of the capacity of the financial community for recurrent insanity , '' says Mr. Galbraith , an economist .	COREF_OTHER	59
`` I see this as a reaction to the whole junk bond explosion , '' he says .	COREF_PREV	60
`` The explosion of junk bonds and takeovers has lodged a lot of insecure securities in the hands of investors and loaded the corporations that are the objects of takeovers or feared takeovers with huge amounts of debt rather than equity .	COREF_PREV	61
This has both made investors uneasy and the corporations more vulnerable . ''	COREF_PREV	62
Nevertheless , he says a depression does n't appear likely .	COREF_OTHER	63
`` There is more resiliency in the economy at large than we commonly suppose , '' he says .	COREF_OTHER	64
`` It takes more error now to have a major depression than back in the Thirties -- much as the financial community and the government may try . ''	COREF_PREV	65
Mario Gabelli .	NONE	66
New York money manager Mario Gabelli , an expert at spotting takeover candidates , says that takeovers are n't totally gone .	NONE	67
`` Companies are still going to buy companies around the world , '' he says .	NONE	68
Examples are `` Ford looking at Jaguar , BellSouth looking at LIN Broadcasting . ''	COREF_OTHER	69
These sorts of takeovers do n't require junk bonds or big bank loans to finance them , so Mr. Gabelli figures they will continue .	NONE	70
`` The market was up 35 % since -LCB- President -RCB- Bush took office , '' Mr. Gabelli says , so a correction was to be expected .	COREF_PREV	71
He thinks another crash is `` unlikely , '' and says he was `` nibbling at '' selected stocks during Friday 's plunge .	COREF_PREV	72
`` Stocks that were thrown out just on an emotional basis are a great opportunity -LCB- this -RCB- week for guys like me , '' he says .	COREF_PREV	73
Jim Rogers .	NONE	74
`` It seems to me that this is the pin that has finally pricked the balloon , '' says Mr. Rogers , a professor of finance at Columbia University and former co-manager of one of the most successful hedge funds in history , Quantum Fund .	COREF_OTHER	75
He sees `` economic problems , financial problems '' ahead for the U.S. , with a fairly strong possibility of a recession .	COREF_PREV	76
`` Friday you could n't sell dollars , '' he says .	NONE	77
Dealers `` would give you a quote , but then refuse to make the trade . ''	COREF_OTHER	78
If the dollar stays weak , he says , that will add to inflationary pressures in the U.S. and make it hard for the Federal Reserve Board to ease interest rates very much .	COREF_PREV	79
Mr. Rogers wo n't decide what to do today until he sees how the London and Tokyo markets go .	COREF_PREV	80
He recommends that investors sell takeover - related stocks , but hang on to some other stocks -- especially utilities , which often do well during periods of economic weakness .	COREF_PREV	81
Frank Curzio .	NONE	82
Many people now claim to have predicted the 1987 crash .	NONE	83
Queens newsletter writer Francis X. Curzio actually did it : He stated in writing in September 1987 that the Dow Jones Industrial Average was likely to decline about 500 points the following month .	COREF_OTHER	84
Mr. Curzio says what happens now will depend a good deal on the Federal Reserve Board .	COREF_PREV	85
If it promptly cuts the discount rate it charges on loans to banks , he says , `` That could quiet things down . ''	COREF_OTHER	86
If not , `` We could go to 2200 very soon . ''	COREF_OTHER	87
Frank W. Terrizzi .	COREF_OTHER	88
Stock prices `` would still have to go down some additional amount before we become positive on stocks , '' says Mr. Terrizzi , president and managing director of Renaissance Investment Management Inc. in Cincinnati .	COREF_OTHER	89
Renaissance , which manages about $ 1.8 billion , drew stiff criticism from many clients earlier this year because it pulled entirely out of stocks at the beginning of the year and thus missed a strong rally .	COREF_PREV	90
Renaissance is keeping its money entirely in cash equivalents , primarily U.S. Treasury bills .	COREF_PREV	91
`` T - bills probably are the right place to be , '' he says .	COREF_PREV	92
