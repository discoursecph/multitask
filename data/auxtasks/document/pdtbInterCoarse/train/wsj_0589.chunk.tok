When Michael S. Perry took the podium at a recent cosmetics industry event , more than 500 executives packing the room snapped to attention .	O	9..148
Mr. Perry , who runs Unilever Group 's world-wide personal-care business , paused to scan the crowd .	O	151..248
" I see we have about half the audience working for us , " he said , tongue in cheek .	O	249..330
" The other half , we may have before long . "	O	331..373
Members of the audience gasped or laughed nervously ; their industry has been unsettled recently by acquisitions .	O	376..488
First Unilever , the Anglo-Dutch packaged-goods giant , spent $ 2 billion to acquire brands such as Faberge and Elizabeth Arden .	B-contingency	489..614
It now holds the No. 3 position at U.S. department-store cosmetic counters .	I-contingency	615..690
Then Procter & Gamble Co. agreed to buy Noxell Corp. for $ 1.3 billion .	B-expansion	691..761
That acquisition , to be completed by year end , will include the Cover Girl and Clarion makeup lines , making P&G the top marketer of cosmetics in mass-market outlets .	I-expansion	762..927
It 's not so much the idea of acquisitions that has agitated the cosmetics industry as the companies doing the acquiring : P&G and Unilever bring with them great experience with mundane products like soap and toilet paper , sparking disdain in the glitzy cosmetics trade ; but they also bring mammoth marketing clout , sparking fear .	O	930..1258
Though it is far from certain that companies best known for selling Promise margarine and Tide detergent will succeed in cosmetics , there 's little doubt they will shake up the industry .	O	1259..1444
For now , both companies are keeping quiet about their specific plans .	B-comparison	1447..1516
But industry watchers expect them to blend the methodical marketing strategies they use for more mundane products with the more intuitive approach typical of cosmetics companies .	I-comparison	1517..1695
Likely changes include more emphasis on research , soaring advertising budgets and aggressive pricing .	B-comparison	1696..1797
But some cosmetics-industry executives wonder whether techniques honed in packaged goods will translate to the cosmetics business .	B-contingency	1798..1928
Estee Lauder Inc. , Revlon Inc. and other cosmetics houses traditionally have considered themselves fashion enterprises whose product development is guided by the creative intuition of their executives .	B-expansion	1929..2130
Cosmetics companies roll out new makeup colors several times a year , and since most products can be easily copied by competitors , they 're loath to test them with consumers .	I-expansion	2131..2303
" Just because upscale cosmetics look like packaged goods and smell like packaged goods , it does n't mean they are packaged goods , " says Leonard Lauder , chief executive of Estee Lauder .	O	2306..2489
" They 're really fashion items wrapped up in little jars . "	O	2490..2547
In contrast to the more artistic nature of traditional cosmetics houses , Unilever and P&G are the habitats of organization men in gray-flannel suits .	O	2550..2699
Both companies are conservative marketers that rely on extensive market research .	B-expansion	2700..2781
P&G , in particular , rarely rolls out a product nationally before extensive test-marketing .	B-expansion	2782..2872
Both can be extremely aggressive at pricing such products as soaps and diapers -- to the extent that some industry consultants predict cents-off coupons for mascara could result from their entry into the field .	I-expansion	2873..3083
P&G already has shown it can meld some traditional packaged-goods techniques with the image-making of the cosmetics trade in the mass-market end of the business .	B-expansion	3086..3247
Consider Oil of Olay , which P&G acquired as part of Richardson-Vicks International in 1985 .	B-temporal	3248..3339
The moisturizer , introduced in 1962 , had a dowdy image .	I-temporal	3340..3395
" Oil of Olay brought with it the baggage of being used basically by older women who had already aged , " says David Williams , a consultant with New England Consulting Group .	O	3396..3567
P&G set out to reposition the brand by broadening the product line to include facial cleansers and moisturizers for sensitive skin .	B-expansion	3570..3701
It also redesigned Oil of Olay 's packaging , stamping the traditional pink boxes with gold lines to create a more opulent look .	B-expansion	3702..3828
Moreover , P&G shifted its ad campaign from one targeting older women to one featuring a woman in her mid-30s vowing " not to grow old gracefully . "	B-contingency	3829..3974
The company says sales have soared .	I-contingency	3975..4010
Goliaths like Unilever and P&G have enormous financial advantages over smaller rivals .	O	4013..4099
Next year , Noxell plans to roll out a perfume called Navy , says George L. Bunting Jr. , chairman of Noxell .	O	4100..4206
Without P&G 's backing , Noxell might not have been able to spend the estimated $ 5 million to $ 7 million needed to accomplish that without scrimping on its existing brands .	O	4207..4377
Packaged-goods companies " will make it tougher for smaller people to remain competitive , " Mr. Bunting says .	O	4378..4485
Further consolidations in the industry could follow .	B-expansion	4488..4540
Rumors that Unilever is interested in acquiring Schering-Plough Corp. 's Maybelline unit are widespread .	I-expansion	4541..4645
Unilever wo n't comment ; Schering , however , denies the brand is for sale .	O	4646..4718
The presence of Unilever and P&G is likely to increase the impact of advertising on cosmetics .	B-contingency	4721..4815
While the two are among the world 's biggest advertisers , most makers of upscale cosmetics spend relatively little on national ads .	I-contingency	4816..4946
Instead , they focus on events in department stores and pour their promotional budgets into gifts that go along with purchases .	O	4947..5073
Estee Lauder , for example , spends only an estimated 5 % of sales on advertising in the U.S. , and Mr. Lauder says he has no plans to change his strategy .	O	5074..5225
The most dramatic changes , however , probably will come in new-product development .	O	5228..5310
Nearly 70 % of cosmetics sales come through mass-distribution outlets such as drug stores and supermarkets , according to Andrew Shore , an analyst at Shearson Lehman Hutton Inc .	B-expansion	5311..5486
That figure has been inching up for several years .	B-expansion	5487..5537
As the trend continues , demand for mass-market items that are high quality but only mid-priced -- particularly skin-care products -- is expected to increase .	B-expansion	5538..5695
This fall , for example , L'Oreal Group , ordinarily a high-end line , rolled out a drug-store line of skin-care products called Plenitude , which retail for $ 5 to $ 15 .	I-expansion	5696..5859
The packaged-goods marketers may try filling that gap with a spate of new products .	B-contingency	5862..5945
Unlike the old-line cosmetics houses , Unilever and P&G both have enormous research and development bases to draw on for new products .	B-expansion	5946..6079
P&G , in fact , is noted for gaining market leadership by introducing products that offer a technical edge over the competition .	B-expansion	6080..6206
Sales of its Tide detergent soared earlier this year , for example , after P&G introduced a version that includes a bleach safe for all colors and fabrics .	I-expansion	6207..6360
That 's led industry executives to speculate that future product development will be driven more by technological innovation than by fashion whims -- especially among mass-market brands .	O	6363..6548
" There will be more emphasis on quality , " says Guy Peyrelongue , chief executive of Cosmair Inc. , the U.S. licensee of L'Oreal .	O	6549..6675
" You 'll see fewer gimmicks . "	O	6676..6704
But success for Unilever and P&G is far from guaranteed , as shown by the many consumer-product companies that have tried and failed to master the quirky beauty business .	B-expansion	6707..6876
In the 1970s , several pharmaceutical and packaged-goods companies , including Colgate-Palmolive Co. , Eli Lilly & Co. , Pfizer Inc. and Schering-Plough acquired cosmetics companies .	I-expansion	6877..7055
Industry consultants say only Schering-Plough , which makes the mass-market Maybelline , has maintained a meaningful business .	O	7056..7180
Colgate , which acquired Helena Rubenstein in 1973 , sold the brand seven years later after the brand languished .	O	7181..7292
Unilever already has experienced some disappointment .	B-contingency	7295..7348
The mass-market Aziza brand , which it acquired in 1987 along with Chesebrough-Pond 's Inc. , has lost share , according to industry analysts .	I-contingency	7349..7487
The ritzy world of department-store cosmetics retailing , where Unilever is concentrating its efforts , may prove even more treacherous .	B-contingency	7490..7624
In this niche , makeup colors change seasonally because they are linked to ready-to-wear fashions .	B-contingency	7625..7722
Because brand loyalty is weak and most cosmetics purchases are unplanned , careful training of store sales staffs by cosmetics companies is important .	B-expansion	7723..7872
And cultivating a luxury image strong enough to persuade consumers to pay more than $ 15 for lipstick or eye makeup requires a subtle touch that packaged-goods companies have yet to demonstrate on their own .	I-expansion	7873..8079
