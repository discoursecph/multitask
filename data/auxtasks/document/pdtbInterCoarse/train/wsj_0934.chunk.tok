The Soviet State Bank announced a 90 % devaluation of the ruble against the dollar for private transactions , in an apparent attempt to curb the nation 's rapidly growing black market for hard currency .	O	9..208
The measure , which will take effect next Wednesday , will create a two-tier exchange rate .	O	211..300
Commercial transactions will continue to be based on the official rate of about 0.63 rubles to the dollar .	B-comparison	301..407
But for Soviet citizens who travel abroad for business or tourism , the rate will jump to 6.26 rubles to the dollar .	I-comparison	408..523
Tass news agency said the devaluation also will apply to foreigners ' transactions .	O	526..608
But it did n't elaborate , and it remains unclear how far Western tourists and foreigners living in Moscow will be allowed to benefit from the sweeping rate cut .	O	609..768
The current ruble rate has long been out of line with the black market .	B-expansion	771..842
As Soviet leader Mikhail Gorbachev has opened up the country to foreign trade , the discrepancy has become ever greater .	I-expansion	843..962
Western tourists in the Soviet Union who could exchange a dollar -- albeit illegally -- for about four rubles a year ago are now being offered 15 rubles or more .	O	963..1124
Even at such rates , black marketeers have been able to make big profits because of the dire shortage of consumer goods here .	B-expansion	1127..1251
They use dollars to buy Western items such as video recorders and personal computers and then sell them at a huge mark-up .	I-expansion	1252..1374
The going rate for a small personal computer that costs about $ 2,000 in the West is anywhere from 50,000 to 100,000 rubles .	B-expansion	1375..1498
Even a pack of 20 Western cigarettes can fetch 20 rubles or more .	I-expansion	1499..1564
With more than 300 billion rubles in savings accounts and little to spend them on , Soviet consumers grumble at the exorbitant black-market prices for such goods -- but they buy them anyway .	O	1565..1754
Moscow has already tacitly admitted that the ruble is n't worth much , announcing in August that it will pay Soviet farmers in hard currency for grain and other produce that they grow in excess of state-plan quotas .	O	1757..1970
" The absurdity of the official rate should seem obvious to everyone , " the afternoon newspaper Izvestia wrote in a brief commentary on the devaluation .	O	1973..2123
The State Bank 's move is part of a drive to iron out exchange-rate discrepancies as Moscow moves toward making the ruble convertible -- a goal that Soviet bankers and economists say is still far away .	B-entrel	2126..2326
Rumors of an impending devaluation have been circulating in Moscow for weeks , but the size of the cut took many Western bankers by surprise .	I-entrel	2327..2467
" It 's much bigger than we expected , " said one German banker , who asked not to be named .	O	2468..2555
The next step , which could have a larger effect on businesses , will come early next month , when the Bank for Foreign Economic Affairs is to hold its first auction of foreign currency .	B-entrel	2558..2741
Soviet companies needing Western currencies to buy equipment and supplies abroad will be able to submit bids .	I-entrel	2742..2851
Plans for the auction , which was supposed to take place last spring and become a regular event , have been thwarted by a lack of hard currency .	B-entrel	2854..2996
Soviet firms that hold some are unwilling to part with it , and joint ventures are n't yet allowed to participate .	I-entrel	2997..3109
The Kremlin also has been unwilling to provide hard currency for the auction , using a lot of it instead to finance emergency imports of consumer goods .	O	3110..3261
If foreign tourists and businesses could sell their currencies freely at the new , better exchange rate , that would enable the State Bank to increase its dollar reserves and would mop up some of the excess rubles in the economy at the same time .	O	3264..3508
But the amounts they exchange may be limited ; most Soviet hotels , for example , demand payment in hard currency from Western visitors .	O	3509..3642
Unless other rules are changed , the devaluation could cause difficulties for the people it is primarily meant to help : Soviets who travel abroad .	B-expansion	3645..3790
Over the past three years , thousands of people here have made use of looser travel restrictions to get their first taste of life abroad .	B-comparison	3791..3927
But under current rules , they are allowed to change just 200 rubles into dollars and other currencies for each trip .	B-entrel	3930..4046
At the new rate , that would give them about $ 30 to travel on .	B-entrel	4047..4108
It is n't yet clear whether the 200-ruble limit will be lifted .	B-entrel	4109..4171
If it is n't , the black market for dollars probably will continue to thrive .	I-entrel	4172..4247
