Japan 's Finance Ministry strongly denied playing any role in the New York stock-price free fall .	root	9..105
Makoto Utsumi , vice minister for international affairs , said the ministry did n't in any way suggest to Japanese banks that they stay out of the UAL Corp. leveraged buy-out .	norel	108..280
The ministry has never even suggested that Japanese banks be cautious about leveraged buy-outs in general , Mr. Utsumi said .	conjunction	281..404
" There are no facts { behind the assertions } that we sent any kind of signal , " he declared in an interview .	cause	405..511
The comments were the ministry 's first detailed public statement on the subject , and reflect the ministry 's concern that foreigners will think Japan is using its tremendous financial power to control events in foreign markets .	norel	514..740
A number of accounts of the events leading to the 190 point drop in New York stock prices on Oct. 13 accused the ministry of pulling the plug on the UAL deal for one reason or another .	instantiation	741..925
Mr. Utsumi said the most the ministry had ever done was ask Japanese banks about " the status of their participation " in one previous U.S. leveraged buy-out .	norel	928..1084
The ministry inquired about that deal -- which Mr. Utsumi declined to identify -- because the large presence of Japanese banks in the deal was " being strongly criticized in the U.S. Congress " and it was " necessary for us to grasp the situation . "	entrel	1085..1330
He said the inquiry was n't made in a way that the banks could have interpreted as either encouraging or discouraging participation , and he added that none of the Japanese banks changed their posture on the deal as a result of the inquiry .	comparison	1331..1569
Mr. Utsumi also said some Japanese banks were willing to participate in the UAL financing up to the very end , which would suggest at the very least that they were n't under orders to back out .	norel	1572..1763
In general , Mr. Utsumi said , Japanese banks are becoming more " independent " in their approach to overseas deals .	norel	1764..1876
" Each Japanese bank has its own judgment on the profits and risks in that { UAL } deal , " he said .	norel	1879..1974
" They are becoming more independent .	conjunction	1975..2011
It 's a sound phenomenon . "	entrel	2012..2037
Sanwa Bank Ltd. is one Japanese bank that decided not to participate in the first UAL proposal .	norel	2038..2133
A Sanwa Bank spokesman denied that the finance ministry played any part in the bank 's decision .	entrel	2134..2229
" We made our own decision , " he said .	alternative	2230..2266
Still , Mr. Utsumi may have a hard time convincing market analysts who have rightly or wrongly believed that the ministry played a role in orchestrating recent moves by Japanese banks .	norel	2269..2452
All week there has been much speculation in financial circles in Tokyo and abroad about the ministry 's real position .	cause	2453..2570
Bank analysts say ministry officials have been growing increasingly concerned during the past few months about Japanese banks getting in over their heads .	norel	2573..2727
" The { ministry } thinks the banks do n't know what they are doing , that they have very little idea how to cope with risk , " said one foreign bank analyst who asked not to be identified .	norel	2730..2912
" The { ministry } wants to see the Japanese banks pull in their horns " on leveraged buy-outs , he added .	cause	2913..3014
Although some of the Japanese banks involved in the first proposed bid for UAL bowed out because they found the terms unattractive , observers here say they have a hard time believing that commercial considerations were the only reason .	norel	3017..3252
Japanese banks are under " political pressure " as well , the analyst said .	cause	3253..3325
Moreover , analysts point out that Japanese banks have a reputation for doing deals that are n't extremely profitable if they offer the chance to build market share , cement an important business relationship or curry favor with powerful bureaucrats .	conjunction	3326..3573
Clearly , some financial authorities are concerned about the Japanese banks role in leveraged buy-outs .	norel	3576..3678
At a news conference this week , Bank of Japan Gov. Satoshi Sumita cautioned banks to take a " prudent " stance regarding highly leveraged deals .	instantiation	3679..3821
Despite Mr. Sumita 's statements , it is the Finance Ministry , not the central bank , that makes policy decisions .	concession	3822..3933
While recent events may cool some of the leveraged buy-out fever , Japanese banks are n't likely to walk away from the game .	norel	3936..4058
Despite the risks , the deals can be an attractive way for Japanese banks to increase their presence in the U.S. market , bank analysts say .	cause	4059..4197
Flush with cash at home , but with fewer customers to lend to , leading banks are eager to expand overseas .	conjunction	4198..4303
Jumping in on big deals is a high profile way to leapfrog the problem of not having a strong retail-banking network .	entrel	4304..4420
