It 's a California crime saga worthy of an Erle Stanley Gardner title :	(Summary-Evaluation-Topic(Elaboration
The Case of the Purloined Palm Trees .	Elaboration)
Edward Carlson awoke one morning last month	(Elaboration(Elaboration(Elaboration(Temporal(Temporal
to find eight holes in his front yard	(Elaboration
where his prized miniature palms ,	(Textual-organization(Elaboration
called cycads ,	Elaboration)
once stood .	Textual-organization)))
Days later , the thieves returned	(Elaboration(Elaboration(Joint
and dug out more ,	Joint)
this time adding insult to injury .	Elaboration)
`` The second time , ''	(Textual-organization(Attribution
he says ,	Attribution)
`` they left the shovel . ''	Textual-organization)))
No garden-variety crime , palm-tree rustling is sprouting up all over Southern California ,	(Cause
bringing big bucks to crooks	(Elaboration
who know their botany .	Elaboration)))
Cycads ,	(Elaboration(Elaboration(Textual-organization(Elaboration
the most popular of which is the Sago Palm ,	Elaboration)
are doll-sized versions of California 's famous long-necked palms , with stubby trunks and fern-like fronds .	Textual-organization)
Because the Sago is relatively rare	(Elaboration(Elaboration(Cause(Joint
and grows only a couple of inches a year ,	Joint)
it 's a pricey lawn decoration :	Cause)
A two-foot tall Sago can retail for $ 1,000 ,	(Joint
and taller ones often fetch $ 3,000 or more .	Joint))
`` Evidently , somebody has realized	(Attribution(Attribution
it 's easy money to steal these things , ''	Attribution)
says Loran Whitelock , a research associate	(Textual-organization(Elaboration
specializing in cycads	Elaboration)
at the Los Angeles State and County Arboretum .	Textual-organization))))
Just last week , would-be thieves damaged three Sagos at Mr. Whitelock 's home in the Eagle Rock section	(Elaboration(Temporal
before something frightened them off , foiled .	Temporal)
`` It 's hard to think someone is raping your garden , ''	(Attribution
he says .	Attribution))))
Police suspect	(Summary-Evaluation-Topic(Elaboration(Cause(Attribution
that the criminals ,	(Textual-organization(Elaboration
who dig up the plants in the dead of night ,	Elaboration)
are selling them to nurseries or landscapers .	Textual-organization))
The Sago has become a popular accent in tony new housing tracts ,	(Elaboration
apparently giving the rustlers a ready market for their filched fronds .	Elaboration))
Thieves are going to find `` anybody	(Attribution(Elaboration
who has enough bucks	(Cause
to plant these things in their front yard , ''	Cause))
says William Morrissey , an investigator with the police department in Garden Grove , Calif. ,	(Elaboration
where five such thefts have been reported in the past several weeks .	Elaboration)))
The department is advising residents to plant Sagos ,	(Joint(Joint(Textual-organization(Summary-Evaluation-Topic
if they must ,	Summary-Evaluation-Topic)
in the back yard	Textual-organization)
and telling nurseries to be on the lookout for anyone	(Elaboration
trying to palm one off .	Elaboration))
But for those Californians	(Textual-organization(Elaboration
who want exotic gardens out front	(Elaboration
where neighbors can appreciate them ,	Elaboration))
there 's always Harold Smith 's approach .	(Elaboration
After three Sagos were stolen from his home in Garden Grove ,	(Elaboration(Attribution(Temporal
`` I put a big iron stake in the ground	(Temporal
and tied the tree to the stake with a chain , ''	Temporal))
he says proudly .	Attribution)
`` And you ca n't cut this chain with bolt cutters . ''	Elaboration)))))))
