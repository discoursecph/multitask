New orders for durable goods fell back slightly in September	(Elaboration(Attribution(Evaluation(Temporal
after shooting up the month before ,	Temporal)
reflecting weakening auto demand after a spurt of orders for new 1990 models ,	Evaluation)
the Commerce Department reported .	Attribution)
Orders for military equipment , household appliances , machinery and other goods	(Elaboration(Contrast(Attribution(Temporal(Same-unit(Elaboration
expected to last at least three years	Elaboration)
dipped 0.1 % last month , to $ 126.68 billion ,	Same-unit)
after leaping 3.9 % in August ,	Temporal)
the department said .	Attribution)
Most analysts had expected a sharper decline after the steep rise in August .	Contrast)
Moreover , a recent government report	(Elaboration(Contrast(Same-unit(Elaboration
showing widespread layoffs in manufacturing	Elaboration)
had contributed to perceptions	(Elaboration
that the manufacturing sector of the economy had slowed to a crawl .	Elaboration))
But many economists pointed to a 1.8 % September rise in orders outside the volatile transportation category .	Contrast)
That `` suggests	(Elaboration(Elaboration(Attribution(Evaluation
the manufacturing sector is not falling apart , ''	Evaluation)
said Sally Kleinman , an economist at Manufacturers Hanover Securities Corp. in New York .	Attribution)
She added , however :	(Attribution
`` It is not robust by any means . ''	Attribution))
While a decline in orders for cars and civilian airplanes pulled down the orders total ,	(Elaboration(Elaboration(Elaboration(Contrast
an enormous jump in orders for heavy military equipment propped it up .	(Elaboration
Orders for capital defense goods skyrocketed 56 % ,	Elaboration))
and a government analyst said	(Attribution
nearly all areas saw increases ,	(Elaboration
including airplanes , missiles , ships , tanks and communications equipment .	Elaboration)))
Orders for military goods usually catapult in September ,	(Elaboration(Temporal(Temporal(Attribution
government officials say ,	Attribution)
as the Pentagon scrambles to spend its money	Temporal)
before the new fiscal year begins Oct. 1 .	Temporal)
While all the numbers in the durable goods report were adjusted for seasonal fluctuations ,	(Evaluation
a Commerce Department analyst said	(Attribution
that the adjustment probably did n't factor out all of the wide-ranging surge in defense orders .	Attribution))))
Without the increase in defense bookings ,	(Background(Elaboration(Explanation(Condition
September orders would have plummeted 3.9 % .	Condition)
Analysts were most unsettled by evidence	(Elaboration
the backlog of orders at factories is slipping .	Elaboration))
Unfilled orders for durable goods rose 0.4 % in September , to $ 476.14 billion ,	(Temporal
after declining for the first time in 2 1/2 years in August .	Temporal))
In July unfilled orders grew 1 % .	(Elaboration(Contrast
But analysts noted	(Attribution
that	(Same-unit
excluding transportation-	(Elaboration(Elaboration
where what they believe was a temporary surge in auto demand pushed up the figures-	Elaboration)
order backlogs have declined for three months in a row .	Elaboration))))
`` It means we 're eating into the bread	(Elaboration(Evaluation(Elaboration(Elaboration
that keeps us going .	Elaboration)
That is a little disturbing , ''	(Attribution
Ms. Kleinman said .	Attribution))
`` It also means	(Same-unit
if you have a real drop-off in orders ,	(Cause(Condition
production will likely fall off very quickly	Condition)
because there is less	(Elaboration
to keep things going . ''	Elaboration))))
Capital goods orders outside of the defense sector tumbled for the second month in a row ,	(Contrast(Evaluation(Cause
posting a 5.6 % drop after a 10.3 % decline .	Cause)
Such steep drops in a category	(Same-unit(Elaboration
seen as a barometer of business investment	Elaboration)
would customarily be grave news for the economy .	Same-unit))
But a Commerce Department analyst said	(Contrast(Attribution
that in both months orders would have risen	(Contrast
had it not been for a drop in civilian aircraft bookings , a category	(Elaboration
that is showing declines only after a huge surge earlier this year .	Elaboration)))
Still , Milton Hudson , senior economic adviser at Morgan Guaranty Trust Co. in New York , said :	(Attribution
`` If you look back a half-year or so	(Condition
the evidence was pretty good of affirmative strength in the capital-goods sector .	(Elaboration
Now at least there are question marks about that ,	(Joint
and without any question the pace of growth has slowed . ''	Joint))))))))))))))
