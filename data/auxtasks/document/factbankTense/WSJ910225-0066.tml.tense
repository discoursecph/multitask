With the ground battle to liberate Kuwait proceeding with surprising speed and success , Bush administration officials are optimistic the Persian Gulf war can begin winding down in a matter of days .	NONE	3
The enormous ground offensive by U.S. Marines and Army troops , at least during its first 24 hours , met only sporadic Iraqi resistance as it quickly penetrated deep into Kuwait and parts of Iraq , military officials said .	PAST	4
According to press pool reports , U.S. Marines are already on the outskirts of Kuwait City .	NONE	5
" So far , the offensive is progressing with dramatic success , " said a buoyant Gen. Norman Schwarzkopf , commander of U.S. forces .	PAST	6
Similarly , while cautioning about the uncertainty of early battle reports , White House spokesman Marlin Fitzwater said late yesterday that " the operation has been very successful . "	NONE	7
Amid reports that thousands of Iraqi soldiers had surrendered , administration aides were also upbeat in private , with one even talking of victory within a week .	NONE	8
But even continued military success carries political and diplomatic risks for President Bush and the U.S. The allied rejection of the last-minute Soviet-led diplomatic effort to avoid the ground war enabled Mr. Bush to seize the initiative from an Iraq seemingly bent on dictating peace terms .	NONE	9
But it has offended some , especially in Arab countries , who now believe that Mr. Bush 's real objectives are the demise of Saddam Hussein and the destruction of the Iraqi military , not just the liberation of Kuwait .	NONE	10
" Why have a war ? "	PRESENT	11
asked Abdul Latif Shekar , a customs officer in Egypt , a country participating in the attack on Iraqi troops .	PAST	12
" I think the Gorbachev plan was a good one .	PAST	13
Iraq was ready to withdraw . "	INFINITIVE	14
Now , he says , " it looks like the West just wants to destroy Iraq . "	PRESENT	15
Despite the early indications of success , the allied forces could still suffer greater casualties and become bogged down militarily , especially when they encounter the tough Republican Guard , which is entrenched along the Iraq-Kuwait border .	NONE	16
If so , and if it appears that the American goal actually is to destroy the Iraqi regime even at the cost of badly hurting Iraqi society , " the lingering cost of that could be high , " worries former national security adviser Zbigniew Brzezinski .	PRESENT	17
But , he notes , " If everything crumbles totally , that wo n't be such a problem . "	PRESENT	18
American officials staunchly disavow any interest in driving through Iraq toward Baghdad , either in pursuit of Saddam Hussein himself or to set up some American-controlled government inside Iraq .	NONE	19
The Americans say their battle plans call for operating against forces inside Iraq as far north as the city of Basra , about 30 miles north of Kuwait , but say there is n't any plan to drive beyond that .	PRESENT	20
Indeed , French President Francois Mitterrand said yesterday that some allied forces are crossing Iraqi territory as part of a " pincer " movement to trap the soldiers occupying Kuwait , but insisted , " The purpose is n't to invade Iraqi territory , that 's not the aim , that is n't the mandate . "	NONE	21
Nevertheless , American officials over the weekend became more open in declaring that by destroying Saddam Hussein 's military machine they hope to destroy his regime -- a goal likely to be supported by most Americans .	NONE	22
In a pre-attack message , Lt. Gen. Walter Boomer , the top Marine in the Persian Gulf , told U.S. Marines that their goal is to " restore { Kuwait } to its citizens . "	NONE	23
He went on to add that " in so doing you not only return a nation to its people , but you will destroy the war machine of a ruthless dictator . "	NONE	24
Secretary of State James Baker said on ABC-TV 's " This Week With David Brinkley " that the series of United Nations resolutions condemning Iraq 's invasion of Kuwait " imply that the restoration of peace and stability in the Gulf would be a heck of a lot easier if he and that leadership were not in power in Iraq . "	NONE	25
Of course , it is still far too early to assume that the military situation on the ground will stay as smooth for allied forces as it appears to have been so far .	PRESENT	26
Iraq still has the potential to cause significant problems by using forces and weapons that do n't yet seem fully engaged .	NONE	27
For one thing , Iraq still apparently has n't unleashed its stockpile of chemical weapons .	PRESENT	28
Gen. Schwarzkopf said that some early reports that chemical weapons were used against allied troops turned out to be " bogus . "	PAST	29
Iraq is believed to have the ability to deliver chemical weapons in artillery shells or , perhaps , atop Soviet-made Frog7 missiles .	INFINITIVE	30
Perhaps more important , it appears that allied troops have n't yet fully engaged Iraq 's vaunted Republican Guard , which has been sitting just north of the Iraq-Kuwait border and is considered the most potent element in the Iraqi defense .	PRESENT	31
It remains to be seen how much damage the allied air campaign was able to inflict on the Guard , and whether President Hussein will commit his most valued troops to a fight-to-the-death finish .	NONE	32
Certainly Saddam Hussein continues to implore his country to fight on .	INFINITIVE	33
" Fight them , " he urged Iraqis in a radio address .	PAST	34
" All Iraqis , fight them with all the power you have , and all struggle for everything . "	PRESENT	35
American war planners have long assumed that the early stage of the ground attack , in which American forces would use their speed to sweep around Iraqi defenses and their strength to punch through the relatively weak Iraqi front line , would be the easiest part .	NONE	36
Despite these early successes , the mere fact that a ground campaign has begun almost guarantees that the Bush administration will face fresh problems growing out of the military situation .	NONE	37
There are likely to be additional American prisoners of war taken , and there are signs that President Hussein is taking Kuwaiti hostages .	PRESENT	38
U.S. and Kuwaiti officials say there are reports that large numbers of civilians from Kuwait City are being rounded up and held by Iraqi troops , apparently either for use as human shields or for use later in bargaining once the war is over .	NONE	39
President Bush 's political argument for going to a ground war has been strengthened by the growing stream of reports of wanton Iraqi destruction inside Kuwait .	NONE	40
U.S. officials say that hundreds of Kuwaiti oil wells now may have been set afire .	NONE	41
And Robert Gates , Mr. Bush 's deputy national security adviser , asserted in an interview on the Cable News Network that Iraqi troops have set fire to " large sections " of Kuwait City .	NONE	42
Mr. Bush and his aides were leaning toward a military conclusion of the crisis even before the latest reports of Iraqi atrocities in Kuwait came to light .	NONE	43
The president and his top aides tentatively decided on Feb. 11 that a ground war would be necessary .	NONE	44
The decision was made after Defense Secretary Dick Cheney and Gen. Colin Powell , chairman of the Joint Chiefs of Staff , returned from a visit with military commanders in Saudi Arabia , administration officials say .	PAST	45
Then , a week or so ago , Gen. Schwarzkopf secretly picked Saturday night as the optimal time to start the offensive .	PAST	46
The date was unaffected by the last-ditch Soviet peace initiative .	PAST	47
The real problem with the Soviet proposals , U.S. officials now say , was that they all would have required lifting economic sanctions against Iraq .	NONE	48
The Bush administration considers the sanctions essential to keeping Saddam Hussein under control should he survive the war .	NONE	49
Mr. Bush forestalled further diplomatic maneuvering by issuing an ultimatum on behalf of the allies demanding that Iraq withdraw within a week , starting at noon Saturday .	PRESPART	50
Administration aides said that the idea of the ultimatum was Gen. Powell 's .	PAST	51
He argued setting an explicit deadline for Saddam Hussein to break would , when it was broken , give the U.S. military a clear green light to proceed .	PAST	52
In setting out his final challenge to Saddam Hussein , Mr. Bush continued the intensive personal diplomacy he began after the invasion llast August .	NONE	53
After cabling world leaders about his intention to give Saddam Hussein a final deadline to exit Kuwait , he offered him a week to withdraw fully , instead of the four days he originally considered , because of objections from some European partners that four days seemed punitive and unrealistic .	PAST	54
And when he and President Gorbachev spoke about the decision in a talk lasting nearly an hour , the President took pains to listen to what his counterpart had to say , although he already had decided that the Soviet alternative to the allied deadline was unacceptable .	PAST	55
Finally , when Iraq failed to respond to the U.S. ultimatum , Mr. Bush let the ground offensive begin as previously planned Saturday night .	PAST	56
The attack was lightning quick , as allied forces punched through tall sand berms on the border and pushed forward into Iraq and Kuwait .	PAST	57
U.S. Marines were said to have breached troublesome mine fields along the Iraqi lines but Pentagon officials said no amphibious assault on Kuwait 's beaches had begun .	PAST	58
Long columns of Iraqi prisoners of war could be seen trudging through the desert toward the allied rear .	NONE	59
U.S. commanders said 5,500 Iraqi prisoners were taken in the first hours of the ground war , though some military officials later said the total may have climbed above 8,000 .	PAST	60
The U.S. hopes its troops will drive Iraqi forces out of Kuwait quickly , leaving much of Iraq 's offensive military equipment destroyed or abandoned in Kuwait .	NONE	61
It expects that tens of thousands of Iraqi soldiers will surrender to the U.S. and its allies over the next few days .	FUTURE	62
If the allies succeed , Saddam Hussein will have plunged his country first into a fruitless eight-year-long war against Iran and then into a humiliating war against the U.S. and the allies to defend his conquest of Kuwait , leaving much of his country 's military establishment and modern infrastructure in ruins .	NONE	63
Meanwhile , the U.S. hopes , economic sanctions and an international arms embargo will remain in effect until Iraq pays war reparations to Kuwait to cover war damages .	NONE	64
That would undermine any chances of rebuilding either Iraq or its armed forces in short order as long as Saddam Hussein remains in power .	NONE	65
" I think frankly Saddam is finished , no matter what happens , " says Christine Helms , a Middle East scholar who has written extensively about Iraq .	PRESENT	68
" These guys simply do n't retire to condos over the Euphrates . "	PRESENT	69
Despite the lack of any obvious successors , the Iraqi leader 's internal power base appeared to be narrowing even before the war began .	PAST	70
Some analysts say he appeared to be relying on a smaller and smaller circle of close advisers and relatives .	PAST	71
If that 's true , the narrowing of his support would make it easier for someone to push him aside from within .	NONE	72
Yet , paradoxically , the perception that the U.S. wants to destroy Iraq may increase Saddam Hussein 's support within the Iraqi military .	NONE	73
And the U.S. now will face sharper questions in the Arab world since it did n't back the peace proposals worked out in Moscow .	PAST	74
" We looked to the United States , we expected you to have the moral edge , " says Nasser Tahboub , a Jerusalem-born Jordanian who has an American wife and a doctorate in political science from Duke University .	PAST	75
" Now we see that edge eroded .	PASTPART	76
For me , it is a great tragedy .	PRESENT	77
For the first time in history , the U.S. has gone to war with an Arab and Muslim nation , and we know a peaceful solution was in reach . "	NONE	78
