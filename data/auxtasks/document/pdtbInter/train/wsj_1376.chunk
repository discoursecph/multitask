As Helen Boehm, who owns an art porcelain company, sipped her luncheon cocktail, she reeled off the names of a few pals -- Prince Charles, Princess Diana, Sarah Ferguson, John Kluge, Milton Petrie.	O	9..206
Then, flashing a diamond ring as big as the Ritz ("my day diamond, darling"), she told her two companions that she is on the "board" of the Vatican Museum in Rome.	O	209..372
As it turns out, the board has a lot of important members, including Winton Blount (former postmaster general of the U.S.), Mrs. Henry Gaisman (widow of the inventor of auto-strop razor) and Vincent Murphy (an investment banker at Merrill Lynch & Co.	B-concession	375..626
But Mrs. Boehm didn't mention any of them.	I-concession	627..669
"Helen Boehm has a way with names," says James Revson, a gossip columnist for Newsday (and son of Joseph Revson, a founder of Revlon).	O	672..806
Like which are droppable and which are not.	O	807..850
With the fall social season well under way, name-droppers are out in force, trying to impress their betters and sometimes put down their lessers.	B-conjunction	853..998
But the truth is that almost everyone, from real-estate agents to city fathers, name-drops; and a surprising number of people have an ancient uncle who claims he lived next door to the cartoonist who did the Katzenjammer Kids.	I-conjunction	999..1225
(In case you have forgotten, his name was Rudolph Dirks.)	O	1226..1283
"Name-dropping is pervasive and getting more so as society becomes more complex and alienating," says Herbert Freudenberger, a New York psychoanalyst, with a high-powered clientele.	O	1286..1467
"It can be an avenue of entrance to a certain sector of society... .	O	1468..1536
It provides some people a needed sense of affiliation and can help open up a conversation with someone you don't know."	O	1537..1656
Like the Long Island matron in the theater district the other day who swore to a stranger that she once met Liza Minnelli.	O	1659..1781
"I was having a drink in Sardi's, when all of a sudden I saw a woman's backside coming up the steps on the second floor and she was wearing sequined slacks.	O	1782..1938
I knew it was someone important, so I followed her into the ladies room and sure enough, it was Liza.	O	1939..2040
So I said, `Hello. '	O	2041..2061
And she said, `Hello. '	O	2062..2085
Can you imagine	B-entrel	2086..2102
Liza said hello to me."	I-entrel	2103..2126
Some people must drop names -- call it an irresistible impulse.	O	2129..2192
"They can't help talking about the big important people they know, even if they don't really know them," says Dr. Freudenberger.	O	2193..2321
Beauregard Houston-Montgomery, a New York writer who changed his name from William Stretch in 1980, is an inveterate name-dropper.	O	2324..2454
"I do it innately and pathologically, and while it may occasionally get me into trouble, it's also gotten me access to parties and society," he says.	O	2455..2604
Name-dropping recently helped Mr. Houston-Montgomery crash a party Fame magazine threw for 100 of the 2,809 people mentioned in the diaries of the late Andy Warhol.	O	2607..2771
"I guess I might have asked Beauregard to leave, but he drops so many good names, we decided to let him stay," says Steven Greenberg, publisher of Fame.	O	2772..2924
After all, Warhol was the ultimate namedropper, dropping five a day in his diaries	B-conjunction	2925..3009
And Beauregard was mentioned twice -- although very briefly and in passing."	I-conjunction	3010..3086
Mr. Houston-Montgomery says that at the party he waved to Malcolm Forbes, publisher of Forbes magazine ("We've been in the columns together"), Mary Boone, a New York art dealer ("I think she knows me, but I'm not sure ") and Bridget Fonda, the actress ("She knows me, but we're not really the best of friends").	O	3089..3400
Mr. Revson, the gossip columnist, says there are people who actually plan whose names they are going to drop before attending a party.	O	3403..3537
These droppers don't flaunt only their friendships with the Trumps, Brooke Astor or Georgette Mosbacher.	O	3538..3642
"They even drop semi-obscure names like Wolfgang Flottl, whom everybody these days apparently has heard of but no one really knows," says Mr. Revson.	O	3643..3792
It's the one-upsmanship of name-dropping that counts	B-contrast	3793..3848
But name-dropping has other benefits, often civic.	I-contrast	3851..3901
In the name of civic pride and from the desire to nullify a negative image, some city promoters seek to link their municipality with the most recognizable names the city has to offer.	O	3902..4085
Take Cleveland.	O	4088..4103
It has gotten a bad rep because its once heavily polluted Cuyahoga River caught fire, because former Mayor Ralph Perk set his hair on fire with an acetylene torch and because its proposed Rock 'n' Roll Hall of Fame was recently refused an urban-development grant	B-conjunction	4104..4367
Some people call it "The Mistake on the Lake" -- Lake Erie, that is.	I-conjunction	4368..4436
"It helps to point out how many important people came through Cleveland on their way to the top," says George Miller, executive director of the New Cleveland Campaign, a nonprofit organization devoted to citing the city's strengths.	O	4439..4671
Mr. Miller notes that actor Paul Newman's family owned a sporting-goods store in Cleveland, that the late actress Margaret Hamilton, who played the bad witch in "The Wizard Of Oz," once ran a nursery school in Cleveland and that comedian Bob Hope's father, a stonemason, once worked on a church next to Severence Hall, Cleveland's main concert hall.	O	4674..5023
"Power names like that don't hurt the city's reputation," Mr. Miller says.	O	5024..5098
In Hollywood, an average family can gain cachet from moving into a home vacated by the famous or near famous.	O	5101..5210
"Why we even just sold a three-bedroom house in Van Nuys and were able to keep the price firm in a weak real-estate market by noting that the original Lone Ranger lived there," says David Rambo, a sales associate with Jon Douglas Co., a Los Angeles real-estate agency.	O	5211..5479
"Most people can't even remember his name."	O	5480..5523
(It is John Hart.)	O	5524..5542
Mr. Rambo says that a 3.2-acre property overlooking the San Fernando Valley is priced at $4 million because the late actor Erroll Flynn once lived there.	O	5545..5698
"If Flynn hadn't lived there, the property might have been priced $1 million lower," says Mr. Rambo, noting that Flynn's house has been bulldozed, and only the swimming pool remains.	O	5699..5881
Press agents and public-relations practitioners are notorious name-droppers	B-conjunction	5884..5960
And some even do it with malice aforethought	B-instantiation	5961..6006
Len Kessler, a financial publicist in New York, sometimes uses it to get the attention of journalists who try to avoid him.	I-instantiation	6007..6130
He says that when Dan Dorfman, a financial columnist with USA Today, hasn't returned his phone calls, he leaves messages with Mr. Dorfman's office saying that he has an important story on Donald Trump, Meshulam Riklis or Marvin Davis.	O	6131..6365
He admits he has no story on any of them on these occasions.	O	6368..6428
"But it does get him to return my calls, and it makes me feel better for the times he's given me the brushoff," Mr. Kessler says.	O	6429..6558
There are, of course, obvious dangers to blatant, unsubstantiated name-dropping.	O	6561..6641
Jeffry Thal, a publicity agent for the Lantz Office in Los Angeles, warns that dropping the wrong name labels the dropper as a fake and a fraud.	O	6642..6786
"Get caught and you're dead in the water," says Mr. Thal.	O	6787..6844
Mr. Thal says that Elizabeth Taylor, a client, "hates being called `Liz. '...	O	6847..6924
If directors or producers phone me and say they know `Liz, ' I know they've never met her.	O	6925..7015
She prefers `Elizabeth. '"	O	7016..7042
In New York society, Pat Buckley, the very social wife of author William Buckley, has the nicknames "Mrs. Buckles" and "Patsy."	O	7045..7172
And her husband sometimes calls her "Ducky."	O	7173..7217
"But call her `Patty, ' and it's a sure giveaway you're not in her circle, because she doesn't use that name," says Joan Kron, editor-in-chief of Avenue magazine, a monthly publication sent to all the right names.	O	7218..7431
John Spencer Churchill, a nephew of the late Sir Winston Churchill, former prime minister of Great Britain, isn't that impressed with most name-droppers he meets.	O	7434..7596
That's because they only drop "mere names," says Mr. Churchill.	O	7597..7660
Currently writing his memoirs, Mr. Churchill, an artist, tells how tycoons such as the late Jean Paul Getty, the oil billionnaire, were, in fact, known only by one initial, their last.	O	7663..7847
"When you're at the club, you ask whether they've spoken to `G.'	O	7848..7912
Now they know who you mean and you know who you mean.	O	7913..7966
But no one else does	B-cause	7967..7988
Now that's name-dropping, if you know what I mean.	I-cause	7989..8039
