It is now a commonplace that prosecutors are bringing criminal indictments in cases where until a few years ago only a civil action at most would have been brought .	B-comparison	9..173
It is now a commonplace that prosecutors are bringing criminal indictments in cases where until a few years ago only a civil action at most would have been brought .	B-expansion	9..173
Yet it is also axiomatic that the power to create new crimes belongs only to the legislature , and not to courts .	B-expansion	174..286
Beginning in the early 19th century , with U.S. v. Hudson and Goodwin , the Supreme Court has repeatedly held that a judicial power to declare conduct to be against the public interest and hence criminal , while well established in British law , would usurp legislative authority under the doctrine of separation of powers .	I-expansion	287..606
That 's the conventional theory anyway .	B-comparison	609..647
In practice , however , the line between interpretation and redefinition of the criminal law long ago began to blur .	B-expansion	648..762
In particular , a common law of white-collar crime has developed with surprising rapidity over the past decade .	B-expansion	763..873
For example , although insider trading has long been criminal , it has never been statutorily defined .	I-expansion	874..974
In 1983 , the Supreme Court tried to supply a workable definition in the Dirks v. SEC decision , which found that liability depended on whether the tipper had breached his fiduciary duty to the corporation in order to obtain " some personal gain " and whether the tippee knew or recklessly disregarded this fact .	B-comparison	975..1283
Gradually , however , lower courts and prosecutors have pushed this definition to its breaking point .	B-expansion	1286..1385
Consider the facts underlying the 1989 conviction of Robert Chestman .	I-expansion	1386..1455
Prior to a tender offer by A&P for Waldbaum Inc. in 1986 , the founder of the Waldbaum 's supermarket chain called an elderly relative to tell her to assemble her stock certificates for delivery .	B-temporal	1458..1651
She called her daughter to take her to the bank , who , in turn , persuaded her husband , a Mr. Loeb , to run this errand .	I-temporal	1652..1769
Hearing of this information , the husband discussed it with his broker , Mr. Chestman , and Mr. Chestman then bought for his own account and other clients .	B-contingency	1770..1922
Basically , Mr. Chestman was a fourth-level tippee .	B-contingency	1923..1973
Did Mr. Loeb , his tipper , breach a fiduciary duty ( and , if so , to whom ) ?	I-contingency	1974..2046
Did Mr. Loeb seek personal gain ( and if so , how ) ?	O	2047..2096
Or did Mr. Chestman only hear a market rumor ( which one may lawfully trade upon ) ?	O	2097..2178
The line seems awfully thin for criminal-law purposes .	O	2179..2233
A second illustration is supplied by the recent guilty plea entered by Robert Freeman , formerly head of arbitrage at Goldman , Sachs & Co .	B-expansion	2236..2373
Essentially , Mr. Freeman had invested heavily in the Beatrice leveraged buy-out , when he was told by another prominent trader , Bernard " Bunny " Lasker , that the deal was in trouble .	I-expansion	2374..2554
After placing orders to sell , Mr. Freeman called Martin Siegel , an investment banker at Kidder , Peabody & Co. , who was advising on the deal , to confirm these rumors .	O	2555..2720
Mr. Siegel asked Mr. Freeman who his source was and , on hearing that it was Bunny Lasker , responded : " Well , your bunny has a good nose . "	O	2721..2857
The illegal " tip " of the bunny 's good nose was then largely a confirmation of rumors already known to many in the market .	B-comparison	2860..2981
Had the case gone to trial the same issues would have surfaced :	I-comparison	2982..3045
Was there a fiduciary breach in order to obtain personal gain ?	O	3046..3108
Did Mr. Freeman have notice of this ?	B-expansion	3109..3145
Finally , was the information material ?	I-expansion	3146..3184
Yet , all these issues are subsidiary to a more central issue : Who is and who should be making the criminal law here ?	O	3185..3301
It is not my contention that either Mr. Chestman or Mr. Freeman was an innocent victim of prosecutorial overzealousness .	B-comparison	3304..3424
Arguably , both were on notice that their behavior was at least risky .	B-comparison	3425..3494
But even if they behaved recklessly , reasons still exist to fear and resist this steady process of case-by-case judicial extension of the law of insider trading .	I-comparison	3495..3656
Courts and legislatures make decisions in very different ways and are each susceptible to very different kinds of errors .	B-expansion	3659..3780
After-the-fact judicial examination of an actor 's conduct has always been the common law 's method .	B-contingency	3781..3879
When only civil liability is involved , this method has the undeniable strengths of factual specificity and avoidance of overgeneralization .	I-contingency	3880..4019
Still , case-by-case retrospective decision making of this sort is vulnerable to the tunnel vision caused by a fixation on ad hoc ( and usually sleazy ) examples .	B-expansion	4020..4179
When a court decides that a particular actor 's conduct was culpable and so extends the definition of insider trading to reach this conduct , it does not see the potentially enormous number of other cases that will be covered by the expanded rule .	B-contingency	4180..4425
Thus , a court is poorly positioned to make judgments about the social utility of the expanded rule .	B-expansion	4428..4527
For example , in focusing on Mr. Freeman 's attempt to gain nonpublic information about a deal 's collapse , one does not naturally think about the reverse side of the coin : What if the rumor had been false ?	I-expansion	4528..4731
Can a security analyst call an investment banker to make certain that a seemingly improbable rumor is in fact false ?	O	4732..4848
In the past , not only would reputable professionals have rushed to check out such rumors with the company , but companies listed on the major stock exchanges were encouraged by the exchanges to respond openly to such inquiries from securities analysts .	B-comparison	4849..5100
Today , after Mr. Freeman 's plea , there is an uncertainty that is both unfair and inefficient .	I-comparison	5101..5194
In this light , the comparative advantages of legislative law-making become clear : ( 1 ) Before it acts , the legislature typically will hear the views of representatives of all those affected by its decision , not just the immediate parties before the court ; and ( 2 ) the legislature can frame " bright line " standards that create less uncertainty than the fact-bound decisions of courts .	O	5197..5579
Although legislative lines can result in under-inclusion ( which explains why the SEC has long resisted a legislative definition of insider trading ) , judicial lawmaking inevitably creates uncertainty because of the shadowy outer edges and implications of most judicial decisions .	O	5582..5860
At least when the stakes are high , uncertainty in turn results in overinclusion , as individuals do not dare to approach an uncertain line closely .	O	5861..6007
The federal mail and wire fraud statutes provide even better illustrations of the rapid evolution of a federal common law of white-collar crime .	B-expansion	6010..6154
In 1987 , the Supreme Court attempted in McNally v. U.S. to halt the inexorable expansion of these statutes by adopting a rule of strict construction for ambiguous criminal statues .	B-comparison	6155..6335
Yet , late last year , Congress effectively reversed this decision by enacting a one-sentence statute that defined fraud to include any scheme to deprive another of " the intangible right of honest services . "	I-comparison	6336..6541
At a stroke , this may criminalize all fiduciary breaches ( and possibly all misrepresentations by an agent or employee ) .	O	6542..6661
Such a statute illustrates the fundamental problem : Congress finds it is easier to pass sweepingly moralistic prohibitions , which the courts must thereafter interpret , than to engage in the difficult line-drawing distinctions that are inherently its responsibility .	B-expansion	6664..6929
We are confronted less with a judicial power grab than with a legislative giveaway .	B-contingency	6930..7013
Predictably , when confronted with morally dubious behavior , prosecutors will exploit the latitude such openended statutes give them .	B-comparison	7014..7146
Over the long run , however , sleazy cases will make bad law .	I-comparison	7147..7206
Mr. Coffee is a professor at Columbia Law School .	O	7209..7258
