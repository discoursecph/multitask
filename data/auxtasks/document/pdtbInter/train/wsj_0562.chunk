The rationale for responding to your customers' needs faster than the competition can is clear: Your company will benefit in terms of market share, customer satisfaction and profitability.	O	9..197
In fact, managers today are probably more aware of speed as a competitive variable than ever before.	B-contrast	198..298
However, for many, managing speed does not come naturally.	I-contrast	299..357
"Most of us grew up believing in the axioms `Haste makes waste' and `Don't cut corners, ' ideas that seem to run counter to the concept of managing speed," says Dean Cassell, vice president for product integrity at Grumman Corp.	O	360..588
But in the real world, you learn that speed and quality are not a trade-off.	B-alternative	589..666
Speed is a component of quality -- one of the things we must deliver to satisfy customers."	I-alternative	667..758
Companies that actually market speed as part of their service train their managers to lead and participate in teams that increase speed and improve quality in everyday operations.	B-cause	761..940
Managers learn to spot opportunities to increase customer satisfaction through speed, and shift some responsibility for analyzing, improving and streamlining work processes from themselves to teams of employees.	I-cause	941..1152
One team at the Federal Express Ground Operations station in Natick, Mass., focused on a particularly time-sensitive operation: the morning package sort.	B-restatement	1155..1308
Every morning, tractor-trailer trucks arrive at the Natick Ground Station from Boston's Logan Airport, carrying the day's package load.	B-conjunction	1309..1444
In peak periods that load may include 4,000 pieces.	B-conjunction	1445..1496
The packages must be sorted quickly and distributed to smaller vans for delivery, so couriers can be on the road by 8:35.	B-cause	1497..1618
No customer is present at the morning package sort, but the process is nevertheless critical to customer satisfaction.	I-cause	1619..1737
"We're committed to deliver the customer's package by a stated time, usually 10:30," notes Glenn Mortimer, a Federal Express courier who led the Natick team.	O	1740..1897
"The sooner our vans hit the road each morning, the easier it is for us to fulfill that obligation."	O	1898..1998
Following a problem-solving formula used by teams throughout Federal Express, members of the Natick team monitored their morning routine, carefully noting where and when the work group's resources were used effectively and where they were idle, waiting for others upstream in the process to send packages their way.	O	2001..2316
"We suspected there was downtime built into our process.	O	2319..2375
But we didn't know just where it was until we completed our data gathering," Mr. Mortimer says.	O	2376..2471
We used the data to redesign our sorting system and put our resources where they could do the most good.	B-conjunction	2472..2578
The team even created a points system to identify those couriers and subgroups that were doing the most to reduce package-sort cycle time.	I-conjunction	2579..2717
Winners of the friendly competition earn a steak dinner out with their spouses.	O	2718..2797
"Monitoring shows that the Natick team's new system really does reduce cycle time for the morning package sort," reports James Barksdale, chief operating officer at Federal Express.	O	2800..2981
The vans leave at least 15 minutes earlier, on average, than they used to.	B-conjunction	2982..3057
And service levels have increased to the point where they're consistently above 99%."	I-conjunction	3058..3143
A cross-functional team at Union Carbide's Tonawanda, N.Y., facility, which produces air-separation plants, followed a similar path to reduce manufacturing cycle time.	O	3146..3313
"The team included craftsmen from the shop floor as well as engineering, scheduling and purchasing personnel," reports Alan Westendorf, director of quality.	O	3316..3472
First, they produced a flowchart detailing the process by which an air-separation plant actually gets built.	B-asynchronous	3473..3582
Then they identified snags in the process."	I-asynchronous	3583..3626
The Tonawanda team determined that holdups for inspections were the main problem and identified which kinds of delays involved critical inspections and which were less critical or could be handled by workers already on the line.	B-asynchronous	3629..3857
The team then proposed modifications in their work process to management.	I-asynchronous	3858..3931
"The streamlined manufacturing process benefits our customers in at least two ways," Mr. Westendorf concludes.	O	3934..4044
"First, we have better quality assurance than ever, because the people building the product have taken on more responsibility for the quality of their own work.	O	4045..4205
Second, we trimmed more than a month off the time required to deliver a finished product."	O	4206..4296
At Grumman's Aircraft Systems Division, a cross-functional team reduced the cycle time required to produce a new business proposal for an important government contract.	B-entrel	4299..4467
The team was composed of representatives from engineering, manufacturing, corporate estimating, flight test, material, quality control, and other departments.	I-entrel	4468..4626
"We needed contributions from all these departments to generate the proposal," says Carl Anton, configuration-data manager for Grumman's A-6 combat aircraft program.	O	4629..4794
"But instead of gathering their input piecemeal, we formed the team, which reached consensus on the proposal objectives and produced a statement of work to guide all the functions that were involved."	O	4795..4995
Armed with this shared understanding and requisite background information, each department developed its specialized contribution to the proposal, submitting data and cost estimates on a closely managed schedule.	O	4998..5210
"We cleared up questions and inconsistencies very quickly, because the people who had the skills and perspective required to resolve them were part of the task team," Mr. Anton explains.	O	5211..5397
The team trimmed more than two months from the cycle time previously required to develop comparable proposals.	B-conjunction	5400..5510
"The team eliminated the crisis mentality that proposal deadlines can generate.	I-conjunction	5511..5590
The result was a more thoughtful, complete and competitive proposal," Mr. Anton concludes.	O	5591..5681
The successes achieved at Federal Express, Union Carbide and Grumman suggest that managing speed may be an underutilized source of competitive advantage.	O	5684..5837
Managers in all three companies recognize speed as a component of quality and a key to customer satisfaction.	B-cause	5838..5947
They effectively lead team efforts to reduce cycle time.	B-conjunction	5948..6004
And they prepare all their people to increase the speed and improve the quality of their own work.	I-conjunction	6005..6103
Mr. Labovitz is president of ODI, a consulting firm in Burlington, Mass.	O	6106..6178
