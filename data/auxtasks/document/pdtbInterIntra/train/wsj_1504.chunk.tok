Bob Stone stewed over a letter from his manager putting him on probation for insubordination .	root	9..102
Mr. Stone thought the discipline was unfair ; he believed that his manager wanted to get rid of him for personal reasons .	cause	103..223
Unable to persuade the manager to change his decision , he went to a " company court " for a hearing .	cause	224..322
At the scheduled time , Mr. Stone entered a conference room in a building near where he worked .	norel	325..419
After the three members of the court introduced themselves , the chairman of the panel said : " Go ahead and tell us what happened .	asynchronous	420..548
We may ask questions as you go along , or we may wait until the end . "	entrel	549..617
No lawyers or tape recorders were present .	norel	620..662
The only extra people were a couple of personnel specialists , one of whom knew Mr. Stone 's case intimately and would help fill in any facts needed to give the court the full picture .	conjunction	663..845
Over a cup of coffee , Mr. Stone told his story .	norel	848..895
He talked about 20 minutes .	entrel	896..923
When he was through , the court members asked many questions , then the chairman said they would like to hear his manager 's side and talk to witnesses .	asynchronous	924..1073
The chairman promised Mr. Stone a decision within two weeks .	asynchronous	1074..1134
Bob Stone is a fictional name	norel	1137..1166
but the incident described is real .	contrast	1167..1203
It happened at Northrop Corp. in Los Angeles .	restatement	1204..1249
The court is called the Management Appeals Committee , or just " MAC	entrel	1250..1316
" and it is likely to hear a couple of dozen cases a year .	conjunction	1317..1375
Alter some details of this example	norel	1378..1412
and it could be taking place today at Federal Express in Memphis , the Defense and Underseas Systems divisions of Honeywell in Minneapolis , a General Electric plant in Columbia , Md. , or a number of other companies .	condition	1413..1626
These firms are pioneers in a significant new trend in the corporate world : the rise of what I call corporate due process .	cause	1627..1749
Although corporate due process is practiced today in few companies -- perhaps 40 to 60 -- it is one of the fastest developing trends in industry .	restatement	1750..1895
In the coming decade a majority of people-oriented companies are likely to adopt it .	restatement	1896..1980
Corporate due process appeals to management for a variety of reasons .	norel	1983..2052
It reduces lawsuits from disgruntled employees and ex-employees , with all that means for reduced legal costs and better public relations .	restatement	2053..2190
It helps to keep out unions .	list	2191..2219
It increases employee commitment to the company , with all that means for efficiency and quality control .	list	2220..2324
What must your management team do to establish corporate due process ?	cause	2325..2394
Here are four key steps :	norel	2395..2419
1 . Make sure you have a strong personnel department .	norel	2422..2474
It must be able to handle most of the complaints that can not be solved in the trenches by managers and their subordinates	cause	2475..2596
else the company court or adjudicators will be inundated with cases .	alternative	2597..2666
At Polaroid , the Personnel Policy Planning Committee may hear only about 20 cases a year	instantiation	2667..2755
the rest of the many hundreds of complaints are resolved at earlier stages .	contrast	2756..2832
At TWA , the System Board of Adjustment hears 50 to 75 cases a year , only a fraction of the complaints brought to personnel specialists .	list	2833..2968
At Citicorp , the Problem Review Board may hear only 12 or so cases because of personnel 's skill in complaint-resolution .	list	2969..3089
In a typical year , up to 20 % of the work force goes to personnel specialists with complaints of unfair treatment .	norel	3092..3205
In a large company that means many hundreds of complaints for personnel to handle .	restatement	3206..3288
2 . Formally or informally , train all your managers and supervisors in the company 's due-process approach .	norel	3291..3396
See that they know company personnel policy backwards and forwards	list	3397..3463
for it is the " law " governing company courts and adjudicators .	cause	3464..3527
Coach them in handling complaints	list	3528..3561
so that they can resolve problems immediately .	cause	3562..3608
In case managers and personnel specialists are unsuccessful and subordinates take their complaints to a company court or adjudicator , teach managers to accept reversals as a fact of business life , for in a good due-process system they are bound to happen .	list	3609..3864
In the 15 companies I studied , reversal rates range on the average from 20 % to 40 % .	instantiation	3865..3948
3 . Decide whether you want a panel system or a single adjudicator .	norel	3951..4017
A panel system like that in the Bob Stone example enjoys such advantages as high credibility and , for the panelists , mutual support .	restatement	4018..4150
An adjudicator system -- that is , an investigator who acts first as a fact-finder and then switches hats and arbitrates the facts -- has such advantages as speed , flexibility and maximum privacy .	contrast	4151..4346
International Business Machines and Bank of America are among the companies using the single-adjudicator approach .	entrel	4347..4461
4 . Make your due-process system visible .	norel	4464..4504
It wo n't do any good for anybody	cause	4505..4537
unless employees know about it .	alternative	4538..4569
Most managements hesitate to go all out in advertising their due-process systems for fear of encouraging cranks and chronic soreheads to file complaints .	contrast	4570..4723
On the other hand , they make sure at a minimum that their systems are described in their employee handbooks and talked up by personnel specialists .	contrast	4724..4871
Smith-Kline Beecham goes further and sometimes features its grievance procedure in closed-circuit TV programs .	entrel	4872..4982
Naturally , one of the best ways to guarantee visibility for your due-process system is for top management to support it .	norel	4985..5105
At IBM , the company 's Open Door system is sometimes the subject of memorandums from the chief executive .	instantiation	5106..5210
Federal Express goes further in this respect than any company I know of with both Frederick Smith and James Barksdale , chief executive and chief operating officer , respectively , sitting in on the Appeals Board almost every Tuesday to decide cases .	conjunction	5211..5458
Mr. Ewing is a consultant based in Winchester , Mass. , and author of " Justice on the Job : Resolving Grievances in the Nonunion Workplace " ( Harvard Business School Press , 1989 ) .	norel	5461..5636
