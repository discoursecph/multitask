These are the last words	(Topic-Drift(elaboration-additional(restatement(consequence(background(elaboration-additional(antithesis(elaboration-general-specific(elaboration-additional(temporal-before(elaboration-object-attribute
Abbie Hoffman ever uttered , more or less ,	elaboration-object-attribute)
before he killed himself .	temporal-before)
And You Are There , sort of :	elaboration-additional)
ABBIE :	(Sequence(elaboration-additional(elaboration-additional(elaboration-additional(attribution
`` I 'm OK , Jack .	(restatement
I 'm OK . ''	restatement))
( listening )	(circumstance
`` Yeah .	circumstance))
I 'm out of bed .	(elaboration-additional(restatement(evidence
I got my feet on the floor .	evidence)
Yeah .	restatement)
Two feet .	elaboration-additional))
I 'll see you Wednesday ?	(Question-Answer
. . . Thursday . ''	Question-Answer))
He listens impassively .	(Sequence
ABBIE	(Sequence(attribution(elaboration-additional
( cont'd. ) :	elaboration-additional)
`` I 'll always be with you , Jack .	(elaboration-additional
Do n't worry . ''	elaboration-additional))
Abbie lies back	(List
and leaves the frame empty .	List)))))
Of course that was n't the actual conversation	(elaboration-object-attribute
the late anti-war activist , protest leader and founder of the Yippies ever had with his brother .	elaboration-object-attribute))
It 's a script	(elaboration-object-attribute
pieced together from interviews by CBS News for a re-enactment , a dramatic rendering by an actor of Mr. Hoffman 's ultimately unsuccessful struggle with depression .	elaboration-object-attribute))
The segment is soon to be broadcast on the CBS News series `` Saturday Night With Connie Chung , ''	background)
thus further blurring the distinction between fiction and reality in TV news .	consequence)
It is the New Journalism come to television .	restatement)
Ms. Chung 's program is just one of several network shows	(elaboration-additional(elaboration-additional(elaboration-object-attribute(elaboration-additional
( and many more in syndication )	elaboration-additional)
that rely on the controversial technique	(elaboration-object-attribute
of reconstructing events ,	(means
using actors	(elaboration-object-attribute
who are supposed to resemble real people , living and dead .	elaboration-object-attribute))))
Ms. Chung 's , however , is said to be the only network news program in history	(elaboration-object-attribute
to employ casting directors .	elaboration-object-attribute))
Abbie Hoffman in this case is to be played by Hollywood actor Paul Lieber ,	(elaboration-additional(elaboration-additional
who is n't new to the character .	elaboration-additional)
He was Mr. Hoffman in a 1979 Los Angeles production of a play	(elaboration-object-attribute
called `` The Chicago Conspiracy Trial . ''	elaboration-object-attribute))))
Television news , of course , has always been part show-biz .	(Contrast(elaboration-additional(interpretation(comparison(example(background(explanation-argumentative
Broadcasters have a healthy appreciation of the role	(elaboration-object-attribute
entertainment values play in captivating an audience .	elaboration-object-attribute))
But ,	(Same-Unit(attribution
as CBS Broadcast Group president Howard Stringer puts it ,	attribution)
the network now needs to `` broaden the horizons of nonfiction television ,	(elaboration-additional
and that includes some experimentation . ''	elaboration-additional)))
Since its premiere Sept. 16 , the show	(List(Same-Unit(elaboration-object-attribute
on which Ms. Chung appears	elaboration-object-attribute)
has used an actor to portray the Rev. Vernon Johns , a civil-rights leader , and one to play a teenage drug dealer .	Same-Unit)
It has depicted the bombing of Pan Am flight 103 over the Scottish town of Lockerbie .	(List
On Oct. 21 , it did a rendition of the kidnapping and imprisonment of Associated Press correspondent Terry Anderson ,	(elaboration-additional(elaboration-object-attribute
who was abducted in March 1985	(List
and is believed to be held in Lebanon .	List))
The production had actors	(elaboration-object-attribute
playing Mr. Anderson and former hostages David Jacobsen , the Rev. Benjamin Weir and Father Lawrence Jenco .	elaboration-object-attribute)))))
ABC News has similarly branched out into entertainment gimmickry .	(Comparison(elaboration-additional(example
`` Prime Time Live , '' a new show this season	(Same-Unit(elaboration-object-attribute
featuring Sam Donaldson and Diane Sawyer ,	elaboration-object-attribute)
has a studio audience	(elaboration-object-attribute
that applauds	(List
and that one night	(Same-Unit
( to the embarrassment of the network )	(consequence
waved at the camera like the crowd on `` Let 's Make a Deal . ''	consequence))))))
( ABC stops short of using an `` applause '' sign and a comic	(elaboration-additional(purpose
to warm up the audience .	purpose)
The stars do that themselves . )	elaboration-additional))
NBC News has produced three episodes of an occasional series	(elaboration-object-attribute(elaboration-additional(elaboration-object-attribute(elaboration-object-attribute
produced by Sid Feders	elaboration-object-attribute)
called `` Yesterday , Today and Tomorrow , ''	elaboration-object-attribute)
starring Maria Shriver , Chuck Scarborough and Mary Alice Williams ,	elaboration-additional)
that also gives work to actors .	elaboration-object-attribute)))
Call it a fad .	(Contrast
Or call it the wave of the future .	Contrast))
NBC 's re-creations are produced by Cosgrove-Meurer Productions ,	(elaboration-additional(circumstance(example(elaboration-object-attribute
which also makes the successful prime-time NBC Entertainment series `` Unsolved Mysteries . ''	elaboration-object-attribute)
The marriage of news and theater , if not exactly inevitable , has been consummated nonetheless .	example)
News programs ,	(Comparison(explanation-argumentative(Same-Unit
particularly if they score well in the ratings ,	(condition
appeal to the networks ' cost-conscious corporate parents	condition))
because they are so much less expensive to produce	(elaboration-additional(comparison
than an entertainment show is	comparison)
-- somewhere between $ 400,000 and $ 500,000 for a one-hour program .	elaboration-additional))
Entertainment shows tend to cost twice that .	Comparison))
Re-enactments have been used successfully for several seasons on such syndicated `` tabloid TV '' shows as `` A Current Affair , ''	(elaboration-additional(evaluation(elaboration-additional(elaboration-additional
which is produced by the Fox Broadcasting Co. unit of Rupert Murdoch 's News Corp .	elaboration-additional)
That show ,	(Same-Unit(elaboration-additional
whose host is Ms. Chung 's husband , Maury Povich ,	elaboration-additional)
has a particular penchant for grisly murders and stories	(elaboration-additional(elaboration-object-attribute
having to do with sex	elaboration-object-attribute)
-- the Robert Chambers murder case , the Rob Lowe tapes ,	(comment
what have you .	comment))))
Gerald Stone , the executive producer of `` A Current Affair , '' says ,	(attribution
`` We have opened eyes to being a little less conservative and more imaginative in how to present the news . ''	attribution))
Nowhere have eyes been opened wider than at CBS News .	(elaboration-additional(elaboration-additional(example(evidence
At 555 W. 57th St. in Manhattan , one floor below the offices of `` 60 Minutes , '' the most successful prime-time news program ever , actors wait in the reception area	(List(purpose
to audition for `` Saturday Night With Connie Chung . ''	purpose)
CBS News sends scripts to agents ,	(List(elaboration-object-attribute
who pass them along to clients .	elaboration-object-attribute)
The network deals a lot with unknowns ,	(Contrast(example
including Scott Wentworth ,	(Same-Unit(elaboration-object-attribute
who portrayed Mr. Anderson ,	elaboration-object-attribute)
and Bill Alton as Father Jenco ,	Same-Unit))
but the network has some big names to contend with , too .	(example
James Earl Jones is cast to play the Rev. Mr. Johns .	(List
Ned Beatty may portray former California Gov. Pat Brown in a forthcoming epsiode on Caryl Chessman , the last man	(elaboration-object-attribute
to be executed in California , in 1960 .	elaboration-object-attribute)))))))
`` Saturday Night '' has cast actors to appear in future stories	(elaboration-general-specific
ranging from the abortion rights of teen-agers to a Nov. 4 segment on a man	(elaboration-object-attribute
named Willie Bosket ,	(elaboration-object-attribute
who calls himself a `` monster ''	(List
and is reputed to be the toughest prisoner in New York .	List)))))
CBS News ,	(evaluation(Same-Unit(elaboration-additional
which as recently as two years ago fired hundreds of its employees in budget cutbacks ,	elaboration-additional)
now hires featured actors	(elaboration-object-attribute
beginning at $ 2,700 a week .	elaboration-object-attribute))
That is n't much	(Contrast(comparison
compared with what Bill Cosby makes ,	(List
or even Connie Chung for that matter	(elaboration-additional
( who is paid $ 1.6 million a year	(List
and who recently did a guest shot of her own on the sitcom `` Murphy Brown '' ) .	List))))
But the money is n't peanuts either , particularly for a news program .	Contrast)))
CBS News is also re-enacting the 1979 Three Mile Island nuclear accident in Middletown , Pa. , with something less than a cast of thousands .	(elaboration-additional(elaboration-additional
It is combing the town of 10,000 for about 200 extras .	(elaboration-additional
On Oct. 20 , the town 's mayor , Robert Reid , made an announcement on behalf of CBS during half-time at the Middletown High School football game	(Statement-Response(elaboration-object-attribute
asking for volunteers .	elaboration-object-attribute)
`` There was a roll of laughter through the stands , ''	(attribution
says Joe Sukle , the editor of the weekly Press and Journal in Middletown .	attribution))))
`` They 're filming right now at the bank down the street ,	(elaboration-additional(List
and they want shots of people	(Same-Unit(elaboration-object-attribute
getting out of cars	elaboration-object-attribute)
and kids on skateboards .	Same-Unit))
They are approaching everyone on the street	(Question-Answer(List
and asking	(attribution
if they want to be in a docudrama . ''	attribution))
Mr. Sukle says	(attribution
he would n't dream of participating himself :	(reason(restatement
`` No way .	restatement)
I think	(attribution
re-enactments stink . ''	attribution))))))))))
Though a re-enactment may have the flavor ,	(elaboration-additional(elaboration-additional(evaluation(consequence(elaboration-additional(evaluation
Hollywood on the Hudson it is n't .	evaluation)
Some producers seem tentative about the technique , squeamish even .	elaboration-additional)
So the results , while not news , are n't exactly theater either , at least not good theater .	consequence)
And some people do think	(example(attribution
that acting out scripts is n't worthy of CBS News ,	(elaboration-object-attribute
which once lent prestige to the network	(List
and set standards for the industry .	List)))
In his review of `` Saturday Night With Connie Chung , '' Tom Shales , the TV critic of the Washington Post and generally an admirer of CBS , wrote	(attribution
that	(Same-Unit
while the show is `` impressive , . . .	(antithesis
one has to wonder	(attribution
if this is the proper direction for a network news division	(elaboration-object-attribute
to take . ''	elaboration-object-attribute)))))))
Re-creating events has , in general , upset news traditionalists ,	(example(example
including former CBS News President Richard S. Salant and former NBC News President Reuven Frank , former CBS News anchorman Walter Cronkite and the new dean of the Columbia University Graduate School of Journalism , Joan Konner .	example)
Says she :	(attribution
`` Once you add dramatizations ,	(elaboration-additional(evaluation(interpretation(condition
it 's no longer news ,	condition)
it 's drama ,	interpretation)
and that has no place on a network news broadcast. . . .	evaluation)
They should never be on .	(restatement
Never . ''	restatement)))))
Criticism of the Abbie Hoffman segment is particularly scathing among people	(Contrast(elaboration-additional(example(elaboration-object-attribute
who knew and loved the man .	elaboration-object-attribute)
That includes his companion of 15 years , Johanna Lawrenson , as well as his former wife , Anita .	(elaboration-additional(elaboration-additional(elaboration-additional
Both women say	(attribution
they also find it distasteful	(attribution
that CBS News is apparently concentrating on Mr. Hoffman 's problems as a manic-depressive .	attribution)))
`` This is dangerous	(elaboration-additional(attribution(List
and misrepresents Abbie 's life , ''	List)
says Ms. Lawrenson ,	(elaboration-object-attribute
who has had an advance look at the 36-page script .	elaboration-object-attribute))
`` It 's a sensational piece about someone	(elaboration-object-attribute
who is not here	(purpose
to defend himself . ''	purpose))))
Mrs. Hoffman says	(elaboration-additional(attribution
that dramatization `` makes the truth flexible .	attribution)
It takes one person 's account	(List
and gives it authenticity . ''	List))))
CBS News interviewed Jack Hoffman and his sister , Phyllis , as well as Mr. Hoffman 's landlord in Solebury Township , Pa .	(elaboration-additional
Also Jonathan Silvers ,	(elaboration-additional(elaboration-object-attribute
who collaborated with Mr. Hoffman on two books .	elaboration-object-attribute)
Mr. Silvers says ,	(attribution
`` I wanted to be interviewed	(elaboration-additional(purpose
to get Abbie 's story out ,	purpose)
and maybe talking about the illness will do some good . ''	elaboration-additional)))))
The executive producer of `` Saturday Night With Connie Chung , '' Andrew Lack , declines to discuss re-creactions as a practice or his show , in particular .	(List(elaboration-additional
`` I do n't talk about my work , ''	(attribution
he says .	attribution))
The president of CBS News , David W. Burke , did n't return numerous telephone calls .	(List
One person close to the process says	(List(elaboration-additional(attribution
it would not be in the best interest of CBS News to comment on a `` work in progress , '' such as the Hoffman re-creation ,	attribution)
but says	(antithesis(attribution
CBS News is `` aware '' of the concerns of Ms. Lawrenson and Mr. Hoffman 's former wife .	attribution)
Neither woman was invited by CBS News to participate in a round-table discussion about Mr. Hoffman	(elaboration-object-attribute
that is to follow the re-enactment .	elaboration-object-attribute)))
Mr. Lieber , the actor	(explanation-argumentative(attribution(attribution(elaboration-object-attribute
who plays Mr. Hoffman ,	elaboration-object-attribute)
says	attribution)
he was concerned at first	(antithesis(attribution
that the script would `` misrepresent an astute political mind , one	(elaboration-object-attribute
that I admired , ''	elaboration-object-attribute))
but that his concerns were allayed .	antithesis))
The producers ,	(Same-Unit(attribution
he says ,	attribution)
did a good job	(elaboration-object-attribute
of depicting someone	(elaboration-object-attribute
`` who had done so much ,	(Contrast
but who was also a manic-depressive . ''	Contrast))))))))))))
