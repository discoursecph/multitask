( During its centennial year , The Wall Street Journal will report events of the past century that stand as milestones of American business history . )	O	9..156
Thomas Jefferson sold Congress on the idea of the decimal system for currency , thus saving Americans the headaches of pounds , shillings and pence .	B-contrast	159..305
But he struck out with the decimal system of metric weights and measures the French had invented .	B-alternative	306..403
Instead , Congress opted for the inches , feet and yards the colonists had brought with them .	I-alternative	404..495
Americans did n't dislike metrics ; they simply ignored them .	B-contrast	498..557
Scientists felt differently .	B-instantiation	558..586
In 1807 , the Swiss mathematician who headed the U.S. Coast and Geodetic Survey made an " iron meter " that he had brought from Europe the standard of measure .	B-entrel	587..743
By the end of the century scientists had embraced the system .	I-entrel	744..805
Businessmen took their cue from the engineers .	B-cause	808..854
When Congress finally passed the Metric Conversion Act in 1975 , industry was far ahead .	B-contrast	855..942
Because the law made compliance voluntary , it inspired little more than jokes .	B-instantiation	943..1021
( The press had a field day with questions about what would happen to " six-footer , " " yardstick " and " inchworm . " )	I-instantiation	1022..1133
Today , though the public is barely aware , much of U.S. industry , particularly companies manufacturing or selling overseas , have made metrics routine .	B-instantiation	1136..1285
General Motors , for example , uses metric terms for its automobile bodies and power trains .	B-concession	1286..1376
( In auto advertising , however , items such as wheelbases are still described in inches . )	B-list	1377..1464
Farm-machine makers such as Caterpillar and Deere work in the metric system .	B-list	1465..1541
The liquor industry went metric 10 years ago .	I-list	1542..1587
The Pentagon has led the charge , particularly as military alliances spread world-wide .	O	1590..1676
New weapons systems will be around until the next century , notes John Tascher , the Defense Department 's metric coordinator .	O	1677..1800
Still , like the auto makers , when dealing with Mr. Everyman the Pentagon sticks to the tried and true .	B-restatement	1803..1905
Soldiers and sailors are still measured in inches and pounds .	I-restatement	1906..1967
