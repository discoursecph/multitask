Every workday at 11 a.m. , 40-year-old Mike Sinyard dons cycling clothes , hops on a bike he keeps at his Morgan Hill , Calif. , office and sets out to cover a distance most people would travel only by car .	B-entrel	9..211
As many as 50 of his employees at Specialized Bicycle Components Inc. ride with him .	B-temporal	212..296
When they return to their desks at 1 p.m. , they have pedaled 20 miles .	I-temporal	297..367
Such fervor for cycling helped Mr. Sinyard build a creative company at the forefront of its industry .	B-entrel	370..471
Founded by bike enthusiasts rather than businessmen , Specialized spotted the appeal of fat-tired bikes that go almost anywhere and began mass-producing them in 1981 .	B-entrel	472..637
In the past five years , the company 's sales have grown to $ 80 million from $ 26 million .	B-entrel	638..725
Today , so-called mountain bikes account for two-thirds of the $ 2 billion spent annually on all bicycles in the U.S.	B-entrel	726..841
With 65 % of its sales coming from mountain bikes , Specialized is widely considered to be a market leader .	B-entrel	842..947
( Accessories , largely for mountain-bike users , account for much of the rest of sales . )	I-entrel	948..1034
But today , the company needs its entrepreneurial spirit more than ever .	B-contingency	1037..1108
One large competitor after another is leaping into the booming market Specialized helped create , turning out mountain bikes with such well-known names as Schwinn , Peugeot , Raleigh and Nishiki .	I-contingency	1109..1301
Thus , Mr. Sinyard 's company must innovate more than ever to stay ahead of them , by developing new products specifically for mountain biking .	B-comparison	1304..1444
At the same time , though , it must become more structured to better manage its growth .	B-expansion	1445..1530
Accomplishing both will be a balancing act as challenging as riding a unicycle .	I-expansion	1531..1610
It is a problem common to small companies that have grown fast -- especially when their success attracts big-time competitors .	B-entrel	1613..1739
" The big word around Specialized is passion , " says Erik Eidsmo , a former ski-industry executive whom Mr. Sinyard recruited from Citicorp to run marketing and sales .	I-entrel	1740..1904
" What I hope to bring to this is another word : process .	B-entrel	1905..1960
That 's my challenge .	B-expansion	1961..1981
It 's Mike 's challenge as well . "	I-expansion	1982..2013
Mr. Eidsmo is one of several key people from outside the cycling industry who were hired to bring the free-wheeling , fast-growing company under tighter control .	O	2016..2176
" We had a lot of problems , " Mr. Sinyard says .	O	2177..2222
While the company 's sales were soaring , " We still had a system that was probably appropriate for $ 10 million to $ 20 million in sales . "	O	2223..2357
Adds Mr. Eidsmo , " What felt good that day was done that day . "	O	2358..2419
Since his arrival in May , Mr. Eidsmo has put in place techniques learned while working for Citicorp , such as management-by-objective , detailed project plans and forecasts of company sales and product trends .	O	2422..2629
" We 're finally getting -- and it 's been very painful -- some understanding of what the company 's long-term horizon should begin to look like , " Mr. Eidsmo says .	O	2630..2789
" But it 's risky , " he says of Specialized 's attempt to adopt a corporate structure .	O	2790..2872
" You do n't want to lose the magic " of the company 's creative drive .	O	2873..2940
Hoping to stay ahead of the pack , the company is emphasizing innovation .	B-expansion	2943..3015
At a recent trade show , convention-goers lined up to view a new Specialized bike frame that weighs just 2.7 pounds -- a pound less than the lightest mountain-bike frame on the market .	B-expansion	3016..3199
By replacing the frame 's steel lugs with titanium ones , Mr. Sinyard 's company plans to make its next generation of frames even lighter .	I-expansion	3200..3335
At the trade show , Specialized also unveiled a revolutionary three-spoked bike wheel developed jointly by Specialized and Du Pont Co .	B-entrel	3338..3471
Made of space-age materials , the wheel spokes are designed like airplane wings to shave 10 minutes off the time of a rider in a 100-mile race , the company claims .	B-entrel	3472..3634
It currently costs $ 750 , though Mr. Sinyard thinks the price can be reduced within three years to between $ 200 and $ 250 .	B-temporal	3635..3755
He was able to slash the price of the company 's least expensive mountain bike to $ 279 from $ 750 in	I-temporal	3756..3854
But demands on the company 's creativity are certain to grow .	B-contingency	3857..3917
Competition is intensifying as larger companies invade a mountain-bike market Mr. Sinyard 's company once had virtually all to itself .	I-contingency	3918..4051
U.S. Cycling Federation official Philip Milburn says mountain biking is " growing at such a monstrous rate that a lot of companies are getting into this . "	O	4052..4205
One especially coveted Specialized market the new players are targeting is mountain-bike accessories , which Mr. Eidsmo calls " the future of our business . "	O	4208..4362
Accessories not only sell faster than whole bikes , they also offer profit margins nearly double the 25 % to 30 % or so on sales of complete cycles .	B-contingency	4363..4508
To get a piece of the business , Nike Inc. , Beaverton , Ore. , introduced a line of mountain-bike shoes .	B-expansion	4509..4610
About a month ago , Michelin Tire Corp. , Greenville , S.C. , began selling mountain-bike tires , for years a Specialized stronghold .	I-expansion	4611..4739
Competition in the sale of complete bikes is heating up too .	B-expansion	4742..4802
Trek Bicycle Corp. , which accounts for one-quarter of the $ 400 million in annual sales at its Milwaukee-based parent , Intrepid Corp. , entered the mountain-bike business in 1983 .	B-entrel	4803..4980
Trek previously made only traditional road bikes , but " it did n't take a rocket scientist to change a road bike into a mountain bike , " says Trek 's president , Dick Burke .	B-entrel	4981..5149
The segment now makes up roughly two-thirds of his company 's total sales .	I-entrel	5150..5223
At Giant Bicycle Inc. , Rancho Dominguez , Calif. , sales have tripled since the company entered the U.S. mountain-bike business in 1987 .	B-entrel	5226..5360
A subsidiary of a Taiwanese holding company with world-wide sales of $ 150 million , Giant is one example of the sudden globalization of Mr. Sinyard 's once-cozy market niche .	B-expansion	5361..5533
Schwinn Bicycle Co. , Chicago , established joint ventures with bike companies in mainland China and Hungary to sell bikes .	B-expansion	5534..5655
In the past year , Derby International Corp. , Luxembourg , has acquired such major brands as Peugeot , Raleigh and Nishiki .	I-expansion	5656..5776
In response to the internationalization of the business , Mr. Sinyard 's company is replacing independent distributors overseas with wholly owned subsidiaries .	B-contingency	5779..5936
The move will cut out the cost of a middleman and give Specialized more control over marketing and sales .	I-contingency	5937..6042
But as Bill Austin , Giant 's president , puts it , " With some of the bigger players consolidating their strength , the game has changed .	O	6043..6175
