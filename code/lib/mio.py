import glob
import numpy as np
import sys

def read_discourse_chunk_file(folder_name):
    """
    read in discourse chunking files in given folder *.c
    In the second step of a reorganization that began earlier this year, Boeing Co. said it will create a Defense and Space Group to consolidate several divisions  B-conjunction   0

    sentence \t label  \t sentence_id
    """
    current_sentences = []
    current_tags = []
    types = ("/*.chunk.tok", "/*.seq.const", "/*.seq.dep", "/*.seq.nuc.const", "/*.seq.nuc.dep", "/*.tml.aspect", "/*.tml.factuality", "/*.tml.modality", "/*.tml.polarity", "/*.tml.tense", "/*.pos.seq", "/*.coref", "/*.ccg.seq", "/*.speech")
    files_grabbed = []
    for files in types:
        files_grabbed.extend( glob.glob(folder_name+files) )
    #for file_name in glob.glob(folder_name+"/*.chunk.tok") or file_name in glob.glob(folder_name+"/*.seq.const"):
    for file_name in files_grabbed:
        for line in open(file_name):
            line = line.strip()
            sentence_label = line.split("\t")
            # chunking files have a 3rd field, but not.seq files
            sentence = sentence_label[0]
            label = sentence_label[1]
            current_sentences.append(sentence.split())
            current_tags.append(label)
        yield (current_sentences, current_tags)
        current_sentences = []
        current_tags = []

    # check for last one
    if current_tags != []:
        yield (current_sentences, current_tags)

def load_embeddings_file(file_name, sep=" ",lower=False):
    ## load embeddings
    emb={}
    for line in open(file_name):
        fields = line.split(sep)
        vec = [float(x) for x in fields[1:]]
        word = fields[0]
        if lower:
            word = word.lower()
        emb[word] = vec

    print("loaded pre-trained embeddings (word->emb_vec) size: {} (lower: {})".format(len(emb.keys()), lower))
    return emb, len(emb[word])

if __name__=="__main__":

    t2i = {}
    for sents, tags in read_discourse_chunk_file("../data/rstdt_chunk_I/origin/TRAIN/"):
        for s, tag in zip(sents,tags):
            #print("{}\t{}".format(s,t))
            if tag not in t2i:
                t2i[tag] = len(t2i)

    print(t2i)
    for sents, tags in read_discourse_chunk_file("../data/rstdt_chunk_I/origin/TEST/"):
         for s, tag in zip(sents,tags):
             if tag not in t2i:
                 print("unknown tag!!", tag)
