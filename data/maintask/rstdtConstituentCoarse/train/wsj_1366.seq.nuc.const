It was the kind of snubbing	(NS-Summary-Evaluation-Topic(NS-Elaboration(NS-Cause(NS-Elaboration(SN-Cause(NS-Elaboration(NS-Elaboration
rarely seen within the Congress , let alone within the same party .	NS-Elaboration)
Sen. Alan Cranston trekked over to the House side of Capitol Hill a few days ago	(NN-Comparison(NS-Cause(NN-Joint
and volunteered his testimony to fellow Democrat Rep. Henry Gonzalez .	NN-Joint)
It was offered as an expression of cooperation to Mr. Gonzalez ,	(NS-Elaboration
who is investigating the $ 2.5 billion failure of Lincoln Savings & Loan Association .	NS-Elaboration))
But instead of thanks , Sen. Cranston was treated with cool formality .	(NS-Elaboration
`` Every witness receives a formal subpoena , ''	(NS-Attribution
Rep. Gonzalez told him .	NS-Attribution))))
Seldom have House hearings caused so much apprehension in the Senate ,	(NS-Elaboration
where California Sen. Cranston and four other senators were already writhing in the glare of unfavorable publicity over the alleged looting of Lincoln by their friend and political benefactor , Charles Keating Jr. , principal stockholder of Lincoln 's parent company , American Continental Corp. of Phoenix , Ariz .	NS-Elaboration))
At the first day of the House Banking Committee 's hearings last week , William Seidman , chairman of the Resolution Trust Corp. , the federal agency	(NS-Summary-Evaluation-Topic(NS-Elaboration(NS-Temporal(SN-Attribution(NN-Textual-organization(NS-Elaboration
created	(NS-Cause
to sell sick thrifts ,	NS-Cause))
said	NN-Textual-organization)
the agency is investigating	(SN-Attribution
whether Lincoln made illegal political contributions .	SN-Attribution))
Mr. Keating arranged nearly $ 1 million in donations to Sen. Cranston and his various political causes , and hundreds of thousands more to other lawmakers .	NS-Temporal)
Future witnesses include a former federal S & L regulator	(NS-Elaboration
who has accused the five senators of attempting to `` subvert '' the regulatory process	(NS-Joint
by intervening on behalf of Mr. Keating .	NS-Joint)))
Unlike many lawmakers , Chairman Gonzalez says	(SN-Cause(NS-Summary-Evaluation-Topic(SN-Attribution
he considers intervening with regulators to be improper .	SN-Attribution)
`` When you reach a point	(NS-Attribution(SN-Summary-Evaluation-Topic(NS-Elaboration
where a policy-making body is trying to shape administrative decisions ,	NS-Elaboration)
then that 's a no-no in my book , ''	SN-Summary-Evaluation-Topic)
the Texas lawmaker says .	NS-Attribution))
And he has attached himself to the Lincoln story tenaciously .	(NS-Elaboration
`` Unless the questions are answered ,	(SN-Cause
I will keep on going . ''	SN-Cause)))))
Lawmakers often are reluctant to embarrass colleagues , even those of opposing political parties .	(NN-Comparison(NS-Elaboration
In the recent Housing and Urban Development Department scandal , for example , Rep. Thomas Lantos , the California Democrat	(NN-Textual-organization(NS-Elaboration
who led the hearings ,	NS-Elaboration)
tiptoed through embarrassing disclosures about HUD grants	(NS-Elaboration
secured by Sen. Alfonse D'Amato , a New York Republican .	NS-Elaboration)))
But Chairman Gonzalez is a genuine maverick .	(NS-Elaboration(NS-Comparison
He comes from the same political line as Wright Patman , a bank-baiting Texas populist	(NS-Elaboration
who chaired the Banking Committee until 1974 .	NS-Elaboration))
Mr. Gonzalez is also a stickler for ethical standards	(NS-Cause(NS-Elaboration
who refuses to accept honorariums	(NN-Joint
and who believes in conducting official business in the open .	NN-Joint))
Early in his political career , as a city councilman in San Antonio , he walked out of a meeting	(NS-Joint(NS-Temporal
when political supporters asked	(SN-Attribution
that the police chief be replaced ,	SN-Attribution))
denouncing the closed-door affair publicly as a `` bat-roost meeting . ''	NS-Joint)))))
The immediate target of Rep. Gonzalez 's inquiry is Danny Wall , chairman of the Office of Thrift Supervision .	(NN-Comparison(SN-Cause(NS-Elaboration(NS-Elaboration(NS-Cause
As the principal regulator of the thrift industry , Mr. Wall delayed seizing Lincoln S & L	(NS-Temporal
for more than two years after his staff told him	(SN-Attribution
that the California thrift was insolvent	(NN-Joint
and that potential losses to taxpayers were growing rapidly .	NN-Joint))))
Rep. Gonzalez seems primed to lash out at Mr. Wall	(NS-Temporal
when hearings resume Thursday with testimony by two federal regulators from San Francisco , William Black and Mike Patriarca .	(NS-Temporal
Mr. Wall relieved them of responsibility	(NS-Cause
for supervising Lincoln in 1988 .	NS-Cause))))
Mr. Gonzalez expressed concern over a report	(SN-Summary-Evaluation-Topic(NS-Elaboration
that the two had been summoned to Washington by Mr. Wall last week	(NS-Cause
to discuss their testimony in advance .	NS-Cause))
`` I think	(NS-Attribution(SN-Attribution
he is trying to improperly influence a witness ,	(NN-Comparison
and by God I 'm not going to tolerate it , ''	NN-Comparison))
he says .	NS-Attribution)))
Mr. Wall , however , is a self-proclaimed `` child of the Senate '' and former staff director of its Banking Committee .	(NS-Cause(NS-Elaboration(NS-Cause(NS-Cause(NS-Summary-Evaluation-Topic(NS-Elaboration(SN-Cause
An inquiry into his handling of Lincoln S & L inevitably will drag in Sen. Cranston and the four others , Sens. Dennis DeConcini	(NN-Textual-organization(NS-Elaboration
( D. , Ariz. ) ,	NS-Elaboration)
John McCain	(NN-Textual-organization(NS-Elaboration
( R. , Ariz. ) ,	NS-Elaboration)
John Glenn	(NN-Textual-organization(NS-Elaboration
( D. , Ohio )	NS-Elaboration)
and Donald Riegle	(NS-Elaboration
( D. , Mich. ) .	NS-Elaboration)))))
They all attended a meeting in April 1987	(NS-Elaboration
questioning	(SN-Attribution
why a federal audit of Lincoln S & L had dragged on for two years .	SN-Attribution)))
`` I 'm certain	(NS-Attribution(SN-Attribution
that in the course of the hearings the names { of the senators } will be brought out , ''	SN-Attribution)
Mr. Gonzalez says .	NS-Attribution))
This is raising eyebrows .	(NS-Cause
`` When I first got a glimpse at the witness list ,	(NN-Joint(NS-Elaboration(NS-Attribution(SN-Temporal
I could n't believe	(SN-Attribution
that they were going to go ahead and do this , ''	SN-Attribution))
says Michael Waldman , director of Congress Watch , a consumer group .	NS-Attribution)
`` There are some witnesses	(NS-Elaboration
who will be forced to testify about their meetings with senators . ''	NS-Elaboration))
And a Democratic aide to a Banking Committee member remarks ,	(SN-Attribution
`` I too am astounded by it ,	(NS-Cause
because Gonzalez has certainly placed a lot of Democratic senators in a very bad position . ''	NS-Cause)))))
All the senators say	(NS-Elaboration(SN-Attribution
they merely were trying to ensure fairness for a constituent .	SN-Attribution)
Mr. Keating lives in Phoenix ,	(NN-Joint
and the California thrift 's parent is an Ohio-chartered corporation with holdings in Michigan .	NN-Joint)))
Chairman Gonzalez expresses sympathy for Sen. Riegle , his counterpart as chairman of the Senate Banking Committee .	(SN-Comparison(NS-Elaboration(SN-Comparison(NS-Summary-Evaluation-Topic
`` He 's wise ,	(NS-Attribution(NN-Joint
he 's good	(NN-Joint
and I know	(SN-Attribution
he 's an honest man , ''	SN-Attribution)))
the Texan says .	NS-Attribution))
But at the same time , Mr. Gonzalez has n't forgotten a confrontation over Mr. Wall during House-Senate negotiations over S & L bailout legislation during the summer .	SN-Comparison)
The Senate negotiators included Sens. Cranston and Riegle and Mr. Wall 's principal sponsor , Republican Sen. Jake Garn of Utah .	(NS-Temporal(NS-Elaboration
They were willing to trade important provisions in the bailout legislation	(NN-Joint(NS-Cause
to preserve Mr. Wall 's job	(NN-Joint
and to avoid a reconfirmation hearing	(NS-Elaboration
in which he would be called upon to testify about Lincoln S & L .	NS-Elaboration)))
Most importantly , the Senate traded away the Bush administration 's controversial plan	(NS-Elaboration
to finance the bailout ,	(NS-Elaboration
which was partly reinstated later .	NS-Elaboration))))
At the time , Mr. Gonzalez said	(SN-Attribution
several senators told him	(SN-Attribution
that they `` could get some roadblocks out of the way	(NS-Cause
if there could be some understanding on Garn 's insistence on Wall . ''	NS-Cause)))))
Now Mr. Gonzalez is holding the equivalent of reconfirmation hearings anyway , under the guise of the Lincoln investigation .	(NS-Summary-Evaluation-Topic
`` In a way , that 's what this is , ''	(NS-Attribution
Mr. Gonzalez concedes .	NS-Attribution))))
Even some House Banking Committee members could suffer from the fallout .	(NS-Elaboration
Mr. Keating raised $ 20,000 for Rep. Doug Barnard 's 1986 re-election campaign	(NS-Elaboration(NS-Temporal
while the Georgia Democrat was taking his side against regulators	(NS-Elaboration
who wanted to curb risky investments and wholesale deposit brokering .	NS-Elaboration))
He recently voted `` present ''	(NN-Temporal(NS-Temporal
when the committee authorized a subpoena	(NS-Elaboration
to compel Mr. Keating to testify ,	NS-Elaboration))
then changed his vote to yes .	NN-Temporal)))))
But the chairman 's supporters have the upper hand	(NS-Elaboration(NS-Temporal
as federal regulators press a $ 1.1 billion fraud suit against Mr. Keating and others .	NS-Temporal)
Rep. Jim Leach	(NN-Joint(NS-Elaboration(SN-Attribution(NN-Textual-organization(NS-Elaboration
( R. , Iowa )	NS-Elaboration)
says	NN-Textual-organization)
the Lincoln S & L affair is `` the biggest bank heist in history , ''	SN-Attribution)
and adds :	(SN-Attribution
`` The great question	(NN-Textual-organization(NS-Elaboration
that remains to be resolved	NS-Elaboration)
is whether we have a congressional Watergate in the making . ''	NN-Textual-organization)))
A witness	(NN-Joint(NN-Summary-Evaluation-Topic(SN-Attribution(NN-Textual-organization(NS-Elaboration
set to testify on Thursday	NS-Elaboration)
was quoted in a news report over the weekend	NN-Textual-organization)
as saying	(SN-Attribution
Lincoln `` laundered '' campaign contributions illegally .	SN-Attribution))
But the witness , William Crawford , California 's chief state thrift regulator , denies saying that .	(SN-Comparison(NS-Elaboration
`` I do n't know	(NS-Attribution(NS-Cause(SN-Attribution
whether it was done properly or not ,	SN-Attribution)
because I 'm not a lawyer ,	NS-Cause)
`` he said in a telephone interview yesterday .	NS-Attribution))
But he said	(SN-Attribution
he is prepared to testify	(SN-Attribution
that executives of Lincoln and its parent corporation got unusually high salaries and frequent calls	(NS-Elaboration
directing them to make specific contributions .	NS-Elaboration)))))
The committee also has summoned Mr. Wall 's predecessor , Edwin Gray .	(NS-Elaboration
He has characterized the five senators ' roles as `` tantamount to an attempt	(NN-Joint(NS-Elaboration
to subvert the . . . regulatory process , ''	NS-Elaboration)
and he is n't expected to back down	(NS-Comparison
even though the five senators have disputed his account of a 1987 meeting .	NS-Comparison))))))))
So the senators must brace themselves .	(NS-Elaboration
Sen. Cranston ,	(SN-Attribution(NN-Textual-organization
as he returned to the capital last week from a one-day trip	(SN-Temporal(NS-Elaboration
to inspect earthquake damage in San Francisco ,	NS-Elaboration)
sighed to an aide :	SN-Temporal))
`` Well , back to Keatingland . ''	SN-Attribution)))
