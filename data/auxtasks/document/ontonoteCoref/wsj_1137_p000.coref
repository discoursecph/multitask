The Voting Rights Act of 1965 was enacted to keep the promise of the Fifteenth Amendment and enable Southern blacks to go to the polls , unhindered by literacy tests and other exclusionary devices .	ROOT	0
Twenty - five years later , the Voting Rights Act has been transformed by the courts and the Justice Department into a program of racial gerrymandering designed to increase the number of blacks and other minorities -- Hispanics , Asians and native Americans -- holding elective office .	COREF_PREV	1
In the 1980s , the Justice Department and lower federal courts that enforce the Voting Rights Act have required state legislatures and municipal governments to create the maximum number of `` safe '' minority election districts -- districts where minorities form between 65 % and 80 % of the voting population .	COREF_PREV	2
The program has even been called upon to create `` safe '' white electoral districts in municipalities where whites are the minority .	COREF_PREV	3
Although Section 2 of the act expressly disclaims requiring that minorities win a proportional share of elective offices , few municipal and state government plans achieve preclearance by the Justice Department or survive the scrutiny of the lower federal courts unless they carve out as many solidly minority districts as possible .	COREF_PREV	4
The new goal of the Voting Rights Act -- more minorities in political office -- is laudable .	COREF_PREV	5
For the political process to work , all citizens , regardless of race , must feel represented .	NONE	6
One essential indicator that they are is that members of minority groups get elected to public office with reasonable frequency .	COREF_PREV	7
As is , blacks constitute 12 % of the population , but fewer than 2 % of elected leaders .	NONE	8
But racial gerrymandering is not the best way to accomplish that essential goal .	NONE	9
It is a quick fix for a complex problem .	COREF_PREV	10
Far from promoting a commonality of interests among black , white , Hispanic and other minority voters , drawing the district lines according to race suggests that race is the voter 's and the candidate 's most important trait .	NONE	11
Such a policy implies that only a black politician can speak for a black person , and that only a white politician can govern on behalf of a white one .	NONE	12
Examples of the divisive effects of racial gerrymandering can be seen in two cities -- New York and Birmingham , Ala .	NONE	13
When they reapportion their districts after the 1990 census , every other municipality and state in the country will face this issue .	COREF_PREV	14
New York City :	NONE	15
Racial gerrymandering has been a familiar policy in New York City since 1970 , when Congress first amended the Voting Rights Act to expand its reach beyond the Southern states .	COREF_OTHER	16
In 1972 , the Justice Department required that the electoral map in the borough of Brooklyn be redrawn to concentrate black and Hispanic votes , despite protests that the new electoral boundaries would split a neighborhood of Hasidic Jews into two different districts .	COREF_OTHER	17
This year , a commission appointed by the mayor to revise New York 's system of government completed a new charter , expanding the City Council to 51 from 35 members .	COREF_OTHER	18
Sometime in 1991 , as soon as the 1990 census becomes available , a redistricting panel will redraw the City Council district lines .	COREF_PREV	19
The Charter Revision Commission has made it clear that in response to the expectations of the Justice Department and the commission 's own commitment to enhancing minority political leadership , the new district lines will be drawn to maximize the number of solidly minority districts .	COREF_OTHER	20
Blacks and Hispanics currently make up 38 % of the city 's population and hold only 25 % of the seats on the council .	COREF_OTHER	21
Several of the city 's black leaders , including Democratic mayoral nominee David Dinkins , have spoken out for racial gerrymandering to accord blacks and Hispanics `` the fullest opportunity for representation . ''	COREF_PREV	22
In this connection , it is important to note that several members of New York 's sitting City Council represent heterogeneous districts that bring together sizable black , Hispanic , and non-Hispanic white populations -- Carolyn Maloney 's 8th district in northern Manhattan and the south Bronx and Susan Alter 's 25th district in Brooklyn , for example .	COREF_PREV	23
To win their seats on the council , these political leaders have had to listen to all the voices in their district and devise public policies that would benefit all .	COREF_PREV	24
Often they have found that the relevant issue is not race , but rather housing , crime prevention or education .	COREF_PREV	25
Birmingham , Ala. :	NONE	26
The unusual situation in Birmingham vividly illustrates the divisive consequences of carving out safe districts for racial minorities .	COREF_OTHER	27
In Birmingham , which is 57 % black , whites are the minority .	COREF_PREV	28
Insisting that they are protected by the Voting Rights Act , a group of whites brought a federal suit in 1987 to demand that the city abandon at - large voting for the nine member City Council and create nine electoral districts , including four safe white districts .	COREF_PREV	29
The white group argued that whites were not fully and fairly represented , because in city - wide elections only black candidates or white candidates who catered to `` black interests '' could win .	COREF_PREV	30
No federal court has ruled that the Voting Rights Act protects a white minority , but in June the Justice Department approved a districting plan for Birmingham that carves out three white - majority districts and six black - majority districts .	COREF_OTHER	31
Richard Arrington , Birmingham 's black mayor , lamented the consequences .	COREF_PREV	32
`` In the past , people who had to run for office had to moderate their views because they could n't afford to offend blacks or whites , '' he said .	NONE	33
`` Now you go to districts , you 're likely to get candidates whose views are more extreme , white and black , on racial issues . ''	NONE	34
Two hundred years ago , critics of the new United States Constitution warned that the electoral districts for Congress were too large and encompassed too many different economic interests .	COREF_OTHER	35
A small farmer and a seaport merchant could not be represented by the same spokesman , they said .	NONE	36
But James Madison refuted that argument in one of the most celebrated political treatises ever written , No. 10 of the Federalist Papers .	COREF_OTHER	37
Madison explained that a representative 's duty was to speak not for the narrow interests of one group but instead for the common good .	COREF_PREV	38
Large , heterogeneous election districts would encourage good government , said Madison , because a representative would be compelled to serve the interests of all his constituents and be servile to none .	COREF_PREV	39
Madison 's noble and unifying vision of the representative still can guide us .	COREF_PREV	40
As long as we believe that all Americans , of every race and ethnic background , have common interests and can live together cooperatively , our political map should reflect our belief .	NONE	41
Racial gerrymandering -- creating separate black and white districts -- says that we have discarded that belief in our ability to live together and govern ourselves as one people .	COREF_PREV	42
Ms. McCaughey is a constitutional scholar at the Center for the Study of the Presidency in New York .	COREF_PREV	43
