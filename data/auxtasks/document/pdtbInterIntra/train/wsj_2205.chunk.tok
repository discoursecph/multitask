Steve Clark , a Shearson Lehman Hutton Inc. trader , reported for work at 5 a.m. , two and a half hours before the usual Monday morning strategy meeting .	root	9..159
At Jefferies & Co. , J. Francis Palamara did n't reach the office until 5:30	norel	162..240
, but then he had been up most of the night at home .	contrast	241..293
" I had calls all night long from the States , " he said .	cause	294..348
" I was woken up every hour -- 1:30 , 2:30 , 3:30 , 4:30 .	restatement	349..402
People are looking for possible opportunities to buy , but nobody wants to stick their chin out . "	entrel	403..499
For many of London 's securities traders , it was a day that started nervously in the small hours .	norel	502..598
By lunchtime , the selling was at near-panic fever .	asynchronous	599..649
But as the day ended in a frantic Wall Street-inspired rally , the City breathed a sigh of relief .	contrast	650..747
So it went yesterday in the trading rooms of London 's financial district .	norel	750..823
In the wake of Wall Street 's plunge last Friday , the London market was considered especially vulnerable .	entrel	824..928
And before the opening of trading here yesterday , all eyes were on early trading in Tokyo for a clue as to how widespread the fallout might be .	conjunction	929..1072
By the time trading officially got under way at 9 a.m. , the news from Asia was in .	norel	1075..1157
And it left mixed signals for London .	conjunction	1158..1195
Tokyo stocks closed off a significant but less-than-alarming 1.8 % on thin volume	restatement	1196..1276
Hong Kong stocks declined 6.5 % in orderly trading .	contrast	1277..1328
At Jefferies ' trading room on Finsbury Circus , a stately circle at the edge of the financial district , desktop computer screens displayed the London market 's major barometer -- the Financial Times-Stock Exchange 100 Share Index .	norel	1331..1559
Red figures on the screens indicated falling stocks ; blue figures , rising stocks .	entrel	1560..1641
Right away , the reds outnumbered the blues , 80 to 20	entrel	1642..1694
as the index opened at 2076.8 , off 157.1 points , or 7 % .	synchrony	1695..1751
" I see concern , but I do n't see any panic , " said Mr. Palamara , a big , good-humored New York native who runs the 15-trader office .	norel	1754..1883
The Jefferies office , a branch of the Los Angeles-based firm , played it conservatively , seeking to avoid risk .	norel	1886..1996
" This is not the sort of market to have a big position in , " said David Smith , who heads trading in all non-U.S. stocks .	cause	1997..2116
" We tend to run a very tight book . "	cause	2117..2152
Jefferies spent most of its energies in the morning trying to match buyers and sellers , and there were n't many buyers .	entrel	2153..2271
" All the takeover stocks -- Scottish & Newcastle , B.A.T , DRG -- are getting pretty well pasted this morning , " Mr. Smith said .	norel	2274..2399
Seconds later , a 7,500-share " sell " order for Scottish & Newcastle came in .	asynchronous	2400..2475
For the third time in 15 minutes , a trader next to Mr. Smith left the no-smoking area to have a cigarette .	cause	2476..2582
On the screens , only two forlorn blue figures remained	conjunction	2583..2637
but the index had recovered a few points and was off about 140 .	contrast	2638..2702
" Because Tokyo did n't collapse , let 's pick up a little stock , " Mr. Smith said .	norel	2705..2783
He targeted 7,500 shares of Reuters and punched a button to call up on his screen other dealers ' price quotes .	asynchronous	2784..2894
The vivid yellow figures showed the best price at 845 pence , ( $ 13.27 )	asynchronous	2895..2964
and Mr. Smith 's traders started putting out feelers .	conjunction	2965..3017
But the market sensed a serious buyer on a day dominated by selling	contrast	3018..3085
and the quotes immediately jumped to 850 pence .	conjunction	3086..3134
" When I want to buy , they run from you -- they keep changing their prices , " Mr. Smith said .	norel	3137..3228
" It 's very frustrating . "	cause	3229..3253
He temporarily abandoned his search for the Reuters shares .	cause	3254..3313
By this time , it was 4:30 a.m. in New York , and Mr. Smith fielded a call from a New York customer wanting an opinion on the British stock market , which had been having troubles of its own even before Friday 's New York market break .	norel	3316..3547
" Fundamentally dangerous ... , " Mr. Smith said , almost in a whisper , " ... . fundamentally weak ... fairly vulnerable still ... extremely dangerously poised ...	norel	3550..3707
we 're in for a lot of turbulence ... . "	norel	3708..3746
He was right .	norel	3749..3762
By midday , the London market was in full retreat .	cause	3763..3812
" It 's falling like a stone , " said Danny Linger , a pit trader who was standing outside the London International Financial Futures Exchange .	restatement	3813..3951
Only half the usual lunchtime crowd gathered at the tony Corney & Barrow wine bar on Old Broad Street nearby .	cause	3952..4061
Conversation was subdued as most patrons watched the latest market statistics on television .	conjunction	4062..4154
At 12:49 p.m. , the index hit its low , 2029.7 , off 204.2 points .	entrel	4155..4218
" France opened the limit down , off at least 10 % if you could calculate the index , which you could n't , " Mr. Clark , the Shearson trader , said early in the afternoon .	norel	4221..4384
" Spain is down 10 % and suspended , Sweden 's down 8 % , Norway 11 % .	conjunction	4385..4448
This market has been very badly damaged . "	restatement	4449..4490
As 2:30 p.m. -- Wall Street 's opening time -- neared , Shearson traders and salesmen traded bets on how low the New York market would open .	norel	4493..4631
In the center of the trading floor , chief trader Roger Streeter and two colleagues scrambled for the telephones as soon as the New York market opened -- plummeting more than 60 points in the first few minutes .	norel	4634..4843
They saw an opportunity created by the sell-off .	cause	4844..4892
As Wall Street traders dumped American Depositary Receipts in Jaguar PLC , Mr. Streeter and trader Sam Ruiz bought them to resell in the U.K .	restatement	4893..5033
Investors here still expect Ford Motor Co. or General Motors Corp. to bid for Jaguar .	cause	5034..5119
Suddenly , after about 45 minutes , the U.S. markets rallied .	norel	5122..5181
" The MMI has gone better , " shouted one trader at about 3:15 London time , as the U.S. Major Markets Index contract suddenly indicated a turnabout .	entrel	5182..5327
As Wall Street strengthened , the London trading room went wild .	norel	5330..5393
Traders shouted as their screens posted an ever-narrowing loss on Wall Street .	restatement	5394..5472
Then , nine minutes later , Wall Street suddenly rebounded to a gain on the day .	asynchronous	5473..5551
" Rally ! Rally ! Rally ! " shouted Shearson trader Andy Rosen , selling more Jaguar shares .	entrel	5552..5638
" This is panic buying ! "	entrel	5639..5662
As the London market rallied , some wondered whether the weekend of worrying and jitters had been worth it .	norel	5665..5771
The London index closed at 2163.4 , its high for the day , off 70.5 , or about 3.3 % .	cause	5772..5853
