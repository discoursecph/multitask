September 's steep rise in producer prices shows that inflation still persists , and the pessimism over interest rates caused by the new price data contributed to the stock market 's plunge Friday .	O	9..203
After falling for three consecutive months , the producer price index for finished goods shot up 0.9 % last month , the Labor Department reported Friday , as energy prices jumped after tumbling through the summer .	O	206..415
Although the report , which was released before the stock market opened , did n't trigger the 190.58-point drop in the Dow Jones Industrial Average , analysts said it did play a role in the market 's decline .	O	418..621
Analysts immediately viewed the price data , the grimmest inflation news in months , as evidence that the Federal Reserve was unlikely to allow interest rates to fall as many investors had hoped .	O	622..815
Further fueling the belief that pressures in the economy were sufficient to keep the Fed from easing credit , the Commerce Department reported Friday that retail sales grew 0.5 % in September , to $ 145.21 billion .	O	818..1028
That rise came on top of a 0.7 % gain in August , and suggested there is still healthy consumer demand in the economy .	O	1029..1145
" I think the Friday report , combined with the actions of the Fed , weakened the belief that there was going to be an imminent easing of monetary policy , " said Robert Dederick , chief economist at Northern Trust Co. in Chicago .	O	1148..1372
But economists were divided over the extent of the inflation threat signaled by the new numbers .	O	1375..1471
" The overall 0.9 % increase is serious in itself , but what is even worse is that excluding food and energy , the producer price index still increased by 0.7 % , " said Gordon Richards , an economist at the National Association of Manufacturers .	B-contrast	1474..1712
But Sung Won Sohn , chief economist at Norwest Corp. in Minneapolis , blamed rising energy prices and the annual autumn increase in car prices for most of the September jump .	I-contrast	1715..1887
" I would say this is not bad news ; this is a blip , " he said .	O	1888..1948
" The core rate is not really out of line . "	O	1949..1991
All year , energy prices have skewed the producer price index , which measures changes in the prices producers receive for goods .	B-entrel	1994..2121
Inflation unquestionably has fallen back from its torrid pace last winter , when a steep run-up in world oil prices sent the index surging at double-digit annual rates .	I-entrel	2122..2289
Energy prices then plummeted through the summer , causing the index to decline for three consecutive months .	O	2290..2397
Overall , the index has climbed at a 5.1 % compound annual rate since the start of the year , the Labor Department said .	O	2400..2517
While far more restrained than the pace at the beginning of the year , that is still a steeper rise than the 4.0 % increase for all of 1988 .	B-conjunction	2518..2656
Moreover , this year 's good inflation news may have ended last month , when energy prices zoomed up 6.5 % after plunging 7.3 % in August .	B-contrast	2659..2792
Some analysts expect oil prices to remain relatively stable in the months ahead , leaving the future pace of inflation uncertain .	I-contrast	2793..2921
Analysts had expected that the climb in oil prices last month would lead to a substantial rise in the producer price index , but the 0.9 % climb was higher than most anticipated .	O	2924..3100
" I think the resurgence { in inflation } is going to continue for a few months , " said John Mueller , chief economist at Bell Mueller Cannon , a Washington economic forecasting firm .	O	3101..3278
He predicted that inflation will moderate next year , saying that credit conditions are fairly tight world-wide .	O	3279..3390
But Dirk Van Dongen , president of the National Association of Wholesaler-Distributors , said that last month 's rise " is n't as bad an omen " as the 0.9 % figure suggests .	O	3393..3559
" If you examine the data carefully , the increase is concentrated in energy and motor vehicle prices , rather than being a broad-based advance in the prices of consumer and industrial goods , " he explained .	O	3560..3763
Passenger car prices jumped 3.8 % in September , after climbing 0.5 % in August and declining in the late spring and summer .	O	3766..3887
Many analysts said the September increase was a one-time event , coming as dealers introduced their 1990 models .	B-contrast	3888..3999
Although all the price data were adjusted for normal seasonal fluctuations , car prices rose beyond the customary autumn increase .	I-contrast	4000..4129
Prices for capital equipment rose a hefty 1.1 % in September , while prices for home electronic equipment fell 1.1 % .	B-contrast	4132..4246
Food prices declined 0.6 % , after climbing 0.3 % in August .	B-contrast	4247..4304
Meanwhile , the retail sales report showed that car sales rose 0.8 % in September to $ 32.82 billion .	I-contrast	4307..4405
But at least part of the increase could have come from higher prices , analysts said .	O	4406..4490
Sales at general merchandise stores rose 1.7 % after declining 0.6 % in August , while sales of building materials fell 1.8 % after rising 1.7 % .	O	4493..4633
Producer prices for intermediate goods grew 0.4 % in September , after dropping for three consecutive months .	B-contrast	4636..4743
Prices for crude goods , an array of raw materials , jumped 1.1 % after declining 1.9 % in August and edging up 0.2 % in July .	I-contrast	4744..4865
Here are the Labor Department 's producer price indexes ( 1982 = 100 ) for September , before seasonal adjustment , and the percentage changes from September , 1988 .	O	4868..5025
