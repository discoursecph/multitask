General Motors Corp. 's general counsel hopes to cut the number of outside law firms	(Summary-Evaluation-Topic(Summary-Evaluation-Topic(Summary-Evaluation-Topic(Cause(Cause(Joint(Cause(Textual-organization(Elaboration
the auto maker uses	Elaboration)
from about 700 to 200 within two years .	Textual-organization)
Harry J. Pearce ,	(Attribution(Textual-organization(Elaboration
named general counsel in May 1987 ,	Elaboration)
says	Textual-organization)
the reduction is a cost-cutting measure and an effort	(Elaboration
to let the No. 1 auto maker 's 134-lawyer in-house legal department take on matters	(Elaboration
it is better equipped and trained to handle .	Elaboration))))
GM trimmed about 40 firms from its approved local counsel list ,	(Attribution
Mr. Pearce says .	Attribution))
The move is consistent with a trend	(Elaboration
for corporate legal staffs to do more work in-house ,	(Comparison
instead of farming it out to law firms .	Comparison)))
Mr. Pearce set up GM 's first in-house litigation group in May with four lawyers , all former assistant U.S. attorneys with extensive trial experience .	(Joint(Elaboration(Elaboration
He intends to add to the litigation staff .	Elaboration)
Among the types of cases	(Textual-organization(Elaboration
the in-house litigators handle	Elaboration)
are disputes	(Elaboration
involving companies	(Elaboration(Elaboration
doing business with GM and product-related actions ,	Elaboration)
including one	(Elaboration
in which a driver is suing GM for damages	(Cause
resulting from an accident .	Cause))))))
Mr. Pearce has also encouraged his staff to work more closely with GM 's technical staffs	(Cause(Cause
to help prevent future litigation .	Cause)
GM lawyers have been working with technicians	(Joint(Cause(Cause
to develop more uniform welding procedures	Cause)
-- the way	(Textual-organization(Elaboration
a vehicle is welded	Elaboration)
has a lot to do with its durability .	Textual-organization))
The lawyers also monitor suits	(Cause
to identify specific automobile parts	(Elaboration
that cause the biggest legal problems .	Elaboration))))))
Mr. Pearce says	(Summary-Evaluation-Topic(Summary-Evaluation-Topic(Attribution
law firms with the best chance	(Textual-organization(Elaboration
of retaining or winning business with GM	Elaboration)
will be those	(Elaboration
providing the highest-quality service at the best cost	Elaboration)))
-- echoing similar directives from GM 's auto operations to suppliers .	Summary-Evaluation-Topic)
This does n't necessarily mean larger firms have an advantage ;	(Cause
Mr. Pearce said	(Attribution
GM works with a number of smaller firms	(Elaboration
it regards highly .	Elaboration)))))
Mr. Pearce has shaken up GM 's legal staff	(Joint
by eliminating all titles	(Joint
and establishing several new functions ,	(Elaboration
including a special-projects group	(Elaboration
that has made films on safety and drunk driving .	Elaboration)))))
FEDERAL PROSECUTORS are concluding fewer criminal cases with trials .	(Summary-Evaluation-Topic(Cause(Joint(Cause(Summary-Evaluation-Topic(Comparison(Temporal(Summary-Evaluation-Topic(Attribution
That 's a finding of a new study of the Justice Department by researchers at Syracuse University .	Attribution)
David Burnham , one of the authors , says	(Cause(Attribution
fewer trials probably means a growing number of plea bargains .	Attribution)
In 1980 , 18 % of federal prosecutions concluded at trial ;	(Comparison
in 1987 , only 9 % did .	Comparison)))
The study covered 11 major U.S. attorneys ' offices	(Textual-organization(Elaboration
-- including those in Manhattan and Brooklyn , N.Y. , and New Jersey --	Elaboration)
from 1980 to 1987 .	Textual-organization))
The Justice Department rejected the implication	(Cause(Attribution
that its prosecutors are currently more willing to plea bargain .	Attribution)
`` Our felony caseloads have been consistent for 20 years , ''	(Attribution(Joint
with about 15 % of all prosecutions going to trial ,	Joint)
a department spokeswoman said .	Attribution)))
The discrepancy is somewhat perplexing in that the Syracuse researchers said	(Attribution
they based their conclusions on government statistics .	Attribution))
`` One possible explanation for this decline ''	(Cause(Textual-organization(Attribution(Elaboration
in taking cases to trial ,	Elaboration)
says Mr. Burnham ,	Attribution)
`` is that the number of defendants	(Textual-organization(Elaboration
being charged with crimes by all U.S. attorneys	Elaboration)
has substantially increased . ''	Textual-organization))
In 1980 ,	(Comparison(Textual-organization(Attribution
the study says ,	Attribution)
prosecutors surveyed filed charges against 25 defendants for each 100,000 people	(Elaboration
aged 18 years and older .	Elaboration))
In 1987 , prosecutors filed against 35 defendants for every 100,000 adults .	Comparison)))
Another finding from the study :	(Elaboration
Prosecutors set significantly different priorities .	(Cause
The Manhattan U.S. attorney 's office stressed criminal cases from 1980 to 1987 ,	(Joint(Comparison(Summary-Evaluation-Topic
averaging 43 for every 100,000 adults .	Summary-Evaluation-Topic)
But the New Jersey U.S. attorney averaged 16 .	Comparison)
On the civil side , the Manhattan prosecutor filed an average of only 11 cases for every 100,000 adults during the same period ;	(Comparison
the San Francisco U.S. attorney averaged 79 .	Comparison)))))
The study is to provide reporters , academic experts and others raw data	(Elaboration
on which to base further inquiries .	Elaboration))
IMELDA MARCOS asks for dismissal ,	(Summary-Evaluation-Topic(Elaboration(Elaboration(Temporal(Summary-Evaluation-Topic(Joint
says	(Attribution
she was kidnapped .	Attribution))
The former first lady of the Philippines , asked a federal court in Manhattan to dismiss an indictment against her ,	(Cause
claiming among other things ,	(Attribution
that she was abducted from her homeland .	Attribution)))
Mrs. Marcos and her late husband , former Philippines President Ferdinand Marcos , were charged with embezzling more than $ 100 million from that country and then fraudulently concealing much of the money through purchases of prime real estate in Manhattan .	Temporal)
Mrs. Marcos 's attorneys asked federal Judge John F. Keenan to give them access to all U.S. documents about her alleged abduction .	(Summary-Evaluation-Topic
The U.S. attorney 's office , in documents	(Attribution(Textual-organization(Elaboration
it filed in response ,	Elaboration)
said	Textual-organization)
Mrs. Marcos was making the `` fanciful	(Textual-organization(Elaboration
-- and factually unsupported --	Elaboration)
claim	(Elaboration
that she was kidnapped into this country ''	(Cause
in order to obtain classified material in the case .	Cause))))))
The office also said	(Comparison(Cause(Attribution
Mrs. Marcos and her husband were n't brought to the U.S. against their will	(Temporal
after Mr. Marcos was ousted as president .	Temporal))
The prosecutor quoted statements from the Marcoses	(Elaboration
in which they said	(Attribution
they were in this country at the invitation of President Reagan	(Joint
and that they were enjoying the hospitality of the U.S .	Joint))))
Lawyers for Mrs. Marcos say	(Attribution
that	(Textual-organization(Temporal
because she was taken to the U.S. against her wishes ,	Temporal)
the federal court lacks jurisdiction in the case .	Textual-organization))))
THE FEDERAL COURT of appeals in Manhattan ruled	(Summary-Evaluation-Topic(Elaboration(Temporal(Cause(Elaboration(Attribution
that the dismissal of a 1980 indictment against former Bank of Crete owner George Koskotas should be reconsidered .	Attribution)
The indictment ,	(Textual-organization(Elaboration
which was sealed	(Joint
and apparently forgotten by investigators until 1987 ,	Joint))
charges Mr. Koskotas and three others with tax fraud and other violations .	Textual-organization))
He made numerous trips to the U.S. in the early 1980s ,	(Comparison(Temporal(Comparison
but was n't arrested until 1987	(Temporal
when he showed up as a guest of then-Vice President George Bush at a government function .	Temporal))
A federal judge in Manhattan threw out the indictment ,	(Cause
finding	(Attribution
that the seven-year delay violated the defendant 's constitutional right to a speedy trial .	Attribution)))
The appeals court , however , said	(Attribution
the judge did n't adequately consider	(Attribution
whether the delay would actually hurt the chances of a fair trial .	Attribution))))
Mr. Koskotas is fighting extradition proceedings	(Elaboration(Elaboration
that would return him to Greece ,	Elaboration)
where he is charged with embezzling more than $ 250 million from the Bank of Crete .	Elaboration))
His attorney could n't be reached for comment .	Elaboration)
PRO BONO VOLUNTARISM :	(Summary-Evaluation-Topic(Textual-organization
In an effort	(Summary-Evaluation-Topic(Elaboration(Textual-organization(Elaboration
to stave off a plan	(Elaboration
that would require all lawyers in New York state to provide twenty hours of free legal aid a year ,	Elaboration))
the state bar recommended an alternative program	(Elaboration
to increase voluntary participation in pro bono programs .	Elaboration))
The state bar association 's policy making body , the House of Delegate , voted Saturday to ask Chief Judge Sol Wachtler to give the bar 's voluntary program three years to prove its effectiveness	(Temporal
before considering mandatory pro bono .	Temporal))
`` We believe	(Attribution(Attribution
our suggested plan is more likely to improve the availability of quality legal service to the poor	(Joint(Comparison
than is the proposed mandatory pro bono plan	Comparison)
and will achieve that objective	(Joint
without the divisiveness , distraction , administrative burdens and possible failure	(Elaboration
that	(Textual-organization(Attribution
we fear	Attribution)
would accompany an attempt	(Elaboration
to impose a mandatory plan , ''	Elaboration))))))
said Justin L. Vigdor of Rochester ,	(Elaboration
who headed the bar 's pro bono study committee .	Elaboration))))
DALLAS AND HOUSTON law firms merge :	(Summary-Evaluation-Topic(Textual-organization
Jackson & Walker , a 130-lawyer firm in Dallas and Datson & Scofield , a 70-lawyer firm in Houston said	(Elaboration(Elaboration(Cause(Attribution
they have agreed in principle to merge .	Attribution)
The consolidated firm ,	(Textual-organization(Elaboration
which would rank among the 10 largest in Texas ,	Elaboration)
would operate under the name Jackson & Walker .	Textual-organization))
The merger must be formally approved by the partners of both firms	(Comparison
but is expected to be completed by year end .	Comparison))
Jackson & Walker has an office in Fort Worth , Texas ,	(Joint
and Dotson & Scofield has an office in New Orleans .	Joint)))
PILING ON ?	(Textual-organization
Piggybacking on government assertions	(Comparison(Elaboration(Elaboration(Temporal(Elaboration(Elaboration(Temporal(Elaboration
that General Electric Co. may have covered up fraudulent billings to the Pentagon ,	Elaboration)
two shareholders have filed a civil racketeering suit against the company .	Temporal)
The suit was filed by plaintiffs ' securities lawyer Richard D. Greenfield in U.S. District Court in Philadelphia .	Elaboration)
He seeks damages from the company 's 15 directors on grounds	(Elaboration
that they either `` participated in or condoned the illegal acts . . .	(Joint
or utterly failed to carry out their duties as directors . ''	Joint)))
GE is defending itself against government criminal charges of fraud and false claims in connection with a logistics-computer contract for the Army .	Temporal)
The trial begins today in federal court in Philadelphia .	Elaboration)
The government 's assertions of the cover-up were made in last minute pretrial motions .	Elaboration)
GE ,	(Elaboration(Textual-organization(Elaboration
which vehemently denies the government 's allegations ,	Elaboration)
denounced Mr. Greenfield 's suit .	Textual-organization)
`` It is a cheap-shot suit	(Elaboration(Attribution(Elaboration(Elaboration
-- procedurally defective and thoroughly fallacious --	Elaboration)
which was hurriedly filed by a contingency-fee lawyer	(Cause
as a result of newspaper reports , ''	Cause))
said a GE spokeswoman .	Attribution)
She added	(Attribution
that the company was considering bringing sanctions against Mr. Greenfield	(Cause
for making `` grossly inaccurate and unsupported allegations . ''	Cause))))))))))))
