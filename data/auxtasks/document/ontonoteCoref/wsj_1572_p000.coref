FOR THOSE WHO DELIGHT in the misfortune of others , read on .	ROOT	0
This is a story about suckers .	NONE	1
Most of us know a sucker .	NONE	2
Many of us are suckers .	COREF_PREV	3
But what we may not know is just what makes somebody a sucker .	COREF_PREV	4
What makes people blurt out their credit - card numbers to a caller they 've never heard of ?	NONE	5
Do they really believe that the number is just for verification and is simply a formality on the road to being a grand - prize winner ?	COREF_PREV	6
What makes a person buy an oil well from some stranger knocking on the screen door ?	NONE	7
Or an interest in a retirement community in Nevada that will knock your socks off , once it is built ?	NONE	8
Because in the end , these people always wind up asking themselves the same question : `` How could I be so stupid ? ''	NONE	9
There are , unfortunately , plenty of answers to that question -- and scam artists know all of them .	COREF_PREV	10
`` These people are very skilled at finding out what makes a person tick , '' says Kent Neal , chief of the economic - crime unit of the Broward County State Attorney 's Office in Fort Lauderdale , , Fla.a major haven for boiler rooms .	COREF_PREV	11
`` Once they size them up , then they know what buttons to push . ''	COREF_PREV	12
John Blodgett agrees -- and he ought to know .	NONE	13
He used to be a boiler - room salesman , peddling investments in oil and gas wells and rare coins .	COREF_PREV	14
`` There 's a definite psychology of the sale and different personalities you pitch different ways , '' he says .	NONE	15
The most obvious pitch , of course , is the lure of big returns .	COREF_OTHER	16
`` We 're all a little greedy .	NONE	17
Everyone is vulnerable , '' says Charles Harper , associate regional administrator for the Securities and Exchange Commission in Miami .	COREF_PREV	18
`` These guys prey on human frailties . ''	COREF_OTHER	19
While the promises of big profits ought to set off warning bells , they often do n't , in part because get - rich - quick tales have become embedded in American folklore .	NONE	20
`` The overnight success story is part of our culture , and our society puts an emphasis on it with lotteries and Ed McMahon making millionaires out of people , '' says Michael Cunningham , an associate professor of psychology at the University of Kentucky in Louisville .	NONE	21
`` Other people are making it overnight , and the rest who toil daily do n't want to miss that opportunity when it seems to come along . ''	NONE	22
Adds Spencer Barasch , branch chief for enforcement at the SEC in Fort Worth , Texas : `` Why do people play the lottery when the odds are great against them ?	COREF_OTHER	23
People are shooting for a dream . ''	COREF_PREV	24
Clearly , though , scam artists have to be a bit more subtle than simply promising millions ; the psychology of suckers is n't simply the psychology of the greedy .	NONE	25
There 's also , for instance , the need to be part of the in - crowd .	NONE	26
So one popular ploy is to make a prospective investor feel like an insider , joining an exclusive group that is about to make a killing .	NONE	27
Between 1978 and 1987 , for instance , SH Oil in Winter Haven , Fla. , sold interests in oil wells to a very select group of local residents , while turning away numerous other eager investors .	COREF_OTHER	28
The owner of , the companyStephen Smith , who has since pleaded guilty to state and federal fraud charges , confided to investors that he had a secret agreement with Amoco Oil Co. and said the location of his wells was confidential , according to a civil suit filed in a Florida state court by the Florida comptroller 's office .	COREF_PREV	29
Neither the Amoco agreement nor the wells existed , the suit alleged .	COREF_PREV	30
Such schemes , says Tony Adamski , chief of the financial - crimes unit of the Federal Bureau of Investigation in Washington , D.C. , appeal to investors ' `` desire to believe this is really true and that they are part of a chosen group being given this opportunity . ''	COREF_OTHER	31
At times , salesmen may embellish the inside information with `` the notion that this is some slightly shady , slightly illegal investment the person is being included in , '' says Mr. Cunningham .	COREF_PREV	32
In appealing to those with a bit of larceny in their hearts , the fraud artist can insist that a person keep an investment secret -- insulating himself from being discovered and keeping his victim from consulting with others .	COREF_OTHER	33
It also adds to the mystery of the venture .	COREF_PREV	34
Mr. Blodgett , the boiler - room veteran , believes that for many investors , the get - rich - quick scams carry a longed - for element of excitement .	COREF_OTHER	35
`` Once people got into it , I was allowing them to live a dream , '' he says .	COREF_PREV	36
He phoned them with updates on the investment , such as `` funny things that happened at the well that week , '' he says .	COREF_PREV	37
`` You gave them some excitement that they did n't have in their lives . ''	COREF_PREV	38
( Mr. Blodgett , who was convicted in Florida state court of selling unregistered securities and in California state court of unlawful use of the telephone to defraud and deceive , is now on probation .	COREF_PREV	39
He says he has quit the business and is back in school , majoring in psychology with aspirations to go into industrial psychology . )	COREF_PREV	40
For some investors , it 's the appearances that leave them deceived .	NONE	41
`` The trappings of success go a long way -- wearing the right clothes , doing the right things , '' says Paul Andreassen , an associate professor of psychology at Harvard .	COREF_PREV	42
Conservative appearances make people think it 's a conservative investment .	NONE	43
`` People honestly lose money on risky investments that they did n't realize were a crapshoot , '' he says .	NONE	44
Paul Wenz , a Phoenix , Ariz. , attorney , says a promise of unrealistic returns would have made him leery .	COREF_OTHER	45
But Mr. Wenz , who says he lost $ 43,000 in one precious - metals deal and $ 39,000 in another , says a salesman `` used a business - venture approach '' with him , sending investment literature , a contract limiting the firm 's liability , and an insurance policy .	COREF_PREV	46
When he visited the company 's office , he says , it had `` all the trappings of legitimacy . ''	COREF_PREV	47
Still others are stung by a desire to do both well and good , says Douglas Watson , commanding officer of the Los Angeles Police Department 's bunko - forgery division .	COREF_PREV	48
Born - again Christians are the most visible targets of unscrupulous do - gooder investment pitches .	NONE	49
But hardly the only ones : The scams promise -- among other things -- to help save the environment , feed starving families and prevent the disappearance of children .	NONE	50
Psychologists say isolated people who do n't discuss their investments with others are particularly at risk for fraud .	NONE	51
Scam artists seek out such people -- or try to make sure that their victims isolate themselves .	NONE	52
For instance , salesmen may counter a man 's objection that he wants to discuss an investment with his wife by asking , `` Who wears the pants in your family ? ''	COREF_PREV	53
Or an investor who wants his accountant 's advice may be told , `` You seem like a guy who can make up his own mind . ''	COREF_PREV	54
Often con artists will try to disarm their victims by emphasizing similarities between them .	NONE	55
William Lynes , a retired engineer from ,Lockheed Corp. says he and , his wifeLily , warmed to the investment pitches of a penny - stock peddler from Stuart - James Co. in Atlanta after the broker told them he , too , had once worked with Lockheed .	NONE	56
The Lyneses , of Powder Springs , Ga. , have filed suit in Georgia state court against Stuart James , alleging fraud .	COREF_PREV	57
They are awaiting an arbitration proceeding .	COREF_OTHER	58
They say the broker took them out for lunch frequently .	COREF_OTHER	59
He urged them to refer their friends , who also lost money .	COREF_PREV	60
( Donald Trinen , an attorney for the penny - brokerage firm , denies the fraud allegations and says the Lyneses were fully apprised that they were pursuing a high - risk investment . )	COREF_PREV	61
`` It 's not uncommon for these guys to send pictures of themselves or their families to ingratiate themselves to their clients , '' says Terree Bowers , chief of the major - frauds section of the U.S. attorney 's office in Los Angeles .	COREF_PREV	62
`` We 've seen cases where salesmen will affect the accent of the region of the country they are calling .	COREF_PREV	63
Anything to make a sale . ''	COREF_OTHER	64
Experts say that whatever a person 's particular weak point , timing is crucial .	COREF_OTHER	65
People may be particularly vulnerable to flim - flam pitches when they are in the midst of a major upheaval in their lives .	NONE	66
`` Sometimes when people are making big changes , retiring from their jobs , moving to a new area , they lose their bearings , '' says Maury Elvekrog , a licensed psychologist who is now an investment adviser and principal in Seger - Elvekrog Inc. , a Birmingham , Mich. , investment - counseling firm .	COREF_PREV	67
`` They may be susceptible to some song and dance if it hits them at the right time . ''	COREF_PREV	68
They are obviously also more susceptible when they need money - retirees , for instance , trying to bolster their fixed income or parents fretting over how to pay for a child 's college expenses .	COREF_PREV	69
`` These people are n't necessarily stupid or naive .	NONE	70
Almost all of us in comparable circumstances might be victimized in some way , '' says Jerald Jellison , a psychology professor at the University of Southern California in Los Angeles .	COREF_OTHER	71
Nick Cortese thinks that 's what happened to him .	NONE	72
Mr. Cortese , a 33 - year - old Delta Air Lines engineer , invested some $ 2,000 in penny stocks through a broker who promised quick returns .	COREF_OTHER	73
`` We were saving up to buy a house , and my wife was pregnant , '' says Mr. Cortese .	NONE	74
`` It was just before the Christmas holidays , and I figured we could use some extra cash . ''	COREF_OTHER	75
The investment is worth about $ 130 today .	COREF_PREV	76
`` Maybe it was just a vulnerable time , '' says Mr. Cortese .	NONE	77
`` Maybe the next day or even an hour later , I would n't have done it . ''	COREF_OTHER	78
Ms. Brannigan is a staff reporter in The Wall Street Journal 's Atlanta bureau .	COREF_PREV	79
