Earnings for most of the nation 's major pharmaceutical makers are believed to have moved ahead briskly in the third quarter ,	(Elaboration(Explanation(Elaboration(Contrast(Elaboration
as companies with newer , big-selling prescription drugs fared especially well .	Elaboration)
For the third consecutive quarter , however , most of the companies ' revenues were battered by adverse foreign-currency translations	(Cause
as a result of the strong dollar abroad .	Cause))
Analysts said	(Comparison(Attribution
that Merck & Co. , Eli Lilly & Co. , Warner-Lambert Co. and the Squibb Corp. unit of Bristol-Myers Squibb Co. all benefited from strong sales of relatively new , higher-priced medicines	(Elaboration
that provide wide profit margins .	Elaboration))
Less robust earnings at Pfizer Inc. and Upjohn Co. were attributed to those companies ' older products ,	(Elaboration
many of which face stiffening competition from generic drugs and other medicines .	Elaboration)))
Joseph Riccardo , an analyst with Bear , Stearns & Co. , said	(Cause(Attribution
that over the past few years most drug makers have shed their slow-growing businesses	(Joint
and instituted other cost savings ,	(Elaboration
such as consolidating manufacturing plants and administrative staffs .	Elaboration)))
As a result , `` major new products are having significant impact , even on a company with very large revenues , ''	(Attribution
Mr. Riccardo said .	Attribution)))
Analysts said	(Elaboration(Evaluation(Attribution
profit for the dozen or so big drug makers , as a group , is estimated to have climbed between 11 % and 14 % .	Attribution)
While that 's not spectacular ,	(Contrast
Neil Sweig , an analyst with Prudential Bache , said	(Attribution
that the rate of growth will `` look especially good	(Condition(Comparison
as compared to other companies	Comparison)
if the economy turns downward . ''	Condition))))
Mr. Sweig estimated	(Joint(Contrast(Elaboration(Elaboration(Attribution
that Merck 's profit for the quarter rose by about 22 % ,	(Explanation
propelled by sales of its line-up of fast-growing prescription drugs ,	(Elaboration
including its anti-cholesterol drug , Mevacor ; a high blood pressure medicine , Vasotec ; Primaxin , an antibiotic , and Pepcid , an anti-ulcer medication .	Elaboration)))
Profit climbed	(Attribution(Cause(Contrast
even though Merck 's sales were reduced by `` one to three percentage points ''	Contrast)
as a result of the strong dollar ,	Cause)
Mr. Sweig said .	Attribution))
In the third quarter of 1988 , Merck earned $ 311.8 million , or 79 cents a share .	Elaboration)
In Rahway , N.J. , a Merck spokesman said	(Attribution
the company does n't make earnings projections .	Attribution))
Mr. Sweig said	(Joint(Contrast(Elaboration(Evaluation(Elaboration(Attribution
he estimated	(Attribution
that Lilly 's earnings for the quarter jumped about 20 % ,	(Cause
largely because of the performance of its new anti-depressant Prozac .	Cause)))
The drug ,	(Same-unit(Elaboration
introduced last year ,	Elaboration)
is expected to generate sales of about $ 300 million this year .	Same-unit))
`` It 's turning out to be a real blockbuster , ''	(Attribution
Mr. Sweig said .	Attribution))
In last year 's third quarter , Lilly earned $ 171.4 million , or $ 1.20 a share .	Elaboration)
In Indianapolis , Lilly declined comment .	Contrast)
Several analysts said	(Joint(Elaboration(Attribution
they expected Warner-Lambert 's profit also to increase by more than 20 % from $ 87.7 million , or $ 1.25 a share ,	(Elaboration
it reported in the like period last year .	Elaboration))
The company is praised by analysts	(Elaboration(Explanation
for sharply lowering its costs in recent years	(Joint
and shedding numerous companies with low profit margins .	Joint))
The company 's lean operation ,	(Elaboration(Same-unit(Attribution
analysts said ,	Attribution)
allowed sharp-rising sales from its cholesterol drug , Lopid ,	(Cause
to power earnings growth .	Cause))
Lopid sales are expected to be about $ 300 million this year , up from $ 190 million in 1988 .	(Elaboration
In Morris Plains , N.J. , a spokesman for the company said	(Attribution
the analysts ' projections are `` in the ballpark . ''	Attribution)))))
Squibb 's profit ,	(Joint(Elaboration(Elaboration(Same-unit(Elaboration
estimated by analysts to be about 18 % above the $ 123 million , or $ 1.25 a share ,	(Elaboration
it earned in the third quarter of 1988 ,	Elaboration))
was the result of especially strong sales of its Capoten drug	(Elaboration
for treating high blood pressure and other heart disease .	Elaboration))
The company was officially merged with Bristol-Myers Co. earlier this month .	Elaboration)
Bristol-Myers declined to comment .	Elaboration)
Mr. Riccardo of Bear Stearns said	(Joint(Elaboration(Elaboration(Attribution
that Schering-Plough Corp. 's expected profit rise of about 18 % to 20 % , and Bristol-Meyers 's expected profit increase of about 13 % are largely because `` those companies are really managed well . ''	Attribution)
ScheringPlough earned $ 94.4 million , or 84 cents a share ,	(Comparison
while Bristol-Myers earned $ 232.3 million , or 81 cents a share , in the like period a year earlier .	Comparison))
In Madison , N.J. , a spokesman for Schering-Plough said	(Explanation(Attribution
the company has `` no problems '' with the average estimate by a analysts	(Elaboration
that third-quarter earnings per share rose by about 19 % , to $ 1 .	Elaboration))
The company expects to achieve the 20 % increase in full-year earnings per share ,	(Attribution(Comparison
as it projected in the spring ,	Comparison)
the spokesman said .	Attribution)))
Meanwhile , analysts said	(Joint(Elaboration(Elaboration(Elaboration(Elaboration(Attribution
Pfizer 's recent string of lackluster quarterly performances continued ,	(Elaboration
as earnings in the quarter were expected to decline by about 5 % .	Elaboration))
Sales of Pfizer 's important drugs , Feldene	(Explanation(Same-unit(Elaboration
for treating arthritis ,	Elaboration)
and Procardia , a heart medicine , have shrunk	Same-unit)
because of increased competition .	Explanation))
`` The ( strong ) dollar hurt Pfizer a lot , too , ''	(Attribution
Mr. Sweig said .	Attribution))
In the third quarter last year , Pfizer earned $ 216.8 million , or $ 1.29 a share .	Elaboration)
In New York , the company declined comment .	Elaboration)
Analysts said	(Elaboration(Explanation(Attribution
they expected Upjohn 's profit to be flat or rise by only about 2 % to 4 %	(Comparison
as compared with $ 89.6 million , or 49 cents a share ,	(Elaboration
it earned a year ago .	Elaboration)))
Upjohn 's biggest-selling drugs are Xanax , a tranquilizer , and Halcion , a sedative .	(Joint(Elaboration
Sales of both drugs have been hurt by new state laws	(Same-unit(Elaboration
restricting the prescriptions of certain tranquilizing medicines	Elaboration)
and adverse publicity about the excessive use of the drugs .	Same-unit))
Also , the company 's hair-growing drug , Rogaine , is selling well	(Attribution(Contrast(Elaboration
-- at about $ 125 million for the year ,	Elaboration)
but the company 's profit from the drug has been reduced by Upjohn 's expensive print and television campaigns for advertising ,	Contrast)
analysts said .	Attribution)))
In Kalamazoo , Mich. , Upjohn declined comment .	Elaboration)))))))))
