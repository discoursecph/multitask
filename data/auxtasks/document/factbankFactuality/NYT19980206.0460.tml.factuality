The economy created jobs at a surprisingly robust pace in January , the government reported on Friday , evidence that America 's economic stamina has withstood any disruptions caused so far by the financial tumult in Asia .	CT+	8
The Bureau of Labor Statistics said the economy added 358,000 jobs last month , far above the 235,000 forecast by economists .	CT+	9
With growing opportunities for work drawing more people into the labor force , the unemployment rate remained at 4.7 percent last month , just a notch above its quarter-century low .	CT+	11
The demand for workers also led employers to raise wages again last month .	CT+	12
Average hourly earnings of production and nonsupervisory employees rose 4 cents , to $ 12.51 .	CT+	13
The gain left wages 3.8 percent higher than a year earlier , extending a trend that has given back to workers some of the earning power they lost to inflation in the last decade .	CT+	14
But economists said the wage increase was not enough to raise any concerns about higher inflation .	CT+	15
And because the Federal Reserve has made clear that it expects the economy to slow in coming months as ripple effects from Asia reach the United States , investors disregarded the large jobs gains , betting that neither a rise in interest rates nor resurgent inflation is looming .	Uu	16
Stocks rose , pushing the Dow Jones industrial average up 72.24 points , to 8,189.49 , leaving the index within 70 points of its record high set on Aug. 6 .	CT+	17
In the bond market , a sensitive barometer of inflation anxiety , prices edged up , pushing down the yield on the benchmark 30-year Treasury bond to 5.92 percent from 5.93 percent .	CT+	18
`` Job creation is sturdy and we have effective price stability , '' said Paul McCulley , an economist at UBS Securities .	CT+	19
With potential inflation counterbalanced by worries about possible price declines , the Fed and its chairman , Alan Greenspan , have suggested that they intend to keep interest rates unchanged until the economy tilts clearly in one direction or another .	CT+	21
The job figures for January extended a surge in employment gains that began last fall , just when the labor market had showed signs of cooling .	CT+	22
After accounting for a small downward revision Friday to December 's figures , the economy has been creating jobs at a rate of 358,000 a month for the last four months _ and 381,000 over the last three months _ after averaging 242,000 for the first nine months of 1997 .	CT+	23
The surge in jobs reflects a remarkable confluence of positive and self-reinforcing economic forces .	CT+	24
After a short , sharp drop when Asia 's financial problems first became clear , the stock market has rebounded and corporate profits have remained healthy .	CT+	26
Long-term interest rates , an important indicator of economic stability , have fallen in the last few months .	CT+	27
Lower rates have helped invigorate housing and other interest-sensitive sectors of the economy by making loans more affordable , and have allowed companies to continue investing heavily in more efficient equipment .	CT+	28
The likelihood that the federal budget will soon move from deficit into surplus has further improved the outlook .	PR+	29
President Clinton welcomed the job figures at a news conference Friday .	CT+	31
His advisers said the results reflected not just from balancing the budget , but also initiatives like improved access to education and training and the opening of foreign markets to trade .	CT+	32
Some of the biggest employment gains came in the construction industry , which added 92,000 jobs in January .	CT+	34
The increase reflected the strength of the housing industry , which has been helped not just by declining interest rates but also by unusually warm weather .	CT+	35
Service industries also showed solid job gains , as did manufacturers , two areas expected to be hardest hit when the effects of the Asian crisis hit the American economy .	CT+	36
The major harm from Asia is likely to come from the plunge in the value of many Asian currencies relative to the dollar , a situation that is expected to lead to a surge of inexpensive imports into the United States , hurting American competitors .	PR+	37
And the weakness of the Asian economies is also likely to lead to decreased demand for American exports .	PR+	38
`` The economy has been running on the fumes of the strength from before the Asia shock , '' McCulley said .	CT+	39
`` Those fumes will exhaust themselves , and the manufacturing sector is going to start getting beat up in the spring . ''	Uu	40
The figures showed the unemployment rate for adult men declined to 3.8 percent last month from 4.1 percent in December , the lowest figure for almost two decades .	CT+	41
The rate for adult women rose 0.4 percent , to 4.4 percent .	CT+	42
For the first time , the Labor Department broke out unemployment by education level as well as gender , age and race .	CT+	43
The analysis showed unemployment rates dropped steadily with higher levels of education .	CT+	44
Among people with less than a high school education , the rate in January was 7.2 percent .	CT+	45
