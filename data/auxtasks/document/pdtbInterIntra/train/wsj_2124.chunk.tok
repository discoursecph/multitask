No strikeout , but certainly no home run .	root	9..49
That 's how the stock-picking game is shaping up for the months ahead , according to money managers and a few brokers .	norel	52..168
Yesterday 's 88-point recovery from Friday 's megadrop in the Dow Jones industrials had many brokerage houses proclaiming that stocks are a good bargain again .	norel	171..328
But quite a few money managers are n't buying it .	contrast	329..377
Weakening corporate earnings , they say , are no prescription for a bull market .	cause	378..456
" The stock market ai n't going to do much of anything " for a while , says John Neff of Wellington Management , who runs the $ 8.3 billion Windsor Fund .	norel	459..606
He suspects that Friday 's market decline may have a second leg , perhaps a 10 % to 15 % drop later on .	conjunction	607..706
Mr. Neff says the stock market has lost some powerful driving forces , namely earnings growth and the " LBO sweepstakes " -- buy-out fever that induced investors to bid up whole groups of stocks , such as media and airlines .	norel	709..929
After sitting with 20 % of his fund in cash before Friday 's sell-off , Mr. Neff says he bought " a narrow list of stocks " yesterday .	entrel	930..1059
With flat corporate profits on the horizon for 1990 , money managers say price-earnings multiples that look cheap today might go on being cheap for a long time .	norel	1062..1221
" This is not a grossly overvalued market , but it 's not cheap either , " says George Collins , president of the mutual fund company T. Rowe Price Associates in Baltimore .	norel	1224..1390
According to Institutional Brokers Estimate System , Wall Street market strategists see only a 2.4 % jump in company profits in 1990 -- unlike in 1987 , when profits a year out looked good ( they did soar 36 % in 1988 ) .	norel	1393..1607
Bulls say the market is an incredible bargain , priced at only about 12 times estimated 1989 earnings for stocks in the Standard & Poor 's 500 index .	norel	1610..1757
Before the 1987 crash , the P/E was more than 20 .	contrast	1758..1806
The common view , says Abby Cohen , strategist for Drexel Burnham Lambert , is that there will be " mild economic growth , modest profit expansion , and things are going to be hunky-dory .	norel	1809..1990
Our view is that we may see a profit decline . "	contrast	1991..2037
Some think investors should sell into rallies .	synchrony	2038..2084
The market " is going to wind down , " says Gerald W. Perritt , a Chicago money manager .	cause	2085..2169
" Things are a little less overpriced " after Friday 's jolt in the market .	restatement	2170..2242
He expects stocks to decline an additional 5 % to 30 % , with the Dow perhaps bottoming out between 2000 and 2100 " between now and June . "	contrast	2243..2377
After Friday 's decline , Mr. Perritt 's firm ran statistical tests on 100 high-quality stocks , using old-fashioned value criteria devised by Benjamin Graham , an analyst and author in the 1930s and 1940s who is widely considered to be the father of modern securities analysis .	norel	2380..2653
He found 85 still overvalued and 15 fairly valued .	conjunction	2654..2704
Nicholas Parks , a New York money manager , expects the market to decline about 15 % .	norel	2707..2789
" I 've been two-thirds in cash since July , and I continue to think that having a defensive position is appropriate , " he says .	cause	2790..2914
Companies that piled on debt in leveraged buy-outs during the past two years " will continue to surface as business problems . "	cause	2915..3040
" Generalizations about value are n't useful , " says New York money manager John LeFrere of Delta Capital Management .	norel	3043..3157
For instance , he says , International Business Machines and Unisys might look cheap	instantiation	3158..3240
but investors might continue to do better with stocks like Walt Disney , Procter & Gamble and Coca-Cola , strong performers in recent years .	contrast	3241..3380
Money manager Robert Ross , head of Duncan Ross Associates Ltd. in Vancouver , British Columbia , says stocks would have to fall 15 % to 20 % before they are competitive with less risky investment alternatives .	norel	3383..3588
Fredric Russell , a money manager in Tulsa , Okla. , says Friday 's cave-in " is going to have more of a permanent impact on the psyche of many investors than Wall Street would want to admit . "	conjunction	3589..3776
There are still bulls out there .	norel	3779..3811
" I still think we will have a 3000 Dow , whether it 's six months or 12 months from now I do n't know , " says David Dreman , managing partner of Dreman Value Management in New York .	instantiation	3812..3988
" We 're doing a little buying " in some stocks " that have really been smashed down . "	cause	3989..4071
Many brokerage house officials also are optimistic .	norel	4074..4125
Yesterday , Goldman Sachs , Merrill Lynch and Dean Witter all increased the proportion of assets they recommend investors commit to stocks .	instantiation	4126..4263
Dean Witter now recommends 85 % , Goldman 65 % and Merrill Lynch 50 % .	restatement	4264..4330
Some investors say Friday 's sell-off was a good thing , because it deflated a lot of crazy takeover speculation .	norel	4333..4444
" It was a healthy cleansing , " says Michael Holland , who runs Salomon Brothers Asset Management in New York .	restatement	4445..4552
From here out , these investors see a return to old-fashioned investing , based on a company 's ability to show profit growth .	norel	4555..4678
" The fundamentals are pretty strong , " Mr. Dreman says .	norel	4681..4735
" I do n't see this as a bear market at all .	cause	4736..4778
It 's a recognition that there was much too much fluff in the LBO market . "	alternative	4779..4852
Friday 's big fall was " just a blunder by the stock market , " says John Connolly , chief strategist for Dean Witter .	norel	4855..4968
" It was an overreaction to an event { the failure of a management and union group to get bank financing for a takeover of UAL } that does n't mean that much to lots of stocks . "	restatement	4969..5142
Many investors have nagging worries , however .	norel	5143..5188
Newspapers are full of headlines about companies defaulting on their debts and banks writing off real estate loans .	cause	5189..5304
That hurts investors ' confidence in the economy and stocks .	cause	5305..5364
Not even all the brokerage firms see clear sailing ahead .	norel	5367..5424
" Disappointing profits are likely to get worse in the next two quarters , " says Mary Farrell , a market strategist at PaineWebber .	instantiation	5425..5553
She thinks the market could drop about 10 % in the next few months , then recover and go higher .	entrel	5554..5648
Companies with steady earnings growth could do well , she says , while others with high debt or poor earnings could see their shares decline far more than 10 % .	restatement	5649..5806
