As the Soviet Union grapples with its worsening economy, leading reformers have drawn up a blueprint for change designed to push the nation much closer to a free-market system.	O	9..185
The proposals go far beyond the current and rather confused policies of perestroika, Mikhail Gorbachev's restructuring of the economy.	B-restatement	188..322
They lay out a clear timetable and methodology for liberalizing the system of setting prices, breaking up huge industrial monopolies and putting unprofitable state-owned companies out of business.	B-conjunction	323..519
They also address such taboo subjects as the likelihood of unemployment and high inflation, and recommend ways to soften the social consequences.	I-conjunction	520..665
While many solutions to the nation's economic troubles are being discussed, the blueprint is attracting widespread attention here because of its comprehensiveness and presumed high-level authorship.	O	668..866
Although it was published unsigned in the latest edition of the weekly Ekonomicheskaya Gazeta, Soviet sources say the article was written by Leonid Abalkin and a small group of colleagues.	B-entrel	869..1057
Mr. Abalkin, head of the Academy of Science's Institute of Economics, was recently appointed deputy chairman of the Soviet government and head of a state commission on economic reform.	I-entrel	1058..1242
"It's clearly a manifesto for the next stage of perestroika," said one analyst.	O	1245..1324
The economic ideas in the document are much bolder than current policies.	B-instantiation	1327..1400
For example, the proposed overhaul of prices -- an extremely sensitive political topic -- is far more precise than the vague plans announced by Mr. Gorbachev in 1987 and later dropped.	I-instantiation	1401..1585
But the proposals also display political savvy, couching some of the most controversial ideas in cautious language so as not to alienate powerful conservatives in the government who stand to lose out if they are implemented.	B-entrel	1588..1812
Seeking a middle path between opponents of change and radicals who demand overnight solutions, the article advocates what it calls a "radical-moderate approach."	I-entrel	1813..1974
The document is to be discussed at a conference of leading economists late this month, and will probably be presented to the Soviet Parliament for consideration this year.	B-entrel	1977..2148
As policy-makers draw up proposals for the next five-year plan, which starts in 1991, the blueprint represents a powerful first shot in what is likely to be a fierce battle over economic reform.	I-entrel	2149..2343
The authors make a gloomy assessment of the economy and concede that "quick and easy paths to success simply don't exist."	O	2346..2468
Instead, they map out a strategy in several phases from now until 1995.	B-restatement	2469..2540
Most of the measures would probably only start to have an effect on beleaguered Soviet consumers in two to three years at the earliest.	I-restatement	2541..2676
The key steps advocated include:   -- PROPERTY.	O	2677..2724
Rigid ideological restrictions on property ownership should be abandoned.	O	2725..2798
The document proposes breaking up the monolithic system of state-owned enterprises and farms and allowing a big private sector to flourish, helped by tough anti-monopoly legislation.	O	2799..2981
The economy would be thrown open to numerous types of ownership between now and 1992, including factories leased by workers or owned by shareholders, cooperatives and joint ventures.	B-conjunction	2982..3164
breaking up the monolithic system of state-owned enterprises and farms and allowing a big private sector to flourish, helped by tough anti-monopoly legislation.The economy would be thrown open to numerous types of ownership between now and 1992, including factories leased by workers or owned by shareholders, cooperatives and joint ventures.Some forms of private property would be sanctioned.	B-restatement	3165..3216
Such moves would greatly reduce the power of government ministries, who now jealously guard their turf and are seen as one of the major obstacles blocking economic reform.	I-restatement	3217..3388
FINANCES.	B-entrel	3391..3403
Emergency measures would be introduced to ease the country's financial crisis, notably its $200 billion budget deficit.	B-instantiation	3404..3523
Emergency measures would be introduced to ease the country's financial crisis, notably its $200 billion budget deficit.By the end of next year, all loss-making state enterprises would be put out of business or handed over to workers who would buy or lease them or turn them into cooperatives.	B-conjunction	3524..3697
Emergency measures would be introduced to ease the country's financial crisis, notably its $200 billion budget deficit.By the end of next year, all loss-making state enterprises would be put out of business or handed over to workers who would buy or lease them or turn them into cooperatives.Similar steps would be taken to liquidate unprofitable state and collective farms by the end of 1991.	B-conjunction	3698..3799
Emergency measures would be introduced to ease the country's financial crisis, notably its $200 billion budget deficit.By the end of next year, all loss-making state enterprises would be put out of business or handed over to workers who would buy or lease them or turn them into cooperatives.Similar steps would be taken to liquidate unprofitable state and collective farms by the end of 1991.A unified system of taxation should be introduced rapidly.	B-conjunction	3800..3858
To mop up some of the 300 billion rubles in circulation, the government should encourage home ownership, including issuing bonds that guarantee holders the right to purchase an apartment.	I-conjunction	3859..4046
LABOR.	B-entrel	4049..4058
A genuine market for labor and wages would replace the present rigid, centralized system.	B-contrast	4059..4148
Departing from decades of Soviet dogma, the new system would lead to big differences in pay between workers and almost certainly to unemployment.	B-cause	4149..4294
To cushion the blows, the government would introduce a minimum wage and unemployment benefits.	I-cause	4295..4389
PRICES.	B-entrel	4392..4402
The entire system of centrally set prices would be overhauled, and free-market prices introduced for most wholesale trade and some retail trade.	B-cause	4403..4547
Consumers would still be able to buy some food and household goods at subsidized prices, but luxury and imported items, including food, would be sold at market prices.	B-list	4548..4715
Consumers would still be able to buy some food and household goods at subsidized prices, but luxury and imported items, including food, would be sold at market prices.Wholesale prices would be divided into three categories: raw materials sold at fixed prices close to world levels; government-set procurement prices for a small number of key products; and free prices for everything else to be determined by contracts between suppliers and purchasers.	B-list	4716..5000
Inflation-adjusted social benefits would ensure that the poor and elderly don't suffer unduly.	I-list	5001..5095
FOREIGN TRADE.	B-entrel	5098..5115
The current liberalization and decentralization of foreign trade would be taken much further.	B-restatement	5116..5209
Soviet companies would face fewer obstacles for exports and could even invest their hard currency abroad.	B-list	5210..5315
Soviet companies would face fewer obstacles for exports and could even invest their hard currency abroad.Foreigners would receive greater incentives to invest in the U.S.S.R.	B-list	5316..5385
Soviet companies would face fewer obstacles for exports and could even invest their hard currency abroad.Foreigners would receive greater incentives to invest in the U.S.S.R. Alongside the current non-convertible ruble, a second currency would be introduced that could be freely exchanged for dollars and other Western currencies.	B-list	5386..5541
A domestic foreign exchange market would be set up as part of an overhaul of the nation's banking system.	I-list	5542..5647
The blueprint is at its vaguest when referring to the fate of the two powerful economic institutions that seem likely to oppose such sweeping plans: the State Planning Committee, known as Gosplan, and the State Committee for Material Supply, or Gossnab.	O	5650..5903
But it hints strongly that both organizations would increasingly lose their clout as the changes, particularly the introduction of wholesale trade and the breakup of state monopolies, take effect.	O	5904..6100
