In terms of sheer brutality, the Somali regime of Siad Barre may rank as No. 1 in the world.	O	9..101
The only reason that Somalia remains in obscurity is numbers: a sparsely populated wasteland of 8.5 million people spread out over an expanse nearly the size of Texas.	B-cause	102..269
The Barre dictatorship simply is limited in the amount of people it can torture and kill.	I-cause	270..359
Beheading small children, stabbing elderly people to death, raping and shooting women, and burying people alive are just a few of the grisly activities that the Somali armed forces have been engaged in over the past two years.	O	362..588
Up to 500,000 Somalis have escaped to the relative safety of Marxist Ethiopia because of the behavior of President Barre's troops.	O	591..721
In the port of Berbera, for example, hundreds of men of the rival Issak clan were rounded up in May 1988, imprisoned, and then taken out at night in groups of five to 50 men to be executed without any judicial process whatsoever.	O	724..953
Guns were never used: Each man was stabbed to death with a large knife.	O	954..1025
The horrific details are only now emerging from a painstakingly documented report, based on hundreds of interviews with randomly selected refugees.	B-entrel	1028..1175
The study was done by Robert Gersony, a consultant to the U.S. State Department who has years of experience in investigating human-rights abuses on both sides of the left-right ideological divide.	I-entrel	1176..1372
What gives these events particular significance, however, is the fact that they are part of a wider drama affecting the strategic positions of both the U.S. and the Soviet Union on the horn of Africa.	O	1375..1575
Not since the late 1970s has the horn been so up for grabs as it has suddenly become in just the past few weeks.	O	1576..1688
Mr. Barre's rule is crumbling fast.	B-restatement	1691..1726
Mutinies wrack his armed forces (really just an armed gang), which control less than half the country.	I-restatement	1727..1829
Inflation is at record levels.	O	1830..1860
Desperate, he has called in the Libyans to help fight the rebels of the Somali National Movement in the north, which is only one of several groups picking away at the regime in the capital of Mogadishu.	O	1861..2063
Seventy years old and a self-declared "scientific socialist," President Barre has a power base, composed only of his minority Mareham clan, that according to observers is "narrowing."	O	2064..2247
The U.S.'s interest in Somalia consists of a single runway at the port of Berbera, which U.S. military aircraft have the right to use for surveillance of the Gulf of Aden and the Indian Ocean.	B-entrel	2250..2442
That strip of concrete is backed up by a few one-story, air-conditioned shacks where a handful of American nationals -- buttressed by imported food, cold soft drinks and back issues of Sports Illustrated -- maintain radio contact with the outside world.	I-entrel	2443..2696
In the past two years, the desert behind them has become a land of mass executions and utter anarchy, where, due to Mr. Barre's brutality and ineptitude, nobody is any longer in control.	O	2697..2883
As long as the rival Soviet-backed regime of Mengistu Haile Mariam held a total gridlock over neighboring Ethiopia, the U.S. was forced to accept that lonely Berbera runway as a distant No. 2 to the Soviets' array of airfields next door.	O	2886..3123
But due to dramatic events on the battlefield over the past few days and weeks, those Soviet bases may soon be as endangered and as lonely as the American runway.	O	3124..3286
On Sept. 7, I wrote on these pages about the killing and capturing of 10,000 Ethiopian soldiers by Eritrean and Tigrean guerrillas.	B-entrel	3289..3420
Recently, in Wollo province in the center of Ethiopia, Tigrean forces have killed, wounded and captured an additional 20,000 government troops.	B-entrel	3421..3564
(Think what these numbers mean -- considering the headline space devoted to hundreds of deaths in Lebanon, a small country of little strategic importance!)	I-entrel	3565..3720
Tigrean armies are now 200 miles north of Addis Ababa, threatening the town of Dese, which would cut off Mr. Mengistu's capital from the port of Assab, through which all fuel and other supplies reach Addis Ababa.	O	3723..3935
As a result, Mr. Mengistu has been forced to transfer thousands of troops from Eritrea just to hold the town, thereby risking the loss of even more territory in Eritrea only to keep the Tigreans at bay.	O	3936..4138
Mr. Mengistu is in an increasingly weak position: Half his army is tied down defending the northern city of Asmara from the Eritreans.	B-entrel	4141..4275
The weaker he gets, the more he turns toward the U.S. for help.	I-entrel	4276..4339
While the Tigreans are communists, like the Eritreans they are among the most anti-Soviet guerrillas in the world, having suffered more than a decade of aerial bombardment by the Soviet-supplied Mengistu air force.	O	4340..4554
What this all means in shorthand is that Soviet dominance in Ethiopia is collapsing as fast as President Barre's regime in Somalia is.	O	4557..4691
The U.S., therefore, has a historic opportunity both to strike a blow for human rights in Somalia and to undo the superpower flip-flop of the late 1970s on the Horn of Africa.	O	4692..4867
Back to Somalia:	O	4870..4886
The State Department, to its credit, has already begun distancing itself from Mr. Barre, evinced by its decision to publish the Gersony report (which the press has ignored)	B-conjunction	4889..5062
What's more, the U.S. has suspended $2.5 million in military aid and $1 million in economic aid.	I-conjunction	5063..5159
But this is not enough.	B-cause	5162..5185
Because the U.S. is still perceived to be tied to Mr. Barre, when he goes the runway could go too.	I-cause	5186..5284
Considering how tenuous the security of that runway is anyway, the better option -- both morally and strategically -- would be for the Bush administration to blast the regime publicly, in terms clear enough for all influential Somalis to understand.	B-cause	5285..5534
It is a certainty that Mr. Barre's days are numbered.	I-cause	5535..5588
The U.S. should take care, however, that its own position in the country does not go down with him.	O	5589..5688
Nobody is sure what will come next in Somalia or whom the successor might be.	O	5691..5768
But as one expert tells me: "Whoever it is will have to work pretty damn hard to be worse than Barre."	O	5769..5871
While the State Department positions itself for the post-Barre period in Somalia, it should continue to back former President Carter's well-intentioned role as a mediator between Mr. Mengistu and the Eritrean guerrillas in Ethiopia, while concomitantly opening up channels of communications with the Tigrean rebels through neighboring Sudan.	O	5874..6215
Ethiopian politics are the most sophisticated, secretive and Byzantine in all of black Africa.	O	6218..6312
Remember that it took Mr. Mengistu many months, in what became known as the "creeping coup," to topple Emperor Haile Selassie in 1974 and 1975.	O	6313..6456
There is simply no way to engineer a succession covertly, as is sometimes possible elsewhere on the continent.	O	6457..6567
But the U.S. has one great advantage: The Soviets are universally loathed throughout Ethiopia for what they did to the country this past decade -- famine and all.	O	6568..6730
It's not just in Eastern Europe where the march of events is finally on the U.S. side, but on the horn of Africa as well.	B-entrel	6733..6854
The only U.S. liability in the region is what remains of the link to Mr. Barre, and that should be cut fast.	I-entrel	6855..6963
Mr. Kaplan, author of "Surrender or Starve: The Wars Behind the Famine" (Westview Press, 1988), lives in Lisbon.	O	6966..7078
