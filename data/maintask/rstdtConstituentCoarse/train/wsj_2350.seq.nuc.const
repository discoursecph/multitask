Time magazine , in a move	(NS-Elaboration(NS-Summary-Evaluation-Topic(NS-Temporal(NS-Temporal(NS-Elaboration(NN-Textual-organization(NS-Elaboration
to reduce the costs of wooing new subscribers ,	NS-Elaboration)
is lowering its circulation guarantee to advertisers for the second consecutive year ,	(NN-Joint
increasing its subscription rates	(NN-Joint
and cutting back on merchandise giveaways .	NN-Joint)))
In an announcement to its staff last week , executives at Time Warner Inc. 's weekly magazine said	(NN-Joint(SN-Attribution
Time will `` dramatically de-emphasize '' its use of electronic giveaways such as telephones in television subscription drives ;	(NN-Joint
cut the circulation	(NN-Joint(NN-Textual-organization(NS-Elaboration
it guarantees advertisers	NS-Elaboration)
by 300,000 , to four million ;	NN-Textual-organization)
and increase the cost of its annual subscription rate by about $ 4 to $ 55 .	NN-Joint)))
In a related development , the news-weekly , for the fourth year in a row , said	(SN-Comparison(NS-Elaboration(SN-Attribution
it wo n't increase its advertising rates in 1990 ;	SN-Attribution)
a full , four-color page in the magazine costs about $ 120,000 .	NS-Elaboration)
However ,	(NS-Attribution(NN-Textual-organization
because the guaranteed circulation base is being lowered ,	(SN-Cause
ad rates will be effectively 7.5 % higher per subscriber ,	SN-Cause))
according to Richard Heinemann , Time associate publisher .	NS-Attribution))))
Time is following the course of some other mass-circulation magazines	(NS-Elaboration(NS-Elaboration
that in recent years have challenged the publishing myth	(NS-Elaboration
that maintaining artificially high , and expensive , circulations is the way	(NS-Elaboration
to draw advertisers .	NS-Elaboration)))
In recent years , Reader 's Digest , New York Times Co. 's McCall 's , and most recently News Corp. 's TV Guide , have cut their massive circulation rate bases	(NS-Cause
to eliminate marginal circulation	(NN-Joint
and hold down rates for advertisers .	NN-Joint))))
Deep discounts in subscriptions and offers of free clock radios and watches have become accepted forms	(NS-Elaboration
of attracting new subscribers in the hyper-competitive world of magazine news-weeklies .	(NS-Elaboration(SN-Comparison
But Time , as part of the more cost-conscious Time Warner , wants to wean itself away from expensive gimmicks .	SN-Comparison)
Besides , Time executives think	(NS-Elaboration(SN-Attribution
selling a news magazine with a clock radio is tacky .	SN-Attribution)
`` Giveaways just give people the wrong image , ''	(NS-Elaboration(NS-Attribution
said Mr. Heinemann .	NS-Attribution)
`` That perception takes the focus off the magazine . ''	NS-Elaboration)))))
Time magazine executives predictably paint the circulation cut as a show of strength and actually a benefit to advertisers .	(NS-Elaboration(SN-Comparison(NS-Cause
`` What we are doing is screening out the readers	(NS-Elaboration(NS-Attribution(NS-Elaboration
who are only casually related to the magazine	(NN-Joint
and do n't really read it , ''	NN-Joint))
said Mr. Heinemann .	NS-Attribution)
`` We are trying to create quality and involvement . ''	NS-Elaboration))
However , Time executives used the same explanation	(NS-Elaboration(NS-Temporal
when in October 1988 the magazine cut its guaranteed circulation from 4.6 million to 4.3 million .	NS-Temporal)
And Time 's paid circulation ,	(NN-Textual-organization(NS-Attribution
according to Audit Bureau of Circulations ,	NS-Attribution)
dropped 7.3 % to 4,393,237 in the six months	(NS-Elaboration
ended June 30 , 1989 .	NS-Elaboration))))
Still , Time 's move is being received well , once again .	(NS-Elaboration
`` It 's terrific for advertisers to know	(NS-Elaboration(NS-Attribution(SN-Attribution
the reader will be paying more , ''	SN-Attribution)
said Michael Drexler , national media director at Bozell Inc. ad agency .	NS-Attribution)
`` A few drops in circulation are of no consequence .	(NS-Elaboration
It 's not a show of weakness ;	(NN-Joint
they are improving the quality of circulation	(NS-Temporal
while insuring their profits . ''	NS-Temporal)))))))
Mr. Heinemann said	(NN-Comparison(NS-Elaboration(SN-Attribution
the changes represent a new focus in the magazine industry :	(NS-Elaboration
a magazine 's net revenue per subscriber , or the actual revenue from subscribers	(NS-Temporal
after discounts and the cost of premiums have been stripped away .	NS-Temporal)))
`` The question is how much are we getting from each reader , ''	(NS-Attribution
said Mr. Heinemann .	NS-Attribution))
Time 's rivals news-weeklies , Washington Post Co. 's Newsweek and U.S. News & World Report , are less reliant on electronic giveaways ,	(NS-Elaboration(NS-Elaboration
and in recent years both have been increasing their circulation rate bases .	NS-Elaboration)
Both magazines are expected to announce their ad rates and circulation levels for 1990 within a month .	NS-Elaboration)))
