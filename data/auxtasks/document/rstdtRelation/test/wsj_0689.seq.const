Wanted :	(Summary(Elaboration
An investment	(Elaboration
that 's as simple and secure as a certificate of deposit	(Joint
but offers a return	(Elaboration
worth getting excited about .	Elaboration))))
With $ 150 billion of CDs	(Elaboration(Elaboration(Elaboration(Contrast(Elaboration(Explanation(Background(Background(Elaboration
maturing this month ,	Elaboration)
a lot of people have been scouring the financial landscape for just such an investment .	Background)
In April ,	(Background(Contrast(Background(Same-unit(Temporal
when many of them bought their CDs ,	Temporal)
six-month certificates were yielding more than 9 % ;	Same-unit)
investors	(Same-unit(Elaboration
willing to look	Elaboration)
could find double-digit yields at some banks and thrifts .	Same-unit))
Now , the nationwide average yield on a six-month CD is just under 8 % ,	(Elaboration
and 8.5 % is about the best around .	Elaboration))
But investors	(Same-unit(Elaboration
looking for alternatives	Elaboration)
are n't finding it easy .	Same-unit)))
Yields on most fixed-income securities are lower than several months ago .	(Joint
And the stock market 's recent gyrations are a painful reminder of the dangers there .	Joint))
`` If you 're looking for a significantly higher yield with the same level of risk as a CD ,	(Attribution(Condition
you 're not going to find it , ''	Condition)
says Washington financial planner Dennis M. Gurtz .	Attribution))
There are , however , some alternatives	(Attribution(Elaboration
that income-oriented investors should consider ,	Elaboration)
investment advisers say .	Attribution))
Short-term municipal bonds , bond funds and tax-deferred annuities are some of the choices	(Elaboration(Elaboration
they mention	Elaboration)
-- and not just as a way	(Elaboration
to get a higher return .	Elaboration)))
In particular ,	(Topic-Comment(Same-unit(Attribution
advisers say ,	Attribution)
investors may want to look at securities	(Elaboration
that reduce the risk	(Elaboration(Elaboration
that CD holders are confronting right now ,	Elaboration)
of having to reinvest the proceeds of maturing short-term certificates at lower rates .	Elaboration)))
A mix of CDs and other holdings may make the most sense .	(Elaboration
`` People should remember	(Attribution(Attribution
their money is n't all or nothing	(Summary
-- they do n't need to be shopping for the one interest-rate-type investment and putting all their money in it , ''	Summary))
says Bethesda , Md. , adviser Karen Schaeffer .	Attribution))))
Here 's a look at some of the alternatives :	(Elaboration
SHORT-TERM MUNICIPALS :	(Joint(Elaboration
Investors with a heavy tax load should take out their calculators .	(Background(Elaboration
Yields on municipal bonds can be higher than after-tax yields on CDs for maturities of perhaps one to five years .	(Elaboration(Explanation
That 's because municipal-bond interest is exempt from federal income tax	(Elaboration
-- and from state and local taxes too , for in-state investors .	Elaboration))
For an investor	(Same-unit(Elaboration
paying tax at a 33 % rate ,	Elaboration)
a seemingly puny 6 % yield on a one-year muni is equivalent to a taxable 9 % .	(Comparison
Rates approach 6.5 % on five-year municipals .	Comparison))))
Some of the more cautious CD holders might like `` pre-refunded '' municipals .	(Elaboration(Elaboration(Elaboration
These securities get top credit ratings	(Explanation
because the issuers have put aside U.S. bonds	(Elaboration
that will be sold	(Enablement
to pay off holders	(Background
when the municipals are retired .	Background)))))
`` It 's a no-brainer :	(Attribution(Elaboration
You do n't have to worry about diversification ;	(Joint
you do n't have to worry about quality , ''	Joint))
says Steven J. Hueglin , executive vice president of the New York bond firm of Gabriele , Hueglin & Cashman Inc .	Attribution))
Consider a `` laddered '' bond portfolio , with issues	(Elaboration(Attribution(Elaboration
maturing in , say , 1992 , 1993 and 1994 ,	Elaboration)
advises Malcolm A. Makin , a Westerly , R.I. , financial planner .	Attribution)
The idea is to have money rolling over each year at prevailing interest rates .	Elaboration))))
BOND FUNDS :	(Joint(Elaboration
Bond mutual funds offer diversification	(Elaboration(Evaluation(Joint
and are easy to buy and sell .	Joint)
That makes them a reasonable option for investors	(Elaboration(Elaboration
who will accept some risk of price fluctuation	(Enablement
in order to make a bet	(Elaboration
that interest rates will decline over the next year or so .	Elaboration)))
Buyers can look forward to double-digit annual returns	(Contrast(Condition
if they are right .	Condition)
But they will have disappointing returns or even losses	(Condition
if interest rates rise instead .	Condition))))
Bond resale prices , and thus fund share prices , move in the opposite direction from rates .	(Elaboration(Elaboration(Elaboration
The price movements get bigger	(Background
as the maturity of the securities lengthens .	Background))
Consider , for instance , two bond funds from Vanguard Group of Investment Cos .	(Elaboration(Elaboration
that were both yielding 8.6 % on a recent day .	Elaboration)
The Short Term Bond Fund , with an average maturity of 2 1/2 years , would deliver a total return for one year of about 10.6 %	(Contrast(Same-unit(Condition
if rates drop one percentage point	Condition)
and a one-year return of about 6.6 %	(Condition
if rates rise by the same amount .	Condition))
But , in the same circumstances , the returns would be a more extreme 14.6 % and 2.6 % for the Vanguard Bond Market Fund , with its 12 1/2-year average maturity .	Contrast)))
`` You get equity-like returns '' from bonds	(Elaboration(Attribution(Condition
if you guess right on rates ,	Condition)
says James E. Wilson , a Columbia , S.C. , planner .	Attribution)
If interest rates do n't change ,	(Condition
bond fund investors ' returns will be about equal to the funds ' current yields .	Condition)))))
DEFERRED ANNUITIES :	(Joint(Elaboration
These insurance company contracts feature some of the same tax benefits and restrictions as non-deductible individual retirement accounts :	(Elaboration(Elaboration(Elaboration
Investment gains are compounded	(Contrast(Temporal(Background
without tax consequences	Background)
until money is withdrawn ,	Temporal)
but a 10 % penalty tax is imposed on withdrawals	(Elaboration
made before age 59 1/2 .	Elaboration)))
Aimed specifically at CD holders are so-called CD-type annuities , or certificates of annuity .	(Explanation(Elaboration
An interest rate is guaranteed for between one and seven years ,	(Temporal
after which holders get 30 days	(Enablement
to choose another guarantee period	(Background(Joint
or to switch to another insurer 's contract	Joint)
without the surrender charges	(Elaboration
that are common to annuities .	Elaboration)))))
Some current rates exceed those on CDs .	(Elaboration
For instance , a CD-type annuity from North American Co. for Life & Health Insurance , Chicago , offers 8.8 % interest for one year or a 9 % rate for two years .	Elaboration)))
Annuities are rarely a good idea at age 35	(Contrast(Explanation
because of the withdrawal restrictions .	Explanation)
But at age 55 , `` they may be a great deal , ''	(Attribution
says Mr. Wilson , the Columbia , S.C. , planner .	Attribution))))
MONEY MARKET FUNDS :	(Elaboration
That 's right ,	(Elaboration(Evaluation
money market mutual funds .	Evaluation)
The conventional wisdom is to go into money funds	(Elaboration(Elaboration(Contrast(Elaboration(Contrast(Background
when rates are rising	Background)
and shift out at times such as the present ,	(Background
when rates seem headed down .	Background))
With average maturities of a month or so , money funds offer fixed share prices and floating returns	(Same-unit(Elaboration
that track market interest rates ,	Elaboration)
with a slight lag .	Same-unit))
Still , today 's highest-yielding money funds may beat CDs over the next year	(Explanation(Attribution(Contrast
even if rates fall ,	Contrast)
says Guy Witman , an editor of the Bond Market Advisor newsletter in Atlanta .	Attribution)
That 's because top-yielding funds currently offer yields almost 1 1/2 percentage points above the average CD yield .	Explanation))
Mr. Witman likes the Dreyfus Worldwide Dollar Money Market Fund , with a seven-day compound yield just under 9.5 % .	(Elaboration
A new fund , its operating expenses are being temporarily subsidized by the sponsor .	Elaboration))
Try combining a money fund and an intermediate-term bond fund as a low-risk bet on falling rates ,	(Explanation(Attribution
suggests Back Bay Advisors Inc. , a mutual fund unit of New England Insurance Co .	Attribution)
If rates unexpectedly rise ,	(Condition
the increasing return on the money fund will partly offset the lower-than-expected return from the bond fund .	Condition)))))))))))
