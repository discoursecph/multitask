They did n't play the third game of the World Series on Tuesday night as scheduled	root	9..90
and they did n't play it on Wednesday or Thursday either .	conjunction	91..148
But you knew that , did n't you ?	conjunction	149..179
They are supposed to play the game next Tuesday , in Candlestick Park here .	norel	182..256
The theory is that the stadium , damaged by Tuesday 's earthquake , will be repaired by then , and that people will be able to get there .	cause	257..390
Like just about everything else , that remains to be seen .	contrast	391..448
Aftershocks could intervene .	cause	449..477
But , at least , the law of averages should have swung to the favorable side .	contrast	478..553
It may seem trivial to worry about the World Series amid the destruction to the Bay Area wrought by Tuesday 's quake	norel	556..671
but the name of this column is " On Sports , " so I feel obliged to do so .	contrast	672..744
You might be interested to know that baseball , not survival , appeared to be the first thought of most of the crowd of 60,000-odd that had gathered at Candlestick at 5:04 p.m. Tuesday , a half-hour before game time , when the quake struck .	entrel	745..981
As soon as the tremor passed , many people spontaneously arose and cheered , as though it had been a novel kind of pre-game show .	conjunction	982..1109
One fan , seated several rows in front of the open , upper-deck auxiliary press section where I was stationed , faced the assembled newsies and laughingly shouted , " We arranged that just for you guys ! "	norel	1112..1310
I thought and , I 'm sure , others did : " You should n't have bothered . "	norel	1311..1378
I 'd slept through my only previous brush with natural disaster , a tornado 15 or so summers ago near Traverse City ,	norel	1381..1500
, so I was unprepared for one reaction to such things : the urge to talk about them .	cause	1501..1584
Perhaps primed by the daily diet of radio and TV reporters thrusting microphones into people 's faces and asking how they " feel " about one calamity or another , fellow reporters and civilians who spied my press credential were eager to chat .	contrast	1585..1824
" It felt like I was on a station platform and a train went by , " said one man , describing my own reaction .	norel	1827..1932
A women said she saw the park 's light standards sway .	conjunction	1933..1986
A man said he saw the upper rim undulate .	conjunction	1987..2028
I saw neither .	contrast	2029..2043
Dictates of good sense to the contrary not withstanding , the general inclination was to believe that the disturbance would be brief and that ball would be played .	norel	2046..2208
" I was near the top of the stadium , and saw a steel girder bow six feet from where I sat , but I stayed put for 10 or 15 minutes , " confessed a friend .	instantiation	2209..2358
" I guess I thought , ' This is the World Series and I 'm not gon na wimp out ! ' "	cause	2359..2434
Here in the Global Village , though , folks do not stay uninformed for long .	norel	2437..2511
Electrical power was out in still-daylighted Candlestick Park	cause	2512..2573
but battery-operated radios and television sets were plentiful .	contrast	2574..2638
Within a few minutes , the true extent of the catastrophe was becoming clear .	cause	2639..2715
Its Richter Scale measurement was reported as 6.5 , then 6.9 , then 7.0 .	restatement	2716..2786
A section of the Bay Bridge had collapsed , as had a part of Interstate Highway 880 in Oakland .	conjunction	2787..2881
People had died .	conjunction	2882..2898
At 5:40 p.m. , scheduled game time having passed , some fans chanted " Let 's Play Ball . "	norel	2901..2986
No longer innocent , they qualified as fools .	contrast	2987..3031
The stadium was ordered evacuated soon afterward	asynchronous	3032..3080
the announcement , made over police bullhorns , cited the power outage , but it later was revealed that there also had been damage of the sort reported by my friend .	cause	3081..3244
Outside , I spotted two young men lugging blocks of concrete .	conjunction	3245..3305
" Pieces of Candlestick , " they said .	entrel	3306..3341
The crowd remained good natured , even bemused .	norel	3344..3390
TV reporters interviewed fans in the parking lots	entrel	3391..3440
while , a few feet away , others watched the interviews on their portable TVs .	synchrony	3441..3517
The only frenzy I saw was commercial	conjunction	3518..3554
Booths selling World Series commemorative stamps and dated postmarks were besieged by fledgling speculators who saw future profit in the items .	restatement	3555..3699
The traffic jam out of the park was monumental .	norel	3702..3749
It took me a half-hour to move 10 feet from my parking spot in an outer lot to an aisle , and an additional hour to reach an inner roadway a half-block away .	restatement	3750..3906
The six-mile trip to my airport hotel that had taken 20 minutes earlier in the day took more than three hours .	conjunction	3907..4017
At my hotel , the Westin , power was out , some interior plaster had broken loose	norel	4020..4098
and there had been water damage , but little else .	conjunction	4099..4148
With Garpian randomness , a hotel across the street , the Amfac , had been hit harder	contrast	4149..4231
A large sheet of its concrete facade and several window balconies were torn away .	restatement	4232..4314
The Westin staff had , kindly , set out lighted candles in the ballroom , prepared a cold-cuts buffet and passed around pillows and blankets .	norel	4317..4455
I fell asleep on the lobby floor , next to a man wearing a Chicago Cubs jacket .	entrel	4456..4534
I expected him to say , " I told you so	entrel	4535..4572
" but he already was snoring .	contrast	4573..4602
The journalistic consensus was that the earthquake made the World Series seem unimportant .	norel	4605..4695
My response was that sports rarely are important , only diverting	contrast	4696..4760
and the quake merely highlighted that fact .	conjunction	4761..4805
Should the rest of the Series be played at all ?	norel	4808..4855
Sure .	norel	4856..4861
The quake and baseball were n't related , unlike the massacre of athletes that attended the 1972 Olympics .	cause	4862..4966
That heavily politicized event learned nothing from the horrifying experience , and seems doomed to repeat it .	contrast	4967..5076
Two ironies intrude .	norel	5079..5099
This has been widely dubbed the BART Series , after the local subway line , and the Bay Bridge Series .	conjunction	5100..5200
Flags fly at half-staff for the death of Bart Giamatti , the late baseball commissioner	conjunction	5201..5287
and now the Bay Bridge lies in ruins .	conjunction	5288..5326
A Series that was shaping up as the dullest since the one-sided Detroit-over-San Diego go of 1984 has become memorable in the least fortunate way .	cause	5327..5473
Still , its edge is lost .	comparison	5474..5498
It now will be played mostly for the record , and should be wrapped up as quickly as possible , without " off " days .	cause	5499..5612
And I will never again complain about a rainout .	conjunction	5615..5663
