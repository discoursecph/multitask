Donald Trump ,	(Topic-Change(Elaboration(Background(Elaboration(Elaboration(Same-unit(Elaboration
who faced rising doubt about his bid for American Airlines parent AMR Corp .	(Contrast
even before a United Airlines buy-out came apart Friday ,	Contrast))
withdrew his $ 7.54 billion offer .	Same-unit)
Separately , bankers	(Contrast(Explanation(Same-unit(Elaboration
representing the group	(Elaboration
trying to buy United 's parent UAL Corp .	Elaboration))
met with other banks	Same-unit)
about reviving that purchase at a lower price , possibly around $ 250 a share , or $ 5.65 billion .	Explanation)
But a lower bid could face rejection by the UAL board .	Contrast))
Mr. Trump ,	(Elaboration(Elaboration(Attribution(Same-unit(Elaboration
who vowed Wednesday to `` go forward '' with the bid ,	Elaboration)
said	Same-unit)
he was dropping it `` in light of the recent change in market conditions . ''	Attribution)
He said	(Attribution
he might now sell his AMR stake ,	(Condition
buy more shares ,	(Condition
or make another offer at a lower price .	Condition))))
The Manhattan real-estate developer acted	(Cause(Background
after the UAL buyers failed to obtain financing for their earlier $ 300-a-share bid ,	Background)
which sparked a selling panic among analysts	(Elaboration
that snowballed into a 190-point drop Friday in the Dow Jones Industrial Average .	Elaboration))))
News about UAL and AMR ,	(Elaboration(Elaboration(Same-unit(Elaboration
whose shares never reopened	(Background
after trading was halted Friday for the UAL announcement ,	Background))
sent both stocks nosediving in composite trading on the New York Stock Exchange .	Same-unit)
UAL tumbled $ 56.875 to $ 222.875 on volume of 2.3 million shares ,	(Background(Joint
and AMR declined by $ 22.125 to $ 76.50	Joint)
as 4.7 million shares changed hands .	Background))
Together , the two stocks wreaked havoc among takeover stock traders ,	(Evaluation(Cause
and caused a 7.3 % drop in the Dow Jones Transportation Average , second in size only to the stock-market crash of Oct. 19 , 1987 .	Cause)
Some said	(Background(Attribution
Friday 's market debacle had given Mr. Trump an excuse	(Elaboration
to bail out of an offer	(Elaboration
that showed signs	(Contrast(Elaboration
of stalling	Elaboration)
even before problems emerged with the UAL deal .	Contrast))))
After reaching an intraday high of $ 107.50 the day Mr. Trump disclosed his bid Oct. 17 ,	(Background(Temporal
AMR 's stock had retreated as low as $ 97.75 last week .	Temporal)
Some takeover stock traders had been betting against Mr. Trump	(Explanation(Explanation
because he has a record	(Elaboration
of disclosing stakes in companies	(Temporal(Elaboration
that are potential takeover targets ,	Elaboration)
then selling at a profit	(Manner-Means
without making a bid .	Manner-Means))))
`` He still has n't proven his mettle as a big-league take-out artist , ''	(Elaboration(Attribution
said airline analyst Kevin Murphy of Morgan Stanley & amp ; Co .	Attribution)
`` He 's done this thing	(Topic-Comment(Elaboration(Elaboration
where he 'll buy a little bit of a company	(Temporal
and then trade out of it .	Temporal))
He 's written this book , ` The Art of the Deal .	Elaboration)
' Why does n't he just follow through on one of these things ? ''	Topic-Comment))))))))
Mr. Trump withdrew his bid	(Elaboration(Elaboration(Temporal
before the AMR board ,	(Same-unit(Elaboration
which is due to meet tomorrow ,	Elaboration)
ever formally considered it .	Same-unit))
AMR had weighed a wide range of possible responses , from flat rejection to recapitalizations and leveraged buy-outs	(Joint(Elaboration
that might have included either employees , a friendlier buyer such as Texas billionaire Robert Bass , or both .	Elaboration)
AMR had also sought to foil Mr. Trump in Congress	(Manner-Means
by lobbying for legislation	(Elaboration
that would have bolstered the authority of the Transportation Department	(Elaboration
to reject airline buy-outs .	Elaboration)))))
Yesterday , Mr. Trump tried to put the blame for the collapse of the UAL deal on Congress ,	(Explanation(Manner-Means
saying	(Attribution
it was rushing through a bill	(Enablement
to protect AMR executives .	Enablement)))
`` I believe	(Elaboration(Elaboration(Attribution(Attribution
that the perception	(Same-unit(Elaboration
that legislation in this area may be hastily approved	Elaboration)
contributed to the collapse of the UAL transaction , and the resulting disruption in the financial markets	(Elaboration
experienced this past Friday , ''	Elaboration)))
Mr. Trump wrote members of Congress .	Attribution)
AMR declined to comment ,	(Joint
and Mr. Trump did n't respond to requests for interviews .	Joint))
Mr. Trump never said	(Elaboration(Contrast(Contrast(Attribution
how much AMR stock he had bought ,	Attribution)
only that his holdings were `` substantial . ''	Contrast)
However , he only received federal clearance	(Elaboration(Elaboration
to buy more than $ 15 million of the stock on Sept. 20 ,	(Background
when the price rose $ 3.00 a share to $ 78.50 .	Background))
Between then and his bid on Oct. 17 , the price fluctuated between $ 75.625 and $ 87.375 .	Elaboration))
In an attempt	(Evaluation(Background(Background(Same-unit(Elaboration
to persuade investors	(Attribution
that his bid was n't just `` a stock play , ''	Attribution))
Mr. Trump promised last week to notify the market	Same-unit)
before selling any shares .	Background)
AMR was trading at around $ 84 yesterday before his withdrawal announcement ,	(Elaboration(Elaboration(Temporal
then immediately fell to about $ 76 .	Temporal)
Assuming	(Condition(Joint(Attribution
that he paid a rough average price of $ 80 a share ,	Attribution)
and assuming	(Attribution
he did n't sell	(Background
before his announcement reached the market ,	Background)))
Mr. Trump could be sitting with a modest loss with the stock at $ 76.50 .	Condition))
Some analysts said	(Contrast(Attribution
AMR Chairman Robert Crandall might seize the opportunity	(Elaboration
presented by the stock price drop	(Enablement
to protect the nation 's largest airline with a defensive transaction , such as the sale of stock to a friendly holder or company employees .	Enablement)))
However , other knowledgeable observers said	(Attribution
they believed Mr. Crandall and the AMR board might well decide to tough it out	(Manner-Means
without taking any extra steps .	Manner-Means)))))
Some analysts said	(Contrast(Elaboration(Attribution
they believed Mr. Trump ,	(Elaboration
whose towering ego had been viewed by some as a reason	(Elaboration
to believe	(Attribution
he would n't back out ,	(Joint
might come back with a lower bid .	Joint)))))
Ray Neidl of Dillon Read & amp ; Co. said	(Evaluation(Attribution
Mr. Trump `` is stepping back	(Joint
and waiting for the dust to settle .	Joint))
I 'm sure	(Attribution
he still wants AMR . ''	Attribution)))
But others remained skeptical .	(Evaluation(Elaboration
`` I was never sure	(Explanation(Attribution(Attribution
Donald Trump really wanted to take AMR , ''	Attribution)
said John Mattis , a bond analyst with Shearson Lehman Hutton Inc .	Attribution)
`` What happened with United was a gracious way for him	(Elaboration(Elaboration
to bow out . ''	Elaboration)
Mr. Trump never obtained financing for his bid .	Elaboration)))
That skepticism would leave him with an even greater credibility problem	(Elaboration(Condition
should he return	Condition)
that would handicap him in any effort	(Elaboration
to oust the board in a proxy fight .	Elaboration))))))))))
Meanwhile , Citicorp and Chase Manhattan Corp. , the two lead lenders on the UAL buy-out , met with other banks yesterday	(Background(Elaboration(Enablement
to determine	(Attribution
if they would be willing to finance the buy-out at a lower price .	Attribution))
Officials familiar with the talks said	(Elaboration(Contrast(Attribution
Citicorp had discussed lowering the offer to $ 250 a share ,	Attribution)
but said	(Attribution
that price was a talking point	(Joint
and that no decision has been made .	Joint)))
At $ 250 a share , the group would have to borrow about $ 6.1 billion from banks .	Elaboration))
The first UAL deal unraveled	(Background(Background
after Citibank and Chase could n't raise $ 7.2 billion .	Background)
Citibank and Chase had agreed to commit $ billion ,	(Evaluation(Elaboration(Joint
and said	(Attribution
they were `` highly confident ''	(Background
of raising another $ 4.2 billion .	Background)))
Together , Citicorp and Chase received $ million in fees	(Contrast(Enablement
to raise the rest of the financing .	Enablement)
But other banks balked at the low interest rate and banking fees	(Elaboration
the UAL group was willing to pay them .	Elaboration)))
Officials familiar with the bank talks said	(Elaboration(Elaboration(Attribution
the UAL buy-out group	(Contrast(Same-unit(Elaboration
-- UAL pilots , management , and British Airways PLC --	Elaboration)
is now willing to pay higher bank fees and interest ,	Same-unit)
but is n't likely to boost its $ 965 million equity contribution .	(Elaboration
Nor is the group likely to come forward with a revised offer within the next 48 hours	(Elaboration(Cause(Contrast
despite the hopes of many traders .	Contrast)
The group 's advisers want to make certain	(Attribution
they have firm bank commitments the second time around .	Attribution))
Even if the buy-out group is able to obtain financing ,	(Explanation(Contrast
the transaction still faces obstacles .	Contrast)
UAL 's board could reject the new price as too low ,	(Explanation
especially since there are n't any competing bids .	Explanation))))))
Los Angeles investor Marvin Davis ,	(Elaboration(Same-unit(Elaboration
whose $ 275-a-share offer was rejected by UAL 's board ,	Elaboration)
has n't shown signs	(Elaboration
of pursuing a $ 300-a-share back-up bid	(Elaboration
he made last month .	Elaboration)))
In addition , the coalition of labor and management , longtime enemies	(Same-unit(Elaboration
who joined forces only under the threat of Mr. Davis 's bid ,	Elaboration)
could break apart now .	Same-unit)))
The group 's resilience gets its first test today	(Elaboration(Background
when 30 top pilot union leaders convene outside Chicago in a previously scheduled meeting .	Background)
Union Chairman F.C .	(Same-unit(Elaboration
( Rick )	Elaboration)
Dubinsky faces the tough task	(Elaboration(Elaboration
of explaining	(Attribution
why banks refused to finance a buy-out	(Elaboration
the members approved overwhelmingly last week .	Elaboration)))
The pilot union is vowing to pursue an acquisition	(Contrast(Condition
whatever the board decides .	Condition)
But	(Joint(Condition(Same-unit
if the board rejects a reduced bid	(Condition(Joint
and decides to explore other alternatives ,	Joint)
it could transform what has been a harmonious process into an adversarial one .	Condition))
The pilots could play hardball	(Explanation(Manner-Means
by noting	(Attribution
they are crucial to any sale or restructuring	(Cause
because they can refuse to fly the airplanes .	Cause)))
If they were to insist on a low bid of , say $ 200 a share ,	(Cause(Condition
the board might n't be able to obtain a higher offer from other bidders	Condition)
because banks might hesitate to finance a transaction	(Elaboration
the pilots oppose .	Elaboration))))
Also ,	(Cause(Same-unit
because UAL Chairman Stephen Wolf and other UAL executives have joined the pilots ' bid ,	(Cause
the board might be forced to exclude him from its deliberations	(Enablement
in order to be fair to other bidders .	Enablement)))
That could cost him the chance	(Elaboration
to influence the outcome	(Cause
and perhaps join the winning bidder .	Cause)))))))))))))
