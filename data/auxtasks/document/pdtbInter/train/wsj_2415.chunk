Put down that phone.	O	9..29
Walk around the room; take two deep breaths.	B-conjunction	32..76
Resist the urge to call your broker and sell all your stocks.	I-conjunction	77..138
That's the advice of most investment professionals after Friday's 190-point drop in the Dow Jones Industrial Average.	O	141..258
No one can say for sure what will happen today.	B-conjunction	261..308
And investment pros are divided on whether stocks will perform well or badly in the next six months.	B-contrast	309..409
But they're nearly unanimous on one point: Don't sell into a panic.	I-contrast	410..477
Investors who sold everything after the crash of 1987 lived to regret it.	B-conjunction	480..553
Even after Friday's plunge, the Dow Jones Industrial Average was 48% above where it landed on Oct. 19 two years ago.	I-conjunction	554..670
Panic selling also was unwise during other big declines in the past.	B-instantiation	673..741
The crash of 1929 was followed by a substantial recovery before the great Depression and awful bear market of the 1930s began.	B-conjunction	742..868
The "October massacres" of 1978 and 1979 were scary, but didn't lead to severe or sustained downturns.	I-conjunction	869..971
Indeed, some pros see Friday's plunge, plus any further damage that might occur early this week, as a chance for bargain hunting.	O	974..1103
"There has been a lot of emotional selling that presents a nice buying opportunity if you've got the cash," says Stephen B. Timbers, chief investment officer of Chicago-based Kemper Financial Services Inc.	O	1104..1309
But most advisers think the immediate course for individual investors should be to stand pat.	O	1312..1405
"When you see a runaway train," says Steve Janachowski, partner in the San Francisco investment advisory firm Brouwer & Janachowski, "you wait for the train to stop."	O	1406..1572
Even for people who expect a bear market in coming months -- and a sizable number of money managers and market pundits do -- the advice is: Wait for the market to bounce back, and sell shares gradually during rallies.	O	1575..1792
The best thing individual investors can do is "just sit tight," says Marshall B. Front, executive vice president and head of investment counseling at Stein Roe & Farnham Inc., a Chicago-based investment counseling firm that manages about $18 billion.	O	1793..2043
On the one hand, Mr. Front says, it would be misguided to sell into "a classic panic."	O	2046..2132
On the other hand, it's not necessarily a good time to jump in and buy.	O	2133..2204
"This is all emotion right now, and when emotion starts to run, it can run further than anyone anticipates," he said.	O	2205..2322
"So it's more prudent to wait and see how things stabilize."	O	2323..2383
Roger Ibbotson, professor of finance at Yale University and head of the market information firm Ibbotson Associates Inc., says, "My real advice would be to just ride through it.	O	2386..2563
Generally, it isn't wise to be in and out" of the stock market.	O	2564..2627
Mr. Ibbotson thinks that this week is "going to be a roller-coaster week.	B-contrast	2630..2704
Mr. Ibbotson thinks that this week is "going to be a roller-coaster week.	B-conjunction	2630..2704
But he also thinks it is "a good week to consider buying."	I-conjunction	2705..2763
John Snyder, former president of the Los Angeles chapter of the National Association of Investors Corp., an organization of investment clubs and individual investors, says his fellow club members didn't sell in the crash of 1987, and see no reason to sell now.	O	2766..3026
"We're dedicated long-term investors, not traders," he says.	O	3027..3087
We understand panics and euphoria.	B-cause	3088..3123
And we hope to take advantage of panics and buy stocks when they plunge."	I-cause	3124..3197
One camp of investment pros sees what happened Friday as an opportunity.	O	3200..3272
Over the next days and weeks, they say, investors should look for stocks to buy.	O	3273..3353
Friday's action "was an old-fashioned panic," says Alfred Goldman, director of technical market analysis for A.G. Edwards & Sons in St. Louis.	O	3356..3498
"Stocks were being thrown out of windows at any price."	O	3499..3554
His advice: "You ought to be there with a basket catching them."	O	3555..3619
James Craig, portfolio manager for the Denver-based Janus Fund, which has one of the industry's better track records, started his buying during Friday's plunge.	O	3622..3782
Stocks such as Hershey Foods Corp., Wal-Mart Stores Inc., American International Group Inc. and Federal National Mortgage Association became such bargains that he couldn't resist them, he says.	O	3783..3976
And Mr. Craig expects to pick up more shares today.	B-entrel	3979..4030
"It will be chaotic at first, but I would not be buying if I thought we were headed for real trouble," he says.	I-entrel	4031..4142
He argues that stocks are reasonably valued now, and that interest rates are lower now than in the fall of 1987.	O	4143..4255
Mr. Front of Stein Roe suggests that any buying should "concentrate in stocks that have lagged the market on the up side, or stocks that have been beaten down a lot more than the market in this correction.	B-cause	4258..4464
His firm favors selected computer, drug and pollution-control stocks.	I-cause	4465..4534
Other investment pros are more pessimistic.	B-restatement	4537..4580
They say investors should sell stocks -- but not necessarily right away.	I-restatement	4581..4653
Many of them stress that the selling can be orderly, gradual, and done when stock prices are rallying.	O	4654..4756
On Thursday, William Fleckenstein, a Seattle money manager, used futures contracts in his personal account to place a bet that the broad market averages would decline.	B-cause	4759..4926
He thinks the underlying inflation rate is around 5% to 6%, far higher than most people suppose.	I-cause	4927..5023
In the pension accounts he manages, Mr. Fleckenstein has raised cash positions and invested in gold and natural gas stocks, partly as an inflation hedge.	B-cause	5026..5179
He thinks government officials are terrified to let a recession start when government, corporate and personal debt levels are so high.	I-cause	5180..5314
So he thinks the government will err on the side of rekindled inflation.	B-cause	5315..5387
As a result, Mr. Fleckenstein says, "I think the ball game's over," and investors are about to face a bear market.	I-cause	5390..5504
David M. Jones, vice president at Aubrey G. Lanston & Co., recommends Treasury securities (of up to five years' maturity).	O	5507..5629
He says the Oct. 6 employment report, showing slower economic growth and a severe weakening in the manufacturing sector, is a warning sign to investors.	O	5630..5782
One strategy for investors who want to stay in but hedge their bets is to buy "put" options, either on the individual stocks they own or on a broad market index.	B-entrel	5785..5946
A put option gives its holder the right (but not the obligation) to sell a stock (or stock index) for a specified price (the strike price) until the option expires.	I-entrel	5947..6111
Whether this insurance is worthwhile depends on the cost of an option.	B-entrel	6114..6184
The cost, or premium, tends to get fat in times of crisis.	B-cause	6185..6243
Thus, buying puts after a big market slide can be an expensive way to hedge against risk.	I-cause	6244..6333
The prices of puts generally didn't soar Friday.	B-instantiation	6336..6384
For example, the premium as a percentage of the stock price for certain puts on Eli Lilly & Co. moved up from 3% at Thursday's close to only 3.3% at Friday's close, even though the shares dropped more than $5.50.	I-instantiation	6385..6597
But put-option prices may zoom when trading resumes today.	O	6598..6656
It's hard to generalize about a reasonable price for puts.	O	6659..6717
But investors should keep in mind, before paying too much, that the average annual return for stock holdings, long-term, is 9% to 10% a year; a return of 15% is considered praiseworthy.	O	6718..6903
Paying, say, 10% for insurance against losses takes a deep bite out of the return.	O	6904..6986
James A. White and Tom Herman contributed to this article.	O	6989..7047
