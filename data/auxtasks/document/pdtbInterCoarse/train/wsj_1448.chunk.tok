This city is girding for gridlock today as hundreds of thousands of commuters avoid travel routes ravaged by last week 's earthquake .	O	9..141
Estimates of damage in the six-county San Francisco Bay area neared $ 5 billion , excluding the cost of repairing the region 's transportation system .	O	144..291
The Bay Bridge , the main artery into San Francisco from the east , will be closed for at least several weeks .	O	294..402
Part of the bridge collapsed in the quake , which registered 6.9 on the Richter scale .	B-entrel	403..488
The bridge normally carries 250,000 commuters a day .	I-entrel	489..541
Also , most of the ramps connecting the city to its main link to the south , the 101 freeway , have been closed for repairs .	O	542..663
The Bay Area Rapid Transit system , which runs subway trains beneath the bay , is braced for a doubling of its daily regular ridership to 300,000 .	O	666..810
BART has increased service to 24 hours a day in preparation for the onslaught .	O	811..889
Most unusual will be water-borne commuters from the East Bay towns of Oakland and Berkeley .	B-contingency	892..983
For the first time in 32 years , ferry service has been restored between the East Bay and San Francisco .	I-contingency	984..1087
The Red and White Fleet , which operates regular commuter ferry service to and from Marin County , and tourist tours of the bay , is offering East Bay commuters a chance to ride the waves for the price of $ 10 round-trip .	O	1088..1305
That tariff is too stiff for some Financial District wage earners .	O	1308..1374
" I 'll stay with BART , " said one secretary , swallowing her fears about using the transbay tube .	O	1375..1469
Officials expect the Golden Gate Bridge to be swamped with an extra load of commuters , including East Bay residents making a long detour .	O	1472..1609
" We 're anticipating quite a traffic crunch , " said one official .	B-entrel	1610..1673
About 23,000 people typically travel over the Golden Gate Bridge during commute hours .	B-expansion	1674..1760
About 130,000 vehicles cross during a 24-hour period .	I-expansion	1761..1814
Meetings canceled by Apple Computer Inc. 's European sales force and by other groups raised the specter of empty hotel rooms and restaurants .	B-expansion	1817..1958
It also raised hackles of the city 's tourism boosters .	I-expansion	1959..2013
" Other cities are calling { groups booked here for tours and conferences } and -- not to be crass -- stealing our booking list , " said Scott Shafer , a spokesman for Mayor Art Agnos .	O	2014..2192
City officials stuck by their estimate of $ 2 billion in damage to the quake-shocked city .	B-expansion	2195..2284
The other five Bay area counties have increased their total damage estimates to $ 2.8 billion .	I-expansion	2285..2378
All estimates exclude highway repair , which could exceed $ 1 billion .	O	2379..2447
Among the expensive unknowns are stretches of elevated freeway in San Francisco that were closed because of quake-inflicted damage .	O	2450..2581
The most worrisome stretch is 1.2 miles of waterfront highway known as the Embarcadero Freeway .	B-temporal	2584..2679
Until it was closed Tuesday , it had provided the quickest series of exits for commuters from the Bay Bridge heading into the Financial District .	B-entrel	2680..2824
Engineers say it will take at least eight months to repair the Embarcadero structure .	I-entrel	2825..2910
As part of the quake recovery effort , the city Building Department has surveyed about 3,000 buildings , including all of the Financial District 's high-rises .	O	2913..3069
The preliminary conclusion from a survey of 200 downtown high-rises is that " we were incredibly lucky , " said Lawrence Kornfield , San Francisco 's chief building inspector .	O	3072..3242
While many of these buildings sustained heavy damage , little of that involved major structural damage .	B-contingency	3243..3345
City building codes require construction that can resist temblors .	I-contingency	3346..3412
In England , Martin Leach , a spokesman for Lloyd 's of London , said the insurance market has n't yet been able to estimate the total potential claims from the disaster .	O	3415..3580
" The extent of the claims wo n't be known for some time , " Mr. Leach said .	O	3581..3653
On Friday , during a visit to California to survey quake damage , President Bush promised to " meet the federal government 's obligation " to assist relief efforts .	O	3656..3815
California officials plan to ask Congress for $ 3 billion or more of federal aid , in the form of grants and low-interest loans .	B-expansion	3818..3944
The state has a $ 1 billion reserve , and is expected to add $ 1 billion to that fund in the next year .	I-expansion	3945..4045
Some of that money will be available for highway repair and special emergency aid , but members of the legislature are also mulling over a temporary state gasoline tax to raise money for earthquake relief .	O	4046..4250
However , state initiatives restrict the ability of the legislature to raise such taxes unless the voters approve in a statewide referendum .	O	4251..4390
G. Christian Hill and Ken Wells contributed to this article .	O	4393..4453
