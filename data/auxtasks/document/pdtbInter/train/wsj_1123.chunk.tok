Blue-chip advertisers have plenty of complaints about the magazines they advertise in , ranging from inadequate consumer research to ad " clutter " and a seemingly unchecked proliferation of special interest magazines .	O	9..224
Criticism from such big advertisers as Estee Lauder Inc. , Colgate-Palmolive Co. and Seagram Co. put a damper on the euphoria at the American Magazine Conference here .	B-contrast	227..393
The conference opened Monday with glowing reports about consumer magazines ' growth in circulation and advertising revenue in the past year .	I-contrast	394..533
" Magazines are not providing us in-depth information on circulation , " said Edgar Bronfman Jr. , president and chief operating officer of Seagram , in a panel discussion .	O	536..703
" How do readers feel about the magazine ?	O	704..744
How deeply do they read it ?	B-conjunction	745..772
Research does n't tell us whether people actually do read the magazines they subscribe to . "	I-conjunction	773..863
Reuben Mark , chief executive of Colgate-Palmolive , said advertisers lack detailed demographic and geographic breakdowns of magazines ' audiences .	O	866..1010
" We need research that convinces us that magazines are a real value in reader 's lives , that readers are really involved . "	O	1011..1132
The critics also lambasted the magazine industry for something executives often are very proud of : the growth in magazine titles during the 1980s .	O	1135..1281
Leonard Lauder , president and chief executive officer of Estee Lauder , said consumer magazines are suffering from what he called " niche-itis , " the increasing number of magazines that target the idosyncratic interests of readers .	O	1282..1509
" Niche-itis fragments our advertising dollars , " said Mr. Lauder .	O	1512..1576
" We are being over-magazined .	B-cause	1577..1606
We are constantly faced with deciding which partnerships { with magazines } we can keep . "	I-cause	1607..1694
He added : " There 's probably even a magazine for left-handed golfers ... but the general interest magazine is something we all miss , and it should come back . "	O	1695..1852
Mr. Lauder also attacked what he sees as the wide imitation of Elle , a fashion magazine published by Diamandis Communications Inc. , and criticized the practice of stacking ads at the front of magazines .	O	1855..2057
" Readers do n't want to face all those ad pages at the front of a magazine , " he said .	O	2058..2142
Magazine editors did not take the criticisms lying down .	O	2145..2201
" We spend a fortune on research information , " said Steve Burzon , publisher of Meredith Corp. 's Metropolitan Home .	B-list	2202..2316
And Tina Brown , editor of Conde Nast Publications Inc. 's Vanity Fair , said advertisers are frequently asked to take advertising positions in the back of her magazine to relieve ad clutter .	I-list	2319..2508
" But advertisers would n't think of it , " she said .	O	2509..2558
Bernard Leser , president of Conde Nast , added : " Our research shows we sell more of our heavier issues ... because readers believe they are getting more for what they pay for .	O	2561..2735
