For Cathay Pacific Airways , the smooth ride may be ending .	ROOT	0
The first signs of trouble came last month when the , Hong Kong carriera subsidiary of Swire Pacific Ltd. , posted a 5 % drop in operating profit for the first six months and warned that margins will remain under pressure for the rest of the year .	COREF_PREV	1
Securities analysts , many of whom scrapped their buy recommendations after seeing Cathay 's interim figures , believe more jolts lie ahead .	COREF_PREV	2
Fuel and personnel costs are rising , and tourism in and through Hong Kong remains clouded by China 's turmoil since the June 4 killings in Beijing .	COREF_OTHER	3
In addition , delivery delays for the first two of as many as 28 Boeing 747 - 400s that the carrier has ordered have raised costs because personnel had been hired to man the planes .	COREF_OTHER	4
And tough competition in the air - freight market is cutting into an important sideline .	NONE	5
There also is concern that once Hong Kong reverts to China 's sovereignty in 1997 , Cathay will be forced to play second fiddle to China 's often - disparaged flag carrier , Civil Aviation Administration of China , or CAAC .	COREF_OTHER	6
`` The sense is we would never be in a position again where everything works for us the way it did before , '' says Rod Eddington , Cathay 's commercial director .	COREF_PREV	7
Sarah Hall , an analyst at James Capel ( Far East ) Ltd. , says there is n't much Cathay can do about rising costs for jet fuel , Hong Kong 's tight labor market , or the strengthening of the local currency , which is pegged to the U.S. dollar .	COREF_PREV	8
These factors are further complicated by the airline 's push to transform itself from a regional carrier to an international one , Ms. Hall says .	COREF_PREV	9
Ms. Hall expects Cathay 's profit to grow around 13 % annually this year and next .	COREF_PREV	10
In 1988 , it earned $ 2.82 billion Hong Kong ( US$ 361.5 million ) on revenue of HK$ 11.79 billion .	COREF_PREV	11
Cathay is taking several steps to bolster business .	COREF_PREV	12
One step is to beef up its fleet .	NONE	13
In addition to aircraft from Boeing Co. , Cathay announced earlier this year an order for as many as 20 Airbus A330 - 300s .	COREF_OTHER	14
The expansion , which could cost as much as US$ 5.7 billion over the next eight years , will expand the fleet to about 43 planes by 1991 , up from 30 at the end of last year , according to Sun Hung Kai Securities Ltd .	COREF_PREV	15
The fuel - efficient Airbus planes will be used largely to replace Cathay 's aging fleet of Lockheed Tristars for regional flights , while the Boeing aircraft will be used on long - haul routes to Europe and North America .	COREF_OTHER	16
Cathay also is moving some of its labor - intensive data - processing operations outside Hong Kong .	COREF_PREV	17
Fierce bidding for young employees in Hong Kong is pushing up Cathay 's labor costs by 20 % a year for low - level staff , while experienced , skilled employees are leaving the colony as part of the brain drain .	COREF_PREV	18
Some jobs already have been moved to Australia , and there are plans to place others in Canada .	NONE	19
David Bell , a spokesman for ,the airline says the move is partly aimed at retaining existing staff who are leaving to secure foreign passports ahead of 1997 .	COREF_OTHER	20
Cathay is working to promote Hong Kong as a destination worth visiting on its own merits , rather than just a stopover .	COREF_PREV	21
Although the June 4 killings in Beijing have hurt its China flights , Cathay 's other routes have retained high load factors .	COREF_PREV	22
Mr. Eddington regards promoting Hong Kong as an important part of attracting visitors from Japan , South Korea and Taiwan , where the number of people looking to travel abroad has surged .	COREF_OTHER	23
There also has been speculation that Cathay will be among the major private - sector participants in the Hong Kong government 's plans to build a new airport , with the carrier possibly investing in its own terminal .	COREF_PREV	24
Cathay officials decline to comment on the speculation .	COREF_PREV	25
Mr. Eddington sees alliances with other carriers -- particularly Cathay 's recent link with AMR Corp. 's American Airlines -- as an important part of Cathay 's strategy .	COREF_PREV	26
But he emphasizes that Cathay has n't any interest in swapping equity stakes with the U.S. carrier or with Lufthansa , the West German airline with which it has cooperated for about a decade .	COREF_PREV	27
Analysts believe Cathay is approached for such swaps by other carriers on a regular basis , particularly as the popularity of share exchanges has grown among European carriers .	COREF_PREV	28
`` We think alliances are very important , '' Mr. Eddington says .	COREF_PREV	29
`` But we 'd rather put funds into our own business rather than someone else 's .	COREF_PREV	30
I 'm not sure cross-ownership would necessarily make things smoother . ''	COREF_PREV	31
In a pattern it aims to copy in several key U.S. destinations , Cathay recently announced plans to serve San Francisco by flying into American Airlines ' Los Angeles hub and routing continuing passengers onto a flight on the U.S. carrier .	COREF_OTHER	32
`` We 'll never have a big operation in the U.S. , and they 'll never have one as big as us in the Pacific , '' Mr. Eddington says .	COREF_PREV	33
`` But this way , American will coordinate good extensions to Boston , New York , Chicago and Dallas .	COREF_PREV	34
We 'll coordinate on this end to places like Bangkok , Singapore and Manila . ''	COREF_OTHER	35
Asian traffic , which currently accounts for 65 % of Cathay 's business , is expected to continue as the carrier 's mainstay .	COREF_PREV	36
Cathay has long stated its desire to double its weekly flights into China to 14 , and it is applying to restart long - canceled flights into Vietnam .	COREF_PREV	37
Further expansion into southern Europe is also possible , says Mr. Bell , the spokesman .	NONE	38
While a large number of Hong Kong companies have reincorporated offshore ahead of 1997 , such a move is n't an option for Cathay because it would jeopardize its landing rights in Hong Kong .	COREF_OTHER	39
And Mr. Eddington emphatically rules out a move to London : `` Our lifeblood is Hong Kong traffic rights . ''	COREF_PREV	40
He says the airline is putting its faith in the Sino - British agreement on Hong Kong 's return to China .	COREF_PREV	41
A special section dealing with aviation rights states that landing rights for Hong Kong 's airlines , which include the smaller Hong Kong Dragon Airlines , will continue to be negotiated by Hong Kong 's government .	COREF_PREV	42
But critics fret that post-1997 officials ultimately will be responsible to Beijing .	COREF_PREV	43
`` My feeling is -LCB- Cathay does n't -RCB- have a hope in the long run , '' says an analyst , who declines to be identified .	COREF_OTHER	44
`` Cathay would love to keep going , but the general sense is they 're going to have to do something . ''	COREF_PREV	45
Mr. Eddington acknowledges that the carrier will have to evolve and adapt to local changes , but he feels that the Sino - British agreement is firm ground to build on for the foreseeable future .	COREF_PREV	46
`` We 're confident that it protects our route structure , '' he says , `` and our ability to grow and prosper .	COREF_PREV	47
