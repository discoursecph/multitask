Heidi Ehman might have stepped from a recruiting poster for young Republicans .	B-cause	9..87
White , 24 years old , a singer in her church choir , she symbolizes a generation that gave its heart and its vote to Ronald Reagan .	I-cause	88..217
" I felt kind of safe , " she says .	O	218..250
No longer .	B-cause	253..263
When the Supreme Court opened the door this year to new restrictions on abortion , Ms. Ehman opened her mind to Democratic politics .	B-cause	264..395
Then a political novice , she stepped into a whirl of " pro-choice " marches , house parties and fund-raisers .	B-entrel	396..502
Now she leads a grassroots abortion-rights campaign in Passaic County for pro-choice Democratic gubernatorial candidate James Florio .	I-entrel	503..636
" This is one where I cross party lines , " she says , rejecting the anti-abortion stance of Rep. Florio 's opponent , Reagan-Republican Rep. James Courter .	O	639..789
" People my age thought it was n't going to be an issue .	B-contrast	790..844
Now it has -- especially for people my age . "	I-contrast	845..889
Polls bear out this warning , but after a decade of increased Republican influence here , the new politics of abortion have contributed to a world turned upside down for Mr. Courter .	O	892..1072
Unless he closes the gap , Republicans risk losing not only the governorship but also the assembly next month .	B-conjunction	1073..1182
Going into the 1990s , the GOP is paying a price for the same conservative social agenda that it used to torment Democrats in the past .	I-conjunction	1183..1317
This change comes less from a shift in public opinion , which has n't changed much on abortion over the past decade , than in the boundaries of the debate .	O	1320..1472
New Jersey 's own highest court remains a liberal bulwark against major restrictions on abortion , but the U.S. Supreme Court ruling , Webster vs. Missouri , has engaged voters across the nation who had been insulated from the issue .	O	1473..1702
Before July , pro-choice voters could safely make political decisions without focusing narrowly on abortion .	B-contrast	1705..1812
Now , the threat of further restrictions adds a new dimension , bringing an upsurge in political activity by abortion-rights forces .	B-instantiation	1813..1943
A recent pro-choice rally in Trenton drew thousands , and in a major reversal , Congress is defying a presidential veto and demanding that Medicaid abortions be permitted in cases of rape and incest .	I-instantiation	1944..2141
" If Webster had n't happened , you would n't be here , " Linda Bowker tells a reporter in the Trenton office of the National Organization for Women .	O	2144..2287
" We could have shouted from the rooftops about Courter ... and no one would have heard us . "	O	2288..2379
New Jersey is a proving ground for this aggressive women's-rights movement this year .	B-contrast	2382..2467
The infusion of activists can bring a clash of cultures .	I-contrast	2468..2524
In Cherry Hill , the National Abortion Rights Action League , whose goal is to sign up 50,000 pro-choice voters , targets a union breakfast to build labor support for its cause .	O	2525..2699
The league organizers seem more a fit with a convention next door of young aerobics instructors in leotards than the beefy union leaders ; " I wish I could go work out , " says a slim activist .	O	2700..2889
A labor chief speaks sardonically of having to " man and woman " Election Day phones .	O	2890..2973
No age group is more sensitive than younger voters , like Ms. Ehman .	O	2976..3043
A year ago this fall , New Jersey voters under 30 favored George Bush by 56 % to 39 % over Michael Dukakis , according to a survey then by Rutgers University 's Eagleton Institute .	O	3044..3219
A matching Eagleton-Newark Star Ledger poll last month showed a complete reversal .	B-conjunction	3220..3302
Voters in the same age group backed Democrat Florio 55 % to 29 % over Republican Courter .	I-conjunction	3303..3390
Abortion alone ca n't explain this shift , but New Jersey is a model of how so personal an issue can become a baseline of sorts in judging a candidate .	O	3393..3542
By a 2-to-1 ratio , voters appear more at ease with Mr. Florio 's stance on abortion , and polls indicate his lead widens when the candidates are specifically linked to the issue .	O	3543..3719
" The times are my times , " says Mr. Florio .	B-entrel	3722..3764
The Camden County congressman still carries himself with a trademark " I'm-coming-down-your-throat " intensity , but at a pause in Newark 's Columbus Day parade recently , he was dancing with his wife in the middle of the avenue in the city 's old Italian-American ward .	B-entrel	3765..4029
After losing by fewer than 1,800 votes in the 1981 governor 's race , he has prepared himself methodically for this moment , including deciding in recent years he could no longer support curbs on federal funding for Medicaid abortions .	I-entrel	4030..4262
" If you 're going to be consistent and say it is a constitutionally protected right , " he asks , " how are you going to say an upscale woman who can drive to the hospital or clinic in a nice car has a constitutional right and someone who is not in great shape financially does not ? "	O	4265..4543
Mr. Courter , by comparison , seems a shadow of the confident hawk who defended Oliver North before national cameras at Iran-Contra hearings two years ago .	B-entrel	4546..4699
Looking back , he says he erred by stating his " personal " opposition to abortion instead of assuring voters that he would n't impose his views on " policy " as governor .	I-entrel	4700..4865
It is a distinction that satisfies neither side in the debate .	O	4866..4928
" He does n't know himself , " Kathy Stanwick of the Abortion Rights League says of Mr. Courter 's position .	O	4931..5034
Even abortion opponents , however angry with Mr. Florio , ca n't hide their frustration with the Republican 's ambivalence .	O	5035..5154
" He does n't want to lead the people , " says Richard Traynor , president of New Jersey Right to Life .	O	5155..5253
Moreover , by stepping outside the state 's pro-choice tradition , Mr. Courter aggravates fears that he is too conservative as well on more pressing concerns such as auto insurance rates and the environment .	O	5256..5460
He hurt himself further this summer by bringing homosexual issues into the debate ; and by wavering on this issue and abortion , he has weakened his credibility in what is already a mean-spirited campaign on both sides .	O	5461..5678
Elected to Congress in 1978 , the 48-year-old Mr. Courter is part of a generation of young conservatives who were once very much in the lead of the rightward shift under Mr. Reagan .	O	5681..5861
Like many of his colleagues , he did n't serve in Vietnam in the 1960s yet embraced a hawkish defense and foreign policy -- even voting against a 1984 resolution critical of the U.S. mining of Nicaraguan harbors .	O	5862..6072
Jack Kemp and the writers Irving Kristol and George Gilder were influences , and Mr. Courter 's own conservative credentials proved useful to the current New Jersey GOP governor , Thomas Kean , in the 1981 Republican primary here .	O	6075..6301
The same partnership is now crucial to Mr. Courter 's fortunes , but the abortion issue is only a reminder of the gap between his record and that of the more moderate , pro-choice Gov. Kean .	O	6304..6491
While the Warren County congressman pursued an anti-government , anti-tax agenda in Washington , Gov. Kean was approving increased income and sales taxes at home and overseeing a near doubling in the size of New Jersey 's budget in his eight years in office .	O	6492..6747
Kean forces play down any differences with Mr. Courter , but this history makes it harder for the conservative to run against government .	O	6750..6886
Mr. Courter 's free-market plan to bring down auto insurance rates met criticism from Gov. Kean 's own insurance commissioner .	O	6887..7011
Mr. Courter is further hobbled by a record of votes opposed to government regulation on behalf of consumers .	O	7012..7120
Fluent in Spanish from his days in the Peace Corps , Mr. Courter actively courts minority voters but seems oddly over his head .	O	7123..7249
He is warm and polished before a Puerto Rican Congress in Asbury Park .	B-contrast	7250..7320
Yet minutes after promising to appoint Hispanics to high posts in state government , he is unable to say whether he has ever employed any in his congressional office .	I-contrast	7321..7486
" I do n't think we do now , " he says .	O	7487..7522
" I think we did . "	O	7523..7540
Asked the same question after his appearance , Democrat Florio identifies a staff member by name and explains her whereabouts today .	B-conjunction	7543..7674
When he is presented with a poster celebrating the organization 's 20th anniversary , he recognizes a photograph of one of the founders and recalls time spent together in Camden .	I-conjunction	7675..7851
Details and Camden are essential Florio .	B-entrel	7854..7894
Elected to Congress as a " Watergate baby " in 1974 , he ran for governor three years later .	I-entrel	7895..7984
In the opinion of many , he has n't stopped running since , even though he declined a rematch with Gov. Kean in 1985 .	O	7985..8099
His base in South Jersey and on the House Energy and Commerce Committee helped him sustain a network of political-action committees to preserve his edge .	B-conjunction	8100..8253
With limited budgets for television in a high-priced market , Mr. Florio 's higher recognition than his rival is a major advantage .	I-conjunction	8254..8383
More than ever , his pro-consumer and pro-environment record is in sync with the state .	B-cause	8386..8472
Auto insurance rates are soaring .	I-cause	8473..8506
A toxic-waste-dump fire destroyed part of an interstate highway this summer .	O	8507..8583
In Monmouth , an important swing area , Republican freeholders now run on a slogan promising to keep the county " clean and green . "	O	8584..8712
Mr. Florio savors this vindication , but at age 52 , the congressman is also a product of his times and losses .	O	8715..8824
He speaks for the death penalty as if reading from Exodus 21 ; to increase state revenue he focuses not on " taxes " but on " audits " to cut waste .	O	8825..8968
Hard-hitting consultants match ads with Mr. Courter 's team , and Mr. Florio retools himself as the lean , mean Democratic fighting machine of the 1990s .	O	8969..9119
Appealing to a young audience , he scraps an old reference to Ozzie and Harriet and instead quotes the Grateful Dead .	O	9120..9236
The lyric chosen -- " long strange night " -- may be an apt footnote to television spots by both candidates intended to portray each other as a liar .	O	9237..9384
The Democratic lawmaker fits a pattern of younger reformers arising out of old machines , but his ties to Camden remain a sore point because of the county 's past corruption .	O	9387..9559
His campaign hierarchy is chosen from elsewhere in the state , and faced with criticism of a sweetheart bank investment , he has so far blunted the issue by donating the bulk of his profits to his alma mater , Trenton State College .	O	9560..9789
Mr. Florio 's forcefulness on the abortion issue after the Webster ruling divides some of his old constituency .	B-instantiation	9792..9902
Pasquale Pignatelli , an unlikely but enthusiastic pipe major in an Essex County Irish bagpipe band , speaks sadly of Mr. Florio .	I-instantiation	9903..10030
" I am a devout Catholic , " says Mr. Pignatelli , a 40-year-old health officer .	O	10031..10107
" I ca n't support him because of abortion . "	O	10108..10150
Bill Wames Sr. , 72 , is Catholic too , but unfazed by Mr. Florio 's stand on abortion .	O	10153..10236
A security guard at a cargo terminal , he wears a Sons of Italy jacket and cap celebrating " The US 1 Band . "	O	10237..10343
" I still think the woman has the right to do with her body as she pleases , " he says .	O	10344..10428
" If you want more opinions ask my wife .	B-cause	10429..10468
She has lots of opinions .	I-cause	10469..10494
