The scene opens with pinstripe-suited executives -- Easterners , obviously -- glued to cellular phones and hightailing it out of town in chauffeur-driven limousines .	root	9..173
" The carpetbaggers , " snorts the narrator with a Texas twang , " have packed their bags and went . "	norel	176..271
But , he continues , " They 're forgetting we 're all Texans .	pragmatic contrast	272..328
The Lone Star is on the rise again . "	conjunction	329..365
As the music swells , viewers discover they 're watching a commercial for Lone Star Beer , the pride of Texas , a product of G. Heileman Brewing Co. , a La Crosse , Wis. , unit of Bond Corp .	norel	368..551
As the ad 's tone implies , the Texas spirit is pretty xenophobic these days	norel	554..628
and Lone Star is n't alone in trying to take advantage of that .	conjunction	629..692
From Chevy trucks to Lipton iced tea to a host of battling banks , the state has been inundated with broadcast commercials and print advertising campaigns celebrating Texans and castigating outsiders .	restatement	693..892
While advertisers have long appealed to Texans ' state pride and prejudices , the latest trend has been sparked , in part , by the state 's recent hard economic times .	norel	895..1057
That has taken some of the swagger out of natives who like to brag that Texas is the only state that was once a nation	cause	1058..1176
but it has increased their legendary resentment of outsiders .	contrast	1177..1239
In the past , writes Houston Chronicle columnist Jim Barlow , outlanders were accepted only after passing a series of tests to prove they had the " right " Texas attitudes and " of course they had to be dipped for parasites . "	asynchronous	1240..1460
There is no small irony in the fact that some of the most-jingoistic advertising comes courtesy of -- you guessed it -- outsiders .	norel	1463..1593
Lone Star 's Bond Corp. parent , for instance , hails from Perth , Australia .	instantiation	1594..1667
North Carolinians , New Yorkers , Californians , Chicagoans and Ohioans own Texas banks .	list	1668..1753
All kinds of landmark Texas real estate has been snapped up by out-of-staters .	list	1754..1832
Even the beloved Dallas Cowboys were bought by an Arkansas oil man .	list	1833..1900
" Texas has lost its distinctiveness , leaving Texans with a hunger to feel proud about themselves , " says Stephen Klineberg , a sociology professor at Rice University , Houston .	norel	1903..2076
" This plays right into the hands of the advertising agencies . "	conjunction	2077..2139
For example , the iced-tea radio campaign for Thomas J. Lipton Co. , an Englewood Cliffs , N.J. , unit of Anglo-Dutch Unilever Group , emphatically proclaims : " Real Texans do not wear dock-siders -- ever .	instantiation	2142..2341
Real Texans do n't play paddleball , at least I hope not .	list	2342..2397
This is football country .	cause	2398..2423
And another thing -- real Texans drink Lipton iced tea . "	list	2424..2480
In developing that theme at Interpublic Group of Cos . ' Lintas : New York unit , account supervisor Lisa Buksbaum says she made a " couple of phone calls " to Dallas ad friends and reported her " findings " to a team of writers .	norel	2483..2705
Her findings ?	entrel	2706..2719
" You know , " she says , " stereotypical stuff like armadillos , cowboys and football . "	norel	2720..2802
Not exactly sophisticated market research	norel	2805..2846
but who cares as long as the campaigns work .	contrast	2847..2892
And ad agencies insist that they do .	conjunction	2893..2929
Stan Richards of Richards Group Inc. , Dallas , tells of the Texan who saw the agency 's tear-jerking commercial for First Gibraltar Bank F.S.B. -- complete with the state 's anthem -- and promptly invested $ 100,000 in the thrift 's CDs .	instantiation	2930..3162
Never mind that First Gibraltar is one of the failed Texas thrifts taken over by outsiders -- in this case , an investor group headed by New York financier Ronald Perelman .	concession	3163..3334
The North Texas Chevy Dealers recently had a record sales month	norel	3337..3400
after the debut of ad campaign that thumbs its nose at elite Easterners .	asynchronous	3401..3473
And deposits at NCNB Texas National Bank , a unit of NCNB Corp. , Charlotte , N.C. , have increased $ 2 billion since last year	list	3474..3596
after heavy advertising stressing commitment to Texas .	asynchronous	3597..3651
" Obviously , pride sells in Texas , " says a spokeswoman for Bozell Inc. , Omaha , Neb. , which represents	cause	3652..3752
The ad campaigns usually follow one of three tracks -- stressing the company 's ' Texasness , ' pointing out the competition 's lack thereof , or trying to be more Texan than Texans .	norel	3755..3932
Ford trucks may outsell Chevy trucks in places like " Connecticut and Long Island , " sniffs a commercial for Chevrolet , a division of General Motors Corp .	instantiation	3933..4085
The commercial , created by Bateman , Bryan & Galles Inc. , of Dallas , adds derisively : " I bet it takes a real tough truck to haul your Ivy League buddies to the yacht club . "	asynchronous	4086..4257
Because they want a truck that is " Texas tough , " the commercial concludes , " Texans drive Chevy . "	contrast	4258..4354
J.C. Penney Co. , which relocated from New York to suburban Dallas two years ago , gently wraps itself in Texas pride through a full-page magazine ad : " Taking the long-range view to conserve what is of value to future generations is part of the Lone Star lifestyle , " the ad reads .	norel	4357..4635
" It 's part of our style , too . "	entrel	4636..4666
According to several ad-agency sources , newcomers to the Texas banking market are spending a combined $ 50 million this year to woo Texans .	norel	4669..4807
Meanwhile , surviving Texas banking institutions are busily pitching themselves as the only lenders who truly care about the state .	synchrony	4808..4938
The most-strident anti-outsider sentiment among bankers comes from the Independent Bankers Association of Texas	norel	4941..5052
although it 's hard to tell from previews of the $ 5 million " The I 's of Texas " TV campaign .	concession	5053..5144
Commercials will highlight heart-rending scenes of Texas and chest-swelling , ain't-it-great-to-be-a-Texan music .	restatement	5145..5257
Supporting banks will sign a " Texas Declaration of Independents . "	list	5258..5323
But in introductory material for the campaign , the trade group urges members to " arm " for a " revolution " against big , out-of-state bank-holding companies .	norel	5326..5480
A video sent to association members , featuring shots of the Alamo , cowboys , fajitas and a statue of Sam Houston , does n't mince words .	instantiation	5481..5614
" Texans can sniff a phony a mile away , " the narrator warns outsiders .	restatement	5615..5684
" So , do n't come and try to con us with a howdy y'all or a cowboy hat . "	cause	5685..5755
Young & Rubicam 's Pact	norel	5758..5780
Young & Rubicam , fighting charges that it bribed Jamaican officials to win the Jamaica Tourist Board ad account in 1981 , said it will no longer create the tourist board 's advertising .	norel	5783..5966
In a statement , Alex Kroll , Young & Rubicam 's chairman , said " under the present circumstances { we } have agreed that it is prudent to discontinue that contract . "	norel	5969..6129
Young & Rubicam has pleaded innocent to the charges .	entrel	6130..6182
The board would n't comment on its impending search for a new ad agency to handle its estimated $ 5 million to $ 6 million account .	entrel	6183..6311
Ad Notes ... .	norel	6314..6327
NEW ACCOUNT :	norel	6330..6342
Sunshine Biscuits Inc. , Woodbridge , N.J. , awarded its estimated $ 5 million account to Waring & LaRosa , New York .	norel	6343..6455
The account had been at Della Femina McNamee WCRS , New York .	asynchronous	6456..6516
MEDIA POLICY :	norel	6519..6532
MacNamara Clapp & Klein , a small New York shop , is asking magazine ad representatives to tell it when major advertising inserts will run in their publications .	norel	6533..6692
It says it may pull its clients ' ads from those magazines .	cause	6693..6751
COKE ADS :	norel	6754..6763
Coca-Cola Co. said it produced a new version of its 1971 " I 'd like to teach the world to sing " commercial .	norel	6764..6870
The ad is part of Classic Coke 's 1990 ad campaign , with the tag line , " Ca n't beat the Real Thing . "	entrel	6871..6969
Basketball star Michael Jordan and singer Randy Travis have also agreed to appear in ads .	conjunction	6970..7059
