You'd think all the stories about well-heeled communities and developers getting HUD grants would prompt Congress to tighten up on upscale housing subsidies.	B-concession	9..166
No way.	B-contrast	167..174
Congress has just made it easier for the affluent to qualify for insured loans from the deficit-ridden Federal Housing Administration.	B-restatement	175..309
It appears that the only thing Congress is learning from the HUD story is how to enlarge its control of the honey pot going to special interests.	I-restatement	310..455
Right now, the largest loan the FHA can insure in high-cost housing markets is $101,250.	B-contrast	458..546
Last week, housing lobbies persuaded Congress to raise the ceiling to $124,875, making FHA loans more accessible to the well-to-do.	B-contrast	547..678
But it does that at the cost of deepening the taxpayer's exposure if the FHA is forced to pay for more loans going sour.	B-conjunction	679..799
This is no idle fearlast year the FHA lost $4.2 billion in loan defaults.	I-conjunction	800..873
But the higher mortgage ceiling is only the starter kit for what Senator Alan Cranston and Majority Leader George Mitchell have in mind for housing.	B-instantiation	876..1024
The Senate Banking Committee will begin hearings next week on their proposal to expand existing federal housing programs.	B-conjunction	1025..1146
Other Senators want to lower the down payments required on FHA-insured loans.	I-conjunction	1147..1224
That would be a formula for ensuring even more FHA red ink.	B-cause	1227..1286
Experience has shown that the most important element in predicting a housing-loan default is the down payment.	B-entrel	1287..1397
Because a purchaser can use an FHA loan to finance all points and closing costs, the FHA can wind up lending more than a house is worth.	B-cause	1398..1534
If housing prices continue to fall, many borrowers would be better off walking away from their homes and leaving taxpayers with the losses.	B-conjunction	1535..1674
Much the same thing happened with busted S&Ls, a problem Congress just "solved" with a $166 billion bailout.	I-conjunction	1675..1783
We hear that HUD Secretary Jack Kemp is toying with going along with some of the Cranston-Mitchell proposals.	O	1786..1895
That sounds like a formula for ensuring that he gets dragged into the next HUD tar pit.	B-conjunction	1896..1983
A group of 27 Senators has written Mr. Kemp urging him to reject Cranston-Mitchell and focus on programs that empower the poor rather than create vast new government obligations.	I-conjunction	1984..2162
But even if he agrees, Mr. Kemp doesn't write the nation's housing law -- Congress does.	O	2163..2251
And the majority of Members cynically view the current discrediting of HUD as mainly a chance to shove through their own slate of projects.	O	2252..2391
Exhibit A is last week's House vote to fund 40 pet projects out of the same discretionary fund that is at the heart of the HUD scandal.	B-cause	2394..2529
None of the grants had been requested by HUD, judged competitively or were the subject of a single hearing.	I-cause	2530..2637
More and more observers now realize that the key to ending future HUD scandals lies in forcing Congress to clean up its own act.	B-instantiation	2640..2768
This week, a Baltimore Sun editorial said the Lantos subcommittee on HUD should forget about Sam Pierce's testimony for the moment and call some other witnesses: the various congressional sponsors of the 40 pork-barrel projects.	B-entrel	2769..2997
The Sun concluded that Mr. Pierce is only part of the problem -- and a part that's gone.	I-entrel	2998..3086
"If HUD is to be reformed," it concluded, Members of Congress will "have to start looking into, and doing something about, the practices of their colleagues."	O	3087..3245
Of course, self-reform is about the last thing this Congress is interested in.	B-contrast	3248..3326
Proponents of expanding FHA programs say they merely want to help home buyers who are frozen out of high-priced markets.	B-contrast	3327..3447
Proponents of expanding FHA programs say they merely want to help home buyers who are frozen out of high-priced markets.But the FHA program is hemorrhaging bad loans.	B-conjunction	3448..3494
Jack Kemp has submitted a package of reforms, and they are surely headed for the Capitol Hill sausage-grinder.	I-conjunction	3495..3605
Like the S&L mess before it, this is a problem Congress should be solving, not ignoring.	O	3606..3694
