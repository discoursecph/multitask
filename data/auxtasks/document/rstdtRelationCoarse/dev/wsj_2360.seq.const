CenTrust Savings Bank said	(Temporal(Temporal(Temporal(Summary-Evaluation-Topic(Cause(Attribution
federal thrift regulators ordered it to suspend dividend payments on its two classes of preferred stock ,	(Cause
indicating that regulators ' concerns about the troubled institution have heightened .	Cause))
In a statement , Miami-based CenTrust said	(Summary-Evaluation-Topic(Elaboration(Attribution
the regulators cited the thrift 's operating losses and `` apparent losses '' in its junk-bond portfolio	(Temporal
in ordering the suspension of the dividends .	Temporal))
Regulators also ordered CenTrust to stop buying back the preferred stock .	Elaboration)
David L. Paul , chairman and chief executive officer , criticized the federal Office of Thrift Supervision ,	(Joint(Elaboration
which issued the directive ,	Elaboration)
saying	(Elaboration(Attribution
it was `` inappropriate ''	(Joint
and based on `` insufficient '' reasons .	Joint))
He said	(Attribution
the thrift will try to get regulators to reverse the decision .	Attribution)))))
The suspension of a preferred stock dividend is a serious step	(Elaboration
that signals	(Attribution
that regulators have deep concerns about an institution 's health .	Attribution)))
In March , regulators labeled CenTrust a `` troubled institution , ''	(Elaboration(Cause
largely because of its big junk-bond holdings and its operating losses .	Cause)
In the same month , the Office of Thrift Supervision ordered the institution to stop paying common stock dividends	(Cause
until its operations were on track .	Cause)))
For the nine months	(Temporal(Elaboration(Comparison(Textual-organization(Elaboration
ended June 30 ,	Elaboration)
CenTrust had a net loss of $ 21.3 million ,	Textual-organization)
compared with year-earlier net income of $ 52.8 million .	Comparison)
CenTrust ,	(Textual-organization(Elaboration
which is Florida 's largest thrift ,	Elaboration)
holds one of the largest junk-bond portfolios of any thrift in the nation .	Textual-organization))
Since April , it has pared its high-yield bond holdings to about $ 890 million from $ 1.35 billion .	(Elaboration
Mr. Paul said	(Elaboration(Attribution
only about $ 150 million of the current holdings are tradeable securities	(Elaboration
registered with the Securities and Exchange Commission .	Elaboration))
The remainder ,	(Elaboration(Textual-organization(Attribution
he said ,	Attribution)
are commercial loan participations , or private placements ,	Textual-organization)
that are n't filed with the SEC	(Joint
and do n't have a ready market .	Joint))))))
CenTrust and regulators have been in a dispute over market valuations for the junk bonds .	(Elaboration(Elaboration(Elaboration(Elaboration
The Office of Thrift Supervision has been hounding CenTrust to provide current market values for its holdings ,	(Comparison
but CenTrust has said	(Attribution
it ca n't easily obtain such values	(Cause
because of the relative illiquidity of the bonds and lack of a ready market .	Cause))))
Regulators have become increasingly antsy about CenTrust 's and other thrifts ' junk-bond holdings in light of the recent federal thrift bailout legislation and the recent deep decline in the junk-bond market .	(Elaboration
The legislation requires thrifts to divest themselves of junk bonds in the new , somber regulatory climate .	Elaboration))
In American Stock Exchange composite trading Friday , CenTrust common shares closed at $ 3 , down 12.5 cents .	Elaboration)
In a statement Friday , Mr. Paul challenged the regulators ' decision ,	(Elaboration(Elaboration(Joint
saying	(Attribution
the thrift 's operating losses and `` apparent '' junk-bond losses `` have been substantially offset by gains in other activities of the bank . ''	Attribution))
He also said	(Elaboration(Attribution
substantial reserves have been set aside for possible losses from the junk bonds .	Attribution)
In the third quarter , for instance , CenTrust added $ 22.5 million to its general reserves .	Elaboration))
Mr. Paul said	(Summary-Evaluation-Topic(Attribution
the regulators should instead move ahead with approving CenTrust 's request	(Elaboration
to sell 63 of its 71 branches to Great Western Bank , a unit of Great Western Financial Corp .	(Elaboration
based in Beverly Hills , Calif .	Elaboration)))
The branch sale is the centerpiece of CenTrust 's strategy	(Elaboration(Attribution(Elaboration
to transform itself into a traditional S & L from a high-flying institution	(Elaboration
that relied heavily on securities trading for profits ,	Elaboration))
according to Mr. Paul .	Attribution)
Most analysts and thrift executives had expected a decision on the proposed transaction ,	(Summary-Evaluation-Topic(Textual-organization(Elaboration
which was announced in July ,	Elaboration)
long before now .	Textual-organization)
Many interpret the delay as an indication	(Comparison(Cause(Elaboration
that regulators are skeptical about the proposal .	Elaboration)
Branches and deposits can be sold at a premium in the event	(Elaboration
federal regulators take over an institution .	Elaboration))
CenTrust , however , touts the branch sale ,	(Elaboration(Joint
saying	(Attribution
it would bring in $ 150 million	(Joint
and reduce the thrift 's assets to $ 6.7 billion from $ 9 billion .	Joint)))
It said	(Elaboration(Attribution
the sale would give it positive tangible capital of $ 82 million , or about 1.2 % of assets , from a negative $ 33 million as of Sept. 30 ,	(Cause
thus bringing CenTrust close to regulatory standards .	Cause))
CenTrust said	(Comparison(Attribution
the branch sale would also reduce the company 's large amount of good will by about $ 180 million .	Attribution)
Critics , however , say	(Summary-Evaluation-Topic(Attribution
the branch sale will make CenTrust more dependent than ever on brokered deposits and junk bonds .	Attribution)
Mr. Paul counters	(Elaboration(Attribution
that he intends to further pare the size of CenTrust	(Joint
by not renewing more than $ 1 billion of brokered certificates of deposit	(Temporal
when they come due .	Temporal)))
The thrift is also working to unload its junk-bond portfolio	(Joint(Joint
by continuing to sell off the bonds ,	Joint)
and it plans to eventually place some of them in a separate affiliate ,	(Temporal
as required under the new thrift law .	Temporal))))))))))))))
