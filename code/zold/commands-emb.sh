mkdir -p nohup
mkdir -p models
ITER=20
SEED=3068836234
EMBEDS=embs/en.polyglot.txt
INDIM=64

# with emb
echo "python discourse.py --cnn-seed $SEED --iters $ITER --embeds $EMBEDS --in_dim $INDIM --train ../data/pdtb_chunk_I/origin/TRAIN/ --test ../data/pdtb_chunk_I/origin/TEST --save models/pdtb_chunk_I.embeds.i$ITER.model --output > predictions/pdtb_I.test.embeds.i20.out 2> nohup/pdtb.embeds.out2"
echo "python discourse.py --cnn-seed $SEED --iters $ITER --embeds $EMBEDS --in_dim $INDIM --train ../data/rstdt_chunk_I/origin/TRAIN/ --test ../data/rstdt_chunk_I/origin/TEST --save models/rstdt_chunk_I.embeds.i$ITER.model --output > predictions/rstdt_I.test.embeds.i20.out 2> nohup/rstdt.embeds.out2"
echo "python discourse.py --cnn-seed $SEED --iters $ITER --embeds $EMBEDS --in_dim $INDIM --train ../data/pdtb_chunk_I/origin_testRST/TRAIN/ --test ../data/pdtb_chunk_I/origin_testRST/TEST --save models/pdtb_chunk_I.origin_testRST.embeds.i$ITER.model --output > predictions/pdtb_I.origin_testRST.test.embeds.i20.out 2> nohup/pdtb.origin_testRST.embeds.out2"

#without emb
echo "python discourse.py --cnn-seed $SEED --iters $ITER --in_dim $INDIM --train ../data/pdtb_chunk_I/origin/TRAIN/ --test ../data/pdtb_chunk_I/origin/TEST --save models/pdtb_chunk_I.i$ITER.model --output > predictions/pdtb_I.test.i20.out 2> nohup/pdtb.out2"
echo "python discourse.py --cnn-seed $SEED --iters $ITER --in_dim $INDIM --train ../data/rstdt_chunk_I/origin/TRAIN/ --test ../data/rstdt_chunk_I/origin/TEST --save models/rstdt_chunk_I.i$ITER.model --output > predictions/rstdt_I.test.i20.out 2> nohup/rstdt.out2"
echo "python discourse.py --cnn-seed $SEED --iters $ITER --in_dim $INDIM --train ../data/pdtb_chunk_I/origin_testRST/TRAIN/ --test ../data/pdtb_chunk_I/origin_testRST/TEST --save models/pdtb_chunk_I.origin_testRST.i$ITER.model --output > predictions/pdtb_I.origin_testRST.test.i20.out 2> nohup/pdtb.origin_testRST.out2"

