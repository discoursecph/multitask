The Bakersfield Supermarket went out of business last May .	root	9..67
The reason was not high interest rates or labor costs .	concession	68..122
Nor was there a shortage of customers in the area , the residential Inwood section of northern Manhattan .	alternative	123..227
The business closed	alternative	228..247
when the owner was murdered by robbers .	asynchronous	248..287
The owner was Israel Ortiz , a 29-year-old entrepreneur and father of two .	norel	290..363
In his first year of operating the store he bought for $ 220,000 , Mr. Ortiz was robbed at least twice at gunpoint .	entrel	364..477
The first time he was shot in the hand	restatement	478..516
as he chased the robbers outside .	synchrony	517..550
The second time he identified two robbers , who were arrested and charged .	list	551..624
Two weeks later -- perhaps in retaliation -- Mr. Ortiz was shot three times in the back , during what police classified as a third robbery attempt .	asynchronous	625..771
That was his reward for working until 11 p.m. seven days a week to cover his $ 3,000 a month rent .	norel	774..871
For providing what his customers described as very personal and helpful service .	list	872..952
For creating a focus for neighborhood life .	list	953..996
Israel Ortiz is only one of the thousands of entrepreneurs and their employees who will be injured or killed by crime this year .	norel	999..1127
The U.S. Bureau of Justice Statistics reports that almost 2 % of all retail-sales workers suffer injuries from crime each year , almost twice the national average and about four times the rate for teachers , truck drivers , medical workers and door-to-door salespeople .	restatement	1128..1393
Only a few other occupations have higher reported rates of criminal injury , such as police , bartenders and taxi drivers .	conjunction	1394..1514
Yet these figures show only the most visible part of the problem .	norel	1517..1582
Recent data from New York City provide more of the picture .	contrast	1583..1642
While by no means the highest crime community in the country , New York is a prime example of a city where crime strangles small-business development .	restatement	1643..1792
A survey of small businesses there was conducted this spring by Interface , a policy research organization .	entrel	1793..1899
It gave 1,124 businesses a questionnaire and analyzed 353 responses .	restatement	1900..1968
The survey found that over a three-year period 22 % of the firms said employees or owners had been robbed on their way to or from work or while on the job .	restatement	1969..2123
Seventeen percent reported their customers being robbed .	conjunction	2124..2180
Crime was the reason that 26 % reported difficulty recruiting personnel and that 19 % said they were considering moving .	conjunction	2181..2299
More than one-third of the responding businesses said they suffer from drug dealing and loitering near their premises .	norel	2302..2420
In Brooklyn and the Bronx , one out of four commercial firms is burglarized each year .	conjunction	2421..2506
Industrial neighborhoods fare even worse , with burglary rates twice the citywide average .	conjunction	2507..2596
Crime is clearly more deadly to small-scale entrepreneurship than to big businesses .	norel	2599..2683
Two decades ago , the Small Business Administration reported Yale Prof. Albert Reiss 's landmark study of crime against 2,500 small businesses drawn from national IRS records .	entrel	2684..2857
He found that monetary crime losses , as a proportion of gross receipts , were 37 times higher for small businesses than for large ones .	restatement	2858..2992
The New York study 's companies averaged 27 employees ; their annual crime losses averaged about $ 15,000 , with an additional $ 8,385 annual cost in security -- enough money to hire at least one more worker .	norel	2995..3198
The costs of crime may also be enough to destroy a struggling business .	conjunction	3199..3270
Whatever the monetary crime losses , they may not be nearly as important to entrepreneurs as the risk of personal injury .	norel	3273..3393
After repeated gun robberies , some entrepreneurs may give up a business out of fear for their lives .	restatement	3394..3494
One Washington couple recently sold their liquor store after 34 years in business that included four robbery deaths and 16 robberies or burglaries on the premises .	instantiation	3495..3658
These findings illustrate the vicious cycle that National Institute of Justice Director James K. Stewart calls " crime causing poverty . "	norel	3661..3796
Underclass neighborhoods offer relatively few employment opportunities , contributing to the poverty of local residents .	restatement	3797..3916
Small neighborhood businesses could provide more jobs	comparison	3917..3970
if crime were not so harmful to creating and maintaining those businesses .	condition	3971..4046
This may help explain why small businesses create 65 % of all jobs nationally , but only 22 % of jobs in a crime-ridden city like New York .	cause	4047..4183
Bigger business can often better afford to minimize the cost of crime .	norel	4186..4256
The New York study found that the cost of security measures in firms with fewer than five employees was almost $ 1,000 per worker , compared with one-third that amount for firms with more than 10 employees .	restatement	4257..4461
The shift of retailing to large shopping centers has created even greater economies of scale for providing low-crime business environments .	norel	4464..4603
Private security guards and moonlighting police can invoke the law of trespass to regulate access to these quasi-public places .	cause	4604..4731
Since 1984 , in fact , revenues of the 10 largest guard companies , primarily serving such big businesses , have increased by almost 62 % .	norel	4732..4865
Few small neighborhood businesses , however , can afford such protection , even in collaboration with other local merchants .	norel	4868..4989
In the neighborhoods with the highest crime rates , small business generally relies on the public police force for protection .	cause	4990..5115
This creates several problems .	cause	5116..5146
One is that there are not enough police to satisfy small businesses .	instantiation	5147..5215
The number one proposal for reducing crime in the New York survey was to put more police on foot or scooter patrol , suggested by more than two-thirds of the respondents .	restatement	5216..5385
Only 22 % supported private security patrols funded by the merchants themselves .	contrast	5386..5465
A second problem is the persistent frustration of false alarms , which can make urban police less than enthusiastic about responding to calls from small businesses .	norel	5468..5631
Only half the New York small businesses surveyed , for their part , are satisfied with the police response they receive .	conjunction	5632..5750
Some cities , including New York , have experimented with special tax districts for commercial areas that provide additional patrols funded by local businesses .	norel	5753..5911
But this raises added cost barriers to urban entrepreneurship .	comparison	5912..5974
Another solution cities might consider is giving special priority to police patrols of small-business areas .	norel	5977..6085
For cities losing business to suburban shopping centers , it may be a wise business investment to help keep those jobs and sales taxes within city limits .	cause	6086..6239
Increased patrolling of business zones makes sense	conjunction	6240..6290
because urban crime is heavily concentrated in such " hot spots " of pedestrian density .	cause	6291..6377
With National Institute of Justice support , the Minneapolis police and the Crime Control Institute are currently testing the effects of such a strategy , comparing its deterrence value with traditional random patrols .	expansion	6378..6594
Small-business patrols would be an especially helpful gesture whenever a small-business person is scheduled to testify against a robbery suspect .	norel	6597..6742
While no guarantee , an increased police presence might even deter further attacks .	expansion	6743..6825
It might even have saved the life , and business , of Israel Ortiz .	norel	6828..6893
Mr. Sherman is a professor of criminology at the University of Maryland and president of the Crime Control Institute in Washington , D.C .	norel	6896..7032
