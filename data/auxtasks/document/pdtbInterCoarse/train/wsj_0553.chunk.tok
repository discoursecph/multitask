We 're sorry to see Nigel Lawson 's departure from the British government .	B-contingency	9..81
He is a politician with the courage of true conviction , as in summarily sacking exchange controls and in particular slashing the top rate of income taxation to 40 % .	I-contingency	82..246
But in the end his resignation as Chancellor of the Exchequer may be a good thing , especially if it works as he no doubt intends -- by forcing Prime Minister Thatcher and her counterparts elsewhere to confront the genuine intellectual issues involved .	O	247..498
The early omens , we admit , scarcely suggest so wholesome an outcome .	O	501..569
The Fleet Street reaction was captured in the Guardian headline , " Departure Reveals Thatcher Poison . "	O	570..671
British politicians divide into two groups of chickens , those with their necks cut and those screaming the sky is falling .	B-contingency	672..794
So far as we can see only two persons are behaving with a dignity recognizing the seriousness of the issues : Mr. Lawson and Sir Alan Walters , the counterpoint of the Chancellor 's difficulties , who also resigned as personal adviser to Mrs. Thatcher .	I-contingency	795..1043
The problem is that on the vital issue of monetary policy and exchange rates , conservative , free-market economists divide into at least three incompatible camps .	B-expansion	1046..1207
There are the strict monetarists , who believe that floating exchange rates free an economy to stabilize its price level by stabilizing the monetary aggregates .	B-expansion	1208..1367
There are the supply-side globalists , who seek to spread the advantages of a common currency through fixed exchange rates .	B-expansion	1368..1490
And there are the twin-deficit Keynesians , who predict/advocate devaluations to balance trade flows .	I-expansion	1491..1591
This is a problem not only for Prime Minister Thatcher but for President Bush , as shown in the ongoing bickering over the dollar between the Federal Reserve and the Mulford Treasury .	O	1592..1774
In the British case , Mr. Lawson is the closest thing in London to a supply-side globalist .	B-contingency	1777..1867
He not only slashed marginal tax rates , initially sparking fresh growth in Britain , but he wanted to regulate monetary policy by targeting exchange rates , indeed joining the European Monetary System .	B-comparison	1868..2067
While no doubt agreeing with Mr. Lawson on everything else , Sir Alan is a dyed-in-the-wool monetarist , inclined to defend floating rates to the death .	I-comparison	2068..2218
To make matters even more confusing , the earlier U.S. experience made clear that Mr. Lawson 's tax cuts would have profound effects on Britain 's international accounts and the value of sterling .	O	2221..2414
They increased the after-tax rate of return and made Britain a far more attractive place to invest , producing sudden capital inflows .	B-contingency	2415..2548
By accounting definitions , this had to produce a sudden trade deficit .	B-expansion	2549..2619
As in the U.S. , it also produced a sudden burst in the demand for sterling , that is a surge in the sterling monetary aggregates , M-Whatever .	I-expansion	2620..2760
At this point , the options were : Crunch money to stop the boost in the aggregates , as Sir Alan surely advised , and forget the soaring pound .	B-contingency	2763..2903
To push the pound even lower trying to cure the trade deficit , a policy Britain has repeatedly proved disastrous .	I-contingency	2904..3017
Or to supply enough money to meet the increased demand and stabilize the exchange rate , as the Chancellor argued , and ensure the permanence of this policy by joining the EMS .	O	3018..3192
Faced with a similar situation , Paul Volcker let the dollar soar , ( though monetary aggregates also grew so rapidly monetarists issued egg-on-the-face warnings of inflation ) .	O	3195..3368
But this devastated the U.S. manufacturing sector , laying the seeds of protectionism .	B-contingency	3369..3454
Mr. Lawson , though not allowed to join the EMS , chose to " shadow " the deutsche mark .	B-contingency	3455..3539
He reaped inflation along with rapid growth , no doubt validating Sir Alan 's predictions in the Prime Minister 's mind .	B-comparison	3540..3657
But more recently , the pound has been falling with high inflation , which has also seemed almost impervious to the high interest rates Mr. Lawson deployed to stop it .	B-contingency	3658..3823
So the British experience presents a genuine puzzle that reaches far beyond the shores of Albion .	B-expansion	3826..3923
We had been soliciting opinions on it long before Mr. Lawson 's resignation , and offer some of the collection for the benefit of his successor and one-time deputy , John Major .	B-expansion	3924..4098
To begin with , we should note that in contrast to the U.S. deficit , Britain has been running largish budget surpluses .	B-entrel	4099..4217
In pursuit of this mystery , Keynesian adepts and twin-deficit mavens need not apply .	I-entrel	4218..4302
We should also add Mr. Lawson 's own explanation , as we understand it .	B-expansion	4305..4374
Unlike the U.S. , Britain never achieved even a momentary reduction in real wages .	B-expansion	4375..4456
The wage stickiness , which OECD studies confirm is particularly high in Britain , gives its economy a structural bias toward inflation .	B-expansion	4457..4591
Inflation is easier to spark and harder to control .	I-expansion	4592..4643
We should also concede that in the British experience the monetarist cause regains some of the credibility it lost in the U.S. experience .	O	4646..4784
Nearby Paul Craig Roberts , a distinguished supply-sider with monetarist sympathies , argues the case for Sir Alan .	O	4785..4898
Perhaps the fiscal shock of tax cuts is after all best absorbed by floating rates , though of course in the event Mr. Lawson resigned over whether to support a weak pound , not restrain a strong one .	O	4899..5096
We recall that Mr. Roberts not only chides the Chancellor for being too easy because of a desire to constrain sterling , but also led the chorus saying that Mr. Volcker was too tight when he let the dollar rise .	O	5097..5307
Somewhere in between there must be a golden mean , perhaps measured by M-Whatever , but perhaps measured by purchasing power parity .	O	5308..5438
The globalists tend to think Mr. Lawson ran onto technical reefs .	B-expansion	5441..5506
In fixing rates the choice of initial parities is crucial , for example , and perhaps he picked the wrong pound-DM rate .	B-expansion	5507..5625
For that matter , perhaps he fixed to the wrong currency .	B-expansion	5626..5682
We sympathize with Mrs. Thatcher 's reluctance to tie her currency to one governed by the domestic political imperatives of West Germany .	B-expansion	5683..5819
Perhaps the shock would have been less if they 'd fixed to another low-tax , deregulated , supply-side economy .	I-expansion	5820..5928
Alan Reynolds of Polyconomics adds his suspicion that the unrecognized inflationary culprit is the budget surplus .	O	5931..6045
Those who can shake Keynesian ghosts out of their heads might recognize that the retirement of gilts for cash is equivalent to an expansionary open-market operation , indeed , it is the definition of an open market operation to expand the money supply .	O	6046..6296
Mr. Reynolds also notes that since British banks have no reserve requirements , high interest rates are less likely to curb inflation than to cause recession .	O	6297..6454
We would add that in political terms , Mrs. Thatcher 's problem was failing to decide between the Chancellor and her adviser .	O	6457..6580
In the end , neither policy was followed , and instead of learning anything we are left with a mystery .	B-expansion	6581..6682
In particular , " shadowing " a currency is anything but fixing ; it is an open announcement that the exchange rate target has no credibility .	I-expansion	6683..6821
All the more so when strong voices are heard opposing the policy .	B-contingency	6822..6887
Better to have a true monetarist policy , just for the experience .	B-contingency	6888..6953
So Mr. Lawson had to resign .	B-expansion	6956..6984
In the end his move was sparked by remarks in excerpts from Sir Alan 's autobiography in The American Economist , a 10,000-circulation academic journal .	B-comparison	6985..7135
But it was the underlying situation that became intolerable .	I-comparison	7136..7196
What Mr. Major and Mrs. Thatcher will do now remains to be seen .	B-contingency	7199..7263
They confront stubborn inflation and a sagging economy , that is to say , stagflation .	I-contingency	7264..7348
This can not be solved by provoking a further downturn ; reducing the supply of goods does not solve inflation .	O	7349..7458
Our advice is this : Immediately return the government surpluses to the economy through incentive-maximizing tax cuts , and find some monetary policy target that balances both supply and demand for money ( which neither aggregates nor interest rates can do ) .	O	7459..7714
This was the version of supply-side economics that , in the late 1970s and early '80s , worked in America and world-wide to solve a far more serious stagflation than afflicts Britain today .	O	7715..7902
