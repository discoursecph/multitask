Congressional Democrats and the Bush administration agreed on a compromise minimum-wage bill , opening the way for the first wage-floor boost in more than nine years .	O	9..174
The agreement ended a long impasse between the congressional leaders and the White House over the wage issue .	B-expansion	177..286
President Bush in June vetoed a measure passed by Congress and said he would n't accept any minimum-wage rise that went beyond limits he set early in this year 's debate on the issue .	I-expansion	287..468
The compromise was a somewhat softened version of what the White House had said it would accept .	B-expansion	471..567
Under the agreement with the House and Senate leaders , the minimum wage would rise from the current $ 3.35 an hour to $ 4.25 an hour by April 1991 .	B-expansion	568..713
Employers could also pay a subminimum " training wage " for 90 days to new workers who are up to 19 years old , and then for another 90 days if the company institutes a specific training program for the newcomers .	I-expansion	714..924
White House officials were delighted that the compromise includes the concept of a training wage , which Mr. Bush has fought for throughout the year .	O	927..1075
" For the first time in history , we have a training wage that will be part " of the nation 's labor laws , said Roger Porter , assistant to the president for economic and domestic policy .	O	1078..1260
White House aides said that although they made a small compromise on the length of a training wage , the final minimum-wage increase will meet the standards set by Mr. Bush .	O	1263..1435
The bill vetoed by the president in June , which the House failed to override , would have lifted the minimum wage to $ 4.55 an hour by late 1991 , with a training wage for up to two months , generally for a worker 's first job .	O	1438..1660
Mr. Bush had been holding out for a bill boosting the wage floor to $ 4.25 an hour by the end of 1991 , coupled with a six-month training wage for workers newly hired by any employer .	B-comparison	1663..1844
Under the compromise , the $ 4.25 level would be reached nine months earlier , while the training subminimum would be shorter , unless it is tied to a training plan .	I-comparison	1845..2006
Democrats argued that the training wage was a way of allowing employers to pay less than the minimum wage , while new workers need far less than six months to be trained for their jobs .	O	2009..2193
Democrats had been negotiating with some Republican congressional leaders on a compromise lately .	B-contingency	2196..2293
With congressional elections next year , GOP leaders have worried about opposing a minimum-wage rise for low-paid workers at a time when Congress is moving toward a capital-gains tax cut that would directly benefit wealthier taxpayers .	B-contingency	2294..2528
Republicans have been imploring the White House to compromise on the wage issue .	I-contingency	2529..2609
In the Senate , Edward Kennedy ( D. , Mass. ) , chairman of the Labor Committee , and Pete Domenici , ( R. , N.M . ) ranking minority member of the Budget Committee , have been working on a compromise , and their soundings showed that the Senate appeared to be heading toward enough strength to override another Bush veto , a Democratic staff official said .	O	2612..2955
The House is scheduled to vote this week on the compromise , as a substitute to a new Democratic bill , itself watered down from last spring 's version .	B-expansion	2958..3107
The Senate will probably vote not long afterward .	I-expansion	3108..3157
Some Democrats thought they might have compromised too much .	B-expansion	3160..3220
Rep. Austin Murphy ( D. , Pa. ) , chairman of the House labor standards subcommittee , said they might have done better " if we 'd held their feet to the fire . "	B-expansion	3221..3374
Mr. Kennedy suggested Democrats " yielded a great deal " on the size of the increase , but he cited concessions from the White House on the training wage , which he said make it " less harsh . "	I-expansion	3375..3562
With only 16-year-olds to 19-year-olds eligible , 68 % of workers getting less than $ 4.25 an hour , who are adults , wo n't be subject to the training wage , he said .	O	3565..3725
The AFL-CIO , which previously opposed the administration 's subminimum idea , said the compromise has " adequate safeguards , so the youth are not exploited and older workers are not displaced . "	O	3726..3916
Gerald F. Seib contributed to this article .	O	3919..3962
