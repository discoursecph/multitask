Democracy can be cruel to politicians : Sometimes it forces them to make choices .	B-instantiation	9..89
Now that the Supreme Court opened the door on the subject of abortion , politicians are squinting under the glare of democratic choice .	B-contrast	90..224
Their discomfort is a healthy sign for the rest of us .	I-contrast	225..279
Republicans are squinting most painfully , at least at first , which is only fair because they 've been shielded the most .	O	282..401
So long as abortion was a question for litigation , not legislation , Republicans could find political security in absolutism .	B-cause	402..526
They could attract one-issue voters by adopting the right-to-life movement 's strongest position , even as pro-choice Republicans knew this mattered little on an issue monopolized by the court .	I-cause	527..718
Now it matters .	O	719..734
Much of Washington thought it detected George Bush in a characteristic waffle on abortion the past week .	B-restatement	737..841
Only a month ago he 'd warned Congress not to pass legislation to pay for abortions in cases of rape or incest .	B-contrast	842..952
Last Friday , after Congress passed it anyway , he hinted he was looking for compromise .	B-cause	953..1039
Was the man who once was pro-choice , but later pro-life , converting again ?	I-cause	1040..1114
In fact , Mr. Bush 's dance was more wiggle than waffle .	O	1117..1171
Pro-life advocates say the White House never wavered over the veto .	O	1172..1239
Christopher Smith ( R. , N.J. ) , a pro-life leader in the House , suggested a compromise that would have adapted restrictive language from rape and incest exceptions in the states .	B-entrel	1240..1416
The White House , never eager for a fight , was happy to try , which is why George Bush said he was looking for " flexibility " last week .	I-entrel	1417..1550
When Democrats refused to budge , pro-life Republicans met at the White House with Chief of Staff John Sununu on Monday , and Mr. Bush quickly signaled a veto .	O	1553..1710
Amid charges of " timidity " on Panama and elsewhere , the president was n't about to offend his most energetic constituency .	O	1711..1832
The GOP doubters were in Congress .	B-restatement	1835..1869
In last week 's House vote , 41 Republicans defected .	B-conjunction	1870..1921
After the vote , Connecticut Rep. Nancy Johnson rounded up nearly as many signatures on a letter to Mr. Bush urging him not to veto .	B-conjunction	1922..2053
Even such a pro-life stalwart as Sen. Orrin Hatch ( R. , Utah ) had counseled some kind of compromise .	B-conjunction	2054..2153
The Senate passed the same bill yesterday , with a veto-proof majority of 67 .	I-conjunction	2154..2230
The manuevering illustrates an emerging Republican donnybrook , pacified since the early 1980s .	B-restatement	2233..2327
At the 1988 GOP convention , abortion was barely discussed at all , though delegates were evenly divided on the question of an anti-abortion constitutional amendment .	B-conjunction	2328..2492
Ms. Johnson made a passionate statement to the platform committee , but she was talking to herself .	I-conjunction	2493..2591
Now many Republicans are listening .	B-cause	2594..2629
They 're frightened by what they see in New Jersey , and especially Virginia , where pro-life GOP candidates for governor are being pummeled on abortion .	I-cause	2630..2780
Eddie Mahe , a Republican consultant , says the two GOP candidates could have avoided trouble if they had framed the issue first .	B-entrel	2781..2908
( In Virginia , Marshall Coleman and his running mate , Eddy Dalton , are both on the defensive for opposing abortions even in cases of rape or incest . )	I-entrel	2909..3057
But Mr. Mahe adds , " The net loser in the next few years is the right-to-life side . "	O	3058..3141
Darla St. Martin , of the National Right to Life Committee , says exit polls from the 1988 election had single-issue , pro-life voters giving Mr. Bush about five more percentage points of support than pro-choice voters gave Michael Dukakis .	O	3144..3381
But the Supreme Court 's opening of debate may have changed even that .	O	3382..3451
GOP pollster Neil Newhouse , of the Wirthlin Group , says polls this summer showed that the single-issue voters had about evened out .	B-entrel	3452..3583
Polls are no substitute for principle , but they 'll do for some politicians .	I-entrel	3584..3659
The Republican danger is that abortion could become for them what it 's long been for Democrats , a divisive litmus test .	B-conjunction	3662..3781
It 's already that in the Bush administration , at least for any job in which abortion is even remotely an issue .	I-conjunction	3782..3893
Oklahoma official Robert Fulton lost a chance for a senior job in the Department of Health and Human Services after right-to-life activists opposed him .	B-conjunction	3896..4048
Caldwell Butler , a conservative former congressman , was barred from a Legal Services post , after he gave wrong answers on abortion .	B-conjunction	4049..4180
Even the president 's doctor , Burton Lee , has said on the record that he 'd love to be surgeon general but could n't pass the pro-life test .	I-conjunction	4181..4318
In the case of HHS Secretary Louis Sullivan , the litmus test could yet damage issues important to other parts of the Republican coalition .	B-cause	4321..4459
After Mr. Sullivan waffled on abortion last year , the White House appeased right-to-lifers by surrounding him with pro-life deputies .	B-contrast	4460..4593
Their views on health care and welfare did n't much matter , though HHS spends billions a year on both .	I-contrast	4594..4695
It makes only a handful of abortion-related decisions .	O	4696..4750
Though Democrats can gloat at all this for now , they may want to contain their glee .	B-cause	4753..4837
On abortion , their own day will come .	B-cause	4838..4875
Eventually even Republicans will find a way to frame the issue in ways that expose pro-choice absolutism .	B-instantiation	4876..4981
Does the candidate favor parental consent for teen-age abortions ?	I-instantiation	4982..5047
( The pro-choice lobby does n't .	B-entrel	5048..5078
) What about banning abortions in the second and third trimesters ?	I-entrel	5078..5144
( The lobby says no again . )	O	5145..5171
Democracy is forcing the abortion debate toward healthy compromise , toward the unpolarizing middle .	B-asynchronous	5174..5273
Roe v. Wade pre-empted political debate , so the extremes blossomed .	B-contrast	5274..5341
Now the ambivalent middle , a moral majority of sorts , is reasserting itself .	B-cause	5342..5418
Within a few years , the outcome in most states is likely to be that abortion will be more restricted , but not completely banned .	B-cause	5419..5547
This is where the voters are , which is where politicians usually end up .	I-cause	5548..5620
