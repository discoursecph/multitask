Israel has launched a new effort to prove the Palestine Liberation Organization continues to practice terrorism, and thus to persuade the U.S. to break off talks with the group.	O	9..186
U.S. officials, however, said they aren't buying the Israeli argument.	O	187..257
Israeli counterterrorism officials provided the State Department with a 20-page list of recent terrorist incidents they attribute directly to forces controlled by PLO Chairman Yasser Arafat.	O	260..450
Mr. Arafat publicly renounced terrorism Dec. 15, satisfying the U.S. precondition for a direct "dialogue" with the PLO.	O	451..570
A U.S. counterterrorism official said experts are studying the Israeli list.	O	573..649
"We have no independent evidence linking Fatah to any acts of terrorism since Dec. 15, 1988," he said, referring to the specific PLO group that Mr. Arafat heads.	O	650..811
So far, this list doesn't change our view.	B-restatement	812..855
Israel wants to end the dialogue, but our analysts take a different view than theirs."	I-restatement	856..942
Israeli Prime Minister Yitzhak Shamir's top adviser on counterterrorism, Yigal Carmon, was here Monday to present the report to members of Congress, reporters and others.	O	945..1115
Mr. Carmon said he also presented the list last week to William Brown, U.S. Ambassador to Israel.	O	1116..1213
Separately, the New York Times reported that the Israeli government had provided its correspondent in Jerusalem with different documents that Israel said prove the PLO has been conducting terrorism from the occupied Arab territories.	O	1216..1449
The State Department said it hasn't yet seen copies of those papers.	O	1450..1518
"If the dialogue was based on the assumption that Arafat or the PLO would stop terrorism, and we have evidence of continued terrorism, what would be the logical conclusion?" Mr. Carmon asked.	O	1521..1712
Israel has long claimed Mr. Arafat never meant to renounce terrorism, particularly because he and his lieutenants reserved the right to press "armed struggle" against the Jewish state.	O	1715..1899
Now, Jerusalem says it is backing up its contention with detailed accounts of alleged terrorist acts and plans linked to Mr. Arafat.	O	1900..2032
It blames most of these on Fatah.	O	2033..2066
The new accusations come at a delicate time in U.S. efforts to bring about talks between Israel and Palestinian representatives.	O	2069..2197
The State Department said it had received a new letter on the subject from Israeli Foreign Minister Moshe Arens, restating Israel's previous objection to negotiating with any Palestinian tied to the PLO.	O	2198..2401
Deciding what constitutes "terrorism" can be a legalistic exercise.	B-instantiation	2404..2471
The U.S. defines it as "premediated, politically motivated violence perpetrated against noncombatant targets by subnational groups or clandestine state agents."	I-instantiation	2472..2632
To meet the U.S. criteria, Israel contended it only listed incidents that involved civilians and occurred inside its pre-1967 borders.	O	2633..2767
At the heart of Israel's report is a list of a dozen incidents Jerusalem attributes to Fatah, including the use of bombs and Molotov cocktails.	O	2770..2913
But U.S. officials say they aren't satisfied these incidents constitute terrorism because they may be offshoots of the intifadah, the Palestinian rebellion in the occupied territories, which the U.S. doesn't classify as terrorism.	O	2914..3144
In addition, the officials say Israel hasn't presented convincing evidence these acts were ordered by Fatah or by any group Mr. Arafat controls.	B-conjunction	3145..3289
U.S. terrorism experts also say they are highly uncertain about the veracity of the separate documents leaked to the New York Times.	I-conjunction	3292..3424
The papers, which Israel says were discovered in Israeli-occupied Gaza, refer to terrorist acts to be carried out in the name of a group called "the Revolutionary Eagles."	O	3425..3596
Some supporters of Israel say U.S. policy on Palestinian terrorism is colored by an intense desire to maintain the dialogue with the PLO.	B-contrast	3599..3736
But State Department officials accuse Israel of leaking questionable claims to embarrass the U.S.	I-contrast	3737..3834
