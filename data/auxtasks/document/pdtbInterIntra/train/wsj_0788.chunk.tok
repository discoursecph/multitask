The Senate Agriculture Committee is responding to trading abuses in the futures markets with a far-reaching bill that would become the Futures Trading Practices Act of 1989 .	root	9..182
The proposed legislation has a laudable goal : to assure the integrity of the U.S. futures markets .	entrel	183..281
However , as is common with sweeping legislation , the proposal contains many provisions that could destroy important parts of the system it sets out to preserve .	comparison	282..442
The complex bill , introduced by Sens. Patrick Leahy ( D. , Vt. ) , Richard Lugar ( R. , Ind. ) , and Bob Kerrey ( D. , Neb. ) , covers a wide range of provisions that would affect the funding and authority of the Commodity Futures Trading Commission and would profoundly change the way the industry is regulated .	norel	445..745
These include provisions relating to the technology and systems that must be employed by exchanges , oversight and disciplinary procedures for exchange trading practices , the relationship between commodity brokerage firms and floor traders , and exchange governance .	restatement	746..1010
The bill also elevates even minor rule infractions to felonies and provides for recovery of punitive damages in civil lawsuits and arbitration cases without any showing of willful misconduct .	norel	1011..1202
Many aspects of the bill are salutary , providing appropriate public safeguards that can and should be instituted throughout the industry .	norel	1205..1342
Indeed , some of the bill 's requirements , including broad representation on the exchanges ' boards of directors and strong measures to prevent conflicts of interest , already have been put in place by the Coffee , Sugar & Cocoa Exchange and other futures exchanges .	conjunction	1343..1604
Other aspects of the bill , however , are either structured in ways that create unnecessary burdens for the industry or actually are harmful to the exchanges , the industry and ultimately the general public .	norel	1607..1811
One of the most prominent features is the requirement that in three years all exchanges have in place a system that records all trades by a source independent of the executing broker .	norel	1814..1997
The New York futures exchanges have been working together to develop a trade recording system much like the one called for in the bill .	conjunction	1998..2133
We would be delighted to have such a system in place today .	entrel	2134..2193
But is it realistic for Congress to mandate by a rigid deadline a system that has not yet been subjected to feasibility studies ?	comparison	2194..2322
What if the system does n't work ?	norel	2323..2355
What if the only system that does work is so expensive that , at best , only the largest exchanges can afford it ?	norel	2356..2467
Cost is a key consideration because of the global sweep of the financial markets .	norel	2470..2551
The U.S. futures exchanges compete world-wide as never before .	entrel	2552..2614
Today , trading in almost any commodity can be diverted from U.S. markets with just a few strokes of a keyboard .	cause	2615..2726
All foreign markets are aggressively courting U.S. business .	conjunction	2727..2787
In fact , several London markets already offer lower costs for trading in the same or very similar contracts .	restatement	2788..2896
The U.S. exchanges need both market integrity and cost-efficiency	cause	2897..2962
long-term growth depends on it .	cause	2963..2995
The Senate bill contains many provisions that will increase the costs of trading .	norel	2998..3079
The most arbitrary of these is the imposition of " service fees , " which will directly widen the cost spread between U.S. and foreign markets .	restatement	3080..3220
Other provisions have a more subtle , but nonetheless real and detrimental effect on the international position of U.S. exchanges .	norel	3223..3352
These include the extension of liability into areas beyond those established by judicial precedent and the expansion of liability to include punitive damages .	restatement	3353..3511
In addition to increasing costs as a result of greater financial exposure for members , these measures could have other , far-reaching repercussions .	norel	3514..3661
One section of the bill would make all commodity brokerage firms and floor brokers liable for damages without willful misconduct .	norel	3664..3793
Nowhere in the federal securities law is simple negligence or inadvertent action a source of liability under similar circumstances .	contrast	3794..3925
It is only logical to assume that the enactment of this provision will lead to increased litigation .	norel	3928..4028
In an already low-profitmargin business , commodity brokerage firms may well decide to eliminate the risk and expense of dealing with the retail public , depriving the private individual of access to the markets .	cause	4029..4239
Another measure makes commodity brokerage firms liable for violations committed by independent floor brokers who execute trades for them .	norel	4242..4379
This untried concept would expose these firms to potentially astronomical punitive damages .	comparison	4380..4471
Faced with the virtually impossible task of supervising the execution of each trade , many commodity brokerage firms are likely to stop doing business with independents and instead hire their own salaried floor brokers .	norel	4474..4692
This would force out of business many of the individuals and small firms that function as floor brokers .	cause	4693..4797
A consequence of their departure could be a serious diminution of market liquidity .	cause	4798..4881
Finally , under the bill , a number of legitimate , longstanding business practices would be arbitrarily banned	norel	4884..4992
unless the CFTC were to take specific and timely action to permit them to continue .	alternative	4993..5077
In other words , regulation will occur through inaction and happenstance , rather than through a normal deliberative procedure .	restatement	5078..5203
The affected practices include the placing of oral orders , which is the way most public customer orders are placed , and trading between affiliated brokers	norel	5206..5360
even though in some cases trading with affiliates may be the only way to obtain the best execution for a client .	concession	5361..5474
Also precluded would be dual trading , whereby a broker trades for customers as well as his own account , a practice that provides needed liquidity to the markets .	conjunction	5475..5636
All U.S. futures exchanges agree that these and other trading practices require proper regulation and supervision .	norel	5639..5753
Nonetheless , each has too much potential value to the system to be banned by legislative fiat before the CFTC carefully considers all the consequences of a ban and what the regulatory alternatives are .	comparison	5754..5955
The markets are complex , as is the environment in which they function .	norel	5958..6028
When problems surface , the temptation becomes strong to summarily overhaul a market system that has served for more than 100 years .	entrel	6029..6160
That temptation must be put aside to permit careful consideration of all the implications , positive and negative , of the proposed resolutions to those problems , and to avoid creating a marketplace where no one trades .	comparison	6161..6378
Mr. Nastro is chairman of the Coffee , Sugar & Cocoa Exchange in New York and director of commodity administration at Shearson Lehman Hutton .	norel	6381..6521
