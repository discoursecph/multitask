Crude oil futures prices fell further	(Topic-Change(Elaboration(Explanation(Elaboration(Background
as analysts and traders said	(Attribution
OPEC oil producers are n't putting the brakes on output ahead of the traditionally weak first quarter .	Attribution))
In trading on the New York Mercantile Exchange , the U.S. benchmark West Texas Intermediate crude fell 39 cents a barrel to $ 19.76 for December delivery .	(Joint
Petroleum products prices also declined .	Joint))
Analysts pointed to reports	(Elaboration(Elaboration
that the Organization of Petroleum Exporting Countries is producing substantially more than its official limit of 20.5 million barrels a day ,	(Evaluation
with some accounts putting the 13-nation group 's output as high as 23 million barrels a day .	Evaluation))
That level of production did n't take its toll on futures prices for the fourth quarter ,	(Contrast(Temporal
when demand is traditionally strong .	Temporal)
But	(Elaboration(Same-unit
because first-quarter demand is normally the weakest of the year ,	(Explanation
several market participants say ,	(Attribution
OPEC production will have to decline	(Enablement
to keep prices from eroding further .	Enablement))))
The group plans to meet in a month	(Enablement
to discuss production strategy for early 1990 .	Enablement)))))
With prices already headed lower ,	(Evaluation(Elaboration(Background(Cause(Background
news of a series of explosions at a major Phillips Petroleum Co. chemical facility on the Houston Ship Channel also was bearish for prices .	Background)
Even though such facilities use a relatively small amount of crude ,	(Contrast
analysts say ,	(Attribution
now the facility wo n't need any , at a time of already high availability .	Attribution)))
The Phillips plant makes polyethylene , polypropylene and other plastic products .	Background)
A company official said	(Evaluation(Cause(Attribution
the explosions began	(Cause
when a seal blew out .	Cause))
Dozens of workers were injured ,	(Attribution
authorities said .	Attribution))
There was no immediate estimate of damage from the company .	Evaluation))
Some petroleum futures traders say	(Elaboration(Attribution
technical considerations now will help to put downward pressure on futures prices .	Attribution)
For instance , one trader said	(Summary(Attribution
that prices inevitably will go lower	(Background
now that they 've fallen below $ 20 a barrel .	Background))
`` Our technician is a little bearish	(Attribution(Background
now that we 've taken out $ 20 , ''	Background)
he said .	Attribution)))))
In other commodity markets yesterday :	(Textual-organization
COPPER :	(Joint(Textual-organization
The selling	(Explanation(Same-unit(Elaboration
that started on Friday	Elaboration)
continued yesterday .	Same-unit)
The December contract fell 3.85 cents a pound to $ 1.1960 .	(Joint
London Metal Exchange warehouse stocks were down only 4,800 metric tons for the week to 84,500 tons ;	(Joint(Elaboration
expectations late last week were a drop of 10,000 to 15,000 tons .	Elaboration)
The New York market made its high for the day on the opening	(Elaboration(Evaluation(Evaluation(Elaboration(Joint
and	(Background(Same-unit
when it dropped below the $ 1.23-a-pound level ,	(Temporal
selling picked up	Temporal))
as previous buyers bailed out of their positions	(Temporal
and aggressive short sellers	(Same-unit(Elaboration
-- anticipating further declines --	Elaboration)
moved in .	Same-unit))))
Fund selling also picked up at that point .	Elaboration)
According to Bernard Savaiko , senior commodity analyst at PaineWebber ,	(Cause(Attribution
the only stability to the market came	(Background
when short sellers periodically moved in	(Enablement
to cover their positions	(Manner-Means
by buying contracts .	Manner-Means))))
This activity produced small rallies ,	(Cause
which in turn attracted new short selling .	Cause)))
Mr. Savaiko noted	(Evaluation(Explanation(Attribution
that copper had a steep fall	(Contrast
in spite of a weak dollar ,	(Elaboration
which would normally support the U.S. copper market .	Elaboration)))
Such support usually comes from arbitragers	(Elaboration
who use a strong British pound	(Enablement
to buy copper in New York .	Enablement)))
`` The sell-off would probably have been worse	(Attribution(Condition
if the dollar had been strong , ''	Condition)
he said .	Attribution)))
Copper has been stuck in a trading range of $ 1.19 to $ 1.34 .	(Evaluation
Mr. Savaiko believes	(Attribution
that	(Same-unit(Condition
if copper falls below the bottom of this range	Condition)
the next significant support level will be about $ 1.04 .	Same-unit))))))))
PRECIOUS METALS :	(Joint(Textual-organization
Platinum and palladium struggled to maintain their prices all day	(Elaboration(Evaluation(Elaboration(Explanation(Contrast
despite news stories over the weekend	(Elaboration
that recent cold fusion experiments ,	(Same-unit(Elaboration
which use both metals ,	Elaboration)
showed signs	(Elaboration
of producing extra heat .	Elaboration))))
January platinum closed down $ 2.80 an ounce at $ 486.30 , nearly $ 4 above its low for the day .	(Evaluation(Joint
December palladium was off $ 1.55 an ounce at $ 137.20 .	Joint)
Platinum is believed to have good support around $ 480 and palladium at around $ 130 .	Evaluation))
Some traders were thought to be waiting for the auto sales report ,	(Explanation(Elaboration
which will be released today .	Elaboration)
Such sales are watched closely by platinum and palladium traders	(Explanation
because both metals are used in automobile catalytic converters .	Explanation)))
Mr. Savaiko theorized	(Elaboration(Attribution
that the news on cold fusion did n't affect the market yesterday	(Explanation
because many traders have already been badly burnt by such stories .	Explanation))
He said	(Attribution
the traders are demanding a higher level of proof	(Background
before they will buy palladium again .	Background))))
Also weighing on both metals ' prices is the role of the chief supplier , the Soviet Union .	(Explanation
Many analysts believe	(Attribution
that the Soviets ' thirst for dollars this year	(Condition(Same-unit(Elaboration
to buy grain and other Western commodities and goods	Elaboration)
will bring them to the market	Same-unit)
whenever prices rally very much .	Condition)))))
GRAINS AND SOYBEANS :	(Joint(Textual-organization
Prices closed mixed	(Elaboration(Explanation(Background
as contracts reacted to largely offsetting bullish and bearish news .	Background)
On the Chicago Board of Trade , soybeans for November delivery closed at $ 5.63 a bushel , down half a cent ,	(Temporal
while the December wheat contract rose three-quarters of a cent to $ 4.0775 a bushel .	Temporal))
Supporting prices was the announcement late Friday of additional grain sales to the Soviet Union .	(Elaboration(Contrast
But acting as a drag on prices was the improved harvest weather over the weekend and the prospect for continued fair weather this week over much of the Farm Belt .	Contrast)
Strong farmer selling over the weekend also weighed on prices .	Elaboration)))
SUGAR :	(Joint(Textual-organization
World prices tumbled , mostly from their own weight ,	(Evaluation(Explanation(Elaboration(Attribution
according to analysts .	Attribution)
The March contract ended at 13.79 cents a pound , down 0.37 cent .	Elaboration)
For the past week or so , traders have been expecting India to buy between 150,000 and 200,000 tons of refined sugar ,	(Contrast(Joint
and there have been expectations of a major purchase by Japan .	Joint)
But with no reports	(Same-unit(Attribution(Elaboration
of either country actually entering the market ,	Elaboration)
analysts said ,	Attribution)
futures prices became vulnerable .	Same-unit)))
Developing countries such as India ,	(Contrast(Same-unit(Attribution
some analysts said ,	Attribution)
seem to have made it a point	(Elaboration
to stay away	(Joint(Condition
whenever sugar reached the top of its trading range , around 14.75 cents ,	Condition)
and wait for prices to return to the bottom of the range , around 13.50 cents .	Joint)))
But Erik Dunlaevy , a sugar analyst with Balfour Maclaine International Ltd. , said	(Joint(Elaboration(Attribution
the explanation for the latest drop in sugar prices is much simpler :	Attribution)
Speculators ,	(Cause(Same-unit(Attribution
he said ,	Attribution)
`` got too long too soon	Same-unit)
and ran into resistance around the old contract highs . ''	Cause))
A PaineWebber analyst said	(Attribution
that in light of a new estimate of a production increase of four million metric tons and only a modest increase in consumption , sugar is n't likely to rise above the top of its trading range	(Contrast
without a crop problem in a major producing country .	Contrast))))))
COCOA :	(Textual-organization
Futures rallied modestly .	(Evaluation(Evaluation(Evaluation(Evaluation(Explanation
The December contract rose $ 33 a metric ton to $ 1,027 , near its high for the day .	Explanation)
Gill & Duffus Ltd. , a British cocoa-trading house , estimated	(Attribution
that the 1989-90 world cocoa surplus would be 231,000 tons , down from 314,000 tons for the previous year .	Attribution))
Market technicians were encouraged by the price patterns ,	(Explanation(Elaboration
which in the past have preceded sharp rallies .	Elaboration)
Recent prices for cocoa have been near levels	(Elaboration
last seen in the mid-1970s .	Elaboration)))
At such prices ,	(Contrast(Same-unit(Attribution
according to Mr. Savaiko ,	Attribution)
bargain hunting and short-covering	(Same-unit(Elaboration
-- buying back of contracts	(Elaboration
previously sold --	Elaboration))
by speculators is n't uncommon .	Same-unit))
But Mr. Savaiko expects stepped-up producer selling at around the $ 1,040 to $ 1,050 level .	Contrast))
He also noted	(Joint(Attribution
that a strong sterling market yesterday might have helped cocoa in New York	(Background
as arbitragers took advantage of the currency move .	Background))
Sandra Kaul , research analyst at Shearson Lehman Hutton , said	(Attribution
the market pushed higher mainly in anticipation of a late harvest in the Ivory Coast , a major cocoa producer .	Attribution))))))))))
