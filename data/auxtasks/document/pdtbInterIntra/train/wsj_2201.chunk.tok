We 're about to see if advertising works .	root	9..49
Hard on the heels of Friday 's 190-point stock-market plunge and the uncertainty that 's followed , a few big brokerage firms are rolling out new ads trumpeting a familiar message : Keep on investing , the market 's just fine .	norel	52..272
Their mission is to keep clients from fleeing the market , as individual investors did in droves after the crash in October	cause	273..395
Just days after the 1987 crash , major brokerage firms rushed out ads to calm investors .	norel	398..485
This time around , they 're moving even faster .	contrast	486..531
PaineWebber Inc. filmed a new television commercial at 4 p.m. EDT yesterday and had it on the air by last night .	norel	534..646
Fidelity Investments placed new ads in newspapers yesterday , and wrote another new ad appearing today .	conjunction	647..749
Shearson Lehman Hutton Inc. by yesterday afternoon had already written new TV ads .	conjunction	750..832
It considered running them during tomorrow night 's World Series broadcast but decided not to when the market recovered yesterday .	entrel	833..962
Other brokerage firms , including Merrill Lynch & Co. , were plotting out potential new ad strategies .	conjunction	963..1063
The brokerage firms learned a lesson the last time around , when frightened investors flooded the phone lines and fled the market in a panic .	norel	1066..1206
This time , the firms were ready .	cause	1207..1239
Fidelity , for example , prepared ads several months ago in case of a market plunge .	instantiation	1240..1322
When the market went into its free fall Friday afternoon , the investment firm ordered full pages in the Monday editions of half a dozen newspapers .	asynchronous	1323..1470
The ads touted Fidelity 's automated 800-number beneath the huge headline , " Fidelity Is Ready For Your Call . "	entrel	1471..1579
A Fidelity spokesman says the 800-line , which already was operating but which many clients did n't know about , received about double the usual volume of calls over the weekend .	norel	1582..1757
" A lot of investor confidence comes from the fact that they can speak to us , " he says .	entrel	1758..1844
" To maintain that dialogue is absolutely crucial .	cause	1845..1894
It would have been too late to think about on Friday .	contrast	1895..1948
We had to think about it ahead of time . "	cause	1949..1989
Today 's Fidelity ad goes a step further , encouraging investors to stay in the market or even to plunge in with Fidelity .	norel	1992..2112
Underneath the headline " Diversification , " it counsels , " Based on the events of the past week , all investors need to know their portfolios are balanced to help protect them against the market 's volatility . "	restatement	2113..2319
It goes on to plug a few diversified Fidelity funds by name .	asynchronous	2320..2380
PaineWebber also was able to gear up quickly thanks to the 1987 crash .	norel	2383..2453
In the aftermath of the 1987 debacle , the brokerage firm began taping commercials in-house	restatement	2454..2544
ultimately getting its timing down fast enough to tape a commercial after the market closed and rush it on the air that night .	asynchronous	2545..2672
It also negotiated an arrangement with Cable News Network under which CNN would agree to air its last-minute creations .	conjunction	2673..2792
The new PaineWebber commercial , created with ad agency Saatchi & Saatchi Co. , features Mary Farrell , one of the firm 's most visible investment strategists , sounding particularly bullish .	norel	2795..2981
Taped just as the market closed yesterday , it offers Ms. Farrell advising , " We view the market here as going through a relatively normal cycle ... .	restatement	2982..3129
We continue to feel that the stock market is still the place to be for long-term appreciation . "	conjunction	3130..3225
The spot was scheduled to appear three times on CNN last night .	entrel	3226..3289
PaineWebber considered an even harder sell , recommending specific stocks .	norel	3292..3365
Instead , it settled on just urging the clients who are its lifeline to keep that money in the market .	alternative	3366..3467
" We 're saying the worst thing that anyone can do is to see the market go down and dump everything , which just drives the prices down further , " says John Lampe , PaineWebber 's director of advertising .	norel	3470..3668
" If you owned it and liked it Friday , the true value has n't changed . "	cause	3669..3738
He adds , " This is n't 1987 revisited . "	conjunction	3739..3776
With the market fluctuating and then closing up more than 88 points yesterday , investment firms had to constantly revise their approach .	norel	3779..3915
At Shearson Lehman , executives created potential new commercials Friday night and throughout the weekend , then had to regroup yesterday afternoon .	instantiation	3916..4062
The plan had been to make one of Shearson 's easy-to-film , black-and-white " Where We Stand " commercials , which have been running occasionally in response to news events since 1985 .	entrel	4063..4242
The ad would have run during the World Series tomorrow , replacing the debut commercial of Shearson 's new ad campaign , " Leadership by Example . "	entrel	4243..4385
But in a meeting after the market closed yesterday , Shearson executives decided not to go ahead with the stock-market ad .	contrast	4388..4509
" We do n't think at this point anything needs to be said .	cause	4510..4566
The market seems to be straightening out ; we 're taking a wait-and-see attitude , " says Cathleen B. Stewart , executive vice president of marketing .	cause	4567..4712
In any case , the brokerage firms are clearly moving faster to create new ads than they did in the fall of 1987 .	norel	4715..4826
But it remains to be seen whether their ads will be any more effective .	contrast	4827..4898
In 1987 , despite a barrage of ads from most of the major investment firms , individuals ran from the market en masse .	cause	4899..5015
Now the firms must try their hardest to prove that advertising can work this time around .	cause	5016..5105
Ad Notes ... .	norel	5108..5121
ARNOLD ADVERTISING :	norel	5124..5143
Edward Eskandarian , former chairman of Della Femina , McNamee WCRS/Boston , reached an agreement in principle to acquire a majority stake in Arnold Advertising , a small Boston shop .	norel	5144..5323
Terms were n't disclosed .	entrel	5324..5348
Mr. Eskandarian , who resigned his Della Femina post in September , becomes chairman and chief executive of Arnold .	entrel	5349..5462
John Verret , the agency 's president and chief executive , will retain the title of president .	contrast	5463..5555
Separately , McDonald 's Corp. , Oak Brook , Ill. , named Arnold to handle its estimated $ 4 million cooperative ad account for the Hartford , Conn. , area .	norel	5556..5704
That account had been handled by Della Femina , McNamee WCRS .	asynchronous	5705..5765
EDUCATION ADS :	norel	5768..5782
A 142-page ad supplement to Business Week 's special " Corporate Elite " issue calls on business leaders to use their clout to help solve the nation 's education crisis .	norel	5783..5948
The supplement , the largest ever for the magazine , includes ads from 52 corporate advertisers and kicks off a two-year Business Week initiative on education .	entrel	5949..6106
The magazine will distribute 10 % of the gross revenues from the supplement as grants to innovative teachers .	entrel	6107..6215
