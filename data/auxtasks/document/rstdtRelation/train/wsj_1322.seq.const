The bond market ,	(Summary(Same-unit(Elaboration
which sometimes thrives on bad news ,	Elaboration)
cheered yesterday 's stock market sell-off and perceptions	(Elaboration
that the economy is growing weaker .	Elaboration))
Early in the day , bonds rose modestly on economists ' forecasts	(Topic-Change(Temporal(Elaboration(Evaluation(Elaboration
that this week 's slate of economic data will portray an economy headed for trouble .	Elaboration)
Such news is good for bonds	(Evaluation
because economic weakness sometimes causes the Federal Reserve to lower interest rates in an effort	(Elaboration
to stimulate the economy	(Joint
and stave off a recession .	Joint))))
For example , today the Department of Commerce is scheduled to release the September durable goods report .	(Elaboration
The consensus forecast of 14 economists	(Background(Same-unit(Elaboration
surveyed by Dow Jones Capital Markets Report	Elaboration)
is for a 1.2 % drop in September orders .	Same-unit)
That would follow a 3.9 % advance in August .	Background)))
Bonds received a bigger boost later in the day	(Elaboration(Elaboration(Background
when stock prices moved broadly lower .	Background)
The Dow Jones Industrial Average fell 26.23 points to 2662.91 .	Elaboration)
`` Bond investors have been watching stocks closely , ''	(Joint(Attribution
said Joel Marver , chief fixed-income analyst at Technical Data Global Markets Group .	Attribution)
`` When you get a big sell-off in equities ,	(Attribution(Background
money starts to shift into bonds , ''	(Elaboration
which are considered safer ,	Elaboration))
he said .	Attribution))))
The Treasury 's benchmark 30-year bond ended about 1/2 point higher , or up about $ 5 for each $ 1,000 face amount ,	(Topic-Change(Joint(Contrast
while the yield slid to 7.93 % from 7.98 % Friday .	Contrast)
Municipals ended mixed ,	(Joint(Joint
while mortgage-backed and investment-grade corporate bonds rose .	Joint)
Prices of high-yield , high-risk corporate securities ended unchanged .	(Joint
In more evidence of the growing division between `` good '' and `` bad '' junk bonds , a $ 150 million issue by Imo Industries Inc. was snapped up by investors	(Joint(Contrast
while underwriters for Beatrice Co. 's $ 350 million issue are considering restructuring the deal	(Enablement
to attract buyers .	Enablement))
In the Treasury market , analysts expect bond prices to trade in narrow ranges this week	(Joint(Evaluation(Background
as the market takes in positive and negative news .	Background)
`` On the negative side , the market will be affected by constant supply in all sectors of the market , ''	(Contrast(Attribution
said William M. Brachfeld , economist at Daiwa Securities America Inc .	Attribution)
`` On the other hand , we have economic news	(Joint(Elaboration
that is { expected to be } relatively positive for the bond market .	Elaboration)
We will go back and forth with a tilt toward slightly lower yields , ''	(Attribution
he said .	Attribution))))
Today , the Treasury will sell $ 10 billion of new two-year notes .	(Joint
Tomorrow , Resolution Funding Corp. , a division of a new government agency	(Joint(Evaluation(Elaboration(Same-unit(Elaboration
created	(Enablement
to bail out the nation 's troubled thrifts ,	Enablement))
will hold its first bond auction	(Elaboration
at which it will sell $ 4.5 billion of 30-year bonds .	Elaboration))
So far , money managers and other bond buyers have n't shown much interest in the Refcorp bonds .	Elaboration)
Analysts have mixed views about the two-year note auction .	(Elaboration
While some say	(Contrast(Attribution
the auction should proceed smoothly ,	Attribution)
others contend	(Attribution
that yesterday 's sale of $ 2.58 billion of asset-backed securities by Ford Motor Credit Corp. may have siphoned some potential institutional buyers from the government 's note sale .	Attribution))))
The division of auto maker Ford Motor Co. made its debut in the asset-backed securities market with the second-largest issue in the market 's four-year history .	(Comparison(Elaboration(Elaboration
The company offered securities	(Elaboration(Elaboration
backed by automobile loans through an underwriting group	(Elaboration
headed by First Boston Corp .	Elaboration))
The issue yields 8.90 %	(Elaboration(Joint
and carries a guarantee	(Elaboration
covering 9 % of the deal from the company .	Elaboration))
First Boston sweetened the terms from the original yield estimate in an apparent effort	(Elaboration
to place the huge offering .	Elaboration))))
The issue was offered at a yield nearly one percentage point above the yield on two-year Treasurys .	Elaboration)
The only asset-backed deal larger than Ford 's was a $ 4 billion offering by General Motors Acceptance Corp. in 1986 .	Comparison))))))))
Treasury Securities	(Textual-organization(Textual-organization
Treasury bonds were 1/8 to 1/2 point higher yesterday in light trading .	(Elaboration
The benchmark 30-year bond ended at a price of 102 3/32 ,	(Elaboration(Elaboration(Joint(Comparison
compared with 101 17/32 Friday .	Comparison)
The latest 10-year notes were quoted late at 100 17/32	(Joint(Joint(Comparison(Cause
to yield 7.90 % ,	Cause)
compared with 100 3/32	(Cause
to yield 7.97 % .	Cause))
The latest two-year notes were quoted late at 100 28/32	(Cause
to yield 7.84 % .	Cause))
Short-term rates rose yesterday at the government 's weekly Treasury bill auction ,	(Joint(Comparison
compared with the previous bill sale .	Comparison)
The Treasury sold $ 7.81 billion of three-month bills with an average discount rate of 7.52 % , the highest since the average of 7.63 % at the auction on Oct. 10 .	(Joint
The $ 7.81 billion of six-month Treasury bills were sold with an average discount rate of 7.50 % , the highest since the average of 7.60 % at the Oct. 10 auction .	Joint))))
The rates were up from last week 's auction ,	(Comparison
when they were 7.37 % and 7.42 % , respectively .	Comparison))
Here are auction details :	(Elaboration
Rates are determined by the difference between the purchase price and face value .	(Joint(Evaluation
Thus , higher bidding narrows the investor 's return	(Contrast
while lower bidding widens it .	Contrast))
The percentage rates are calculated on a 360-day year ,	(Joint(Contrast
while the coupon-equivalent yield is based on a 365-day year .	Contrast)
Both issues are dated Oct. 26 .	(Elaboration
The 13-week bills mature Jan. 25 , 1990 ,	(Comparison
and the 26-week bills mature April 26 , 1990 .	Comparison))))))))
Corporate Issues	(Textual-organization(Textual-organization
Investment-grade corporates closed about 1/4 point higher in quiet trading .	(Joint
In the junk bond market , Imo Industries ' issue of 12-year debentures ,	(Elaboration(Elaboration(Elaboration(Same-unit(Elaboration
considered to be one of the market 's high-quality credits ,	Elaboration)
was priced at par to yield 12 % .	Same-unit)
Peter Karches , managing director at underwriter Morgan Stanley & Co. , said	(Explanation(Attribution
the issue was oversubscribed .	Attribution)
`` It 's a segmented market ,	(Attribution(Background
and	(Same-unit
if you have a good , strong credit ,	(Condition
people have an appetite for it , ''	Condition)))
he said .	Attribution)))
Morgan Stanley is expected to price another junk bond deal , $ 350 million of senior subordinated debentures by Continental Cablevision Inc. , next Tuesday .	Elaboration)
In light of the recent skittishness in the high-yield market , junk bond analysts and traders expect other high-yield deals to be sweetened or restructured	(Background(Elaboration(Condition
before they are offered to investors .	Condition)
In the case of Beatrice , Salomon Brothers Inc. is considering restructuring the reset mechanism on the $ 200 million portion of the offering .	Elaboration)
Under the originally contemplated terms of the offering , the notes would have been reset annually at a fixed spread above Treasurys .	(Comparison
Under the new plan	(Elaboration(Cause(Same-unit(Elaboration
being considered ,	Elaboration)
the notes would reset annually at a rate	Same-unit)
to maintain a market value of 101 .	Cause)
Price talk calls for the reset notes to be priced at a yield of between 13 1/4 % and 13 1/2 % .	Elaboration))))))
Mortgage-Backed Securities	(Textual-organization(Textual-organization
Activity in derivative markets was strong with four new real estate mortgage investment conduits announced and talk of several more deals in today 's session .	(Elaboration(Elaboration
The Federal National Mortgage Association offered $ 1.2 billion of Remic securities in three issues ,	(Explanation(Joint
and the Federal Home Loan Mortgage Corp. offered a $ 250 million Remic	(Elaboration
backed by 9 % 15-year securities .	Elaboration))
Part of the reason for the heavy activity in derivative markets is that underwriters are repackaging mortgage securities	(Joint(Elaboration
being sold by thrifts .	Elaboration)
Traders said	(Background(Attribution
thrifts have stepped up their mortgage securities sales	Attribution)
as the bond market has risen in the past two weeks .	Background))))
In the mortgage pass-through sector , active issues rose	(Joint(Comparison
but trailed gains in the Treasury market .	Comparison)
Government National Mortgage Association 9 % securities for November delivery were quoted late yesterday at 98 10/32 , up 10/32 ;	(Elaboration(Joint
and Freddie Mac 9 % securities were at 97 1/2 , up 1/4 .	Joint)
The Ginnie Mae 9 % issue was yielding 8.36 % to a 12-year average life assumption ,	(Joint
as the spread above the Treasury 10-year note widened slightly to 1.46 percentage points .	Joint)))))
Municipals	(Textual-organization(Topic-Change
A $ 575 million San Antonio , Texas , electric and gas system revenue bond issue dominated the new issue sector .	(Topic-Change(Elaboration
The refunding issue ,	(Contrast(Same-unit(Elaboration
which had been in the wings for two months ,	Elaboration)
was one of the chief offerings	(Elaboration
overhanging the market	(Joint
and limiting price appreciation .	Joint)))
But alleviating that overhang failed to stimulate much activity in the secondary market ,	(Explanation(Elaboration
where prices were off 1/8 to up 3/8 point .	Elaboration)
An official with lead underwriter First Boston said	(Elaboration(Attribution
orders for the San Antonio bonds were `` on the slow side . ''	Attribution)
He attributed that to the issue 's aggressive pricing and large size , as well as the general lethargy in the municipal marketplace .	(Joint
In addition ,	(Same-unit(Attribution
he noted ,	Attribution)
the issue would normally be the type	(Contrast(Elaboration
purchased by property and casualty insurers ,	Elaboration)
but recent disasters , such as Hurricane Hugo and the Northern California earthquake , have stretched insurers ' resources	(Joint
and damped their demand for bonds .	Joint))))))))
A $ 137.6 million Maryland Stadium Authority sports facilities lease revenue bond issue appeared to be off to a good start .	(Topic-Change(Elaboration
The issue was oversubscribed	(Attribution(Joint
and `` doing very well , ''	Joint)
according to an official with lead underwriter Morgan Stanley .	Attribution))
Activity quieted in the New York City bond market ,	(Background
where heavy investor selling last week drove yields on the issuer 's full faith and credit backed bonds up as much as 0.50 percentage point .	Background))))
Foreign Bonds	(Textual-organization
Japanese government bonds ended lower	(Topic-Change(Elaboration(Background
after the dollar rose modestly against the yen .	(Explanation
The turnaround in the dollar fueled bearish sentiment about Japan 's bond market .	Explanation))
The benchmark No. 111 4.6 % bond due 1998 ended on brokers ' screens at a price of 95.39 , off 0.28 .	(Elaboration
The yield rose to 5.38 % .	Elaboration))
West German bond prices ended lower after a day of aimless trading .	(Topic-Change(Elaboration
The benchmark 7 % bond due October 1999 fell 0.20 point to 99.80	(Joint(Cause
to yield 7.03 % ,	Cause)
while the 6 3/4 % notes due July 1994 fell 0.10 to 97.65	(Cause
to yield 7.34 % .	Cause)))
British government bonds ended slightly higher in quiet trading	(Elaboration(Temporal
as investors looked ahead to today 's British trade report .	Temporal)
The benchmark 11 3/4 % Treasury bond due 2003/2007 rose 1/8 to 111 21/32	(Joint(Cause
to yield 10.11 % ,	Cause)
while the 12 % issue of 1995 rose 3/32 to 103 23/32	(Cause
to yield 11.01 % .	Cause)))))))))))))
