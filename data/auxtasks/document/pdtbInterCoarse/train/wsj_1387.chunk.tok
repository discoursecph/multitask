Yet another political scandal is racking Japan .	B-comparison	9..56
But this time it 's hurting opposition as well as ruling-party members .	B-expansion	57..127
And as it unfolds , it 's revealing some of the more tangled and seamier aspects of Japanese society .	I-expansion	128..227
Already , ruling Liberal Democratic Party demands that opposition members testify under oath in parliament have stalled one budget committee session and forced the committee to plan a special two-day investigation at the end of the month .	B-comparison	230..467
But the scandal itself is so convoluted that ruling-party members are divided between those who want to pursue the matter in hope of undermining the opposition and those who favor leaving well enough alone .	I-comparison	468..674
" The opposition can be the most hurt because everyone already figures the LDP is that kind of beast , " says Shigezo Hayasaka , former aide to LDP kingmaker Kakuei Tanaka and now an independent analyst .	O	677..876
But , he adds , " We ca n't tell where it will go at all because we 're still in the middle of it . "	O	877..971
This time , the scandal centers on donations made by the not-quite-mainstream pachinko parlor industry .	B-entrel	974..1076
Pachinko , a kind of pinball , is Japan 's favorite form of legal gambling .	B-entrel	1077..1149
The donations so far appear to be small , especially compared with the huge sums that changed hands in the Recruit Co . influence-peddling scandal that plagued the ruling party last year .	B-comparison	1150..1335
But the implications could be great .	I-comparison	1336..1372
Pachinko is slightly on the shady side , often linked to the lower ranks of Japan 's underworld and regularly at the top of annual lists of tax evaders .	B-entrel	1375..1525
Recently the industry has faced the threat of new restrictions , and political donations may have been made with the intent to bribe .	I-entrel	1526..1658
Also , about 60 % of pachinko parlor owners are Korean , many of whom maintain close ties with North or South Korean residents ' organizations , and donations by such foreign groups are illegal in Japan .	O	1659..1857
To many Japanese , pachinko is benign or enticingly unsavory .	B-entrel	1860..1920
Garish neon pachinko marquees blaze from the main streets and narrow alleys of cities and towns across the country .	B-expansion	1921..2036
Devotees pass hours , watching the lights blink and listening to the metal balls ping , as much to gamble as to get a little time to be anonymous , alone with their thoughts .	B-entrel	2037..2208
At 500 yen ( $ 3.60 ) for a handful of balls , pachinko is a common pastime , and has been since it took root as cheap entertainment in the years after World War II .	B-expansion	2209..2369
But the total of all those pinging balls has created an industry with a reported annual income of 13 trillion yen ( almost $ 92 billion ) , or nearly the size of Japan 's vaunted automobile industry .	B-expansion	2372..2566
And because the pachinko industry is regularly at the top of annual lists for tax evasion , some observers estimate the real income could be as much as 20 trillion yen .	I-expansion	2567..2734
If that money were being taxed , it could bring the government a badly needed several trillion yen .	B-contingency	2737..2835
In 1984 , an attempt was made to crack down on the industry with tougher restrictions .	B-temporal	2836..2921
Then , in 1988 , a proposal to keep better track of income by selling prepaid cards for pachinko was fielded in parliament .	B-comparison	2922..3043
The proposal split the industry in two , along the lines of national origin : North Koreans oppose the plan while South Koreans , Japanese and Taiwanese accept it or are neutral .	I-comparison	3044..3219
In August , a conservative weekly magazine reported that a pachinko industry organization donated money to Japan Socialist Party members .	B-expansion	3222..3358
The magazine alleged that in making the donations , the pachinko industry may have been offering bribes to win support in the battle against prepaid cards , or it may have been laundering money back and forth between the JSP and the North Korean residents ' organization , the Chosen Soren .	I-expansion	3359..3645
The Chosen Soren and the JSP immediately denied the report .	B-expansion	3648..3707
And at first , neither the opposition nor the LDP wanted to pursue the issue .	B-comparison	3708..3784
But the press kept it alive ; as with the Recruit scandal , lists began circulating with names of people who had received money .	I-comparison	3785..3911
Within a matter of weeks , less-conservative magazines reported that members of the ruling LDP had received much larger donations from pachinko organizations .	O	3914..4071
So far , though , there have been no allegations that the contributions the LDP members received amounted to bribes .	O	4072..4186
Then the two camps upped the ante : Reports that Chosen Soren had donated directly to JSP members were rapidly countered by statements that the South Korean residents ' organization had long been donating directly to LDP members .	O	4187..4414
The JSP admitted Oct. 13 that its members received about eight million yen from the pachinko organization , and charged LDP members with receiving 125 million yen ( $ 880,000 ) and other opposition parties with taking about 2.5 million yen .	B-temporal	4417..4653
On Friday , the chief cabinet secretary announced that eight cabinet ministers had received five million yen from the industry , including 450,000 yen ( $ 3,175 ) by Prime Minister Toshiki Kaifu .	B-comparison	4654..4844
No one has alleged that the donations were by themselves illegal .	I-comparison	4845..4910
Direct donations from either of the residents ' organizations would be illegal because the groups are defined as foreign , but both groups deny making direct donations .	O	4913..5079
They say it s possible some of their members may be donating privately .	O	5080..5150
The issue is further complicated because although the organizations represent Korean residents , those residents were largely born and raised in Japan and many speak only Japanese .	B-entrel	5153..5332
That they retain Korean citizenship and ties is a reflection of history -- their parents were shipped in as laborers during the decades when Japan occupied Korea before World War II -- and the discrimination that still faces Koreans in Japanese society .	I-entrel	5333..5586
Many Japanese think it only natural that the organizations or their members would donate to politicians , the way many Japanese do , to win favor or support .	O	5589..5744
Both residents ' organizations admit to receiving some funding from abroad .	B-comparison	5747..5821
But LDP members and supporters of the prepaid card idea tend to speak in innuendo about the JSP 's alleged donations , implying that North Korean money would be more suspect than South Korean because North Korea is communist and South Korea is an ally .	I-comparison	5822..6072
