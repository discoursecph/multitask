Next to Kohlberg Kravis Roberts 's megabillion RJR Nabisco deal , SCI Television is small fry .	B-contrast	9..101
But the troubles of SCI TV are a classic tale of the leveraged buy-out excesses of the 1980s , especially the asset-stripping game .	I-contrast	104..234
SCI TV , which expects to release a plan to restructure $ 1.3 billion of debt in the next day or so , is n't just another LBO that went bad after piling on debt -- though it did do that .	B-contrast	237..419
The cable and TV station company was an LBO of an LBO , a set of assets that were leveraged twice , enabling the blue-chip buy-out king Henry Kravis in 1987 to take more than $ 1 billion of cash out of the com - pany .	I-contrast	420..633
SCI TV 's buy-out was an ace in the hole for Mr. Kravis and for investors in KKR partnerships .	B-contrast	636..729
But it has left holders of SCI TV 's junk bonds holding the bag , including some heavyweights that KKR might need to finance future deals , such as Kemper Financial Services , First Executive , Columbia Savings & Loan and Prudential Insurance Co. of America .	I-contrast	730..983
Some junk-holders are said to be considering legal action against KKR or moves to force SCI TV into bankruptcy court .	B-conjunction	986..1103
Some junk-holders are said to be considering legal action against KKR or moves to force SCI TV into bankruptcy court .	B-conjunction	986..1103
And KKR 's majority partner in SCI TV 's buy-out , Nashville , Tenn. , entrepreneur George Gillett , also is said to be very unhappy .	I-conjunction	1104..1231
SCI TV 's six stations once were part of Storer Communications .	O	1234..1296
KKR loaded up the cable and television company with debt in an 1985 buy-out , then later sold Storer 's cable operations at a fat profit .	O	1297..1432
In 1987 , KKR for the second time piled debt onto Storer 's TV stations , selling them for $ 1.3 billion to a new entity that was 45%-owned by KKR and 55%-owned by Gillett Corp. , which now operates the SCI TV stations .	O	1435..1649
In this second LBO , KKR with one hand took more than $ 1 billion of cash out of the TV company 's assets and moved it into the Storer cable operations , making them more valuable in a 1988 sale .	B-conjunction	1650..1841
( Storer also took $ 125 million of junior SCI TV bonds as partial payment for the TV assets . )	I-conjunction	1842..1934
With the other hand , KKR put back into SCI TV less than 10 % of the cash it had taken out , buying SCI TV common and preferred shares .	B-cause	1937..2069
So , while KKR today has an estimated $ 250 million sunk in now-shaky SCI TV , including equity and debt , the LBO firm still is $ 1 billion ahead on the SCI TV buy-out after taking cash up front .	B-conjunction	2072..2263
On Storer as a whole , KKR racked up compound annual returns of 60 % in the three years it owned Storer .	B-synchrony	2264..2366
Meanwhile , Mr. Gillett risks losing his entire equity investment of about $ 100 million in SCI TV if the company ca n't be restructured .	B-restatement	2369..2503
Meanwhile , Mr. Gillett risks losing his entire equity investment of about $ 100 million in SCI TV if the company ca n't be restructured .	B-conjunction	2369..2503
Overall , Mr. Gillett 's holding company , Gillett Holdings , is heavily indebted and , except for its Vail Mountain resorts , is n't doing very well .	I-conjunction	2504..2647
With the TV business falling on hard times in recent years , analysts say that if SCI TV had to be liquidated today , it might fetch 30 % less than in the 1987 buy-out , wiping out most of the company 's junk-holders and its stockholders .	O	2650..2883
Meanwhile , SCI TV can barely pay its cash interest bill , and to stay out of bankruptcy court it must soon reschedule a lot of bank loans and junk bonds that have fallen due .	O	2884..3057
SCI TV 's grace period for paying its bills is due to expire Nov. 16 .	B-cause	3060..3128
It now is quietly circulating among creditors a preliminary plan to restructure debt .	B-entrel	3129..3214
Negotiations " have started con - structively , but that 's not to say we like this particular offer , " says Wilbur Ross of Rothschild Inc. , adviser to SCI TV junk-holders .	I-entrel	3215..3382
No major player in the SCI TV deal will talk publicly .	B-contrast	3385..3439
But it 's understood that Mr. Kravis is disappointed that Mr. Gillett did n't manage to boost SCI TV 's operating profit after the buy-out .	I-contrast	3440..3576
Mr. Kravis apparently thinks SCI TV can survive if lenders extend its debt payments until TV stations rise in value again , allowing SCI TV to sell assets to pay debt .	B-entrel	3577..3743
Mr. Gillett is said to be proud of his operating record ; he has lifted some stations ' ratings and turned around a Detroit station .	I-entrel	3744..3874
As for junk-holders , they 're discovering it can be a mistake to take the other side of a trade by KKR .	B-pragmatic cause	3877..3979
The bonds of SCI TV now are being quoted at prices ranging from only five cents to about 60 cents on the dollar , according to R.D. Smith & Co. in New York , which trades distressed securities .	I-pragmatic cause	3980..4171
People who have seen SCI TV 's restructuring plan say it offers concessions by KKR and Gillett Corp .	O	4174..4273
They would both give part of their combined $ 50 million in common equity in SCI TV to holders of SCI TV 's $ 488 million of junk bonds , as a carrot to persuade them to accept new bonds that might reduce the value of their claims on the company .	B-concession	4274..4516
But some militant SCI TV junk-holders say that 's not enough .	B-cause	4519..4579
They contend that SCI TV 's equity now is worthless .	I-cause	4580..4631
They add that it is n't costing KKR anything to give up equity because of its big up-front cash profit on the buy-out , which they think contributed to SCI TV 's current problems .	B-conjunction	4632..4808
Kemper , the biggest holder of senior SCI TV bonds , has refused to join the bond-holders committee and is said to be reviewing its legal options .	I-conjunction	4809..4953
To protect their claims , some junk-holders want KKR and perhaps Mr. Gillett to invest new money in SCI TV , perhaps $ 50 million or more .	B-entrel	4956..5091
One investment banker who is n't involved in the deal says SCI TV needs at least $ 50 million of new equity to survive .	I-entrel	5092..5209
Junk-holders say they have a stick to beat KKR with : " The threat of bankruptcy is a legitimate tool " to extract money from KKR , says one big SCI TV holder .	O	5212..5367
This could be the first major bankruptcy-law proceeding for KKR , he adds .	B-entrel	5368..5441
A big bankruptcy-court case might tarnish KKR 's name , and provide new fuel for critics of LBOs in Washington and elsewhere .	I-entrel	5442..5565
But others say junk-holders have nothing to gain by putting SCI TV into bankruptcy-law proceedings .	O	5566..5665
While KKR does n't control SCI TV which is unusual for a KKR investment -- it clearly has much deeper pockets than Mr. Gillett .	O	5668..5793
Bankruptcy specialists say Mr. Kravis set a precedent for putting new money in sour LBOs recently when KKR restructured foundering Seaman Furniture , doubling KKR 's equity stake .	O	5794..5971
But with Seaman , KKR was only trying to salvage its original investment , says bankruptcy investor James Rubin of Sass Lamle Rubin in New York .	O	5974..6116
By contrast , KKR probably has already made all the money it can on SCI TV .	O	6117..6191
And people who know Mr. Kravis say he is n't in a hurry to pour more money into SCI TV .	O	6192..6278
