USX Corp. and its Japanese partner, Kobe Steel Ltd., agreed to form a joint venture to build a new plant to produce hot-dipped galvanized sheet products, mainly for the automotive market.	O	9..196
Terms weren't disclosed for the plant, which will have annual capacity of 600,000 tons.	O	199..286
The move by the nation's largest steelmaker follows a string of earlier announcements by other major steel companies.	B-restatement	289..406
Bethlehem Steel Corp., LTV Corp. and Armco Inc. all have plans to build additional lines for such coated corrosion-resistant steel.	I-restatement	407..538
The surge in production, analysts say, raises questions about capacity outpacing demand.	O	541..629
They note that most of the new plants will come on line in 1992, when the current import trade restraint program ends, which could result in more imports.	O	630..784
"There's too much capacity," contended Charles Bradford, an analyst with Merrill Lynch Capital Markets.	O	787..890
"I don't think there's anyone not building one."	O	891..939
He does add, however, that transplanted Japanese car makers are boosting the levels of U.S.-made steel in their autos, instead of relying heavily on imported steel.	O	940..1104
That trend could increase demand for hot-dipped galvanized sheet.	O	1105..1170
The hot-dipped galvanized segment is one of the fastest-growing and most profitable segments of the steel market, coveted by all major integrated steelmakers wanting to maintain an edge over smaller minimills and reconstructed mills -- those spun off to employees.	O	1173..1437
Indeed, USX said it expects the market for coated sheet steel to reach 12 million tons annually by 1992, compared with 10.2 million tons shipped in 1988.	B-entrel	1440..1593
For the first eight months of 1989, analysts say shipments of hot-dipped galvanized steel increased about 8% from a year earlier, while overall steel shipments were up only 2.4%.	I-entrel	1594..1772
USX and Kobe Steel hope to reach a definitive agreement establishing the 50-50 partnership by the end of the year, with construction tentatively slated for the spring of 1990 and production by 1992.	O	1775..1973
USX already has six lines in existing plants producing hot-dipped galvanized steel, but this marks the first so-called greenfield plant for such production.	B-conjunction	1976..2132
Moreover, it will boost by 50% USX's current hot-dipped capacity of 1,275,000 tons.	I-conjunction	2133..2216
The company said it doesn't expect the new line's capacity to adversely affect the company's existing hot-dipped galvanizing lines.	O	2219..2350
Steelmakers have also been adding capacity of so-called electrogalvanized steel, which is another way to make coated corrosion-resistant steel.	B-entrel	2353..2496
One of the advantages of the hot-dipped process is that it allows the steel to be covered with a thicker coat of zinc more quickly.	I-entrel	2497..2628
