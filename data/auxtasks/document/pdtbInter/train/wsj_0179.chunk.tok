Junk-bond markdowns , an ongoing Securities and Exchange Commission investigation , a Drexel Burnham Lambert connection , a fizzled buy-out rumor .	O	9..152
All this has cast a pall over Columbia Savings & Loan Association and its high-rolling 43-year-old chairman , Thomas Spiegel , who built the $ 12.7 billion Beverly Hills , Calif. , thrift with high-yield junk bonds .	O	155..365
Bears have targeted Columbia 's stock because of the thrift 's exposure to the shaky junk market .	O	366..461
And some investors fault Mr. Spiegel 's life style ; he earns millions of dollars a year and flies around in Columbia 's jet planes .	O	462..591
Columbia stock recently hit 4 1/8 , after reaching 11 3/4 earlier this year on rumors that Mr. Spiegel would take the thrift private .	O	594..726
Moreover , junk professionals think Columbia 's huge third-quarter markdown of its junk portfolio to $ 4.4 billion was n't enough , meaning another markdown could be coming .	O	727..895
But in recent days , Columbia has edged up , closing at 5 1/4 , up 3/8 , yesterday on revived speculation that the thrift might restructure .	O	898..1034
Mr. Spiegel 's fans say Columbia 's Southern California branches are highly salable , and the thrift has $ 458 million of shareholders equity underlying its assets .	B-entrel	1035..1195
That 's almost $ 10 of equity for each Columbia share , including convertible preferred shares , though more junk markdowns would reduce the cushion .	I-entrel	1196..1341
Columbia has only about 10 million common shares in public hands .	B-entrel	1344..1409
The Spiegel family has 25 % of the common and 75 % of the votes .	B-entrel	1410..1472
Other big common holders are Carl Lindner 's American Financial , investor Irwin Jacobs and Pacific Financial Research , though the latter cut its stake this summer .	I-entrel	1473..1635
While many problems would attend a restructuring of Columbia , investors say Mr. Spiegel is mulling such a plan to mitigate Columbia 's junk problems .	O	1638..1786
Indeed , Columbia executives recently told reporters they were interested in creating a separate unit to hold Columbia 's junk bonds and perhaps do merchant banking .	O	1787..1950
Columbia wo n't comment on all the speculation .	B-contrast	1953..1999
But like other thrifts , it 's expected to seek regulators ' consent to create a distinct junk-bond entity .	B-entrel	2000..2104
Plans to do this are due to be filed in a week or so .	I-entrel	2105..2158
New rules force thrifts to write down their junk to market value , then sell the bonds over five years .	B-cause	2161..2263
That 's why Columbia just wrote off $ 130 million of its junk and reserved $ 227 million for future junk losses .	I-cause	2264..2373
But if Columbia could keep its junk bonds separate from the thrift till they mature -- at full value , unless the issuer goes bust or restructures -- the junk portfolio might do all right .	O	2374..2561
Columbia , a longtime Drexel client , wo n't provide current data on its junk .	B-contrast	2564..2639
But its 17 big junk holdings at year end showed only a few bonds that have been really battered .	I-contrast	2640..2736
These were Allied Stores , Western Union Telegraph , Gillett Holdings , SCI Television and Texas Air , though many other bonds in Columbia 's portfolio also have lost value .	O	2737..2905
Possibly offsetting that , Columbia recently estimated it has unrealized gains on publicly traded equity investments of more than $ 70 million .	B-conjunction	2906..3047
It also hopes for ultimate gains of as much as $ 300 million on equity investments in buy-outs and restructurings .	I-conjunction	3048..3161
One trial balloon Mr. Spiegel is said to have floated to investors : Columbia might be broken up , as Mellon Bank was split into a good bank and a bad bank .	O	3164..3318
But Columbia 's good bank would be a regulated thrift , while the bad bank would be a private investment company , holding some of Columbia 's junk bonds , real estate and equity investments .	O	3319..3505
Some think Columbia 's thrift , which now is seeking a new chief operating officer , might be capitalized at , say $ 300 million , and shopped to a commercial bank that wants a California presence .	O	3508..3699
The thrift surely could be sold for more than the value of its equity , financial industry executives say .	O	3700..3805
Meanwhile , the bad bank with the junk bonds -- and some capital -- might be spun off to Columbia shareholders , including Mr. Spiegel , who might then have a new career , investors say .	O	3808..3990
It is n't clear how much a restructuring would help Columbia stockholders .	B-contrast	3993..4066
But " the concept is workable .	I-contrast	4067..4096
You sell the good bank as an ongoing operation and use some of the proceeds to capitalize the bad bank , " says thrift specialist Lewis Ranieri of Ranieri Associates in New York .	O	4097..4273
Mr. Spiegel 's next career move is a subject of speculation on Wall Street .	B-cause	4276..4350
Few people think Mr. Spiegel wants to run a bread-and-butter thrift , which current rules would force Columbia to become .	I-cause	4351..4471
" They are n't really a thrift , " says Jonathan Gray , a Sanford C. Bernstein analyst .	O	4474..4556
Of course , regulators would have to approve Columbia 's reorganization .	B-conjunction	4559..4629
And some investment bankers say a restructuring is n't feasible while the SEC still is scrutinizing Mr. Spiegel 's past junk-bond trades .	I-conjunction	4630..4765
Pauline Yoshihashi in Los Angeles contributed to this article .	O	4768..4830
Columbia Savings & Loan ( NYSE ; Symbol : CSV )	O	4833..4878
Business : Savings and loan	O	4881..4907
Year ended Dec. 31 , 1988 : Net income : $ 65 million ; or $ 1.49 a share	O	4910..4979
Third quarter , Sept. 30 , 1989 : Net loss : $ 11.57 a share vs. net income : 37 cents a share	O	4982..5074
Average daily trading volume : 83,206 shares	O	5077..5122
Common shares outstanding : 19.6 million	O	5125..5164
Note : All per-share figures are fully diluted .	O	5167..5213
